Bowser's Blueprints is under heavy development, and I have most core features
planned out, but there are some places that I'm looking for contributors. The
main thing right now is supporting more file formats for importing models.

To implement support for a new model format, create a new importer class in
`/src/core/import` that extends from `IModelImporter`. Build up a hash map of
material names to `MaterialFaces` objects, then pass them into the protected
function `optimizeAndSave`, and return the resulting optimized model and
bounding box. See `wavefront.cpp` and `wavefront.hpp` for an existing
implementation to use as reference.
