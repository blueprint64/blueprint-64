QT += core gui widgets network
TEMPLATE = app

TARGET = "blueprint64"

CONFIG += qt object_parallel_to_source
CONFIG(debug, debug|release){
	OBJECTS_DIR = "build/debug/obj"
	MOC_DIR = "build/debug/moc"
	UI_DIR = "build/debug/ui"
	DEFINES += DEBUG
}
CONFIG(release, debug|release){
	OBJECTS_DIR = "build/release/obj"
	MOC_DIR = "build/release/moc"
	UI_DIR = "build/release/ui"
}

unix {
	# TODO: The Jammy builds set these flags, but I don't want them
	
	QMAKE_CXXFLAGS -= -flto=auto -ffat-lto-objects
	QMAKE_LFLAGS -= -flto=auto -ffat-lto-objects
	
	QMAKE_CFLAGS -= -flto=auto -ffat-lto-objects
	QMAKE_CFLAGS_RELEASE -= -flto=auto -ffat-lto-objects
	QMAKE_CFLAGS_DEBUG -= -flto=auto -ffat-lto-objects
	QMAKE_CXXFLAGS_RELEASE -= -flto=auto -ffat-lto-objects
	QMAKE_CXXFLAGS_DEBUG -= -flto=auto -ffat-lto-objects
	QMAKE_LFLAGS_RELEASE -= -flto=auto -ffat-lto-objects
	QMAKE_CXXFLAGS_DEBUG -= -flto=auto -ffat-lto-objects
}

win32 {
	CONFIG += embed_manifest_exe
	QMAKE_CXXFLAGS += /std:c++17 /WX /utf-8 /Zc:preprocessor /Wv:18
	DEFINES += _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
	LIBS += -lonecore -Lwindows/lib -lzlib
	INCLUDEPATH += windows/include
	
	VERSION = 0.20.0
	RC_ICONS = "data\\img\\appicon.ico"
	RC_LANG = "EN-CA"
	QMAKE_TARGET_COMPANY = "Matt Pharoah"
	QMAKE_TARGET_COPYRIGHT = "GNU GENERAL PUBLIC LICENSE Version 2"
	QMAKE_TARGET_PRODUCT = "Bowser's Blueprints"
}

!win32 {
	QMAKE_CXXFLAGS += -std=c++17 -Werror -fno-strict-aliasing
	QMAKE_LFLAGS += -no-pie -fno-pie
	LIBS += -lstdc++fs -lz
}

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += src/main.cpp src/core/*.cpp src/core/data/*.cpp src/core/import/*.cpp src/core/level-writer/*.cpp src/polyfill/*.cpp src/ui/*.cpp src/ui/tweak-values/*.cpp src/ui/widgets/*.cpp
HEADERS += src/types.hpp src/core/*.hpp src/core/data/*.hpp src/core/import/*.hpp src/core/level-writer/*.hpp src/polyfill/*.hpp src/ui/*.hpp src/ui/tweak-values/*.hpp src/ui/widgets/*.hpp
FORMS += src/ui/designer/*.ui
RESOURCES = data/resources.qrc
