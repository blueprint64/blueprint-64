.create "scrolling-textures.bin",0
.n64

/*
Custom scrolling texture data format:
struct ScrollingTexture {
  0x00	void *addr; // segmented pointer to G_SETTILESIZE command
  0x04  short areaMask;
  0x06  short type; // 0 = linear, 1 = sine
  0x08  ushort width; // width in quarter pixels
  0x0A  ushort height; // height in quarter pixels
  // For Linear Scrolling
  0x0C  int ds; // S speed (quarter pixels/frame) [fixed point >> 16]
  0x10  int dt; // T speed (quarter pixels/frame) [fixed point >> 16]
  // For Sine Wave Scrolling
  0x0C  ushort angle;
  0x0E  ushort period;
  0x10  float amplitude;
} (size 0x14)
*/

g_current_level equ 0x8032DDF8 ; short
g_area_timer equ 0x8032DF08 ; ushort
g_area_index equ 0x8033BACA ; ushort
segmented_to_virtual equ 0x80277F50

imported_levels_table equ 0x802005D8
animation_table equ 0x0E000188

; animate_textures()
LW T0, 0x80361158
BEQ T0, R0, exec_vanilla ; Mario not loaded yet. Don't animate
LH T0, g_current_level
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU AT, 0x0 (T0)
BEQ AT, R0, exec_vanilla ; Not a custom level
NOP

; In a custom level
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)
SW S0, 0x18 (SP)
SW S1, 0x14 (SP)
SW S2, 0x10 (SP)
LI.U A0, animation_table
JAL segmented_to_virtual
LI.L A0, animation_table
OR S0, V0, R0
LHU S1, g_area_timer
LHU S2, g_area_index
ORI AT, R0, 0x1
SLLV S2, AT, S2

for_each_scrolling_texture:
	LW AT, 0x0 (S0) ; addr
	BEQ AT, R0, end_loop
	LHU AT, 0x4 (S0) ; area mask
	AND AT, AT, S2
	BEQ AT, R0, continue
	LHU AT, 0x6 (S0) ; type
	BNE AT, R0, handle_sine_wave
	NOP
	handle_linear:
		LW T0, 0x0C (S0)
		MULT T0, S1
		MFLO T1
		SRA A0, T1, 16
		LW T0, 0x10 (S0)
		MULT T0, S1
		MFLO T1
		B set_offet
		SRA A1, T1, 16
	handle_sine_wave:
		LHU T0, 0x0C (S0)
		ANDI T0, T0, 0xFFF0
		SRL T0, T0, 0x2
		LUI AT, 0x8038
		ADDU T0, AT, T0
		L.S F4, 0x7000 (T0) ; cos(angle)
		L.S F16, 0x6000 (T0) ; sin(angle)
		LHU T0, 0x0E (S0)
		DIVU S1, T0
		MFHI T1
		MTC1 T1, F6
		MTC1 T0, F18
		ORI AT, R0, 0xFFFF
		MTC1 AT, F8
		CVT.S.W F6, F6
		CVT.S.W F18, F18
		CVT.S.W F8, F8
		DIV.S F6, F6, F18
		MUL.S F6, F6, F8
		CVT.W.S F6, F6
		MFC1 T0, F6
		ANDI T0, T0, 0xFFF0
		SRL T0, T0, 0x2
		LUI AT, 0x8038
		ADDU T0, AT, T0
		L.S F6, 0x6000 (T0)
		L.S F18, 0x10 (S0)
		MUL.S F6, F6, F18
		MUL.S F4, F4, F6
		MUL.S F16, F16, F6
		CVT.W.S F4, F4
		CVT.W.S F16, F16
		MFC1 A0, F4
		MFC1 A1, F16
		
	set_offet:
	LHU T0, 0x08 (S0)
	DIV A0, T0
	MFHI V0
	SLT AT, V0, R0
	BEQ AT, R0, endif_s_negative
	NOP
		ADD V0, V0, T0
	endif_s_negative:
	LHU T0, 0x0A (S0)
	DIV A1, T0
	MFHI V1
	SLT AT, V1, R0
	BEQ AT, R0, endif_t_negative
	NOP
		ADD V1, V1, T0
	endif_t_negative:
	ANDI V0, V0, 0xFFF
	ANDI V1, V1, 0xFFF
	LUI T0, 0xF200
	SLL V0, V0, 12
	OR V0, V0, V1
	OR S1, T0, V0
	JAL segmented_to_virtual
	LW A0, 0x0 (S0)
	SW S1, 0x0 (V0)
	LHU S1, g_area_timer
	
	continue:
	B for_each_scrolling_texture
	ADDIU S0, S0, 0x14
end_loop:

ORI A0, R0, 0x0
LW S2, 0x10 (SP)
LW S1, 0x14 (SP)
LW S0, 0x18 (SP)
LW RA, 0x1C (SP)
J 0x8029D690
ADDIU SP, SP, 0x20

exec_vanilla:
J 0x8029D690
ORI A0, R0, 0x0

.close
