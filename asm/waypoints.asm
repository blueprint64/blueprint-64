.create "waypoints.bin",0
.n64

vanilla_waypoints_table equ 0x80200454
custom_waypoints_table equ 0x0E00012C
imported_levels_table equ 0x802005D8

segmented_to_virtual equ 0x80277F50

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

SLL A0, A0, 0x2

LH T0, 0x8032DDF8
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU AT, 0x0 (T0)
BNE AT, R0, @@else_if_custom_level
	LI T0, vanilla_waypoints_table
	ADDU A0, T0, A0
	B @@endif_vanilla_level
	LW A0, 0x0 (A0)
@@else_if_custom_level:
	SW A0, 0x10 (SP)
	LI.U A0, custom_waypoints_table
	JAL segmented_to_virtual
	LI.L A0, custom_waypoints_table
	LW A0, 0x10 (SP)
	ADDU A0, V0, A0
	LW A0, 0x0 (A0)
@@endif_vanilla_level:

LW RA, 0x14 (SP)
J segmented_to_virtual
ADDIU SP, SP, 0x18

.close
