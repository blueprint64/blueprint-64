.create "geo-switch-opacity.bin",0
.n64

g_current_node_object equ 0x8032DF00
o_opacity equ 0x17F
o_case equ 0x1E

; geo_switch_opacity
ORI AT, R0, 1
BNE A0, AT, @@return
	LW T0, g_current_node_object
	LBU T0, o_opacity (T0)
	
	ORI AT, R0, 0xFF
	BEQ T0, AT, @@return
	SH R0, o_case (A1)
	
	BNE T0, R0, @@endif_invisible
		ORI AT, R0, 2
		B @@return
		SH AT, o_case (A1)
	@@endif_invisible:
		
	ORI AT, R0, 1
	SH AT, o_case (A1)
@@return:
JR RA
OR V0, R0, R0

.close
