/*
This asm code is for injecting the main block of custom code into the game.
This code overwrites the code that developers used to record demos, but is not
called by anything in the finished game. This code loads other custom asm code
into an unused buffer of 0x1400 bytes located at 0x801FF200

Additionally, this also loads 0x11200 bytes of data from 0x7CC6C0 in the ROM to
0x80367500 in RAM. This memory is not used by Bowser's Blueprints, and is
instead used as a place for ROMhackers to place their own custom code.
*/

.create "blueprint-code-injector.bin",0x802481E0
.n64
.area 0x124

SHIMMED_FUNCTION equ 0x80248964
dma_read equ 0x80278504
flush_all_writes equ 0x80322F40
invalidate_instruction_cache equ 0x80324610
invalidate_data_cache equ 0x803243B0

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

JAL flush_all_writes
NOP

LI A0, 0x801FF200
LI A1, 0x03C9DE00
JAL read_with_cache_invalidation
ORI A2, R0, 0x1400

LI A0, 0x80367500
LI A1, 0x7CC6C0
LUI A2, 0x1
JAL read_with_cache_invalidation
ORI A2, A2, 0x1200

LW RA, 0x14 (SP)
J SHIMMED_FUNCTION
ADDIU SP, SP, 0x18

read_with_cache_invalidation:
; a0 = RAM start
; a1 = ROM start
; a2 = num bytes
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)
SW A0, 0x18 (SP)
SW A2, 0x20 (SP)

JAL dma_read
ADDU A2, A1, A2

LW A0, 0x18 (SP)
JAL invalidate_instruction_cache
LW A1, 0x20 (SP)

LW A0, 0x18 (SP)
LW A1, 0x20 (SP)
LW RA, 0x14 (SP)
J invalidate_data_cache
ADDIU SP, SP, 0x18

.endarea
.close
