.create "pause-camera-shim.bin",0
.n64

.definelabel bbp_pause_settings, 0x8032DF90
.definelabel imported_levels_table, 0x802005D8
.definelabel g_current_level, 0x8032DDF8

gc_pos_x equ 0x1C
gc_pos_y equ 0x20
gc_pos_z equ 0x24
gc_focus_x equ 0x28
gc_focus_y equ 0x2C
gc_focus_z equ 0x30

LH T0, g_current_level
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU AT, 0x0 (T0)
BEQ AT, R0, @@return

LI T0, bbp_pause_settings
LW T1, 0x40 (SP) ; camera graph node

LW AT, 0x0 (T0)
SW AT, gc_pos_x (T1)
LW AT, 0x4 (T0)
SW AT, gc_pos_y (T1)
LW AT, 0x8 (T0)
SW AT, gc_pos_z (T1)

LW AT, 0xC (T0)
SW AT, gc_focus_x (T1)
LW AT, 0x10 (T0)
SW AT, gc_focus_y (T1)
LW AT, 0x14 (T0)
SW AT, gc_focus_z (T1)

@@return:
J 0x80287B9C
NOP

.close
