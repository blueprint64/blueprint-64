.create "boss-camera-trigger.bin",0
.n64

BOSS_CAM equ 0xB
FLAG_STOP_TRIGGER_PROCESSING equ 0x1000
cam_mode equ 0x0
cam_next_yaw equ 0x3A
g_cam_offset_yaw equ 0x8033C772
g_secondary_camera_focus equ 0x8032DF30
g_obj_ptr_U equ 0x8036
g_current_L equ 0x1160
g_mario_L equ 0x1158
g_camera_status_flags equ 0x8033C84A
cur_obj_nearest_object_with_behavior equ 0x8029F95C
transition_to_camera_mode equ 0x80286088

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)
SW A0, 0x18 (SP)
SW A2, 0x1C (SP)

LBU T0, cam_mode (A0)
ORI AT, R0, BOSS_CAM
BEQ T0, AT, @@return
	LUI AT, g_obj_ptr_U
	LW T0, g_current_L (AT)
	SW T0, 0x10 (SP)
	LW T0, g_mario_L (AT)
	SW T0, g_current_L (AT)
	
	JAL cur_obj_nearest_object_with_behavior
	OR A0, A1, R0
	
	LUI AT, g_obj_ptr_U
	LW T0, 0x10 (SP)
	SW T0, g_current_L (AT)
	
	BEQ V0, R0, @@return
	LUI T0, hi( g_camera_status_flags )
	LHU AT, lo( g_camera_status_flags ) (T0)
	ORI AT, AT, FLAG_STOP_TRIGGER_PROCESSING
	SH AT, lo( g_camera_status_flags ) (T0)

	SW V0, g_secondary_camera_focus
	LW A0, 0x18 (SP)
	ORI A1, R0, BOSS_CAM
	JAL transition_to_camera_mode
	LW A2, 0x1C (SP)

	LW T0, 0x18 (SP)
	LH T0, cam_next_yaw (T0)
	ADDIU T0, T0, 0xE000
	SH T0, g_cam_offset_yaw

@@return:
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18 

.close
