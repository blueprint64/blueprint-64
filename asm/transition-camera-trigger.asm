.create "transition-camera-trigger.bin",0
.n64

transition_to_camera_mode equ 0x80286088
g_camera_status_flags equ 0x8033C84A
g_sc_base_yaw equ 0x8033C776
g_sc_offset_yaw equ 0x8033C778
c_mode equ 0x0

; a0 = camera pointer
; a1 = camera mode
; a2 = baseAngle [short] (only used for semi-cardinal)
; a3 = transition frames
LUI T0, hi( g_camera_status_flags )
LHU AT, lo( g_camera_status_flags ) (T0)
ORI AT, AT, 0x1000
SH AT, lo( g_camera_status_flags ) (T0)

LBU AT, c_mode (A0)
BEQ AT, A1, @@return
	ORI AT, R0, 14
	BNE A1, AT, @@endif_semi_cardinal
		LUI AT, hi( g_sc_base_yaw )
		SH A2, lo( g_sc_base_yaw ) (AT)
		SH R0, lo( g_sc_offset_yaw ) (AT)
		OR A3, R0, R0
	@@endif_semi_cardinal:
	LUI T0, hi( g_camera_status_flags )
	LHU AT, lo( g_camera_status_flags ) (T0)
	BEQ A3, R0, @@commit_flags
	ANDI AT, AT, 0xFFFE
		ORI AT, AT, 0x1
	@@commit_flags:
	SH AT, lo( g_camera_status_flags ) (T0)
	J transition_to_camera_mode
	OR A2, R0, A3
@@return:
JR RA
NOP

.close
