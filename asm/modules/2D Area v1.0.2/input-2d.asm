; Ignore the Y axis on the controller for the purposes of determining Mario's
; movement (menus still work normally)

$adjust_inputs:
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)

JAL $is_2d_area
NOP

BNE V0, R0, @@endif_3d
	LW RA, 0x1C (SP)
	#continue adjust_inputs
	ADDIU SP, SP, 0x20
@@endif_3d:

LW T0, ($g_mario + $m_controller)
L.S F4, $c_x (T0)
S.S F4, 0x18 (SP)
LW AT, $c_y (T0)
SW AT, 0x14 (SP)
LW AT, $c_mag (T0)
SW AT, 0x10 (SP)

SW R0, $c_y (T0)
ABS.S F4, F4
#await adjust_inputs
S.S F4, $c_mag (T0)

LW T0, ($g_mario + $m_controller)
LW AT, 0x10 (SP)
SW AT, $c_mag (T0)
LW AT, 0x14 (SP)
SW AT, $c_y (T0)
LW AT, 0x18 (SP)
SW AT, $c_x (T0)

@@return:
LW RA, 0x1C (SP)
JR RA
ADDIU SP, SP, 0x20
