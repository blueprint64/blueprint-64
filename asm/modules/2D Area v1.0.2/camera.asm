@backup_camera_data:
.word 0

$force_camera_mode:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	SW A0, 0x10 (SP)
	
	LBU AT, $cam_custcene (A0)
	BNE AT, R0, @@return
	NOP

	JAL $is_2d_area
	NOP
	BEQ V0, R0, @@return
	NOP

	LW A0, 0x10 (SP)
	LI T0, @backup_camera_data

	LHU T1, 0x8033C684 ; Camera selection flags
	SH T1, 0x0 (T0)
	ANDI T1, T1, 0xFFFE
	SH T1, 0x8033C684

	LBU AT, $cam_mode (A0)
	SB AT, 0x3 (T0)
	SB R0, $cam_mode (A0)

	@@return:
	LW A0, 0x10 (SP)
	LW RA, 0x14 (SP)
	#continue force_camera_mode
	ADDIU SP, SP, 0x18

$2d_cam:
	ADDIU SP, SP, 0xFFE0
	SW RA, 0x14 (SP)
	SW A0, 0x18 (SP)
	SW A1, 0x1C (SP)
	
	LBU AT, $cam_custcene (A0)
	BNE AT, R0, @@return
	NOP

	JAL $is_2d_area
	NOP
	BEQ V0, R0, @@return
	NOP

	LW A0, 0x18 (SP)
	LBU AT, $cam_mode (A0)
	BNE AT, R0, @@return
	NOP

	JAL @update_2d_cam
	NOP

	LHU T0, @backup_camera_data
	SH T0, 0x8033C684
	LW T0, 0x18 (SP)
	LBU T1, (@backup_camera_data + 0x3)
	LW A0, 0x18 (SP)
	SB T1, $cam_mode (A0)

	@@return:
	LW A1, 0x1C (SP)
	LW A0, 0x18 (SP)
	LW RA, 0x14 (SP)
	#continue 2d_cam
	ADDIU SP, SP, 0x20

@update_2d_cam:
	ADDIU SP, SP, 0xFFE0
	SW RA, 0x1C (SP)
	SW A0, 0x20 (SP)
	
	JAL @get_target_focus
	ADDIU A0, SP, 0x10
	
	LW A0, 0x20 (SP)
	ADDIU A0, A0, $cam_focus
	ADDIU A1, SP, 0x10
	JAL @approach_vector
	LUI A2, 0x42C0
	
	LW A0, 0x20 (SP)
	LW AT, $cam_focus_x (A0)
	SW AT, $cam_x (A0)

	LI T0, float( $CAMERA_DISTANCE )
	SW T0, $cam_z (A0)
	
	LI.S F6, $CAMERA_HEIGHT
	L.S F4, $cam_focus_y (A0)
	ADD.S F4, F4, F6
	.if $CAMERA_IGNORE_FLOORS
		S.S F4, $cam_y (A0)
	.else
		MFC1 A1, F4
		JAL @set_camera_height_fast
		S.S F4, 0x14 (SP)
		LW A0, 0x20 (SP)
		L.S F4, $cam_y (A0)
		L.S F6, 0x14 (SP)
		SUB.S F6, F4, F6
		L.S F4, $cam_focus_y (A0)
		ADD.S F4, F4, F6
		S.S F4, $cam_focus_y (A0)
	.endif
	
	SH R0, $cam_yaw (A0)
	SH R0, $cam_next_yaw (A0)
	
	LW RA, 0x1C (SP)
	JR RA
	ADDIU SP, SP, 0x20
	
@get_target_focus:
	LI.S F6, $CAMERA_LOOKAHEAD
	LI T0, $g_mario
	LW T1, $m_controller (T0)
	LHU T1, $c_buttons_held (T1)
	
	L.S F4, $m_x (T0)
	ANDI AT, T1, $C_BUTTON_C_RIGHT
	BEQ AT, R0, @@endif_look_right
	NOP
		ADD.S F4, F4, F6
	@@endif_look_right:
	ANDI AT, T1, $C_BUTTON_C_LEFT
	BEQ AT, R0, @@endif_look_left
		NOP
		SUB.S F4, F4, F6
	@@endif_look_left:
	S.S F4, 0x0 (A0)

	L.S F4, $m_y (T0)
	ANDI AT, T1, $C_BUTTON_C_UP
	BEQ AT, R0, @@endif_look_up
	NOP
		ADD.S F4, F4, F6
	@@endif_look_up:
	ANDI AT, T1, $C_BUTTON_C_DOWN
	BEQ AT, R0, @@endif_look_down
		NOP
		SUB.S F4, F4, F6
	@@endif_look_down:
	S.S F4, 0x4 (A0)
	
	JR RA
	SW R0, 0x8 (A0)

@approach_vector:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	SW A0, 0x18 (SP)
	SW A1, 0x1C (SP)
	SW A2, 0x20 (SP)
	
	L.S F12, 0x0 (A0)
	JAL $approach_float
	L.S F14, 0x0 (A1)
	
	LW A0, 0x18 (SP)
	S.S F0, 0x0 (A0)
	LW A1, 0x1C (SP)
	LW A2, 0x20 (SP)
	
	L.S F12, 0x4 (A0)
	JAL $approach_float
	L.S F14, 0x4 (A1)
	
	LW A0, 0x18 (SP)
	S.S F0, 0x4 (A0)
	LW A1, 0x1C (SP)
	LW A2, 0x20 (SP)
	
	L.S F12, 0x8 (A0)
	JAL $approach_float
	L.S F14, 0x8 (A1)
	
	LW A0, 0x18 (SP)
	S.S F0, 0x8 (A0)
	
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

@set_camera_height_fast:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	SW A0, 0x18 (SP)
	JAL $set_camera_height
	SW A1, 0x1C (SP)
	LW A0, 0x18 (SP)
	JAL $set_camera_height
	LW A1, 0x1C (SP)
	LW A0, 0x18 (SP)
	JAL $set_camera_height
	LW A1, 0x1C (SP)
	LW A0, 0x18 (SP)
	JAL $set_camera_height
	LW A1, 0x1C (SP)
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18
