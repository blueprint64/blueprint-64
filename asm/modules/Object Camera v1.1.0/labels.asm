.definelabel $copy_vector, 0x80378800
.definelabel $get_nearest_object_with_behaviour, 0x8029F95C
.definelabel $segmented_to_virtual, 0x80277F50
.definelabel $default_mario_cam, 0x80284D38

.definelabel $mario_cam_state, 0x8032DF60
.definelabel $camera_selection, 0x8033C684
.definelabel $yaw_speed, 0x8032DF34
.definelabel $g_current_obj_ptr, 0x80361160
.definelabel $g_mario_obj_ptr, 0x80361158

$c_pos equ 0x10
$c_focus equ 0x4
$c_target_yaw equ 0x3A

$mc_pos equ 0x4

$o_pos equ 0xA0
$o_pitch equ 0xD0
$o_pitch_hw equ 0xD2
$o_yaw equ 0xD4
$o_yaw_hw equ 0xD6
$o_roll equ 0xD8
$o_roll_hw equ 0xDA
$o_active_flags equ 0x74
$o_behaviour equ 0x20C

$MARIO_CAM equ 1
