@replace_mario_cam:
.word 0
@behaviour_type:
.word 0
@object_instance:
.word 0
@camera_roll:
.halfword 0, 0

$roll_shim:
	LH T0, @camera_roll
	SH R0, @camera_roll
	LHU T1, 0x0 (A0)
	ADDU T1, T1, T0
	#continue roll_shim
	SH T1, 0x0 (A0)

$object_cam_init:
	LI T0, @replace_mario_cam
	LW AT, 0x0 (A1)
	SW AT, 0x0 (T0)
	LW AT, 0x4 (A1)
	SW AT, 0x4 (T0)
	JR RA
	SW R0, 0x8 (T0)
	
$object_cam:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	SW A0, 0x10 (SP)
	
	LW T0, @replace_mario_cam
	BNE T0, R0, @@exec_camera
	LHU T0, $camera_selection
	ANDI T0, T0, $MARIO_CAM
	BNE T0, R0, @@fallback_camera
	
	@@exec_camera:
	LW T0, @object_instance
	BEQ T0, R0, @@try_get_object_instance
	NOP
	
	LHU AT, $o_active_flags (T0)
	ANDI AT, AT, 0x1
	BEQ AT, R0, @@try_get_object_instance
	LW.U A0, @behaviour_type
	JAL $segmented_to_virtual
	LW.L A0, @behaviour_type
	LW T0, @object_instance
	LW AT, $o_behaviour (T0)
	BNE V0, AT, @@try_get_object_instance
	
	@@update_camera:
	ORI T0, R0, 0x1000
	SH T0, $yaw_speed
	
	LW A0, 0x10 (SP)
	LW A1, @object_instance
	LW AT, $o_yaw (A1)
	ADDIU AT, AT, 0x8000
	SH AT, $c_target_yaw (A0)
	
	ADDIU A0, A0, $c_pos
	JAL $copy_vector
	ADDIU A1, A1, $o_pos
	
	LW T0, @object_instance
	LW T1, 0x10 (SP)
	LW A0, $o_pitch (T0)
	LW A1, $o_yaw (T0)
	JAL @pitch_and_yaw_to_unit_vector
	ADDIU A2, T1, $c_focus
	
	LI.S F12, 1024
	LW T0, 0x10 (SP)
	JAL @scale_vector
	ADDIU A0, T0, $c_focus
	
	LW T0, 0x10 (SP)
	ADDIU A0, T0, $c_pos
	ADDIU A1, T0, $c_focus
	JAL @add_vectors
	ADDIU A2, T0, $c_focus
	
	LW T0, @object_instance
	LHU T1, $o_pitch_hw (T0)
	LHU T0, $o_roll_hw (T0)
	
	ORI AT, R0, 0x4000
	BEQ T1, AT, @@up_or_down
	ORI AT, R0, 0xC000
	BEQ T1, AT, @@up_or_down
	
	SLTI AT, T1, 0x4000
	BNE AT, R0, @@endif_upside_down
	ORI AT, R0, 0xC000
	SLTU AT, T1, AT
	BEQ AT, R0, @@endif_upside_down
		ORI AT, R0, 0x8000
		ADDU T0, T0, AT
	@@endif_upside_down:
	
	SH T0, @camera_roll
	
	B @@return
	NOP
	
	@@up_or_down:
	LW T0, @object_instance
	LHU T1, $o_yaw_hw (T0)
	LHU T2, $o_roll_hw (T0)
	ADDU T0, T1, T2
	SH T0, @camera_roll
	B @@return
	NOP
	
	@@try_get_object_instance:
	LW.U A0, @behaviour_type
	JAL @try_get_nearest_to_mario
	LW.L A0, @behaviour_type
	SW V0, @object_instance
	BNE V0, R0, @@update_camera
	NOP
	
	@@fallback_camera:
	JAL $default_mario_cam
	LW A0, 0x10 (SP)
	
	@@return:
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

@angle_to_unit_vector:
	ANDI A0, A0, 0xFFF0
	SRL A0, A0, 0x2
	LUI AT, 0x8038
	ADDU A0, AT, A0
	L.S F0, 0x6000 (A0)
	JR RA
	L.S F2, 0x7000 (A0)

@pitch_and_yaw_to_unit_vector:
	ADDIU SP, SP, 0xFFE0
	SW RA, 0x1C (SP)
	SW A2, 0x18 (SP)

	JAL @angle_to_unit_vector
	SW A1, 0x14 (SP)

	NEG.S F0, F0
	S.S F0, 0x4 (A2)
	S.S F2, 0x0 (A2)

	JAL @angle_to_unit_vector
	LW A0, 0x14 (SP)

	LW A2, 0x18 (SP)
	L.S F4, 0x0 (A2)
	MUL.S F0, F0, F4
	MUL.S F2, F2, F4
	S.S F0, 0x0 (A2)
	S.S F2, 0x8 (A2)

	LW RA, 0x1C (SP)
	JR RA
	ADDIU SP, SP, 0x20
	
@try_get_nearest_to_mario:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	
	LW T0, $g_current_obj_ptr
	SW T0, 0x10 (SP)
	LW T0, $g_mario_obj_ptr
	SW T0, $g_current_obj_ptr
	JAL $get_nearest_object_with_behaviour
	NOP
	LW T0, 0x10 (SP)
	SW T0, $g_current_obj_ptr
	
	@@return:
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

@scale_vector:
	L.S F4, 0x0 (A0)
	L.S F6, 0x4 (A0)
	L.S F8, 0x8 (A0)
	MUL.S F4, F4, F12
	MUL.S F6, F6, F12
	MUL.S F8, F8, F12
	S.S F4, 0x0 (A0)
	S.S F6, 0x4 (A0)
	JR RA
	S.S F8, 0x8 (A0)

@add_vectors:
	L.S F4, 0x0 (A0)
	L.S F6, 0x0 (A1)
	ADD.S F4, F4, F6
	S.S F4, 0x0 (A2)
	L.S F4, 0x4 (A0)
	L.S F6, 0x4 (A1)
	ADD.S F4, F4, F6
	S.S F4, 0x4 (A2)
	L.S F4, 0x8 (A0)
	L.S F6, 0x8 (A1)
	ADD.S F4, F4, F6
	JR RA
	S.S F4, 0x8 (A2)
