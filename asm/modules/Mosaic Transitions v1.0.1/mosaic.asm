.definelabel @g_warp_transition, 0x8033BAB0
.definelabel @g_warp_delay, 0x8032DDDC
.definelabel @g_transition_timer, 0x80330EC0

@wt_active equ 0
@wt_type equ 1
@wt_duration equ 2

$mosaic_filter:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	SW A0, 0x10 (SP)

	LBU T0, @g_warp_delay
	BNE T0, R0, @@return

	LI T0, @g_warp_transition
	LBU AT, @wt_active (T0)
	BEQ AT, R0, @@return

	LBU AT, @wt_type (T0)
	SLTIU AT, AT, 2
	BEQ AT, R0, @@return

	LBU A0, @g_transition_timer
	LBU AT, @wt_type (T0)
	BNE AT, R0, @@endif_reverse
		LBU AT, @wt_duration (T0)
		SUBU A0, AT, A0
	@@endif_reverse:

	ADDIU A0, A0, 1
	JAL @render_mosaic
	LW A1, 0x10 (SP)
	
	@@return:
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

@render_mosaic:
	ADDIU SP, SP, 0xFFE0
	SW RA, 0x1C (SP)
	SW S0, 0x18 (SP)
	SW S1, 0x14 (SP)
	SW S2, 0x10 (SP)
	SW A1, 0x24 (SP)
	
	SLTI AT, A0, 2
	BNE AT, R0, @@return
	OR S2, A0, R0
	
	OR S1, R0, R0
	@@loop_y:
		OR S0, R0, R0
		@@loop_x:
			OR A0, R0, S0
			OR A1, R0, S1
			OR A2, R0, S2
			JAL @render_pixel
			LW A3, 0x24 (SP)
		
			ADDU S0, S0, S2
			SLTIU AT, S0, 320
			BNE AT, R0, @@loop_x
			NOP
		
		ADDU S1, S1, S2
		SLTIU AT, S1, 240
		BNE AT, R0, @@loop_y
		NOP
	
	@@return:
	LW S0, 0x18 (SP)
	LW S1, 0x14 (SP)
	LW S2, 0x10 (SP)
	LW RA, 0x1C (SP)
	JR RA
	ADDIU SP, SP, 0x20

@render_pixel:
	ADDIU SP, SP, 0xFFD0
	SW RA, 0x2C (SP)
	SW S0, 0x28 (SP)
	SW S1, 0x24 (SP)
	SW S2, 0x20 (SP)
	SW S3, 0x1C (SP)
	SW S4, 0x18 (SP)
	SW S5, 0x14 (SP)
	SW S6, 0x10 (SP)
	
	SW A0, 0x30 (SP)
	SW A1, 0x34 (SP)
	SW A2, 0x38 (SP)
	SW A3, 0x3C (SP)
	
	OR S2, R0, R0
	OR S3, R0, R0
	OR S4, R0, R0
	
	ADDU T0, A0, A2
	MINIU S5, T0, 320
	
	ADDU T0, A1, A2
	MINIU S6, T0, 240
	
	LW S1, 0x34 (SP)
	@@loop_y:
		LW S0, 0x30 (SP)
		@@loop_x:
			ORI AT, R0, 320
			MULTU S1, AT
			MFLO T0
			ADDU T0, T0, S0
			SLL T0, T0, 1
			LW T1, 0x3C (SP)
			ADDU T0, T1, T0
			LHU T0, 0x0 (T0)
			
			SRL T0, T0, 1
			ANDI T1, T0, 0x1F
			ADDU S4, S4, T1
			SRL T0, T0, 5
			ANDI T1, T0, 0x1F
			ADDU S3, S3, T1
			SRL T0, T0, 5
			ANDI T1, T0, 0x1F
			ADDU S2, S2, T1
			
			ADDIU S0, S0, 1
			SLTU AT, S0, S5
			BNE AT, R0, @@loop_x
			NOP
		
		ADDIU S1, S1, 1
		SLTU AT, S1, S6
		BNE AT, R0, @@loop_y
		NOP
		
	@reset_labels:
	
	LW T1, 0x30 (SP)
	LW T2, 0x34 (SP)
	SUBU T1, S5, T1
	SUBU T2, S6, T2
	MULTU T1, T2
	MFLO T0
	
	NOP
	NOP
	DIVU S2, T0
	MFLO S2
	ANDI S2, S2, 0x1F
	SLL S2, S2, 11
	DIVU S3, T0
	MFLO S3
	ANDI S3, S3, 0x1F
	SLL S3, S3, 6
	DIVU S4, T0
	MFLO S4
	ANDI S4, S4, 0x1F
	SLL S4, S4, 1
	OR S3, S3, S4
	OR S2, S2, S3
	ORI S2, S2, 0x1
	
	LW S1, 0x34 (SP)
	@@loop_y:
		LW S0, 0x30 (SP)
		@@loop_x:
			ORI AT, R0, 320
			MULTU S1, AT
			MFLO T0
			ADDU T0, T0, S0
			SLL T0, T0, 1
			LW T1, 0x3C (SP)
			ADDU T0, T1, T0
			SH S2, 0x0 (T0)
			
			ADDIU S0, S0, 1
			SLTU AT, S0, S5
			BNE AT, R0, @@loop_x
			NOP
		
		ADDIU S1, S1, 1
		SLTU AT, S1, S6
		BNE AT, R0, @@loop_y
		NOP
		
	@@return:
	LW RA, 0x2C (SP)
	LW S0, 0x28 (SP)
	LW S1, 0x24 (SP)
	LW S2, 0x20 (SP)
	LW S3, 0x1C (SP)
	LW S4, 0x18 (SP)
	LW S5, 0x14 (SP)
	LW S6, 0x10 (SP)
	JR RA
	ADDIU SP, SP, 0x30
