$void_out:
LW T0, $m_action (A0)
LI T1, ACT_VOID_OUT
BNE T0, T1, @@endif_voiding_out
NOP
	JR RA
	MOVE V0, R0
@@endif_voiding_out:

LB T0, $m_health (A0)
SLTI AT, T0, ($VOID_DAMAGE + 1)
BEQ AT, R0, @@endif_lethal_fall
NOP
	#continue void_out
	NOP
@@endif_lethal_fall:

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LI A1, ACT_VOID_OUT
JAL $drop_and_set_mario_action
MOVE A2, R0

SETU A0, 0x1
SETU A1, $FADE_OUT_TIME
SETU A2, $FADE_COLOUR_red
SETU A3, $FADE_COLOUR_green
SETU AT, $FADE_COLOUR_blue
JAL $play_transition
SW AT, 0x10 (SP)

SETU V0, 20
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18
