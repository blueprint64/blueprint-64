/* Functions */
.definelabel $mario_floor_is_slippery, 0x80251BD4
.definelabel $play_transition, 0x8027B1A0
.definelabel $drop_and_set_mario_action, 0x80253178
.definelabel $set_mario_action, 0x80252CF4
.definelabel $set_mario_animation, 0x802509B8
.definelabel $mario_air_step, 0x80256B24
.definelabel $reset_camera, 0x80286F68
.definelabel $update_mario_surface_refs, 0x80253A60
.definelabel $atan2s, 0x8037A9A8
.definelabel $common_air_knockback_step, 0x8026D1B0
.definelabel $play_knockback_sound, 0x8026A090
.definelabel $is_animation_finished, 0x80250940
.definelabel $play_mario_landing_sound_once, 0x80251510
.definelabel $play_mario_heavy_landing_sound_once, 0x802515D8
.definelabel $play_sound_if_no_flag, 0x80251120

/* Global Variables */
$g_mario equ 0x8033B170
$g_current_area equ 0x8032DDCC

/* Mario Properties */
$m_action equ 0xC
$m_action_timer equ 0x1A
$m_health equ 0xAE
$m_hurt_counter equ 0xB2
$m_x equ 0x3C
$m_y equ 0x40
$m_z equ 0x44
$m_angle_pitch equ 0x2C
$m_angle_yaw equ 0x2E
$m_angle_roll equ 0x30
$m_angle_vel_pitch equ 0x32
$m_angle_vel_yaw equ 0x34
$m_angle_vel_roll equ 0x36
$m_floor_ptr equ 0x68
$m_speed_x equ 0x48
$m_speed_y equ 0x4C
$m_speed_z equ 0x50
$m_speed_h equ 0x54
$m_slide_speed_x equ 0x58
$m_slide_speed_z equ 0x5C
$m_squish_timer equ 0xB4
$m_peak_height equ 0xBC
$m_iframes equ 0x26
$m_object equ 0x88

/* Object Properties */
$o_gfx_pitch equ 0x1A

/* Surface Triangle Properties */
$t_collision_type equ 0x1
$t_object equ 0x2C

/* Area Properties */
$a_camera equ 0x24

/* Camera Properties */
$c_cutscene equ 0x30

/* Vanilla Actions */
$ACT_IDLE equ 0x0C400201
