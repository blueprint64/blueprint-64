$handle_respawn_action:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LI A0, $g_mario
JAL $play_knockback_sound
SW A0, 0x10 (SP)

LW A0, 0x10 (SP)
LI A1, $ACT_VOID_RESPAWN_KNOCKDOWN
MOVE A2, A1
SETU A3, 0x2
JAL $common_air_knockback_step
SW R0, 0x10 (SP)

MOVE V0, R0
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

$handle_respawn_knockdown_action:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)
SW S0, 0x10 (SP)

LI S0, $g_mario

LI A1, 0x04188081
JAL $play_mario_heavy_landing_sound_once
MOVE A0, S0

MOVE A0, S0
LI A1, 0x240BD081
JAL $play_sound_if_no_flag
LUI A2, 0x2

MOVE A0, S0
JAL $set_mario_animation
SETU A1, 0x01

SETU AT, 0x45
BEQ V0, AT, @@endif_play_landing_sound
	LI A1, 0x04088081
	JAL $play_mario_landing_sound_once
	MOVE A0, S0
@@endif_play_landing_sound:

JAL $is_animation_finished
MOVE A0, S0

BEQ V0, R0, @@return
	SETU AT, 30
	SH AT, $m_iframes (S0)
	MOVE A0, S0
	LI A1, $ACT_IDLE
	JAL $set_mario_action
	MOVE A2, R0

@@return:
MOVE V0, R0

LW S0, 0x10 (SP)
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18
