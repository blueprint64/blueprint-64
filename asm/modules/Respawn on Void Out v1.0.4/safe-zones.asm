@just_entered_area:
.word 1

$respawn_location:
.word 0, 0, 0

$on_area_load:
SETU AT, 1
SW.U AT, @just_entered_area
JR RA
SW.L AT, @just_entered_area

$check_for_safe_ground:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LI T0, $g_mario

LW AT, @just_entered_area
BEQ AT, R0, @@endif_initial_spawn
	LI T1, $respawn_location
	SW R0, @just_entered_area
	LW AT, $m_x (T0)
	SW AT, 0x0 (T1)
	LW AT, $m_y (T0)
	SW AT, 0x4 (T1)
	LW AT, $m_z (T0)
	B @@return
	SW AT, 0x8 (T1)
@@endif_initial_spawn:

LW AT, $m_floor_ptr (T0)
BEQ AT, R0, @@return

LW T1, $m_action (T0)
ANDI T1, T1, 0x1C0
SLTI AT, T1, 0x80
BNE AT, R0, @@grounded
SLTI AT, T1, 0x180
BNE AT, R0, @@return
NOP

@@grounded:
LW T1, $m_floor_ptr (T0)
LW AT, $t_object (T1)
BNE AT, R0, @@return
LBU T1, $t_collision_type (T1)

LI T2, @unsafe_terrain_table
LI T3, @unsafe_terrain_table_end

@@loop:
	LBU AT, 0x0 (T2)
	BEQ T1, AT, @@return
	ADDIU T2, T2, 0x1
	BNE T2, T3, @@loop
	NOP

JAL $mario_floor_is_slippery
MOVE A0, T0

BNE V0, R0, @@return
LW T0, ($g_mario + $m_floor_ptr)
LI T1, $respawn_location
LI.S F6, 3

LH T2, 0x0A (T0)
LH T3, 0x10 (T0)
LH T4, 0x16 (T0)
ADDU T2, T2, T3
ADDU T2, T2, T4
MTC1 T2, F4
NOP
CVT.S.W F4, F4
DIV.S F4, F4, F6
S.S F4, 0x0 (T1)

LH T2, 0x0C (T0)
LH T3, 0x12 (T0)
LH T4, 0x18 (T0)
ADDU T2, T2, T3
ADDU T2, T2, T4
MTC1 T2, F4
NOP
CVT.S.W F4, F4
DIV.S F4, F4, F6
S.S F4, 0x4 (T1)

LH T2, 0x0E (T0)
LH T3, 0x14 (T0)
LH T4, 0x1A (T0)
ADDU T2, T2, T3
ADDU T2, T2, T4
MTC1 T2, F4
NOP
CVT.S.W F4, F4
DIV.S F4, F4, F6
S.S F4, 0x8 (T1)

@@return:
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

@unsafe_terrain_table:
.byte 0x01 ; lava
.byte 0x0A ; death plane
.byte 0x12, 0x72 ; intangible
.byte 0x1B, 0x1C, 0x1D, 0x1E ; instant warp
.byte 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x2D ; quicksand
.byte 0x2C ; wind
.byte 0x32 ; warp
.byte 0x33 ; timer start
.byte 0x38 ; death plane with wind
.byte 0x7B ; vanish cap surface
.byte 0xFD ; HMC warp
@unsafe_terrain_table_end:
.byte 0
.align 4
