$handle_void_action:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LI T0, $g_mario
LHU T1, $m_action_timer (T0)
SLTI AT, T1, $FADE_OUT_TIME
BEQ AT, R0, @@respawn
ADDIU T1, T1, 0x1
SH T1, $m_action_timer (T0)

MOVE A0, T0
JAL $mario_air_step
MOVE A1, R0

MOVE V0, R0
LI T0, $g_mario
LUI T1, 0x1
LHU AT, $m_angle_pitch (T0)
SUBU T1, T1, AT
ANDI T1, T1, 0xFFFF
LW T0, $m_object (T0)
SH T1, $o_gfx_pitch (T0)

@@return:
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

@@respawn:
LI A0, $g_mario
LI A1, $ACT_VOID_RESPAWN
JAL $set_mario_action
MOVE A2, R0

LI T0, $g_mario
LI T1, $respawn_location
L.S F6, $m_x (T0)
L.S F16, $m_z (T0)
L.S F4, 0x0 (T1)
S.S F4, $m_x (T0)
LUI AT, 0x42B4
MTC1 AT, F18
L.S F8, 0x4 (T1)
ADD.S F8, F8, F18
S.S F8, $m_y (T0)
S.S F8, $m_peak_height (T0)
L.S F10, 0x8 (T1)
S.S F10, $m_z (T0)
SH R0, $m_angle_pitch (T0)
SUB.S F14, F6, F4
JAL $atan2s
SUB.S F12, F16, F10
LI T0, $g_mario
SH R0, $m_angle_roll (T0)
SH V0, $m_angle_yaw (T0)
SH R0, $m_angle_vel_pitch (T0)
SH R0, $m_angle_vel_yaw (T0)
SH R0, $m_angle_vel_roll (T0)
SW R0, $m_speed_x (T0)
SW R0, $m_speed_y (T0)
SW R0, $m_speed_z (T0)
SW R0, $m_speed_h (T0)
SW R0, $m_slide_speed_x (T0)
SW R0, $m_slide_speed_z (T0)
SB R0, $m_squish_timer (T0)
SETU AT, $FADE_IN_TIME
SH AT, $m_iframes (T0)
SETU AT, ($VOID_DAMAGE << 2)
SB AT, $m_hurt_counter (T0)

MOVE A0, R0
SETU A1, $FADE_IN_TIME
SETU A2, $FADE_COLOUR_red
SETU A3, $FADE_COLOUR_green
SETU AT, $FADE_COLOUR_blue
JAL $play_transition
SW AT, 0x10 (SP)

LI.U A0, $g_mario
JAL $update_mario_surface_refs
LI.L A0, $g_mario

LW T0, $g_current_area
JAL $reset_camera
LW A0, $a_camera (T0)

LW T0, $g_current_area
LW T0, $a_camera (T0)
SB R0, $c_cutscene (T0)

B @@return
SETU V0, 0x1
