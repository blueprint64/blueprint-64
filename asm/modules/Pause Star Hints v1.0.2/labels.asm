/* Global Variables */
$g_course_num equ 0x8033BAC6
$g_save_file_num equ 0x8032DDF4
$g_current_act equ 0x8033BAC8
$g_mario equ 0x8033B170
$g_zero_vector equ 0x803331F0
$g_act_name_table equ 0x0201192C
$g_level_name_table equ 0x02010F68
$g_display_list_head equ 0x8033B06C

/* Mario State Properties */
$m_controller equ 0x9C

/* Controller Properties */
$c_buttons_pressed equ 0x12

/* Controller Button Flags */
$C_BUTTON_D_PAD_RIGHT equ 0x100
$C_BUTTON_D_PAD_LEFT equ 0x200

/* Sounds */
$SOUND_MENU_CHANGE equ 0x7000F881

/* Functions */
.definelabel $get_stars_collected, 0x8027A1C8
.definelabel $printf, 0x802D62D8
.definelabel $set_sound, 0x8031EB00
.definelabel $segmented_to_virtual, 0x80277F50
.definelabel $print_string, 0x802D77DC
.definelabel $get_x_for_centred_text, 0x802D8844
