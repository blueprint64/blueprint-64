@icons:
.byte 0, 0x20, 0, 0x20, 0, 0x20, 0, 0x20, 0, 0x20, 0
.if $SHOW_100_COIN_STAR_HINT
	.byte 0x20
.else
	.byte 0
.endif
.byte 0, 0

$selected_star:
.byte 0

@up_arrow:
.byte 0x50, 0xFF

.align 4

$render_pause_star_hints:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)

	LHU T0, $g_course_num
	BEQ T0, R0, @@continue
	NOP

	SLTI AT, T0, 16
	BEQ AT, R0, @@continue
	NOP

	LHU A0, $g_save_file_num
	ADDIU A0, A0, 0xFFFF
	JAL $get_stars_collected
	ADDIU A1, T0, 0xFFFF

	LI T1, @icons
	MOVE T0, R0
	@@loop:
		SETU AT, 0x1
		SLLV AT, AT, T0
		AND AT, V0, AT
		BEQ AT, R0, @@endif_collected
		SETU AT, 0x2A ; X
			SETU AT, 0x2D ; star
		@@endif_collected:
		SB AT, 0x0 (T1)
		ADDIU T1, T1, 0x2
		ADDIU T0, T0, 0x1
		SLTI AT, T0, 7
		BNE AT, R0, @@loop
		NOP
		
	.if $SHOW_100_COIN_STAR_HINT
		SETU A0, 0x50
	.else
		SETU A0, 0x60
	.endif
	SETU A1, 0xB0
	LI A2, @icons
	JAL $printf
	MOVE A3, R0
	
	JAL @handle_star_hint_nav
	NOP
	
	; setup graphics for printing menu text
	LW T0, $g_display_list_head
	LI AT, 0xBA001402
	SW AT, 0x0 (T0)
	SW R0, 0x4 (T0)
	LUI AT, 0x0600
	SW AT, 0x8 (T0)
	LI AT, 0x02011CC8
	SW AT, 0xC (T0)
	LUI AT, 0xFB00
	SW AT, 0x10 (T0)
	ADDIU AT, R0, 0xFFFF
	SW AT, 0x14 (T0)
	ADDIU T0, T0, 0x18
	SW T0, $g_display_list_head
	
	LI.U A0, $g_act_name_table
	JAL $segmented_to_virtual
	LI.L A0, $g_act_name_table
	
	LBU T1, $selected_star
	SETU AT, 6
	BEQ AT, T1, @@else_if_coin_star
		LHU T0, $g_course_num
		ADDIU T0, T0, 0xFFFF
		SLL AT, T0, 1
		SLL T0, T0, 2
		ADDU T0, T0, AT
		ADDU T0, T0, T1
		SLL T0, T0, 2
		LI RA, @@endif_normal_star
		ADDU A0, V0, T0
		J $segmented_to_virtual
		LW A0, 0x0 (A0)
	@@else_if_coin_star:
		LI V0, $HINT_TEXT_100_COIN_STAR
	@@endif_normal_star:
	
	SW V0, 0x10 (SP)
	SETU A0, 160
	MOVE A1, V0
	JAL $get_x_for_centred_text
	LUI A2, 0x4100
	
	MOVE A0, V0
	SETU A1, 0x90
	JAL $print_string
	LW A2, 0x10 (SP)
	
	.if $SHOW_100_COIN_STAR_HINT
		SETU A0, 0x54
	.else
		SETU A0, 0x64
	.endif
	LBU T0, $selected_star
	SLL AT, T0, 3
	SLL T0, T0, 4
	ADDU T0, T0, AT
	ADDU A0, A0, T0
	LI A2, @up_arrow
	JAL $print_string
	SETU A1, 0xA0
	
	LI.U A0, $g_level_name_table
	JAL $segmented_to_virtual
	LI.L A0, $g_level_name_table
	LHU T0, $g_course_num
	ADDIU T0, T0, 0xFFFF
	SLL T0, T0, 2
	ADDU A0, V0, T0
	JAL $segmented_to_virtual
	LW A0, 0x0 (A0)
	ADDIU V0, V0, 0x3
	SW V0, 0x10 (SP)
	
	SETU A0, 160
	MOVE A1, V0
	JAL $get_x_for_centred_text
	LUI A2, 0x4100
	
	MOVE A0, V0
	SETU A1, 0xC0
	JAL $print_string
	LW A2, 0x10 (SP)
	
	LW T0, $g_display_list_head
	LI T1, 0x02011D50
	LUI AT, 0x0600
	SW AT, 0x0 (T0)
	SW T1, 0x4 (T0)
	ADDIU T0, T0, 0x8
	SW T0, $g_display_list_head
	
	LW RA, 0x14 (SP)
	JR RA ; intentionally doesn't use #continue here
	ADDIU SP, SP, 0x18
	
	@@continue:
	LW RA, 0x14 (SP)
	#continue render_pause_star_hints
	ADDIU SP, SP, 0x18
	
@handle_star_hint_nav:
	.if $SHOW_100_COIN_STAR_HINT
		@@num_stars equ 7
	.else
		@@num_stars equ 6
	.endif
	
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)

	LI T0, $g_mario
	LW T0, $m_controller (T0)
	LHU T0, $c_buttons_pressed (T0)
	SW T0, 0x10 (SP)
	
	ANDI AT, T0, $C_BUTTON_D_PAD_RIGHT
	BEQ AT, R0, @@endif_move_right
		LI A0, $SOUND_MENU_CHANGE
		LI.U A1, $g_zero_vector
		JAL $set_sound
		LI.L A1, $g_zero_vector
		
		LBU T0, $selected_star
		ADDIU T0, T0, 0x1
		SLTI AT, T0, @@num_stars
		BNE AT, R0, @@endif_wrap_right
		NOP
			MOVE T0, R0
		@@endif_wrap_right:
		SB T0, $selected_star
	@@endif_move_right:
		
	ANDI AT, T0, $C_BUTTON_D_PAD_LEFT
	BEQ AT, R0, @@endif_move_left
	NOP
		LI A0, $SOUND_MENU_CHANGE
		LI.U A1, $g_zero_vector
		JAL $set_sound
		LI.L A1, $g_zero_vector
		
		LBU T0, $selected_star
		ADDIU T0, T0, 0xFFFF
		SLT AT, T0, R0
		BEQ AT, R0, @@endif_wrap_left
		NOP
			SETU T0, (@@num_stars - 1)
		@@endif_wrap_left:
		SB T0, $selected_star
	@@endif_move_left:
	
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18
