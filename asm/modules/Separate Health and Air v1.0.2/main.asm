$mario_air:
.halfword 0, $MAX_AIR

$on_level_load:
	LI T0, $MAX_AIR
	SW T0, $mario_air
	JR RA
	NOP

$mario_underwater:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)

	LI T0, $g_mario
	BEQ A0, R0, @@endif_freezing_water
		LH AT, $m_health (T0)
		ADDIU AT, AT, 0xFFFD
		SH AT, $m_health (T0)
	@@endif_freezing_water:

	LW T0, $mario_air
	BNE T0, R0, @@endif_drowned
		LI A0, $g_mario
		LI A1, $ACT_DROWNING
		LI RA, @@return
		J $set_mario_action
		MOVE A2, R0
	@@endif_drowned:

	ADDIU T0, T0, 0xFFFF
	SW T0, $mario_air
	
	SLTI AT, T0, $LOW_AIR_THRESHOLD
	BEQ AT, R0, @@endif_low_air
		LI A0, $SOUND_ALMOST_DROWNING
		LI.U A1, $g_zero_vector
		JAL $set_sound
		LI.L A1, $g_zero_vector
	@@endif_low_air:

	@@return:
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18
	
$on_mario_step:
	LI T0, $g_mario
	LW T0, $m_action (T0)
	ANDI T0, T0, $ACT_FLAG_SWIMMING
	BEQ T0, R0, @@refill_air
	NOP
	
	LI T0, $g_mario
	LH T1, $m_water_level (T0)
	MTC1 T1, F8
	LI.S F6, 140
	L.S F4, $m_y (T0)
	CVT.S.W F8, F8
	SUB.S F8, F8, F6
	C.LT.S F4, F8
	NOP
	BC1T @@return
	
	@@refill_air:
	LW T0, $mario_air
	ADDIU T0, T0, $AIR_REFILL_RATE
	MINIU T0, T0, $MAX_AIR
	SW T0, $mario_air
	
	@@return:
	JR RA
	NOP

$on_coin_get:
	LW T0, $o_coin_value (A2)
	SETU T1, $COIN_AIR
	MULTU T0, T1
	MFLO T0
	LW T1, $mario_air
	ADDU T0, T1, T0
	MINI T0, T0, $MAX_AIR
	SW T0, $mario_air
	#continue on_coin_get
	NOP
	
$interact_air_bubble:
	LW T0, $o_coin_value (A2)
	SETU T1, $COIN_AIR
	MULTU T0, T1
	MFLO T0
	LW T1, $mario_air
	ADDU T0, T1, T0
	MINI T0, T0, $MAX_AIR
	SW T0, $mario_air
	SETU AT, 0x8000
	SW AT, $o_interaction_status (A2)
	JR RA
	MOVE V0, R0
	
