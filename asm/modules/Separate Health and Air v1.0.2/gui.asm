@AIR_BAR_HEIGHT equ 200
@AIR_BAR_WIDTH equ 20

$render_air_meter:
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)

LW T0, $g_mario_obj_ptr
BEQ T0, R0, @@return

LW T1, $mario_air

SLTI AT, T1, $MAX_AIR
BEQ AT, R0, @@return

SLTI AT, T1, $LOW_AIR_THRESHOLD
BEQ AT, R0, @@endif_low_air
SETU T0, 0xD7FF
	SETU T0, 0xF801
@@endif_low_air:
SLL AT, T0, 16
OR T0, T0, AT
SW T0, @bar_colour

LHU A0, ($g_mario + $m_yaw)
JAL angle_to_unit_vector
NOP

LI.S F4, 75
NOP
MUL.S F6, F4, F2
MUL.S F4, F4, F0

LI T0, $g_mario
LW AT, $m_y (T0)
SW AT, 0x14 (SP)
L.S F8, $m_x (T0)
L.S F10, $m_z (T0)
ADD.S F8, F8, F4
ADD.S F10, F10, F6
S.S F8, 0x10 (SP)
S.S F10, 0x18 (SP)
JAL perspective_transform
ADDIU A0, SP, 0x10

LI.S F4, (100 + @AIR_BAR_WIDTH)
LI.S F6, (@AIR_BAR_HEIGHT - 60)
NOP
SUB.S F4, F0, F4
SUB.S F6, F2, F6
CVT.W.S F4, F4
CVT.W.S F6, F6
MFC1 T0, F4
MFC1 T1, F6
MAX A0, T0, R0
MAX A1, T1, R0

ANDI A0, A0, 0xFFFC
ANDI A1, A1, 0xFFFC
SW A0, 0x10 (SP)
SW A1, 0x14 (SP)

SETU A2, @AIR_BAR_WIDTH
JAL create_draw_rect_command
SETU A3, @AIR_BAR_HEIGHT
LI T0, @draw_border
SW V0, 0x0 (T0)
SW V1, 0x4 (T0)

LW A0, 0x10 (SP)
LW A1, 0x14 (SP)
ADDIU A0, A0, 4
ADDIU A1, A1, 4
SETU A2, (@AIR_BAR_WIDTH-8)
JAL create_draw_rect_command
SETU A3, (@AIR_BAR_HEIGHT-8)
LI T0, @draw_background
SW V0, 0x0 (T0)
SW V1, 0x4 (T0)

LW T1, $mario_air
BEQ T1, R0, @@else_if_drowned
	LW A0, 0x10 (SP)
	LW A1, 0x14 (SP)
	ADDIU A0, A0, 4
	ADDIU A1, A1, 4
	SETU A2, (@AIR_BAR_WIDTH-8)
	SETU T0, (@AIR_BAR_HEIGHT-8)
	MULTU T1, T0
	MFLO T0
	SETU T1, $MAX_AIR
	NOP
	DIVU T0, T1
	MFLO A3
	ANDI A3, A3, 0xFFFC
	ADDIU A1, A1, (@AIR_BAR_HEIGHT-8)
	JAL create_draw_rect_command
	SUBU A1, A1, A3
	LI T0, @draw_bar
	SW V0, 0x0 (T0)
	B @@endif_has_air
	SW V1, 0x4 (T0)
@@else_if_drowned:
	LI T0, @draw_bar
	SW R0, 0x0 (T0)
	SW R0, 0x4 (T0)
@@endif_has_air:

LI A0, (@air_meter_f3d - 0x80000000)
JAL exec_display_list
NOP

@@return:
LW RA, 0x1C (SP)
JR RA
ADDIU SP, SP, 0x20

.align 8
@air_meter_f3d:
$G_RDPPIPESYNC
$G_SET_CYCLE_TYPE $G_CYC_FILL
$G_CLEAR_RENDERMODE
$G_SETFILLCOLOR_16 0x0000
@draw_border: $G_NOOP
$G_RDPPIPESYNC
$G_SETFILLCOLOR_16 0x3263
@draw_background: $G_NOOP
$G_RDPPIPESYNC
.word 0xF7000000
@bar_colour: .word 0xD7FFD7FF
@draw_bar: $G_NOOP
$G_RDPPIPESYNC
$G_ENDDL
