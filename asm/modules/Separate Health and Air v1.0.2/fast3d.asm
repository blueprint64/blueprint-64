.macro $G_RDPPIPESYNC
	.word 0xE7000000
	.word 0x00000000
.endmacro

.macro $G_RDPFULLSYNC
	.word 0xE9000000
	.word 0x00000000
.endmacro

.macro $G_SET_CYCLE_TYPE, cycleType
	.word 0xBA001402
	.word ( cycleType << 20 )
.endmacro

.definelabel $G_CYC_FILL, 3

.macro $G_SETFILLCOLOR_16, colour
	.word 0xF7000000
	.word ((colour) << 16) | (colour)
.endmacro

.macro $G_ENDDL
	.word 0xB8000000
	.word 0x00000000
.endmacro

.macro $G_NOOP
	.word 0x00000000
	.word 0x00000000
.endmacro

.macro $G_CLEAR_RENDERMODE
	.word 0xB900031D
	.word 0x00000000
.endmacro
