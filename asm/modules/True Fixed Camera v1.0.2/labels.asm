.definelabel $copy_vector, 0x80378800
.definelabel $memcpy, 0x803273F0
.definelabel $default_mario_cam, 0x80284D38
.definelabel $atan2s, 0x8037A9A8

.definelabel $mario_cam_state, 0x8032DF60
.definelabel $camera_selection, 0x8033C684
.definelabel $yaw_speed, 0x8032DF34

$c_pos equ 0x10
$c_focus equ 0x4
$c_target_yaw equ 0x3A

$mc_pos equ 0x4

$MARIO_CAM equ 1
