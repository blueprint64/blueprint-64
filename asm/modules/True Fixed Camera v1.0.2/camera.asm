@camera_args:
	@replace_mario_cam:
	.word 0
	@fixed_position:
	.word 0, 0, 0
	@fixed_focus:
	.word 0, 0, 0

$pivot_cam_init:
	LI A0, @camera_args
	J $memcpy
	ORI A2, R0, 16

$static_cam_init:
	LI A0, @camera_args
	J $memcpy
	ORI A2, R0, 28

$pivot_cam:
	LW T0, @replace_mario_cam
	BNE T0, R0, @@exec_camera
	LHU T0, $camera_selection
	ANDI T0, T0, $MARIO_CAM
	BEQ T0, R0, @@exec_camera
	NOP
	J $default_mario_cam
	NOP
	
	@@exec_camera:
	ADDIU SP, SP, 0xFFE8
	SW A0, 0x10 (SP)
	SW RA, 0x14 (SP)
	
	LI A1, @fixed_position
	JAL $copy_vector
	ADDIU A0, A0, $c_pos

	LW A0, 0x10 (SP)
	ADDIU A0, A0, $c_focus
	LW A1, $mario_cam_state
	JAL $copy_vector
	ADDIU A1, A1, $mc_pos
	
	JAL @update_camera_yaw
	LW A0, 0x10 (SP)

	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

$static_cam:
	LW T0, @replace_mario_cam
	BNE T0, R0, @@exec_camera
	LHU T0, $camera_selection
	ANDI T0, T0, $MARIO_CAM
	BEQ T0, R0, @@exec_camera
	NOP
	J $default_mario_cam
	NOP
	
	@@exec_camera:
	ADDIU SP, SP, 0xFFE8
	SW A0, 0x10 (SP)
	SW RA, 0x14 (SP)
	
	ORI T0, R0, 0x1000
	SH T0, $yaw_speed

	LI A1, @fixed_position
	JAL $copy_vector
	ADDIU A0, A0, $c_pos

	LI A1, @fixed_focus
	LW A0, 0x10 (SP)
	JAL $copy_vector
	ADDIU A0, A0, $c_focus
	
	JAL @update_camera_yaw
	LW A0, 0x10 (SP)

	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

@update_camera_yaw:
	ADDIU SP, SP, 0xFFE8
	SW A0, 0x10 (SP)
	SW RA, 0x14 (SP)
	
	ORI T0, R0, 0x1000
	SH T0, $yaw_speed
	
	L.S F4, ($c_pos + 0x0) (A0)
	L.S F6, ($c_pos + 0x8) (A0)
	L.S F8, ($c_focus + 0x0) (A0)
	L.S F10, ($c_focus + 0x8) (A0)
	SUB.S F14, F4, F8
	JAL $atan2s
	SUB.S F12, F6, F10
	
	LW A0, 0x10 (SP)
	SH V0, $c_target_yaw (A0)
	
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18
