.definelabel @g_frameBufferIndex, 0x8032d5d8

@stored_args:
.word 0
.word 0
.word 0

$swap_one:
	LW T0, $is_console_or_ares
	BNE T0, R0, @@skip
		LI T0, @stored_args
		SW A0, 0x0 (T0)
		SW A1, 0x4 (T0)
		JR RA
		SW A2, 0x8 (T0)
	@@skip:
	#continue swap_one
	NOP

$swap_two:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)
	
	#await swap_two
	NOP
	
	LW T0, $is_console_or_ares
	BNE T0, R0, @@skip
		LI T1, @stored_args
		LW A0, 0x0 (T1)
		LW A1, 0x4 (T1)
		#await swap_one
		LW A2, 0x8 (T1)
	@@skip:
	
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18

$post_vsync:
	LW T0, $is_console_or_ares
	BNE T0, R0, @@skip
		LI T1, @g_frameBufferIndex
		LHU AT, 0x0 (T1)
		ANDI AT, AT, 0x1
		SH AT, 0x0 (T1)
		LHU AT, 0x4 (T1)
		ANDI AT, AT, 0x1
		SH AT, 0x4 (T1)
	@@skip:
	#continue post_vsync
	NOP
