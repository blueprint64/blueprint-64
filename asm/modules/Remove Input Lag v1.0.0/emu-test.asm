.definelabel @osDisableInt, 0x803274d0
.definelabel @osRestoreInt, 0x803274f0
.definelabel @osInvalICache, 0x80324610

$is_console_or_ares:
.word 0

$init:
	ADDIU SP, SP, 0xFFE0
	SW RA, 0x1C (SP)

	ORI T0, R0, 1
	JAL @osDisableInt
	SW T0, 0x18 (SP)

	ADDIU T0, SP, 0x18
	LUI AT, 0xA000
	OR T0, AT, SP

	SW V0, 0x14 (SP)
	SW R0, 0x18 (T0)
	LW V0, 0x18 (SP)
	
	SW V0, $is_console_or_ares
	
	BNE V0, R0, @@endif_patch_libultra
		LI A0, 0x80323174
		SW R0, 0x0 (A0)
		JAL @osInvalICache
		ORI A1, R0, 0x4
		
		LI T0, 0x080CA020 ;; J 0x80328080 (sViSwapContext)
		LI A0, 0x80323A48
		SW T0, 0x0 (A0)
		JAL @osInvalICache
		ORI A1, R0, 0x4
	@@endif_patch_libultra:

	LW RA, 0x1C (SP)
	LW A0, 0x14 (SP)
	J @osRestoreInt
	ADDIU SP, SP, 0x20
