ALLOW_WALLRUNNING_CHECK:
.word 0 ; user can store a function pointer here to add an additional condition

$wallrun_check:
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)

LW T0, ALLOW_WALLRUNNING_CHECK
BEQ T0, R0, @@check_jump
MOVE V0, R0
JALR T0
NOP
BEQ V0, R0, @@return

@@check_jump:
LI T0, $g_mario
LW T1, $m_action (T0)
LI AT, 0x03000880 ; Single Jump
BEQ T1, AT, @@check_speed
LI AT, 0x03000881 ; Double Jump
BEQ T1, AT, @@check_speed
LI AT, 0x03000886 ; Walljump
BEQ T1, AT, @@check_speed
LI AT, 0x03000888 ; Longjump
BEQ T1, AT, @@check_speed
.if $TRIPLE_JUMP_WALLRUN
	LI AT, 0x01000882 ; Triple Jump
	BEQ T1, AT, @@check_speed
.endif
.if $SIDEFLIP_WALLRUN
	LI AT, 0x01000887 ; Slide Flip
	BEQ T1, AT, @@check_speed
.endif
.if $CUSTOM_JUMP_WALLRUN
	LI AT, 0x40000800 ; Air | UnusedFlag
	AND AT, T1, AT
	BNE AT, R0, @@check_speed
.endif

NOP
B @@return

@@check_speed:
LI.S F8, $MIN_WALLRUN_H_SPEED
L.S F4, $m_speed_h (T0)
C.LT.S F4, F8
NOP
BC1T @@return
LI.S F8, $MAX_WALLRUN_V_SPEED
L.S F4, $m_speed_y (T0)
ABS.S F4, F4
C.LE.S F4, F8
NOP
BC1F @@return
NOP

JAL $try_get_wall_angles
NOP

BEQ V0, R0, @@return
NOP

SLTI AT, V1, $MIN_WALLRUN_ANGLE
BNE AT, R0, @@return

SLTI AT, V1, $MAX_WALLRUN_ANGLE
BEQ AT, R0, @@return

; Do not wallrun if the wall is ice or lava
LI T0, $g_mario
LW T0, $m_wall_ptr (T0)
JAL $collision_type_supports_wallrunning
LHU A0, $t_collision_type (T0)
BEQ V0, R0, @@return

LI T0, $g_mario
L.S F6, $m_speed_h (T0)
MUL.S F4, F0, F6
MUL.S F8, F2, F6
S.S F4, $m_speed_x (T0)
S.S F8, $m_speed_z (T0)
SW R0, $m_speed_y (T0)
MOV.S F12, F2
JAL $atan2s
MOV.S F14, F0
LI T0, $g_mario
SH V0, $m_angle_yaw (T0)
SH V0, 0x24 (T0)
MOVE A0, T0
LI A1, ACT_WALLRUN
JAL $set_mario_action
MOVE A2, R0

@@return:
LW RA, 0x1C (SP)
JR RA
ADDIU SP, SP, 0x20
