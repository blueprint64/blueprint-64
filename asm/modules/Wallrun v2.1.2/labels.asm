/* Functions */
.definelabel $atan2s, 0x8037A9A8
.definelabel $abs_angle_diff, 0x802A11A8
.definelabel $set_mario_action, 0x80252CF4
.definelabel $turn_angle, 0x8029E530
.definelabel $set_sound, 0x8031EB00

/* Global Variables */
$g_mario equ 0x8033B170
$g_mario_obj_ptr equ 0x80361158
$g_current_obj_ptr equ 0x80361160

/* Mario Properties */
$m_action equ 0xC
$m_subaction equ 0x18
$m_action_timer equ 0x1A
$m_x equ 0x3C
$m_y equ 0x40
$m_z equ 0x44
$m_speed_h equ 0x54
$m_speed_x equ 0x48
$m_speed_y equ 0x4C
$m_speed_z equ 0x50
$m_angle_yaw equ 0x2E
$m_angle_roll equ 0x30
$m_controller equ 0x9C
$m_wall_ptr equ 0x60
$m_wall_kick_timer equ 0x2A

/* Object Properties */
$o_face_angle_yaw equ 0xD4
$o_face_angle_roll equ 0xD8
$o_move_angle_yaw equ 0xC8
$o_gfx_x equ 0x20
$o_gfx_z equ 0x28
$o_gfx_angle_yaw equ 0x1C
$o_gfx_angle_roll equ 0x1E

/* Surface Triangle Properties */
$t_collision_type equ 0x0
$t_normal_x equ 0x1C
$t_normal_z equ 0x24

/* Controller Properties */
$c_buttons_pressed equ 0x12

$C_TRIGGER_Z equ 0x2000
$C_BUTTON_A equ 0x8000

/* Vanilla Actions */
$ACT_WALLJUMP equ 0x03000886
$ACT_WALKING equ 0x04000440
$ACT_FREEFALL equ 0x0100088C
