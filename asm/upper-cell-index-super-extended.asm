.create "upper-cell-index-super-extended.bin",0
.n64

; Replacement for the upper_cell_index function when using super extended boundaries
.area 0x9C

SLL A0, A0, 0x10
SRA A0, A0, 0x10
ORI AT, R0, 0x8000
ADDU A0, A0, AT
SRA V0, A0, 0xA
SLTI AT, V0, 0x3F
BEQ AT, R0, @@return
ANDI A0, A0, 0x3FF
SLTI AT, A0, 0x3CF
BNE AT, R0, @@return
NOP
ADDIU V0, V0, 0x1
@@return:
JR RA
NOP

.endarea
.close
