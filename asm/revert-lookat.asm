.create "revert-lookat.bin",0
.n64

.definelabel append_display_list, 0x8027B904

; reset_lookat
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

ORI AT, R0, 1
BNE A0, AT, @@return
LI A0, 0x802002C8 ; RAM address of Extensions::defaultLookAtDl
JAL append_display_list
LW A1, 0x18 (A1)

@@return:
OR V0, R0, R0
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

.close
