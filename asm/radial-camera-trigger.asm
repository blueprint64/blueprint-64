.create "radial-camera-trigger.bin",0
.n64

transition_to_camera_mode equ 0x80286088
g_camera_status_flags equ 0x8033C84A
g_sc_base_yaw equ 0x8033C776
g_sc_offset_yaw equ 0x8033C778
c_mode equ 0x0
c_poi_x equ 0x28
c_poi_z equ 0x2C

; a0 = camera pointer
; a1 = camera mode (1 or 2)
; a2 = transition frames
; f12 = centre x
; f14 = centre z
LUI T0, hi( g_camera_status_flags )
LHU AT, lo( g_camera_status_flags ) (T0)
ORI AT, AT, 0x1000
SH AT, lo( g_camera_status_flags ) (T0)

LBU AT, c_mode (A0)
BEQ AT, A1, @@return
	LUI T0, hi( g_camera_status_flags )
	LHU AT, lo( g_camera_status_flags ) (T0)
	BEQ A2, R0, @@commit_flags
	ANDI AT, AT, 0xFFFE
		ORI AT, AT, 0x1
	@@commit_flags:
	SH AT, lo( g_camera_status_flags ) (T0)
	S.S F12, c_poi_x (A0)
	J transition_to_camera_mode
	S.S F14, c_poi_z (A0)
@@return:
JR RA
NOP

.close
