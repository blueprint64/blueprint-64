.create "load-area-background.bin",0x801FF200
.n64

imported_levels_table equ 0x802005D8

; Set return address
LI RA, 0x80276490

; Check if this is a custom level
LH T0, 0x8032DDF8
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU AT, 0x0 (T0)
BNE AT, R0, @@endif_vanilla_level
NOP
	JR RA
	NOP
@@endif_vanilla_level:

; Load virtual address of segment 0xA into A0
LW A0, 0x8033B428
LUI AT, 0x8000
OR A0, A0, AT

; Load ROM address to read background from
LHU T0, 0x1E (T8)
SLTI AT, T0, 0xA
BNE AT, R0, @@else_vanilla
	LUI AT, 0x2
	ORI AT, AT, 0x140
	MULTU AT, T0
	LUI AT, 0x385
	MFLO A1
	ORI AT, AT, 0xB380
	B @@endif_custom_background
	ADDU A1, A1, AT
@@else_vanilla:
	LI A1, @background_table
	SLL AT, T0, 0x2
	ADDU A1, A1, AT
	LW A1, 0x0 (A1)
@@endif_custom_background:

; DMA Read data into background segment
LUI AT, 0x2
ORI AT, AT, 0x0140
J 0x80278504 ; dma_read
ADDU A2, A1, AT

@background_table:
.word 0xB35714
.word 0xBA22D4
.word 0xBC2C14
.word 0xBEAD54
.word 0xB5D854
.word 0xC12E94
.word 0xC3AFD4
.word 0xC57914
.word 0xB85994
.word 0xC7FA54

.close
