.create "lower-cell-index-super-extended.bin",0
.n64

; Replacement for the lower_cell_index function when using super extended boundaries
.area 0x94

SLL A0, A0, 0x10
SRA A0, A0, 0x10
ORI AT, R0, 0x8000
ADDU A0, A0, AT
SRA V0, A0, 0xA
BEQ V0, R0, @@return
ANDI A0, A0, 0x3FF
SLTI AT, A0, 0x32
BEQ AT, R0, @@return
NOP
ADDIU V0, V0, 0xFFFF
@@return:
JR RA
NOP

.endarea
.close
