.create "reflection-mapping.bin",0
.n64

.definelabel alloc_display_list, 0x80278F2C
.definelabel append_display_list, 0x8027B904

.definelabel g_object, 0x8032DF00
.definelabel g_camera, 0x8033CBD0
.definelabel g_camera_roll, 0x8033C712

c_focus equ 0x4
c_x equ 0x10
c_y equ 0x14
c_z equ 0x18
o_gfx_pos equ 0x20

GEO_CONTEXT_RENDER equ 1

; entry function
LW A1, 0x18 (A1)
ORI AT, R0, GEO_CONTEXT_RENDER
BNE A0, AT, @@return
LW A0, g_object
BNE A0, R0, @set_lookat
ADDIU A0, A0, o_gfx_pos
LW A0, g_camera
B @set_lookat
ADDIU A0, A0, c_focus
@@return:
JR RA
OR V0, R0, R0

/*
let e = camera position
let a = object/focus position
let r = camera roll

let L = normalize( e - a )
x = normalize( ( L[2] * cos(r), sin(r), -L[0] * cos(r) ) )
y = L cross x
*/

; A0 = lookat point
; A1 = drawing layer
@set_lookat:
ADDIU SP, SP, 0xFFD0
SW RA, 0x2C (SP)
SW A0, 0x30 (SP)
SW A1, 0x34 (SP)

JAL alloc_display_list ; LookAt data
ORI A0, R0, 32
SW V0, 0x28 (SP)

SW R0, 0x0 (V0)
SW R0, 0x4 (V0)
LUI AT, 0x0080
SW AT, 0x10 (V0)
SW AT, 0x14 (V0)

LW T0, g_camera
L.S F4, c_x (T0)
L.S F6, c_y (T0)
L.S F8, c_z (T0)

LW A0, 0x30 (SP)
L.S F10, 0x0 (A0)
L.S F12, 0x4 (A0)
L.S F14, 0x8 (A0)

SUB.S F4, F4, F10
SUB.S F6, F6, F12
SUB.S F8, F8, F14

S.S F4, 0x10 (SP)
S.S F6, 0x14 (SP)
S.S F8, 0x18 (SP)

BGEZAL R0, @normalize_in_place
ADDIU A0, SP, 0x10

LH.U A0, g_camera_roll
BGEZAL R0, @angle_to_unit_vector
LH.L A0, g_camera_roll

MTC1 R0, F12
L.S F4, 0x10 (SP)
L.S F6, 0x18 (SP)
MUL.S F8, F4, F4
MUL.S F10, F6, F6
ADD.S F8, F8, F10
C.EQ.S F8, F12
SQRT.S F8, F8
BC1T @@elseif_zero
NOP
	DIV.S F4, F4, F8
	B @@endif_nonzero
	DIV.S F6, F6, F8
@@elseif_zero:
	MTC1 R0, F4
	LUI AT, 0x3F80
	MTC1 AT, F6
@@endif_nonzero:

MUL.S F4, F4, F2
MUL.S F6, F6, F2
NEG.S F4, F4

S.S F6, 0x1C (SP)
S.S F0, 0x20 (SP)
S.S F4, 0x24 (SP)

BGEZAL R0, @normalize_in_place
ADDIU A0, SP, 0x1C

ADDIU A0, SP, 0x10
ADDIU A1, SP, 0x1C
BGEZAL R0, @normalized_cross_product
OR A2, A0, R0

BGEZAL R0, @to_byte_vector
ADDIU A0, SP, 0x1C
LW AT, 0x28 (SP)
SW V0, 0x8 (AT)
SW R0, 0xC (AT)

BGEZAL R0, @to_byte_vector
ADDIU A0, SP, 0x10
LW AT, 0x28 (SP)
SW V0, 0x18 (AT)
SW R0, 0x1C (AT)

JAL alloc_display_list
ORI A0, R0, 24

LUI T2, 0x00FF
ORI T2, T2, 0xFFFF

LI T0, 0x03840010
SW T0, 0x0 (V0)

LW T1, 0x28 (SP)
AND T1, T1, T2
SW T1, 0x4 (V0)

LI T0, 0x03820010
SW T0, 0x8 (V0)

ADDIU T1, T1, 16
SW T1, 0xC (V0)

LUI AT, 0xB800
SW AT, 0x10 (V0)
SW R0, 0x14 (V0)

OR A0, V0, R0
JAL append_display_list
LW A1, 0x34 (SP)

OR V0, R0, R0
LW RA, 0x2C (SP)
JR RA
ADDIU SP, SP, 0x30

; A0 = vector
@normalize_in_place:
L.S F4, 0x0 (A0)
L.S F6, 0x4 (A0)
L.S F8, 0x8 (A0)
MUL.S F12, F4, F4
MUL.S F14, F6, F6
MUL.S F16, F8, F8
MTC1 R0, F10
ADD.S F0, F12, F14
ADD.S F0, F16, F0
C.EQ.S F0, F10
SQRT.S F0, F0
BC1T @@return
OR V0, R0, R0
DIV.S F4, F4, F0
DIV.S F6, F6, F0
DIV.S F8, F8, F0
S.S F4, 0x0 (A0)
S.S F6, 0x4 (A0)
S.S F8, 0x8 (A0)
ORI V0, R0, 1
@@return:
JR RA
NOP

; A0 = vector A
; A1 = vector B
; A2 = result vector
@normalized_cross_product:
L.S F4, 0x4 (A0)
L.S F6, 0x8 (A1)
MUL.S F4, F4, F6
L.S F6, 0x8 (A0)
L.S F8, 0x4 (A1)
MUL.S F6, F6, F8
SUB.S F10, F4, F6

L.S F4, 0x8 (A0)
L.S F6, 0x0 (A1)
MUL.S F4, F4, F6
L.S F6, 0x0 (A0)
L.S F8, 0x8 (A1)
MUL.S F6, F6, F8
SUB.S F12, F4, F6

L.S F4, 0x0 (A0)
L.S F6, 0x4 (A1)
MUL.S F4, F4, F6
L.S F6, 0x4 (A0)
L.S F8, 0x0 (A1)
MUL.S F6, F6, F8
SUB.S F14, F4, F6

S.S F10, 0x0 (A2)
S.S F12, 0x4 (A2)
S.S F14, 0x8 (A2)

B @normalize_in_place
OR A0, A2, R0

@angle_to_unit_vector:
ANDI A0, A0, 0xFFF0
SRL A0, A0, 0x2
LUI AT, 0x8038
ADDU A0, AT, A0
L.S F0, 0x6000 (A0)
JR RA
L.S F2, 0x7000 (A0)

; A0 = float vector
@to_byte_vector:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

BGEZAL R0, @float_to_byte
L.S F12, 0x0 (A0)
SLL T0, V0, 24
BGEZAL R0, @float_to_byte
L.S F12, 0x4 (A0)
SLL AT, V0, 16
OR T0, T0, AT
BGEZAL R0, @float_to_byte
L.S F12, 0x8 (A0)
SLL AT, V0, 8
OR V0, T0, AT

LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

; F12 = float
@float_to_byte:
LUI AT, 0x4300
MTC1 AT, F4
NOP
MUL.S F4, F12, F4
CVT.W.S F4, F4
MFC1 V0, F4
NOP
SLTI AT, V0, 128
BNE AT, R0, @@return
ANDI V0, V0, 0xFF
	ORI V0, R0, 0x7F
@@return:
JR RA
NOP

.close
