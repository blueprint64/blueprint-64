.create "default-camera-trigger.bin",0
.n64

transition_to_camera_mode equ 0x80286088
cam_mode equ 0x0
cam_preset equ 0x1

LBU A1, cam_preset (A0)
LBU AT, cam_mode (A0)
BEQ A1, AT, @@return
NOP
	J transition_to_camera_mode
	ORI A2, R0, 15
@@return:
JR RA
NOP

.close
