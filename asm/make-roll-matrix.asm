.create "make-roll-matrix.bin", 0
.n64

.definelabel mtxf_identity, 0x80378EB4
.definelabel guMtxF2L, 0x80329450

ADDIU SP, SP, 0xFFA8
SW RA, 0x54 (SP)
SW A0, 0x58 (SP)
SW A1, 0x5C (SP)

JAL mtxf_identity
ADDIU A0, SP, 0x10

LW A1, 0x5C (SP)
ANDI A1, A1, 0xFFF0
SRL A1, A1, 0x2
LUI AT, 0x8038
ADDU A1, AT, A1

LW AT, 0x7000 (A1)
SW AT, 0x10 (SP)
SW AT, 0x24 (SP)

L.S F4, 0x6000 (A1)
S.S F4, 0x14 (SP)
NEG.S F4, F4
S.S F4, 0x20 (SP)

ADDIU A0, SP, 0x10
LW A1, 0x58 (SP)

LW RA, 0x54 (SP)
J guMtxF2L
ADDIU SP, SP, 0x58

.close
