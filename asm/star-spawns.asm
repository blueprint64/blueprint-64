.create "star-spawns.bin",0
.n64

vanilla_star_spawn_table equ 0x802004AC
imported_levels_table equ 0x802005D8

segmented_to_virtual equ 0x80277F50
create_star equ 0x802F2B88

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)
SW A0, 0x10 (SP)

LH T0, 0x8032DDF8
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU AT, 0x0 (T0)
BNE AT, R0, @@else_if_custom_level
	LI.U V0, vanilla_star_spawn_table
	B @@endif_vanilla_level
	LI.L V0, vanilla_star_spawn_table
@@else_if_custom_level:
	JAL segmented_to_virtual
	LUI A0, 0x0E00
@@endif_vanilla_level:

LW T0, 0x10 (SP)
SLL AT, T0, 0x3
ADDU V0, V0, AT
SLL AT, T0, 0x2
ADDU V0, V0, AT

L.S F12, 0x0 (V0)
L.S F14, 0x4 (V0)
LW A2, 0x8 (V0)

LW RA, 0x14 (SP)
J create_star
ADDIU SP, SP, 0x18

.close
