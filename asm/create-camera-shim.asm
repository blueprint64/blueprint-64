.create "create-camera-shim.bin", 0
.n64

.definelabel create_camera, 0x80287BE0
.definelabel bbp_pause_settings, 0x8032DF90

gc_pos_x equ 0x1C
gc_pos_y equ 0x20
gc_pos_z equ 0x24
gc_focus_x equ 0x28
gc_focus_y equ 0x2C
gc_focus_z equ 0x30

LI T0, bbp_pause_settings

LW AT, gc_pos_x (A0)
SW AT, 0x0 (T0)
LW AT, gc_pos_y (A0)
SW AT, 0x4 (T0)
LW AT, gc_pos_z (A0)
SW AT, 0x8 (T0)

LW AT, gc_focus_x (A0)
SW AT, 0xC (T0)
LW AT, gc_focus_y (A0)
SW AT, 0x10 (T0)
LW AT, gc_focus_z (A0)
J create_camera
SW AT, 0x14 (T0)

.close
