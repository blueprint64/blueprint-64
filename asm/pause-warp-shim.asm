.create "pause-warp-shim.bin", 0
.n64

.definelabel imported_levels_table, 0x802005D8
.definelabel g_level_num, 0x8032DDF8

.definelabel initiate_warp, 0x8024A700
.definelabel segmented_to_virtual, 0x80277F50

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LH T0, g_level_num
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU T0, 0x0 (T0)
BEQ T0, R0, @@continue
	LUI A0, 0x0E00
	JAL segmented_to_virtual
	ORI A0, A0, 0x0184
	
	LHU A0, 0x0 (V0)
	LBU A1, 0x2 (V0)
	LBU A2, 0x3 (V0)
	OR A3, R0, R0
	
@@continue:
LW RA, 0x14 (SP)
J initiate_warp
ADDIU SP, SP, 0x18

.close
