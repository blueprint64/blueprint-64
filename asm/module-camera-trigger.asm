.create "module-camera-trigger.bin",0
.n64

transition_to_camera_mode equ 0x80286088
g_camera_status_flags equ 0x8033C84A
c_mode equ 0x0

; a0 = camera pointer
; a1 = camera mode
; a2 = transition frames (0 for instant)
; SP+0x10 ... = parameters
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LUI T0, hi( g_camera_status_flags )
LHU AT, lo( g_camera_status_flags ) (T0)
ORI AT, AT, 0x1000
SH AT, lo( g_camera_status_flags ) (T0)

LBU AT, c_mode (A0)
BEQ A1, AT, @@return

SW A0, 0x18 (SP)
SW A1, 0x1C (SP)
SW A2, 0x20 (SP)

.word 0xDEADBEEF ; placeholder (LUI T0, hi(bbp_custom_camera_init_table - 0x48))
.word 0xDEADBEEF ; placeholder (ORI T0, T0, lo(bbp_custom_camera_init_table - 0x48))
SLL AT, A1, 2
ADDU T0, T0, AT
LW T0, 0x0 (T0)
JALR T0
ADDIU A1, SP, 0x28

LW A2, 0x20 (SP)
LUI T0, hi( g_camera_status_flags )
LHU AT, lo( g_camera_status_flags ) (T0)
BEQ A2, R0, @@commit_flags
ANDI AT, AT, 0xFFFE
	ORI AT, AT, 0x1
@@commit_flags:
SH AT, lo( g_camera_status_flags ) (T0)

LW A0, 0x18 (SP)
JAL transition_to_camera_mode
LW A1, 0x1C (SP)

@@return:
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

.close
