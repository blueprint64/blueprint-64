.create "fixed-camera-trigger.bin",0
.n64

g_camera_status_flags equ 0x8033C84A
set_camera_mode_fixed equ 0x8028D44C
set_camera_mode equ 0x80286188

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LUI T0, hi( g_camera_status_flags )
LHU AT, lo( g_camera_status_flags ) (T0)
ORI AT, AT, 0x1000
SH AT, lo( g_camera_status_flags ) (T0)

;JAL set_camera_mode_fixed
SB R0, 0x0 (A0)
SW A0, 0x10 (SP)

LI T0, 0x8033c698
LI T1, 0x8032df6c

LW AT, 0x0 (T1)
;SW AT, 0xC (T0)
SW AT, 0x24 (T0)
LW AT, 0x4 (T1)
;SW AT, 0x10 (T0)
SW AT, 0x28 (T0)
LW AT, 0x8 (T1)
;SW AT, 0x14 (T0)
SW AT, 0x2C (T0)

LI A1, (0x8033B170 + 0x3C)
JAL 0x80378800
OR A0, T0, R0

LI T0, 0x8033c698

ADDIU A0, T0, 0xC
ADDIU A1, T0, 0x24
LUI A2, 0x3d4d ;0x3F00
OR A3, A2, R0
JAL 0x80289610
SW A2, 0x10 (SP)

LI A0, (0x8033cbd0 + 0x04)
LI A1, 0x8033c698
JAL 0x80378800
NOP

LI A0, (0x8033cbd0 + 0x10)
LI A1, (0x8033c698 + 0xC)
JAL 0x80378800
NOP

LI A0, (0x8033c698 + 0x8C)
LI A1, (0x8033c698 + 0x10)
JAL 0x80378800
NOP


LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

.close
