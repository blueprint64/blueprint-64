.create "use-object-alpha.bin",0
.n64

alloc_dl equ 0x80278F2C
append_dl equ 0x8027B904
g_current_node_object equ 0x8032DF00
o_opacity equ 0x17F

ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LW A1, 0x18 (A1)
SW A1, 0x10 (SP)

ORI AT, R0, 1
BNE A0, AT, @@return
NOP
	JAL alloc_dl
	ORI A0, R0, 16
	
	LUI AT, 0xFA00
	SW AT, 0x0 (V0)
	
	LW T0, g_current_node_object
	LBU AT, o_opacity (T0)
	ADDIU AT, AT, 0xFF00
	SW AT, 0x4 (V0)
	
	LUI AT, 0xB800
	SW AT, 0x8 (V0)
	SW R0, 0xC (V0)
	
	OR A0, V0, R0
	JAL append_dl
	LW A1, 0x10 (SP)
	
@@return:
OR V0, R0, R0
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

.close
