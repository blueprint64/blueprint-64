.create "to-fixed-matrix.bin", 0
.n64

.definelabel imported_levels_table, 0x802005D8
.definelabel g_level_num, 0x8032DDF8
.definelabel guMtxF2L, 0x80329450

.area 124

; Bowser's Blueprints writes instructions here to set F12

ADDIU SP, SP, 0xFFA8
SW RA, 0x54 (SP)

LH T0, g_level_num
LI T1, imported_levels_table
ADDU T0, T1, T0
LBU T0, 0x0 (T0)
BNE T0, R0, @@endif_vanilla_level
	LUI AT, 0x3F80
	MTC1 AT, F12 ; Always use a scale of 1 for vanilla levels (no change)
@@endif_vanilla_level:

ADDIU T0, SP, 0x10
ADDIU T1, A1, 0x40

/*
Scales the matrix using the division in F12, but only the main 3x3 rows.
The bottom [0 0 0 1] row remains the same.

Does some pointer arithmatic to get which entries need to be scaled, and which
should just be copied in order to save some instructions so the code will fit.
*/

@@loop:
	SUBU AT, T1, A1
	ADDIU AT, AT, 0xFFFC
	ANDI AT, AT, 0xF
	BEQ AT, R0, @@endif_scale
	L.S F4, 0x0 (A1)
		DIV.S F4, F4, F12
	@@endif_scale:
	S.S F4, 0x0 (T0)
	
	ADDIU A1, A1, 0x4
	BNE A1, T1, @@loop
	ADDIU T0, T0, 0x4

OR A1, A0, R0
JAL guMtxF2L
ADDIU A0, SP, 0x10

LW RA, 0x54 (SP)
JR RA
ADDIU SP, SP, 0x58

.endarea
.close
