#include "src/core/backup.hpp"

#include <iostream>
#include "src/polyfill/appdata.hpp"

FileBackup::FileBackup( const fs::path &filePath ) :
	m_sourcePath( filePath ),
	m_backupPath( fs::to_path( createTemporaryDirectory() ) / "backup-file" ),
	m_discard( false )
{
	if( fs::exists( filePath ) ) {
		std::error_code copyError;
		fs::copy( filePath, m_backupPath, copyError );
		if( copyError ) {
			m_discard = true;
		}
	} else {
		m_discard = true;
	}
}

FileBackup::~FileBackup() {
	if( !m_discard ) {
		if( !fs::tryCopyFileOverwrite( m_backupPath, m_sourcePath ) ) {
			std::cerr << "Failed to restore " << m_sourcePath << " from backup." << std::endl;
		} else {
			std::cout << "File " << m_sourcePath << " restored from backup." << std::endl << std::flush;
		}
	}

	if( !fs::tryDeleteRecursive( m_backupPath.parent_path() ) ) {
		std::cerr << "Failed to remove temporary directory " << m_backupPath.parent_path() << std::endl << std::flush;
	}
}
