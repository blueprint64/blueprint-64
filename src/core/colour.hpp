#ifndef CORE_COLOUR_HPP_
#define CORE_COLOUR_HPP_

#include <cassert>
#include <functional>
#include "src/polyfill/byte-order.hpp"
#include "src/core/json.hpp"
#include "src/types.hpp"

struct ColourRGBA32;
class ColourRGBA5551;

struct ColourRGB24 {
	ubyte red, green, blue;

	ColourRGB24() : red( 0 ), green( 0 ), blue( 0 ) {}

	ColourRGB24( ubyte r, ubyte g, ubyte b ) :
		red( r ), green( g ), blue( b ) {}

	inline void writeToRom( std::ostream &rom, bool addAlpha = false ) const {
		const ubyte data[] = { red, green, blue, 0xFF };
		rom.write( (char*)data, addAlpha ? 4 : 3 );
	}

	inline static ColourRGB24 readFromRom( std::istream &rom, bool skipAlpha = false ) {
		char data[4];
		rom.read( data, skipAlpha ? 4 : 3 );
		return ColourRGB24( data[0], data[1], data[2] );
	}

	bool operator==( const ColourRGB24 &other ) const {
		return red == other.red && green == other.green && blue == other.blue;
	}

	bool operator!=( const ColourRGB24 &other ) const {
		return red != other.red || green != other.green || blue != other.blue;
	}

	string toString() const;
	bool tryParse( const string &str );
	static ColourRGB24 parse( const string &str );

	explicit operator ColourRGBA32() const;
	explicit operator ColourRGBA5551() const;

};

struct ColourRGBA32 {
	ubyte red;
	ubyte green;
	ubyte blue;
	ubyte alpha;

	ColourRGBA32( ubyte r, ubyte g, ubyte b, ubyte a ) :
		red( r ), green( g ), blue( b ), alpha( a ) {}

	ColourRGBA32( uint colour ) :
		red( colour >> 24 ),
		green( ( colour >> 16 ) & 0xFF ),
		blue( ( colour >> 8 ) & 0xFF ),
		alpha( colour & 0xFF ) {}

	void writeToRom( std::ostream &rom ) const {
		const ubyte data[] = { red, green, blue, alpha };
		rom.write( (char*)data, sizeof(data) );
	}

	static ColourRGBA32 readFromRom( std::istream &rom ) {
		ubyte data[4];
		rom.read( (char*)data, sizeof(data) );
		return ColourRGBA32( data[0], data[1], data[2], data[3] );
	}

	inline uint toInt() const {
		return ( (uint)red << 24 ) + ( (uint)green << 16) + ( (uint)blue << 8 ) + (uint)alpha;
	}

	bool operator==( const ColourRGBA32 &other ) const {
		return toInt() == other.toInt();
	}

	bool operator!=( const ColourRGBA32 &other ) const {
		return toInt() != other.toInt();
	}

	string toString() const;
	bool tryParse( const string &str );
	static ColourRGBA32 parse( const string &str );

	explicit operator ColourRGB24() const;
	explicit operator ColourRGBA5551() const;

};

class ColourRGBA5551 final {

	private:
	ushort m_data;

	public:
	ColourRGBA5551( ubyte red, ubyte green, ubyte blue, bool alpha = true ) {
		assert( red <= 0x1F && green <= 0x1F && blue <= 0x1F );
		m_data = (
			( red << 11 ) |
			( green << 6 ) |
			( blue << 1 ) |
			( alpha ? 1 : 0 )
		);
	}

	ColourRGBA5551( ushort data ) : m_data( data ) {}
	ColourRGBA5551() : ColourRGBA5551( 0 ) {}

	inline ubyte getRed() const {
		return m_data >> 11;
	}

	inline ubyte getGreen() const {
		return ( m_data >> 6 ) & 0x1F;
	}

	inline ubyte getBlue() const {
		return ( m_data >> 1 ) & 0x1F;
	}

	inline bool getAlpha() const {
		return m_data & 1;
	}

	inline void setRed( ubyte red ) {
		assert( red <= 0x1F );
		m_data = ( m_data & 0x7FF ) | ( red << 11 );
	}

	inline void setGreen( ubyte green ) {
		assert( green <= 0x1F );
		m_data = ( m_data & 0xF83F ) | ( green << 6 );
	}

	inline void setBlue( ubyte blue ) {
		assert( blue <= 0x1F );
		m_data = ( m_data & 0xFFC1 ) | ( blue << 1 );
	}

	inline void setAlpha( bool alpha ) {
		m_data = ( m_data & 0xFFFE ) | ( alpha ? 1 : 0 );
	}

	inline void writeToRom( std::ostream &rom ) const {
		const ushort data = htons( m_data );
		rom.write( (char*)&data, sizeof(ushort) );
	}

	static ColourRGBA5551 readFromRom( std::istream &rom ) {
		ushort data;
		rom.read( (char*)&data, sizeof(ushort) );
		return ColourRGBA5551( ntohs( data ) );
	}

	inline ushort toShort() const {
		return m_data;
	}

	bool operator==( const ColourRGBA5551 &other ) const {
		return m_data == other.m_data;
	}

	bool operator!=( const ColourRGBA5551 &other ) const {
		return m_data != other.m_data;
	}

	string toString() const;
	bool tryParse( const string &str );
	inline static ColourRGBA5551 parse( const string &str ) {
		return (ColourRGBA5551)ColourRGBA32::parse( str );
	}

	explicit operator ColourRGB24() const;
	explicit operator ColourRGBA32() const;

};

inline ColourRGB24::operator ColourRGBA32() const {
	return ColourRGBA32( red, green, blue, 0xFF );
}

inline ColourRGB24::operator ColourRGBA5551() const {
	return ColourRGBA5551( red >> 3, green >> 3, blue >> 3 );
}

inline ColourRGBA32::operator ColourRGB24() const {
	return ColourRGB24( red, green, blue );
}

inline ColourRGBA32::operator ColourRGBA5551() const {
	return ColourRGBA5551( red >> 3, green >> 3, blue >> 3, alpha > 0 );
}

inline ColourRGBA5551::operator ColourRGB24() const {
	return ColourRGB24( getRed() << 3, getGreen() << 3, getBlue() << 3 );
}

inline ColourRGBA5551::operator ColourRGBA32() const {
	return ColourRGBA32( getRed() << 3, getGreen() << 3, getBlue() << 3, getAlpha() ? 0xFF : 0x00 );
}

namespace std {
	template <> struct hash<ColourRGBA32> {
		size_t operator()(const ColourRGBA32 & x) const {
			return x.toInt();
		}
	};

	template <> struct hash<ColourRGB24> {
		size_t operator()(const ColourRGB24 & x) const {
			return ((size_t)x.red << 16) | ((size_t)x.green << 8) | (size_t)x.blue;
		}
	};

	template <> struct hash<ColourRGBA5551> {
		size_t operator()(const ColourRGBA5551 & x) const {
			return x.toShort();
		}
	};
}

namespace JsonSerializer {
	template<> void serialize<ColourRGBA32>( JsonWriter &jw, const ColourRGBA32 &obj );
	template<> ColourRGBA32 parse<ColourRGBA32>( const Json &json );

	template<> void serialize<ColourRGB24>( JsonWriter &jw, const ColourRGB24 &obj );
	template<> ColourRGB24 parse<ColourRGB24>( const Json &json );

	template<> void serialize<ColourRGBA5551>( JsonWriter &jw, const ColourRGBA5551 &obj );
	template<> ColourRGBA5551 parse<ColourRGBA5551>( const Json &json );
}

#endif /* CORE_COLOUR_HPP_ */
