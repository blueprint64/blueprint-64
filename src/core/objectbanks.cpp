#include "src/core/objectbanks.hpp"

const char *objectBank1Names[11] = {
	"TTM Enemies",
	"WF Enemies + Heave-Ho",
	"THI Enemies",
	"BBH Objects",
	"Snow Objects",
	"SSL Objects",
	"Water Objects",
	"LLL Enemies",
	"Castle Grounds Objects",
	"Cap Switches",
	"BOB Objects"
};
const char *objectBank2Names[6] = {
	"HMC Enemies",
	"BOB + WF Enemies",
	"SML Enemies",
	"Water Objects 2",
	"Castle Objects",
	"Bowser"
};
const char *objectBank3Names[30] = {
	"Castle Grounds",
	"Inside the Castle",
	"Castle Courtyard",
	"Bob-Omb Battlefield",
	"Whomp's Fortress",
	"Jolly Rodger Bay",
	"Cool Cool Mountain",
	"Big Boo's Haunt",
	"Hazy Maze Cave",
	"Lethal Lava Land",
	"Shifting Sand Land",
	"Dire Dire Docks",
	"Snowman's Land",
	"Wet Dry World",
	"Tall Tall Mountain",
	"Tiny Huge Island",
	"Tick Tock Clock",
	"Rainbow Ride",
	"Bowser in the Dark World",
	"Bowser in the Fire Sea",
	"Bowser in the Sky",
	"Bowser 1",
	"Bowser 2",
	"Bowser 3",
	"Tower of the Wing Cap",
	"Cavern of the Metal Cap",
	"Vanish Cap Under the Moat",
	"Peach's Secret Slide",
	"Secret Aquarium",
	"Wing Mario Over the Rainbow"
};

static_assert( sizeof( objectBank1Names) == sizeof(void*) * 11 );
static_assert( sizeof( objectBank2Names) == sizeof(void*) * 6 );
static_assert( sizeof( objectBank3Names) == sizeof(void*) * 30 );

const std::vector<const char*> objectBank1Contents[11] = {
	{ "Fwoosh", "Monty Mole", "Ukiki" },
	{ "Bullet Bill", "Heave-Ho", "Hoot", "Thwomp", "Yellow Sphere (WF Pole)", "Yoshi Egg" },
	{ "Bubba (Big Fish)", "Lakitu (Enemy)", "Spiny", "Wiggler" },
	{ "Beta Key", "Boo", "Bookend", "Haunted Cage", "Haunted Chair", "Mad Piano" },
	{ "Mr. Blizzard (Snowman)", "Penguin", "Spindrift" },
	{ "Eyerok", "Klepto", "Pokey", "Dust Devil" },
	{ "Clam", "Manta Ray", "Sushi (Shark)", "Unagi (Eel)", "Whirlpool" },
	{ "Beta Blargg", "Big Bully", "Bully" },
	{ "Birds", "Peach", "Yoshi" },
	{ "Cap Switch" },
	{ "King Bob-Omb", "Water Bomb" }
};

const std::vector<const char*> objectBank2Contents[6] {
	{ "Dorrie", "Mr. I", "Scuttlebug", "Snufit", "Swoop (Bat)" },
	{ "Chain Chomp", "Koopa", "Metal Ball", "Piranha Plant", "Race Flag", "Whomp", "Wooden Post" },
	{ "Chilly Bully", "Moneybag" },
	{ "Cheep Cheep (Fish)", "Skeeter", "Small Fish", "Treasure Chest", "Water Ring" },
	{ "Boo (Castle)", "Lakitu (Friendly)", "MIPS", "Toad" },
	{ "Bowser", "Bowser Bomb", "Bowser Bomb Flames", "Bowser Bomb Smoke" }
};

const std::vector<const char*> objectBank3Contents[30] {
	{ "Level Model, Collision, and Textures", "Castle Central Spire", "Flag", "Grate" },
	{ "Level Model, Collision, and Textures", "Bowser Trap", "Clock Hour Hand", "Clock Minute Hand", "Clock Pendulum", "Pillar", "Big Star Door" },
	{ "Level Model, Collision, and Textures", "Castle Central Spire" },
	{ "Level Model, Collision, and Textures", "Bridge", "Chain Chomp Gate", "Metal Bars" },
	{ "Level Model, Collision, and Textures", "Breakable Wall", "Brick Pillar", "Brick Platfrom", "Bullet Bill Box", "Dirt Ramp", "Fences", "Big Grass and Flowers Platform", "Gravel Ramp", "Kickable Board", "Platform with Plank", "Pole", "Stone Platform and Ramp", "Spinning Grass Platform", "Stairs", "Tower", "Tower Door", "Wallkick Star Platforms" },
	{ "Level Model, Collision, and Textures", "Falling Pillar", "JRB Rock", "JRB Ship", "Raft", "Sliding Crate", "Sunken Ship" },
	{ "Level Model, Collision, and Textures", "Cabin 1", "Cabin 2", "Ice Pillar", "Ropeway Lift", "Snowman Body Platform", "Snowman Head", "Wallkicks With Work Platforms" },
	{ "Level Model, Collision, and Textures", "Coffin", "Elevator", "False Floor", "Merry-Go-Round", "Moving Bookshelf", "Staircase Step", "Tumbling Platform" },
	{ "Level Model, Collision, and Textures", "Arrow Platform", "Boulder", "Elevator", "Metal Platform", "Red Mesh Gate" },
	{ "Level Model, Collision, and Textures", "Arrow Platform", "Bowser Puzzle Pieces", "Bully Triplet Platform", "Cage Platform", "Drawbridge", "Fire Bar Spinner", "Fire Spitter", "Hexagonal Grate", "Hexagonal Metal Platform", "Long Platform", "Moving Octagonal Platform", "M Platform", "Octagonal Platform", "Platform with Archway", "Rocky Platform", "Rotating Hexagonal Ring", "Sinking Platform", "Sinking Rock", "Tilting Platform", "Various Falling Platforms", "Volcano", "Wooden Bridges" },
	{ "Level Model, Collision, and Textures", "Grindel", "Moving Stairs", "Pyramid Elevator", "Pyramid Top", "Quicksand Pool", "Spindel" },
	{ "Level Model, Collision, and Textures", "Bowser Gate", "Bowser's Sub", "Moving Pole" },
	{ "Level Model, Collision, and Textures", "Cracked Ice", "Snow Wave" },
	{ "Level Model, Collision, and Textures", "Moving Arrow Block", "Spinning Platform", "Various Floating Wood Platforms", "Water Level Switch", "Wooden Elevator" },
	{ "Level Model, Collision, and Textures", "Cannon Hole", "Hangable Vines", "Hidden Slide Warp", "Rolling Log", "Slide Exit Area", "Star Cage", "Various Mountain Pieces", "Various Smiley Faces", "Various Tall Mushrooms" },
	{ "Level Model, Collision, and Textures", "Grass Platform", "Mountaintop Hole" },
	{ "Level Model, Collision, and Textures", "Clock Hand", "Elevator", "Gears", "Pendulum", "Piston Block", "Rotating Cube", "Rotating Hexagonal Platform", "Rotating Prism", "Rotating Triangular Platform", "Spinner", "Striped Blocks", "Treadmills" },
	{ "Level Model, Collision, and Textures", "Boat", "Boat Wing", "Castle", "Crow's Nest", "Donut Platform", "Elevator", "L-Shaped Platform", "Magic Carpet", "Octagonal Platform", "Rainbow Ring", "Rotating Bridge Platform", "Seesaw Platform", "Sliding Platform", "Swinging Platform", "Triangular Platform", "Tricky Triangles", "Various Rainbow Paths", "Various Static Platforms", "Wooden Bridge" },
	{ "Level Model, Collision, and Textures", "Blue Crystal", "Blue Platform", "Ferris Wheel Platform", "Seesaw Platform", "Sliding Platform", "Square Platform", "Staircase", "Various Static Platforms" },
	{ "Level Model, Collision, and Textures", "Blue Pole", "Bobbing Wave Platform", "Double Pillars", "Elevator", "Exit Funnel and Platform", "Flat Star", "Moving Cage Platform", "Seesaw Platform", "Sinking Cage Platform", "Sliding Platform", "Tilting Platform", "Tumbling Bridge", "Various Static Platforms" },
	{ "Level Model, Collision, and Textures", "Arrow Platform", "Blue Platform", "Ferris Wheel Platforms", "Octagonal Platform", "Seesaw Platform", "Sliding Platform 1", "Sliding Platform 2", "Staircase", "Tilting W Platform" },
	{ "Level Model, Collision, and Textures" },
	{ "Level Model, Collision, and Textures", "Tilting Arena" },
	{ "Level Model, Collision, and Textures", "Bowser Bomb Pedestal", "Falling Platform Pieces" },
	{ "Level Model, Collision, and Textures", "Flat Cloud" },
	{ "Level Model, Collision, and Textures" },
	{ "Level Model, Collision, and Textures", "Seesaw Platform" },
	{ "Level Model, Collision, and Textures" },
	{ "Level Model, Collision, and Textures" },
	{ "Level Model, Collision, and Textures" }
};
