#ifndef SRC_CORE_ARMIPS_HPP_
#define SRC_CORE_ARMIPS_HPP_

#include <string>
#include <map>
#include "src/core/enums.hpp"

typedef std::map<LevelId,std::map<std::string,uint>> AsmReferences;

namespace Armips {

	extern bool exists();

	extern void autogenerateWithoutReferences();
	extern void autogenerate( AsmReferences asmReferences, const std::map<string,uint> &cameraTriggerRefs );
	extern void build();

	extern AsmReferences getCurrentAsmReferences();

}



#endif /* SRC_CORE_ARMIPS_HPP_ */
