struct BankInfo1 {
	uint bankCstart;
	uint bankCend;
	uint bank5start;
	uint bank5end;
};

struct BankInfo2 {
	uint bankDstart;
	uint bankDend;
	uint bank6start;
	uint bank6end;
};

struct BankInfo3 {
	uint bank12start;
	uint bank12end;
	uint bank7start;
	uint bank7end;
	uint bank9start;
	uint bank9end;
};

static const BankInfo1 s_bank1[11] = {
	{ 0x001602E0, 0x00160670, 0x008DD7DC, 0x008F3894 },
	{ 0x00132850, 0x00132C60, 0x00860EDC, 0x0087623C },
	{ 0x00187FA0, 0x00188440, 0x00960204, 0x009770C4 },
	{ 0x0016D5C0, 0x0016D870, 0x0091BE8C, 0x0092C004 },
	{ 0x001656E0, 0x00165A50, 0x008FB894, 0x009089C4 },
	{ 0x00151B70, 0x001521D0, 0x008C118C, 0x008D57DC },
	{ 0x00145C10, 0x00145E90, 0x008A545C, 0x008B918C },
	{ 0x00134A70, 0x00134D20, 0x0087E23C, 0x008843BC },
	{ 0x00180540, 0x00180BB0, 0x00934004, 0x00958204 },
	{ 0x00166BD0, 0x00166C60, 0x009109C4, 0x00913E8C },
	{ 0x0013B5D0, 0x0013B910, 0x0088C3BC, 0x0089D45C }
};

static const BankInfo2 s_bank2[6] = {
	{ 0x001F1B30, 0x001F2200, 0x00A647AC, 0x00A7981C },
	{ 0x001D7C90, 0x001D8310, 0x00A09934, 0x00A2EABC },
	{ 0x001E7D90, 0x001E7EE0, 0x00A56934, 0x00A5C7AC },
	{ 0x001C3DB0, 0x001C4230, 0x009E9FD4, 0x00A01934 },
	{ 0x001E4BF0, 0x001E51F0, 0x00A36ABC, 0x00A4E934 },
	{ 0x001B9070, 0x001B9CC0, 0x0097F0C4, 0x009E1FD4 }
};

static const BankInfo3 s_bank3[30] = {
	{ 0x004545E0, 0x00454E00, 0x00FC7806, 0x00FD907E, 0x00D769DC, 0x00D84670 },
	{ 0x003CF0D0, 0x003D0DC0, 0x00E0BB06, 0x00E84C1E, 0x00D2320C, 0x00D31320 },
	{ 0x004AF670, 0x004AF930, 0x010E99EA, 0x010F0866, 0x00D769DC, 0x00D84670 },
	{ 0x00405A60, 0x00405FB0, 0x00EF0E36, 0x00F025F8, 0x00CD34BC, 0x00CE03D0 },
	{ 0x0049DA50, 0x0049E710, 0x010A0882, 0x010B269A, 0x00D611C8, 0x00D6E9DC },
	{ 0x00423B20, 0x004246D0, 0x00F42808, 0x00F53BB4, 0x00CE83D0, 0x00CF64E4 },
	{ 0x00395C90, 0x00396340, 0x00DE0360, 0x00E03B06, 0x00D0FDF8, 0x00D1B20C },
	{ 0x003828C0, 0x00383950, 0x00DB151C, 0x00DD8360, 0x00CBD3A8, 0x00CCB4BC },
	{ 0x003E6A00, 0x003E76B0, 0x00E8CC1E, 0x00EB8586, 0x00D2320C, 0x00D31320 },
	{ 0x0048C9B0, 0x0048D930, 0x010582A2, 0x01080B72, 0x00CA7B94, 0x00CB53A8 },
	{ 0x003FB990, 0x003FC2B0, 0x00EC0586, 0x00EE8E36, 0x00CD34BC, 0x00CE03D0 },
	{ 0x00495A60, 0x00496090, 0x01088B72, 0x01098882, 0x00CE83D0, 0x00CF64E4 },
	{ 0x0040E840, 0x0040ED70, 0x00F0A5F8, 0x00F1A080, 0x00D0FDF8, 0x00D1B20C },
	{ 0x00419F90, 0x0041A760, 0x00F22080, 0x00F3A808, 0x00D611C8, 0x00D6E9DC },
	{ 0x004EB1F0, 0x004EC000, 0x01173142, 0x011A35B6, 0x00D4B0B4, 0x00D591C8 },
	{ 0x0042C6E0, 0x0042CF20, 0x00F5BBB4, 0x00F69F70, 0x00D611C8, 0x00D6E9DC },
	{ 0x00437400, 0x00437870, 0x00F71F70, 0x00F88990, 0x00D39320, 0x00D430B4 },
	{ 0x0044A140, 0x0044ABC0, 0x00F90990, 0x00FBF806, 0x00CFE4E4, 0x00D07DF8 },
	{ 0x0045BF60, 0x0045C600, 0x00FE107E, 0x00FF0EAE, 0x00CFE4E4, 0x00D07DF8 },
	{ 0x0046A840, 0x0046B090, 0x0100BB76, 0x0102177E, 0x00CFE4E4, 0x00D07DF8 },
	{ 0x00477D00, 0x004784A0, 0x01034AAE, 0x010502A2, 0x00CFE4E4, 0x00D07DF8 },
	{ 0x004C41C0, 0x004C4320, 0x01136270, 0x01138D38, 0, 0 },
	{ 0x004CE9F0, 0x004CEC00, 0x0115C4E6, 0x0115E086, 0x00CA7B94, 0x00CB53A8 },
	{ 0x004D14F0, 0x004D1910, 0x01166086, 0x0116B142, 0, 0 },
	{ 0x004C2700, 0x004C2920, 0x011258AA, 0x0112E270, 0x00CFE4E4, 0x00D07DF8 },
	{ 0x004BE9E0, 0x004BEC30, 0x01111902, 0x0111D8AA, 0x00D2320C, 0x00D31320 },
	{ 0x00461220, 0x004614D0, 0x00FF8EAE, 0x01003B76, 0x00D769DC, 0x00D84670 },
	{ 0x004B7F10, 0x004B80D0, 0x010F8866, 0x01109902, 0x00D4B0B4, 0x00D591C8 },
	{ 0x0046C1A0, 0x0046C3A0, 0x0102977E, 0x0102CAAE, 0x00D8C670, 0x00D9A784 },
	{ 0x004CD930, 0x004CDBD0, 0x01140D38, 0x011544E6, 0x00CFE4E4, 0x00D07DF8 }
};

static const uint s_modelLoader1[11] = {
	0x150007E8,
	0x1500071C,
	0x150008A4,
	0x1500084C,
	0x1500080C,
	0x150007B4,
	0x15000788,
	0x15000750,
	0x15000888,
	0x15000830,
	0x1500076C
};

static const uint s_modelLoader2[6] = {
	0x150009DC,
	0x15000958,
	0x150009C0,
	0x15000914,
	0x1500099C,
	0x150008D8
};

static inline void loadBank3Models( std::ostream &rom, sbyte bankIndex ) {
	switch( bankIndex ) {
		case 0: { // Castle Grounds
			loadModelFromGeoLayout( rom, 0x03, 0x120006F4 );
			loadModelFromGeoLayout( rom, 0x36, 0x1200070C );
			loadModelFromGeoLayout( rom, 0x37, 0x12000660 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000724 );
			return;
		}
		case 1: { // Inside the Castle
			loadModelFromGeoLayout( rom, 0x35, 0x12000F18 );
			loadModelFromGeoLayout( rom, 0x36, 0x12001940 );
			loadModelFromGeoLayout( rom, 0x37, 0x12001530 );
			loadModelFromGeoLayout( rom, 0x38, 0x12001548 );
			loadModelFromGeoLayout( rom, 0x39, 0x12001518 );
			loadModelFromGeoLayout( rom, 0xD0, 0x12000F00 );
			// Half Star Door is assigned to 4 different model IDs. Because reasons.
			loadModelFromGeoLayout( rom, 0xD5, 0x12000F00 );
			loadModelFromGeoLayout( rom, 0xD1, 0x12000F00 );
			loadModelFromGeoLayout( rom, 0xD6, 0x12000F00 );
			return;
		}
		case 2: { // Castle Courtyard
			loadModelFromGeoLayout( rom, 0x03, 0x12000200 );
			return;
		}
		case 3: { // Bob-Omb Battlefield
			loadModelFromGeoLayout( rom, 0x36, 0x12000440 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000458 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000470 );
			return;
		}
		case 4: { // Whomp's Fortress
			loadModelFromGeoLayout( rom, 0x03, 0x120007E0 );
			loadModelFromGeoLayout( rom, 0x04, 0x12000820 );
			loadModelFromGeoLayout( rom, 0x05, 0x12000860 );
			loadModelFromGeoLayout( rom, 0x06, 0x12000878 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000890 );
			loadModelFromGeoLayout( rom, 0x08, 0x120008A8 );
			loadModelFromGeoLayout( rom, 0x09, 0x120008E8 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000900 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000940 );
			loadModelFromGeoLayout( rom, 0x0D, 0x12000AE0 );
			loadModelFromGeoLayout( rom, 0x0E, 0x12000958 );
			loadModelFromGeoLayout( rom, 0x0F, 0x120009A0 );
			loadModelFromGeoLayout( rom, 0x10, 0x120009B8 );
			loadModelFromGeoLayout( rom, 0x11, 0x120009D0 );
			loadModelFromGeoLayout( rom, 0x12, 0x120009E8 );
			loadModelFromGeoLayout( rom, 0xAE, 0x12000A00 );
			loadModelFromGeoLayout( rom, 0xB1, 0x12000A40 );
			loadModelFromGeoLayout( rom, 0xAF, 0x12000A58 );
			loadModelFromGeoLayout( rom, 0xAD, 0x12000A98 );
			loadModelFromGeoLayout( rom, 0xB0, 0x12000AB0 );
			loadModelFromGeoLayout( rom, 0xB2, 0x12000AC8 );
			loadModelFromGeoLayout( rom, 0x2C, 0x12000AF8 );
			loadModelFromGeoLayout( rom, 0x2D, 0x12000B10 );
			loadModelFromGeoLayout( rom, 0x2E, 0x12000B38 );
			loadModelFromGeoLayout( rom, 0x2F, 0x12000B60 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000B78 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000B90 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000BA8 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000BE0 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000BC8 );
			return;
		}
		case 5: { // Jolly Rodger Bay
			loadModelFromGeoLayout( rom, 0x35, 0x12000978 );
			loadModelFromGeoLayout( rom, 0x36, 0x120009B0 );
			loadModelFromGeoLayout( rom, 0x37, 0x120009E8 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000A00 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000990 );
			loadModelFromGeoLayout( rom, 0x3A, 0x120009C8 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000930 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000960 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000900 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000918 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000948 );
			return;
		}
		case 6: { // Cool Cool Mountain
			loadModelFromGeoLayout( rom, 0x03, 0x1200043C );
			loadModelFromGeoLayout( rom, 0x04, 0x1200046C );
			loadModelFromGeoLayout( rom, 0x05, 0x120004A4 );
			loadModelFromGeoLayout( rom, 0x06, 0x120004CC );
			loadModelFromGeoLayout( rom, 0x07, 0x120004F4 );
			loadModelFromGeoLayout( rom, 0x36, 0x120003E0 );
			loadModelFromGeoLayout( rom, 0xD2, 0x12000400 );
			loadModelFromGeoLayout( rom, 0x37, 0x1200041C );
			return;
		}
		case 7: { // Big Boo's Haunt
			loadModelFromGeoLayout( rom, 0x35, 0x120005B0 );
			loadModelFromGeoLayout( rom, 0x36, 0x120005C8 );
			loadModelFromGeoLayout( rom, 0x37, 0x120005E0 );
			loadModelFromGeoLayout( rom, 0x38, 0x120005F8 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000610 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000628 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000640 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000658 );
			return;
		}
		case 8: { // Hazy Maze Cave
			loadModelFromGeoLayout( rom, 0x36, 0x120005A0 );
			loadModelFromGeoLayout( rom, 0x37, 0x120005B8 );
			loadModelFromGeoLayout( rom, 0x38, 0x120005D0 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000548 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000570 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000588 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000530 );
			return;
		}
		case 9: { // Lethal Lava Land
			loadModelFromGeoLayout( rom, 0x03, 0x120009E0 );
			loadModelFromGeoLayout( rom, 0x04, 0x120009F8 );
			loadModelFromGeoLayout( rom, 0x05, 0x12000A10 );
			loadModelFromGeoLayout( rom, 0x06, 0x12000A28 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000A40 );
			loadModelFromGeoLayout( rom, 0x08, 0x12000A60 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000A90 );
			loadModelFromGeoLayout( rom, 0x0B, 0x12000AA8 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000AC0 );
			loadModelFromGeoLayout( rom, 0x0D, 0x12000AD8 );
			loadModelFromGeoLayout( rom, 0x0E, 0x12000AF0 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000B20 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000B38 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000BB0 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000BC8 );
			loadModelFromGeoLayout( rom, 0x40, 0x12000BE0 );
			loadModelFromGeoLayout( rom, 0x41, 0x12000BF8 );
			loadModelFromGeoLayout( rom, 0x43, 0x12000C10 );
			loadModelFromGeoLayout( rom, 0x44, 0x12000C30 );
			loadModelFromGeoLayout( rom, 0x45, 0x12000C50 );
			loadModelFromGeoLayout( rom, 0x46, 0x12000C70 );
			loadModelFromGeoLayout( rom, 0x47, 0x12000C90 );
			loadModelFromGeoLayout( rom, 0x48, 0x12000CB0 );
			loadModelFromGeoLayout( rom, 0x49, 0x12000CD0 );
			loadModelFromGeoLayout( rom, 0x4A, 0x12000CF0 );
			loadModelFromGeoLayout( rom, 0x4B, 0x12000D10 );
			loadModelFromGeoLayout( rom, 0x4C, 0x12000D30 );
			loadModelFromGeoLayout( rom, 0x4D, 0x12000D50 );
			loadModelFromGeoLayout( rom, 0x4E, 0x12000D70 );
			loadModelFromGeoLayout( rom, 0x4F, 0x12000D90 );
			loadModelFromGeoLayout( rom, 0x50, 0x12000DB0 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000B08 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000DD0 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000DE8 );
			loadModelFromGeoLayout( rom, 0x09, 0x12000A78 );
			loadModelFromGeoLayout( rom, 0x35, 0x12000B50 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000B68 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000B80 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000B98 );
			loadModelFromGeoLayout( rom, 0x53, 0x12000EA8 );
			return;
		}
		case 10: { // Shifting Sand Land
			loadModelFromGeoLayout( rom, 0x03, 0x120005C0 );
			loadModelFromGeoLayout( rom, 0x04, 0x120005D8 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000618 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000734 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000764 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000794 );
			loadModelFromGeoLayout( rom, 0x39, 0x120007AC );
			loadModelFromGeoLayout( rom, 0xC7, 0x12000630 );
			return;
		}
		case 11: { // Dire Dire Docks
			loadModelFromGeoLayout( rom, 0x36, 0x12000478 );
			loadModelFromGeoLayout( rom, 0x37, 0x120004A0 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000450 );
			return;
		}
		case 12: { // Snowman's Land
			loadModelFromGeoLayout( rom, 0x36, 0x12000390 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000360 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000378 );
			return;
		}
		case 13: { // Wet Dry World
			loadModelFromGeoLayout( rom, 0x36, 0x12000580 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000598 );
			loadModelFromGeoLayout( rom, 0x38, 0x120005C0 );
			loadModelFromGeoLayout( rom, 0x39, 0x120005E8 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000610 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000628 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000640 );
			return;
		}
		case 14: { // Tall Tall Mountain
			loadModelFromGeoLayout( rom, 0x7B, 0x12000DF4 );
			loadModelFromGeoLayout( rom, 0x35, 0x12000730 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000710 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000D14 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000D4C );
			loadModelFromGeoLayout( rom, 0x39, 0x12000D84 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000DBC );
			loadModelFromGeoLayout( rom, 0x03, 0x12000748 );
			loadModelFromGeoLayout( rom, 0x04, 0x12000778 );
			loadModelFromGeoLayout( rom, 0x05, 0x120007A8 );
			loadModelFromGeoLayout( rom, 0x06, 0x120007D8 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000808 );
			loadModelFromGeoLayout( rom, 0x08, 0x12000830 );
			loadModelFromGeoLayout( rom, 0x09, 0x12000858 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000880 );
			loadModelFromGeoLayout( rom, 0x0B, 0x120008A8 );
			loadModelFromGeoLayout( rom, 0x0C, 0x120008D0 );
			loadModelFromGeoLayout( rom, 0x0D, 0x120008F8 );
			loadModelFromGeoLayout( rom, 0x0F, 0x12000920 );
			loadModelFromGeoLayout( rom, 0x10, 0x12000948 );
			loadModelFromGeoLayout( rom, 0x11, 0x12000970 );
			loadModelFromGeoLayout( rom, 0x12, 0x12000990 );
			loadModelFromGeoLayout( rom, 0x13, 0x120009C0 );
			loadModelFromGeoLayout( rom, 0x14, 0x120009F0 );
			loadModelFromGeoLayout( rom, 0x15, 0x12000A18 );
			loadModelFromGeoLayout( rom, 0x16, 0x12000A40 );
			return;
		}
		case 15: { // Tiny Huge Island
			loadModelFromGeoLayout( rom, 0x03, 0x120005F0 );
			loadModelFromGeoLayout( rom, 0x36, 0x120005B0 );
			loadModelFromGeoLayout( rom, 0x37, 0x120005C8 );
			return;
		}
		case 16: { // Tick Tock Clock
			loadModelFromGeoLayout( rom, 0x36, 0x12000240 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000258 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000270 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000288 );
			loadModelFromGeoLayout( rom, 0x3A, 0x120002A8 );
			loadModelFromGeoLayout( rom, 0x3B, 0x120002C8 );
			loadModelFromGeoLayout( rom, 0x3C, 0x120002E0 );
			loadModelFromGeoLayout( rom, 0x3D, 0x120002F8 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000310 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000328 );
			loadModelFromGeoLayout( rom, 0x40, 0x12000340 );
			loadModelFromGeoLayout( rom, 0x41, 0x12000358 );
			loadModelFromGeoLayout( rom, 0x42, 0x12000370 );
			loadModelFromGeoLayout( rom, 0x43, 0x12000388 );
			loadModelFromGeoLayout( rom, 0x44, 0x120003A0 );
			return;
		}
		case 17: { // Rainbow Ride
			loadModelFromGeoLayout( rom, 0x03, 0x12000660 );
			loadModelFromGeoLayout( rom, 0x04, 0x12000678 );
			loadModelFromGeoLayout( rom, 0x05, 0x12000690 );
			loadModelFromGeoLayout( rom, 0x06, 0x120006A8 );
			loadModelFromGeoLayout( rom, 0x07, 0x120006C0 );
			loadModelFromGeoLayout( rom, 0x08, 0x120006D8 );
			loadModelFromGeoLayout( rom, 0x09, 0x120006F0 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000708 );
			loadModelFromGeoLayout( rom, 0x0B, 0x12000720 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000738 );
			loadModelFromGeoLayout( rom, 0x0D, 0x12000758 );
			loadModelFromGeoLayout( rom, 0x0E, 0x12000770 );
			loadModelFromGeoLayout( rom, 0x0F, 0x12000788 );
			loadModelFromGeoLayout( rom, 0x10, 0x120007A0 );
			loadModelFromGeoLayout( rom, 0x11, 0x120007B8 );
			loadModelFromGeoLayout( rom, 0x12, 0x120007D0 );
			loadModelFromGeoLayout( rom, 0x13, 0x120007E8 );
			loadModelFromGeoLayout( rom, 0x14, 0x12000800 );
			loadModelFromGeoLayout( rom, 0x15, 0x12000818 );
			loadModelFromGeoLayout( rom, 0x16, 0x12000830 );
			loadModelFromGeoLayout( rom, 0x36, 0x120008C0 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000848 );
			loadModelFromGeoLayout( rom, 0x38, 0x120008A8 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000878 );
			loadModelFromGeoLayout( rom, 0x3A, 0x120008D8 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000890 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000908 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000940 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000860 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000920 );
			loadModelFromGeoLayout( rom, 0x40, 0x120008F0 );
			loadModelFromGeoLayout( rom, 0x41, 0x12000958 );
			loadModelFromGeoLayout( rom, 0x42, 0x12000970 );
			loadModelFromGeoLayout( rom, 0x43, 0x12000988 );
			loadModelFromGeoLayout( rom, 0x44, 0x120009A0 );
			loadModelFromGeoLayout( rom, 0x45, 0x120009B8 );
			return;
		}
		case 18: { // Bowser in the Dark World
			loadModelFromGeoLayout( rom, 0x03, 0x120003C0 );
			loadModelFromGeoLayout( rom, 0x04, 0x120003D8 );
			loadModelFromGeoLayout( rom, 0x05, 0x120003F0 );
			loadModelFromGeoLayout( rom, 0x06, 0x12000408 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000420 );
			loadModelFromGeoLayout( rom, 0x08, 0x12000438 );
			loadModelFromGeoLayout( rom, 0x09, 0x12000450 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000468 );
			loadModelFromGeoLayout( rom, 0x0B, 0x12000480 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000498 );
			loadModelFromGeoLayout( rom, 0x0D, 0x120004B0 );
			loadModelFromGeoLayout( rom, 0x0E, 0x120004C8 );
			loadModelFromGeoLayout( rom, 0x0F, 0x120004E0 );
			loadModelFromGeoLayout( rom, 0x10, 0x120004F8 );
			loadModelFromGeoLayout( rom, 0x11, 0x12000510 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000558 );
			loadModelFromGeoLayout( rom, 0x37, 0x12000540 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000528 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000570 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000588 );
			loadModelFromGeoLayout( rom, 0x3B, 0x120005A0 );
			loadModelFromGeoLayout( rom, 0x3C, 0x120005B8 );
			loadModelFromGeoLayout( rom, 0x3D, 0x120005D0 );
			loadModelFromGeoLayout( rom, 0x3E, 0x120005E8 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000600 );
			return;
		}
		case 19: { // Bowser in the Fire Sea
			loadModelFromGeoLayout( rom, 0x03, 0x120004B0 );
			loadModelFromGeoLayout( rom, 0x04, 0x120004C8 );
			loadModelFromGeoLayout( rom, 0x05, 0x120004E0 );
			loadModelFromGeoLayout( rom, 0x06, 0x120004F8 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000510 );
			loadModelFromGeoLayout( rom, 0x08, 0x12000528 );
			loadModelFromGeoLayout( rom, 0x09, 0x12000540 );
			loadModelFromGeoLayout( rom, 0x0A, 0x12000558 );
			loadModelFromGeoLayout( rom, 0x0B, 0x12000570 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000588 );
			loadModelFromGeoLayout( rom, 0x0D, 0x120005A0 );
			loadModelFromGeoLayout( rom, 0x0E, 0x120005B8 );
			loadModelFromGeoLayout( rom, 0x0F, 0x120005D0 );
			loadModelFromGeoLayout( rom, 0x10, 0x120005E8 );
			loadModelFromGeoLayout( rom, 0x11, 0x12000600 );
			loadModelFromGeoLayout( rom, 0x12, 0x12000618 );
			loadModelFromGeoLayout( rom, 0x13, 0x12000630 );
			loadModelFromGeoLayout( rom, 0x14, 0x12000648 );
			loadModelFromGeoLayout( rom, 0x15, 0x12000660 );
			loadModelFromGeoLayout( rom, 0x36, 0x12000758 );
			loadModelFromGeoLayout( rom, 0x37, 0x120006C0 );
			loadModelFromGeoLayout( rom, 0x38, 0x12000770 );
			loadModelFromGeoLayout( rom, 0x39, 0x120006A8 );
			loadModelFromGeoLayout( rom, 0x3A, 0x12000690 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000678 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000708 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000788 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000728 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000740 );
			loadModelFromGeoLayout( rom, 0x40, 0x120006D8 );
			loadModelFromGeoLayout( rom, 0x41, 0x120006F0 );
			return;
		}
		case 20: { // Bowser in the Sky
			loadModelFromGeoLayout( rom, 0x03, 0x12000430 );
			loadModelFromGeoLayout( rom, 0x04, 0x12000448 );
			loadModelFromGeoLayout( rom, 0x05, 0x12000460 );
			loadModelFromGeoLayout( rom, 0x06, 0x12000478 );
			loadModelFromGeoLayout( rom, 0x07, 0x12000490 );
			loadModelFromGeoLayout( rom, 0x08, 0x120004A8 );
			loadModelFromGeoLayout( rom, 0x09, 0x120004C0 );
			loadModelFromGeoLayout( rom, 0x0A, 0x120004D8 );
			loadModelFromGeoLayout( rom, 0x0B, 0x120004F0 );
			loadModelFromGeoLayout( rom, 0x0C, 0x12000508 );
			loadModelFromGeoLayout( rom, 0x0D, 0x12000520 );
			loadModelFromGeoLayout( rom, 0x0E, 0x12000538 );
			loadModelFromGeoLayout( rom, 0x0F, 0x12000550 );
			loadModelFromGeoLayout( rom, 0x10, 0x12000568 );
			loadModelFromGeoLayout( rom, 0x11, 0x12000580 );
			loadModelFromGeoLayout( rom, 0x12, 0x12000598 );
			loadModelFromGeoLayout( rom, 0x13, 0x120005B0 );
			loadModelFromGeoLayout( rom, 0x14, 0x120005C8 );
			loadModelFromGeoLayout( rom, 0x36, 0x120005E0 );
			loadModelFromGeoLayout( rom, 0x37, 0x120005F8 );
			loadModelFromGeoLayout( rom, 0x39, 0x12000610 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000628 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000640 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000658 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000670 );
			loadModelFromGeoLayout( rom, 0x40, 0x12000688 );
			loadModelFromGeoLayout( rom, 0x41, 0x120006A0 );
			loadModelFromGeoLayout( rom, 0x42, 0x120006B8 );
			loadModelFromGeoLayout( rom, 0x43, 0x120006D0 );
			loadModelFromGeoLayout( rom, 0x44, 0x120006E8 );
			loadModelFromGeoLayout( rom, 0x45, 0x12000700 );
			return;
		}
		case 21: { // Bowser 1
			return;
		}
		case 22: { // Bowser 2
			loadModelFromGeoLayout( rom, 0x36, 0x12000170 );
			return;
		}
		case 23: { // Bowser 3
			loadModelFromGeoLayout( rom, 0x36, 0x12000290 );
			loadModelFromGeoLayout( rom, 0x37, 0x120002A8 );
			loadModelFromGeoLayout( rom, 0x38, 0x120002C0 );
			loadModelFromGeoLayout( rom, 0x39, 0x120002D8 );
			loadModelFromGeoLayout( rom, 0x3A, 0x120002F0 );
			loadModelFromGeoLayout( rom, 0x3B, 0x12000308 );
			loadModelFromGeoLayout( rom, 0x3C, 0x12000320 );
			loadModelFromGeoLayout( rom, 0x3D, 0x12000338 );
			loadModelFromGeoLayout( rom, 0x3E, 0x12000350 );
			loadModelFromGeoLayout( rom, 0x3F, 0x12000368 );
			loadModelFromGeoLayout( rom, 0x03, 0x12000380 );
			return;
		}
		case 24: { // Tower of the Wing Cap
			loadModelFromGeoLayout( rom, 0x03, 0x12000160 );
			return;
		}
		case 25: { // Cavern of the Metal Cap
			return;
		}
		case 26: { // Vanish Cap Under the Moat
			loadModelFromGeoLayout( rom, 0x36, 0x120001F0 );
			return;
		}
		default: return;
	}
}
