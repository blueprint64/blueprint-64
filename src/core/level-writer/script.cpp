#include "src/core/level-writer/script.hpp"

#include <unordered_set>
#include "src/core/blueprint.hpp"
#include "src/core/locations.hpp"
#include "src/core/actors.hpp"
#include "src/core/util.hpp"

static void loadModelFromGeoLayout( std::ostream &rom, ubyte modelId, uint location ) {
	writeHalfwordRaw( rom, 0x2208_be16 );
	rom.put( 0x00 );
	rom.put( modelId );
	writeWord( rom, location );
}

#include "src/core/level-writer/bank-locations.cpp.inc"

static void loadSegments( std::ostream &rom, const LevelSpecs &specs, LevelId levelId, bool hasEnvfx, bool usesBackground ) {
	if( usesBackground ) {
		// Loads the ocean background as a placeholder. The actual background will
		// be set by each individual area.
		writeRaw<uint>( rom, { 0x170C000A_be32, 0xB35714_be32, 0xB55854_be32 });
	}
	if( hasEnvfx ) {
		writeRaw<uint>( rom, { 0x170C000B_be32, 0x00DA2784_be32, 0x00DA951C_be32 });
	}

	const uint segmentLocation = RomLocations::getLevelLocation( levelId ).dataLocation;
	writeWords( rom, { 0x170C010E, segmentLocation, segmentLocation + 0x150000 });

	if( specs.objectBanks[0] >= 0 ) {
		const BankInfo1 x = s_bank1[specs.objectBanks[0]];
		writeWords( rom, { 0x170C0105, x.bank5start, x.bank5end });
		writeWords( rom, { 0x170C000C, x.bankCstart, x.bankCend });
	}
	if( specs.objectBanks[1] >= 0 ) {
		const BankInfo2 x = s_bank2[specs.objectBanks[1]];
		writeWords( rom, { 0x170C0106, x.bank6start, x.bank6end });
		writeWords( rom, { 0x170C000D, x.bankDstart, x.bankDend });
	}
	if( specs.objectBanks[2] >= 0 ) {
		const BankInfo3 x = s_bank3[specs.objectBanks[2]];
		writeWords( rom, { 0x170C0012, x.bank12start, x.bank12end });
		writeWords( rom, { 0x170C0107, x.bank7start, x.bank7end });
		if( x.bank9start != 0 ) {
			writeWords( rom, { 0x1A0C0009, x.bank9start, x.bank9end });
		}
	}
	writeRaw<uint>( rom, { 0x170C0008_be32, 0x00A8181C_be32, 0x00AAA40C_be32 });
	writeRaw<uint>( rom, { 0x170C000F_be32, 0x002008D0_be32, 0x00201410_be32 });
}

static void loadModelsFromSegments( std::ostream &rom, const LevelSpecs &specs ) {
	writeRaw<uint>( rom, { 0x06080000_be32, 0x15000660_be32 });
	if( specs.objectBanks[0] >= 0 ) {
		writeWords( rom, { 0x06080000, s_modelLoader1[specs.objectBanks[0]] });
	}
	if( specs.objectBanks[1] >= 0 ) {
		writeWords( rom, { 0x06080000, s_modelLoader2[specs.objectBanks[1]] });
	}
	loadBank3Models( rom, specs.objectBanks[2] );
}

static AreaDataPointerLocations writeAreaScript(
	std::ostream &rom,
	const LevelSpecs &levelSpecs,
	const AreaSpecs &areaSpecs,
	const AreaActors &actors,
	LevelId levelId,
	ubyte areaIndex,
	std::vector<string> &warnings
) {
	AreaDataPointerLocations locations;

	writeWord( rom, 0x1F080000 | ((uint)areaIndex << 8) );
	locations.geoLayout = (uint)rom.tellp();
	writeWordRaw( rom, 0 ); // placeholder

	if( actors.empty() ) {
		uint i = 0;
		if( levelSpecs.marioSpawn.area == areaIndex ) {
			PlacedObject{
				ALL_ACTS,
				0,
				{ levelSpecs.marioSpawn.x, levelSpecs.marioSpawn.y, levelSpecs.marioSpawn.z },
				{ 0, angleToShort( levelSpecs.marioSpawn.yaw ), 0 },
				{ 0, 10, 0, 0 },
				0x13002F74
			}.writeToRom( rom );
			i++;
		}
		while( i++ < areaSpecs.numObjects ) {
			PlacedObject::Default().writeToRom( rom );
		}

		i = 0;
		if( areaSpecs.numWarps > 0 ) {
			NormalWarp{ 241, LevelId::CastleInterior, 1, 100, false }.writeToRom( rom );
			i++;
		}
		if( areaSpecs.numWarps > 1 ) {
			NormalWarp{ 240, LevelId::CastleInterior, 1, 50, false }.writeToRom( rom );
			i++;
		}
		if( areaSpecs.numWarps > 2 && levelSpecs.marioSpawn.area == areaIndex ) {
			NormalWarp{ 10, levelId, areaIndex, 10, false }.writeToRom( rom );
			i++;
		}

		while( i++ < areaSpecs.numObjects ) {
			NormalWarp::Default().writeToRom( rom );
		}

		for( i = 0; i < areaSpecs.numInstantWarps; i++ ) {
			InstantWarp{
				(CollisionType)(0x1B + i),
				areaIndex,
				{ 0, 0, 0 }
			}.writeToRom( rom );
		}
	} else {
		uint i = 0;
		for( const PlacedObject &object : actors.objects ) {
			if( i >= areaSpecs.numObjects ) {
				if( object.isEmpty() ) continue;
				warnings.push_back( "Some existing objects were removed because of your object limit ("s + levelSpecs.name + ", area " + std::to_string( (uint)areaIndex ) + ")"  );
				break;
			}
			object.writeToRom( rom );
			i++;
		}
		while( i++ < areaSpecs.numObjects ) {
			PlacedObject::Default().writeToRom( rom );
		}

		i = 0;
		for( const NormalWarp &warp : actors.warps ) {
			if( i >= areaSpecs.numWarps ) {
				if( warp.isEmpty() ) continue;
				warnings.push_back( "Some existing warps were removed because of your warp limit ("s + levelSpecs.name + ", area " + std::to_string( (uint)areaIndex ) + ")" );
				break;
			}
			warp.writeToRom( rom );
			i++;
		}
		while( i++ < areaSpecs.numWarps ) {
			NormalWarp::Default().writeToRom( rom );
		}

		i = 0;
		bool usedInstantWarp[4] = { false, false, false, false };
		for( const InstantWarp &warp : actors.instantWarps ) {
			if( warp.trigger > CollisionType::InstantWarp4 || warp.trigger < CollisionType::InstantWarp1 ) continue;

			if( i >= areaSpecs.numInstantWarps ) {
				if( warp.areaIndex == areaIndex && warp.offset.zero() ) continue;
				warnings.push_back( "Some existing instant warps were removed because of your instant warp limit. ("s + levelSpecs.name + ", area " + std::to_string( (uint)areaIndex ) + ")" );
				break;
			}
			warp.writeToRom( rom );
			usedInstantWarp[(ubyte)warp.trigger - 0x1B] = true;
			i++;
		}
		for( uint j = 0; j < 4 && j < areaSpecs.numInstantWarps; j++ ) {
			if( !usedInstantWarp[j] ) {
				InstantWarp{
					(CollisionType)(0x1B + j),
					areaIndex,
					{ 0, 0, 0 }
				}.writeToRom( rom );
				i++;
			}
		}

		for( const PaintingWarp &warp : actors.paintingWarps ) {
			warp.writeToRom( rom );
		}
	}

	writeWordRaw( rom, 0x2E080000_be32 );
	locations.collision = (uint)rom.tellp();
	writeWordRaw( rom, 0 ); // placeholder

	writeWords( rom, { 0x36080000, (uint)areaSpecs.music << 16 });
	writeWord( rom, 0x31040000 | (uint)areaSpecs.terrainType );
	writeWordRaw( rom, 0x20040000_be32 );

	return locations;
}

LevelScriptLocations writeLevelScript(
	std::ostream &rom,
	LevelId levelId,
	const LevelSpecs &specs,
	const std::array<AreaActors,8> &areaActors,
	std::vector<string> &warnings,
	size_t segment19Start,
	size_t segment0eStart
) {
	size_t segment0eHead = rom.tellp();
	rom.seekp( segment19Start );

	bool usesBackground = false;
	bool hasEnvfx = false;

	HashSet<ushort> usedAreas;
	const std::map<ushort,AreaSpecs> &areaMap = Blueprint::current()->areas();
	for( auto i = areaMap.lower_bound( AREA_ID( levelId, 0 ) ); i != areaMap.upper_bound( AREA_ID( levelId, 7 ) ); i++ ) {
		const AreaSpecs &area = i->second;

		usesBackground |= (area.backgroundImage != BackgroundImage::None);
		hasEnvfx |= area.environmentEffect != EnvironmentEffect::None;
		usedAreas.insert( i->first );
	}

	writeWordRaw( rom, 0x1B040000_be32 );
	loadSegments( rom, specs, levelId, hasEnvfx, usesBackground );
	writeWordRaw( rom, 0x1D040000_be32 );

	writeRaw<uint>( rom, { 0x250C0001_be32, 0x00000001_be32, 0x13002EC0_be32 });
	loadModelsFromSegments( rom, specs );

	/* Global models from segment 0x16 */
	loadModelFromGeoLayout( rom, 0x17, 0x16000FE8 ); // Bubbly Tree
	loadModelFromGeoLayout( rom, 0x18, 0x16001000 ); // Spiky Tree
	loadModelFromGeoLayout( rom, 0x19, 0x16001018 ); // Snowy Tree
	loadModelFromGeoLayout( rom, 0x1B, 0x16001048 ); // Palm Tree
	loadModelFromGeoLayout( rom, 0x1D, 0x160004D0 ); // Wooden Door
	loadModelFromGeoLayout( rom, 0x1F, 0x160005F8 ); // Metal Door
	loadModelFromGeoLayout( rom, 0x20, 0x1600068C ); // Stone Door
	loadModelFromGeoLayout( rom, 0x22, 0x160007B4 ); // 0 Star Door
	loadModelFromGeoLayout( rom, 0x23, 0x16000868 ); // 1 Star Door
	loadModelFromGeoLayout( rom, 0x24, 0x1600091C ); // 3 Star Door
	loadModelFromGeoLayout( rom, 0x25, 0x160009D0 ); // Key Door
	loadModelFromGeoLayout( rom, 0x26, 0x160003A8 ); // Castle Door
	loadModelFromGeoLayout( rom, 0x27, 0x160004D0 ); // Wooden Door (Duplicate)
	loadModelFromGeoLayout( rom, 0x29, 0x160005F8 ); // Metal Door (Duplicate)
	loadModelFromGeoLayout( rom, 0x30, 0x16000388 ); // Warp Pipe

	LevelScriptLocations locations;
	for( const auto &i : specs.simpleObjects ) {
		loadModelFromGeoLayout( rom, i.second.modelId, 0 );
		locations.objectLocations[i.first] = (uint)rom.tellp() - 4;
	}

	for( const auto &i : specs.advancedObjects ) {
		loadModelFromGeoLayout( rom, i.second.modelId, 0 );
		locations.objectLocations[i.first] = (uint)rom.tellp() - 4;
	}

	writeWordRaw( rom, 0x05080000_be32 );
	writeWord( rom, 0x0E000000 + (uint)( segment0eHead - segment0eStart ) );

	const size_t segment19Head = rom.tellp();
	rom.seekp( segment0eHead );

	for( ushort i : usedAreas ) {
		locations.areaLocations[i] = writeAreaScript( rom, specs, areaMap.at( i ), areaActors[i & 7], levelId, i & 7, warnings );
	}

	writeWordRaw( rom, 0x05080000_be32 );
	writeWord( rom, 0x19000000 + (uint)( segment19Head - segment19Start ) );
	segment0eHead = rom.tellp();
	rom.seekp( segment19Head );

	writeWordRaw( rom, 0x1E040000_be32 );

	writeHalfwordRaw( rom, 0x2B0C_be16 );
	rom.put( specs.marioSpawn.area );
	rom.put( 0 );
	writeHalfword( rom, specs.marioSpawn.yaw );
	writeHalfword( rom, (ushort)specs.marioSpawn.x );
	writeHalfword( rom, (ushort)specs.marioSpawn.y );
	writeHalfword( rom, (ushort)specs.marioSpawn.z );

	writeRaw<uint>( rom, {
		0x11080000_be32, 0x8024BCD8_be32,
		0x12080001_be32, 0x8024BCD8_be32,
		0x1C040000_be32,
		0x04040001_be32,
		0x02040000_be32
	});

	rom.seekp( segment0eHead );
	return locations;
}
