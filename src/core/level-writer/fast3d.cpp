#include "src/core/level-writer/fast3d.hpp"

#include <map>
#include <unordered_map>
#include <utility>
#include "src/polyfill/filestream.hpp"
#include "src/core/util.hpp"
#include "src/core/import/model-optimizer.hpp"
#include "src/core/level-writer/texture.hpp"
#include "src/core/level-writer/f3d-macros.hpp"
#include "src/core/asm-extensions.hpp"

static const uint s_renderModes1[7] = {
	0x00442230 >> 3, // G_RM_ZB_OPA_SURF
	0x00442078 >> 3, // G_RM_AA_ZB_OPA_SURF,
	0x00442D58 >> 3, // G_RM_AA_ZB_OPA_DECAL
	0x00442478 >> 3, // G_RM_AA_ZB_OPA_INTER
	0x00443078 >> 3, // G_RM_AA_ZB_TEX_EDGE
	0x004049D8 >> 3, // G_RM_AA_ZB_XLU_SURF
	0x00404DD8 >> 3  // G_RM_AA_ZB_XLU_DECAL
};

static const uint s_renderModes2[7] = {
	0x00112230 >> 3, // G_RM_ZB_OPA_SURF2
	0x00112078 >> 3, // G_RM_AA_ZB_OPA_SURF2
	0x00112D58 >> 3, // G_RM_AA_ZB_OPA_DECAL2
	0x00112478 >> 3, // G_RM_AA_ZB_OPA_INTER2
	0x00113078 >> 3, // G_RM_AA_ZB_TEX_EDGE2
	0x001049D8 >> 3, // G_RM_AA_ZB_XLU_SURF2
	0x00104DD8 >> 3  // G_RM_AA_ZB_XLU_DECAL2
};

static constexpr uint G_RM_PASS = 0x0C080000 >> 3;
static constexpr uint G_RM_FOG_PRIM_A = 0xC8000000 >> 3;

struct ModelData {
	string material;
	SegmentedPointer verticesLocation;
	std::vector<PatchLoadInfo> patches;

	ModelData( string mat, SegmentedPointer vl, std::vector<PatchLoadInfo> &&p ) :
		material( mat ),
		verticesLocation( vl ),
		patches( p ) {}

	ModelData( const ModelData &other ) = delete;

	ModelData( ModelData &&other ) :
		material( std::move( other.material ) ),
		verticesLocation( other.verticesLocation ),
		patches( std::move( other.patches ) ) {}

};

#define POSITION_SEGMENTED (0x0E000000 | ((uint)rom.tellp() - mainSegmentStart))

static inline bool supportsBinaryAlpha( DrawingLayer layer, bool cutoutDecal ) {
	return layer >= DrawingLayer::Alphatest || ( cutoutDecal && layer == DrawingLayer::Decal );
}

static inline bool shouldDither( const MaterialSpecs &mat, bool cutoutDecal ) {
	return mat.ditheredAlpha && mat.opacity != 0xFF && supportsBinaryAlpha( mat.layer, cutoutDecal );
}

static void writeFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	uint areaIndexOrObjectId,
	const string &context,
	const fs::path &areaOrObjectDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	std::map<string,uint> &asmRefs,
	TextureFiltering defaultTextureFiltering,
	bool isObject,
	RomAddress &animationHead,
	bool cutoutDecal,
	[[maybe_unused]] std::vector<string> &warnings
) {
	alignHead( rom, 8 );
	std::multimap<float,ModelData> layers[7];
	for( const auto &i : materials ) {
		const string &name = i.first;
		const MaterialSpecs &mat = i.second;

		if( !mat.visible || !mat.textureInfo.has_value() ) continue;

		ModelLoadInfo model;
		FileReadStream modelFile( (areaOrObjectDir / ( name + ".model" )).u8string() );
		model.load( modelFile );

		alignHead( rom, 8 );
		const SegmentedPointer verticesLocation = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);
		rom.write( (const char*)model.vertexData.data(), 16 * model.vertexData.size() );

		ModelData modelData( name, verticesLocation, std::move( model.loadData ) );
		layers[(uint)mat.layer].insert( std::make_pair( -model.priority, std::move( modelData ) ) );
	}

	std::map<string,SegmentedPointer> materialDls;
	for( int i = 0; i < 7; i++ ) {
		if( layers[i].empty() ) continue;

		const DrawingLayer layer = (DrawingLayer)i;
		for( const auto &j : layers[i] ) {
			std::optional<string> objectName;
			if( isObject ) objectName = ""s; // don't actually need the name

			const ModelData &model = j.second;
			const MaterialSpecs &specs = materials.at( model.material );
			const TextureLocation &texInfo = textureLocations.at({ specs.textureInfo.value().hash, specs.format, model.material, areaIndexOrObjectId, objectName, specs.asmRefs.texData });
			const string refPrefix = context + "_" + toAlphanumeric( model.material );

			assert( materialDls.count( model.material ) == 0 );
			alignHead( rom, 8 );
			materialDls[model.material] = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);

			if( specs.doubleSided ) {
				CLEAR_GEOMETRY_FLAGS( G_CULL_BACK );
			}

			if( specs.opacity != 0xFF && supportsBinaryAlpha( layer, cutoutDecal ) ) {
				if( specs.asmRefs.opacity ) {
					asmRefs[refPrefix + "_opacity_byte"] = POSITION_SEGMENTED + 7;
				}
				SET_ENV_COLOUR( 0xFFFFFF00 | specs.opacity )
			}

			if( specs.asmRefs.shadePtr ) {
				asmRefs[refPrefix + "_shade_dark_pointer"] = POSITION_SEGMENTED + 4;
				asmRefs[refPrefix + "_shade_light_pointer"] = POSITION_SEGMENTED + 12;
			}
			LOAD_SHADE_VALUES( shadeLocations.at( specs.shadingRef ) )

			if( specs.asmRefs.texPtr ) {
				asmRefs[refPrefix + "_texture_pointer"] = POSITION_SEGMENTED + 4;
			}

			LOAD_TEXTURE( rom, texInfo.pointer, specs.format, specs.textureInfo.value().width, specs.textureInfo.value().height, specs.hwrap, specs.vwrap );
			if( specs.reflection ) {
				SET_GEOMETRY_FLAGS( G_TEXTURE_GEN );
				SET_TILE_SIZE( 0, texInfo.width, texInfo.height );
				SET_TEXTURE_SCALE( texInfo.width - 2, texInfo.height - 2, true );
			} else {
				writeTextureAnimation(
					rom,
					isObject ? (ubyte)0xFF : (ubyte)(1 << areaIndexOrObjectId),
					specs.animation,
					specs.textureInfo.value().width,
					specs.textureInfo.value().height,
					specs.hwrap,
					specs.vwrap,
					animationHead,
					POSITION_SEGMENTED
				);
				SET_TILE_SIZE( 0, texInfo.width, texInfo.height );
			}

			for( const PatchLoadInfo &patch : model.patches ) {
				LOAD_VTX( (uint)patch.numVertices, model.verticesLocation + ( 16 * patch.indexOffset ) );
				for( const std::array<ubyte,3> &face : patch.triangles ) {
					RENDER_TRI( face )
				}
			}

			RDP_PIPE_SYNC

			if( specs.reflection ) {
				CLEAR_GEOMETRY_FLAGS( G_TEXTURE_GEN );
				SET_TEXTURE_SCALE( texInfo.width - 2, texInfo.height - 2, false );
			}

			if( specs.opacity != 0xFF && layer >= DrawingLayer::Translucent ) {
				SET_ENV_COLOUR( 0xFFFFFFFF )
			}

			if( specs.filtering != defaultTextureFiltering ) {
				SET_OTHERMODE_H( 12, 2, defaultTextureFiltering );
			}

			if( specs.doubleSided ) {
				SET_GEOMETRY_FLAGS( G_CULL_BACK );
			}

			END_DL
		}
	}

	for( int i = 0; i < 7 ; i++ ) {
		if( layers[i].empty() ) continue;
		const DrawingLayer layer = (DrawingLayer)i;

		alignHead( rom, 8 );
		const SegmentedPointer fast3dLocation = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);
		writeWordAt( rom, displayListLocations.at( layer ), fast3dLocation );

		RDP_PIPE_SYNC
		SET_GEOMETRY_FLAGS( 0 ) //TODO: is this necessary? Seems like it should do nothing
		DISABLE_TD( 1 )
		RDP_TILE_SYNC
		RDP_LOAD_SYNC

		if( fog.enabled ) {
			SET_CYCLE_MODE( G_CYC_2_CYCLE );
			SET_RENDER_MODE( G_RM_FOG_PRIM_A, s_renderModes2[(int)layer] )
			SET_FOG_COLOUR( fog.colour )
			SET_FOG_DISTANCE( rom, fog.fadeStartDistance, fog.fadeEndDistance );
			SET_GEOMETRY_FLAGS( G_FOG )
		}

		for( const auto &j : layers[i] ) {
			const ModelData &model = j.second;
			const MaterialSpecs &specs = materials.at( model.material );

			if( !supportsBinaryAlpha( (DrawingLayer)i, cutoutDecal ) ) {
				if( fog.enabled ) {
					SET_COMBINE2( COMBINER_OPAQUE, COMBINER2_PASSTRHOUGH )
				} else {
					SET_COMBINE( COMBINER_OPAQUE )
				}
			} else {
				if( fog.enabled ) {
					SET_COMBINE2( COMBINER_TEXTURE_ALPHA, COMBINER2_PASSTRHOUGH )
				} else {
					SET_COMBINE( COMBINER_TEXTURE_ALPHA )
				}
			}

			SET_OTHERMODE_L( 0, 2, shouldDither( specs, cutoutDecal ) ? 3 : 0 );
			SET_OTHERMODE_H( 12, 2, specs.filtering );

			const bool textureHasAlpha = specs.textureInfo.value().hasFullAlpha || specs.textureInfo.value().hasEmptyPixels;
			if( specs.opacity != 0xFF && supportsBinaryAlpha( layer, cutoutDecal ) ) {
				if( textureHasAlpha ) {
					if( fog.enabled ) {
						SET_COMBINE2( COMBINER_TEXTURE_AND_ENVIRONMENT_ALPHA, COMBINER2_PASSTRHOUGH )
					} else {
						SET_COMBINE( COMBINER_TEXTURE_AND_ENVIRONMENT_ALPHA )
					}
				} else {
					if( fog.enabled ) {
						SET_COMBINE2( COMBINER_ENVIRONMENT_ALPHA, COMBINER2_PASSTRHOUGH )
					} else {
						SET_COMBINE( COMBINER_ENVIRONMENT_ALPHA )
					}
				}
			}

			DL_JAL( materialDls.at( model.material ) );
		}

		SET_OTHERMODE_L( 0, 2, 0 );

		if( fog.enabled ) {
			SET_CYCLE_MODE( G_CYC_1_CYCLE );
			SET_RENDER_MODE( s_renderModes1[(int)layer], s_renderModes2[(int)layer] )
			CLEAR_GEOMETRY_FLAGS( G_FOG )
		}

		SET_COMBINE( COMBINER_SHADE_ONLY ) //TODO: is this actually needed?
		DISABLE_TD( 0 )
		END_DL
	}

	bool usesPrimitiveColour = false;
	for( int i = (int)DrawingLayer::Translucent2; i <= (int)DrawingLayer::Shadow2 ; i++ ) {
		if( displayListLocations.count( (DrawingLayer)i ) == 0 ) continue;
		const DrawingLayer layer = (DrawingLayer)(i & 0xF);
		usesPrimitiveColour = true;

		alignHead( rom, 8 );
		const SegmentedPointer fast3dLocation = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);
		writeWordAt( rom, displayListLocations.at( (DrawingLayer)i ), fast3dLocation );

		RDP_PIPE_SYNC
		SET_GEOMETRY_FLAGS( 0 ) //TODO: is this necessary? Seems like it should do nothing
		DISABLE_TD( 1 )
		RDP_TILE_SYNC
		RDP_LOAD_SYNC

		SET_CYCLE_MODE( G_CYC_2_CYCLE );
		if( fog.enabled ) {
			SET_RENDER_MODE( G_RM_FOG_PRIM_A, s_renderModes2[(int)layer] )
			SET_FOG_COLOUR( fog.colour )
			SET_FOG_DISTANCE( rom, fog.fadeStartDistance, fog.fadeEndDistance );
			SET_GEOMETRY_FLAGS( G_FOG )
		} else {
			SET_RENDER_MODE( G_RM_PASS, s_renderModes2[(int)layer] )
		}

		for( int j = 0; j < 7; j++ ) {
			const bool usesLayer6 = (DrawingLayer)j == DrawingLayer::Decal || (DrawingLayer)j == DrawingLayer::Shadow;
			if( usesLayer6 != ((DrawingLayer)i == DrawingLayer::Shadow2) ) continue;

			for( const auto &k : layers[j] ) {
				const ModelData &model = k.second;
				const MaterialSpecs &specs = materials.at( model.material );

				const bool dither = specs.ditheredAlpha && (
					(DrawingLayer)j >= DrawingLayer::Translucent ||
					specs.opacity != 0xFF
				);

				SET_OTHERMODE_L( 0, 2, dither ? 3 : 0 );
				SET_OTHERMODE_H( 12, 2, specs.filtering );

				const bool textureHasAlpha = specs.textureInfo.value().hasFullAlpha || specs.textureInfo.value().hasEmptyPixels;
				if( specs.opacity != 0xFF && supportsBinaryAlpha( (DrawingLayer)j, cutoutDecal ) ) {
					if( textureHasAlpha ) {
						SET_COMBINE2( COMBINER_TEXTURE_AND_ENVIRONMENT_ALPHA, COMBINER2_MIX_PRIMITIVE_ALPHA )
					} else {
						SET_COMBINE2( COMBINER_ENVIRONMENT_AND_PRIMITIVE_ALPHA, COMBINER2_PASSTRHOUGH )
					}
				} else if( textureHasAlpha ) {
					SET_COMBINE2( COMBINER_TEXTURE_AND_PRIMITIVE_ALPHA, COMBINER2_PASSTRHOUGH )
				} else {
					SET_COMBINE2( COMBINER_PRIMITIVE_ALPHA, COMBINER2_PASSTRHOUGH )
				}

				DL_JAL( materialDls.at( model.material ) );
			}
		}

		SET_OTHERMODE_L( 0, 2, 0 );
		SET_CYCLE_MODE( G_CYC_1_CYCLE );
		SET_RENDER_MODE( s_renderModes1[(int)layer], s_renderModes2[(int)layer] )

		if( fog.enabled ) {
			CLEAR_GEOMETRY_FLAGS( G_FOG )
		}

		if( usesPrimitiveColour ) {
			writeRaw<uint>( rom, { 0xFA000000_be32, 0xFFFFFFFF });
		}

		SET_COMBINE( COMBINER_SHADE_ONLY ) //TODO: is this actually needed?
		DISABLE_TD( 0 )
		END_DL
	}

}


void writeAreaFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	ubyte areaIndex,
	const fs::path &areaDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	RomAddress &animationHead,
	bool cutoutDecal,
	std::vector<string> &warnings
) {
	writeFast3d(
		rom,
		fog,
		(uint)areaIndex,
		"area_" + std::to_string( (uint)areaIndex ),
		areaDir,
		mainSegmentStart,
		shadeLocations,
		textureLocations,
		displayListLocations,
		materials,
		asmRefs,
		defaultTextureFiltering,
		false,
		animationHead,
		cutoutDecal,
		warnings
	);
}

void writeObjectFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	objectId objectId,
	const fs::path &objectDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	const string &objectName,
	RomAddress &animationHead,
	bool cutoutDecal,
	std::vector<string> &warnings
) {
	writeFast3d(
		rom,
		fog,
		objectId,
		"object_" + toAlphanumeric( objectName ),
		objectDir,
		mainSegmentStart,
		shadeLocations,
		textureLocations,
		displayListLocations,
		materials,
		asmRefs,
		defaultTextureFiltering,
		true,
		animationHead,
		cutoutDecal,
		warnings
	);
}

#undef POSITION_SEGMENTED
