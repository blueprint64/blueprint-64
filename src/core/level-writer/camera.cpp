#include "src/core/level-writer/camera.hpp"

#include <cassert>
#include "src/core/asm-extensions.hpp"

#define ROM_TO_VIRTUAL( romAddr ) (((romAddr) - segmentStart) + 0x80420000)
#define ASM_J( funcPtr ) writeWord( rom, 0x08000000 | ((funcPtr & 0x3FFFFFFFu) >> 2) )
#define ASM_JAL( funcPtr ) writeWord( rom, 0x0C000000 | ((funcPtr & 0x3FFFFFFFu) >> 2) )

inline static void loadWordToStack(
	std::ostream &rom,
	const CameraModulePropertyValue &value,
	short stackOffset
) {
	uint word = 0;
	switch( value.type ) {
		case ModulePropertyType::Integer:
			word = reinterpret_cast<const uint&>( value.signedValue ); break;
		case ModulePropertyType::Float:
			word = reinterpret_cast<const uint&>( value.floatValue ); break;
		case ModulePropertyType::Boolean:
			word = value.booleanValue ? 1u : 0u; break;
		case ModulePropertyType::Pointer:
			word = value.unsignedValue; break;
		case ModulePropertyType::Angle: {
			word = (ushort)((uint)( (double)value.floatValue * (double)0x10000 / 360.0 ) % 0x10000);
			if( word > 0x7FFF ) {
				word |= 0xFFFF0000u;
			}
			break;
		}
		default: assert( false );
	}

	writeHalfwordRaw( rom, 0x3C01_be16 ); // LUI AT, <value>
	writeHalfword( rom, (ushort)(word >> 16) );
	writeHalfwordRaw( rom, 0x3421_be16 ); // ORI AT, AT, <value>
	writeHalfword( rom, (ushort)(word & 0xFFFFu) );
	writeHalfwordRaw( rom, 0xAFA1_be16 ); // SW AT, <value> (SP)
	writeSignedHalfword( rom, stackOffset );
}

static void writeFunction(
	std::ostream &rom,
	const CameraTrigger &trigger,
	const std::map<Uuid,CustomCamera> &cameraModules
) {
	switch( trigger.type ) {
		case CameraTriggerType::Vanilla: {
			const VanillaCameraOptions &params = std::get<VanillaCameraOptions>( trigger.options );
			switch( params.mode ) {
				case CameraPreset::BossFight: {
					writeHalfwords( rom, {
						0x3C05, (ushort)(params.ext.behaviour >> 16), // LUI A1, (behaviour_upper)
						0x34A5, (ushort)(params.ext.behaviour & 0xFFFF) // ORI A1, A1, (behaviour_lower)
					});
					ASM_J( AsmExtensions::bossCameraTriggerFuncPtr );
					writeHalfwords( rom, { 0x3406, (ushort)params.frames }); // ORI A2, R0, (frames)
					break;
				}
				case CameraPreset::InnerRadial:
				case CameraPreset::OuterRadial: {
					const uint poiX = reinterpret_cast<const int&>( params.ext.centre[0] );
					const uint poiZ = reinterpret_cast<const int&>( params.ext.centre[1] );

					writeHalfwords( rom, { 0x3405, (ushort)params.mode }); // ORI A1, R0, (mode)
					writeHalfwords( rom, { 0x3406, (ushort)params.frames }); // ORI A2, R0, (frames)
					writeHalfwords( rom, { 0x3C01, (ushort)(poiX >> 16) }); // LUI AT, (poiX upper bits)
					if( (poiX & 0xFFFF) != 0 ) {
						writeHalfwords( rom, { 0x3421, (ushort)(poiX & 0xFFFF) }); // ORI AT, AT, (poiX lower bits)
					}
					writeWordRaw( rom, 0x44816000_be32 ); // MTC1 AT, F12
					writeHalfwords( rom, { 0x3C01, (ushort)(poiZ >> 16) }); // LUI AT, (poiZ upper bits)
					if( (poiZ & 0xFFFF) != 0 ) {
						writeHalfwords( rom, { 0x3421, (ushort)(poiZ & 0xFFFF) }); // ORI AT, AT, (poiZ lower bits)
					}
					ASM_J( AsmExtensions::radialCameraTriggerFuncPtr );
					writeWordRaw( rom, 0x44817000_be32 ); // MTC1 AT, F14
					break;
				}
				case CameraPreset::ElasticPivot: {
					writeHalfwords( rom, { 0x3405, reinterpret_cast<const ushort &>( params.ext.pivot[0] ) }); // ORI A1, R0, (xPivot)
					writeHalfwords( rom, { 0x3406, reinterpret_cast<const ushort &>( params.ext.pivot[1] ) }); // ORI A2, R0, (yPivot)
					ASM_J( 0x8028D44C ); // set_camera_mode_fixed
					writeHalfwords( rom, { 0x3407, reinterpret_cast<const ushort &>( params.ext.pivot[2] ) }); // ORI A3, R0, (zPivot)
					break;
				}
				default: {
					writeHalfwords( rom, { 0x3405, (ushort)params.mode }); // ORI A1, R0, (mode)
					if( params.mode == CameraPreset::SemiCardinal ) {
						writeHalfwords( rom, { 0x3406, params.ext.baseAngle }); // ORI A2, R0, (baseAngle)
					}
					ASM_J( AsmExtensions::transitionCameraTriggerFuncPtr );
					writeHalfwords( rom, { 0x3407, (ushort)params.frames }); // ORI A3, R0, (frames)
					break;
				}
			}
			break;
		}
		case CameraTriggerType::Module: {
			const ModuleCameraOptions &params = std::get<ModuleCameraOptions>( trigger.options );
			const CustomCamera &camera = cameraModules.at( params.moduleId );
			const CameraModule &module = *camera.module;

			const short stackSize = (((4 * (int)module.parameters.size()) + 0x14 + 7) >> 3) << 3;

			writeHalfwordRaw( rom, 0x27BD_be16 ); // ADDIU SP, SP, <value>
			writeSignedHalfword( rom, -stackSize );
			writeHalfwordRaw( rom, 0xAFBF_be16 ); // SW RA, <value> (SP)
			writeSignedHalfword( rom, stackSize - 4 );

			short stackOffset = 0x10;
			for( const AsmModuleProperty &prop : module.parameters ) {
				const CameraModulePropertyValue *maybeValue = Util::tryGetValue( params.moduleParams, prop.name );
				if( maybeValue != nullptr && maybeValue->type == prop.type ) {
					loadWordToStack( rom, *maybeValue, stackOffset );
				} else {
					CameraModulePropertyValue value;
					value.type = prop.type;
					switch( prop.type ) {
						case ModulePropertyType::Integer:
							value.signedValue = std::get<int>( prop.defaultValue ); break;
						case ModulePropertyType::Pointer:
							value.unsignedValue = std::get<uint>( prop.defaultValue ); break;
						case ModulePropertyType::Float:
						case ModulePropertyType::Angle:
							value.floatValue = std::get<float>( prop.defaultValue ); break;
						case ModulePropertyType::Boolean:
							value.booleanValue = std::get<bool>( prop.defaultValue ); break;
						default: assert( false );
					}
					loadWordToStack( rom, value, stackOffset );
				}
				stackOffset += 4;
			}

			writeHalfwordRaw( rom, 0x2405_be16 ); // ADDIU A1, R0, <value>
			writeSignedHalfword( rom, camera.cameraId );
			ASM_JAL( AsmExtensions::moduleCameraTriggerFuncPtr );
			writeHalfwordRaw( rom, 0x2406_be16 ); // ADDIU A2, R0, <value>
			writeSignedHalfword( rom, params.frames );

			writeHalfwordRaw( rom, 0x8FBF_be16 ); // LW RA, <value> (SP)
			writeSignedHalfword( rom, stackSize - 4 );
			writeWordRaw( rom, 0x03E00008_be32 ); // JR RA
			writeHalfwordRaw( rom, 0x27BD_be16 ); // ADDIU SP, SP, <value>
			writeSignedHalfword( rom, stackSize );

			break;
		}
		default: assert( false );
	}
}

struct PendingTrigger {
	RomAddress pointerLocation;
	const CameraTrigger *trigger;
};

void writeLevelCameraTriggers(
	std::ostream &rom,
	LevelId levelId,
	const std::map<Uuid,CustomCamera> &cameraModules,
	const std::multimap<ubyte,CameraTrigger> &cameraTriggers,
	std::map<string,uint> &cameraTriggerRefs,
	RomAddress segmentStart
) {
	writeWordAt( rom, 0xE9CB0u + ((uint)levelId << 2), ROM_TO_VIRTUAL( (uint)rom.tellp() ) );

	std::vector<PendingTrigger> pendingTriggers;
	pendingTriggers.reserve( cameraTriggers.size() );

	for( const auto &i : cameraTriggers ) {
		const ubyte area = i.first;
		const CameraTrigger &trigger = i.second;

		writeRaw<ubyte>( rom, { area, 0, 0, 0 });
		if( trigger.type == CameraTriggerType::Custom ) {
			const CustomCameraOptions &params = std::get<CustomCameraOptions>( trigger.options );
			cameraTriggerRefs[params.functionLabel] = (uint)rom.tellp();
		} else {
			pendingTriggers.push_back({ (uint)rom.tellp(), &trigger });
		}
		writeWordRaw( rom, 0 );
		trigger.centre.writeToRom( rom );
		trigger.radius.writeToRom( rom );
		writeHalfword( rom, trigger.angle );
		writeHalfwordRaw( rom, 0 );
	}

	writeRaw<ubyte>( rom, { 0xFF, 0, 0, 0 });
	writeWord( rom, AsmExtensions::defaultCameraTriggerFuncPtr );
	writeRaw<uint>( rom, { 0, 0, 0, 0 });

	writeRaw<uint>( rom, { 0, 0, 0, 0, 0, 0 });

	for( const PendingTrigger &pendingTrigger : pendingTriggers ) {
		writeWordAt( rom, pendingTrigger.pointerLocation, ROM_TO_VIRTUAL( (uint)rom.tellp() ) );
		writeFunction( rom, *pendingTrigger.trigger, cameraModules );
	}
}

static constexpr uint s_vanillaCameraFunction = 0x80287D30u;

uint writeDefaultCameraInitializer(
	std::ostream &rom,
	const CameraTrigger &defaultCamera,
	const std::map<Uuid,CustomCamera> &cameraModules,
	RomAddress segmentStart
) {
	if( defaultCamera.type == CameraTriggerType::Vanilla ) {
		switch( std::get<VanillaCameraOptions>( defaultCamera.options ).mode ) {
			case CameraPreset::ElasticPivot:
			case CameraPreset::BossFight:
			case CameraPreset::InnerRadial:
			case CameraPreset::OuterRadial:
			case CameraPreset::SemiCardinal:
				break;
			default:
				return s_vanillaCameraFunction;
		}
	}

	const uint helperFunction = ROM_TO_VIRTUAL( (uint)rom.tellp() );
	writeFunction( rom, defaultCamera, cameraModules );

	const uint cameraFunction = ROM_TO_VIRTUAL( (uint)rom.tellp() );
	writeRaw<uint>( rom, {
		0x10800005_be32, // BEQ A0, R0, <jump over 5 instructions>
		0x3C088033_be32, // LUI T0, 0x8033
		0x8D01DF28_be32, // LW AT, 0xDF28 (T0)
		0x14200002_be32, // BNE AT, R0, <jump over 2 instructions>
		0x00000000_be32  // NOP
	});
	ASM_J( s_vanillaCameraFunction );
	writeRaw<uint>( rom, {
		0xAD00DF28_be32, // SW R0, 0xDF28 (T0)
		0x27BDFFE8_be32, // ADDIU SP, SP, 0xFFE8
		0xAFBF0014_be32  // SW RA, 0x14 (SP)
	});
	ASM_JAL( s_vanillaCameraFunction );
	writeRaw<uint>( rom, {
		0xAFA50010_be32, // SW A1, 0x10 (SP)
		0x8FA40010_be32, // LW A0, 0x10 (SP)
		0x8C840018_be32  // LW A0, 0x18 (A0)
	});
	ASM_JAL( helperFunction );
	writeRaw<uint>( rom, {
		0xA0800000_be32, // SB R0, 0x0 (A0)
		0x00001025_be32, // OR V0, R0, R0
		0x8FBF0014_be32, // LW RA, 0x14 (SP)
		0x03E00008_be32, // JR RA
		0x27BD0018_be32  // ADDIU SP, SP, 0x18
	});

	return cameraFunction;
}

#undef ASM_J
#undef ASM_JAL
#undef ROM_TO_VIRTUAL
