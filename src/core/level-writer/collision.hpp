#ifndef SRC_CORE_LEVEL_WRITER_COLLISION_HPP_
#define SRC_CORE_LEVEL_WRITER_COLLISION_HPP_

#include <unordered_map>
#include <ostream>
#include <vector>
#include "src/core/data/area.hpp"
#include "src/core/data/material.hpp"
#include "src/types.hpp"
#include "src/polyfill/filesystem.hpp"

extern void writeAreaCollision(
	std::ostream &rom,
	const AreaSpecs &areaSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	const fs::path &areaDir
);

extern ushort writeObjectCollision(
	std::ostream &rom,
	const HashMap<string,MaterialSpecs> &materials,
	const fs::path &objectDir
);


#endif /* SRC_CORE_LEVEL_WRITER_COLLISION_HPP_ */
