#include "src/core/level-writer/geo.hpp"

#include <initializer_list>
#include <ostream>
#include <set>
#include "src/polyfill/filesystem.hpp"
#include "src/core/asm-extensions.hpp"

#define INIT_RENDER_AREA writeRaw<uint>( rom, { 0x0800000A_be32, 0x00A00078_be32, 0x00A00078_be32 } );
#define START_NODE writeWordRaw( rom, 0x04000000_be32 );
#define END_NODE writeWordRaw( rom, 0x05000000_be32 );
#define TOGGLE_DEPTH_BUFFER( enable ) writeWordRaw( rom, enable ? 0x0C010000_be32 : 0x0C000000_be32 );
#define SCENE_ORTHO_PROJECTION writeWordRaw( rom, 0x09000064_be32 );
#define BACKGROUND_IMAGE( background ) writeRaw<ubyte>( rom, { 0x19, 0x00, 0x00, (ubyte)background, 0x80, 0x27, 0x63, 0xD4 });
#define BACKGROUND_COLOUR( colour ) writeHalfwordRaw( rom, 0x1900_be16 ); writeHalfword( rom, colour.toShort() ); writeWordRaw( rom, 0 );
#define SET_CAMERA_FRUSTRUM( fov, near, far, dynamicFov ) \
	writeHalfwordRaw( rom, (dynamicFov) ? 0x0A01_be16 : 0x0A00_be16 ); \
	writeSignedHalfword( rom, fov ); \
	writeHalfwords( rom, { near, far }); \
	if( dynamicFov ) writeWordRaw( rom, 0x8029AA3C_be32 );
#define SET_CAMERA_PRESET( preset, position, focus, func ) \
	writeHalfwordRaw( rom, 0x0F00_be16 ); \
	writeHalfword( rom, (ushort)(preset) ); \
	writeSignedHalfwords( rom, { \
		position.x, position.y, position.z, \
		focus.x, focus.y, focus.z \
	}); \
	writeWord( rom, func );
#define LOAD_DISPLAY_LIST( segPtr, layer ) writeRaw<ubyte>( rom, { 0x15, (ubyte)layer, 0x00, 0x00 }); writeWord( segPtr );
#define CREATE_SCENE_NODE writeWordRaw( rom, 0x17000000_be32 );
#define LOAD_ENVFX( effect ) writeRaw<ubyte>( rom, { 0x18, 0x00, 0x00, (ubyte)effect, 0x80, 0x27, 0x61, 0xD0 });
#define LOAD_SCREEN_TRANSITION writeRaw<uint>( rom, { 0x18000000_be32, 0x802CD1E8_be32 });
#define INIT_WITH_CULLING_RADIUS( cullingRadius ) writeHalfwords( rom, { 0x2000, cullingRadius });
#define INIT_WITH_SHADOW( shadowSpecs ) \
	writeRaw<ubyte>( rom, { 0x16, 0x00, 0x00 }); \
	rom.put( (ubyte)shadowSpecs.shape ); \
	rom.put( 0x00 ); \
	rom.put( shadowSpecs.opacity ); \
	writeHalfword( rom, shadowSpecs.size );
#define END_GEO_LAYOUT writeWordRaw( rom, 0x01000000_be32 );
#define DL_RETURN writeWordRaw( rom, 0x03000000_be32 );
#define GEO_CALL_ASM( funcPtr, arg ) writeWords( rom, { 0x18000000 | ((uint)(arg) & 0xFFFF), funcPtr });
#define GEO_SWITCH( funcPtr, numCases ) writeWords( rom, { 0x0E000000 | ((uint)(arg) & 0xFF), funcPtr });
#define GEO_LOAD_DL( layer, dlMap ) \
	writeRaw<ubyte>( rom, { 0x15, (ubyte)(layer), 0x00, 0x00 }); \
	dlMap[layer] = (uint)rom.tellp(); \
	writeWordRaw( rom, 0 );
#define GEO_JAL( segPtr ) writeWords( rom, { 0x02010000, segPtr });
#define GEO_RETURN writeWordRaw( rom, 0x03000000_be32 );
#define GEO_START_SUB writeWordRaw( rom, 0x0B000000_be32 );

static void writeLayersNormal(
	std::ostream &rom,
	const HashMap<string,MaterialSpecs> &materials,
	HashMap<DrawingLayer,RomAddress> &dlMap
) {
	std::map<DrawingLayer, bool> usedLayers;
	for( const auto &i : materials ) {
		usedLayers[i.second.layer] = i.second.reflection;
	}

	for( const auto &i : usedLayers ) {
		if( i.second ) {
			GEO_CALL_ASM( AsmExtensions::geoSetLookatFuncPtr, i.first )
		}
		GEO_LOAD_DL( i.first, dlMap );
		if( i.second ) {
			GEO_CALL_ASM( AsmExtensions::revertLookatFuncPtr, i.first )
		}
	}
}

HashMap<DrawingLayer,RomAddress> writeAreaGeoLayout(
	std::ostream &rom,
	ushort areaId,
	short cameraId,
	uint cameraFuncPtr,
	const AreaSpecs &areaSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	HashMap<ushort,RomAddress> &waterDls,
	float worldScale,
	[[maybe_unused]] std::vector<string> &warnings
) {
	const ushort nearPlane = (ushort)((float)areaSpecs.nearClippingPlane / worldScale);
	const ushort farPlane = (ushort)((float)areaSpecs.farClippingPlane / worldScale);

	INIT_RENDER_AREA
	START_NODE
		TOGGLE_DEPTH_BUFFER( false )
		START_NODE
			SCENE_ORTHO_PROJECTION
			START_NODE
				if( areaSpecs.backgroundImage != BackgroundImage::None ) {
					BACKGROUND_IMAGE( areaSpecs.backgroundImage )
				} else {
					BACKGROUND_COLOUR( areaSpecs.backgroundColour )
				}
			END_NODE
		END_NODE
		TOGGLE_DEPTH_BUFFER( true )
		START_NODE
			SET_CAMERA_FRUSTRUM( areaSpecs.overrideFOV ? areaSpecs.fieldOfView : 45, nearPlane, farPlane, !areaSpecs.overrideFOV )
			START_NODE
				SET_CAMERA_PRESET( cameraId, areaSpecs.pauseCamera.position, areaSpecs.pauseCamera.focus, cameraFuncPtr )
				START_NODE
					HashMap<DrawingLayer,RomAddress> dlMap;
					writeLayersNormal( rom, materials, dlMap );
					if( !areaSpecs.waterBoxes.empty() ) {
						writeWordRaw( rom, 0x15050000_be32 );
						waterDls[areaId] = (uint)rom.tellp();
						writeWordRaw( rom, 0x00000000 );
					}
					CREATE_SCENE_NODE
					if( areaSpecs.environmentEffect != EnvironmentEffect::None ) {
						LOAD_ENVFX( areaSpecs.environmentEffect )
					}
				END_NODE
			END_NODE
		END_NODE
		TOGGLE_DEPTH_BUFFER( false )
		START_NODE
			LOAD_SCREEN_TRANSITION
		END_NODE
	END_NODE
	END_GEO_LAYOUT
	return dlMap;
}

static void writeObjectLayersTransparent(
	std::ostream &rom,
	const HashMap<string,MaterialSpecs> &materials,
	HashMap<DrawingLayer,RomAddress> &dlMap
) {
	bool usesLayer5 = false;
	bool usesLayer6 = false;

	bool usesReflections5 = false;
	bool usesReflections6 = false;

	for( const auto &i : materials ) {
		if( i.second.layer == DrawingLayer::Decal || i.second.layer == DrawingLayer::Shadow ) {
			usesLayer6 = true;
			usesReflections6 |= i.second.reflection;
		} else {
			usesLayer5 = true;
			usesReflections5 |= i.second.reflection;
		}
	}

	if( usesLayer5 ){
		START_NODE
			GEO_CALL_ASM( AsmExtensions::geoObjectAlphaFuncPtr, DrawingLayer::Translucent );
			if( usesReflections5 ) {
				GEO_CALL_ASM( AsmExtensions::geoSetLookatFuncPtr, DrawingLayer::Translucent );
			}
			writeRaw<ubyte>( rom, { 0x15, (ubyte)DrawingLayer::Translucent, 0x00, 0x00 });
			dlMap[DrawingLayer::Translucent2] = (uint)rom.tellp();
			writeWordRaw( rom, 0 );
			if( usesReflections5 ) {
				GEO_CALL_ASM( AsmExtensions::revertLookatFuncPtr, DrawingLayer::Translucent );
			}
		END_NODE
	}

	if( usesLayer6 ) {
		START_NODE
			GEO_CALL_ASM( AsmExtensions::geoObjectAlphaFuncPtr, DrawingLayer::Shadow );
			if( usesReflections6 ) {
				GEO_CALL_ASM( AsmExtensions::geoSetLookatFuncPtr, DrawingLayer::Shadow );
			}
			writeRaw<ubyte>( rom, { 0x15, (ubyte)DrawingLayer::Shadow, 0x00, 0x00 });
			dlMap[DrawingLayer::Shadow2] = (uint)rom.tellp();
			writeWordRaw( rom, 0 );
			if( usesReflections6 ) {
				GEO_CALL_ASM( AsmExtensions::revertLookatFuncPtr, DrawingLayer::Shadow );
			}
		END_NODE
	}
}

static inline HashMap<DrawingLayer,RomAddress> writeObjectGeoLayoutNormal(
	std::ostream &rom,
	const ObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	if( objectSpecs.shadow.enabled ) {
		INIT_WITH_SHADOW( objectSpecs.shadow )
	} else {
		INIT_WITH_CULLING_RADIUS( objectSpecs.cullingRadius.radius )
	}

	START_NODE
		writeLayersNormal( rom, materials, dlMap );
	END_NODE

	END_GEO_LAYOUT
	return dlMap;
}

static inline HashMap<DrawingLayer,RomAddress> writeObjectGeoLayoutWithVariableAlpha(
	std::ostream &rom,
	const ObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	RomAddress segmentStart
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	if( objectSpecs.shadow.enabled ) {
		INIT_WITH_SHADOW( objectSpecs.shadow )
	} else {
		INIT_WITH_CULLING_RADIUS( objectSpecs.cullingRadius.radius )
	}

	RomAddress opaqueGeo, transparentGeo, invisibleGeo;
	const uint ramOffset = 0x0E000000u - segmentStart;
	START_NODE
		writeWordRaw( rom, 0x0E000003_be32 );
		writeWord( rom, AsmExtensions::geoSwitchOpacityFuncPtr );
		START_NODE
			GEO_JAL( 0 )
			opaqueGeo = (uint)rom.tellp() - 0x4;
			GEO_JAL( 0 )
			transparentGeo = (uint)rom.tellp() - 0x4;
			GEO_JAL( 0 )
			invisibleGeo = (uint)rom.tellp() - 0x4;
		END_NODE
	END_NODE
	END_GEO_LAYOUT

	writeWordAt( rom, opaqueGeo, (uint)rom.tellp() + ramOffset );
	GEO_START_SUB
	START_NODE
		writeLayersNormal( rom, materials, dlMap );
	END_NODE
	GEO_RETURN

	writeWordAt( rom, transparentGeo, (uint)rom.tellp() + ramOffset );
	GEO_START_SUB
	START_NODE
		writeObjectLayersTransparent( rom, materials, dlMap );
	END_NODE
	GEO_RETURN

	writeWordAt( rom, invisibleGeo, (uint)rom.tellp() + ramOffset );
	GEO_START_SUB
	START_NODE
	END_NODE
	GEO_RETURN
	return dlMap;
}

HashMap<DrawingLayer,RomAddress> writeObjectGeoLayout(
	std::ostream &rom,
	const ObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	RomAddress segmentStart,
	[[maybe_unused]] std::vector<string> &warnings
) {
	if( objectSpecs.useObjectAlpha ) {
		return writeObjectGeoLayoutWithVariableAlpha( rom, objectSpecs, materials, segmentStart );
	} else {
		return writeObjectGeoLayoutNormal( rom, objectSpecs, materials );
	}
}

HashMap<DrawingLayer,RomAddress> writePartialObjectGeoLayout(
	std::ostream &rom,
	const PartialObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	[[maybe_unused]] std::vector<string> &warnings
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	for( const auto &i : materials ) {
		if( dlMap.count( i.second.layer ) == 0 ) {
			START_NODE
			if( objectSpecs.useObjectAlpha && i.second.layer >= DrawingLayer::Alphatest ) {
				writeRaw<uint>( rom, { 0x18000000_be32, 0x8029D924_be32 });
			}
			GEO_LOAD_DL( i.second.layer, dlMap );
			END_NODE
		}
	}

	DL_RETURN
	return dlMap;
}
