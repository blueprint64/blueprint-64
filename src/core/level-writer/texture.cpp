#include "src/core/level-writer/texture.hpp"

#include <cassert>
#include <QImage>
#include "src/core/util.hpp"

static void writeRGBA32( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x400 );
	for( size_t i = 0; i < numPixels; i++ ) {
		uint pixel = pixels[i];
		pixel = htonl( (pixel << 8) | (pixel >> 24) );
		rom.write( (char*)&pixel, 4 );
	}
}

static void writeRGBA16( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x800 );
	for( size_t i = 0; i < numPixels; i++ ) {
		const uint src = pixels[i];
		const ushort pixel = htons(
			( ( src >> 8 ) & 0xF800 ) |
			( ( src >> 5 ) & 0x7C0 ) |
			( ( src >> 2 ) & 0x3E ) |
			( src >> 31 )
		);

		rom.write( (char*)&pixel, 2 );
	}
}

static inline constexpr ubyte max( ubyte x, ubyte y ) {
	return x >= y ? x : y;
}

static inline constexpr ubyte min( ubyte x, ubyte y ) {
	return x < y ? x : y;
}

static inline ubyte getIntensity( uint pixel ) {
	const ubyte R = ( pixel >> 16 ) & 0xFF;
	const ubyte G = ( pixel >> 8 ) & 0xFF;
	const ubyte B = pixel & 0xFF;
	return max( R, max( G, B ) );
}

static inline ubyte getIntensityWithAlpha( uint pixel ) {
	return min( getIntensity( pixel ), ( pixel >> 24 ) & 0xFF );
}

static void writeI8( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x1000 );
	for( size_t i = 0; i < numPixels; i++ ) {
		rom.put( getIntensityWithAlpha( pixels[i] ) );
	}
}

static void writeI4( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x2000 );
	for( size_t i = 0; i < numPixels; i++ ) {
		const ubyte nibble1 = getIntensityWithAlpha( pixels[i] ) >> 4;
		const ubyte nibble2 = getIntensityWithAlpha( pixels[++i] ) >> 4;
		rom.put( (nibble1 << 4) | nibble2 );
	}
}

static void writeIA16( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x800 );
	for( size_t i = 0; i < numPixels; i++ ) {
		rom.put( getIntensity( pixels[i] ) );
		rom.put( pixels[i] >> 24 );
	}
}

static void writeIA8( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x1000 );
	for( size_t i = 0; i < numPixels; i++ ) {
		const ubyte nibble1 = getIntensity( pixels[i] ) >> 4;
		const ubyte nibble2 = pixels[i] >> 28;
		rom.put( (nibble1 << 4) | nibble2 );
	}
}

static void writeIA4( std::ostream &rom, const uint *pixels, size_t numPixels ) {
	assert( numPixels <= 0x2000 );
	for( size_t i = 0; i < numPixels; i++ ) {
		const ubyte nibble1 = ( ( getIntensity( pixels[i] ) >> 4 ) & 0xE ) | ( pixels[i] >> 31 );
		i++;
		const ubyte nibble2 = ( ( getIntensity( pixels[i] ) >> 4 ) & 0xE ) | ( pixels[i] >> 31 );
		rom.put( (nibble1 << 4) | nibble2 );
	}
}

static inline void writeTexture( std::ostream &rom, TextureFormat format, const uint *pixels, size_t numPixels ) {
	switch( format ) {
		case TextureFormat::RGBA32: return writeRGBA32( rom, pixels, numPixels );
		case TextureFormat::RGBA16: return writeRGBA16( rom, pixels, numPixels );
		case TextureFormat::IA16: return writeIA16( rom, pixels, numPixels );
		case TextureFormat::I8: return writeI8( rom, pixels, numPixels );
		case TextureFormat::IA8: return writeIA8( rom, pixels, numPixels );
		case TextureFormat::I4: return writeI4( rom, pixels, numPixels );
		case TextureFormat::IA4: return writeIA4( rom, pixels, numPixels );
		default: assert( false ); return;
	}
}

HashMap<TextureInstance,TextureLocation> writeTextures(
	std::ostream &rom,
	uint segmentRomAddr,
	const fs::path &textureDir,
	const HashSet<TextureInstance> &textures,
	std::map<std::string,uint> &asmRefs
) {
	HashMap<TextureInstance,TextureLocation> locations;
	for( const TextureInstance &texture : textures ) {
		alignHead( rom, 8 );
		QImage pixmap = QImage( (textureDir / (texture.hash + ".png")).u8string().c_str() ).convertToFormat( QImage::Format_ARGB32 );
		locations[texture] = {
			0x0E000000 + ( (uint)rom.tellp() - segmentRomAddr ),
			(ushort)pixmap.width(),
			(ushort)pixmap.height()
		};
		if( texture.asmRef ) {
			if( texture.objectName.has_value() ) {
				asmRefs["object_" + toAlphanumeric( texture.objectName.value() ) + "_" + toAlphanumeric( texture.material ) + "_texture_data"] = locations.at( texture ).pointer;
			} else {
				asmRefs["area_" + std::to_string( texture.areaIndexOrObjectId ) + "_" + toAlphanumeric( texture.material ) + "_texture_data"] = locations.at( texture ).pointer;
			}
		}
		writeTexture( rom, texture.format, (const uint*)pixmap.constBits(), pixmap.width() * pixmap.height() );
	}
	return locations;
}

TextureLocation writeSingleTexture(
	std::ostream &rom,
	uint segmentRomAddr,
	const string &texturePath,
	TextureFormat textureFormat
) {
	alignHead( rom, 8 );
	QImage pixmap = QImage( texturePath.c_str() ).convertToFormat( QImage::Format_ARGB32 );
	SegmentedPointer texturePointer = toSegmentedPointer( 0x0E, segmentRomAddr, rom.tellp() );
	writeTexture( rom, textureFormat, (const uint*)pixmap.constBits(), pixmap.width() * pixmap.height() );
	return TextureLocation{ texturePointer, (ushort)pixmap.width(), (ushort)pixmap.height() };
}

void writeTextureAnimation(
	std::ostream &rom,
	ubyte areaMask,
	const AnimationSpecs &animation,
	const ushort textureWidth,
	const ushort textureHeight,
	TextureWrapping hwrap,
	TextureWrapping vwrap,
	RomAddress &animationHead,
	SegmentedPointer cmdAddr
) {
	const uint animationType = (uint)animation.index();
	if( animationType <= 0 ) return;

	const size_t returnPosn = rom.tellp();
	rom.seekp( animationHead );

	writeWord( rom, cmdAddr );
	writeRaw<ubyte>( rom, { 0, areaMask, 0, (ubyte)(animationType - 1) });
	writeHalfword( rom, textureWidth << (hwrap == TextureWrapping::Mirror ? 3 : 2) );
	writeHalfword( rom, textureHeight << (vwrap == TextureWrapping::Mirror ? 3 : 2) );

	if( animationType == 1 ) {
		const LinearAnimationSpecs &specs = std::get<LinearAnimationSpecs>( animation );
		writeSignedWords( rom, { specs.speedX, specs.speedY });
	} else if( animationType == 2 ) {
		const SineWaveAnimationSpecs &specs = std::get<SineWaveAnimationSpecs>( animation );
		writeHalfwords( rom, { specs.angle, specs.period });
		const float amp = (float)specs.amplitude;
		writeWord( rom, reinterpret_cast<const uint&>( amp ) );
	}

	animationHead = (RomAddress)rom.tellp();
	rom.seekp( returnPosn );
}
