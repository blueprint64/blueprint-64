#include "src/core/level-writer/behaviour.hpp"

#include <cassert>

static constexpr ushort OBJ_FLAG_UPDATE_GFX = 0x0001;
static constexpr ushort OBJ_STORE_DISTANCE_TO_MARIO = 0x0040;

static constexpr ubyte P_FLAGS = 1;
static constexpr ubyte P_INTANGIBILITY_TIMER = 5;
static constexpr ubyte P_X = 6;
static constexpr ubyte P_Y = 7;
static constexpr ubyte P_Z = 8;
static constexpr ubyte P_SPEED_X = 9;
static constexpr ubyte P_SPEED_Y = 10;
static constexpr ubyte P_SPEED_Z = 11;
static constexpr ubyte P_PITCH = 18;
static constexpr ubyte P_YAW = 19;
static constexpr ubyte P_ROLL = 20;
static constexpr ubyte P_PITCH_VEL = 35;
static constexpr ubyte P_YAW_VEL = 36;
static constexpr ubyte P_ROLL_VEL = 37;
static constexpr ubyte P_HOME_X = 55;
static constexpr ubyte P_HOME_Y = 56;
static constexpr ubyte P_HOME_Z = 57;
static constexpr ubyte P_COLLISION_DISTANCE = 67;
static constexpr ubyte P_RENDER_DISTANCE = 69;

static constexpr ubyte PX_HOME_PITCH = 27;
static constexpr ubyte PX_HOME_YAW = 28;
static constexpr ubyte PX_HOME_ROLL = 29;
static constexpr ubyte PX_SCRATCH = 30;

static constexpr uint IEEE_INFINITY = 0x7F800000u;

#define __BSV_S16( cmd, prop, value ) writeRaw<ubyte>( rom, { cmd, prop }); writeSignedHalfword( rom, value )
#define __BSV_U16( cmd, prop, value ) writeRaw<ubyte>( rom, { cmd, prop, (ubyte)(((value) >> 8) & 0xFF), (ubyte)((value) & 0xFF) })

#define BHV_START_SURFACE writeWordRaw( rom, 0x00090000_be32 )
#define BHV_JUMP( segPtr ) writeWordRaw( rom, 0x04000000_be32 ); writeWord( rom, segPtr )
#define BHV_SET_FLAGS( prop, value ) __BSV_U16( 0x11, prop, value )

#define BHV_SET_COLLISION( segPtr ) writeWordRaw( rom, 0x2A000000_be32 ); writeWord( rom, segPtr )

#define BHV_STORE_HOME writeWordRaw( rom, 0x2D000000_be32 )

#define BHV_SET_WORD_UINT( prop, value ) writeRaw<ubyte>( rom, { 0x27, prop, 0x00, 0x00 }); writeWord( rom, value )
#define BHV_SET_WORD_FLOAT( prop, value ) { \
	const uint &__valueU32 = reinterpret_cast<const uint&>( value ); \
	BHV_SET_WORD_UINT( prop, __valueU32 ); \
}

#define BHV_SET_INT( prop, value ) __BSV_S16( 0x10, prop, value )
#define BHV_ADD_INT( prop, value ) __BSV_S16( 0x0F, prop, value )
#define BHV_SUM_INTS( destProp, srcProp1, srcProp2 ) writeRaw<ubyte>( rom, { 0x20, destProp, srcProp1, srcProp2 })

#define BHV_SET_FLOAT( prop, value ) __BSV_S16( 0x0E, prop, value )
#define BHV_ADD_FLOAT( prop, value ) __BSV_S16( 0x0D, prop, value )
#define BHV_SUM_FLOATS( destProp, srcProp1, srcProp2 ) writeRaw<ubyte>( rom, { 0x1F, destProp, srcProp1, srcProp2 })

#define BHV_LOOP_BEGIN writeWordRaw( rom, 0x08000000_be32 )
#define BHV_LOOP_END writeWordRaw( rom, 0x09000000_be32 )

#define BHV_REPEAT_BEGIN( iterations ) writeHalfwordRaw( rom, 0x0500_be16 ); writeSignedHalfword( rom, iterations )
#define BHV_REPEAT_END writeWordRaw( rom, 0x06000000_be32 )

#define BHV_SLEEP_1 writeWordRaw( rom, 0x01000001_be32 )

#define BHV_EXEC_PROCESS_COLLISION writeRaw<uint>( rom, { 0x0C000000_be32, 0x803839CC_be32 })
#define BHV_EXEC_MOVE writeRaw<uint>( rom, { 0x0C000000_be32, 0x8029F070_be32 })

static const Vec3f s_zeroVector = Vec3f{ 0, 0, 0 };
static const Angle3s s_zeroAngle = Angle3s{ 0, 0, 0 };

static inline bool fitsInShort( float value ) {
	return value == (float)(short)value;
}

static constexpr SimpleBehaviourInstruction MOVE = SimpleBehaviourInstruction::Move;
static constexpr SimpleBehaviourInstruction ROTATE = SimpleBehaviourInstruction::Rotate;

static inline void stopMovement( std::ostream &rom, Vec3f &currentVelocity ) {
	if( currentVelocity.x != 0 ) {
		BHV_SET_INT( P_SPEED_X, 0 );
		currentVelocity.x = 0;
	}
	if( currentVelocity.y != 0 ) {
		BHV_SET_INT( P_SPEED_Y, 0 );
		currentVelocity.y = 0;
	}
	if( currentVelocity.z != 0 ) {
		BHV_SET_INT( P_SPEED_Z, 0 );
		currentVelocity.z = 0;
	}
}

static inline void stopRotation( std::ostream &rom, Angle3s &currentAngularVelocity ) {
	if( currentAngularVelocity.pitch != 0 ) {
		BHV_SET_INT( P_PITCH_VEL, 0 );
		currentAngularVelocity.pitch = 0;
	}
	if( currentAngularVelocity.yaw != 0 ) {
		BHV_SET_INT( P_YAW_VEL, 0 );
		currentAngularVelocity.yaw = 0;
	}
	if( currentAngularVelocity.roll != 0 ) {
		BHV_SET_INT( P_ROLL_VEL, 0 );
		currentAngularVelocity.roll = 0;
	}
}

void writeBehaviour(
	std::ostream &rom,
	RomAddress segmentStart,
	const SimpleBehaviour &behaviour,
	SegmentedPointer collisionPointer,
	ushort minRadius
) {
	assert( behaviour.segmentedAddress >> 24 == 0x0E );

	const RomAddress head = (uint)rom.tellp();
	rom.seekp( segmentStart + (behaviour.segmentedAddress - 0x0E000000) );
	BHV_START_SURFACE;
	BHV_JUMP( 0x0E000000 + (head - segmentStart) );
	rom.seekp( head );

	BHV_SET_FLAGS( P_FLAGS, OBJ_FLAG_UPDATE_GFX | OBJ_STORE_DISTANCE_TO_MARIO );
	BHV_SET_COLLISION( collisionPointer );
	BHV_SET_INT( P_INTANGIBILITY_TIMER, 0 );

	if( behaviour.renderDistance < 0 ) {
		BHV_SET_WORD_UINT( P_RENDER_DISTANCE, IEEE_INFINITY );
	} else {
		BHV_SET_FLOAT( P_RENDER_DISTANCE, behaviour.renderDistance );
	}

	short collisionDistance = behaviour.getComputedCollisionDistance( minRadius );
	if( behaviour.collisionDistance < 0 ) {
		BHV_SET_WORD_UINT( P_COLLISION_DISTANCE, IEEE_INFINITY );
	} else {
		BHV_SET_FLOAT( P_COLLISION_DISTANCE, collisionDistance );
	}

	if( behaviour.movementLoop.empty() ) {
		BHV_LOOP_BEGIN;
		BHV_EXEC_PROCESS_COLLISION;
		BHV_LOOP_END;
		return;
	}

	const RomAddress loopStart = (int)rom.tellp();

	if( behaviour.resetAfterLoop ) {
		BHV_STORE_HOME;
		BHV_SET_INT( PX_SCRATCH, 0 );
		BHV_SUM_FLOATS( PX_HOME_PITCH, P_PITCH, PX_SCRATCH );
		BHV_SUM_FLOATS( PX_HOME_YAW, P_YAW, PX_SCRATCH );
		BHV_SUM_FLOATS( PX_HOME_ROLL, P_ROLL, PX_SCRATCH );
	}

	const bool singleStep = (behaviour.movementLoop.size() == 1 && !behaviour.resetAfterLoop);

	Vec3f prevSpeed = { 0, 0, 0 };
	Angle3s prevAngleVel = { 0, 0, 0 };

	for( const SimpleBehaviourMovement &step : behaviour.movementLoop ) {
		assert( step.frames > 0 );

		if( behaviour.movesMario ) {
			const Vec3f &speed = ((step.type & MOVE) == MOVE) ? step.speed : s_zeroVector;
			const Angle3s &angleVel = ((step.type & ROTATE) == ROTATE) ? step.angularVelocity : s_zeroAngle;

			if( speed.x != prevSpeed.x ) {
				if( fitsInShort( speed.x ) ) {
					BHV_SET_FLOAT( P_SPEED_X, (short)speed.x );
				} else {
					BHV_SET_WORD_FLOAT( P_SPEED_X, speed.x );
				}
				prevSpeed.x = speed.x;
			}
			if( speed.y != prevSpeed.y ) {
				if( fitsInShort( speed.y ) ) {
					BHV_SET_FLOAT( P_SPEED_Y, (short)speed.y );
				} else {
					BHV_SET_WORD_FLOAT( P_SPEED_Y, speed.y );
				}
				prevSpeed.y = speed.y;
			}
			if( speed.z != prevSpeed.z ) {
				if( fitsInShort( speed.z ) ) {
					BHV_SET_FLOAT( P_SPEED_Z, (short)speed.z );
				} else {
					BHV_SET_WORD_FLOAT( P_SPEED_Z, speed.z );
				}
				prevSpeed.z = speed.z;
			}

			if( angleVel.pitch != prevAngleVel.pitch ) {
				BHV_SET_INT( P_PITCH_VEL, angleVel.pitch );
				prevAngleVel.pitch = angleVel.pitch;
			}
			if( angleVel.yaw != prevAngleVel.yaw ) {
				BHV_SET_INT( P_YAW_VEL, angleVel.yaw );
				prevAngleVel.yaw = angleVel.yaw;
			}
			if( angleVel.roll != prevAngleVel.roll ) {
				BHV_SET_INT( P_ROLL_VEL, angleVel.roll );
				prevAngleVel.roll = angleVel.roll;
			}

		}

		if( singleStep ) {
			BHV_LOOP_BEGIN;
		} else if( step.frames > 1 ) {
			BHV_REPEAT_BEGIN( step.frames );
		}

		if( (step.type & MOVE) == MOVE ) {
			if( behaviour.movesMario ) {
				BHV_EXEC_MOVE;
			} else {
				if( step.speed.x != 0 ) {
					if( fitsInShort( step.speed.x ) ) {
						BHV_ADD_FLOAT( P_X, (short)step.speed.x );
					} else {
						BHV_SET_WORD_FLOAT( PX_SCRATCH, step.speed.x );
						BHV_SUM_FLOATS( P_X, P_X, PX_SCRATCH );
					}
				}
				if( step.speed.y != 0 ) {
					if( fitsInShort( step.speed.y ) ) {
						BHV_ADD_FLOAT( P_Y, (short)step.speed.y );
					} else {
						BHV_SET_WORD_FLOAT( PX_SCRATCH, step.speed.y );
						BHV_SUM_FLOATS( P_Y, P_Y, PX_SCRATCH );
					}
				}
				if( step.speed.z != 0 ) {
					if( fitsInShort( step.speed.z ) ) {
						BHV_ADD_FLOAT( P_Z, (short)step.speed.z );
					} else {
						BHV_SET_WORD_FLOAT( PX_SCRATCH, step.speed.z );
						BHV_SUM_FLOATS( P_Z, P_Z, PX_SCRATCH );
					}
				}
			}
		}

		if( (step.type & ROTATE) == ROTATE ) {
			if( step.angularVelocity.pitch != 0 ) {
				BHV_ADD_INT( P_PITCH, step.angularVelocity.pitch );
			}
			if( step.angularVelocity.yaw != 0 ) {
				BHV_ADD_INT( P_YAW, step.angularVelocity.yaw );
			}
			if( step.angularVelocity.roll != 0 ) {
				BHV_ADD_INT( P_ROLL, step.angularVelocity.roll );
			}
		}

		BHV_EXEC_PROCESS_COLLISION;

		if( singleStep ) {
			BHV_LOOP_END;
		} else if( step.frames == 1 ) {
			BHV_SLEEP_1;
		} else {
			BHV_REPEAT_END;
		}
	}

	if( !singleStep ) {
		if( behaviour.resetAfterLoop ) {
			BHV_SET_INT( PX_SCRATCH, 0 );
			BHV_SUM_FLOATS( P_X, P_HOME_X, PX_SCRATCH );
			BHV_SUM_FLOATS( P_Y, P_HOME_Y, PX_SCRATCH );
			BHV_SUM_FLOATS( P_Z, P_HOME_Z, PX_SCRATCH );
			BHV_SUM_FLOATS( P_PITCH, PX_HOME_PITCH, PX_SCRATCH );
			BHV_SUM_FLOATS( P_YAW, PX_HOME_YAW, PX_SCRATCH );
			BHV_SUM_FLOATS( P_ROLL, PX_HOME_ROLL, PX_SCRATCH );
		}
		stopMovement( rom, prevSpeed );
		stopRotation( rom, prevAngleVel );
		BHV_JUMP( 0x0E000000 + (loopStart - segmentStart) );
	}
}
