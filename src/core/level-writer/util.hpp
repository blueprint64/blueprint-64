#ifndef SRC_CORE_LEVEL_WRITER_TYPES_HPP_
#define SRC_CORE_LEVEL_WRITER_TYPES_HPP_

#include <cassert>
#include <ostream>
#include <optional>
#include "src/polyfill/byte-order.hpp"
#include "src/types.hpp"
#include "src/core/enums.hpp"
#include "src/core/util.hpp"

typedef uint RomAddress;
typedef uint SegmentedPointer;

using namespace BinaryWriter;

struct TextureLocation {
	SegmentedPointer pointer;
	ushort width;
	ushort height;
};

inline SegmentedPointer toSegmentedPointer( ubyte segment, RomAddress segmentRomStart, RomAddress romAddress ) {
	assert( (romAddress - segmentRomStart) >> 24 == 0 );
	return ((SegmentedPointer)segment << 24) | (romAddress - segmentRomStart);
}

inline void alignHead( std::ostream &rom, size_t boundary ) {
	while( rom.tellp() % boundary != 0 ) rom.put( 0x00 );
}

struct TextureInstance {
	std::string hash;
	TextureFormat format;
	std::string material;
	uint areaIndexOrObjectId;
	std::optional<string> objectName;
	bool asmRef;

	bool operator==( const TextureInstance &other ) const {
		return(
			hash == other.hash &&
			format == other.format &&
			( !asmRef || ( other.asmRef && material == other.material && areaIndexOrObjectId == other.areaIndexOrObjectId && objectName.has_value() == other.objectName.has_value() ) )
		);
	}
};

namespace std {
	template <> struct hash<TextureInstance> {
		size_t operator()(const TextureInstance & x) const {
			static const hash<string> strHash;
			if( x.asmRef ) {
				return strHash( x.material ) ^ (size_t)x.areaIndexOrObjectId;
			} else {
				return strHash( x.hash ) ^ (size_t)x.format;
			}
		}
	};
}

#endif /* SRC_CORE_LEVEL_WRITER_TYPES_HPP_ */
