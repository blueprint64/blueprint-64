#ifndef SRC_CORE_LEVEL_WRITER_BEHAVIOUR_HPP_
#define SRC_CORE_LEVEL_WRITER_BEHAVIOUR_HPP_

#include <ostream>
#include "src/core/level-writer/util.hpp"
#include "src/core/data/behaviour.hpp"

extern void writeBehaviour(
	std::ostream &rom,
	RomAddress segmentStart,
	const SimpleBehaviour &behaviour,
	SegmentedPointer collisionPointer,
	ushort minRadius
);

#endif /* SRC_CORE_LEVEL_WRITER_BEHAVIOUR_HPP_ */
