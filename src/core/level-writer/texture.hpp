#ifndef SRC_CORE_LEVEL_WRITER_TEXTURE_HPP_
#define SRC_CORE_LEVEL_WRITER_TEXTURE_HPP_

#include <functional>
#include <unordered_set>
#include <unordered_map>
#include <ostream>
#include <map>
#include "src/polyfill/filesystem.hpp"
#include "src/core/data/animation.hpp"
#include "src/core/enums.hpp"
#include "src/core/level-writer/util.hpp"

extern HashMap<TextureInstance,TextureLocation> writeTextures(
	std::ostream &rom,
	uint segmentRomAddr,
	const fs::path &textureDir,
	const HashSet<TextureInstance> &textures,
	std::map<std::string,uint> &asmRefs
);

extern TextureLocation writeSingleTexture(
	std::ostream &rom,
	uint segmentRomAddr,
	const string &texturePath,
	TextureFormat textureFormat
);

extern void writeTextureAnimation(
	std::ostream &rom,
	ubyte areaMask,
	const AnimationSpecs &animation,
	const ushort textureWidth,
	const ushort textureHeight,
	TextureWrapping hwrap,
	TextureWrapping vwrap,
	RomAddress &animationHead,
	SegmentedPointer cmdAddr
);


#endif /* SRC_CORE_LEVEL_WRITER_TEXTURE_HPP_ */
