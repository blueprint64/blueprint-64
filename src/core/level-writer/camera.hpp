#ifndef SRC_CORE_LEVEL_WRITER_CAMERA_HPP_
#define SRC_CORE_LEVEL_WRITER_CAMERA_HPP_

#include <map>
#include "src/core/asm-modules.hpp"
#include "src/core/level-writer/util.hpp"
#include "src/core/data/camera.hpp"

extern void writeLevelCameraTriggers(
	std::ostream &rom,
	LevelId levelId,
	const std::map<Uuid,CustomCamera> &cameraModules,
	const std::multimap<ubyte,CameraTrigger> &cameraTriggers,
	std::map<string,uint> &cameraTriggerRefs,
	RomAddress segmentStart
);

extern uint writeDefaultCameraInitializer(
	std::ostream &rom,
	const CameraTrigger &defaultCamera,
	const std::map<Uuid,CustomCamera> &cameraModules,
	RomAddress segmentStart
);

#endif /* SRC_CORE_LEVEL_WRITER_CAMERA_HPP_ */
