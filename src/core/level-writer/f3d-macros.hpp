#ifndef SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_
#define SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_

#include "src/polyfill/fastmath.hpp"
#include "src/core/level-writer/util.hpp"

#define CC_SRC_CYCLE1 0u
#define CC_SRC_TEXTURE 1u
#define CC_SRC_PRIMITIVE 3u
#define CC_SRC_SHADE 4u
#define CC_SRC_ENVIRONMENT 5u
#define CC_SRC_FULL 6u /* not valid for b, c, or C */
#define CC_SRC_ZERO 0x1Fu

#define SET_COMBINE_IMPL( a1, b1, c1, d1, A1, B1, C1, D1, a2, b2, c2, d2, A2, B2, C2, D2 ) \
	writeWords( rom, {\
		0xFC000000u | \
		((CC_SRC_##a1 & 0x0F) << 20) | \
		((CC_SRC_##c1 & 0x1F) << 15) | \
		((CC_SRC_##A1 & 0x07) << 12) | \
		((CC_SRC_##C1 & 0x07) << 9) | \
		((CC_SRC_##a2 & 0x0F) << 5) | \
		((CC_SRC_##c2 & 0x1F) << 0) \
	, \
		((CC_SRC_##b1 & 0x0F) << 28) | \
		((CC_SRC_##b2 & 0x0F) << 24) | \
		((CC_SRC_##A2 & 0x07) << 21) | \
		((CC_SRC_##C2 & 0x07) << 18) | \
		((CC_SRC_##d1 & 0x07) << 15) | \
		((CC_SRC_##B1 & 0x07) << 12) | \
		((CC_SRC_##D1 & 0x07) << 9)  | \
		((CC_SRC_##d2 & 0x07) << 6)  | \
		((CC_SRC_##B2 & 0x07) << 3)  | \
		((CC_SRC_##D2 & 0x07) << 0) \
	});

#define SET_COMBINE( mode ) SET_COMBINE_IMPL( mode, mode )
#define SET_COMBINE2( mode1, mode2 ) SET_COMBINE_IMPL( mode1, mode2 )

#define COMBINER_OPAQUE \
	TEXTURE, ZERO, SHADE, ZERO, \
	ZERO, ZERO, ZERO, FULL

#define COMBINER_TEXTURE_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	ZERO, ZERO, ZERO, TEXTURE

#define COMBINER_ENVIRONMENT_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	ZERO, ZERO, ZERO, ENVIRONMENT

#define COMBINER_PRIMITIVE_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	ZERO, ZERO, ZERO, PRIMITIVE

#define COMBINER_ENVIRONMENT_AND_PRIMITIVE_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	PRIMITIVE, ZERO, ENVIRONMENT, ZERO

#define COMBINER_TEXTURE_AND_ENVIRONMENT_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	TEXTURE, ZERO, ENVIRONMENT, ZERO

#define COMBINER_TEXTURE_AND_PRIMITIVE_ALPHA \
	TEXTURE, ZERO, SHADE, ZERO, \
	TEXTURE, ZERO, PRIMITIVE, ZERO

#define COMBINER2_MIX_PRIMITIVE_ALPHA \
	ZERO, ZERO, ZERO, CYCLE1, \
	CYCLE1, ZERO, PRIMITIVE, ZERO

#define COMBINER2_PASSTRHOUGH \
	ZERO, ZERO, ZERO, CYCLE1, \
	ZERO, ZERO, ZERO, CYCLE1

#define COMBINER_TEXTURE_ONLY_OPAQUE \
	ZERO, ZERO, ZERO, TEXTURE, \
	ZERO, ZERO, ZERO, FULL

#define COMBINER_SHADE_ONLY \
	ZERO, ZERO, ZERO, SHADE, \
	ZERO, ZERO, ZERO, SHADE

#define RDP_PIPE_SYNC writeRaw<uint>( rom, { 0xE7000000_be32, 0x00000000 });
#define RDP_TILE_SYNC writeRaw<uint>( rom, { 0xE8000000_be32, 0x00000000 });
#define RDP_LOAD_SYNC writeRaw<uint>( rom, { 0xE6000000_be32, 0x00000000 });
#define LOAD_DARK_SHADE( shadePtr ) writeWords( rom, { 0x03880010, shadePtr });
#define LOAD_LIGHT_SHADE( shadePtr ) writeWords( rom, { 0x03860010, shadePtr });
#define LOAD_SHADE_VALUES( shadePtr ) writeWords( rom, { 0x03860010, shadePtr + 8, 0x03880010, shadePtr });
#define SET_IMG( format, texPtr ) writeWords( rom, { 0xFD000000 | ((uint)(format) << 19), texPtr });
#define LOAD_BLOCK( td, texels, dxt ) writeWords( rom, { 0xF3000000, (td << 24) | (((texels)-1) << 12) | (uint)(dxt) });
#define SET_TILE_SIZE( td, width, height ) writeWords( rom, { 0xF2000000, (td << 24) | (((uint)(width)-1) << 14) | (((uint)(height)-1) << 2) });
#define END_DL writeRaw<uint>( rom, { 0xB8000000_be32, 0x00000000 });
#define LOAD_VTX( num, segPtr ) writeWords( rom, { 0x04000000 | (((num)-1) << 20) | ((num) << 4), segPtr });
#define RENDER_TRI( indices ) writeWordRaw( rom, 0xBF000000_be32 ); rom.put( 0x00 ); rom.put( indices[0] * 10 ); rom.put( indices[1] * 10 ); rom.put( indices[2] * 10 );
#define DISABLE_TD( td ) writeWords( rom, { 0xBB000000 | td, 0xFFFFFFFF });
#define SET_FOG_COLOUR( colour ) writeWordRaw( rom, 0xF8000000_be32 ); colour.writeToRom( rom, true );
#define SET_OTHERMODE_H( shift, num, flags ) writeWords( rom, { 0xBA000000 | ((uint)(shift) << 8) | (uint)(num), (uint)(flags) << shift });
#define SET_OTHERMODE_L( shift, num, flags ) writeWords( rom, { 0xB9000000 | ((uint)(shift) << 8) | (uint)(num), (uint)(flags) << shift });
#define SET_ENV_COLOUR( colour ) writeWords( rom, { 0xFB000000, colour });
#define SET_GEOMETRY_FLAGS( flags ) writeWords( rom, { 0xB7000000, flags });
#define CLEAR_GEOMETRY_FLAGS( flags ) writeWords( rom, { 0xB6000000, flags });
#define DL_JAL( segPtr ) writeWords( rom, { 0x06000000, segPtr });
#define SET_TEXTURE_SCALE( width, height, enable ) writeWords( rom, {\
	0xBB000000 | ((enable) ? 1 : 0), \
	(((uint)(width) << 22) - ((uint)(width) << 17)) | \
	(((uint)(height) << 6) - ((uint)(height) << 1)) \
})

#define G_ZBUFFER				0x00000001
#define G_SHADE					0x00000004
#define G_SHADING_SMOOTH		0x00000200
#define G_CULL_FRONT			0x00001000
#define G_CULL_BACK				0x00002000
#define G_FOG					0x00010000
#define G_LIGHTING				0x00020000
#define G_TEXTURE_GEN			0x00040000
#define G_TEXTURE_GEN_LINEAR	0x00080000
#define G_CLIPPING				0x00800000

#define G_CYC_1_CYCLE 0
#define G_CYC_2_CYCLE 1
#define G_CYC_COPY 2
#define G_CYC_FILL 3

#define SET_CYCLE_MODE( mode ) SET_OTHERMODE_H( 20, 2, mode )
#define SET_RENDER_MODE_2( mode ) SET_OTHERMODE_L( 3, 29, mode )
#define SET_RENDER_MODE( mode1, mode2 ) SET_RENDER_MODE_2( (mode1) | (mode2) )


#define SET_TILE( format, line, td, cmS, cmT, maskS, maskT ) \
	writeWords( rom, { \
		0xF5000000u | \
		((uint)(format) << 19) | \
		((uint)(line) << 9) \
	, \
		((uint)(td) << 24) | \
		((uint)(cmT) << 18) | \
		((uint)(maskT) << 14) | \
		((uint)(cmS) << 8) | \
		((uint)(maskS) << 4) \
	});

#define TD_LOAD 7
#define TD_RENDER 0

inline void SET_FOG_DISTANCE( std::ostream &rom, double min, double max ) {
	if( min == max ) {
		max += 0.002;
		min -= 0.002;
	}

	writeWordRaw( rom, 0xBC000008_be32 );
	const double fmd = (128.0 / (max - min));
	const double fod = (256.0 * (0.5 - min) / (max - min));

	const short fms = fmd > 32767.0 ? 0x7FFF : (fmd < -327678.0 ? 0x8000 : (short)fmd);
	const short fos = fod > 32767.0 ? 0x7FFF : (fod < -327678.0 ? 0x8000 : (short)fod);

	writeSignedHalfwords( rom, { fms, fos });
}

static inline void LOAD_TEXTURE(
	std::ostream &rom,
	SegmentedPointer texturePtr,
	TextureFormat format,
	ushort width,
	ushort height,
	TextureWrapping hwrap,
	TextureWrapping vwrap
) {
	const uint pixels = (uint)width * (uint)height;

	uint loadBitSize, texels, line, tw;
	switch( (uint)format & 3 ) {
		case 0: {
			loadBitSize = 2;
			texels = (pixels + 3) >> 2;
			line = width >> 4;
			tw = line > 0 ? line : 1;
			break;
		}
		case 1: {
			loadBitSize = 2;
			texels = (pixels + 1) >> 1;
			line = width >> 3;
			tw = line > 0 ? line : 1;
			break;
		}
		case 2: {
			loadBitSize = 2;
			texels = pixels;
			line = width >> 2;
			tw = line > 0 ? line : 1;
			break;
		}
		case 3: {
			loadBitSize = 3;
			texels = pixels;
			line = width >> 2;
			tw = width >> 1;
			break;
		}
		default: assert( false );
	}

	const TextureFormat loadFormat = (TextureFormat)(((uint)format & 0x1C) | loadBitSize );
	const uint dxt = (0x7FFu + tw) / tw;

	const uint maskS = ilog2( (uint)width );
	const uint maskT = ilog2( (uint)height );

	SET_IMG( loadFormat, texturePtr );
	SET_TILE( loadFormat, 0, TD_LOAD, hwrap, vwrap, maskS, maskT );
	RDP_LOAD_SYNC
	LOAD_BLOCK( TD_LOAD, texels, dxt );
	RDP_PIPE_SYNC //TODO: pipe sync here is probably not necessary
	SET_TILE( format, line, TD_RENDER, hwrap, vwrap, maskS, maskT );
}

#endif /* SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_ */
