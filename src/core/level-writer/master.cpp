#include "src/core/level-writer/master.hpp"

#include <cassert>
#include <cmath>
#include "src/core/blueprint.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/core/locations.hpp"
#include "src/core/util.hpp"
#include "src/core/level-writer/script.hpp"
#include "src/core/level-writer/texture.hpp"
#include "src/core/level-writer/collision.hpp"
#include "src/core/level-writer/geo.hpp"
#include "src/core/level-writer/fast3d.hpp"
#include "src/core/level-writer/water.hpp"
#include "src/core/level-writer/camera.hpp"
#include "src/core/level-writer/behaviour.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/hardcoded.hpp"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

static const ubyte s_starSelectPatch[25] = {
	0x3C, 0x01, 0x80, 0x34,
	0x94, 0x28, 0xBA, 0xC6,
	0x3C, 0x01, 0x80, 0x25,
	0x00, 0x28, 0x40, 0x21,
	0x10, 0x00, 0x00, 0x09,
	0x91, 0x02, 0xBF, 0x70,
	0x00
};

static inline void applyStarSelectPatch( std::ostream &rom ) {
	rom.seekp( 0x6F58 );
	rom.write( (const char*)s_starSelectPatch, 25 );
	for( ubyte i = 1; i < 16; i++ ) {
		const LevelId level = courseIdToLevelId( (CourseId)i );
		if( Blueprint::current()->hasLevel( level ) ) {
			const LevelSpecs &specs = Blueprint::current()->levels().find( level )->second;
			rom.put( specs.enableStarSelect ? (char)0x01 : (char)0x00 );
		} else {
			rom.put( (char)0x01 );
		}
	}
}

static inline void applyPauseCameraPatch( std::ostream &rom ) {
	// The table in vanilla SM64 only works if you have 4 or fewer areas
	// in every level. But supporting all 8 areas actually makes the ASM
	// code so much simpler, you can fit the entire table in the freed up
	// space from the simpler code. O P T I M I Z A T I O N

	static const ubyte s_pauseCamPatch1[12] = {
		0x00, 0x0E, 0x79, 0x03,
		0x25, 0xC1, 0x00, 0x0F,
		0x00, 0x01, 0x79, 0x03
	};

	static const ubyte s_pauseCamPatch2[32] = {
		0x33, 0x18, 0x00, 0x07,
		0x03, 0x0D, 0x70, 0x04,
		0x29, 0xE1, 0x00, 0x27,
		0x14, 0x20, 0x00, 0x0E,
		0xAF, 0xAE, 0x00, 0x24,
		0xAF, 0xA0, 0x00, 0x28,
		0x10, 0x00, 0x00, 0x0B,
		0xAF, 0xA0, 0x00, 0x24,
	};

	ubyte areaFlags[40] = {
		0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 3, 0, 1, 1, 1, 0, 1,
		0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0
	};

	static const ubyte s_pauseCamPatch3[16] = {
		0x3C, 0x19, 0x80, 0x34,
		0x87, 0x39, 0xC8, 0x48,
		0x33, 0x38, 0x80, 0x00,
		0x13, 0x00, 0x00, 0x4B
	};

	for( const auto &i : Blueprint::current()->areas() ) {
		const ubyte level = i.first >> 3;
		const ubyte area = i.first & 7;
		if( i.second.pauseCamera.fixed ) {
			areaFlags[level] |= 1 << area;
		} else {
			areaFlags[level] &= 0xFF - (1 << area);
		}
	}

	rom.seekp( 0x42A04 );
	rom.write( (char*)s_pauseCamPatch1, 12 );
	rom.seekp( 0x42A20 );
	rom.write( (char*)s_pauseCamPatch2, 32 );
	rom.write( (char*)areaFlags, 40 );
	rom.write( (char*)s_pauseCamPatch3, 16 );
	rom.seekp( 0x42A93 );
	rom.put( 0x28 );
	rom.seekp( 0x42A9E );
	rom.put( 0x7A );
	rom.put( 0x40 );
}

void LevelWriter::applyLevelAndAreaFlags( std::ostream &rom ) {
	rom.seekp( 0x36D2C );
	rom.put( (char)0x95 ); // Make far clipping plane distance be unsigned

	applyStarSelectPatch( rom );
	applyPauseCameraPatch( rom );
}

static inline void writeLightVector( std::ostream &rom, short yaw, short pitch ) {
	const double h = std::abs( std::cos( (double)pitch * M_PI / 180.0 ) );
	const double y = std::sin( (double)-pitch * M_PI / 180.0 );
	const double x = h * std::sin( (double)-yaw * M_PI / 180.0 );
	const double z = h * std::cos( (double)yaw * M_PI / 180.0 );

	const sbyte data[8] = {
		(sbyte)std::round( 127.0 * x ),
		(sbyte)std::round( 127.0 * y ),
		(sbyte)std::round( 127.0 * z ),
		0, 0, 0, 0, 0
	};

	rom.write( (const char*)data, 8 );
}

#define POSITION_SEGMENTED toSegmentedPointer( 0x0E, mainSegmentStart, (uint)rom.tellp() )
#define ASSERT_POSITION( addr ) assert( !rom.good() || POSITION_SEGMENTED == addr );

void LevelWriter::writeToRom(
	LevelId levelId,
	std::iostream &rom,
	const std::map<Uuid,CustomCamera> &cameraModules,
	const std::array<AreaActors,8> &actors,
	std::map<string,uint> &asmRefs,
	std::map<string,uint> &cameraTriggerRefs,
	bool cutoutDecal,
	float worldScale,
	std::vector<string> &warnings
) {
	const Blueprint *blueprint = Blueprint::current();
	assert( blueprint->hasLevel( levelId ) );
	const LevelSpecs &levelSpecs = blueprint->levels().find( levelId )->second;
	const fs::path levelPath = blueprint->getLevelPath( levelId );

	const ushort soundCarryDistanceBE = htons( levelSpecs.maxSoundDistance );
	rom.seekp( 0xEE138 + (2 * (uint)levelId) );
	rom.write( (const char*)&soundCarryDistanceBE, 2 );

	HardcodedLevelCode::remove( rom, levelId );

	rom.seekp( 0xEE0C0u + ((uint)levelId * 3) );
	sbyte echoLevels[3] = { levelSpecs.defaultEchoLevel, levelSpecs.defaultEchoLevel, levelSpecs.defaultEchoLevel };
	if( blueprint->hasArea( levelId, 1 ) ) {
		echoLevels[0] = (char)blueprint->areas().at( AREA_ID( levelId, 1 ) ).echoLevel;
	}
	if( blueprint->hasArea( levelId, 2 ) ) {
		echoLevels[1] = (char)blueprint->areas().at( AREA_ID( levelId, 2 ) ).echoLevel;
	}
	rom.write( (const char*)echoLevels, 3 );

	uint numAnimations = 0;
	HashSet<TextureInstance> usedTextures;
	HashMap<string,MaterialSpecs> areaMaterialMaps[8];
	HashMap<objectId,HashMap<string,MaterialSpecs>> objectMaterialMaps;
	std::multimap<ubyte,CameraTrigger> cameraTriggers;
	for( ubyte i = 0; i < 8; i++ ) {
		if( blueprint->hasArea( levelId, i ) ) {
			for( const CameraTrigger &cameraTrigger : blueprint->areas().at( AREA_ID( levelId, i ) ).cameraTriggers ) {
				cameraTriggers.insert( std::make_pair( i, cameraTrigger ) );
			}
			HashMap<string,MaterialSpecs> materialMap = blueprint->getAreaGeometry( levelId, i );
			for( const auto &j : materialMap ) {
				const MaterialSpecs &material = j.second;
				if( !material.visible || !material.textureInfo.has_value() ) continue;
				usedTextures.insert({ material.textureInfo.value().hash, material.format, j.first, (uint)i, std::nullopt, material.asmRefs.texData });
				if( material.animation.index() > 0 ) {
					numAnimations++;
				}
			}
			areaMaterialMaps[i] = std::move( materialMap );

			for( const WaterBoxSpecs &waterBox : Blueprint::current()->areas().at( AREA_ID( levelId, i ) ).waterBoxes ) {
				if( waterBox.isVisible && waterBox.animation.index() > 0 ) {
					numAnimations++;
				}
			}
		}
	}

	for( const auto &i : levelSpecs.simpleObjects ) {
		HashMap<string,MaterialSpecs> materialMap = blueprint->getObjectGeometry( levelId, i.first );
		for( const auto &j : materialMap ) {
			const MaterialSpecs &material = j.second;
			if( !material.visible || !material.textureInfo.has_value() ) continue;
			usedTextures.insert({ material.textureInfo.value().hash, material.format, j.first, i.first, i.second.name, material.asmRefs.texData });
			if( material.animation.index() > 0 ) {
				numAnimations++;
			}
		}
		objectMaterialMaps[i.first] = std::move( materialMap );
	}

	for( const auto &i : levelSpecs.partialObjects ) {
		HashMap<string,MaterialSpecs> materialMap = blueprint->getObjectGeometry( levelId, i.first );
		for( const auto &j : materialMap ) {
			const MaterialSpecs &material = j.second;
			if( !material.visible || !material.textureInfo.has_value() ) continue;
			usedTextures.insert({ material.textureInfo.value().hash, material.format, j.first, i.first, i.second.name, material.asmRefs.texData });
		}
		objectMaterialMaps[i.first] = std::move( materialMap );
	}

	const RomLocations::LevelLocation &romLocations = RomLocations::getLevelLocation( levelId );
	const uint mainSegmentStart = romLocations.dataLocation;

	rom.seekp( mainSegmentStart );
	for( const Vec3f &starSpawn : levelSpecs.starSpawnLocations ) {
		starSpawn.writeToRom( rom );
	}

	ASSERT_POSITION( 0x0E00012C );
	uint nextWaypoint = 0x0E000194 + ( 0x14 * numAnimations );
	const uint nullWaypointPtr = htonl( nextWaypoint - 8 );
	std::vector<uint> waypointsToWrite;
	for( uint i = 0; i < 22; i++ ) {
		const std::vector<Vec3s> &waypoints = levelSpecs.waypoints[i];
		if( waypoints.size() == 0 || (waypoints.size() == 1 && waypoints[0].zero()) ) {
			rom.write( (const char*)&nullWaypointPtr, 4 );
		} else {
			const uint segPtr = htonl( nextWaypoint );
			rom.write( (const char*)&segPtr, 4 );
			nextWaypoint += 4 + (uint)(levelSpecs.waypoints[i].size() * 8);
			waypointsToWrite.push_back( i );
		}
	}

	ASSERT_POSITION( 0x0E000184 );
	const PauseWarp &pauseWarp = levelSpecs.overridePauseWarp ? levelSpecs.pauseWarp : Blueprint::current()->romSpecs().pauseWarp;
	const ubyte pauseWarpData[] = { 0, (ubyte)pauseWarp.level, pauseWarp.area, pauseWarp.warpId };
	rom.write( (const char*)pauseWarpData, 4 );

	ASSERT_POSITION( 0x0E000188 );
	RomAddress animationHead = (uint)rom.tellp();
	for( uint i = 0; i < 4 + (0x14 * numAnimations); i++ ) {
		rom.put( 0 );
	}

	uint nullWaypoint[] = { 0x00000000, 0xFFFFFFFF };
	rom.write( (const char*)nullWaypoint, 8 );

	for( uint index : waypointsToWrite ) {
		ushort i = 0;
		for( const Vec3s &v : levelSpecs.waypoints[index] ) {
			const ushort j = htons( i );
			rom.write( (const char*)&j, 2 );
			v.writeToRom( rom );
		}
		uint endMarker = 0xFFFFFFFF;
		rom.write( (const char*)&endMarker, 4 );
	}
	ASSERT_POSITION( nextWaypoint );

	std::map<ubyte,uint> cameraFunctions;
	writeLevelCameraTriggers( rom, levelId, cameraModules, cameraTriggers, cameraTriggerRefs, mainSegmentStart );
	for( ubyte i = 0; i < 8; i++ ) {
		if( !blueprint->hasArea( levelId, i ) ) continue;
		const CameraTrigger &defaultCamera = blueprint->areas().at( AREA_ID( levelId, i ) ).defaultCamera;
		cameraFunctions[i] = writeDefaultCameraInitializer( rom, defaultCamera, cameraModules, mainSegmentStart );
	}

	alignHead( rom, 8 );
	const LevelScriptLocations levelLocations = writeLevelScript( rom, levelId, levelSpecs, actors, warnings, romLocations.scriptLocation, mainSegmentStart );

	alignHead( rom, 8 );
	HashMap<uint,SegmentedPointer> shadeMap;
	for( const auto &i : levelSpecs.shadingValues ) {
		shadeMap[i.first] = POSITION_SEGMENTED;
		i.second.ambient.writeToRom( rom, true );
		i.second.ambient.writeToRom( rom, true );
		i.second.diffuse.writeToRom( rom, true );
		i.second.diffuse.writeToRom( rom, true );
		writeLightVector( rom, i.second.lightAngleYaw, i.second.lightAnglePitch );
		if( i.second.asmRef ) {
			const string safeName = toAlphanumeric( i.second.name );
			asmRefs[string("shade_") + safeName + "_dark_data"] = shadeMap.at( i.first );
			asmRefs[string("shade_") + safeName + "_light_data"] = shadeMap.at( i.first ) + 8;
		}
	}

	const TextureFiltering defaultTextureFiltering = Blueprint::current()->tryGetTweakValue(
		Uuid::parse( "3b0c4ce9-353e-4465-813f-12e50adc5daf" ) // disable texture interpolation
	).has_value() ? TextureFiltering::None : TextureFiltering::Bilinear;

	HashMap<TextureInstance,TextureLocation> textureMap = writeTextures( rom, mainSegmentStart, levelPath, usedTextures, asmRefs );
	HashMap<ushort,RomAddress> waterDls;
	alignHead( rom, 4 );
	for( const auto &i : levelLocations.areaLocations ) {
		const ushort areaId = i.first;
		assert( blueprint->hasArea( levelId, areaId & 7 ) );
		const AreaSpecs &areaSpecs = blueprint->areas().at( areaId );
		const fs::path areaDir = blueprint->getAreaPath( levelId, areaId & 7 );
		HashMap<string,MaterialSpecs> &materials = areaMaterialMaps[ areaId & 7 ];

		writeWordAt( rom, i.second.collision, POSITION_SEGMENTED );
		writeAreaCollision( rom, areaSpecs, materials, areaDir );
		alignHead( rom, 4 );

		short cameraId;
		switch( areaSpecs.defaultCamera.type ) {
			case CameraTriggerType::Vanilla:
				cameraId = (short)std::get<VanillaCameraOptions>( areaSpecs.defaultCamera.options ).mode;
				break;
			case CameraTriggerType::Module:
				cameraId = blueprint->getCameraModules().at( std::get<ModuleCameraOptions>( areaSpecs.defaultCamera.options ).moduleId ).cameraId;
				break;
			default:
				assert( false );
				cameraId = 0;
		}

		writeWordAt( rom, i.second.geoLayout, POSITION_SEGMENTED );
		HashMap<DrawingLayer,RomAddress> dlMap = writeAreaGeoLayout( rom, areaId, cameraId, cameraFunctions.at( (ubyte)(areaId & 7) ), areaSpecs, materials, waterDls, worldScale, warnings );
		writeAreaFast3d( rom, areaSpecs.fog, areaId & 7, areaDir, mainSegmentStart, shadeMap, textureMap, dlMap, materials, defaultTextureFiltering, asmRefs, animationHead, cutoutDecal, warnings );
	}

	const Uuid maxObjectCollisionVerticesTweakId = Uuid::parse( "3429558c-fd72-49c2-9e99-eff31ae48f39" );
	const ushort previousMaxObjectCollisionVertices = blueprint->tryGetTweakValue( maxObjectCollisionVerticesTweakId ).value_or( Word::fromUInt( 200 ) ).asUInt();
	ushort maxObjectCollisionVertices = 200;

	SegmentedPointer firstBehaviourStart = 0x0E150000;
	for( const auto &i : levelSpecs.simpleObjects ) {
		writeWordAt( rom, levelLocations.objectLocations.at( i.first ), POSITION_SEGMENTED );
		const HashMap<string,MaterialSpecs> &materialMap = objectMaterialMaps.at( i.first );
		const HashMap<DrawingLayer,RomAddress> dlMap = writeObjectGeoLayout( rom, i.second, materialMap, mainSegmentStart, warnings );
		const fs::path objectDir = Blueprint::current()->getObjectPath( levelId, i.first );
		writeObjectFast3d( rom, i.second.fog, i.first, objectDir, mainSegmentStart, shadeMap, textureMap, dlMap, materialMap, defaultTextureFiltering, asmRefs, i.second.name, animationHead, cutoutDecal, warnings );
		if( i.second.hasCollision ) {
			const SegmentedPointer collisionPointer = POSITION_SEGMENTED;
			switch( i.second.behaviour.type ) {
				case BehaviourType::AsmReference:
					asmRefs["object_" + toAlphanumeric( i.second.name ) + "_collision"] = collisionPointer;
					break;
				case BehaviourType::Vanilla:
					writeWordAt( rom, i.second.behaviour.romAddress, collisionPointer );
					break;
				case BehaviourType::Autogenerated:
					break;
				default:
					assert( false );
			}

			const ushort numVertices = writeObjectCollision( rom, materialMap, objectDir );
			if( numVertices > 432 ) {
				// making the vertex buffer any larger than this will overflow the stack
				throw RomOverflowException( "Object \""s + i.second.name + "\" has too many vertices in its collision mesh."s );
			} else if( numVertices > maxObjectCollisionVertices ) {
				maxObjectCollisionVertices = numVertices;
			}

			alignHead( rom, 4 );
			if( i.second.behaviour.type == BehaviourType::Autogenerated ) {
				writeBehaviour( rom, mainSegmentStart, i.second.behaviour.autoGen, collisionPointer, i.second.max_xyz );
				firstBehaviourStart = std::min( firstBehaviourStart, i.second.behaviour.autoGen.segmentedAddress );
			}
		}
	}

	FogSpecs noFog;
	noFog.enabled = false;

	HashMap<objectId,SegmentedPointer> partialObjectLocations;
	for( const auto &i : levelSpecs.partialObjects ) {
		partialObjectLocations[i.first] = POSITION_SEGMENTED;
		const HashMap<string,MaterialSpecs> &materialMap = objectMaterialMaps.at( i.first );
		const HashMap<DrawingLayer,RomAddress> dlMap = writePartialObjectGeoLayout( rom, i.second, materialMap, warnings );
		const fs::path objectDir = Blueprint::current()->getObjectPath( levelId, i.first );
		writeObjectFast3d( rom, noFog, i.first, objectDir, mainSegmentStart, shadeMap, textureMap, dlMap, materialMap, defaultTextureFiltering, asmRefs, i.second.name, animationHead, cutoutDecal, warnings );
	}

	HashMap<uint,SegmentedPointer> userDataLocations;
	for( const auto &i : levelSpecs.userData ) {
		userDataLocations[i.first] = POSITION_SEGMENTED;
		if( i.second.asmRef ) {
			asmRefs["user_data_" + toAlphanumeric( i.second.name )] = POSITION_SEGMENTED;
		}
		const fs::path dataPath = Blueprint::current()->getUserDataPath( levelId, i.first );
		if( !fs::exists( dataPath ) ) {
			throw BlueprintLoadError( "Data file '" + i.second.name + "' missing" );
		}
		FileReadStream dataFile( dataPath.u8string() );
		for( ubyte c = dataFile.get(); dataFile.good() && !dataFile.eof(); c = dataFile.get() ) {
			rom.put( c );
		}
		alignHead( rom, 4 );
	}

	writeWaterVisuals( rom, mainSegmentStart, waterDls, animationHead, asmRefs );

	for( const auto &i : levelSpecs.userData ) {
		for( const auto &j : i.second.links ) {
			const uint pl = mainSegmentStart + ( userDataLocations.at( i.first ) - 0x0E000000 ) + j.first;
			switch( j.second.linkType ) {
				case LinkType::PartialObject: {
					writeWordAt( rom, pl, partialObjectLocations.at( j.second.targetId ) );
					break;
				};
				case LinkType::UserData: {
					writeWordAt( rom, pl, userDataLocations.at( j.second.targetId ) + j.second.targetOffset );
					break;
				};
				default: continue;
			}
		}
	}

	for( const auto &i : levelSpecs.advancedObjects ) {
		writeWordAt( rom, levelLocations.objectLocations.at( i.first ), userDataLocations.at( i.second.customDataId ) );
	}

	if( POSITION_SEGMENTED > firstBehaviourStart || rom.tellp() > mainSegmentStart + 0x150000 ) {
		throw RomOverflowException( "Segment 0xE overflow: Level '" + levelSpecs.name + "' has too much data." );
	}

	rom.seekp( romLocations.scriptLoaderLocation );
	writeWords( rom, { 0x00100019, romLocations.scriptLocation, mainSegmentStart, 0x19000000 });

	if( maxObjectCollisionVertices > previousMaxObjectCollisionVertices ) {
		warnings.push_back( "The object collision vertex buffer has been made larger to accommodate objects with many vertices." );
		Blueprint::currentEditable()->setTweakValue(
			maxObjectCollisionVerticesTweakId,
			Word::fromUInt( maxObjectCollisionVertices )
		);
		TweakStore::instance().tryGetTweak( maxObjectCollisionVerticesTweakId )->apply( rom, Word::fromUInt( maxObjectCollisionVertices ) );
	}

	if( !rom.good() ) {
		throw RomError( "Unknown I/O error writing to ROM" );
	}

}

#undef POSITION_SEGMENTED
#undef ASSERT_POSITION
