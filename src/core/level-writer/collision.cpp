#include "src/core/level-writer/collision.hpp"

#include <vector>
#include <functional>
#include <cstring>
#include "src/polyfill/filestream.hpp"
#include "src/core/level-writer/util.hpp"
#include "src/core/level-writer/water.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/blueprint.hpp"

typedef ushort ushortBE;
typedef uint CollisionId;

struct CollisionVertex {
	ushortBE coords[3];

	bool operator==( const CollisionVertex &other ) const {
		return std::memcmp( this, &other, sizeof( CollisionVertex ) ) == 0;
	}
};
static_assert( sizeof( CollisionVertex ) == 6 );

struct CollisionFace {
	ushortBE vertexIndices[3];

	bool operator==( const CollisionFace &other ) const {
		return std::memcmp( this, &other, sizeof( CollisionFace ) ) == 0;
	}
};
static_assert( sizeof( CollisionFace ) == 6 );

namespace std {
	template <> struct hash<CollisionVertex> {
		size_t operator()(const CollisionVertex & x) const {
			if constexpr( sizeof( size_t ) >= 6 ) {
				return ((size_t)x.coords[0] << 32) | ((size_t)x.coords[1] << 16) | (size_t)x.coords[2];
			} else {
				return (((size_t)x.coords[0] << 16) | (size_t)x.coords[1] ) ^ ((size_t)x.coords[2] << 8);
			}
		}
	};
}

static inline CollisionId getCollisionId( const MaterialSpecs &mat ) {
	const auto &col = mat.collision;
	if( !Collision::hasDirection( col.type ) ) {
		return (CollisionId)col.type << 24;
	}
	const CollisionId id = ((CollisionId)col.type << 24) | ((CollisionId)col.direction << 8) | 1;
	if( col.type == CollisionType::HorizontalWind ) {
		return id;
	}
	return id | (CollisionId)col.force << 16;
}

static inline ushort getDeathPlaneExtent() {
	switch( Blueprint::current()->romSpecs().levelBoundaries ) {
		case LevelBoundarySize::Normal: return 0x2000;
		case LevelBoundarySize::Doubled: return 0x4000;
		case LevelBoundarySize::Maximum: return 0x7FCD;
		default: return 0;
	}
}

static ushort writeCollision(
	std::ostream &rom,
	std::optional<short> deathPlaneHeight,
	const HashMap<string,MaterialSpecs> &materials,
	const std::vector<WaterBoxSpecs> &waterBoxes,
	const fs::path &dataDir
) {
	writeHalfwordRaw( rom, 0x0040_be16 );

	HashMap<CollisionVertex,ushort> indexMap;
	HashMap<CollisionId,std::vector<CollisionFace>> facesMap;

	RomAddress vertexNumLocation = (uint)rom.tellp();
	writeHalfwordRaw( rom, 0 ); // placeholder

	ushort index = 0;
	for( const auto &i : materials ) {
		const MaterialSpecs &mat = i.second;
		if( !mat.solid ) continue;

		const CollisionId collisionId = getCollisionId( mat );

		const fs::path vfPath = dataDir / ( i.first + ".verts" );
		facesMap[collisionId].reserve( fs::file_size( vfPath ) / 18 );
		FileReadStream vertexFile( vfPath.u8string() );
		while( !vertexFile.eof() && !vertexFile.bad() ) {
			CollisionFace face;
			for( int i = 0; i < 3; i++ ) {
				CollisionVertex vertex;
				vertexFile.read( (char*)&vertex, 6 );
				const auto maybeIndex = indexMap.find( vertex );
				if( maybeIndex == indexMap.end() ) {
					rom.write( (const char*)&vertex, 6 );
					face.vertexIndices[i] = htons( index );
					indexMap[vertex] = index++;
				} else {
					face.vertexIndices[i] = htons( maybeIndex->second );
				}
			}
			facesMap[collisionId].push_back( face );
		}

		if( !vertexFile.eof() && vertexFile.bad() ) {
			throw RomError( "Unexpected error writing collision data to ROM" );
		}

	}

	ushort killPlaneVertices[9];
	if( deathPlaneHeight.has_value() ) {
		const short height = deathPlaneHeight.value() - 2048;

		const ushortBE p = htons( getDeathPlaneExtent() );
		const ushortBE n = htons( 0x10000 - getDeathPlaneExtent() );
		const ushortBE y = htons( reinterpret_cast<const ushort&>( height ) );
		const CollisionVertex killPlane[9] = {
			{{ n, y, n }},
			{{ 0, y, n }},
			{{ p, y, n }},
			{{ n, y, 0 }},
			{{ 0, y, 0 }},
			{{ p, y, 0 }},
			{{ n, y, p }},
			{{ 0, y, p }},
			{{ p, y, p }},
		};
		for( int i = 0; i < 9; i++ ) {
			const auto maybeIndex = indexMap.find( killPlane[i] );
			if( maybeIndex != indexMap.end() ) {
				killPlaneVertices[i] = maybeIndex->second;
			} else {
				rom.write( (const char*)&killPlane[i], 6 );
				indexMap[killPlane[i]] = index;
				killPlaneVertices[i] = index++;
			}
		}
	}

	writeHalfwordAt( rom, vertexNumLocation, index );

	for( const auto &i : facesMap ) {
		const CollisionId collisionId = i.first;
		const std::vector<CollisionFace> &faces = i.second;

		writeHalfword( rom, collisionId >> 24 );
		writeHalfword( rom, (ushort)faces.size() );
		for( const CollisionFace &face : faces ) {
			rom.write( (const char*)&face, 6 );
			if( (collisionId & 1) != 0 ) {
				writeHalfword( rom, (collisionId >> 8) & 0xFFFF );
			}
		}
	}

	if( deathPlaneHeight.has_value() ) {
		writeRaw<ubyte>( rom, { 0x00, (ubyte)CollisionType::DeathFloor, 0x00, 0x08 });
		writeHalfwords( rom, {
			killPlaneVertices[0],
			killPlaneVertices[3],
			killPlaneVertices[4],
			killPlaneVertices[4],
			killPlaneVertices[1],
			killPlaneVertices[0],

			killPlaneVertices[3],
			killPlaneVertices[6],
			killPlaneVertices[7],
			killPlaneVertices[7],
			killPlaneVertices[4],
			killPlaneVertices[3],

			killPlaneVertices[4],
			killPlaneVertices[7],
			killPlaneVertices[8],
			killPlaneVertices[8],
			killPlaneVertices[5],
			killPlaneVertices[4],

			killPlaneVertices[1],
			killPlaneVertices[4],
			killPlaneVertices[5],
			killPlaneVertices[5],
			killPlaneVertices[2],
			killPlaneVertices[1]
		});

	}

	writeHalfwordRaw( rom, 0x0041_be16 );
	writeWaterCollision( rom, waterBoxes );
	writeHalfwordRaw( rom, 0x0042_be16 );

	return index;
}

void writeAreaCollision(
	std::ostream &rom,
	const AreaSpecs &areaSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	const fs::path &areaDir
) {
	std::optional<short> deathPlaneHeight;
	if( areaSpecs.autoDeathFloor ) deathPlaneHeight = areaSpecs.voidHeight;
	writeCollision(
		rom,
		deathPlaneHeight,
		materials,
		areaSpecs.waterBoxes,
		areaDir
	);
}

ushort writeObjectCollision(
	std::ostream &rom,
	const HashMap<string,MaterialSpecs> &materials,
	const fs::path &objectDir
) {
	return writeCollision(
		rom,
		std::nullopt,
		materials,
		std::vector<WaterBoxSpecs>(),
		objectDir
	);
}
