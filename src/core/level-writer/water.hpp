#ifndef SRC_CORE_LEVEL_WRITER_WATER_HPP_
#define SRC_CORE_LEVEL_WRITER_WATER_HPP_

#include <ostream>
#include "src/core/level-writer/util.hpp"
#include "src/core/data/water.hpp"

extern void writeWaterCollision(
	std::ostream &rom,
	const std::vector<WaterBoxSpecs> &waterBoxes
);

extern void writeWaterVisuals(
	std::ostream &rom,
	uint mainSegmentStart,
	const HashMap<ushort,RomAddress> &waterDls,
	RomAddress &animationHead,
	std::map<string,uint> &asmRefs
);

#endif /* SRC_CORE_LEVEL_WRITER_WATER_HPP_ */
