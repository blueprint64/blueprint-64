#ifndef SRC_CORE_LEVEL_WRITER_MASTER_HPP_
#define SRC_CORE_LEVEL_WRITER_MASTER_HPP_

#include <vector>
#include <iostream>
#include <map>
#include "src/core/enums.hpp"
#include "src/core/actors.hpp"
#include "src/core/asm-modules.hpp"
#include "src/types.hpp"

namespace LevelWriter {

	extern void applyLevelAndAreaFlags( std::ostream &rom );

	extern void writeToRom(
		LevelId levelId,
		std::iostream &rom,
		const std::map<Uuid,CustomCamera> &cameraModules,
		const std::array<AreaActors,8> &actors,
		std::map<string,uint> &asmRefs,
		std::map<string,uint> &cameraTriggerRefs,
		bool cutoutDecal,
		float worldScale,
		std::vector<string> &warnings
	);

}


#endif /* SRC_CORE_LEVEL_WRITER_MASTER_HPP_ */
