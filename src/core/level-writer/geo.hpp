#ifndef SRC_CORE_LEVEL_WRITER_GEO_HPP_
#define SRC_CORE_LEVEL_WRITER_GEO_HPP_

#include <vector>
#include <ostream>
#include <unordered_map>
#include "src/core/level-writer/util.hpp"
#include "src/core/enums.hpp"
#include "src/core/data/area.hpp"
#include "src/core/data/material.hpp"
#include "src/core/data/object.hpp"

extern HashMap<DrawingLayer,RomAddress> writeAreaGeoLayout(
	std::ostream &rom,
	ushort areaId,
	short cameraId,
	uint cameraFuncPtr,
	const AreaSpecs &areaSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	HashMap<ushort,RomAddress> &waterDls,
	float worldScale,
	std::vector<string> &warnings
);

extern HashMap<DrawingLayer,RomAddress> writeObjectGeoLayout(
	std::ostream &rom,
	const ObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	RomAddress segmentStart,
	std::vector<string> &warnings
);

extern HashMap<DrawingLayer,RomAddress> writePartialObjectGeoLayout(
	std::ostream &rom,
	const PartialObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	std::vector<string> &warnings
);


#endif /* SRC_CORE_LEVEL_WRITER_GEO_HPP_ */
