#ifndef SRC_CORE_ASM_EXTENSIONS_HPP_
#define SRC_CORE_ASM_EXTENSIONS_HPP_

#include <ostream>
#include "src/types.hpp"

namespace AsmExtensions {
	extern void write( std::ostream &rom );
	extern void extendConsoleRenderLimit( std::ostream &rom, float worldScale );

	extern const uint geoObjectAlphaFuncPtr;
	extern const uint geoSwitchOpacityFuncPtr;

	extern const uint bossCameraTriggerFuncPtr;
	extern const uint defaultCameraTriggerFuncPtr;
	extern const uint transitionCameraTriggerFuncPtr;
	extern const uint radialCameraTriggerFuncPtr;
	extern const uint moduleCameraTriggerFuncPtr;
	extern const uint geoSetLookatFuncPtr;
	extern const uint revertLookatFuncPtr;

	extern const uint customCameraTablePointerRomLocation;
}


#endif /* SRC_CORE_ASM_EXTENSIONS_HPP_ */
