#include "src/core/emulator.hpp"

#include <map>
#include <regex>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/exec.hpp"
#include "src/core/util.hpp"
#include "src/core/storage.hpp"
#include "src/core/exceptions.hpp"

#ifdef _WIN32
#include "src/polyfill/window-slit.hpp"
static const string s_libraryExtension = ".dll";
#else
#include <cstdlib>
static const string s_libraryExtension = ".so";
#endif

#if _WIN32
static inline fs::path getRetroarchDefaultDir() {
	const fs::path defaultPath = Windows::getKnownFolderPath( Windows::KnownFolder::RoamingAppData ) / "RetroArch";
	fs::create_directories( defaultPath );
	return defaultPath;
}
#endif

void Emulator::RetroArch::Settings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
#ifdef _WIN32
		jw.writeProperty( "exe_path", exePath );
#else
		jw.writeProperty( "flatpak", useFlatpakInstall );
#endif
		jw.writeProperty( "core", core );
		jw.writeProperty( "cpu", cpuCore );
		jw.writeProperty( "fullscreen", fullscreen );
		jw.writeProperty( "scale", resolutionScale );
		jw.writeProperty( "vsync", vsync );
		jw.writeProperty( "full_speed", fullspeed );
		jw.writeProperty( "gfx", gfxPlugin );
		jw.writeProperty( "overclock", overclockVI );
	jw.writeObjectEnd();
}

Emulator::RetroArch::Settings Emulator::RetroArch::Settings::parseJSON( const Json &json ) {
#ifdef _WIN32
	static const fs::path defaultPath = getRetroarchDefaultDir();
#endif
	Settings settings = Settings{
#ifdef _WIN32
		json["exe_path"].getOrDefault<string>( (defaultPath / "retroarch.exe").u8string() ),
#else
		json["useFlatpakInstall"].getOrDefault<bool>( hasFlatpakInstall() ),
#endif
		json["core"].getOrDefault<EmulatorCore>( EmulatorCore::ParallelN64 ),
		json["cpu"].getOrDefault<CpuEmulation>( CpuEmulation::DynamicRecompiler ),
		json["gfx"].getOrDefault<GraphicsPlugin>( GraphicsPlugin::Glide64 ),
		json["fullscreen"].getOrDefault<bool>( false ),
		json["scale"].getOrDefault<ubyte>( 4 ),
		json["vsync"].getOrDefault<bool>( false ),
		json["full_speed"].getOrDefault<bool>( true ),
		json["overclock"].getOrDefault<bool>( true )
	};

	return settings;
}

void Emulator::Mupen64Plus::Settings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
#ifdef _WIN32
		jw.writeProperty( "exe_path", exePath );
#endif
		jw.writeProperty( "plugin_directory", pluginDirPath );
		jw.writeProperty( "cpu", cpuCore );
		jw.writeProperty( "gfx", gfxPlugin );
		jw.writeProperty( "rsp", rspPlugin );
		jw.writeProperty( "fullscreen", fullscreen );
		jw.writeProperty( "scale", resolutionScale );
		jw.writeProperty( "osd", onScreenDisplay );
	jw.writeObjectEnd();
}

Emulator::Mupen64Plus::Settings Emulator::Mupen64Plus::Settings::parseJSON( const Json &json ) {
#ifdef _WIN32
	static const fs::path defaultPath = getHomeDirectory() / "Downloads" / "mupen64plus";
#endif
	return Settings{
#ifdef _WIN32
		json["exe_path"].getOrDefault<string>( (defaultPath / "mupen64plus-ui-console.exe").u8string() ),
		json["plugin_directory"].getOrDefault<string>( defaultPath.u8string() ),
#else
		json["plugin_directory"].getOrDefault<string>( "/usr/lib/x86_64-linux-gnu/mupen64plus" ),
#endif
		json["cpu"].getOrDefault<CpuEmulation>( CpuEmulation::DynamicRecompiler ),
		json["gfx"].getOrDefault<string>( "glide64mk2" ),
		json["rsp"].getOrDefault<string>( "hle" ),
		json["fullscreen"].getOrDefault<bool>( false ),
		json["scale"].getOrDefault<ubyte>( 4 ),
		json["osd"].getOrDefault<bool>( true )
	};
}

#ifdef _WIN32
void Emulator::Project64::Settings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
		jw.writeProperty( "exe_path", exePath );
		jw.writeProperty( "full_version", fullVersion );
		jw.writeProperty( "major_version", majorVersion );
	jw.writeObjectEnd();
}

Emulator::Project64::Settings Emulator::Project64::Settings::parseJSON( const Json &json ) {
	return Settings {
		json["exe_path"].getOrDefault<string>( "" ),
		json["full_version"].getOrDefault<string>( "" ),
		json["major_version"].getOrDefault<ushort>( 0 )
	};
}
#endif

void Emulator::EmulatorSettings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
		jw.writeProperty( "perferred_emulator", preferredEmulator );
		jw.writePropertyName( "retroarch_config" );
		retroSettings.writeJson( jw );
		jw.writePropertyName( "mupen_config" );
		mupenSettings.writeJson( jw );
#ifdef _WIN32
		jw.writePropertyName( "pj64_config" );
		pj64Settings.writeJson( jw );
		jw.writeProperty( "pl_path", parallelLauncherPath );
#endif
		jw.writeProperty( "other_emu_cmd", otherEmulatorCmd );
	jw.writeObjectEnd();
}

Emulator::EmulatorSettings Emulator::EmulatorSettings::parseJSON( const Json &json ) {
	return EmulatorSettings{
		json["perferred_emulator"].getOrDefault<Emulator>( Emulator::ParallelLauncher ),
		RetroArch::Settings::parseJSON( json["retroarch_config"] ),
		Mupen64Plus::Settings::parseJSON( json["mupen_config"] ),
#ifdef _WIN32
		Project64::Settings::parseJSON( json["pj64_config"] ),
		json["pl_path"].getOrDefault<string>( "C:\\Program Files (x86)\\parallel-launcher\\parallel-launcher.exe" ),
#endif
		json["other_emu_cmd"].getOrDefault<string>( "cen64 -multithread ~/Documents/pifdata.bin \"%s\"" )
	};
}

static fs::path expand_path( const fs::path &path ) {
	const string pathStr = path.u8string();
	if( pathStr == "~" ) {
		return getHomeDirectory();
	} else if( pathStr.length() >= 2 && pathStr[0] == '~' && (pathStr[1] == '/' || pathStr[1] == '\\' ) ) {
		return getHomeDirectory() / fs::to_path( &path.u8string().c_str()[2] );
	} else return path;
}

static inline bool startsWith( const string &str, const string &prefix ) {
	return str.compare( 0, prefix.length(), prefix ) == 0;
}

static std::vector<string> getMupenPlugins( const string &prefix, const string &pluginDir ) {
	const fs::path pluginsPath = fs::to_path( pluginDir );
	std::vector<string> plugins;

	if( !fs::exists( pluginsPath ) ) {
		return plugins;
	}

	for( const auto &p : fs::directory_iterator( pluginsPath ) ) {
		if( p.status().type() != fs::file_type::regular ) continue;
		if( p.path().extension().u8string() != s_libraryExtension ) continue;
		if( !startsWith( p.path().stem().u8string(), prefix ) ) continue;

		plugins.push_back( p.path().stem().u8string().substr( prefix.length() ) );
	}

	return plugins;
}

std::vector<string> Emulator::Mupen64Plus::getGfxPlugins( const string &pluginDir ) {
	return getMupenPlugins( "mupen64plus-video-", pluginDir );
}

std::vector<string> Emulator::Mupen64Plus::getRspPlugins( const string &pluginDir ) {
	return getMupenPlugins( "mupen64plus-rsp-", pluginDir );
}

static const string &getRetroCfgPath() {
	static const string s_path = (AppData::cacheDir() / "retroarch.cfg").u8string().c_str();
	return s_path;
}

static const string &getRetroCoreCfgPath() {
	static const string s_path = (AppData::cacheDir() / "retroarch-core-options.cfg").u8string().c_str();
	return s_path;
}

static const std::regex s_cfgRegex(
	"^([^=]+)\\s*=\\s*\"([^\"]*)\"",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static void readRetroConfig( const string &config, std::map<string,string> &options, [[maybe_unused]] bool usingFlatpak ) {
#if _WIN32
	fs::path configPath = AppData::cacheDir() / config;
#else
	fs::path configPath;
	if( usingFlatpak ) {
		configPath = getHomeDirectory() / ".var" / "app" / "org.libretro.RetroArch" / "config" / "retroarch" / config;
	} else {
		configPath = AppData::cacheDir() / config;
	}
#endif
	if( !fs::exists( configPath ) ) {
#if _WIN32
		static const fs::path fallbackPath = getRetroarchDefaultDir();
		configPath = fallbackPath / config;
#else
		configPath = AppData::configDir().parent_path() / "retroarch" / config;
		if( !fs::exists( configPath ) ) {
			configPath = fs::to_path( "~/."s + config );
			if( !fs::exists( configPath ) ) return;
		}
#endif
	}

	FileReadStream cfgFile( configPath.u8string(), std::ios_base::in );
	while( cfgFile.good() && !cfgFile.eof() ) {
		string cfgLine;
		std::getline( cfgFile, cfgLine );

		std::smatch matches;
		if( !cfgLine.empty() && std::regex_search( cfgLine, matches, s_cfgRegex ) ) {
			options[trimEnd( matches[1] )] = matches[2];
		}
	}
}

static inline std::map<string,string> getBaseRetroConfig( bool usingFlatpak ) {
	/* The --appendconfig command line option does not override properties in the
	 * main config file because RetroArch is stupid and also dumb. So instead, create
	 * a whole new config file by copying the base config and overriding the options.
	 */
	std::map<string,string> options;
	readRetroConfig( "retroarch.cfg", options, usingFlatpak );
	readRetroConfig( "retroarch-core-options.cfg", options, usingFlatpak );
	//TODO: read existing BB config
	return options;
}

string Emulator::RetroArch::Settings::createConfigFile() const {
	const int width = 320 * (int)resolutionScale;
	const int height = 240 * (int)resolutionScale;
	const string resolution = std::to_string( width ) + "x" + std::to_string( height );
	const int paraLLElUpscaling = ((resolutionScale == 1) || (resolutionScale == 2) || (resolutionScale == 8)) ? resolutionScale : 4;

#ifdef _WIN32
	std::map<string,string> options = getBaseRetroConfig( false );
#else
	std::map<string,string> options = getBaseRetroConfig( useFlatpakInstall );
#endif
	options["suspend_screensaver_enable"] = "true";
	options["video_scale_integer"] = "true";
	options["video_force_aspect"] = "true";
	options["video_window_save_positions"] = "true";
	options["parallel-n64-gfxplugin-accuracy"] = "veryhigh";
	options["parallel-n64-parallel-rdp-native-texture-lod"] = "true";
	options["mupen64plus-parallel-rdp-native-texture-lod"] = "True";
	options["global_core_options"] = "true";
	options["config_save_on_exit"] = "true";
	options["save_file_compression"] = "false";
	options["video_frame_delay"] = "0";
	options["parallel-n64-rspplugin"] = "auto";
	options["mupen64plus-aspect"] = "4:3";
	options["mupen64plus-EnableFBEmulation"] = "True";

	options["video_fullscreen"] = fullscreen ? "true" : "false";
	options["video_vsync"] = vsync ? "true" : "false";
	options["parallel-n64-framerate"] = fullspeed ? "fullspeed" : "original";
	options["mupen64plus-Framerate"] = fullspeed ? "Fullspeed" : "Original";
	options["mupen64plus-virefresh"] = overclockVI ? "2200" : "Auto";
	options["parallel-n64-virefresh"] = overclockVI ? "2200" : "auto";

	options["video_windowed_position_width"] = options["custom_viewport_width"] = std::to_string( width );
	options["video_windowed_position_height"] = options["custom_viewport_height"] = std::to_string( height );
	options["parallel-n64-screensize"] = resolution;
	options["mupen64plus-43screensize"] = resolution;
	options["parallel-n64-parallel-rdp-upscaling"] = std::to_string( paraLLElUpscaling );
	options["mupen64plus-parallel-rdp-upscaling"] = std::to_string( paraLLElUpscaling ) + 'x';

	switch( cpuCore ) {
		case CpuEmulation::DynamicRecompiler:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "dynamic_recompiler";
			break;
		case CpuEmulation::CachedInterpreter:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "cached_interpreter";
			break;
		case CpuEmulation::PureInterpreter:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "pure_interpreter";
			break;
	}

	options["parallel-n64-gfxplugin-accuracy"] = "veryhigh";
	switch( gfxPlugin ) {
		case GraphicsPlugin::ParaLLEl:
			options["parallel-n64-gfxplugin"] = "parallel";
			options["mupen64plus-rdp-plugin"] = "parallel";
			break;
		case GraphicsPlugin::Angrylion:
			options["parallel-n64-gfxplugin"] = "angrylion";
			options["mupen64plus-rdp-plugin"] = "angrylion";
			break;
		case GraphicsPlugin::Glide64:
			options["parallel-n64-gfxplugin"] = "glide64";
			options["mupen64plus-rdp-plugin"] = "gliden64";
			break;
		case GraphicsPlugin::Rice:
			options["parallel-n64-gfxplugin"] = "rice";
			options["mupen64plus-rdp-plugin"] = "gliden64";
			break;
		case GraphicsPlugin::GlideN64:
			options["parallel-n64-gfxplugin"] = "gln64";
			options["mupen64plus-rdp-plugin"] = "gliden64";
			break;
	}

	FileWriteStream baseConfig( getRetroCfgPath() );
	FileWriteStream coreConfig( getRetroCoreCfgPath() );

	for( const auto &i : options ) {
		const string &key = i.first;
		const string &value = i.second;

		if( startsWith( key, "parallel-n64-" ) || startsWith( key, "mupen64plus-" ) ) {
			coreConfig << key << " = \"" << value << "\"" << std::endl;
		} else {
			baseConfig << key << " = \"" << value << "\"" << std::endl;
		}
	}
	baseConfig << std::endl;
	coreConfig << std::endl;

	if( options.count( "libretro_directory" ) == 0 || options["libretro_directory"].empty() ) {
		throw EmulatorLoadException( "RetroArch cores directory not found" );
	}

	return expand_path( fs::to_path( options.at( "libretro_directory" ) ) ).u8string();
}

static inline bool launchRetroArch( const string &romPath, const Emulator::RetroArch::Settings &config, std::error_code &err ) {
	static const string s_parallelCore = "parallel_n64_libretro" + s_libraryExtension;
	static const string s_mupenCore = "mupen64plus_next_libretro" + s_libraryExtension;

	const fs::path corePath = fs::to_path( config.createConfigFile() ) / (config.core == Emulator::RetroArch::EmulatorCore::Mupen64plusNext ? s_mupenCore : s_parallelCore);

#ifdef _WIN32
	string retroArchPath = config.exePath;
	if( retroArchPath.empty() ) {
		retroArchPath = "retroarch";
	} else {
		retroArchPath = quoteAndEscape( retroArchPath );
	}
#else
	const string retroArchPath = config.useFlatpakInstall ? "flatpak run org.libretro.RetroArch" : "retroarch";
#endif

	return Command::execDetached(
		retroArchPath +
		" -L "s + quoteAndEscape( corePath.u8string() ) +
		" --config " + quoteAndEscape( getRetroCfgPath() ) +
		" " + quoteAndEscape( romPath ),
		err
	);
}

static inline bool launchMupen64Plus( const string &romPath, const Emulator::Mupen64Plus::Settings &config, std::error_code &err ) {
#ifdef _WIN32
	string mupenPath = AppPreferences::current().emuConfig.mupenSettings.exePath;
	if( mupenPath.empty() ) {
		mupenPath = "mupen64plus";
	} else {
		mupenPath = quoteAndEscape( mupenPath );
	}
#else
	const string mupenPath = "mupen64plus";
#endif

	const string resolution = std::to_string( 320 * (int)config.resolutionScale ) + "x" + std::to_string( 240 * (int)config.resolutionScale );
	return Command::execDetached(
		mupenPath +
		(config.onScreenDisplay ? " --osd" : " --noosd") +
		(config.fullscreen ? " --fullscreen" : " --windowed" ) +
		" --resolution " + resolution +
		" --plugindir " + quoteAndEscape( config.pluginDirPath ) +
		" --gfx mupen64plus-video-" + config.gfxPlugin +
		" --rsp mupen64plus-rsp-" + config.rspPlugin +
		" --emumode " + std::to_string( 2 - (int)config.cpuCore ) +
		" " + quoteAndEscape( romPath ),
		err
	);
}

static inline bool launchOtherEmulator( const string &romPath, const string &cmd, std::error_code &err ) {
	if( cmd.empty() ) return false;

	string processedCmd;
	bool replace = false;
	for( char c : cmd ) {
		if( replace ) {
			switch( c ) {
				case '%': break;
				case 's': processedCmd += romPath; break;
				default: processedCmd += "%"s + c; break;
			}
			replace = false;
		} else if( c == '%' ) {
			replace = true;
		} else {
			processedCmd += c;
		}
	}

	if( replace ) {
		processedCmd += '%';
	}

	return Command::execDetached( processedCmd, err );
}

#ifdef _WIN32
static inline bool launchParallelLauncher( const string &romPath, const string &launcherPath, std::error_code &err ) {
	return Command::execDetached( quoteAndEscape( launcherPath ) + ' ' + quoteAndEscape( romPath ), err );
}
#else
static inline bool launchParallelLauncher( const string &romPath, std::error_code &err ) {
	return Command::execDetached( "parallel-launcher "s + quoteAndEscape( romPath ), err );
}
#endif

#ifdef _WIN32
static const std::regex s_pjRegex(
	"^Project64 (\\d{1,4})(\\.\\d.*)$",
	std::regex_constants::ECMAScript | std::regex_constants::optimize | std::regex_constants::icase
);

std::vector<Emulator::Project64::Settings> Emulator::Project64::getInstalledVersions() {
	std::vector<Settings> versions;
	const fs::path programFiles( Windows::getKnownFolderPath( Windows::KnownFolder::ProgramFiles32 ) );
	for( auto &i : fs::directory_iterator( programFiles ) ) {
		if( !i.is_directory() ) continue;
		if( !fs::exists( i.path() / "Project64.exe" ) ) continue;

		std::smatch matches;
		const std::string pathString = i.path().filename().u8string();
		if( std::regex_search( pathString, matches, s_pjRegex ) ) {
			versions.push_back({
				(i.path() / "Project64.exe").u8string(),
				(string)matches[1] + (string)matches[2],
				(ushort)std::stoi( matches[1], nullptr, 10 )
			});
		}
	}
	return versions;
}

static inline bool launchProject64( const string &romPath, const Emulator::Project64::Settings &config, std::error_code &err ) {
	const string cmdBase = quoteAndEscape( config.exePath ) + ' ';
	if( config.majorVersion < 2 ) {
		return Command::execDetached( cmdBase + romPath, err );
	} else {
		return Command::execDetached( cmdBase + quoteAndEscape( romPath ), err );
	}
}
#endif

bool Emulator::launch( const string &romPath, std::error_code &err ) {
	const EmulatorSettings &config = AppPreferences::current().emuConfig;
	switch( config.preferredEmulator ) {
		case Emulator::ParallelLauncher:
#ifdef _WIN32
			return launchParallelLauncher( romPath, config.parallelLauncherPath, err );
#else
			return launchParallelLauncher( romPath, err );
#endif
		case Emulator::RetroArch:
			return launchRetroArch( romPath, config.retroSettings, err );
		case Emulator::Mupen64Plus:
			return launchMupen64Plus( romPath, config.mupenSettings, err );
#ifdef _WIN32
		case Emulator::Project64:
			return launchProject64( romPath, config.pj64Settings, err );
#endif
		default:
			return launchOtherEmulator( romPath, config.otherEmulatorCmd, err );
	}
}

bool Emulator::emulatorInstalled() {
	std::error_code err;
	const EmulatorSettings &config = AppPreferences::current().emuConfig;
	switch( config.preferredEmulator ) {
		case Emulator::ParallelLauncher:
#ifdef _WIN32
			return fs::exists( fs::to_path( config.parallelLauncherPath ) );
#else
			return Command::execSync( "which parallel-launcher > /dev/null", err );
#endif
		case Emulator::RetroArch:
#ifdef _WIN32
			return fs::exists( fs::to_path( config.retroSettings.exePath ) );
#else
			return config.retroSettings.useFlatpakInstall ? RetroArch::hasFlatpakInstall() : Command::execSync( "which retroarch > /dev/null", err );
#endif
		case Emulator::Mupen64Plus:
#ifdef _WIN32
			return fs::exists( fs::to_path( config.mupenSettings.exePath ) );
#else
			return Command::execSync( "which mupen64plus > /dev/null", err );
#endif
#ifdef _WIN32
		case Emulator::Project64:
			return !config.pj64Settings.exePath.empty() && fs::exists( fs::to_path( config.pj64Settings.exePath ) );
#endif
		default:
			return !config.otherEmulatorCmd.empty();
	}
}

#ifndef _WIN32
bool Emulator::RetroArch::hasFlatpakInstall() {
	return(
		std::system( "which flatpak > /dev/null" ) == 0 &&
		std::system( "flatpak info org.libretro.RetroArch > /dev/null" ) == 0
	);
}
#endif
