#ifndef SRC_CORE_ALIGNMENT_FIX_HPP_
#define SRC_CORE_ALIGNMENT_FIX_HPP_

#include <ostream>

extern void fixMisalignedSegments( std::ostream &rom );

#endif /* SRC_CORE_ALIGNMENT_FIX_HPP_ */
