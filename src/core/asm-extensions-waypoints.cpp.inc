static const uint s_vanillaWaypoints[22] = {
	0x07011530, // metal balls 1 (BOB Upper)
	0x070170A0, // metal balls 3 (TTM)
	0x070115C4, // metal balls 2 (BOB Lower)
	0x003326C4, // metal balls 5 (THI Big)
	0x00332718, // metal balls 4 (THI Smol)
	0x070116A0, // koopa the quick (BOB)
	0x0700E258, // koopa the quick (THI)
	0x0702EA80, // track 1 (carpet 1)
	0x0702EB04, // track 2 (carpet 2)
	0x0701669C, // track 3 (ski lift)
	0x070159AC, // track 4 (BitFS Grate)
	0x0702B86C, // track 5 (HMC Track)
	0x070284DC, // track 6 (LLL Track 1)
	0x070285D0, // track 7 (LLL Track 2)
	0x0702EBE0, // track 8 (carpet 3)
	0x0702ED24, // track 9 (carpet 4)
	0x07023604, // penguin race
	0x003327B8, // manta ray
	0x003306DC, // ukiki
	0x0700D20C, // unagi 1
	0x0700D240, // unagi 2
	0x070165A8  // snowman's head
};

#define WRITE_SIMPLE_SHIM( shimAddr, waypointId ) \
	rom.seekp( shimAddr ); \
	rom.write( (const char*)&jalCmd, 4 ); \
	rom.write( (const char*)oriCmd, 3 ); \
	rom.put( waypointId );

static inline void shimWaypoints( std::ostream &rom, uint functionAddr ) {
	rom.seekp( 0x3C9F054 );
	for( int i = 0; i < 22; i++ ) {
		const uint segPtr = htonl( s_vanillaWaypoints[i] );
		rom.write( (const char*)&segPtr, 4 );
	}
	if( !rom.good() ) {
		throw RomError( "Error writing to ROM file. Do you have write permission on the file?" );
	}
	assert( rom.tellp() == 0x3C9F0AC );

	const uint jalCmd = htonl( 0x0C000000 | ((functionAddr & 0x0FFFFFFFu) >> 2) );
	const ubyte oriCmd[] = { 0x34, 0x04, 0x00 };

	const ubyte ukikiAndMantaRayHelper[] = {
		0x00, 0x00, 0x00, 0x00, // NOP
		0x3C, 0x08, 0x80, 0x36, // LUI T0, 0x8036
		0x8D, 0x08, 0x11, 0x60, // LW T0, 0x1160 (T0)
		0xAD, 0x02, 0x00, 0xFC  // SW V0, 0xFC (T0)
	};

	rom.seekp( 0x758CC );
	rom.write( (const char*)&jalCmd, 4 );
	rom.write( (const char*)ukikiAndMantaRayHelper, sizeof( ukikiAndMantaRayHelper ) );

	/* Metal Balls */
	rom.seekp( 0xA9A8C );
	rom.write( (const char*)&jalCmd, 4 );
	const ubyte mbShim[] = {
		0x8D, 0xC4, 0x01, 0x44, // LW A0, 0x144 (T6)
		0x3C, 0x08, 0x80, 0x36, // LUI T0, 0x8036
		0x8D, 0x08, 0x11, 0x60, // LW T0, 0x1160 (T0)
		0xAD, 0x02, 0x00, 0xFC, // SW V0, 0xFC (T0)
		0x8F, 0xBF, 0x00, 0x14, // LW RA, 0x14 (SP)
		0x03, 0xE0, 0x00, 0x08, // JR RA
		0x27, 0xBD, 0x00, 0x18  // ADDIU SP, SP, 0x18
	};
	rom.write( (const char*)mbShim, sizeof( mbShim ) );

	/* Koopa the Quick */
	rom.seekp( 0xB7E00 );
	rom.write( (const char*)&jalCmd, 4 );
	const ubyte ktqShim[] = {
		0x25, 0xA4, 0x00, 0x05 // ADDIU A0, T5, 0x5
	};
	rom.write( (const char*)ktqShim, sizeof( ktqShim ) );

	/* Platform on Track */
	rom.seekp( 0xC01AC );
	rom.write( (const char*)&jalCmd, 4 );
	const ubyte potShim[] = {
		0x27, 0x04,0x00, 0x07 // ADDIU A0, T8, 0x7
	};
	rom.write( (const char*)potShim, sizeof( potShim ) );

	/* Penguin Race */
	WRITE_SIMPLE_SHIM( 0xCCA70, 16 )

	/* Manta Ray */
	const ubyte mrShim[] = {
		0x0C, 0x0A, 0xEA, 0x31, // JAL ukikiAndMantaRayHelper
		0x34, 0x04, 0x00, 0x11  // ORI A0, R0, 0x11
	};
	rom.seekp( 0xB1ED4 );
	rom.write( (const char*)mrShim, 8 );

	/* Ukiki */
	const ubyte uShim[] = {
		0x0C, 0x0A, 0xEA, 0x31, // JAL ukikiAndMantaRayHelper
		0x34, 0x04, 0x00, 0x12  // ORI A0, R0, 0x12
	};
	rom.seekp( 0x7647C );
	rom.write( (const char*)uShim, 8 );

	/* Unagi the Eel */
	WRITE_SIMPLE_SHIM( 0xC51E4, 19 )
	WRITE_SIMPLE_SHIM( 0xC523C, 20 )

	/* Snowman's Head */
	WRITE_SIMPLE_SHIM( 0xABCA0, 21 )

}

#undef WRITE_SIMPLE_SHIM
