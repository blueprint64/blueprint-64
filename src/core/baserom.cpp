#include "src/core/baserom.hpp"

#include <cstring>
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/buffer.hpp"

const fs::path &BaseRom::filePath() {
	static const fs::path s_path = AppData::cacheDir() / "baserom.z64";
	return s_path;
}

bool BaseRom::matches( std::istream &rom, uint bytes ) {
	FileReadStream baseRom( BaseRom::filePath().u8string() );
	Buffer bufferA( bytes );
	Buffer bufferB( bytes );

	baseRom.seekg( rom.tellg() );
	baseRom.read( bufferA.data(), bytes );
	rom.read( bufferB.data(), bytes );

	return rom.good() && baseRom.good() && std::memcpy( bufferA.data(), bufferB.data(), (size_t)bytes ) == 0;
}

void BaseRom::revert( std::ostream &rom, uint bytes ) {
	FileReadStream baseRom( BaseRom::filePath().u8string() );
	Buffer buffer( bytes );

	baseRom.seekg( rom.tellp() );
	baseRom.read( buffer.data(), bytes );
	rom.write( buffer.data(), bytes );
}

void BaseRom::copyTo( std::ostream &rom, uint posn, uint bytes ) {
	FileReadStream baseRom( BaseRom::filePath().u8string() );
	Buffer buffer( bytes );

	baseRom.seekg( posn );
	baseRom.read( buffer.data(), bytes );
	rom.write( buffer.data(), bytes );
}

uint BaseRom::getWord( uint posn ) {
	FileReadStream baseRom( BaseRom::filePath().u8string() );
	uint word;
	baseRom.seekg( posn );
	baseRom.read( (char*)&word, 4 );
	return ntohl( word );
}

const std::map<string,VanillaWaterTexture> &BaseRom::waterTextures() {
	static const std::map<string,VanillaWaterTexture> s_water({
		{ "18a04966c955a3616e289a6fe83eef64", { 0x02016AB8, TextureFormat::RGBA16 } },
		{ "6c3bdd69e099a0d8c6fb8421edddd09d", { 0x02015AB8, TextureFormat::RGBA16 } },
		{ "bece1f48cb87b874f118f4f695bde306", { 0x020162B8, TextureFormat::IA16 } },
		{ "d7a61aa7a30f31f421bb7f5d801ee254", { 0x020152B8, TextureFormat::RGBA16 } },
		{ "f615067efa00c1112ac503d006781c24", { 0x02014AB8, TextureFormat::RGBA16 } }
	});
	return s_water;
}
