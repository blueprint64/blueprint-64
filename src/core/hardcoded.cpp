#include "src/core/hardcoded.hpp"

#include "src/types.hpp"
#include "src/core/baserom.hpp"

#define ROM_WRITE_BYTES(...) { \
	const ubyte __patch[] = { __VA_ARGS__ }; \
	rom.write( (const char*)__patch, sizeof(__patch) ); \
}

template<bool remove> inline void removeOrRestore( std::ostream &rom, LevelId levelId ) {

	if constexpr( !remove ) {
		rom.seekp( 0xE9CB0 + ((uint)levelId << 2) );
		BaseRom::revert( rom, 4 );
	}

	//TODO: How to deal with Mario's lost hat?
	switch( levelId ) {
		case LevelId::CastleGrounds: {
			// Hardcoded camera code
			rom.seekp( 0x425A0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00, 0x00, 0x52 )
			else					ROM_WRITE_BYTES( 0x3C, 0x01, 0xC4, 0xA6 )

			return;
		}
		case LevelId::CastleInterior: {
			// Disable rooms (TODO: room support)
			rom.seekp( 0xEB015 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xFF )
			else					ROM_WRITE_BYTES( 0x06 )

			// Hardcoded camera code
			rom.seekp( 0x426BC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0xE5, 0xD0, 0x00, 0x08 )

			rom.seekp( 0x50344 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			rom.seekp( 0x3F888 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x81 )

			rom.seekp( 0x41D98 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0x01, 0x00, 0x58 )

			rom.seekp( 0x3D2C8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x0E )

			rom.seekp( 0x47338 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x2A )
			else					ROM_WRITE_BYTES( 0x03 )

			rom.seekp( 0x488A4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x10 )

			rom.seekp( 0xE9CC8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE3, 0xB0 )

			// Instant warps should still work after collecting 70 stars
			rom.seekp( 0x5388 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			// Disable infinite stairs music when in the right position and area with < 70 stars
			rom.seekp( 0xFAD4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x0C, 0x09, 0x24, 0x1C )

			// Leave in the warp sound effect since it's probably still going to be the hub world
			return;
		}
		case LevelId::CastleCourtyard: {
			// Hardcoded camera code
			rom.seekp( 0x42688 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0xE5, 0x4A, 0x00, 0x08 )

			return;
		}
		case LevelId::BobOmbBattlefield: {
			// Hardcoded camera code
			rom.seekp( 0x3B6C8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x1C )

			rom.seekp( 0x3D9AC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x06 )

			rom.seekp( 0x4A298 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x69 )

			// Pink Bob-Omb that opens the cannon has different text in BOB, but this is left in since it
			// may be desirable for some

			return;
		}
		case LevelId::WhompsFortress: {
			// Hardcoded camera code
			rom.seekp( 0x3D9B8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x09 )

			rom.seekp( 0x4A260 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x17 )

			// Whomp's Fortress hides all piranha plants if Mario is above y = 3400
			rom.seekp( 0x795C0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )
			return;
		}
		case LevelId::JollyRodgerBay: {
			// Hardcoded camera code
			rom.seekp( 0x4D6F4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			// Conditional Background Music Volume Changes
			rom.seekp( 0xEDFF7 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xB8 )
			else					ROM_WRITE_BYTES( 0x6C )

			// Skybox becomes slightly darker and greener after collecting a star
			rom.seekp( 0x8AF50 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			return;
		}
		case LevelId::CoolCoolMountain:
			// Hardcoded camera code
			rom.seekp( 0xE9CC4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE3, 0x68 )

			rom.seekp( 0x4DC94 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x61 )

			rom.seekp( 0x42720 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x3E )

			rom.seekp( 0x4272C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x19 )

			return;
		case LevelId::BigBoosHaunt: {
			// Disable rooms (TODO: room support)
			rom.seekp( 0xEB014 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xFF )
			else					ROM_WRITE_BYTES( 0x04 )

			// Hardcoded camera code
			rom.seekp( 0xE9CC0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE6, 0xF8 )

			rom.seekp( 0x3DA04 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x21 )

			rom.seekp( 0x3DCF8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			rom.seekp( 0x3D2BC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x19 )

			rom.seekp( 0x47328 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x1D )

			rom.seekp( 0x4A274 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x44 )

			// Conditional Background Music Volume Changes
			rom.seekp( 0xEDFD7 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xB8 )
			else					ROM_WRITE_BYTES( 0x48 )

			// Flamethrower doesn't activate in BBH if you are not on the merry-go-round
			rom.seekp( 0x6A210 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x01 )

			return;
		}
		case LevelId::HazyMazeCave: {
			// Disable rooms (TODO: room support)
			rom.seekp( 0xEB016 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xFF )
			else					ROM_WRITE_BYTES( 0x07 )

			// Hardcoded camera code
			rom.seekp( 0xE9CCC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE1, 0x28 )

			rom.seekp( 0x50380 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			rom.seekp( 0x513D4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x0C )

			rom.seekp( 0x51570 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xE1, 0x00, 0x07 )

			rom.seekp( 0x51A80 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x06 )

			// Conditional Background Music Volume Changes
			rom.seekp( 0xEDFE3 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xB8 )
			else					ROM_WRITE_BYTES( 0x98 )

			return;
		}
		case LevelId::LethalLavaLand: {
			// Hardcoded camera code
			rom.seekp( 0x426A4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0xA4, 0x2B, 0xC8, 0x48 )

			rom.seekp( 0x4655B );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x3A )
			else					ROM_WRITE_BYTES( 0x37 )

			// Lethal Lava Land disables water sound effects
			rom.seekp( 0xC8E4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0x41, 0x00, 0x0E )

			// Lethal Lava Land adjusts the position of Mario's shadow while he is over lava
			rom.seekp( 0x89740 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			return;
		}
		case LevelId::ShiftingSandLand: {
			// Hardcoded camera code
			rom.seekp( 0xE9CD0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE1, 0xD0 )

			rom.seekp( 0x4A280 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x5F )

			rom.seekp( 0x4A28C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x57 )

			rom.seekp( 0x3C2DC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			rom.seekp( 0x3F800 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x21 )

			rom.seekp( 0x3F874 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xA1, 0x00, 0x06 )

			rom.seekp( 0x46540 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x27 )

			rom.seekp( 0x42738 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x0F )

			return;
		}
		case LevelId::DireDireDocks: {
			// Hardcoded camera code
			rom.seekp( 0x48984 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0x61, 0x00, 0x07 )

			rom.seekp( 0x4D72C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x01 )

			// Conditional Background Music Volume Changes
			rom.seekp( 0xEE023 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xB8 )
			else					ROM_WRITE_BYTES( 0x54 )

			// Whirlpool radius is doubled in DDD
			rom.seekp( 0x2B738 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			rom.seekp( 0x4A248 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0xB7 )

			rom.seekp( 0x4A254 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0xB9 )

			rom.seekp( 0x3BAA0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			return;
		}
		case LevelId::SnowmansLands: {
			// Hardcoded camera code
			rom.seekp( 0xE9CD8 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE0, 0x98 )

			rom.seekp( 0x4D888 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			rom.seekp( 0x4DD04 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			rom.seekp( 0x42744 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x25 )

			rom.seekp( 0x42750 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x1C )

			return;
		}
		case LevelId::WetDryWorld: {
			// Hardcoded camera code
			rom.seekp( 0x48990 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0x61, 0x00, 0x04 )

			rom.seekp( 0x4D764 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			rom.seekp( 0x3B6D4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x09 )

			rom.seekp( 0x3E250 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			rom.seekp( 0x4A2A4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x84 )

			rom.seekp( 0x4A2B0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x8F )

			rom.seekp( 0x3FC50 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x61 )

			// Conditional Background Music Volume Changes
			rom.seekp( 0xEDFF3 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0xB8 )
			else					ROM_WRITE_BYTES( 0x88 )

			return;
		}
		case LevelId::TallTallMountain: {
			// Hardcoded camera code
			rom.seekp( 0x4F4EC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x61 )

			rom.seekp( 0x53804 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			rom.seekp( 0x53B58 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xC1 )

			rom.seekp( 0x4DCCC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xA1 )

			rom.seekp( 0x411C4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			rom.seekp( 0x42768 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x33 )

			return;
		}
		case LevelId::TinyHugeIsland: {
			// Hardcoded camera code
			rom.seekp( 0xE9CE4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE0, 0xE0 )

			rom.seekp( 0x42B64 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xA1, 0x00, 0x06 )

			rom.seekp( 0x3B6E3 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x42 )
			else					ROM_WRITE_BYTES( 0x24 )

			rom.seekp( 0x3B6E3 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x3F )
			else					ROM_WRITE_BYTES( 0x2F )

			rom.seekp( 0x3E29C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xA1 )

			rom.seekp( 0x3E2E0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			rom.seekp( 0x4A230 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x7A )

			rom.seekp( 0x4275C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x13 )

			return;
		}
		case LevelId::TickTockClock: {
			// Hardcoded camera code
			rom.seekp( 0x4D8B0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x21 )

			rom.seekp( 0x4654C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x06 )

			rom.seekp( 0x4A23C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x7B )

			// Default render distance is halved in TTC
			rom.seekp( 0x84D8C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x21 )

			return;
		}
		case LevelId::RainbowRide: {
			// Hardcoded camera code
			rom.seekp( 0xE9CEC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE2, 0x48 )

			rom.seekp( 0x426DC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x0C, 0x0D, 0xE2, 0x10 )

			rom.seekp( 0x513E0 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x1D )

			rom.seekp( 0x5157C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xE1, 0x00, 0x04 )

			rom.seekp( 0x4DD3C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x21 )

			rom.seekp( 0x3D2D7 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x1B )
			else					ROM_WRITE_BYTES( 0x03 )

			return;
		}
		case LevelId::BowserInTheDarkWorld:
		case LevelId::BowserInTheFireSea:
		case LevelId::BowserInTheSky:
			return; // Nothing to do here
		case LevelId::Bowser1:
		case LevelId::Bowser2:
		case LevelId::Bowser3:
			return; // Handled by a tweak instead (TODO: make this tweak)
		case LevelId::CavernOfTheMetalCap: {
			// Hardcoded camera code
			rom.seekp( 0xE9D20 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x80, 0x32, 0xE3, 0x38 )

			rom.seekp( 0x4899C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x61 )

			rom.seekp( 0x513EC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x10 )

			rom.seekp( 0x51564 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xE1, 0x00, 0x0A )

			rom.seekp( 0x51A8C );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x12, 0x01, 0x00, 0x0D )

			return;
		}
		case LevelId::TowerOfTheWingCap:
		case LevelId::VanishCapUnderTheMoat:
		case LevelId::PeachsSecretSlide:
			return; // Nothing to do here
		case LevelId::SecretAquarium: {
			// Hardcoded camera code
			rom.seekp( 0x42670 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0xE5, 0x28, 0x00, 0x08 )

			rom.seekp( 0x488B3 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x17 )
			else					ROM_WRITE_BYTES( 0x03 )

			// Fish behaviour depends on if the level is Secret Aquarium or not
			rom.seekp( 0x7AE10 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x01 )

			rom.seekp( 0x7AEC4 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x00, 0x00, 0x00, 0x00 )
			else					ROM_WRITE_BYTES( 0x11, 0xC1, 0x00, 0x12 )

			rom.seekp( 0x7AF94 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x01 )

			rom.seekp( 0x7B160 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0x61 )

			rom.seekp( 0x7B7AC );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x17, 0x01 )

			return;
		}
		case LevelId::WingedMarioOverTheRainbow: {
			// Hardcoded camera code
			rom.seekp( 0x513FB );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x2D )
			else					ROM_WRITE_BYTES( 0x22 )

			rom.seekp( 0x51588 );
			if constexpr( remove )	ROM_WRITE_BYTES( 0x10, 0x00 )
			else					ROM_WRITE_BYTES( 0x15, 0xE1 )

			return;
		}
		default: return;
	}
}

#undef ROM_WRITE_BYTES

void HardcodedLevelCode::remove( std::ostream &rom, LevelId levelId ) {
	removeOrRestore<true>( rom, levelId );
}

void HardcodedLevelCode::restore( std::ostream &rom, LevelId levelId ) {
	removeOrRestore<false>( rom, levelId );
}

