#ifndef SRC_CORE_BASEROM_HPP_
#define SRC_CORE_BASEROM_HPP_

#include <iostream>
#include <map>
#include "src/polyfill/filesystem.hpp"
#include "src/core/data/water.hpp"

// base ROM is saved to app data by ../ui/base-rom.hpp (TODO: unify these files?)
namespace BaseRom {

	const fs::path &filePath();
	uint getWord( uint posn );
	bool matches( std::istream &rom, uint bytes );
	void revert( std::ostream &rom, uint bytes );
	void copyTo( std::ostream &rom, uint posn, uint bytes );

	const std::map<string,VanillaWaterTexture> &waterTextures();
}



#endif /* SRC_CORE_BASEROM_HPP_ */
