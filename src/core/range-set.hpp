#ifndef SRC_CORE_RANGE_SET_HPP_
#define SRC_CORE_RANGE_SET_HPP_

#include <map>
#include <limits>

template<typename T> class RangeSet {

	private:
	std::map<T,T> m_ranges;

	public:
	inline RangeSet() {
		static_assert( std::is_integral<T>::value );
	}

	bool add( T value ) {
		auto range = m_ranges.upper_bound( value );
		if( range == m_ranges.end() ) {
			if( value > std::numeric_limits<T>::min() && !m_ranges.empty() && (--range)->second == value - 1 ) {
				range->second = value;
			} else {
				m_ranges[value] = value;
			}
			return true;
		} else if( range == m_ranges.begin() ) {
			if( value < std::numeric_limits<T>::max() && range->first == value + 1 ) {
				const T rangeEnd = range->second;
				m_ranges.erase( range );
				m_ranges[value] = rangeEnd;
			} else if( value > std::numeric_limits<T>::min() && range->second == value - 1 ) {
				range->second = value;
			} else {
				m_ranges[value] = value;
			}
			return true;
		}

		range--;
		if( value >= range->first && value <= range->second ) {
			return false;
		}

		if( value > std::numeric_limits<T>::min() && range->second == value - 1 ) {
			range->second = value;
			if( (++range)->first == value + 1 ) {
				const T newEnd = range->second;
				(--range)->second = newEnd;
				m_ranges.erase( ++range );
			}
		} else if( value < std::numeric_limits<T>::max() && range->first == value + 1 ) {
			const T rangeEnd = range->second;
			if( range == m_ranges.begin() ) {
				m_ranges.erase( range );
				m_ranges[value] = rangeEnd;
			} else if( (--range)->second == value ) {
				range->second = rangeEnd;
				m_ranges.erase( ++range );
			} else {
				m_ranges.erase( ++range );
				m_ranges[value] = rangeEnd;
			}
		} else if( (++range)->first == value + 1 ) {
			const T rangeEnd = range->second;
			m_ranges.erase( range );
			m_ranges[value] = rangeEnd;
		} else {
			m_ranges[value] = value;
		}

		return true;
	}

	bool has( T value ) const {
		auto range = m_ranges.upper_bound( value );
		if( range == m_ranges.end() || range == m_ranges.begin() ) return false;
		range--;
		return value >= range->first && value <= range->second;
	}

};

#endif /* SRC_CORE_RANGE_SET_HPP_ */
