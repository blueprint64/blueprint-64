#include "src/core/music.hpp"

#include <cassert>
#include <set>
#include <stack>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/polyfill/buffer.hpp"
#include "src/core/baserom.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/range-set.hpp"

struct VanillaMusicInfo {
	string name;
	const std::vector<ubyte> soundBanks;
	uint offset;
	uint numBytes;
	MuteBehaviour muteBehaviour;
	sbyte volume;
};

static const VanillaMusicInfo s_vanillaMusic[35] = {
	{ "Sound Effects", { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, 0x240, 0x3490, MuteBehaviour::Mute | MuteBehaviour::Quiet, 127 },
	{ "Star Catch Fanfare", { 34 }, 0x36D0, 0x270, MuteBehaviour::Pause, 80 },
	{ "Title Theme", { 17 }, 0x3940, 0x2040, MuteBehaviour::Pause, -1 },
	{ "SM64 Main Theme (BOB, WF, TTM, THI)", { 34 }, 0x5980, 0x1410, MuteBehaviour::Pause, 75 },
	{ "Inside the Castle Walls", { 14 }, 0x6D90, 0x9C0, MuteBehaviour::Quiet, 70 },
	{ "Dire Dire Docks (JRB, DDD, SA)", { 19 }, 0x7750, 0x12B0, MuteBehaviour::Pause, 75 },
	{ "Lethal Lava Land (SSL, LLL)", { 15 }, 0x8A00, 0x9A0, MuteBehaviour::Pause, 75 },
	{ "Bowser's Theme (Bowser 1, Bowser 2)", { 18 }, 0x93A0, 0xD60, MuteBehaviour::Pause, 75 },
	{ "Snow Mountain (CCM, SML)", { 11 }, 0xA100, 0x1FD0, MuteBehaviour::Pause, 70 },
	{ "Slider (TTC, RR, PSS)", { 13 }, 0xC0D0, 0x1D10, MuteBehaviour::Pause, 65 },
	{ "Haunted House (BBH)", { 16, 33 }, 0xDDE0, 0x1630, MuteBehaviour::Pause, 80 },
	{ "Piranha Plant's Lullaby", { 20 }, 0xF410, 0x580, MuteBehaviour::Pause, 65 },
	{ "Cave Dungeon (HMC, WDW)", { 21 }, 0xF990, 0x1320, MuteBehaviour::Pause, 85 },
	{ "Game Start (Star Select)", { 22 }, 0x10CB0, 0x90, MuteBehaviour::Pause, 75 },
	{ "Powerful Mario (Wing Cap/Vanish Cap)", { 23 }, 0x10D40, 0xC40, MuteBehaviour::Pause, 65 },
	{ "Metallic Mario (Metal Cap)", { 24 }, 0x11980, 0x0AE0, MuteBehaviour::Pause, 70 },
	{ "Bowser's Message", { 18 }, 0x12460, 0x230, MuteBehaviour::Pause, 65 },
	{ "Bowser's Road (BitDW, BitFS, BitS)", { 25 }, 0x12690, 0x1290, MuteBehaviour::Pause, 70 },
	{ "Course Clear", { 31 }, 0x13920, 0x110, MuteBehaviour::Pause, 70 },
	{ "Merry-Go-Round", { 33 }, 0x13A30, 0x680, MuteBehaviour::Pause, 65 },
	{ "Race Fanfare", { 26 }, 0x140B0, 0xD0, MuteBehaviour::Pause, 80 },
	{ "Power Star (Star Spawn)", { 14 }, 0x14180, 0x290, MuteBehaviour::Pause, 70 },
	{ "Stage Boss", { 27 }, 0x14410, 0xD70, MuteBehaviour::Pause, 85 },
	{ "Bowser Clear (Key Get)", { 26 }, 0x15180, 0x2A0, MuteBehaviour::Pause, 75 },
	{ "Looping Steps (Infinite Staircase)", { 28 }, 0x15420, 0x700, MuteBehaviour::Quiet, 75 },
	{ "Ultimate Bowser (Bowser 3)", { 29 }, 0x15B20, 0xDC0, MuteBehaviour::Pause, 85 },
	{ "Staff Role (Credits)", { 37 }, 0x168E0, 0x37F0, MuteBehaviour::Pause, 85 },
	{ "Correct Solution", { 20 }, 0x1A0D0, 0xE0, MuteBehaviour::Pause, 80 },
	{ "Toad's Message", { 32 }, 0x1A1B0, 0xD0, MuteBehaviour::Pause, 80 },
	{ "Peach's Message", { 30 }, 0x1A280, 0x1B0, MuteBehaviour::Pause, 70 },
	{ "Opening", { 27 }, 0x1A430, 0x6F0, MuteBehaviour::Pause, 75 },
	{ "Ultimate Bowser Clear (Grand Star Get)", { 26 }, 0x1AB20, 0x810, MuteBehaviour::Pause, 80 },
	{ "Ending Demo", { 35 }, 0x1B330, 0x760, MuteBehaviour::Pause, 70 },
	{ "File Select", { 36 }, 0x1BA90, 0x310, MuteBehaviour::Pause, 65 },
	{ "Lakitu Appears", { 27 }, 0x1BDA0, 0x140, MuteBehaviour::Pause, 75 }
};

static inline void wordAlign( std::ostream &rom ) {
	while( rom.tellp() % 4 != 0 ) rom.put( 0 );
}

void MusicData::writeToRom( std::ostream &rom ) {
	const std::vector<MusicInfo> music = MusicData::getMusicInfo();
	assert( music.size() >= 35 );
	assert( music[0].isVanilla );

	std::vector<uint> offsets;
	offsets.reserve( music.size() );

	/* Write sound bank usage */
	rom.seekp( 0x7F0000 + ( 2 * music.size() ) );
	for( const MusicInfo &track : music ) {
		offsets.push_back( (uint)rom.tellp() - 0x7F0000 );
		rom.put( (ubyte)track.soundBanks.size() );
		for( auto i = track.soundBanks.rbegin(); i != track.soundBanks.rend(); i++ ) {
			rom.put( *i );
		}
	}

	if( rom.tellp() > 0x7F1000 ) {
		throw RomOverflowException( "How did you even MANAGE to overflow the sound bank sets data?" );
	}

	assert( offsets.size() == music.size() );
	rom.seekp( 0x7F0000 );
	for( uint offset : offsets ) {
		const ushort offsetBE = htons( (ushort)offset );
		rom.write( (const char*)&offsetBE, 2 );
	}

	/* Write m64 data */
	offsets.clear();
	rom.seekp( 0x3E00004 + (8 * music.size() ) );
	uint prevOffset = (uint)rom.tellp();
	for( uint i = 0; i < music.size(); i++ ) {
		const MusicInfo &track = music[i];

		wordAlign( rom );
		if( track.isPlaceholder ) {
			offsets.push_back( (uint)prevOffset - 0x3E00000 );
			continue;
		}

		offsets.push_back( (uint)rom.tellp() - 0x3E00000 );
		prevOffset = (uint)rom.tellp();

		if( track.isVanilla ) {
			BaseRom::copyTo( rom, 0x3E00000 + s_vanillaMusic[i].offset, s_vanillaMusic[i].numBytes );
		} else {
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( !fs::exists( musicPath ) ) {
				throw BlueprintLoadError( "Blueprint is missing an m64 file." );
			}

			const size_t trackStart = rom.tellp();
			const size_t trackSize = fs::file_size( musicPath );

			FileReadStream m64( musicPath.u8string() );
			Buffer sequence( trackSize );

			m64.read( sequence.data(), trackSize );
			rom.write( sequence.data(), trackSize );

			const size_t trackEnd = rom.tellp();
			const CustomMusicInfo &info = Blueprint::current()->customMusic().at( i );
			if( info.masterVolumeLocation != 0 ) {
				rom.seekp( trackStart + info.masterVolumeLocation );
				rom.put( track.volume );
			}
			if( info.muteBehaviourLocation != 0 ) {
				rom.seekp( trackStart + info.muteBehaviourLocation );
				rom.put( (ubyte)track.muteBehaviour );
			}

			rom.seekp( trackEnd );
		}
	}

	// Reserve 8kB of data at the end of the ROM for potential metadata
	if( (uint)rom.tellp() > 0x3FFE000 ) {
		throw RomOverflowException( "That's a lot of music. Exceeded 2040 kB limit." );
	}

	/* Write sequence pointers */
	const ushort numSeqBE = htons( (ushort)music.size() );
	rom.seekp( 0x3E00002 );
	rom.write( (const char*)&numSeqBE, 2 );

	assert( offsets.size() == music.size() );
	for( uint i = 0; i < music.size(); i++ ) {
		const uint data[2] = {
			htonl( offsets[i] ),
			htonl( music[i].numBytes )
		};
		rom.write( (const char*)data, 8 );
	}

}

std::vector<MusicInfo> MusicData::getMusicInfo() {
	const std::map<ubyte,CustomMusicInfo> &customMusic = Blueprint::current()->customMusic();
	assert( customMusic.count( 0 ) == 0 );

	std::vector<MusicInfo> music;

	ubyte foundTracks = 0;
	for( ubyte i = 0; i < 35; i++ ) {
		if( customMusic.count( i ) > 0 ) {
			foundTracks++;
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( fs::exists( musicPath ) ) {
				const CustomMusicInfo &info = customMusic.at( i );
				music.push_back({
					info.name,
					info.soundBanks,
					(uint)fs::file_size( musicPath ),
					false,
					false,
					info.muteBehaviour,
					info.masterVolume
				});
				continue;
			}
		}

		const VanillaMusicInfo &info = s_vanillaMusic[i];
		music.push_back({
			info.name,
			info.soundBanks,
			info.numBytes,
			true,
			false,
			info.muteBehaviour,
			info.volume
		});
	}

	for( ubyte i = 35; foundTracks < customMusic.size(); i++ ) {
		if( customMusic.count( i ) > 0 ) {
			foundTracks++;
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( fs::exists( musicPath ) ) {
				const CustomMusicInfo &info = customMusic.at( i );
				music.push_back({
					info.name,
					info.soundBanks,
					(uint)fs::file_size( musicPath ),
					false,
					false,
					info.muteBehaviour,
					info.masterVolume
				});
				continue;
			}
		}

		const MusicInfo &info = music[music.size() - 1];
		music.push_back({
			"Placeholder",
			info.soundBanks,
			info.numBytes,
			false,
			true,
			MuteBehaviour::Undefined,
			-1
		});
	}

	return music;
}

const string &MusicData::getVanillaMusicName( ubyte i ) {
	return s_vanillaMusic[i].name;
}

static inline void readMuteBehaviour( ubyte behaviour, MusicSequenceInfo &info ) {
	info.muteBehaviour = (MuteBehaviour)behaviour;
	if( info.muteBehaviour == (MuteBehaviour::Mute | MuteBehaviour::Quiet) ) {
		info.muteBehaviour = MuteBehaviour::Mute;
		info.soundBank0 = 37;
	} else switch( info.muteBehaviour ) {
		case MuteBehaviour::Ignore:
		case MuteBehaviour::Pause:
		case MuteBehaviour::Quiet:
		case MuteBehaviour::Mute:
			info.soundBank0 = 37;
			break;
		default:
			/* The 0xD3 command is supposed to set the mute behaviour, but a
			 * number of m64 sequences people have created incorrectly put the
			 * sound bank ID here instead of a valid flag.
			 */
			info.soundBank0 = (behaviour > 37) ? 37 : behaviour;
			info.muteBehaviour = MuteBehaviour::Pause;
			break;
	}
}

static inline ushort getShort( std::ifstream &m64 ) {
	const ushort upperByte = (ubyte)m64.get();
	const ushort lowerByte = (ubyte)m64.get();
	return (upperByte << 8) | lowerByte;
}

inline static std::set<ushort> parseHeader( std::ifstream &m64, MusicSequenceInfo &info ) {
	RangeSet<ushort> parsedRanges;
	std::stack<ushort> unexploredBranches;
	std::set<ushort> channelScripts;

	bool deadEnd = false;
	bool readVolume = false;
	while( m64.good() && !m64.eof() ) {

		if( deadEnd ) {
			while( !unexploredBranches.empty() ) {
				const ushort candidateBranch = unexploredBranches.top();
				unexploredBranches.pop();
				if( !parsedRanges.has( candidateBranch ) ) {
					deadEnd = false;
					m64.seekg( candidateBranch );
					break;
				}
			}
			if( deadEnd ) break;
		}

		const ushort pc = (ushort)m64.tellg();
		ubyte cmd = m64.get();
		if( !m64.good() ) break;

		if( cmd < 0xC0 ) {
			cmd &= 0xF0;
		}

		if( !parsedRanges.add( pc ) ) {
			deadEnd = true;
			continue;
		}

		switch( cmd ) {
			case 0xFF:
				deadEnd = true;
				break;
			case 0xFB:
				deadEnd = true;
				[[fallthrough]];
			case 0xF5: case 0xF9: case 0xFA: case 0xFC:
				unexploredBranches.push( getShort( m64 ) );
				break;
			case 0xD3: {
				const ubyte behaviour = m64.get();
				if( info.muteBehaviourLocation == 0 ) {
					info.muteBehaviourLocation = pc + 1;
					readMuteBehaviour( behaviour, info );
				}
				break;
			}
			case 0xDB: {
				const ubyte volume = m64.get();
				if( readVolume || volume >= 0x80 ) {
					// Volume set multiple times. Cannot override.
					info.masterVolume = -1;
					info.masterVolumeLocation = 0;
				} else {
					info.masterVolume = (sbyte)volume;
					info.masterVolumeLocation = pc + 1;
					readVolume = true;
				}
				break;
			}
			case 0x90: {
				const ushort upperByte = (ubyte)m64.get();
				const ushort lowerByte = (ubyte)m64.get();
				channelScripts.insert( (upperByte << 8) | lowerByte );
				break;
			}
			case 0xC9: case 0xD1: case 0xD2: case 0xD6: case 0xD7:
				m64.get();
				[[fallthrough]];
			case 0xC8: case 0xCC: case 0xD0: case 0xD5: case 0xD9: case 0xDA:
			case 0xDC: case 0xDD: case 0xDE: case 0xDF: case 0xF1: case 0xF2:
			case 0xF3: case 0xF4: case 0xF8:
				m64.get();
				break;
			case 0xFD:
				if( m64.get() & 0x80 ) m64.get();
				break;
			default:
				break;
		}
	}

	return channelScripts;
}

inline static void parseChannelScript( std::ifstream &m64, MusicSequenceInfo &info ) {
	RangeSet<ushort> parsedRanges;
	std::stack<ushort> unexploredBranches;

	bool deadEnd = false;
	while( m64.good() && !m64.eof() ) {

		if( deadEnd ) {
			while( !unexploredBranches.empty() ) {
				const ushort candidateBranch = unexploredBranches.top();
				unexploredBranches.pop();
				if( !parsedRanges.has( candidateBranch ) ) {
					deadEnd = false;
					m64.seekg( candidateBranch );
					break;
				}
			}
			if( deadEnd ) break;
		}

		const ushort pc = (ushort)m64.tellg();
		ubyte cmd = m64.get();
		if( !m64.good() ) break;

		if( cmd <= 0xC0 ) {
			cmd &= 0xF0;
		}

		if( !parsedRanges.add( pc ) ) {
			deadEnd = true;
			continue;
		}

		switch( cmd ) {
			case 0xF3: case 0xFF:
				deadEnd = true;
				break;
			case 0xC6: {
				const ubyte bankIndex = m64.get();
				if( bankIndex >= info.numSoundBanks ) {
					info.numSoundBanks = bankIndex + 1;
				}
				break;
			}
			case 0xFB:
				deadEnd = true;
				[[fallthrough]];
			case 0xF5: case 0xF9: case 0xFA: case 0xFC:
				unexploredBranches.push( getShort( m64 ) );
				break;
			case 0xC7: case 0xE1: case 0xE2:
				m64.get();
				[[fallthrough]];
			case 0x90: case 0xC2: case 0xCB: case 0xDE: case 0xFD:
				m64.get();
				[[fallthrough]];
			case 0xC1: case 0xC8: case 0xC9: case 0xCC: case 0xCA: case 0xD0:
			case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD7: case 0xD8:
			case 0xD9: case 0xDC: case 0xDD: case 0xDF: case 0xE0: case 0xE3:
			case 0xF2: case 0xF8:
				m64.get();
				break;
			default:
				break;
		}
	}
}

bool MusicData::tryGetSequenceInfo( const string &filePath, MusicSequenceInfo &info ) {
	if( !fs::exists( fs::to_path( filePath ) ) ) {
		return false;
	}

	info.fileSize = fs::file_size( fs::to_path( filePath ) );
	if( info.fileSize == 0 ) {
		return false;
	}

	FileReadStream m64( filePath );
	if( !m64.good() || m64.eof() ) {
		return false;
	}

	info.soundBank0 = 37;
	info.numSoundBanks = 1;
	info.muteBehaviour = MuteBehaviour::Undefined;
	info.masterVolume = -1;
	info.muteBehaviourLocation = 0;
	info.masterVolumeLocation = 0;

	const std::set<ushort> channelScripts = parseHeader( m64, info );
	for( ushort scriptStart : channelScripts ) {
		m64.seekg( scriptStart );
		parseChannelScript( m64, info );
	}

	assert( info.numSoundBanks > 0 );
	assert( info.soundBank0 <= 37 );
	assert( ((ubyte)info.muteBehaviour & 0xF) == 0 );
	return true;
}
