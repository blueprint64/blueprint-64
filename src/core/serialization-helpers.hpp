#ifndef SRC_CORE_SERIALIZATION_HELPERS_HPP_
#define SRC_CORE_SERIALIZATION_HELPERS_HPP_

#include <vector>
#include <array>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include "src/core/json.hpp"

namespace JsonSerializer {

	template<typename T> void serializeIterator( JsonWriter &jw, const T &begin, const T &end ) {
		jw.writeArrayStart();
		for( auto i = begin; i != end; ++i ) {
			serialize( jw, *i );
		}
		jw.writeArrayEnd();
	}

	template<typename T> void serializeDictionaryValues( JsonWriter &jw, const T &begin, const T &end ) {
		jw.writeArrayStart();
		for( auto i = begin; i != end; ++i ) {
			serialize( jw, i->second );
		}
		jw.writeArrayEnd();
	}

	template<typename T> void serializeIterator( JsonWriter &jw, const string &name, const T &begin, const T &end ) {
		jw.writePropertyName( name );
		serializeIterator( jw, begin, end );
	}

	template<typename T> void serializeDictionaryValues( JsonWriter &jw, const string &name, const T &begin, const T &end ) {
		jw.writePropertyName( name );
		serializeDictionaryValues( jw, begin, end );
	}

	template<typename T> std::vector<T> parseVector( const Json &json, bool optional = false ) {
		std::vector<T> collection;
		if( optional && !json.isArray() ) return collection;
		const JArray &jArray = json.array();
		collection.reserve( jArray.size() );
		for( const Json &element : jArray ) {
			collection.push_back( element.get<T>() );
		}
		return collection;
	}

	template<typename T> std::set<T> parseSet( const Json &json, bool optional = false ) {
		std::set<T> collection;
		if( optional && !json.isArray() ) return collection;
		for( const Json &element : json.array() ) {
			collection.insert( element.get<T>() );
		}
		return collection;
	}

	template<typename T> std::unordered_set<T> parseHashSet( const Json &json, bool optional = false ) {
		std::unordered_set<T> collection;
		if( optional && !json.isArray() ) return collection;
		const JArray &jArray = json.array();
		collection.reserve( jArray.size() );
		for( const Json &element : jArray ) {
			collection.insert( element.get<T>() );
		}
		return collection;
	}

	template<typename K, typename V> std::map<K,V> parseMap( const Json &json, const char *idField = "id", bool optional = false ) {
		std::map<K,V> collection;
		if( optional && !json.isArray() ) return collection;
		for( const Json &element : json.array() ) {
			collection[element[idField].get<K>()] = element.get<V>();
		}
		return collection;
	}

	template<typename K, typename V> std::unordered_map<K,V> parseHashMap( const Json &json, const char *idField = "id", bool optional = false ) {
		std::unordered_map<K,V> collection;
		if( optional && !json.isArray() ) return collection;
		const JArray &jArray = json.array();
		collection.reserve( jArray.size() );
		for( const Json &element : jArray ) {
			collection[element[idField].get<K>()] = element.get<V>();
		}
		return collection;
	}

}



#endif /* SRC_CORE_SERIALIZATION_HELPERS_HPP_ */
