#ifndef CORE_UUID_HPP_
#define CORE_UUID_HPP_

#include <string>
#include <functional>
#include "src/types.hpp"
#include "src/core/json.hpp"

class Uuid final {

	private:
	ubyte m_data[16];

	public:
	Uuid();
	Uuid( const Uuid &src );

	string toString() const;
	string toHex() const;
	void writeToRom( std::ostream &rom ) const;
	static Uuid readFromRom( std::istream &rom );
	static Uuid parse( const string &str );

	Uuid &operator=( const Uuid &other );

	static int compare( const Uuid &lhs, const Uuid &rhs );
	static Uuid generate();

};

inline bool operator==( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) == 0;
}
inline bool operator!=( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) != 0;
}
inline bool operator<( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) < 0;
}
inline bool operator<=( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) <= 0;
}
inline bool operator>( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) > 0;
}
inline bool operator>=( const Uuid &lhs, const Uuid &rhs ) {
	return Uuid::compare( lhs, rhs ) >= 0;
}

namespace std {
	template <> struct hash<Uuid> {
		size_t operator()(const Uuid & x) const {
			static_assert( sizeof( size_t ) == 8 || sizeof( size_t ) == 4 );
			const size_t *data = (const size_t*)&x;
			if constexpr( sizeof( size_t ) == 8 ) {
				return data[0] ^ data[1];
			} else {
				return (data[0] ^ data[1]) ^ (data[2] ^ data[3]);
			}
		}
	};
}

namespace JsonSerializer {
	template<> void serialize<Uuid>( JsonWriter &jw, const Uuid &obj );
	template<> Uuid parse<Uuid>( const Json &json );
}

#endif /* CORE_UUID_HPP_ */
