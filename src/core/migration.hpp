#ifndef SRC_CORE_MIGRATION_HPP_
#define SRC_CORE_MIGRATION_HPP_

#include "src/core/version.hpp"

class DeserializationContext {

	private:
	DeserializationContext( const Version &schemaVersion );

	public:
	~DeserializationContext();

	static const Version &schemaVersion();
	[[nodiscard]] static DeserializationContext create( const Version &schemaVersion );

};

#endif /* SRC_CORE_MIGRATION_HPP_ */
