#include "src/core/text.hpp"

#include <cstdio>
#include <array>
#include <cassert>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/core/json.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/baserom.hpp"
#include "src/types.hpp"

using namespace std;

static constexpr ushort SCREEN_WIDTH = 320;
static constexpr ushort SCREEN_HEIGHT = 240;

static constexpr ushort DIALOG_PADDING_LEFT = 7;
static constexpr ushort DIALOG_PADDING_TOP = 5;
static constexpr ushort DIALOG_PADDING_BOTTOM = 3;
static constexpr ushort DIALOG_WIDTH = 142;
static constexpr ushort DIALOG_LINE_HEIGHT = 15;

static const char *s_defaultNames[] = {
	"BOB Entry",
	"BOB Bob-Omb - Mission",
	"BOB Bob-Omb - Advice",
	"BOB Bob-Omb - Thanks",
	"BOB Bob-Omb - Cannon",
	"BOB Koopa Race Start",
	"BOB Koopa Race Cheater",
	"BOB Koopa Race Win",
	"BOB Sign - Chain Chomp",
	"THI Koopa Race Start",
	"Wing Cap Unlocked",
	"Metal Cap Unlocked",
	"Vanish Cap Unlocked",
	"100 Coin Star",
	"Star Get 2",
	"BOB Sign - How to Punch",
	"BOB Sign - Shell Ride",
	"King Bob-Omb Intro",
	"WF Sign - Sleepy Plant",
	"Castle Sign - Hallway",
	"Peach's Letter",
	"Bowser - Enter Castle",
	"Need a Key",
	"Wrong Key",
	"Star Door - 1",
	"Star Door - 3",
	"Star Door - 8",
	"Star Door - 30",
	"Star Door - 50",
	"Star Door - 70",
	"WF Entry",
	"THI Koopa Race Win",
	"BOB Sign - Wing Cap",
	"Game Start",
	"Lakitu Introduction",
	"BOB Sign - Camera Controls",
	"WF Sign - Observation Platform",
	"Penguin Race Failed",
	"Star Door Opened",
	"BOB Sign - No Visitors",
	"CCM Sign - Crevasse Warning",
	"Koopa Race Failed",
	"WF Sign - Narrow Bridge",
	"HMC Sign - Grabbable Ceiling",
	"Hoot Wakes Up",
	"Hoot Exhausted",
	"Castle Sign - Jump Tutorial",
	"Opening Cannon",
	"CCM Entry",
	"CCM Sign - Wall Kick",
	"BOB Sign - Z Button",
	"WF Sign - Poles and Trees",
	"Castle Sign - Backflip",
	"BOB Sign - Coin Ring Secret",
	"CCM Sign - Snow Slide",
	"Penguin Race Start",
	"Penguin Race Win",
	"Mother Penguin Quest",
	"Rescued Baby Penguin",
	"Wrong Baby Penguin",
	"JRB Sign - Swimming",
	"SML Sign - Frigid Water",
	"HMC Sign - Metal Cap",
	"BBH Sign - Vanish Cap",
	"BOB Sign - Flying",
	"Unused Sign - Swimming Lessons",
	"BitDW Sign - Peach's Warning",
	"Bowser Fight 1 Start",
	"LLL Sign - Lava",
	"Castle Sign - Out of Bounds",
	"Castle Sign - Exit Course",
	"HMC Sign - Toxic Gas",
	"TTM Sign - Windy",
	"JRB Sign - Treasure Chests",
	"BOB Sign - Ledge Grab",
	"Castle Sign - Secret Stars",
	"Toad Star 2",
	"Castle Sign - Pound the Pillars",
	"WF Sign - Blue Coin Switch",
	"Ukiki Caught",
	"Ukiki Laugh",
	"WDW Sign - Water Level",
	"Toad Star 1",
	"Toad Star 3",
	"MIPS 1",
	"BBH Sign - Ghost Puns",
	"BBH Sign - Mr. I",
	"CCM Sign - Santa",
	"HMC Sign - Work Elevator Drop Off",
	"HMC Sign - Map",
	"BitDW Entry",
	"CCM Sign - Updraft",
	"Bowser Fight 2 Start",
	"Bowser Fight 3 Start",
	"CMM Sign - Long Jump",
	"BOB Sign - Talk and Read",
	"WF Sign - Narrow Path",
	"LLL Entry",
	"BBH Entry",
	"Unused Ghost Dialog",
	"Ukiki Stole Hat",
	"Ukiki Thief Caught",
	"Castle Sign - Boos",
	"SSL Sign - Four Towers",
	"BOB Sign - Star Marker",
	"BOB Cannon Opened",
	"Cannon Opened",
	"Ghosts. Don't. Die.",
	"Big Boo Appears",
	"CCM Snowman Head",
	"CCM Snowman Body",
	"CCM Snowman Star",
	"BOB Sign - Coins",
	"BOB Sign - Caps",
	"King Whomp Fight Start",
	"King Whomp Defeated",
	"King Bob-Omb Defeated",
	"Eyerok Fight Start",
	"Eyerok Defeated",
	"Bowser 1 Defeated",
	"Bowser 2 Defeated",
	"Bowser 3 Defeated (Missing Stars)",
	"HMC Sign - Black Hole",
	"Metal Cap Sign",
	"HMC Sign - Work Elevator Pickup",
	"HMC Sign - Hazy Maze Exit",
	"HMC Sign - Hazy Maze",
	"HMC Sign - Dorrie",
	"King Bob-Omb Ring Out",
	"Vanish Cap Level Entry",
	"Metal Cap Level Entry",
	"Wing Cap Level Entry",
	"Penguin Race Cheated",
	"Toad - Intro",
	"Toad - Star Hints",
	"Toad - Stars per Level",
	"Toad - Healing Coins",
	"Toad - 100 Coins",
	"HMC Sign - Directions",
	"HMC Sign - Elevator",
	"HMC Sign - Elevator Area",
	"Star Milestone 1",
	"Star Milestone 2",
	"Star Milestone 3",
	"Star Milestone 4",
	"Star Milestone 5",
	"Star Milestone 6",
	"Castle Sign - Cap Blocks",
	"SML Sign - Snow Waves",
	"Secret Slide Sign",
	"Wiggler Fight Start",
	"Wiggler 1st Hit",
	"Wiggler Defeated",
	"Giant Snowman",
	"Toad - Lost Hat",
	"Toad - Mirror Room",
	"Toad - Clock",
	"SSL Sign - Quicksand",
	"Castle Sign - Triple Jump",
	"Castle Sign - Longjump and Backflip",
	"Castle Sign - Dive",
	"Yoshi",
	"MIPS 2",
	"Bowser 3 Defeated (True Ending)",
	"Penguin Race Rematch",
	"THI Sign - Wooden Post",
	"THI Sign - Koopa's Note",
	"Castle Sign - First Sign",
	"Wiggler 2nd Hit",
	"JRB Sign - Keep Out"
};
static_assert( sizeof( s_defaultNames ) == 170 * sizeof(char*));

static DialogEntry s_vanillaDialog[170];
static string s_vanillaLevelNames[26];
static string s_vanillaStarNames[97];
static bool s_initialized = false;

const DialogEntry* VanillaText::dialog() {
	assert( s_initialized );
	return s_vanillaDialog;
}

const string* VanillaText::levelNames() {
	assert( s_initialized );
	return s_vanillaLevelNames;
}

const string* VanillaText::starNames() {
	assert( s_initialized );
	return s_vanillaStarNames;
}

static constexpr char ___ = '\0';
static constexpr char XXX = '\0';
static const char decodeMap[256] = {
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
	'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
	'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
	'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\'', '.',
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	'^', '=', '<', '>', '[', ']', '|', '{', '}', ___, ___, ___, ___, ___, ___, ___,
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ',',
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ' ', '-',
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	XXX, XXX, XXX, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___, ___,
	'0', '(', XXX, ')', '_', '&', ':', ___, ___, ___, ___, ___, ___, ___, ___, ___, // 0xE0 is another 0 because reasons
	___, ___, '!', '%', '?', '`', '"', '~', ___, '$', '*', '#', '@', '+', '\n', '\0'
};

// 0x20 - 0x7F
static constexpr ubyte ____ = 0xFF;
static const ubyte encodeMap[96] = {
	0x9E, 0xF2, 0xF6, 0xFB, 0xF9, 0xF3, 0xE5, 0x3E, 0xE1, 0xE3, 0xFA, 0xFD, 0x6F, 0x9F, 0x3F, ____,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xE6, ____, 0x52, 0x51, 0x53, 0xF4,
	0xFC, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
	0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x54, ____, 0x55, 0x50, 0xE4,
	0xF5, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32,
	0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x57, 0x56, 0x58, 0xF7, ____
};

static string readEncodedStringFromRom( istream &rom ) {
	string result;
	while( rom.good() && !rom.eof() ) {
		ubyte c = rom.get();
		if( c == 0xFFu ) return result;
		switch( c ) {
			case 0xD0: result += "  "; break;
			case 0xD1: result += "the"; break;
			case 0xD2: result += "you"; break;
			case 0xE2: result += ")("; break;
			[[likely]] default: {
				if( decodeMap[c] == 0 ) {
					std::cout << "0x" << std::hex << (uint)c << std::endl << std::flush;
				}
				assert( decodeMap[c] != 0 );
				result += decodeMap[c];
			}
		}
	}
	return result;
}

typedef std::vector<ubyte> EncodedString;
static EncodedString encodeString( const string &asciiString ) {
	EncodedString encodedString;
	encodedString.reserve( asciiString.length() + 1 );
	for( size_t i = 0; i < asciiString.length(); i++ ) {
		switch( asciiString[i] ) {
			case ' ': {
				if( asciiString[i+1] == ' ' ) {
					encodedString.push_back( 0xD0 );
					i++;
				} else {
					encodedString.push_back( 0x9E );
				}
				break;
			}
			case 't': {
				if( asciiString.size() > i + 2 && asciiString[i+1] == 'h' && asciiString[i+2] == 'e' ) {
					encodedString.push_back( 0xD1 );
					i += 2;
				} else {
					encodedString.push_back( 0x37 );
				}
				break;
			}
			case 'y': {
				if( asciiString.size() > i + 2 && asciiString[i+1] == 'o' && asciiString[i+2] == 'u' ) {
					encodedString.push_back( 0xD2 );
					i += 2;
				} else {
					encodedString.push_back( 0x3C );
				}
				break;
			}
			case '\n': {
				encodedString.push_back( 0xFE );
				break;
			}
			[[unlikely]] case ')': {
				if( asciiString[i+1] == '(' ) {
					encodedString.push_back( 0xE2 );
					i++;
				} else {
					encodedString.push_back( 0xE3 );
				}
				break;
			}
			[[likely]] default: {
				const ubyte c = asciiString[i];
				if( c >= 0x20 && c < 0x7F ) {
					const ubyte e = encodeMap[c-0x20];
					if( e != 0xFF ) {
						encodedString.push_back( e );
					}
				}
			}
		}
	}
	encodedString.push_back( 0xFF );
	return encodedString;
}

inline static void initDialog( ifstream &rom, uint bankStart ) {
	const uint dialogTableAddr = bankStart + 0xFFC8;
	for( int i = 0; i < 170; i++ ) {
		DialogEntry &entry = s_vanillaDialog[i];
		entry.name = s_defaultNames[i];
		rom.seekg( dialogTableAddr + ( i << 4 ) + 4 );
		entry.pageSize = (ubyte)rom.get();

		rom.ignore( 1 );
		ushort temp;

		rom.read( (char*)&temp, 2 );
		temp = ntohs( temp );
		if( temp < 95 ) {
			entry.halign = HAlign::Left;
			entry.hoffset = temp - DIALOG_PADDING_LEFT;
		} else if( temp == 95 ) {
			entry.halign = HAlign::Centre;
			entry.hoffset = 0;
		} else {
			entry.halign = HAlign::Right;
			entry.hoffset = SCREEN_WIDTH - ( temp + DIALOG_WIDTH - DIALOG_PADDING_LEFT );
		}

		const ushort DIALOG_HEIGHT = DIALOG_PADDING_TOP + DIALOG_PADDING_BOTTOM + ( entry.pageSize * DIALOG_LINE_HEIGHT );
		const ushort MID_Y = (SCREEN_HEIGHT + DIALOG_HEIGHT)/2 - DIALOG_PADDING_TOP;
		rom.read( (char*)&temp, 2 );
		temp = ntohs( temp );
		if( temp > MID_Y ) {
			entry.valign = VAlign::Top;
			entry.voffset = SCREEN_HEIGHT - ( temp + DIALOG_PADDING_TOP );
		} else if( temp == MID_Y ) {
			entry.valign = VAlign::Centre;
			entry.voffset = 0;
		} else {
			entry.valign = VAlign::Bottom;
			entry.voffset = temp + DIALOG_PADDING_TOP - DIALOG_HEIGHT;
		}

		rom.ignore( 2 );
		uint segPtr;
		rom.read( (char*)&segPtr, 4 );
		rom.seekg( ntohl( segPtr ) - 0x2000000 + bankStart );
		entry.text = readEncodedStringFromRom( rom );
		assert( rom.good() );
	}

	rom.seekg( 0xEDE58 );
	for( int i = 0; i < 170; i++ ) {
		s_vanillaDialog[i].soundEffect = (sbyte)rom.get();
	}
	assert( rom.good() );

	for( int i = 10; i <= 14; i++ ) {
		s_vanillaDialog[i].soundEffect = 11;
	}
}

inline static void initLevelNames( ifstream &rom, uint bankStart ) {
	uint textAddrs[26];
	rom.seekg( bankStart + 0x10F68 );
	for( int i = 0; i < 26; i++ ) {
		rom.read( (char*)&textAddrs[i], 4 );
		textAddrs[i] = ntohl( textAddrs[i] ) - 0x2000000 + bankStart;
	}

	assert( rom.good() );
	for( int i = 0; i < 26; i++ ) {
		rom.seekg( textAddrs[i] );
		string levelName = readEncodedStringFromRom( rom );
		s_vanillaLevelNames[i] = levelName.size() >= 3 ? levelName.substr( 3 ) : levelName;
		assert( rom.good() );
	}
}

inline static void initStarNames( ifstream &rom, uint bankStart ) {
	uint textAddrs[97];
	rom.seekg( bankStart + 0x1192C );
	for( int i = 0; i < 97; i++ ) {
		rom.read( (char*)&textAddrs[i], 4 );
		textAddrs[i] = ntohl( textAddrs[i] ) - 0x2000000 + bankStart;
	}

	assert( rom.good() );
	for( int i = 0; i < 97; i++ ) {
		rom.seekg( textAddrs[i] );
		s_vanillaStarNames[i] = readEncodedStringFromRom( rom );
		assert( rom.good() );
	}
}

inline static uint bank2Start( istream &rom ) {
	ushort mio0uo;
	rom.seekg( 0x80000E );
	rom.read( (char*)&mio0uo, 2 );
	return 0x800000 + ntohs( mio0uo );
}

void VanillaText::init() {
	assert( !s_initialized );

	const fs::path &cachedRomPath = BaseRom::filePath();
	assert( fs::exists( cachedRomPath ) );

	FileReadStream rom( cachedRomPath.u8string() );
	assert( rom.good() );

	const uint bankStart = bank2Start( rom );
	initDialog( rom, bankStart );
	initLevelNames( rom, bankStart );
	initStarNames( rom, bankStart );
	s_initialized = true;
}

static const uint textDataRegionStart[4] = { 0x7D34, 0x10D14, 0x10FD4, 0x18A0E };
static const uint textDataRegionEnd[3] = { 0xFFC8, 0x10F68, 0x1192C };

static uint writeTextString(
	std::ostream &rom,
	const EncodedString &str,
	uint bankStart,
	uint &currentOffset,
	ubyte &currentRegion
) {
	if( currentRegion < 3 && currentOffset + str.size() >= textDataRegionEnd[currentRegion] ) {
		currentOffset = textDataRegionStart[++currentRegion];
		rom.seekp( bankStart + currentOffset );
	}
	uint segPtrBE = htonl( 0x2000000 + currentOffset );
	rom.write( (char*)str.data(), str.size() );
	currentOffset += (uint)str.size();
	return segPtrBE;
}

static uint roundUpToWordAlignment( uint x ) {
	if( x % 4 == 0 ) return x;
	return x + 4 - (x % 4);
}

void Text::applyChangesToRom(
	std::iostream &rom,
	const DialogEntry *dialog,
	const string *levelNames,
	const string *starNames,
	const uint *sfxTable
) {
	const char ZERO[4] = { 0, 0, 0, 0 };
	rom.seekp( 0x95118 );
	rom.write( ZERO, 4 );

	rom.seekp( 0xEDE58 );
	for( int i = 0; i < 170; i++ ) {
		rom.put( (char)dialog[i].soundEffect );
	}

	rom.seekp( 0xEDF04 );
	for( int i = 0; i < 16; i++ ) {
		const uint soundEffect = htonl( sfxTable[i] );
		rom.write( (const char*)&soundEffect, 4 );
	}

	const uint bankStart = bank2Start( rom );

	uint currentOffset = textDataRegionStart[0];
	ubyte currentRegion = 0;
	rom.seekp( bankStart + currentOffset );

	std::array<uint,170> dialogSegPtrsBE;
	for( int i = 0; i < 170; i++ ) {
		const EncodedString str = encodeString( dialog[i].text );
		dialogSegPtrsBE[i] = writeTextString( rom, str, bankStart, currentOffset, currentRegion );
	}

	std::array<uint,26> levelNameSegPtrsBE;
	for( int i = 0; i < 26; i++ ) {
		EncodedString str;
		if( i < 15 ) {
			char prefix[4] = "   ";
			std::sprintf( prefix, "%2d ", i+1 );
			str = encodeString( string(prefix) + levelNames[i] );
		} else {
			str = encodeString( levelNames[i] );
			str.insert( str.begin(), { 0x9E, 0x9E, 0x9E });
		}
		levelNameSegPtrsBE[i] = writeTextString( rom, str, bankStart, currentOffset, currentRegion );
	}

	std::array<uint,97> starNameSegPtrsBE;
	for( int i = 0; i < 97; i++ ) {
		const EncodedString str = encodeString( starNames[i] );
		starNameSegPtrsBE[i] = writeTextString( rom, str, bankStart, currentOffset, currentRegion );
	}

	const uint bank2size = roundUpToWordAlignment( currentRegion < 3 ? textDataRegionStart[3] : currentOffset );
	const uint bank2sizeBE = htonl( bank2size );

	if( bankStart + bank2size > 0x823B64 ) {
		throw RomOverflowException( "Bank 0x2 Overflow: Too much text data." );
	}

	rom.seekp( bankStart + 0xFFC8 );
	for( int i = 0; i < 170; i++ ) {
		const DialogEntry &d = dialog[i];
		rom.write( ZERO, 4 );
		rom.put( (char)d.pageSize );
		rom.put( '\0' );
		ushort x, y;
		switch( d.halign ) {
			case HAlign::Left: x = d.hoffset + DIALOG_PADDING_LEFT; break;
			case HAlign::Right: x = SCREEN_WIDTH + DIALOG_PADDING_LEFT - (d.hoffset + DIALOG_WIDTH); break;
			default: x = 95;
		}
		const ushort DIALOG_HEIGHT = DIALOG_PADDING_TOP + DIALOG_PADDING_BOTTOM + ( d.pageSize * DIALOG_LINE_HEIGHT );
		switch( d.valign ) {
			case VAlign::Top: y = SCREEN_HEIGHT - ( d.voffset + DIALOG_PADDING_TOP ); break;
			case VAlign::Bottom: y = d.voffset + DIALOG_HEIGHT - DIALOG_PADDING_TOP; break;
			default: y = (SCREEN_HEIGHT + DIALOG_HEIGHT)/2 - DIALOG_PADDING_TOP;
		}

		x = htons( x );
		y = htons( y );
		rom.write( (char*)&x, 2 );
		rom.write( (char*)&y, 2 );

		rom.write( ZERO, 2 );
		rom.write( (char*)&dialogSegPtrsBE[i], 4 );
	}

	rom.seekp( bankStart + 0x10F68 );
	for( int i = 0; i < 26; i++ ) {
		rom.write( (char*)&levelNameSegPtrsBE[i], 4 );
	}

	rom.seekp( bankStart + 0x1192C );
	for( int i = 0; i < 97; i++ ) {
		rom.write( (char*)&starNameSegPtrsBE[i], 4 );
	}

	rom.seekp( 0x800004 );
	rom.write( (const char*)&bank2sizeBE, 4 );

	const uint bankEnd = bankStart + bank2size;
	const ushort seg2startBE[2] = { htons( bankStart >> 16 ), htons( bankStart & 0xFFFF ) };
	const ushort seg2endBE[2] = { htons( bankEnd >> 16 ), htons( bankEnd & 0xFFFF ) };

	rom.seekp( 0x3AC2 );
	rom.write( (const char*)&seg2startBE[0], 2 );
	rom.seekp( 0x3AC6 );
	rom.write( (const char*)&seg2endBE[0], 2 );
	rom.put( (char)0x34 );
	rom.put( (char)0xC6 );
	rom.write( (const char*)&seg2endBE[1], 2 );
	rom.put( (char)0x34 );
	rom.put( (char)0xA5 );
	rom.write( (const char*)&seg2startBE[1], 2 );
	rom.put( (char)0x00 );
	rom.put( (char)0x00 );
	rom.put( (char)0x38 );
	rom.put( (char)0x25 );
	rom.seekp( 0x3AD8 );
	rom.put( (char)0x0C );
	rom.put( (char)0x09 );
	rom.put( (char)0xE1 );
	rom.put( (char)0xA3 );

	const uint layoutBitsSize = roundUpToWordAlignment( bank2size / 8 ) + 2;
	if( layoutBitsSize + 0x10 == bankStart - 0x800000 ) {
		return;
	}

	// Bank 2 needs to be shifted
	std::vector<ubyte> bank2data;
	bank2data.resize( bank2size );
	rom.seekg( bankStart );
	rom.read( (char*)bank2data.data(), bank2size );

	const uint OUBE = htonl( layoutBitsSize + 0x10 );
	rom.seekp( 0x80000C );
	rom.write( (const char*)&OUBE, 4 );
	for( uint i = 0; i < layoutBitsSize; i++ ) {
		rom.put( (char)0xFF );
	}
	rom.write( (char*)bank2data.data(), bank2size );

}

DialogSideEffect Text::getSideEffect( uint dialogId ) {
	switch( dialogId ) {
		case 10: case 11: case 12:
			return DialogSideEffect::PlayPuzzleSolvedJingle;
		case 5: case 9: case 55: case 164:
			return DialogSideEffect::PlayRaceStartFanfare;
		case 17: case 114: case 117: case 128: case 150:
			return DialogSideEffect::PlayBossMusic;
		case 115: case 118: case 152:
			return DialogSideEffect::StopBossMusic;
		default:
			return DialogSideEffect::None;
	}
}

