#include "src/core/alignment-fix.hpp"

#include "src/core/baserom.hpp"
#include "src/polyfill/byte-order.hpp"

static uint s_segRefs[] = {
	/* BOB */
	0x00405CC8,
	0x00405CD4,
	0x00405CE0,
	/* WF */
	0x0049DFBC,
	0x0049DFC8,
	0x0049DFD4,
	/* JRB */
	0x00424210,
	0x0042421C,
	0x00424228,
	0x00424234,
	/* CCM */
	0x00395E0C,
	0x00395E18,
	0x00395E24,
	0x00395E30,
	/* BBH */
	0x00382CDC,
	0x00382CE8,
	0x00382CF4,
	/* HMC */
	0x003E6D8C,
	0x003E6D98,
	/* LLL */
	0x0048D030,
	0x0048D03C,
	0x0048D048,
	0x0048D054,
	/* SSL */
	0x003FBC4C,
	0x003FBC58,
	0x003FBC64,
	/* DDD */
	0x00495CD0,
	0x00495CDC,
	0x00495CE8,
	0x00495CF4,
	/* SML */
	0x0040E944,
	0x0040E950,
	0x0040E95C,
	0x0040E968,
	/* WDW */
	0x0041A304,
	0x0041A310,
	0x0041A31C,
	/* TTM */
	0x004EB588,
	0x004EB594,
	0x004EB5A0,
	/* THI */
	0x0042C974,
	0x0042C980,
	0x0042C98C,
	/* TTC */
	0x004374CC,
	0x004374D8,
	/* RR */
	0x0044A528,
	0x0044A534,
	0x0044A540,
	/* Castle Grounds */
	0x00454AEC,
	0x00454AF8,
	0x00454B04,
	/* Castle Interior */
	0x003CFBCC,
	0x003CFBD8,
	/* Castle Courtyard */
	0x004AF70C,
	0x004AF718,
	0x004AF724,
	/* Bowser Course 1 */
	0x0045C0D8,
	0x0045C0E4,
	0x0045C0F0,
	/* Bowser Course 2 */
	0x0046AA78,
	0x0046AA84,
	0x0046AA90,
	0x0046AA9C,
	/* Bowser Course 3 */
	0x00477EEC,
	0x00477EF8,
	0x00477F04,
	/* Metal Cap */
	0x004BEA4C,
	0x004BEA58,
	/* Wing Cap */
	0x004C273C,
	0x004C2748,
	0x004C2754,
	/* Vanish Cap */
	0x004612F0,
	0x004612FC,
	/* Bowser 1 */
	0x004C41C4,
	0x004C41D0,
	/* Bowser 2 */
	0x004CEA70,
	0x004CEA7C,
	0x004CEA88,
	0x004CEA94,
	/* Bowser 3 */
	0x004D1660,
	0x004D1684,
	/* Secret Aquarium */
	0x0046C1F4,
	0x0046C200,
	0x0046C20C,
	0x0046C218,
	/* WMotR */
	0x004CD9E4,
	0x004CD9F0,
	0x004CD9FC,
	/* PSS */
	0x004B7F14,
	0x004B7F20,
	/* File Select */
	0x002A6134,
	0x002A6260
};

void fixMisalignedSegments( std::ostream &rom ) {
	// Align skybox segments (0xA)
	rom.seekp( 0xB35714 );
	BaseRom::copyTo( rom, 0xB35715, 0x16A480 );

	// Align envfx segment (0xB)
	rom.seekp( 0xDA2784 );
	BaseRom::copyTo( rom, 0xDA2785, 0x6D98 );

	// Align bank 0x7
	rom.seekp( 0xDB151C );
	BaseRom::copyTo( rom, 0xDB151D, 0x3F209A );

	// Align bank 0x9
	rom.seekp( 0xCA7B94 );
	BaseRom::copyTo( rom, 0xCA7B95, 0xF2BF0 );

	// Align File Select Bank 0x7
	rom.seekp( 0xB1F8B4 );
	BaseRom::copyTo( rom, 0xB1F8B5, 0xDE60 );

	// Align End Cake Screen Bank
	rom.seekp( 0x10BA69A );
	BaseRom::copyTo( rom, 0x10BA69B, 0x27350 );

	for( size_t i = 0; i < sizeof( s_segRefs ) / sizeof( uint ); i++ ) {
		const uint start = BaseRom::getWord( s_segRefs[i] + 4 );
		const uint end = BaseRom::getWord( s_segRefs[i] + 8 );

		const uint data[] = { htonl( start - 1 ), htonl( end - 1 ) };
		rom.seekp( s_segRefs[i] + 4 );
		rom.write( (const char*)data, 8 );
	}

}
