#include "src/core/blueprint-module-manager.hpp"

#include "src/core/blueprint.hpp"
#include "src/core/util.hpp"
#include "src/polyfill/filesystem.hpp"

struct CustomHookInfo {
	ShimType type;
	sbyte nice;
};

static inline string getDisplayName( const AsmModuleDefinition &module ) {
	return module.name + " (v" + module.version.toString() + ')';
}

//TODO: Replace this basic brute force approach with a more efficient solution
static bool verifyShimsAndGlobalAsm(
	const AsmModuleDefinition &moduleA,
	const AsmModuleDefinition &moduleB,
	string &conflict
) {
	HashMap<uint,CustomHookInfo> customShims;
	for( const AsmModuleHook &hook : moduleA.hooks ) {
		if( hook.type != ModuleHook::Custom ) continue;

		if( CustomHookInfo *const shim = Util::tryGetValue( customShims, hook.shimAddress ) ) {
			if( shim->type != hook.shimType ) {
				conflict = "Module "s + getDisplayName( moduleA ) + " is invalid because is specifies two different shim types on the same ROM address.";
				return false;
			} else if( shim->nice < 5 - hook.niceness ) {
				conflict = "Module "s + getDisplayName( moduleA ) + " is invalid because it has two shims on the same address with conflicting niceness values.";
				return false;
			} else if( hook.niceness < shim->nice ) {
				shim->nice = hook.niceness;
			}
		} else {
			customShims[hook.shimAddress] = { hook.shimType, hook.niceness };
		}
	}

	if( !customShims.empty() ) {
		for( const AsmModuleHook &hook : moduleB.hooks ) {
			if( hook.type != ModuleHook::Custom ) continue;

			if( const CustomHookInfo *const shimA = Util::tryGetValue( customShims, hook.shimAddress ) ) {
				if( shimA->type != hook.shimType ) {
					conflict = "Modules "s + getDisplayName( moduleA ) + " and " + getDisplayName( moduleB ) + " conflict because they have shims with different types on the same ROM address. One of them is invalid!";
					return false;
				} else if( shimA->nice < 5 - hook.niceness ) {
					conflict = "Modules "s + getDisplayName( moduleA ) + " and " + getDisplayName( moduleB ) + " conflict because they have shims on the same ROM address that have conflicting niceness values.";
					return false;
				}
			}

		}
	}

	if( moduleA.globalFiles.empty() ) {
		return true;
	}

	for( const AbsoluteAsmFile &afb : moduleB.globalFiles ) {
		for( const AbsoluteAsmFile &afa : moduleA.globalFiles ) {
			if( afa.romStart < afb.romEnd && afb.romStart < afa.romEnd ) {
				conflict = "Modules "s + getDisplayName( moduleA ) + " and " + getDisplayName( moduleB ) + " conflict because they both overwrite the same segment of the ROM.";
				return false;
			}
		}
	}

	return true;
}

static bool verifyRecursive(
	const AsmModuleDefinition &module,
	std::deque<Uuid> &dependencyStack,
	HashMap<Uuid,Version> &modulesToAdd,
	string &conflict
) {
	// Verify no circular dependencies
	for( const Uuid &d : dependencyStack ) {
		if( module.id == d ) {
			conflict =  "Module has a circular dependency";
			return false;
		}
	}

	// Verify no shim, global asm, or dependency version conflicts
	for( const auto &i : modulesToAdd ) {
		const AsmModuleDefinition &other = AsmModuleStore::getModuleVersions( i.first ).at( i.second );
		if( !verifyShimsAndGlobalAsm( module, other, conflict ) ) {
			return false;
		}
	}

	for( const auto &i : Blueprint::current()->asmModules() ) {
		const AsmModuleDefinition &other = i.second.definition;
		if( other.id == module.id  || modulesToAdd.count( other.id ) ) {
			continue;
		}

		if( !verifyShimsAndGlobalAsm( module, other, conflict ) ) {
			return false;
		}

		for( const AsmModuleDependency &d : other.dependencies ) {
			if( d.id != module.id ) continue;

			if( d.minVersion > module.version ) {
				conflict = "Modules "s + getDisplayName( module ) + " and " + getDisplayName( other ) + " conflict because the later has a dependency on " +
							d.name + " with a minimum version that is higher than what is allowed by your new module.";
				return false;
			}

			if( d.maxVersion < module.version ) {
				conflict = "Modules "s + getDisplayName( module ) + " and " + getDisplayName( other ) + " conflict because the later has a dependency on " +
							d.name + " with a maximum version that is lower than what is required to add your new module.";
				return false;
			}
		}
	}

	// Verify dependencies
	modulesToAdd[module.id] = module.version;
	dependencyStack.push_back( module.id );
	const std::map<Uuid,AsmModule> &activeModules = Blueprint::current()->asmModules();
	for( const AsmModuleDependency &dependency : module.dependencies ) {
		if( const Version *const activeVersion = Util::tryGetValue( modulesToAdd, dependency.id ) ) {
			if( *activeVersion >= dependency.minVersion && *activeVersion <= dependency.maxVersion ) {
				continue;
			}
		}

		if( const AsmModule *const activeDependency = Util::tryGetValue( activeModules, dependency.id ) ) {
			const Version &activeVersion = activeDependency->definition.version;
			if( activeVersion > dependency.maxVersion ) {
				conflict = "Modules "s + getDisplayName( module ) + " and " + getDisplayName( activeModules.at( dependency.id ).definition ) +
							" conflict because the later is installed at an earlier version than is required by the module you wish to install. "
							"You may be able to resolve this conflict by updating the module.";
				dependencyStack.pop_back();
				return false;
			}

			if( activeVersion >= dependency.minVersion ) {
				continue;
			}
		}


		const std::map<Version,AsmModuleDefinition> &availableVersions = AsmModuleStore::getModuleVersions( dependency.id );
		bool foundVersion = false;
		bool foundGoodVersion = false;

		for( auto i = availableVersions.rbegin(); i != availableVersions.rend(); i++ ) {
			if( i->first >= dependency.minVersion && i->first <= dependency.maxVersion ) {
				foundVersion = true;
				HashMap<Uuid,Version> tentativeModulesToAdd = modulesToAdd;
				if( verifyRecursive( i->second, dependencyStack, tentativeModulesToAdd, conflict ) ) {
					modulesToAdd = std::move( tentativeModulesToAdd );
					foundGoodVersion = true;
					break;
				}
			}
		}

		if( availableVersions.empty() ) {
			conflict = "The module has a dependency ("s + dependency.name + ") that you do not have installed.";
			dependencyStack.pop_back();
			return false;
		}

		if( !foundVersion ) {
			conflict = "You do not have a version of "s + dependency.name + " installed that satisfies the version requirements of the dependencies of the module"
					" you wish to install.";
			dependencyStack.pop_back();
			return false;
		}

		if( !foundGoodVersion ) {
			dependencyStack.pop_back();
			return false;
		}

	}

	dependencyStack.pop_back();
	return true;
}

static inline void addModuleInternal( const AsmModuleDefinition &module ) {
	const fs::path blueprintModulePath = Blueprint::current()->getModulePath( module.id );
	fs::forceDeleteRecursive( blueprintModulePath );
	fs::create_directories( blueprintModulePath.parent_path() );
	fs::copy(
		AsmModuleStore::installedModulePath( module.id, module.version ),
		blueprintModulePath,
		fs::copy_options::recursive
	);
	clearReadOnlyFlagOnWindows( blueprintModulePath );

	AsmModule activeModule;
	activeModule.definition = module;
	activeModule.areaMask.fill( 0 );
	for( const AsmModuleProperty &prop : module.properties ) {
		activeModule.parameters[prop.name] = prop.defaultValue;
	}

	Blueprint::currentEditable()->asmModules()[module.id] = std::move( activeModule );
}

bool BlueprintModules::tryAddModule(
	const AsmModuleDefinition &module,
	string &errorReason
) {
	std::deque<Uuid> dependencyStack;
	HashMap<Uuid,Version> modulesToAdd;

	if( verifyRecursive( module, dependencyStack, modulesToAdd, errorReason ) ) {
		for( const auto &i : modulesToAdd ) {
			const AsmModuleDefinition &m = AsmModuleStore::getModuleVersions( i.first ).at( i.second );
			addModuleInternal( m );
		}
		return true;
	} else {
		return false;
	}
}
