#include "src/core/background.hpp"

#include <QImage>
#include <array>
#include "src/core/exceptions.hpp"
#include "src/types.hpp"
#include "src/polyfill/byte-order.hpp"

static inline void writePixel( std::ostream &rom, uint argb ) {
	const ushort pixel = htons(
		( ( argb >> 8 ) & 0xF800 ) |
		( ( argb >> 5 ) & 0x7C0 ) |
		( ( argb >> 2 ) & 0x3E ) |
		0x1
	);
	rom.write( (const char*)&pixel, 2 );
}

static inline uint getPointerBE( int x, int y ) {
	return htonl( 0x0A000000 | ((y << 14) + (x << 11)) );
}

static const std::array<uint,80> &getTexturePointers() {
	static std::array<uint,80> s_pointers;
	int i = 0;
	for( int y = 0; y < 8; y++ ) {
		s_pointers[i++] = getPointerBE( 7, y );
		for( int x = 0; x < 8; x++ ) {
			s_pointers[i++] = getPointerBE( x, y );
		}
		s_pointers[i++] = getPointerBE( 0, y );
	}
	return s_pointers;
}

void Background::writeToRom( std::ostream &rom, const string &imagePath ) {
	QImage image( imagePath.c_str(), "PNG" );
	if( image.isNull() ) {
		throw BlueprintLoadError( "Failed to load background image from blueprint." );
	}

	if( image.width() != 248 || image.height() != 248 ) {
		image = image.scaled( 248, 248, Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
	}

	if( image.format() != QImage::Format_ARGB32 ) {
		image = image.convertToFormat( QImage::Format_ARGB32 );
	}

	const uint *pixels = (uint*)image.constBits();
	for( int j = 0; j < 8; j++ ) {
		const int y0 = j * 31;
		for( int i = 0; i < 8; i++ ) {
			const int x0 = i * 31;
			for( int y = y0; y < y0 + 32; y++ ) {
				for( int x = x0; x < x0 + 32; x++ ) {
					writePixel( rom, pixels[((y % 248) * 248) + (x % 248)] );
				}
			}
		}
	}

	static const std::array<uint,80> &texturePointers = getTexturePointers();
	rom.write( (const char*)texturePointers.data(), 320 );
}
