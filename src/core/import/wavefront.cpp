#include "src/core/import/wavefront.hpp"

#include <regex>
#include <map>
#include <sstream>
#include <utility>
#include <unordered_set>
#include <cctype>
#include <cstring>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/util.hpp"
#include "src/core/import/texture.hpp"

using namespace std;

static const regex IndexTupleRegex(
	"^(\\d+)/(\\d*)(?:/(\\d+))$",
	regex_constants::ECMAScript | regex_constants::optimize
);

static inline void correctPath( string &path ) {
	for( char &c : path ) {
#ifdef _WIN32
		if( c == '/' ) c = '\\';
#else
		if( c == '\\' ) c = '/';
#endif
	}
}

static const regex WindowsReservedNamesRegex(
	"^(con|prn|aux|nul|com\\d|lpt\\d)$",
	regex_constants::ECMAScript | regex_constants::optimize | regex_constants::nosubs
);

static string makeSafeName( const string &matName ) {
	string safeName;
	safeName.reserve( matName.length() );

	for( const char c : matName ) switch( c ) {
		case '&': safeName += "&amp;"; break;
		case '<': safeName += "&lt;"; break;
		case '>': safeName += "&gt;"; break;
		case ':': safeName += "&#58;"; break;
		case '"': safeName += "&quot;"; break;
		case '/': safeName += "&#47;"; break;
		case '\\': safeName += "&#92;"; break;
		case '|': safeName += "&#124;"; break;
		case '?': safeName += "&#63;"; break;
		case '*': safeName += "&#42;"; break;
		default: safeName += c;
	}

	Util::makeLowerCase( safeName );
	if( regex_match( safeName, IndexTupleRegex ) ) {
		return "&#00;"s + safeName;
	}

	return safeName;
}

static void importMaterials(
	HashMap<string,MaterialFaces> &materialMap,
	const fs::path &matFile,
	const fs::path &textureOutputDir,
	const string &prefix,
	std::vector<string> &warnings
) {
	string currentMaterial;

	FileReadStream file( matFile.u8string() );
	while( file.good() && !file.eof() ) {
		string lineStr, cmd;
		getline( file, lineStr );
		if( lineStr.length() < 1 ) {
			continue;
		}

		istringstream line( lineStr );
		line >> cmd;

		if( cmd == "newmtl" ) {
			currentMaterial.clear();
			getline( line, currentMaterial );
			currentMaterial = makeSafeName( prefix + trim( currentMaterial ) );
			materialMap[currentMaterial].materialName = currentMaterial;
		} else if( cmd == "map_Kd" && !currentMaterial.empty() ) {
			string tex;
			getline( line, tex );
			correctPath( tex );
			fs::path texturePath( trim( tex ) );

			if( texturePath.is_relative() ) {
				texturePath = matFile.parent_path() / texturePath;
			}

			if( !fs::exists( texturePath ) ) {
				warnings.push_back( "Texture file not found: " + texturePath.u8string() );
				currentMaterial.clear();
				continue;
			}

			TextureFile texFile;
			TextureFile::TextureLoadStatus status = TextureFile::tryOpen( texturePath.u8string().c_str(), texFile );
			if( status != TextureFile::Error ) {
				MaterialFaces &material = materialMap[currentMaterial];
				material.textureInfo = texFile.info();
				texFile.saveAsPng( ( textureOutputDir / ( texFile.info().hash + ".png" ) ).u8string().c_str() );
				if( status == TextureFile::Resized ) {
					warnings.push_back( "Texture file \""s + texturePath.u8string() + "\" was resized." );
				}
			} else {
				warnings.push_back( "Error loading texture file: "s + texturePath.u8string() );
			}

			currentMaterial.clear();
		}
	}

	if( !file.good() && !file.eof() ) {
		throw FileParserException( "Error reading material file." );
	}
}

ImportedModel WavefrontImporter::import(
	const fs::path &filePath,
	double scale,
	const fs::path &textureOutputDir,
	const fs::path &modelOutputDir,
	const string &prefix
) const {
	FileReadStream file( filePath.u8string() );
	HashMap<string,MaterialFaces> materials;
	HashMap<string,UVStatus> materialUVStatus;
	size_t lineNumber = 0;

	string currentMaterial = "undefined";
	vector<Vec3f> vertices;
	vector<Vec2f> uvcs;
	vector<Vec3f> normals;

	fs::create_directories( textureOutputDir );
	fs::create_directories( modelOutputDir );

	std::vector<string> warnings;
	while( file.good() && !file.eof() ) {
		string lineStr;
		std::getline( file, lineStr );
		lineNumber++;
		if( isWhitespaceOrEmpty( lineStr ) ) continue;

		istringstream line( lineStr );
		string cmd;
		line >> cmd;

		if( cmd == "mtllib") {
			string matFile;
			std::getline( line, matFile );
			correctPath( matFile );

			fs::path matFilePath( trim( matFile ) );
			if( matFilePath.is_relative() ) {
				matFilePath = filePath.parent_path() / matFilePath;
			}
			importMaterials( materials, matFilePath, textureOutputDir, prefix, warnings );
		} else if( cmd == "usemtl" ) {
			currentMaterial.clear();
			std::getline( line, currentMaterial );
			currentMaterial = makeSafeName( prefix + trim( currentMaterial ) );
		} else if( cmd == "v" ) {
			float x, y, z;
			line >> x >> y >> z;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex.", lineNumber );
			}
			vertices.push_back({ x, y, z });
		} else if( cmd == "vt" ) {
			float u, v;
			line >> u >> v;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex UV coordinates.", lineNumber );
			}
			uvcs.push_back({ u, 1.f - v });
		} else if( cmd == "vn" ) {
			float x, y, z;
			line >> x >> y >> z;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex normal.", lineNumber );
			}
			normals.push_back({ x, y, z });
		} else if( cmd == "f" ) {
			ModelFace face;

			bool hasTexture;
			ushort texWidth, texHeight;

			const MaterialFaces *materialInfo = Util::tryGetValue( materials, currentMaterial );
			if( materialInfo != nullptr && materialInfo->textureInfo.has_value() ) {
				hasTexture = true;
				texWidth = materialInfo->textureInfo.value().width;
				texHeight = materialInfo->textureInfo.value().height;
			} else {
				hasTexture = false;
				texWidth = texHeight = 32;
			}

			Vec3f positions[3];
			bool autocalulateNormals = false;
			std::array<Vec2f,3> triUVs;
			for( int i = 0; i < 3; i++ ) {
				string vrefs;
				line >> vrefs;

				smatch matches;
				if( !regex_search( vrefs, matches, IndexTupleRegex ) || matches.size() < 3 ) {
					throw FileParserException( "Invalid face vertex: "s + vrefs, lineNumber );
				}

				size_t vi = stoull( matches[1].str() ) - 1;
				if( vi >= vertices.size() ) {
					throw FileParserException( "Vertex index out of range: "s + std::to_string( vi ), lineNumber );
				}
				positions[i] = vertices[vi];
				face[i].setPosition( vertices[vi] * (float)scale );

				if( !matches[2].str().empty() ) {
					size_t vti = stoull( matches[2].str() ) - 1;
					if( vti >= uvcs.size() ) {
						throw FileParserException( "UV index out of range: "s + std::to_string( vti ), lineNumber );
					}

					triUVs[i] = uvcs[vti];
				} else {
					triUVs[i] = { 0, 0 };
				}

				if( !matches[3].str().empty() ) {
					size_t vni = stoull( matches[3].str() ) - 1;
					if( vni >= normals.size() ) {
						throw FileParserException( "Vertex normal index out of range: "s + std::to_string( vni ), lineNumber );
					}
					face[i].setNormal( normals[vni] );
				} else {
					autocalulateNormals = true;
				}

			}

			if( !line.eof() ) {
				string extraChars;
				line >> extraChars;

				for( char c : extraChars ) {
					if( !std::isspace( c ) ) {
						throw FileParserException( "Face contains more than 3 vertices. Did you forget to triangulate your model when exporting?", lineNumber );
					}
				}
			}

			if( hasTexture ) {
				UVResult uvStatus = translateUVs( triUVs, texWidth, texHeight );
				face[0].setUV( uvStatus.uvs[0] );
				face[1].setUV( uvStatus.uvs[1] );
				face[2].setUV( uvStatus.uvs[2] );

				if( materialUVStatus.count( currentMaterial ) < 1 || materialUVStatus.at( currentMaterial ) < uvStatus.status ) {
					materialUVStatus[currentMaterial] = uvStatus.status;
				}
			} else {
				face[0].setUV( triUVs[0], texWidth, texHeight );
				face[1].setUV( triUVs[1], texWidth, texHeight );
				face[2].setUV( triUVs[2], texWidth, texHeight );
			}

			if( autocalulateNormals ) {
				Vec3f normal = ( positions[1] - positions[0] ).cross( positions[2] - positions[1] );
				normal /= normal.magnitude();
				face[0].setNormal( normal );
				face[1].setNormal( normal );
				face[2].setNormal( normal );
			}

			materials[currentMaterial].faces.push_back( face );
		}

	}

	if( !file.eof() ) {
		throw FileParserException( lineNumber );
	}

	for( const auto &i : materialUVStatus ) {
		const string &material = i.first;
		const UVStatus &status = i.second;

		switch( status ) {
			case UVStatus::Good:
			case UVStatus::Shifted:
				continue;
			case UVStatus::BreaksClamping:
				warnings.push_back( "UV coordinates on material \""s + material + "\" were automatically shifted to fit in the range supported by SM64. Clamping this texture may not behave as expected." );
				break;
			case UVStatus::BreaksMirroring:
				warnings.push_back( "UV coordinates on material \""s + material + "\" were automatically shifted to fit in the range supported by SM64. Clamping or mirroring this texture may not behave as expected." );
				break;
			case UVStatus::Misaligned:
				warnings.push_back( "UV coordinates on material \""s + material + "\" were automatically shifted to fit in the range supported by SM64, but alignment could not be maintained. To fix this issue, split large triangles containing frequently repeating textures into smaller sections." );
				break;
			case UVStatus::RangeError:
				warnings.push_back( "UV coordinates on material \""s + material + "\" spanned a range larger then 2048 pixels in a single triangle, which is more than SM64 can support. To fix this issue, split large triangles containing frequently repeating textures into smaller sections." );
				break;
		}
	}

	BoundingBox boundingBox = optimizeAndSave( materials, modelOutputDir );
	return ImportedModel{
		std::move( materials ),
		boundingBox,
		std::move( warnings )
	};
}
