#ifndef SRC_CORE_IMPORT_MODEL_OPTIMIZER_HPP_
#define SRC_CORE_IMPORT_MODEL_OPTIMIZER_HPP_

#include "src/core/import/model-importer.hpp"
#include <iostream>
#include <vector>
#include <array>

struct PatchLoadInfo {
	uint indexOffset;
	ubyte numVertices;
	std::vector<std::array<ubyte,3>> triangles;
};

struct ModelLoadInfo {
	std::vector<ModelVertex> vertexData;
	std::vector<PatchLoadInfo> loadData;
	float priority;

	void save( std::ostream &file ) const;
	void load( std::istream &file );
};

struct OptimizedModel {
	ModelLoadInfo modelLoader;
	BoundingBox boundingBox;
};

extern OptimizedModel optimizeModel( const MaterialFaces &material );

#endif /* SRC_CORE_IMPORT_MODEL_OPTIMIZER_HPP_ */
