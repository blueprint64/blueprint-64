#ifndef CORE_WAVEFRONT_HPP_
#define CORE_WAVEFRONT_HPP_

#include "src/core/import/model-importer.hpp"

class WavefrontImporter final : public IModelImporter {

	public:
	WavefrontImporter() {}
	~WavefrontImporter() {}

	static const string &extension() { static const string s_ext = ".obj"; return s_ext; }

	const char *formatName() const override { return "Wavefront"; }
	const char *fileExtension() const override { return extension().data(); }

	ImportedModel import(
		const fs::path &filePath,
		double scale,
		const fs::path &textureOutputDir,
		const fs::path &modelOutputDir,
		const string &prefix = ""
	) const override;

};

#endif /* CORE_WAVEFRONT_HPP_ */
