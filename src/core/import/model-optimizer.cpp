#include "src/core/import/model-optimizer.hpp"

#include <stack>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cassert>
#include <cmath>
#include "src/polyfill/byte-order.hpp"

typedef uint AbsoluteIndex;
typedef ubyte BufferIndex;

struct BoundingBoxF {
	float min_x, max_x;
	float min_y, max_y;
	float min_z, max_z;
};

struct TriangleTags {
	std::array<uint,3> vertexIndicies;
	bool live;
};

struct VertexTags {
	const ModelVertex *data;
	std::vector<TriangleTags*> users;
	uint liveTriangles;
	uint lastVisited;
};

static BoundingBoxF setupTags(
	const MaterialFaces &material,
	std::vector<VertexTags> &vertexTags,
	TriangleTags *triangleTags
) {
	uint i = 0;
	BoundingBoxF boundingBox = {
		INFINITY, -INFINITY,
		INFINITY, -INFINITY,
		INFINITY, -INFINITY
	};
	vertexTags.reserve( 2 * material.faces.size() );
	HashMap<ModelVertex,AbsoluteIndex> indexMap;
	for( const ModelFace &face : material.faces ) {
		TriangleTags *tri = &triangleTags[i++];
		uint j = 0;
		for( const ModelVertex &vertex : face ) {
			if( indexMap.count( vertex ) ) {
				tri->vertexIndicies[j++] = indexMap[vertex];
				vertexTags[indexMap.at( vertex )].users.push_back( tri );
				vertexTags[indexMap.at( vertex )].liveTriangles++;
			} else {
				tri->vertexIndicies[j++] = (uint)vertexTags.size();
				indexMap[vertex] = (uint)vertexTags.size();
				vertexTags.push_back({ &vertex, { tri }, 1, 0 });
				Vec3f position = vertex.getPosition();
				if( position.x < boundingBox.min_x ) boundingBox.min_x = position.x;
				if( position.x > boundingBox.max_x ) boundingBox.max_x = position.x;
				if( position.y < boundingBox.min_y ) boundingBox.min_y = position.y;
				if( position.y > boundingBox.max_y ) boundingBox.max_y = position.y;
				if( position.z < boundingBox.min_z ) boundingBox.min_z = position.z;
				if( position.z > boundingBox.max_z ) boundingBox.max_z = position.z;
			}
		}
		assert( j == 3 );
		tri->live = true;
	}
	assert( i == material.faces.size() );
	return boundingBox;
}

static std::vector<AbsoluteIndex> reorderTriangles( std::vector<VertexTags> &verts, uint numTriangles ) {
	// Optimize vertex order using the Tipsify algorithm
	// https://gfx.cs.princeton.edu/pubs/Sander_2007_%3ETR/tipsy.pdf
	std::vector<AbsoluteIndex> newOrder;
	newOrder.reserve( 3 * numTriangles );

	constexpr uint BUFFER_SIZE = 16;

	int v = 0;
	uint cursor = 1;
	uint visitTime = BUFFER_SIZE + 1;
	std::stack<AbsoluteIndex> deadEnds;

	while( v >= 0 ) {
		std::vector<AbsoluteIndex> candidates;
		candidates.reserve( verts[v].liveTriangles * 3 );
		for( TriangleTags *T : verts[v].users ) {
			if( !T->live ) continue;
			for( AbsoluteIndex tv : T->vertexIndicies ) {
				newOrder.push_back( tv );
				candidates.push_back( tv );
				deadEnds.push( tv );
				verts[tv].liveTriangles--;
				if( visitTime - verts[tv].lastVisited > BUFFER_SIZE ) {
					verts[tv].lastVisited = visitTime++;
				}
			}
			T->live = false;
		}

		v = -1;
		uint bestPriority = -1;
		for( uint cv : candidates ) {
			if( verts[cv].liveTriangles <= 0 ) continue;
			uint priority = 0;
			if( visitTime - verts[cv].lastVisited + (2 * verts[cv].liveTriangles) <= BUFFER_SIZE ) {
				priority = visitTime - verts[cv].lastVisited;
			}
			if( priority > bestPriority ) {
				bestPriority = priority;
				v = cv;
			}
		}

		while( v < 0 && !deadEnds.empty() ) {
			v = deadEnds.top();
			deadEnds.pop();
			if( verts[v].liveTriangles <= 0 ) {
				v = -1;
			}
		}

		while( v < 0 && cursor < verts.size() ) {
			if( verts[cursor++].liveTriangles > 0 ) {
				v = cursor - 1;
				break;
			}
		}
	}

	assert( newOrder.size() == 3 * numTriangles );
	return newOrder;
}


static inline bool canFitNextTriangle(
	const std::vector<AbsoluteIndex> &vertexOrdering,
	const std::vector<AbsoluteIndex> &buffer,
	const HashMap<AbsoluteIndex,BufferIndex> &bufferLookup,
	uint k
) {
	assert( buffer.size() == bufferLookup.size() );
	if( k + 3 > vertexOrdering.size() ) return false;
	int freeSpace = 16 - (int)buffer.size();
	freeSpace -= 1 - (int)bufferLookup.count( vertexOrdering[k++] );
	freeSpace -= 1 - (int)bufferLookup.count( vertexOrdering[k++] );
	freeSpace -= 1 - (int)bufferLookup.count( vertexOrdering[k++] );
	return freeSpace >= 0;
}

static inline uint fillBuffer(
	const std::vector<AbsoluteIndex> &vertexOrdering,
	std::vector<AbsoluteIndex> &buffer,
	HashMap<AbsoluteIndex,BufferIndex> &bufferLookup,
	uint i
) {
	while( canFitNextTriangle( vertexOrdering, buffer, bufferLookup, i ) ) {
		for( uint j = i + 3; i < j; i++ ) {
			const AbsoluteIndex v = vertexOrdering[i];
			if( bufferLookup.count( v ) == 0 ) {
				bufferLookup[v] = (BufferIndex)buffer.size();
				buffer.push_back( v );
				assert( buffer.size() <= 16 );
			}
		}
		assert( i % 3 == 0 );
	}
	return i;
}

static inline ubyte maybeBacktrack(
	const std::vector<AbsoluteIndex> &previousBuffer,
	std::vector<AbsoluteIndex> &currentBuffer,
	HashMap<AbsoluteIndex,BufferIndex> &bufferLookup
) {
	assert( currentBuffer.size() <= 16 );
	assert( bufferLookup.size() == currentBuffer.size() );

	uint freeSpace = 16 - (uint)currentBuffer.size();
	uint stepsBack = 0;

	uint actualFreeSpace = freeSpace;
	uint actualStepsBack = stepsBack;

	while( stepsBack < previousBuffer.size() && stepsBack < currentBuffer.size() ) {
		const AbsoluteIndex candidate =  previousBuffer[previousBuffer.size() - (stepsBack + 1)];
		const auto maybeMatch = bufferLookup.find( candidate );
		if( maybeMatch != bufferLookup.end() ) {
			actualStepsBack = ++stepsBack;
			actualFreeSpace = freeSpace;
		} else if( freeSpace > 0 ) {
			freeSpace--;
			stepsBack++;
		} else break;
	}

	if( actualStepsBack == 0 ) return 0;

	freeSpace = actualFreeSpace;
	stepsBack = actualStepsBack;

	std::vector<AbsoluteIndex> newBuffer;
	HashMap<AbsoluteIndex,BufferIndex> newLookup;
	newBuffer.reserve( 16 - freeSpace );

	BufferIndex j = 0;
	for( uint i = (uint)previousBuffer.size() - stepsBack; i < previousBuffer.size(); i++ ) {
		AbsoluteIndex v = previousBuffer[i];
		newBuffer.push_back( v );
		newLookup[v] = j++;
	}

	for( AbsoluteIndex i : currentBuffer ) {
		if( newLookup.count( i ) == 0 ) {
			newBuffer.push_back( i );
			newLookup[i] = j++;
		}
	}

	assert( j <= 16 );
	assert( j == newLookup.size() );
	assert( j == newBuffer.size() );
	#ifndef NDEBUG
	for( const AbsoluteIndex &v : currentBuffer ) {
		assert( newLookup.count( v ) > 0 );
	}
	#endif

	currentBuffer = std::move( newBuffer );
	bufferLookup = std::move( newLookup );

	return stepsBack;
}

static ModelLoadInfo reduceVertices(
	const std::vector<VertexTags> &uniqueVertices,
	const std::vector<AbsoluteIndex> &vertexOrdering
) {
	ModelLoadInfo out;
	out.vertexData.reserve( vertexOrdering.size() );
	out.loadData.reserve( 1 + (vertexOrdering.size() / 15) );

	std::vector<AbsoluteIndex> currentBuffer;
	std::vector<AbsoluteIndex> previousBuffer;
	currentBuffer.reserve( 16 );
	previousBuffer.reserve( 16 );

	HashMap<AbsoluteIndex,BufferIndex> bufferLookup;
	bufferLookup.rehash( 64 );

	uint i = 0;
	while( i < vertexOrdering.size() ) {
		assert( i % 3 == 0 );
		currentBuffer.clear();
		bufferLookup.clear();

		uint j = fillBuffer( vertexOrdering, currentBuffer, bufferLookup, i );
		assert( (j - i) % 3 == 0 );
		assert( j > i );
		assert( j <= vertexOrdering.size() );

		ubyte overlap = maybeBacktrack( previousBuffer, currentBuffer, bufferLookup );
		assert( overlap < currentBuffer.size() );

		for( BufferIndex b = overlap; b < (BufferIndex)currentBuffer.size(); b++ ) {
			assert( currentBuffer[b] < uniqueVertices.size() );
			out.vertexData.push_back( *uniqueVertices[currentBuffer[b]].data );
		}

		#ifndef NDEBUG
		uint r = (uint)out.vertexData.size() - (uint)currentBuffer.size();
		for( uint k = 0; k < (uint)currentBuffer.size(); k++ ) {
			assert( out.vertexData[r+k] == *uniqueVertices[currentBuffer[k]].data );
			assert( bufferLookup.at( currentBuffer[k] ) == k );
		}
		#endif

		PatchLoadInfo patch;
		patch.indexOffset = (uint)out.vertexData.size() - (uint)currentBuffer.size();
		patch.numVertices = (ubyte)currentBuffer.size();
		while( i < j ) {
			patch.triangles.push_back({
				bufferLookup.at( vertexOrdering[i++] ),
				bufferLookup.at( vertexOrdering[i++] ),
				bufferLookup.at( vertexOrdering[i++] )
			});
		}
		assert( i == j );
		out.loadData.push_back( std::move( patch ) );

		previousBuffer = currentBuffer;
	}
	out.vertexData.shrink_to_fit();
	out.loadData.shrink_to_fit();
	return out;
}

static float approximateOcclusionPotential(
	const std::vector<ModelVertex> &vertices,
	const PatchLoadInfo &patch,
	const Vec3f &modelCentre,
	bool useVertexColouring
) {
	Vec3f patchCentre( 0, 0, 0 );
	Vec3f averageNormal( 0, 0, 0 );

	HashSet<BufferIndex> uniqueVertices;
	for( const std::array<BufferIndex,3> &triangle : patch.triangles ) {
		for( BufferIndex i : triangle ) {
			uniqueVertices.insert( i );
		}
		if( useVertexColouring ) {
			const Vec3f a = vertices[patch.indexOffset + triangle[0]].getPosition();
			const Vec3f b = vertices[patch.indexOffset + triangle[0]].getPosition();
			const Vec3f c = vertices[patch.indexOffset + triangle[0]].getPosition();
			const Vec3f normal = ( b - a ).cross( c - b );
			averageNormal += normal / normal.magnitude();
		}
	}

	for( BufferIndex i : uniqueVertices ) {
		const ModelVertex &vertex = vertices[patch.indexOffset + i];
		patchCentre += vertex.getPosition();
		if( !useVertexColouring ) {
			averageNormal += vertex.getNormal();
		}
	}
	patchCentre /= (float)uniqueVertices.size();
	averageNormal /= useVertexColouring ? (float)patch.triangles.size() : (float)uniqueVertices.size();

	return (patchCentre - modelCentre).dot( averageNormal );
}

static void optimizeLoadOrder( ModelLoadInfo &loadInfo, bool useVertexColouring ) {
	Vec3f modelCentre( 0, 0, 0 );
	for( const ModelVertex &vertex : loadInfo.vertexData ) {
		modelCentre += vertex.getPosition();
	}
	modelCentre /= (float)loadInfo.vertexData.size();

	std::multimap<float,uint> priorityMap;
	for( uint i = 0; i < loadInfo.loadData.size(); i++ ) {
		const float priority = approximateOcclusionPotential( loadInfo.vertexData, loadInfo.loadData[i], modelCentre, useVertexColouring );
		priorityMap.insert({ -priority, i });
	}

	assert( priorityMap.size() == loadInfo.loadData.size() );
	std::vector<PatchLoadInfo> reorderedLoads;
	reorderedLoads.reserve( priorityMap.size() );
	for( const auto &i : priorityMap ) {
		reorderedLoads.push_back( std::move( loadInfo.loadData[i.second] ) );
	}
	loadInfo.loadData = std::move( reorderedLoads );
}

static inline float getModelPriority( const BoundingBoxF &bounds, size_t numTris ) {
	const float size_x = bounds.max_x - bounds.min_x;
	const float size_y = bounds.max_y - bounds.min_y;
	const float size_z = bounds.max_z - bounds.min_z;

	float A, B;
	if( size_x > size_y ) {
		A = size_x;
		B = size_y > size_z ? size_y : size_z;
	} else {
		A = size_y;
		B = size_x > size_z ? size_x : size_z;
	}
	return A * B / std::sqrt( (float)numTris );
}

OptimizedModel optimizeModel( const MaterialFaces &material ) {
	std::vector<VertexTags> taggedVertices;
	TriangleTags *taggedTriangles = new TriangleTags[material.faces.size()];

	BoundingBoxF boundingBox = setupTags( material, taggedVertices, taggedTriangles );
	ModelLoadInfo modelLoadInfo = reduceVertices(
		taggedVertices,
		reorderTriangles( taggedVertices, (uint)material.faces.size() )
	);
	modelLoadInfo.priority = getModelPriority( boundingBox, material.faces.size() );

	optimizeLoadOrder( modelLoadInfo, material.useVertexColouring );

#ifndef NDEBUG
	size_t numTriangles = 0;
	for( const PatchLoadInfo &patch : modelLoadInfo.loadData ) {
		numTriangles += patch.triangles.size();
	}
	assert( numTriangles == material.faces.size() );

	#ifdef DEBUG
	const size_t initialVertexCount = material.faces.size() * 3;
	const size_t optimizedVertexCount = modelLoadInfo.vertexData.size();
	const float relativeSize = 100.0f * (float)((double)optimizedVertexCount/(double)initialVertexCount);

	std::cout << "Compressed " << initialVertexCount << " vertices down to " << optimizedVertexCount << " vertices. (" << relativeSize << "%)" << std::endl;
	#endif
#endif

	return OptimizedModel {
		std::move( modelLoadInfo ),
		{
			(short)boundingBox.min_x, (short)boundingBox.max_x,
			(short)boundingBox.min_y, (short)boundingBox.max_y,
			(short)boundingBox.min_z, (short)boundingBox.max_z
		}
	};
}

void ModelLoadInfo::save( std::ostream &file ) const {
	const uint priorityBE = htonl( reinterpret_cast<const uint&>( priority ) );
	file.write( (const char*)&priorityBE, 4 );
	const uint numVertsBE = htonl( (uint)vertexData.size() );
	file.write( (const char*)&numVertsBE, 4 );
	file.write( (const char*)vertexData.data(), vertexData.size() * 16 );
	const ushort numPatchesBE = htons( (ushort)loadData.size() );
	file.write( (const char*)&numPatchesBE, 2 );
	for( const PatchLoadInfo &patch : loadData ) {
		const uint offsetBE = htonl( patch.indexOffset );
		file.write( (const char*)&offsetBE, 4 );
		file.put( patch.numVertices );
		const ushort numTrisBE = htons( (ushort)patch.triangles.size() );
		file.write( (const char*)&numTrisBE, 2 );
		for( const std::array<BufferIndex,3> &triangle : patch.triangles ) {
			file.put( triangle[0] );
			file.put( triangle[1] );
			file.put( triangle[2] );
		}
	}
}

void ModelLoadInfo::load( std::istream &file ) {
	vertexData.clear();
	loadData.clear();
	uint priorityBE, priorityBits, numVertsBE;
	file.read( (char*)&priorityBE, 4 );
	priorityBits = ntohl( priorityBE );
	priority = reinterpret_cast<float&>( priorityBits );
	file.read( (char*)&numVertsBE, 4 );
	const uint numVerts = ntohl( numVertsBE );
	vertexData.resize( numVerts );
	file.read( (char*)vertexData.data(), 16 * numVerts );
	ushort numPatchesBE;
	file.read( (char*)&numPatchesBE, 2 );
	const ushort numPatches = ntohs( numPatchesBE );
	loadData.reserve( numPatches );
	for( size_t i = 0; i < numPatches; i++ ) {
		PatchLoadInfo patch;
		file.read( (char*)&patch.indexOffset, 4 );
		patch.indexOffset = ntohl( patch.indexOffset );
		patch.numVertices = (ubyte)file.get();
		ushort numTrisBE;
		file.read( (char*)&numTrisBE, 2 );
		const ushort numTris = ntohs( numTrisBE );
		patch.triangles.reserve( numTris );
		for( size_t j = 0; j < numTris; j++ ) {
			patch.triangles.push_back({ (ubyte)file.get(), (ubyte)file.get(), (ubyte)file.get() });
		}
		loadData.push_back( std::move( patch ) );
	}
}
