#ifndef SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_
#define SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_

#include "src/types.hpp"
#include "src/core/util.hpp"
#include "src/core/vector.hpp"
#include "src/core/texture-info.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/file-dialog.hpp"

#include <optional>
#include <vector>
#include <array>
#include <unordered_map>

struct ModelVertex {
	ubyte data[16];

	Vec3f getPosition() const;
	Vec2f getUV( uint textureWidth, uint textureHeight ) const;
	Vec3f getNormal() const;
	uint getColour() const;

	void setPosition( const Vec3f &posn );
	void setUV( const Vec2f &uv, uint textureWidth, uint textureHeight );
	void setUV( const Vec2s &uv );
	void setNormal( const Vec3f &normal );
	void setColour( uint colour );

	inline static ModelVertex create( Vec3f position, Vec2f uv, Vec3f normal, uint textureWidth, uint textureHeight ) {
		ModelVertex v;
		v.setPosition( position );
		v.setUV( uv, textureWidth, textureHeight );
		v.setNormal( normal );
		return v;
	}

	inline static ModelVertex create( Vec3f position, Vec2f uv, uint colour, uint textureWidth, uint textureHeight ) {
		ModelVertex v;
		v.setPosition( position );
		v.setUV( uv, textureWidth, textureHeight );
		v.setColour( colour );
		return v;
	}

	bool operator==( const ModelVertex &other ) const;

};

static_assert( sizeof( ModelVertex ) == 16 );

namespace std {
	template <> struct hash<ModelVertex> {
		size_t operator()(const ModelVertex & x) const {
			if constexpr( sizeof( size_t ) == 8 ) {
				const size_t &A = *((size_t*)x.data);
				const size_t &B = *((size_t*)&x.data[8]);
				return A ^ B;
			} else {
				const uint64 &A = *((uint64*)x.data);
				const uint64 &B = *((uint64*)&x.data[8]);
				return hash<uint64>()( A ^ B );
			}
		}
	};
}

typedef std::array<ModelVertex,3> ModelFace;

struct MaterialFaces {
	string materialName;
	std::optional<TextureInfo> textureInfo;
	bool useVertexColouring;
	std::vector<ModelFace> faces;

	MaterialFaces() : useVertexColouring(false) {}

	MaterialFaces( const MaterialFaces &other ) = delete;

	MaterialFaces( MaterialFaces &&other ) noexcept :
		materialName( std::move( other.materialName ) ),
		textureInfo( std::move( other.textureInfo ) ),
		useVertexColouring( other.useVertexColouring ),
		faces( std::move( other.faces ) )
	{}

};

struct ImportedModel {
	HashMap<std::string,MaterialFaces> materials;
	BoundingBox boundingBox;
	std::vector<std::string> warnings;
};


class IModelImporter {

	protected:
	IModelImporter() {}

	public:
	virtual const char *formatName() const = 0;
	virtual const char *fileExtension() const = 0;

	virtual ImportedModel import(
		const fs::path &filePath,
		double scale,
		const fs::path &textureOutputDir,
		const fs::path &modelOutputDir,
		const string &prefix = ""
	) const = 0;

	virtual ~IModelImporter() {}

	static const IModelImporter *tryGetImporter( const string &fileExtension );

	protected:
	enum class UVStatus : ubyte {
		Good = 0, // No fix needed
		Shifted = 1, // Shifted with no issues
		BreaksClamping = 2, // Shifted, might change the behaviour of clamped textures
		BreaksMirroring = 3, // Shifted, might change the behaviour of mirrored or clamped textures
		Misaligned = 4, // Shifted to fit, but had to misalign UVs
		RangeError = 5 // UVs span too large of a range to fix
	};

	struct UVResult {
		std::array<Vec2s,3> uvs;
		UVStatus status;
	};

	std::array<short,3> translateUVC(
		const std::array<float,3> &v,
		ushort texSize,
		IModelImporter::UVStatus &result
	) const;

	inline UVResult translateUVs(
		std::array<Vec2f,3> &uvs,
		ushort texWidth,
		ushort texHeight
	) const {
		UVStatus status = UVStatus::Good;
		std::array<short,3> up = translateUVC( { uvs[0].x, uvs[1].x, uvs[2].x }, texWidth, status );
		std::array<short,3> vp = translateUVC( { uvs[0].y, uvs[1].y, uvs[2].y }, texHeight, status );

		return UVResult{
			{
				Vec2s{ up[0], vp[0] },
				Vec2s{ up[1], vp[1] },
				Vec2s{ up[2], vp[2] },
			},
			status
		};
	}

	BoundingBox optimizeAndSave(
		HashMap<std::string,MaterialFaces> &materials,
		const fs::path &outputDir
	) const;

};


#endif /* SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_ */
