#ifndef SRC_CORE_IMPORT_TEXTURE_HPP_
#define SRC_CORE_IMPORT_TEXTURE_HPP_

#include <string>
#include "src/core/texture-info.hpp"

class TextureFile {

	private:
	void *m_image;
	TextureInfo m_textureInfo;

	public:
	TextureFile() : m_image( nullptr ) {}
	~TextureFile();

	TextureFile( const TextureFile &other ) = delete;
	TextureFile( TextureFile &&other );

	inline const void *image() const { return m_image; }
	inline const TextureInfo &info() const { return m_textureInfo; }

	bool saveAsPng( const char *destPath ) const;
	void removeAlpha();

	enum TextureLoadStatus {
		Good,
		Resized,
		Error
	};

	static TextureLoadStatus tryOpen( const char *filePath, TextureFile &out );
	static TextureInfo getImageInfo( const void *qImage );

};

#endif /* SRC_CORE_IMPORT_TEXTURE_HPP_ */
