#include "src/core/import/texture.hpp"

#include <QImage>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/fastmath.hpp"
#include "src/ui/image-filter.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/checksum.hpp"
#include "src/core/util.hpp"

TextureFile::TextureFile( TextureFile &&other ) {
	if( m_image != nullptr ) {
		delete (QImage*)m_image;
	}

	m_image = other.m_image;
	m_textureInfo = std::move( other.m_textureInfo );
	other.m_image = nullptr;
}

TextureFile::~TextureFile() {
	if( m_image != nullptr ) {
		delete (QImage*)m_image;
	}
}

bool TextureFile::saveAsPng( const char *destPath ) const {
	return ((QImage*)m_image)->save( destPath, "PNG" );
}

static TextureInfo getInfo( const QImage &pixmap ) {
	const uint *pixels = (const uint*)pixmap.constBits();

	TextureInfo info;
	info.width = pixmap.width();
	info.height = pixmap.height();
	info.isGreyscale = pixmap.allGray();
	info.sourceImageDepth = (ubyte)pixmap.depth();

	info.hasEmptyPixels = false;
	info.hasFullAlpha = false;
	info.hasVaryingAlpha = false;
	info.averageAlpha = 0;

	HashSet<uint> usedColours;
	uint totalAlpha = 0;
	uint nonZeroAlphaPixels = 0;
	ubyte firstAlpha = 0;
	for( int i = 0; i < pixmap.width() * pixmap.height(); i++ ) {
		usedColours.insert( pixels[i] );
		const ubyte alpha = pixels[i] >> 24;
		totalAlpha += alpha;
		if( alpha == 0 ) {
			info.hasEmptyPixels = true;
		} else {
			totalAlpha += alpha;
			info.hasFullAlpha |= (alpha != 0xFF);
			if( nonZeroAlphaPixels++ == 0 ) {
				firstAlpha = alpha;
			} else if( alpha != firstAlpha ){
				info.hasVaryingAlpha = true;
			}
		}
	}
	info.uniqueColours = (ushort)usedColours.size();

	if( nonZeroAlphaPixels > 0 ) {
		info.averageAlpha = totalAlpha / nonZeroAlphaPixels;
	}

#if QT_VERSION >= 0x050A00
	MemoryStream texStream( (char*)pixmap.bits(), pixmap.sizeInBytes() );
#else
	MemoryStream texStream( (char*)pixmap.bits(), pixmap.byteCount() );
#endif
	md5sum hash = getChecksumMD5( texStream );
	info.hash.reserve( 32 );
	for( ubyte b : hash ) {
		ubyte u = b >> 4;
		ubyte l = b & 0xF;
		info.hash += u > 9 ? ( (u - 10) + 'a' ) : ( u + '0' );
		info.hash += l > 9 ? ( (l - 10) + 'a' ) : ( l + '0' );
	}

	return info;
}

TextureInfo TextureFile::getImageInfo( const void *qImage ) {
	return getInfo( *((const QImage*)qImage) );
}

TextureFile::TextureLoadStatus TextureFile::tryOpen( const char *filePath, TextureFile &out ) {
	QImage *image = new QImage( filePath );
	if( image->isNull() ) {
		delete image;
		return TextureFile::Error;
	}

	bool resized = false;
	if(
		image->width() * image->height() > 8192 ||
		!isPowerOf2( image->width() ) ||
		!isPowerOf2( image->height() )
	) {
		resized = true;
		ImageFilter::makeValidTextureSize( *image );
	}

	if( out.m_image != nullptr ) {
		delete (QImage*)out.m_image;
	}

	out.m_image = image;
	out.m_textureInfo = getInfo( image->convertToFormat( QImage::Format_ARGB32 ) );
	return resized ? TextureFile::Resized : TextureFile::Good;
}

void TextureFile::removeAlpha() {
	if( m_image == nullptr ) return;
	QImage *image = (QImage*)m_image;
	if( image->format() != QImage::Format_ARGB32 ) {
		QImage reformattedImage = image->convertToFormat( QImage::Format_ARGB32 );
		delete (QImage*)m_image;
		m_image = new QImage( std::move( reformattedImage ) );
		image = (QImage*)m_image;
	}
	uint *pixelData = (uint*)image->bits();
#if QT_VERSION >= 0x050A00
	for( int i = 0; i < image->sizeInBytes() / 4; i++ ) {
#else
	for( int i = 0; i < image->byteCount() / 4; i++ ) {
#endif
		if( pixelData[i] >> 24 != 0 ) {
			pixelData[i] |= 0xFF000000u;
		}
	}

	m_textureInfo = getInfo( *image );
}

