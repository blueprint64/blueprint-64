#include "src/core/import/model-importer.hpp"

#include <type_traits>
#include <cstring>
#include <cmath>
#include <algorithm>
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/import/model-optimizer.hpp"

#include "src/core/import/wavefront.hpp"

static_assert( std::is_trivially_copyable_v<ModelVertex> );
static_assert( std::is_trivially_copyable_v<ModelFace> );
static_assert( sizeof( ModelVertex ) == 16 );
static_assert( sizeof( ModelVertex ) % alignof( ModelVertex ) == 0 );
static_assert( sizeof( ModelFace ) == 48 );
static_assert( sizeof( ModelFace ) % alignof( ModelFace ) == 0 );

inline static short getShort( const ubyte *data, int index ) {
	const ushort value = ntohs( *((ushort*)&data[index]) );
	return reinterpret_cast<const short &>( value );
}

inline static void setShort( ubyte *data, short value, int index ) {
	((ushort*)&data[index])[0] = htons( reinterpret_cast<const ushort &>( value ) );
}

Vec3f ModelVertex::getPosition() const {
	return Vec3f(
		(float)getShort( data, 0 ),
		(float)getShort( data, 2 ),
		(float)getShort( data, 4 )
	);
}

Vec2f ModelVertex::getUV( uint textureWidth, uint textureHeight ) const {
	return Vec2f(
		(float)((double)getShort( data, 6 ) / (double)(textureWidth << 5)),
		(float)((double)getShort( data, 8 ) / (double)(textureHeight << 5))
	);
}

Vec3f ModelVertex::getNormal() const {
	return Vec3f(
		(float)data[12] / (float)0xFF,
		(float)data[13] / (float)0xFF,
		(float)data[14] / (float)0xFF
	);
}

uint ModelVertex::getColour() const {
	return ntohl( *((uint*)&data[12]) );
}

void ModelVertex::setPosition( const Vec3f &posn ) {
	if(
		posn.x >= 32767.5 || posn.x <= -32769.5 ||
		posn.y >= 32767.5 || posn.y <= -32769.5 ||
		posn.z >= 32767.5 || posn.z <= -32769.5
	) {
		throw FileParserException( "Vertex lies out of range of 16-bit integer. Geometry must lie within the range [-32768,32767]." );
	}
	setShort( data, (short)(posn.x + 0.5), 0 );
	setShort( data, (short)(posn.y + 0.5), 2 );
	setShort( data, (short)(posn.z + 0.5), 4 );
	data[6] = data[7] = 0;
}

void ModelVertex::setUV( const Vec2f &uv, uint textureWidth, uint textureHeight ) {
	setShort( data, (short)((double)uv[0] * (double)(textureWidth << 5)), 8 );
	setShort( data, (short)((1.0 - (double)uv[1]) * (double)(textureHeight << 5)), 10 );
}

void ModelVertex::setUV( const Vec2s &uv ) {
	setShort( data, uv.x, 8 );
	setShort( data, uv.y, 10 );
}

void ModelVertex::setNormal( const Vec3f &normal ) {
	const Vec3f n = normal / normal.magnitude();
	const sbyte nx = (sbyte)(n.x * (float)0x7F);
	const sbyte ny = (sbyte)(n.y * (float)0x7F);
	const sbyte nz = (sbyte)(n.z * (float)0x7F);
	data[12] = static_cast<const ubyte&>( nx );
	data[13] = static_cast<const ubyte&>( ny );
	data[14] = static_cast<const ubyte&>( nz );
	data[15] = 0xFF;
}

void ModelVertex::setColour( uint colour ) {
	((uint*)&data[12])[0] = htonl( colour );
}

bool ModelVertex::operator==( const ModelVertex &other ) const {
	return std::memcmp( this, &other, sizeof( ModelVertex ) ) == 0;
}

static const HashMap<string,const IModelImporter*> &getImporters() {
	static HashMap<string,const IModelImporter*> s_importers = {
		{ WavefrontImporter::extension(), new WavefrontImporter() }
	};
	return s_importers;
}

const IModelImporter *IModelImporter::tryGetImporter( const string &fileExtension ) {
	const auto i = getImporters().find( fileExtension );
	return (i == getImporters().end()) ? nullptr : i->second;
}

BoundingBox IModelImporter::optimizeAndSave(
	HashMap<std::string,MaterialFaces> &materials,
	const fs::path &outputDir
) const {
	BoundingBox bounds = { 0, 0, 0, 0, 0, 0 };
	for( const auto &i : materials ) {
		if( i.second.faces.empty() ) {
			fs::forceDelete( outputDir / (i.first + ".verts") );
			fs::forceDelete( outputDir / (i.first + ".model") );
			continue;
		}

		FileWriteStream vertexFile( ( outputDir / (i.first + ".verts") ).u8string() );
		for( const ModelFace &face : i.second.faces ) {
			for( const ModelVertex &vertex : face ) {
				vertexFile.write( (const char*)&vertex, 6 );
			}
		}

		OptimizedModel model = optimizeModel( i.second );
		FileWriteStream modelFile( ( outputDir / (i.first + ".model") ).u8string() );
		model.modelLoader.save( modelFile );
		bounds.merge( model.boundingBox );
	}
	return bounds;
}

static inline short shortUV( double uvf, double scale ) {
	return (short)std::round( uvf * scale );
}

std::array<short,3> IModelImporter::translateUVC(
	const std::array<float,3> &v,
	ushort texSize,
	UVStatus &result
) const {
	const double scale = (double)texSize * 32.0;

	const double vMin = std::round( scale * (double)std::min({ v[0], v[1], v[2] }) );
	const double vMax = std::round( scale * (double)std::max({ v[0], v[1], v[2] }) );

	if( (vMin >= -32768.0 && vMax <= 32767.0) || scale == 0.0 ) {
		return {
			shortUV( v[0], scale ),
			shortUV( v[1], scale ),
			shortUV( v[2], scale )
		};
	}

	const double span = vMax - vMin;
	if( span > 65535.0 ) {
		result = UVStatus::RangeError;
		const double sf = 65535.0 / span;
		const double left = (double)std::min({ v[0], v[1], v[2] });
		const double shift = -32768.0 / scale;
		return {
			shortUV( ((v[0] - left) * sf) - shift, scale ),
			shortUV( ((v[1] - left) * sf) - shift, scale ),
			shortUV( ((v[2] - left) * sf) - shift, scale )
		};
	}

	double shiftLeft;
	if( span > 65535.0 - scale ) {
		result = std::max( result, UVStatus::Misaligned );
		shiftLeft = std::round( (vMin / 2.0) + (vMax / 2.0) ) / scale;
	} else if( span > 65535.0 - (scale * 2.0) ) {
		result = std::max( result, UVStatus::BreaksMirroring );
		shiftLeft = std::round( ((vMin / 2.0) + (vMax / 2.0)) / scale );
	} else if( span > 32767.0 - (scale * 2.0) || (vMax > 0.0 && vMin < scale) ) {
		result = std::max( result, UVStatus::BreaksClamping );
		shiftLeft = 2.0 * std::round( ((vMin / 4.0) + (vMax / 4.0)) / scale );
	} else if( vMax <= 0.0 ) {
		result = std::max( result, UVStatus::Shifted );
		shiftLeft = std::trunc( vMax / scale );
	} else {
		result = std::max( result, UVStatus::Shifted );
		shiftLeft = std::trunc( vMin / scale );
	}

	return {
		shortUV( v[0] - shiftLeft, scale ),
		shortUV( v[1] - shiftLeft, scale ),
		shortUV( v[2] - shiftLeft, scale )
	};
}
