#include "src/core/uuid.hpp"

#include <cstring>
#include <regex>
#include <cassert>

#include "src/core/exceptions.hpp"
#include "src/polyfill/random.hpp"

using namespace std;

constexpr size_t UUID_SIZE = 16;

static const regex UuuidRegex(
	"([0-9a-fA-f]{8})-([0-9a-fA-f]{4})-([0-9a-fA-f]{4})-([0-9a-fA-f]{4})-([0-9a-fA-f]{12})",
	regex_constants::ECMAScript | regex_constants::optimize
);

Uuid::Uuid() {
	std::memset( m_data, 0, UUID_SIZE );
}

Uuid::Uuid( const Uuid &src ) {
	memcpy( m_data, src.m_data, UUID_SIZE );
}

Uuid &Uuid::operator=( const Uuid &other ) {
	memcpy( m_data, other.m_data, UUID_SIZE );
	return *this;
}

static void writeUuidPart( char *str, const ubyte *data, int numBytes ) {
	for( int i = 0; i < numBytes; i++ ) {
		const ubyte upper = data[i] >> 4;
		const ubyte lower = data[i] & 0xF;
		str[i*2] = upper > 9 ? ('a' + ( upper - 10 ) ) : ( upper + '0' );
		str[i*2+1] = lower > 9 ? ('a' + ( lower - 10 ) ) : ( lower + '0' );
	}
}

string Uuid::toString() const {
	char uuid[37] = "00000000-0000-0000-0000-000000000000";
	writeUuidPart( uuid, m_data, 4 );
	writeUuidPart( &uuid[9], &m_data[4], 2 );
	writeUuidPart( &uuid[14], &m_data[6], 2 );
	writeUuidPart( &uuid[19], &m_data[8], 2 );
	writeUuidPart( &uuid[24], &m_data[10], 6 );
	return string( uuid );
}

string Uuid::toHex() const {
	char uuid[33];
	writeUuidPart( uuid, m_data, 16 );
	uuid[32] = '\0';
	return string( uuid );
}

void Uuid::writeToRom( ostream &rom ) const {
	rom.write( (char*)m_data, UUID_SIZE );
}

Uuid Uuid::readFromRom( istream &rom ) {
	Uuid result;
	rom.read( (char*)result.m_data, UUID_SIZE );
	return result;
}

Uuid Uuid::parse( const string &str ) {
	smatch matches;
	if( !regex_search( str, matches, UuuidRegex ) || matches.size() != 6 ) {
		throw UuidParseException( str );
	}

	const string fullUuid = matches[1].str() + matches[2].str() + matches[3].str() + matches[4].str() + matches[5].str();
	assert( fullUuid.length() == 32 );

	Uuid result;
	for( int i = 0; i < 16; i++ ) {
		const string byteStr = fullUuid.substr( i << 1, 2 );
		result.m_data[i] = stoi( byteStr, nullptr, 16 );
	}

	return result;
}

int Uuid::compare( const Uuid &lhs, const Uuid &rhs ) {
	return memcmp( lhs.m_data, rhs.m_data, UUID_SIZE );
}

Uuid Uuid::generate() {
	Uuid id;
	Pseudorandom::getBytes( (ubyte*)id.m_data, UUID_SIZE );
	id.m_data[6] = (id.m_data[6] & 0x0F) | 0x40;
	id.m_data[8] = (id.m_data[8] & 0x3F) | 0x80;
	return id;
}

template<> void JsonSerializer::serialize<Uuid>( JsonWriter &jw, const Uuid &obj ) {
	jw.writeString( obj.toString() );
}

template<> Uuid JsonSerializer::parse<Uuid>( const Json &json ) {
	return Uuid::parse( json.get<string>() );
}
