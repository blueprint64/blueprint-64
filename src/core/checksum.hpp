#ifndef SRC_CORE_CHECKSUM_HPP_
#define SRC_CORE_CHECKSUM_HPP_

#include <array>
#include <iostream>
#include "src/types.hpp"

typedef std::array<ubyte,16> md5sum;

md5sum getChecksumMD5( std::istream &file );
uint64 computeCRC( std::istream &rom );
void updateCRC( std::iostream &rom );

#endif /* SRC_CORE_CHECKSUM_HPP_ */
