#ifndef SRC_CORE_UTIL_HPP_
#define SRC_CORE_UTIL_HPP_

#include <string>
#include <array>
#include <map>
#include <unordered_map>
#include <cstring>
#include <type_traits>
#include <iostream>
#include <initializer_list>
#include "src/core/enums.hpp"
#include "src/types.hpp"

//TODO: move into Util namespace
extern std::string quoteAndEscape( const std::string &path );
extern CourseId levelIdToCourseId( LevelId levelId );
extern LevelId courseIdToLevelId( CourseId courseId );
extern bool isWhitespaceOrEmpty( const std::string &str );
extern bool stringEndsWith( const std::string &str, const std::string &suffix );
extern std::string trimStart( const std::string &str );
extern std::string trimEnd( const std::string &str );
extern std::string toAlphanumeric( const std::string &str );

namespace BinaryWriter {

	/* Write without adapting byte order */
	template<typename T> inline void writeRaw( std::ostream &rom, std::initializer_list<T> data ) {
		rom.write( (const char*)data.begin(), data.size() * sizeof( T ) );
	}

	inline void writeWordRaw( std::ostream &rom, uint wordBE ) {
		rom.write( (const char*)&wordBE, 4 );
	}

	inline void writeHalfwordRaw( std::ostream &rom, ushort hwBE ) {
		rom.write( (const char*)&hwBE, 2 );
	}

	/* Convert to Big Endian and write */
	inline void writeWord( std::ostream &rom, uint word ) {
		writeWordRaw( rom, htonl( word ) );
	}

	inline void writeSignedWord( std::ostream &rom, int word ) {
		writeWord( rom, reinterpret_cast<const uint &>( word ) );
	}

	inline void writeWords( std::ostream &rom, std::initializer_list<uint> words ) {
		# if __BYTE_ORDER == __LITTLE_ENDIAN
			for( uint w : words ) writeWord( rom, w );
		#else
			writeRaw<uint>( rom, words );
		#endif
	}

	inline void writeSignedWords( std::ostream &rom, std::initializer_list<int> words ) {
		# if __BYTE_ORDER == __LITTLE_ENDIAN
			for( int w : words ) writeSignedWord( rom, w );
		#else
			rom.write( (const char*)words.begin(), words.size() << 2 );
		#endif
	}

	inline void writeHalfword( std::ostream &rom, ushort hw ) {
		writeHalfwordRaw( rom, htons( hw ) );
	}

	inline void writeSignedHalfword( std::ostream &rom, short hw ) {
		writeHalfword( rom, reinterpret_cast<const ushort &>( hw ) );
	}

	inline void writeHalfwords( std::ostream &rom, std::initializer_list<ushort> hws ) {
		# if __BYTE_ORDER == __LITTLE_ENDIAN
			for( ushort hw : hws ) writeHalfword( rom, hw );
		#else
			writeRaw<ushort>( rom, hws );
		#endif
	}

	inline void writeSignedHalfwords( std::ostream &rom, std::initializer_list<short> hws ) {
		# if __BYTE_ORDER == __LITTLE_ENDIAN
			for( short hw : hws ) writeSignedHalfword( rom, hw );
		#else
			rom.write( (const char*)hws.begin(), hws.size() << 1 );
		#endif
	}

	/* Write to another location */
	inline void writeWordAt( std::ostream &rom, uint romAddress, uint value ) {
		const std::ostream::pos_type head = rom.tellp();
		rom.seekp( romAddress );
		writeWord( rom, value );
		rom.seekp( head );
	}

	inline void writeHalfwordAt( std::ostream &rom, uint romAddress, ushort value ) {
		const std::ostream::pos_type head = rom.tellp();
		rom.seekp( romAddress );
		writeHalfword( rom, value );
		rom.seekp( head );
	}

}

namespace Util {
	std::string toHexWord( uint word, bool includePrefix = false );

	inline uint parseHexWord( const string &str ) {
		return (uint)std::stoul( str, nullptr, 16 );
	}

	void makeLowerCase( string &str );
	void makeUpperCase( string &str );

	template<typename K, typename V> inline const V *tryGetValue( const std::map<K,V> &map, const K &key ) {
		const auto i = map.find( key );
		return (i != map.end()) ? &i->second : nullptr;
	}

	template<typename K, typename V> inline V *tryGetValue( std::map<K,V> &map, const K &key ) {
		auto i = map.find( key );
		return (i != map.end()) ? &i->second : nullptr;
	}

	template<typename K, typename V> inline const V *tryGetValue( const HashMap<K,V> &map, const K &key ) {
		const auto i = map.find( key );
		return (i != map.end()) ? &i->second : nullptr;
	}

	template<typename K, typename V> inline V *tryGetValue( HashMap<K,V> &map, const K &key ) {
		auto i = map.find( key );
		return (i != map.end()) ? &i->second : nullptr;
	}

}

inline std::string trim( const std::string &str ) { return trimStart( trimEnd( str ) ); }

inline short angleToShort( float angle ) {
	const ushort sAngle = ((uint)( (double)angle * (double)0x10000 / 360.0 ) % 0x10000u);
	const short result = reinterpret_cast<const short &>( sAngle );
	return result;
}

inline float angleToFloat( short angle ) {
	return (float)angle * (360.0f / (float)0x10000);
}

template<typename T, size_t N> void arraycpy( std::array<T,N> &cppArray, const T *cArray ) {
	if constexpr( std::is_trivially_copyable_v<T> ) {
		std::memcpy( cppArray.data(), cArray, sizeof(T)*N );
	} else {
		for( size_t i = 0; i < N; i++ ) {
			cppArray[i] = cArray[i];
		}
	}
}

class MemoryStream : public std::istream {

	public:
	MemoryStream( char *data, size_t size );
	~MemoryStream() { delete rdbuf(); }

};

struct BoundingBox {
	short min_x, max_x;
	short min_y, max_y;
	short min_z, max_z;

	inline bool isEmpty() const {
		return
			min_x == 0 && max_x == 0 &&
			min_y == 0 && max_y == 0 &&
			min_z == 0 && max_z == 0;
	}

	void merge( const BoundingBox &other ) {
		min_x = other.min_x < min_x ? other.min_x : min_x;
		max_x = other.max_x > max_x ? other.max_x : max_x;
		min_y = other.min_y < min_y ? other.min_y : min_y;
		max_y = other.max_y > max_y ? other.max_y : max_y;
		min_z = other.min_z < min_z ? other.min_z : min_z;
		max_z = other.max_z > max_z ? other.max_z : max_z;
	}
};

#endif /* SRC_CORE_UTIL_HPP_ */
