#include "src/core/texture-info.hpp"

DrawingLayer TextureInfo::suggestLayer() const {
	return hasFullAlpha ? DrawingLayer::Translucent : ( hasEmptyPixels ? DrawingLayer::Alphatest : DrawingLayer::Opaque );
}

static inline TextureFormat suggestFormat4( const TextureInfo &t ) {
	return (t.hasEmptyPixels || t.hasFullAlpha) ? TextureFormat::IA4 : TextureFormat::I4;
}

static TextureFormat suggestFormat8( const TextureInfo &t ) {
	if( t.hasFullAlpha ) {
		return TextureFormat::IA8;
	} else if( t.hasEmptyPixels ) {
		return t.sourceImageDepth > 4 ? TextureFormat::IA8 : TextureFormat::IA4;
	} else {
		return t.sourceImageDepth > 4 ? TextureFormat::I8 : TextureFormat::I4;
	}
}

static TextureFormat suggestFormat32( const TextureInfo &t ) {
	if( t.isGreyscale ) {
		TextureFormat format8 = suggestFormat8( t );
		if( t.sourceImageDepth > 8 && format8 == TextureFormat::IA8 ) {
			return TextureFormat::IA16;
		}
		return format8;
	}
	return t.hasFullAlpha ? TextureFormat::RGBA32 : TextureFormat::RGBA16;
}

static inline TextureFormat suggestFormat16( const TextureInfo &t ) {
	TextureFormat preferredFormat = suggestFormat32( t );
	return (preferredFormat == TextureFormat::RGBA32) ? TextureFormat::RGBA16 : preferredFormat;
}

TextureFormat TextureInfo::suggestFormat() const {
	if( width == 0 && height == 0 ) {
		return TextureFormat::IA4;
	}

	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	if( maxBitsPerPixel < 8 && width >= 16 ) {
		return suggestFormat4( *this );
	} else if( maxBitsPerPixel < 16 && width >= 8 ) {
		return suggestFormat8( *this );
	} else if( maxBitsPerPixel < 32 && width >= 4 ) {
		return suggestFormat16( *this );
	} else {
		return suggestFormat32( *this );
	}
}

bool TextureInfo::supportsFormat( TextureFormat format ) const {
	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	switch( format ) {
		case TextureFormat::RGBA32:
			return maxBitsPerPixel >= 32 && width >= 2;
		case TextureFormat::RGBA16:
		case TextureFormat::IA16:
			return maxBitsPerPixel >= 16 && width >= 4;
		case TextureFormat::IA8:
		case TextureFormat::I8:
			return maxBitsPerPixel >= 8 && width >= 8;
		case TextureFormat::IA4:
		case TextureFormat::I4:
			return maxBitsPerPixel >= 4 && width >=16;
		default:
			return false;
	}

}

HashSet<TextureFormat> TextureInfo::getPossibleFormats() const {
	//TODO: support indexed colours and I guess YUV16 maybe
	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	HashSet<TextureFormat> possibleFormats;
	if( maxBitsPerPixel >= 4 ) {
		if( width >= 16 ) {
			possibleFormats.insert( TextureFormat::I4 );
			possibleFormats.insert( TextureFormat::IA4 );
		}
		if( maxBitsPerPixel >= 8 ) {
			if( width >= 8 ) {
				possibleFormats.insert( TextureFormat::I8 );
				possibleFormats.insert( TextureFormat::IA8 );
			}
			if( maxBitsPerPixel >= 16 ) {
				if( width >= 4 ) {
					possibleFormats.insert( TextureFormat::IA16 );
					possibleFormats.insert( TextureFormat::RGBA16 );
				}
				if( maxBitsPerPixel >= 32 && width >= 2 ) {
					possibleFormats.insert( TextureFormat::RGBA32 );
				}
			}
		}
	}
	return possibleFormats;
}

static constexpr char P_HASH[] = "hash";
static constexpr char P_WIDTH[] = "width";
static constexpr char P_HEIGHT[] = "height";
static constexpr char P_HAS_EMPTY[] = "has_empty_pixels";
static constexpr char P_HAS_ALPHA[] = "has_full_alpha";
static constexpr char P_VAR_ALPHA[] = "has_varying_alpha";
static constexpr char P_GREYSCALE[] = "is_greyscale";
static constexpr char P_COLOUR_DEPTH[] = "colour_depth";
static constexpr char P_AVG_ALPHA[] = "average_alpha";
static constexpr char P_NUM_COLOURS[] = "unique_colours";

template<> void JsonSerializer::serialize<TextureInfo>( JsonWriter &jw, const TextureInfo &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_HASH, obj.hash );
	jw.writeProperty( P_WIDTH, obj.width );
	jw.writeProperty( P_HEIGHT, obj.height );
	jw.writeProperty( P_HAS_EMPTY, obj.hasEmptyPixels );
	jw.writeProperty( P_HAS_ALPHA, obj.hasFullAlpha );
	jw.writeProperty( P_VAR_ALPHA, obj.hasVaryingAlpha );
	jw.writeProperty( P_GREYSCALE, obj.isGreyscale );
	jw.writeProperty( P_COLOUR_DEPTH, obj.sourceImageDepth );
	jw.writeProperty( P_AVG_ALPHA, obj.averageAlpha );
	jw.writeProperty( P_NUM_COLOURS, obj.uniqueColours );
	jw.writeObjectEnd();
}

template<> TextureInfo JsonSerializer::parse<TextureInfo>( const Json &json ) {
	return TextureInfo {
		json[P_HASH].get<string>(),
		json[P_WIDTH].get<ushort>(),
		json[P_HEIGHT].get<ushort>(),
		json[P_HAS_EMPTY].get<bool>(),
		json[P_HAS_ALPHA].get<bool>(),
		json[P_VAR_ALPHA].get<bool>(),
		json[P_GREYSCALE].get<bool>(),
		json[P_COLOUR_DEPTH].get<ubyte>(),
		json[P_AVG_ALPHA].get<ubyte>(),
		json[P_NUM_COLOURS].get<ushort>()
	};
}
