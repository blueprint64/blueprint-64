#ifndef SRC_CORE_LOCATIONS_HPP_
#define SRC_CORE_LOCATIONS_HPP_

#include "src/core/enums.hpp"

namespace RomLocations {

	struct LevelLocation {
		uint scriptLocation;
		uint dataLocation;
		uint scriptLoaderLocation;
		uint vanillaScriptStart;
	};

	extern const LevelLocation &getLevelLocation( LevelId levelId );

	extern const uint importedLevelsTable;

}



#endif /* SRC_CORE_LOCATIONS_HPP_ */
