#include "src/core/vector.hpp"

#include <cmath>
#include "src/polyfill/byte-order.hpp"
#include "src/types.hpp"

static inline constexpr double sqr( float x ) {
	return (double)x * (double)x;
}

float Vec3f::magnitude() const {
	return (float)std::sqrt( sqr( x ) + sqr( y ) + sqr( z ) );
}

float Vec3f::dot( const Vec3f &other ) const {
	return ( x * other.x ) + ( y * other.y ) + ( z * other.z );
}

Vec3f Vec3f::cross( const Vec3f &other ) const {
	return Vec3f(
		( y * other.z ) - ( z * other.y ),
		( z * other.x ) - ( x * other.z ),
		( x * other.y ) - ( y * other.x )
	);
}

Vec3f Vec3f::operator+( const Vec3f &other ) const {
	return Vec3f( x + other.x, y + other.y, z + other.z );
}

Vec3f Vec3f::operator-( const Vec3f &other ) const {
	return Vec3f( x - other.x, y - other.y, z - other.z );
}

Vec3f Vec3f::operator*( float k ) const {
	return Vec3f( x * k, y * k, z * k );
}

Vec3f Vec3f::operator/( float k ) const {
	return Vec3f( x / k, y / k, z / k);
}

Vec3f &Vec3f::operator+=( const Vec3f &other ) {
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

Vec3f &Vec3f::operator-=( const Vec3f &other ) {
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

Vec3f &Vec3f::operator*=( float k ) {
	x *= k;
	y *= k;
	z *= k;
	return *this;
}

Vec3f &Vec3f::operator/=( float k ) {
	x /= k;
	y /= k;
	z /= k;
	return *this;
}

bool Vec3f::operator==( const Vec3f &other ) const {
	return x == other.x && y == other.y && z == other.z;
}

bool Vec3f::operator!=( const Vec3f &other ) const {
	return x != other.x || y != other.y || z != other.z;
}

bool Vec3f::zero() const {
	return x == 0 && y == 0 && z == 0;
}

void Vec3f::writeToRom( std::ostream &rom ) const {
	const uint data[] = {
		htonl( reinterpret_cast<const uint &>( x ) ),
		htonl( reinterpret_cast<const uint &>( y ) ),
		htonl( reinterpret_cast<const uint &>( z ) )
	};
	rom.write( (const char*)data, 12 );
}

void Vec3f::readFromRom( std::istream &rom ) {
	uint data[3];
	rom.read( (char*)data, 12 );
	data[0] = ntohl( data[0] );
	data[1] = ntohl( data[1] );
	data[2] = ntohl( data[2] );
	x = reinterpret_cast<const float &>( data[0] );
	y = reinterpret_cast<const float &>( data[1] );
	z = reinterpret_cast<const float &>( data[2] );
}

float Vec2f::magnitude() const {
	return (float)std::sqrt( sqr( x ) + sqr( y ) );
}

float Vec2f::dot( const Vec2f &other ) const {
	return ( x * other.x ) + ( y * other.y );
}

Vec2f Vec2f::operator+( const Vec2f &other ) const {
	return Vec2f( x + other.x, y + other.y );
}

Vec2f Vec2f::operator-( const Vec2f &other ) const {
	return Vec2f( x - other.x, y - other.y );
}

Vec2f Vec2f::operator*( float k ) const {
	return Vec2f( x * k, y * k );
}

Vec2f Vec2f::operator/( float k ) const {
	return Vec2f( x / k, y / k );
}

Vec2f &Vec2f::operator+=( const Vec2f &other ) {
	x += other.x;
	y += other.y;
	return *this;
}

Vec2f &Vec2f::operator-=( const Vec2f &other ) {
	x -= other.x;
	y -= other.y;
	return *this;
}

Vec2f &Vec2f::operator*=( float k ) {
	x *= k;
	y *= k;
	return *this;
}

Vec2f &Vec2f::operator/=( float k ) {
	x /= k;
	y /= k;
	return *this;
}

bool Vec2f::operator==( const Vec2f &other ) const {
	return x == other.x && y == other.y;
}

bool Vec2f::operator!=( const Vec2f &other ) const {
	return x != other.x || y != other.y;
}

bool Vec2f::zero() const {
	return x == 0 && y == 0;
}

void Vec3s::writeToRom( std::ostream &rom ) const {
	const ushort data[] = {
		htons( reinterpret_cast<const ushort &>( x ) ),
		htons( reinterpret_cast<const ushort &>( y ) ),
		htons( reinterpret_cast<const ushort &>( z ) )
	};
	rom.write( (const char*)data, 6 );
}

void Vec3s::readFromRom( std::istream &rom ) {
	ushort data[3];
	rom.read( (char*)data, 6 );
	data[0] = ntohs( data[0] );
	data[1] = ntohs( data[1] );
	data[2] = ntohs( data[2] );
	x = reinterpret_cast<const short &>( data[0] );
	y = reinterpret_cast<const short &>( data[1] );
	z = reinterpret_cast<const short &>( data[2] );
}

static constexpr char P_X[] = "x";
static constexpr char P_Y[] = "y";
static constexpr char P_Z[] = "z";

template<> void JsonSerializer::serialize<Vec3f>( JsonWriter &jw, const Vec3f &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_X, obj.x );
	jw.writeProperty( P_Y, obj.y );
	jw.writeProperty( P_Z, obj.z );
	jw.writeObjectEnd();
}

template<> Vec3f JsonSerializer::parse<Vec3f>( const Json &json ) {
	return Vec3f{
		json[P_X].get<float>(),
		json[P_Y].get<float>(),
		json[P_Z].get<float>()
	};
}

template<> void JsonSerializer::serialize<Vec3s>( JsonWriter &jw, const Vec3s &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_X, obj.x );
	jw.writeProperty( P_Y, obj.y );
	jw.writeProperty( P_Z, obj.z );
	jw.writeObjectEnd();
}

template<> Vec3s JsonSerializer::parse<Vec3s>( const Json &json ) {
	return Vec3s{
		json[P_X].get<short>(),
		json[P_Y].get<short>(),
		json[P_Z].get<short>()
	};
}
