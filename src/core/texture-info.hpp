#ifndef SRC_CORE_TEXTURE_INFO_HPP_
#define SRC_CORE_TEXTURE_INFO_HPP_

#include <unordered_set>
#include "src/core/enums.hpp"
#include "src/core/json.hpp"

struct TextureInfo {
	string hash;

	ushort width;
	ushort height;

	bool hasEmptyPixels;
	bool hasFullAlpha;
	bool hasVaryingAlpha;
	bool isGreyscale;
	ubyte sourceImageDepth;

	ubyte averageAlpha;
	ushort uniqueColours;

	DrawingLayer suggestLayer() const;
	TextureFormat suggestFormat() const;
	HashSet<TextureFormat> getPossibleFormats() const;
	bool supportsFormat( TextureFormat format ) const;
};

namespace JsonSerializer {
	template<> void serialize<TextureInfo>( JsonWriter &jw, const TextureInfo &obj );
	template<> TextureInfo parse<TextureInfo>( const Json &json );
}

#endif /* SRC_CORE_TEXTURE_INFO_HPP_ */
