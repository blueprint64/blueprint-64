static const float s_vanillaStarSpawns[] = {
	2000.0f,	4500.0f,	-4500.0f,	// King Bob-Omb
	180.0f,		3880.0f,	340.0f,		// King Whomp
	0.0f,		-900.0f,	-3700.0f,	// Eyerok
	0.0f,		2048.0f,	0.0f,		// Wiggler
	1370,		2000.0f,	-320.0f,	// Big Mr. I
	980.0f,		1100.0f,	250.0f,		// King Boo (B. Param 2 = 0)
	-1600.0f,	-2100.0f,	205.0f,		// King Boo (B. Param 2 = 1)
	700.0f,		3200.0f,	1900.0f,	// King Boo (B. Param 2 = 2)
	0.0f,		950.0f,		-6800.0f,	// Big Bully 1
	3700.0f,	600.0f,		-5500.0f,	// Big Bully 2
	130.0f,		1600.0f,	-4335.0f,	// Big Chilly Bully
	3030.f,		4500.f,		-4600.f,	// Koopa the Quick (B. Param 2 = 2)
	7100.f,		-1300.f,	-6000.f,	// Koopa the Quick (B. Param 2 = 3)
	-7339.0f,	-5700.0f,	-6774.0f,	// Penguin Race
	-6358.0f,	-4300.0f,	4700.0f,	// Secret Slide Time Attack
	-4700.0f,	-1024.0f,	1890.0f,	// CCM Snowman's Head
	-6300.0f,	-1850.0f,	-6300.0f,	// Piranha Plants
	3500.0f,	-4300.0f,	4650.0f,	// Tuxie's Mom
	-5550.0f,	300.0f,		-930.0f,	// Klepto
	-1800.0f,	-2500.0f,	-1700.0f,	// Tresure Chests (JRB Cave)
	-1900.0f,	-4000.0f,	-1400.0f,	// Tresure Chests (DDD)
	2500.0f,	-1200.0f,	1300.0f,	// Ukiki's Star Cage
	6833.0f,	-3654.0f,	2230.0f,	// Unagi the Eel
	-3180.0f,	-3600.0f,	120.0f,		// Manta Ray
	3400.0f,	-3200.0f,	-500.0f		// Water Rings
};
static_assert( sizeof( s_vanillaStarSpawns ) == 75 * sizeof( float ) );

static const uint s_starShimLocations[] = {
	0x62AEC,	// King Bob-Omb
	0x82910,    // King Whomp
	0xC9A2C,	// Eyerok
	0xBCFE4,	// Wiggler
	0x61454,	// Big Mr. I
	0x7FBB4,	// King Boo (B. Param 2 = 0)
	0x7FC28,	// King Boo (B. Param 2 = 1)
	0x7FBF0,	// King Boo (B. Param 2 = 2)
	0xA6974,	// Big Bully 1
	0xA6CC0,	// Big Bully 2
	0xA6954,	// Big Chilly Bully
	0,			// Koopa the Quick (handled separately)
	0,			// Koopa the Quick (handled separately)
	0xCD044,	// Penguin Race
	0xB7D4,		// Secret Slide Time Attack
	0xAC4CC,	// CCM Snowman's Head
	0xC8030,	// Piranha Plants
	0x7A12C,	// Tuxie's Mom
	0xCC480,	// Klepto
	0xB32B4,	// Tresure Chests (JRB Cave)
	0xB344C,	// Tresure Chests (DDD)
	0x69A2C,	// Ukiki's Star Cage
	0xC5C88,	// Unagi the Eel
	0xB22D8,	// Manta Ray
	0xA7974		// Water Rings
};
static_assert( sizeof( s_starShimLocations ) == 25 * sizeof( uint ) );

static inline void shimStarSpawns( std::ostream &rom, uint functionAddr ) {
	rom.seekp( 0x3C9F0AC );
	for( uint i = 0; i < 75; i++ ) {
		const uint f = htonl( *((uint*)&s_vanillaStarSpawns[i]) );
		rom.write( (const char*)&f, 4 );
	}
	if( !rom.good() ) {
		throw RomError( "Error writing to ROM file. Do you have write permission on the file?" );
	}
	assert( rom.tellp() == 0x3C9F1D8 );

	const uint jalCmd = htonl( 0x0C000000 | ((functionAddr & 0x0FFFFFFFu) >> 2) );
	for( uint i = 0; i < 25; i++ ) {
		if( s_starShimLocations[i] == 0 ) continue;
		rom.seekp( s_starShimLocations[i] );
		const uint oriCmd = htonl( 0x34040000 | i );
		rom.write( (const char*)&jalCmd, 4 );
		rom.write( (const char*)&oriCmd, 4 );
	}

	rom.seekp( 0xB867C );
	rom.write( (const char*)&jalCmd, 4 );
	const ubyte addiuCmd[] = { 0x25, 0xC4, 0x00, 0x0B }; // ADDIU A0, T6, 0xB
	rom.write( (const char*)addiuCmd, 4 );
}
