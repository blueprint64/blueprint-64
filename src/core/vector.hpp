#ifndef SRC_CORE_VECTOR_HPP_
#define SRC_CORE_VECTOR_HPP_

#include <cstddef>
#include <type_traits>
#include <iostream>
#include "src/core/json.hpp"

struct Angle3s {
	short pitch;
	short yaw;
	short roll;
};

struct Vec3f final {
	float x, y, z;

	Vec3f() : x( 0 ), y( 0 ), z( 0 ) {}
	Vec3f( float _x, float _y, float _z ) : x( _x ), y( _y ), z( _z ) {}

	inline float &operator[]( int i ) {
		return ((float*)this)[i];
	}

	inline const float &operator[]( int i ) const {
		return ((const float*)this)[i];
	}

	float magnitude() const;
	float dot( const Vec3f &other ) const;
	Vec3f cross( const Vec3f &other ) const;
	bool zero() const;

	Vec3f operator+( const Vec3f &other ) const;
	Vec3f operator-( const Vec3f &other ) const;
	Vec3f operator*( float k ) const;
	Vec3f operator/( float k ) const;

	Vec3f &operator+=( const Vec3f &other );
	Vec3f &operator-=( const Vec3f &other );
	Vec3f &operator*=( float k );
	Vec3f &operator/=( float k );

	bool operator==( const Vec3f &other ) const;
	bool operator!=( const Vec3f &other ) const;

	void writeToRom( std::ostream &rom ) const;
	void readFromRom( std::istream &rom );
};

static_assert( std::is_standard_layout_v<Vec3f> );
static_assert( std::is_trivially_copyable_v<Vec3f> );
static_assert( offsetof( Vec3f, x ) == 0 );
static_assert( offsetof( Vec3f, y ) == 4 );
static_assert( offsetof( Vec3f, z ) == 8 );
static_assert( sizeof( Vec3f ) == 3 * sizeof( float ) );

struct Vec2f final {
	float x, y;

	Vec2f() : x( 0 ), y( 0 ) {}
	Vec2f( float _x, float _y ) : x( _x ), y( _y ) {}

	inline float &operator[]( int i ) {
		return ((float*)this)[i];
	}

	inline const float &operator[]( int i ) const {
		return ((const float*)this)[i];
	}

	float magnitude() const;
	float dot( const Vec2f &other ) const;
	bool zero() const;

	Vec2f operator+( const Vec2f &other ) const;
	Vec2f operator-( const Vec2f &other ) const;
	Vec2f operator*( float k ) const;
	Vec2f operator/( float k ) const;

	Vec2f &operator+=( const Vec2f &other );
	Vec2f &operator-=( const Vec2f &other );
	Vec2f &operator*=( float k );
	Vec2f &operator/=( float k );

	bool operator==( const Vec2f &other ) const;
	bool operator!=( const Vec2f &other ) const;
};

static_assert( std::is_standard_layout_v<Vec2f> );
static_assert( std::is_trivially_copyable_v<Vec2f> );
static_assert( offsetof( Vec2f, x ) == 0 );
static_assert( offsetof( Vec2f, y ) == 4 );
static_assert( sizeof( Vec2f ) == 2 * sizeof( float ) );

struct Vec3s final {
	short x, y, z;

	Vec3s() : x( 0 ), y( 0 ), z( 0 ) {}
	Vec3s( short _x, short _y, short _z ) : x( _x ), y( _y ), z( _z ) {}

	inline short &operator[]( int i ) {
		return ((short*)this)[i];
	}

	inline const short &operator[]( int i ) const {
		return ((const short*)this)[i];
	}

	inline bool zero() const {
		return x == 0 && y == 0 && z == 0;
	}

	void writeToRom( std::ostream &rom ) const;
	void readFromRom( std::istream &rom );
};

static_assert( std::is_standard_layout_v<Vec3s> );
static_assert( std::is_trivially_copyable_v<Vec3s> );
static_assert( offsetof( Vec3s, x ) == 0 );
static_assert( offsetof( Vec3s, y ) == 2 );
static_assert( offsetof( Vec3s, z ) == 4 );

struct Vec2s final {
	short x, y;

	Vec2s() : x( 0 ), y( 0 ) {}
	Vec2s( short _x, short _y ) : x( _x ), y( _y ) {}

	inline short &operator[]( int i ) {
		return ((short*)this)[i];
	}

	inline const short &operator[]( int i ) const {
		return ((const short*)this)[i];
	}
};

static_assert( std::is_standard_layout_v<Vec2f> );
static_assert( std::is_trivially_copyable_v<Vec2f> );
static_assert( offsetof( Vec2s, x ) == 0 );
static_assert( offsetof( Vec2s, y ) == 2 );
static_assert( sizeof( Vec2s ) == 2 * sizeof( short ) );

namespace JsonSerializer {
	template<> void serialize<Vec3f>( JsonWriter &jw, const Vec3f &obj );
	template<> Vec3f parse<Vec3f>( const Json &json );

	template<> void serialize<Vec3s>( JsonWriter &jw, const Vec3s &obj );
	template<> Vec3s parse<Vec3s>( const Json &json );
}

#endif /* SRC_CORE_VECTOR_HPP_ */
