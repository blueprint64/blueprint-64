#include "src/core/unicode.hpp"

#include <codecvt>
#include <locale>
#include "src/types.hpp"
#ifdef _WIN32
#include "src/polyfill/window-slit.hpp"
#endif

wstring Unicode::toUTF16( const char *u8str ) {
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
	return converter.from_bytes( u8str );
}

string Unicode::toUTF8( const wchar_t *u16str ) {
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
	return converter.to_bytes( u16str );
}

#ifdef _WIN32
string Unicode::fixWindowsEncoding( const string &str ) {
	wstring wideStr;
	if( Windows::tryWidenString( str.c_str(), wideStr ) ) {
		return Unicode::toUTF8( wideStr );
	}
	return str;
}
#endif
