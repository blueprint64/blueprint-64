#ifndef SRC_CORE_EXTENDED_BOUNDS_HPP_
#define SRC_CORE_EXTENDED_BOUNDS_HPP_

#include <ostream>
#include "src/core/enums.hpp"

extern void setLevelBoundaries( std::ostream &rom, LevelBoundarySize boundarySize );


#endif /* SRC_CORE_EXTENDED_BOUNDS_HPP_ */
