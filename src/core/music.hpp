#ifndef SRC_CORE_MUSIC_HPP_
#define SRC_CORE_MUSIC_HPP_

#include <vector>
#include <ostream>
#include <optional>
#include "src/types.hpp"
#include "src/core/enums.hpp"

struct MusicInfo {
	string name;
	std::vector<ubyte> soundBanks;
	uint numBytes;
	bool isVanilla;
	bool isPlaceholder;
	MuteBehaviour muteBehaviour;
	sbyte volume;
};

struct CustomMusicInfo {
	string name;
	std::vector<ubyte> soundBanks;
	MuteBehaviour muteBehaviour;
	sbyte masterVolume;
	ushort muteBehaviourLocation;
	ushort masterVolumeLocation;
};

struct MusicSequenceInfo {
	size_t fileSize;
	ubyte soundBank0;
	ubyte numSoundBanks;
	MuteBehaviour muteBehaviour;
	sbyte masterVolume;
	ushort muteBehaviourLocation;
	ushort masterVolumeLocation;
};

namespace MusicData {

	void writeToRom( std::ostream &rom );
	std::vector<MusicInfo> getMusicInfo();
	const string &getVanillaMusicName( ubyte i );
	bool tryGetSequenceInfo( const string &filePath, MusicSequenceInfo &info );

}



#endif /* SRC_CORE_MUSIC_HPP_ */
