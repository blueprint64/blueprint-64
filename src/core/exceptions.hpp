#ifndef CORE_EXCEPTIONS_HPP_
#define CORE_EXCEPTIONS_HPP_

#include <stdexcept>
#include <fstream>
#include <optional>
#include <string>

using std::string;

class RomOverflowException : public std::runtime_error {

	public:
	RomOverflowException( const string &message ) :
		runtime_error( message ) {}

};

class ArmipsException : public std::runtime_error {

	public:
	ArmipsException( const string &armipsOutput ) :
		runtime_error( armipsOutput ) {}

};

class VersionError : public std::runtime_error {

	public:
	VersionError( const string &reason ) :
		runtime_error( reason ) {}

};

class BlueprintLoadError : public std::runtime_error {

	public:
	BlueprintLoadError( const string &reason ) :
			runtime_error( reason ) {}
};

class RomError : public std::runtime_error {

	public:
	RomError( const string &reason ) :
		runtime_error( reason ) {}

};

class FileParserException : public std::runtime_error {

	public:
	FileParserException( string detail, size_t lineNumber = 0 ) :
		runtime_error( buildMessage( detail, lineNumber ) ) {}

	FileParserException( size_t lineNumber = 0 ) :
		FileParserException( "", lineNumber ) {}

	private:
	static string buildMessage( string detail, size_t lineNumber ) {
		string message = "Error parsing file";
		if( lineNumber != 0 ) {
			message += " on line " + std::to_string( lineNumber );
		}
		message += detail.empty() ? "." : ":\n";
		message += detail;
		return message;
	}

};

class UuidParseException : public std::runtime_error {

	public:
	UuidParseException( const string &invalidUuid ) :
		runtime_error( "Failed to parse UUID: " + invalidUuid ) {}

};

class VersionParseException : public std::runtime_error {

	public:
	VersionParseException( const string &invalidVersion ) :
		runtime_error( "Failed to parse version: " + invalidVersion ) {}

};

class AsmModuleException : public std::runtime_error {
	public:
	AsmModuleException( const string &reason ) :
			runtime_error( reason ) {}
};

class EmulatorLoadException : public std::runtime_error {
	public:
	EmulatorLoadException( const string &reason ) :
			runtime_error( reason ) {}
};

#endif /* CORE_EXCEPTIONS_HPP_ */
