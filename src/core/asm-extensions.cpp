#include "src/core/asm-extensions.hpp"

#include <fstream>
#include <cassert>
#include <cstring>
#include "src/polyfill/byte-order.hpp"
#include "src/types.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/locations.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/baserom.hpp"

#include "src/core/asm-extensions-star-spawns.cpp.inc"
#include "src/core/asm-extensions-waypoints.cpp.inc"

// See /asm directory for source code of ASM patches

/* Replace whatever nonsense SM64e is trying to do here with a working
 * version. Load segments 5, 6, 7, and E into the space that SM64e
 * reserves for them.
 */
static const ubyte s_fixSegmentLoaderPatch[] = {
	0x3c, 0x08, 0x80, 0x39, 0x8d, 0x08, 0xbe, 0x28, 0x91, 0x01, 0x00, 0x02,
	0x14, 0x20, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x08, 0x0d, 0xfb, 0x29,
	0x00, 0x00, 0x00, 0x00, 0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14,
	0x3c, 0x09, 0x80, 0x40, 0x25, 0x29, 0x15, 0x84, 0x91, 0x01, 0x00, 0x03,
	0x24, 0x21, 0xff, 0xfb, 0x00, 0x01, 0x08, 0x80, 0x01, 0x21, 0x48, 0x21,
	0x8d, 0x24, 0x00, 0x00, 0xaf, 0xa4, 0x00, 0x10, 0x3c, 0x01, 0x80, 0x00,
	0x00, 0x81, 0x20, 0x25, 0x8d, 0x05, 0x00, 0x04, 0x0c, 0x09, 0xe1, 0x41,
	0x8d, 0x06, 0x00, 0x08, 0x8f, 0xa8, 0x00, 0x10, 0x3c, 0x09, 0x80, 0x39,
	0x8d, 0x29, 0xbe, 0x28, 0x91, 0x21, 0x00, 0x03, 0x3c, 0x09, 0x80, 0x34,
	0x25, 0x29, 0xb4, 0x00, 0x00, 0x01, 0x08, 0x80, 0x01, 0x21, 0x48, 0x21,
	0xad, 0x28, 0x00, 0x00, 0x08, 0x0d, 0xfb, 0x32, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x72, 0xbe, 0x00, 0x00, 0x5f, 0xd0, 0xf0, 0x00, 0x58, 0x3f, 0xd0,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x42, 0x00, 0x00
};

// blueprint-code-injector.asm
static const ubyte s_blueprintCodeInjectorAsm[] = {
	0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x0c, 0x0c, 0x8b, 0xd0,
	0x00, 0x00, 0x00, 0x00, 0x3c, 0x04, 0x80, 0x20, 0x24, 0x84, 0xf2, 0x00,
	0x3c, 0x05, 0x03, 0xca, 0x24, 0xa5, 0xde, 0x00, 0x0c, 0x09, 0x20, 0x8c,
	0x34, 0x06, 0x14, 0x00, 0x3c, 0x04, 0x80, 0x36, 0x24, 0x84, 0x75, 0x00,
	0x3c, 0x05, 0x00, 0x7d, 0x24, 0xa5, 0xc6, 0xc0, 0x3c, 0x06, 0x00, 0x01,
	0x0c, 0x09, 0x20, 0x8c, 0x34, 0xc6, 0x12, 0x00, 0x8f, 0xbf, 0x00, 0x14,
	0x08, 0x09, 0x22, 0x59, 0x27, 0xbd, 0x00, 0x18, 0x27, 0xbd, 0xff, 0xe8,
	0xaf, 0xbf, 0x00, 0x14, 0xaf, 0xa4, 0x00, 0x18, 0xaf, 0xa6, 0x00, 0x20,
	0x0c, 0x09, 0xe1, 0x41, 0x00, 0xa6, 0x30, 0x21, 0x8f, 0xa4, 0x00, 0x18,
	0x0c, 0x0c, 0x91, 0x84, 0x8f, 0xa5, 0x00, 0x20, 0x8f, 0xa4, 0x00, 0x18,
	0x8f, 0xa5, 0x00, 0x20, 0x8f, 0xbf, 0x00, 0x14, 0x08, 0x0c, 0x90, 0xec,
	0x27, 0xbd, 0x00, 0x18
};
static_assert( sizeof( s_blueprintCodeInjectorAsm ) <= 0x124 );

namespace Extensions {
	// load-area-background.asm
	static const ubyte loadAreaBackground[] = {
		0x3c, 0x1f, 0x80, 0x27, 0x27, 0xff, 0x64, 0x90, 0x3c, 0x08, 0x80, 0x33,
		0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20, 0x25, 0x29, 0x05, 0xd8,
		0x01, 0x28, 0x40, 0x21, 0x91, 0x01, 0x00, 0x00, 0x14, 0x20, 0x00, 0x03,
		0x00, 0x00, 0x00, 0x00, 0x03, 0xe0, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00,
		0x3c, 0x04, 0x80, 0x34, 0x8c, 0x84, 0xb4, 0x28, 0x3c, 0x01, 0x80, 0x00,
		0x00, 0x81, 0x20, 0x25, 0x97, 0x08, 0x00, 0x1e, 0x29, 0x01, 0x00, 0x0a,
		0x14, 0x20, 0x00, 0x08, 0x3c, 0x01, 0x00, 0x02, 0x34, 0x21, 0x01, 0x40,
		0x00, 0x28, 0x00, 0x19, 0x3c, 0x01, 0x03, 0x85, 0x00, 0x00, 0x28, 0x12,
		0x34, 0x21, 0xb3, 0x80, 0x10, 0x00, 0x00, 0x06, 0x00, 0xa1, 0x28, 0x21,
		0x3c, 0x05, 0x80, 0x20, 0x24, 0xa5, 0xf2, 0x90, 0x00, 0x08, 0x08, 0x80,
		0x00, 0xa1, 0x28, 0x21, 0x8c, 0xa5, 0x00, 0x00, 0x3c, 0x01, 0x00, 0x02,
		0x34, 0x21, 0x01, 0x40, 0x08, 0x09, 0xe1, 0x41, 0x00, 0xa1, 0x30, 0x21,
		0x00, 0xb3, 0x57, 0x14, 0x00, 0xba, 0x22, 0xd4, 0x00, 0xbc, 0x2c, 0x14,
		0x00, 0xbe, 0xad, 0x54, 0x00, 0xb5, 0xd8, 0x54, 0x00, 0xc1, 0x2e, 0x94,
		0x00, 0xc3, 0xaf, 0xd4, 0x00, 0xc5, 0x79, 0x14, 0x00, 0xb8, 0x59, 0x94,
		0x00, 0xc7, 0xfa, 0x54
	};

	// star-spawns.asm
	static const ubyte starSpawnShim[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0xaf, 0xa4, 0x00, 0x10,
		0x3c, 0x08, 0x80, 0x33, 0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20,
		0x25, 0x29, 0x05, 0xd8, 0x01, 0x28, 0x40, 0x21, 0x91, 0x01, 0x00, 0x00,
		0x14, 0x20, 0x00, 0x03, 0x3c, 0x02, 0x80, 0x20, 0x10, 0x00, 0x00, 0x03,
		0x24, 0x42, 0x04, 0xac, 0x0c, 0x09, 0xdf, 0xd4, 0x3c, 0x04, 0x0e, 0x00,
		0x8f, 0xa8, 0x00, 0x10, 0x00, 0x08, 0x08, 0xc0, 0x00, 0x41, 0x10, 0x21,
		0x00, 0x08, 0x08, 0x80, 0x00, 0x41, 0x10, 0x21, 0xc4, 0x4c, 0x00, 0x00,
		0xc4, 0x4e, 0x00, 0x04, 0x8c, 0x46, 0x00, 0x08, 0x8f, 0xbf, 0x00, 0x14,
		0x08, 0x0b, 0xca, 0xe2, 0x27, 0xbd, 0x00, 0x18
	};

	// waypoints.asm
	static const ubyte waypointsShim[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x00, 0x04, 0x20, 0x80,
		0x3c, 0x08, 0x80, 0x33, 0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20,
		0x25, 0x29, 0x05, 0xd8, 0x01, 0x28, 0x40, 0x21, 0x91, 0x01, 0x00, 0x00,
		0x14, 0x20, 0x00, 0x05, 0x3c, 0x08, 0x80, 0x20, 0x25, 0x08, 0x04, 0x54,
		0x01, 0x04, 0x20, 0x21, 0x10, 0x00, 0x00, 0x08, 0x8c, 0x84, 0x00, 0x00,
		0xaf, 0xa4, 0x00, 0x10, 0x3c, 0x04, 0x0e, 0x00, 0x0c, 0x09, 0xdf, 0xd4,
		0x24, 0x84, 0x01, 0x2c, 0x8f, 0xa4, 0x00, 0x10, 0x00, 0x44, 0x20, 0x21,
		0x8c, 0x84, 0x00, 0x00, 0x8f, 0xbf, 0x00, 0x14, 0x08, 0x09, 0xdf, 0xd4,
		0x27, 0xbd, 0x00, 0x18
	};

	//scrolling-textures.asm
	static const ubyte scrollingTexturesCode[] = {
		0x3c, 0x08, 0x80, 0x36, 0x8d, 0x08, 0x11, 0x58, 0x11, 0x00, 0x00, 0x6c,
		0x3c, 0x08, 0x80, 0x33, 0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20,
		0x25, 0x29, 0x05, 0xd8, 0x01, 0x28, 0x40, 0x21, 0x91, 0x01, 0x00, 0x00,
		0x10, 0x20, 0x00, 0x65, 0x00, 0x00, 0x00, 0x00, 0x27, 0xbd, 0xff, 0xe0,
		0xaf, 0xbf, 0x00, 0x1c, 0xaf, 0xb0, 0x00, 0x18, 0xaf, 0xb1, 0x00, 0x14,
		0xaf, 0xb2, 0x00, 0x10, 0x3c, 0x04, 0x0e, 0x00, 0x0c, 0x09, 0xdf, 0xd4,
		0x24, 0x84, 0x01, 0x88, 0x00, 0x40, 0x80, 0x25, 0x3c, 0x11, 0x80, 0x33,
		0x96, 0x31, 0xdf, 0x08, 0x3c, 0x12, 0x80, 0x34, 0x96, 0x52, 0xba, 0xca,
		0x34, 0x01, 0x00, 0x01, 0x02, 0x41, 0x90, 0x04, 0x8e, 0x01, 0x00, 0x00,
		0x10, 0x20, 0x00, 0x4c, 0x96, 0x01, 0x00, 0x04, 0x00, 0x32, 0x08, 0x24,
		0x10, 0x20, 0x00, 0x47, 0x96, 0x01, 0x00, 0x06, 0x14, 0x20, 0x00, 0x0a,
		0x00, 0x00, 0x00, 0x00, 0x8e, 0x08, 0x00, 0x0c, 0x01, 0x11, 0x00, 0x18,
		0x00, 0x00, 0x48, 0x12, 0x00, 0x09, 0x24, 0x03, 0x8e, 0x08, 0x00, 0x10,
		0x01, 0x11, 0x00, 0x18, 0x00, 0x00, 0x48, 0x12, 0x10, 0x00, 0x00, 0x23,
		0x00, 0x09, 0x2c, 0x03, 0x96, 0x08, 0x00, 0x0c, 0x31, 0x08, 0xff, 0xf0,
		0x00, 0x08, 0x40, 0x82, 0x3c, 0x01, 0x80, 0x38, 0x00, 0x28, 0x40, 0x21,
		0xc5, 0x04, 0x70, 0x00, 0xc5, 0x10, 0x60, 0x00, 0x96, 0x08, 0x00, 0x0e,
		0x02, 0x28, 0x00, 0x1b, 0x00, 0x00, 0x48, 0x10, 0x44, 0x89, 0x30, 0x00,
		0x44, 0x88, 0x90, 0x00, 0x34, 0x01, 0xff, 0xff, 0x44, 0x81, 0x40, 0x00,
		0x46, 0x80, 0x31, 0xa0, 0x46, 0x80, 0x94, 0xa0, 0x46, 0x80, 0x42, 0x20,
		0x46, 0x12, 0x31, 0x83, 0x46, 0x08, 0x31, 0x82, 0x46, 0x00, 0x31, 0xa4,
		0x44, 0x08, 0x30, 0x00, 0x31, 0x08, 0xff, 0xf0, 0x00, 0x08, 0x40, 0x82,
		0x3c, 0x01, 0x80, 0x38, 0x00, 0x28, 0x40, 0x21, 0xc5, 0x06, 0x60, 0x00,
		0xc6, 0x12, 0x00, 0x10, 0x46, 0x12, 0x31, 0x82, 0x46, 0x06, 0x21, 0x02,
		0x46, 0x06, 0x84, 0x02, 0x46, 0x00, 0x21, 0x24, 0x46, 0x00, 0x84, 0x24,
		0x44, 0x04, 0x20, 0x00, 0x44, 0x05, 0x80, 0x00, 0x96, 0x08, 0x00, 0x08,
		0x00, 0x88, 0x00, 0x1a, 0x00, 0x00, 0x10, 0x10, 0x00, 0x40, 0x08, 0x2a,
		0x10, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x10, 0x20,
		0x96, 0x08, 0x00, 0x0a, 0x00, 0xa8, 0x00, 0x1a, 0x00, 0x00, 0x18, 0x10,
		0x00, 0x60, 0x08, 0x2a, 0x10, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x68, 0x18, 0x20, 0x30, 0x42, 0x0f, 0xff, 0x30, 0x63, 0x0f, 0xff,
		0x3c, 0x08, 0xf2, 0x00, 0x00, 0x02, 0x13, 0x00, 0x00, 0x43, 0x10, 0x25,
		0x01, 0x02, 0x88, 0x25, 0x0c, 0x09, 0xdf, 0xd4, 0x8e, 0x04, 0x00, 0x00,
		0xac, 0x51, 0x00, 0x00, 0x3c, 0x11, 0x80, 0x33, 0x96, 0x31, 0xdf, 0x08,
		0x10, 0x00, 0xff, 0xb3, 0x26, 0x10, 0x00, 0x14, 0x34, 0x04, 0x00, 0x00,
		0x8f, 0xb2, 0x00, 0x10, 0x8f, 0xb1, 0x00, 0x14, 0x8f, 0xb0, 0x00, 0x18,
		0x8f, 0xbf, 0x00, 0x1c, 0x08, 0x0a, 0x75, 0xa4, 0x27, 0xbd, 0x00, 0x20,
		0x08, 0x0a, 0x75, 0xa4, 0x34, 0x04, 0x00, 0x00
	};

	//create-skybox-shim.asm
	static const ubyte createSkyboxShim[] = {
		0x30, 0xa1, 0xff, 0xff, 0x28, 0x21, 0x00, 0x0a, 0x14, 0x20, 0x00, 0x02,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x25, 0x08, 0x0b, 0x3f, 0xbd,
		0x00, 0x00, 0x00, 0x00
	};

	//use-object-alpha.asm
	static const ubyte geoObjectAlpha[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x8c, 0xa5, 0x00, 0x18,
		0xaf, 0xa5, 0x00, 0x10, 0x34, 0x01, 0x00, 0x01, 0x14, 0x81, 0x00, 0x10,
		0x00, 0x00, 0x00, 0x00, 0x0c, 0x09, 0xe3, 0xcb, 0x34, 0x04, 0x00, 0x10,
		0x3c, 0x01, 0xfa, 0x00, 0xac, 0x41, 0x00, 0x00, 0x3c, 0x08, 0x80, 0x33,
		0x8d, 0x08, 0xdf, 0x00, 0x91, 0x01, 0x01, 0x7f, 0x24, 0x21, 0xff, 0x00,
		0xac, 0x41, 0x00, 0x04, 0x3c, 0x01, 0xb8, 0x00, 0xac, 0x41, 0x00, 0x08,
		0xac, 0x40, 0x00, 0x0c, 0x00, 0x40, 0x20, 0x25, 0x0c, 0x09, 0xee, 0x41,
		0x8f, 0xa5, 0x00, 0x10, 0x00, 0x00, 0x10, 0x25, 0x8f, 0xbf, 0x00, 0x14,
		0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x18
	};

	//geo-switch-opacity.asm
	static const ubyte geoSwitchOpacity[] = {
		0x34, 0x01, 0x00, 0x01, 0x14, 0x81, 0x00, 0x0c, 0x3c, 0x08, 0x80, 0x33,
		0x8d, 0x08, 0xdf, 0x00, 0x91, 0x08, 0x01, 0x7f, 0x34, 0x01, 0x00, 0xff,
		0x11, 0x01, 0x00, 0x07, 0xa4, 0xa0, 0x00, 0x1e, 0x15, 0x00, 0x00, 0x03,
		0x34, 0x01, 0x00, 0x02, 0x10, 0x00, 0x00, 0x03, 0xa4, 0xa1, 0x00, 0x1e,
		0x34, 0x01, 0x00, 0x01, 0xa4, 0xa1, 0x00, 0x1e, 0x03, 0xe0, 0x00, 0x08,
		0x00, 0x00, 0x10, 0x25
	};

	//boss-camera-trigger.asm
	static const ubyte bossCameraTrigger[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0xaf, 0xa4, 0x00, 0x18,
		0xaf, 0xa6, 0x00, 0x1c, 0x90, 0x88, 0x00, 0x00, 0x34, 0x01, 0x00, 0x0b,
		0x11, 0x01, 0x00, 0x1a, 0x3c, 0x01, 0x80, 0x36, 0x8c, 0x28, 0x11, 0x60,
		0xaf, 0xa8, 0x00, 0x10, 0x8c, 0x28, 0x11, 0x58, 0xac, 0x28, 0x11, 0x60,
		0x0c, 0x0a, 0x7e, 0x57, 0x00, 0xa0, 0x20, 0x25, 0x3c, 0x01, 0x80, 0x36,
		0x8f, 0xa8, 0x00, 0x10, 0xac, 0x28, 0x11, 0x60, 0x10, 0x40, 0x00, 0x0f,
		0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a, 0x34, 0x21, 0x10, 0x00,
		0xa5, 0x01, 0xc8, 0x4a, 0x3c, 0x01, 0x80, 0x33, 0xac, 0x22, 0xdf, 0x30,
		0x8f, 0xa4, 0x00, 0x18, 0x34, 0x05, 0x00, 0x0b, 0x0c, 0x0a, 0x18, 0x22,
		0x8f, 0xa6, 0x00, 0x1c, 0x8f, 0xa8, 0x00, 0x18, 0x85, 0x08, 0x00, 0x3a,
		0x25, 0x08, 0xe0, 0x00, 0x3c, 0x01, 0x80, 0x34, 0xa4, 0x28, 0xc7, 0x72,
		0x8f, 0xbf, 0x00, 0x14, 0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x18
	};

	//default-camera-trigger.asm
	static const ubyte defaultCameraTrigger[] = {
		0x90, 0x85, 0x00, 0x01, 0x90, 0x81, 0x00, 0x00, 0x10, 0xa1, 0x00, 0x03,
		0x00, 0x00, 0x00, 0x00, 0x08, 0x0a, 0x18, 0x22, 0x34, 0x06, 0x00, 0x0f,
		0x03, 0xe0, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00
	};

	//transition-camera-trigger.asm
	static const ubyte transitionCameraTrigger[] = {
		0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a, 0x34, 0x21, 0x10, 0x00,
		0xa5, 0x01, 0xc8, 0x4a, 0x90, 0x81, 0x00, 0x00, 0x10, 0x25, 0x00, 0x0e,
		0x34, 0x01, 0x00, 0x0e, 0x14, 0xa1, 0x00, 0x04, 0x3c, 0x01, 0x80, 0x34,
		0xa4, 0x26, 0xc7, 0x76, 0xa4, 0x20, 0xc7, 0x78, 0x00, 0x00, 0x38, 0x25,
		0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a, 0x10, 0xe0, 0x00, 0x02,
		0x30, 0x21, 0xff, 0xfe, 0x34, 0x21, 0x00, 0x01, 0xa5, 0x01, 0xc8, 0x4a,
		0x08, 0x0a, 0x18, 0x22, 0x00, 0x07, 0x30, 0x25, 0x03, 0xe0, 0x00, 0x08,
		0x00, 0x00, 0x00, 0x00
	};

	//radial-camera-trigger.asm
	static const ubyte radialCameraTrigger[] = {
		0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a, 0x34, 0x21, 0x10, 0x00,
		0xa5, 0x01, 0xc8, 0x4a, 0x90, 0x81, 0x00, 0x00, 0x10, 0x25, 0x00, 0x09,
		0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a, 0x10, 0xc0, 0x00, 0x02,
		0x30, 0x21, 0xff, 0xfe, 0x34, 0x21, 0x00, 0x01, 0xa5, 0x01, 0xc8, 0x4a,
		0xe4, 0x8c, 0x00, 0x28, 0x08, 0x0a, 0x18, 0x22, 0xe4, 0x8e, 0x00, 0x2c,
		0x03, 0xe0, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00
	};

	//module-camera-trigger.asm
	static const ubyte moduleCameraTrigger[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x3c, 0x08, 0x80, 0x34,
		0x95, 0x01, 0xc8, 0x4a, 0x34, 0x21, 0x10, 0x00, 0xa5, 0x01, 0xc8, 0x4a,
		0x90, 0x81, 0x00, 0x00, 0x10, 0xa1, 0x00, 0x14, 0xaf, 0xa4, 0x00, 0x18,
		0xaf, 0xa5, 0x00, 0x1c, 0xaf, 0xa6, 0x00, 0x20, 0xde, 0xad, 0xbe, 0xef,
		0xde, 0xad, 0xbe, 0xef, 0x00, 0x05, 0x08, 0x80, 0x01, 0x01, 0x40, 0x21,
		0x8d, 0x08, 0x00, 0x00, 0x01, 0x00, 0xf8, 0x09, 0x27, 0xa5, 0x00, 0x28,
		0x8f, 0xa6, 0x00, 0x20, 0x3c, 0x08, 0x80, 0x34, 0x95, 0x01, 0xc8, 0x4a,
		0x10, 0xc0, 0x00, 0x02, 0x30, 0x21, 0xff, 0xfe, 0x34, 0x21, 0x00, 0x01,
		0xa5, 0x01, 0xc8, 0x4a, 0x8f, 0xa4, 0x00, 0x18, 0x0c, 0x0a, 0x18, 0x22,
		0x8f, 0xa5, 0x00, 0x1c, 0x8f, 0xbf, 0x00, 0x14, 0x03, 0xe0, 0x00, 0x08,
		0x27, 0xbd, 0x00, 0x18
	};
	static const uint moduleCameraTriggerPlaceholder = 44;

	// reflection-mapping.asm
	static const ubyte reflectionMappingCode[] = {
		0x8c, 0xa5, 0x00, 0x18, 0x34, 0x01, 0x00, 0x01, 0x14, 0x81, 0x00, 0x08,
		0x3c, 0x04, 0x80, 0x33, 0x8c, 0x84, 0xdf, 0x00, 0x14, 0x80, 0x00, 0x07,
		0x24, 0x84, 0x00, 0x20, 0x3c, 0x04, 0x80, 0x34, 0x8c, 0x84, 0xcb, 0xd0,
		0x10, 0x00, 0x00, 0x03, 0x24, 0x84, 0x00, 0x04, 0x03, 0xe0, 0x00, 0x08,
		0x00, 0x00, 0x10, 0x25, 0x27, 0xbd, 0xff, 0xd0, 0xaf, 0xbf, 0x00, 0x2c,
		0xaf, 0xa4, 0x00, 0x30, 0xaf, 0xa5, 0x00, 0x34, 0x0c, 0x09, 0xe3, 0xcb,
		0x34, 0x04, 0x00, 0x20, 0xaf, 0xa2, 0x00, 0x28, 0xac, 0x40, 0x00, 0x00,
		0xac, 0x40, 0x00, 0x04, 0x3c, 0x01, 0x00, 0x80, 0xac, 0x41, 0x00, 0x10,
		0xac, 0x41, 0x00, 0x14, 0x3c, 0x08, 0x80, 0x34, 0x8d, 0x08, 0xcb, 0xd0,
		0xc5, 0x04, 0x00, 0x10, 0xc5, 0x06, 0x00, 0x14, 0xc5, 0x08, 0x00, 0x18,
		0x8f, 0xa4, 0x00, 0x30, 0xc4, 0x8a, 0x00, 0x00, 0xc4, 0x8c, 0x00, 0x04,
		0xc4, 0x8e, 0x00, 0x08, 0x46, 0x0a, 0x21, 0x01, 0x46, 0x0c, 0x31, 0x81,
		0x46, 0x0e, 0x42, 0x01, 0xe7, 0xa4, 0x00, 0x10, 0xe7, 0xa6, 0x00, 0x14,
		0xe7, 0xa8, 0x00, 0x18, 0x04, 0x11, 0x00, 0x43, 0x27, 0xa4, 0x00, 0x10,
		0x3c, 0x04, 0x80, 0x34, 0x04, 0x11, 0x00, 0x70, 0x84, 0x84, 0xc7, 0x12,
		0x44, 0x80, 0x60, 0x00, 0xc7, 0xa4, 0x00, 0x10, 0xc7, 0xa6, 0x00, 0x18,
		0x46, 0x04, 0x22, 0x02, 0x46, 0x06, 0x32, 0x82, 0x46, 0x0a, 0x42, 0x00,
		0x46, 0x0c, 0x40, 0x32, 0x46, 0x00, 0x42, 0x04, 0x45, 0x01, 0x00, 0x04,
		0x00, 0x00, 0x00, 0x00, 0x46, 0x08, 0x21, 0x03, 0x10, 0x00, 0x00, 0x04,
		0x46, 0x08, 0x31, 0x83, 0x44, 0x80, 0x20, 0x00, 0x3c, 0x01, 0x3f, 0x80,
		0x44, 0x81, 0x30, 0x00, 0x46, 0x02, 0x21, 0x02, 0x46, 0x02, 0x31, 0x82,
		0x46, 0x00, 0x21, 0x07, 0xe7, 0xa6, 0x00, 0x1c, 0xe7, 0xa0, 0x00, 0x20,
		0xe7, 0xa4, 0x00, 0x24, 0x04, 0x11, 0x00, 0x28, 0x27, 0xa4, 0x00, 0x1c,
		0x27, 0xa4, 0x00, 0x10, 0x27, 0xa5, 0x00, 0x1c, 0x04, 0x11, 0x00, 0x3a,
		0x00, 0x80, 0x30, 0x25, 0x04, 0x11, 0x00, 0x59, 0x27, 0xa4, 0x00, 0x1c,
		0x8f, 0xa1, 0x00, 0x28, 0xac, 0x22, 0x00, 0x08, 0xac, 0x20, 0x00, 0x0c,
		0x04, 0x11, 0x00, 0x54, 0x27, 0xa4, 0x00, 0x10, 0x8f, 0xa1, 0x00, 0x28,
		0xac, 0x22, 0x00, 0x18, 0xac, 0x20, 0x00, 0x1c, 0x0c, 0x09, 0xe3, 0xcb,
		0x34, 0x04, 0x00, 0x18, 0x3c, 0x0a, 0x00, 0xff, 0x35, 0x4a, 0xff, 0xff,
		0x3c, 0x08, 0x03, 0x84, 0x25, 0x08, 0x00, 0x10, 0xac, 0x48, 0x00, 0x00,
		0x8f, 0xa9, 0x00, 0x28, 0x01, 0x2a, 0x48, 0x24, 0xac, 0x49, 0x00, 0x04,
		0x3c, 0x08, 0x03, 0x82, 0x25, 0x08, 0x00, 0x10, 0xac, 0x48, 0x00, 0x08,
		0x25, 0x29, 0x00, 0x10, 0xac, 0x49, 0x00, 0x0c, 0x3c, 0x01, 0xb8, 0x00,
		0xac, 0x41, 0x00, 0x10, 0xac, 0x40, 0x00, 0x14, 0x00, 0x40, 0x20, 0x25,
		0x0c, 0x09, 0xee, 0x41, 0x8f, 0xa5, 0x00, 0x34, 0x00, 0x00, 0x10, 0x25,
		0x8f, 0xbf, 0x00, 0x2c, 0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x30,
		0xc4, 0x84, 0x00, 0x00, 0xc4, 0x86, 0x00, 0x04, 0xc4, 0x88, 0x00, 0x08,
		0x46, 0x04, 0x23, 0x02, 0x46, 0x06, 0x33, 0x82, 0x46, 0x08, 0x44, 0x02,
		0x44, 0x80, 0x50, 0x00, 0x46, 0x0e, 0x60, 0x00, 0x46, 0x00, 0x80, 0x00,
		0x46, 0x0a, 0x00, 0x32, 0x46, 0x00, 0x00, 0x04, 0x45, 0x01, 0x00, 0x08,
		0x00, 0x00, 0x10, 0x25, 0x46, 0x00, 0x21, 0x03, 0x46, 0x00, 0x31, 0x83,
		0x46, 0x00, 0x42, 0x03, 0xe4, 0x84, 0x00, 0x00, 0xe4, 0x86, 0x00, 0x04,
		0xe4, 0x88, 0x00, 0x08, 0x34, 0x02, 0x00, 0x01, 0x03, 0xe0, 0x00, 0x08,
		0x00, 0x00, 0x00, 0x00, 0xc4, 0x84, 0x00, 0x04, 0xc4, 0xa6, 0x00, 0x08,
		0x46, 0x06, 0x21, 0x02, 0xc4, 0x86, 0x00, 0x08, 0xc4, 0xa8, 0x00, 0x04,
		0x46, 0x08, 0x31, 0x82, 0x46, 0x06, 0x22, 0x81, 0xc4, 0x84, 0x00, 0x08,
		0xc4, 0xa6, 0x00, 0x00, 0x46, 0x06, 0x21, 0x02, 0xc4, 0x86, 0x00, 0x00,
		0xc4, 0xa8, 0x00, 0x08, 0x46, 0x08, 0x31, 0x82, 0x46, 0x06, 0x23, 0x01,
		0xc4, 0x84, 0x00, 0x00, 0xc4, 0xa6, 0x00, 0x04, 0x46, 0x06, 0x21, 0x02,
		0xc4, 0x86, 0x00, 0x04, 0xc4, 0xa8, 0x00, 0x00, 0x46, 0x08, 0x31, 0x82,
		0x46, 0x06, 0x23, 0x81, 0xe4, 0xca, 0x00, 0x00, 0xe4, 0xcc, 0x00, 0x04,
		0xe4, 0xce, 0x00, 0x08, 0x10, 0x00, 0xff, 0xd1, 0x00, 0xc0, 0x20, 0x25,
		0x30, 0x84, 0xff, 0xf0, 0x00, 0x04, 0x20, 0x82, 0x3c, 0x01, 0x80, 0x38,
		0x00, 0x24, 0x20, 0x21, 0xc4, 0x80, 0x60, 0x00, 0x03, 0xe0, 0x00, 0x08,
		0xc4, 0x82, 0x70, 0x00, 0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14,
		0x04, 0x11, 0x00, 0x0d, 0xc4, 0x8c, 0x00, 0x00, 0x00, 0x02, 0x46, 0x00,
		0x04, 0x11, 0x00, 0x0a, 0xc4, 0x8c, 0x00, 0x04, 0x00, 0x02, 0x0c, 0x00,
		0x01, 0x01, 0x40, 0x25, 0x04, 0x11, 0x00, 0x06, 0xc4, 0x8c, 0x00, 0x08,
		0x00, 0x02, 0x0a, 0x00, 0x01, 0x01, 0x10, 0x25, 0x8f, 0xbf, 0x00, 0x14,
		0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x18, 0x3c, 0x01, 0x43, 0x00,
		0x44, 0x81, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46, 0x04, 0x61, 0x02,
		0x46, 0x00, 0x21, 0x24, 0x44, 0x02, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x28, 0x41, 0x00, 0x80, 0x14, 0x20, 0x00, 0x02, 0x30, 0x42, 0x00, 0xff,
		0x34, 0x02, 0x00, 0x7f, 0x03, 0xe0, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00
	};

	// revert-lookat.asm
	static const ubyte revertLookAtCode[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x34, 0x01, 0x00, 0x01,
		0x14, 0x81, 0x00, 0x04, 0x3c, 0x04, 0x80, 0x20, 0x24, 0x84, 0x02, 0xc8,
		0x0c, 0x09, 0xee, 0x41, 0x8c, 0xa5, 0x00, 0x18, 0x00, 0x00, 0x10, 0x25,
		0x8f, 0xbf, 0x00, 0x14, 0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x18
	};

	static const uint defaultLookAtDl[] = {
		0x03840010_be32,
		0x002002E0_be32,
		0x03820010_be32,
		0x002002F0_be32,
		0xB8000000_be32,
		0x00000000_be32,

		0x00000000_be32,
		0x00000000_be32,
		0x007F0000_be32,
		0x00000000_be32,
		0x00800000_be32,
		0x00800000_be32,
		0x7F000000_be32,
		0x00000000_be32
	};

	// create-camera-shim.asm
	static const ubyte createCameraShim[] = {
		0x3c, 0x08, 0x80, 0x33, 0x25, 0x08, 0xdf, 0x90, 0x8c, 0x81, 0x00, 0x1c,
		0xad, 0x01, 0x00, 0x00, 0x8c, 0x81, 0x00, 0x20, 0xad, 0x01, 0x00, 0x04,
		0x8c, 0x81, 0x00, 0x24, 0xad, 0x01, 0x00, 0x08, 0x8c, 0x81, 0x00, 0x28,
		0xad, 0x01, 0x00, 0x0c, 0x8c, 0x81, 0x00, 0x2c, 0xad, 0x01, 0x00, 0x10,
		0x8c, 0x81, 0x00, 0x30, 0x08, 0x0a, 0x1e, 0xf8, 0xad, 0x01, 0x00, 0x14
	};

	// pause-camera-shim.asm
	static const ubyte pauseCameraShim[] = {
		0x3c, 0x08, 0x80, 0x33, 0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20,
		0x25, 0x29, 0x05, 0xd8, 0x01, 0x28, 0x40, 0x21, 0x91, 0x01, 0x00, 0x00,
		0x10, 0x20, 0x00, 0x0f, 0x3c, 0x08, 0x80, 0x33, 0x25, 0x08, 0xdf, 0x90,
		0x8f, 0xa9, 0x00, 0x40, 0x8d, 0x01, 0x00, 0x00, 0xad, 0x21, 0x00, 0x1c,
		0x8d, 0x01, 0x00, 0x04, 0xad, 0x21, 0x00, 0x20, 0x8d, 0x01, 0x00, 0x08,
		0xad, 0x21, 0x00, 0x24, 0x8d, 0x01, 0x00, 0x0c, 0xad, 0x21, 0x00, 0x28,
		0x8d, 0x01, 0x00, 0x10, 0xad, 0x21, 0x00, 0x2c, 0x8d, 0x01, 0x00, 0x14,
		0xad, 0x21, 0x00, 0x30, 0x08, 0x0a, 0x1e, 0xe7, 0x00, 0x00, 0x00, 0x00
	};

	// camera-transition-fix.asm
	static const ubyte cameraTransitionFix[] = {
		0x29, 0x01, 0x00, 0x48, 0x14, 0x20, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00,
		0x3c, 0x19, 0x80, 0x28, 0x27, 0x39, 0x3a, 0x18, 0x03, 0x20, 0x00, 0x08,
		0x00, 0x00, 0x00, 0x00
	};

	// make-roll-matrix.asm
	static const ubyte makeRollMatrix[] = {
		0x27, 0xbd, 0xff, 0xa8, 0xaf, 0xbf, 0x00, 0x54, 0xaf, 0xa4, 0x00, 0x58,
		0xaf, 0xa5, 0x00, 0x5c, 0x0c, 0x0d, 0xe3, 0xad, 0x27, 0xa4, 0x00, 0x10,
		0x8f, 0xa5, 0x00, 0x5c, 0x30, 0xa5, 0xff, 0xf0, 0x00, 0x05, 0x28, 0x82,
		0x3c, 0x01, 0x80, 0x38, 0x00, 0x25, 0x28, 0x21, 0x8c, 0xa1, 0x70, 0x00,
		0xaf, 0xa1, 0x00, 0x10, 0xaf, 0xa1, 0x00, 0x24, 0xc4, 0xa4, 0x60, 0x00,
		0xe7, 0xa4, 0x00, 0x14, 0x46, 0x00, 0x21, 0x07, 0xe7, 0xa4, 0x00, 0x20,
		0x27, 0xa4, 0x00, 0x10, 0x8f, 0xa5, 0x00, 0x58, 0x8f, 0xbf, 0x00, 0x54,
		0x08, 0x0c, 0xa5, 0x14, 0x27, 0xbd, 0x00, 0x58
	};

	// pause-warp-shim.asm
	static const ubyte pauseWarpShim[] = {
		0x27, 0xbd, 0xff, 0xe8, 0xaf, 0xbf, 0x00, 0x14, 0x3c, 0x08, 0x80, 0x33,
		0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20, 0x25, 0x29, 0x05, 0xd8,
		0x01, 0x28, 0x40, 0x21, 0x91, 0x08, 0x00, 0x00, 0x11, 0x00, 0x00, 0x07,
		0x3c, 0x04, 0x0e, 0x00, 0x0c, 0x09, 0xdf, 0xd4, 0x34, 0x84, 0x01, 0x84,
		0x94, 0x44, 0x00, 0x00, 0x90, 0x45, 0x00, 0x02, 0x90, 0x46, 0x00, 0x03,
		0x00, 0x00, 0x38, 0x25, 0x8f, 0xbf, 0x00, 0x14, 0x08, 0x09, 0x29, 0xc0,
		0x27, 0xbd, 0x00, 0x18
	};

}

static_assert(
	sizeof( Extensions::loadAreaBackground ) +
	sizeof( Extensions::starSpawnShim ) +
	sizeof( Extensions::waypointsShim ) +
	sizeof( Extensions::scrollingTexturesCode ) +
	sizeof( Extensions::createSkyboxShim ) +
	sizeof( Extensions::geoObjectAlpha ) +
	sizeof( Extensions::geoSwitchOpacity ) +
	sizeof( Extensions::bossCameraTrigger ) +
	sizeof( Extensions::defaultCameraTrigger ) +
	sizeof( Extensions::transitionCameraTrigger ) +
	sizeof( Extensions::radialCameraTrigger ) +
	sizeof( Extensions::moduleCameraTrigger ) +
	sizeof( Extensions::reflectionMappingCode ) +
	sizeof( Extensions::revertLookAtCode ) +
	sizeof( Extensions::createCameraShim ) +
	sizeof( Extensions::pauseCameraShim ) +
	sizeof( Extensions::cameraTransitionFix ) +
	sizeof( Extensions::makeRollMatrix ) +
	sizeof( Extensions::pauseWarpShim ) +
	sizeof( s_vanillaStarSpawns ) +
	sizeof( s_vanillaWaypoints ) +
	sizeof( Extensions::defaultLookAtDl ) +
	40 // imported_levels_table
	<= 0x1400
);

static_assert( 0x3C9EEC8u + sizeof( Extensions::defaultLookAtDl ) == 0x3C9EF00u );

enum class ExtensionShimType {
	Word,
	JAL,
	J,
	None
};

static void writeShim( std::ostream &rom, uint location, ExtensionShimType shimType ) {
	const uint head = (uint)rom.tellp();
	uint shim = 0x801FF200u + (head - 0x03C9DE00);
	switch( shimType ) {
		case ExtensionShimType::JAL:
			shim = 0x0C000000 | ((shim & 0x0FFFFFFFu) >> 2);
			break;
		case ExtensionShimType::J:
			shim = 0x08000000 | ((shim & 0x0FFFFFFFu) >> 2);
			break;
		case ExtensionShimType::Word:
			break;
		default: return;
	}
	shim = htonl( shim );
	rom.seekp( location );
	rom.write( (const char*)&shim, 4 );
	rom.seekp( head );
}

#define WRITE_PATCH( patch, shimType, location ) \
	writeShim( rom, location, ExtensionShimType::shimType ); \
	rom.write( (const char*)Extensions::patch, sizeof( Extensions::patch ) );

void AsmExtensions::write( std::ostream &rom ) {
	static const ubyte s_placeholder[8] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xDE, 0xAD, 0xBE, 0xEF };
	assert( std::memcmp( s_placeholder, &Extensions::moduleCameraTrigger[Extensions::moduleCameraTriggerPlaceholder], 8 ) == 0 );

	rom.seekp( 0x6D99 );
	rom.put( 0x20 );

	rom.seekp( 0x31E0 );
	rom.write( (const char*)s_blueprintCodeInjectorAsm, sizeof( s_blueprintCodeInjectorAsm ) );

	rom.seekp( 0x3B02 );
	rom.put( 0x20 );
	rom.put( 0x78 );

	rom.seekp( 0x1201500 );
	rom.write( (const char*)s_fixSegmentLoaderPatch, sizeof( s_fixSegmentLoaderPatch ) );

	rom.seekp( 0x42AA7 );
	rom.put( 0x3D );

	rom.seekp( 0x03C9DE00 );
	WRITE_PATCH( loadAreaBackground, J, 0x31408 )
	WRITE_PATCH( createCameraShim, JAL, 0x42D7C )
	WRITE_PATCH( pauseCameraShim, J, 0x42B80 )
	WRITE_PATCH( cameraTransitionFix, JAL, 0x41338 )
	WRITE_PATCH( geoObjectAlpha, None, 0 )
	WRITE_PATCH( geoSwitchOpacity, None, 0 )
	WRITE_PATCH( bossCameraTrigger, None, 0 )
	WRITE_PATCH( defaultCameraTrigger, None, 0 )
	WRITE_PATCH( transitionCameraTrigger, None, 0 )
	WRITE_PATCH( radialCameraTrigger, None, 0 )
	WRITE_PATCH( moduleCameraTrigger, None, 0 )
	WRITE_PATCH( reflectionMappingCode, None, 0 )
	WRITE_PATCH( revertLookAtCode, None, 0 )
	uint starSpawnFunc = (uint)rom.tellp() + 0x7C561400;
	WRITE_PATCH( starSpawnShim, None, 0 )
	uint waypointFunc = (uint)rom.tellp() + 0x7C561400;
	WRITE_PATCH( waypointsShim, None, 0 )
	WRITE_PATCH( scrollingTexturesCode, JAL, 0x36180 )
	WRITE_PATCH( createSkyboxShim, JAL, 0x31484 );
	WRITE_PATCH( makeRollMatrix, JAL, 0x36FC0 );
	WRITE_PATCH( pauseWarpShim, JAL, 0x6674 );
	assert( rom.tellp() <= 0x3C9EEC8u );

	shimStarSpawns( rom, starSpawnFunc );
	shimWaypoints( rom, waypointFunc );

	rom.seekp( 0x3C9EEC8 );
	rom.write( (const char*)Extensions::defaultLookAtDl, sizeof( Extensions::defaultLookAtDl ) );
	assert( rom.tellp() == 0x3C9EF00 );
}

static_assert( 0x801FF200u + (0x3C9EEC8u - 0x03C9DE00u) == 0x802002C8u );

const uint AsmExtensions::geoObjectAlphaFuncPtr = 0x801FF200u + sizeof( Extensions::loadAreaBackground ) + sizeof( Extensions::createCameraShim ) + sizeof( Extensions::pauseCameraShim ) + sizeof( Extensions::cameraTransitionFix );
const uint AsmExtensions::geoSwitchOpacityFuncPtr = AsmExtensions::geoObjectAlphaFuncPtr + sizeof( Extensions::geoObjectAlpha );
const uint AsmExtensions::bossCameraTriggerFuncPtr = AsmExtensions::geoSwitchOpacityFuncPtr + sizeof( Extensions::geoSwitchOpacity );
const uint AsmExtensions::defaultCameraTriggerFuncPtr = AsmExtensions::bossCameraTriggerFuncPtr + sizeof( Extensions::bossCameraTrigger );
const uint AsmExtensions::transitionCameraTriggerFuncPtr = AsmExtensions::defaultCameraTriggerFuncPtr + sizeof( Extensions::defaultCameraTrigger );
const uint AsmExtensions::radialCameraTriggerFuncPtr = AsmExtensions::transitionCameraTriggerFuncPtr + sizeof( Extensions::transitionCameraTrigger );
const uint AsmExtensions::moduleCameraTriggerFuncPtr = AsmExtensions::radialCameraTriggerFuncPtr + sizeof( Extensions::radialCameraTrigger );
const uint AsmExtensions::geoSetLookatFuncPtr = AsmExtensions::moduleCameraTriggerFuncPtr + sizeof( Extensions::moduleCameraTrigger );
const uint AsmExtensions::revertLookatFuncPtr = AsmExtensions::geoSetLookatFuncPtr + sizeof( Extensions::reflectionMappingCode );

const uint AsmExtensions::customCameraTablePointerRomLocation = AsmExtensions::moduleCameraTriggerFuncPtr + Extensions::moduleCameraTriggerPlaceholder - 0x7C561400u;

#undef WRITE_PATCH

// to-fixed-matrix.asm
static const ubyte s_toFixedMatrixAsm[] = {
	0x27, 0xbd, 0xff, 0xa8, 0xaf, 0xbf, 0x00, 0x54, 0x3c, 0x08, 0x80, 0x33,
	0x85, 0x08, 0xdd, 0xf8, 0x3c, 0x09, 0x80, 0x20, 0x25, 0x29, 0x05, 0xd8,
	0x01, 0x28, 0x40, 0x21, 0x91, 0x08, 0x00, 0x00, 0x15, 0x00, 0x00, 0x02,
	0x3c, 0x01, 0x3f, 0x80, 0x44, 0x81, 0x60, 0x00, 0x27, 0xa8, 0x00, 0x10,
	0x24, 0xa9, 0x00, 0x40, 0x01, 0x25, 0x08, 0x23, 0x24, 0x21, 0xff, 0xfc,
	0x30, 0x21, 0x00, 0x0f, 0x10, 0x20, 0x00, 0x02, 0xc4, 0xa4, 0x00, 0x00,
	0x46, 0x0c, 0x21, 0x03, 0xe5, 0x04, 0x00, 0x00, 0x24, 0xa5, 0x00, 0x04,
	0x14, 0xa9, 0xff, 0xf7, 0x25, 0x08, 0x00, 0x04, 0x00, 0x80, 0x28, 0x25,
	0x0c, 0x0c, 0xa5, 0x14, 0x27, 0xa4, 0x00, 0x10, 0x8f, 0xbf, 0x00, 0x54,
	0x03, 0xe0, 0x00, 0x08, 0x27, 0xbd, 0x00, 0x58
};
static_assert( sizeof( s_toFixedMatrixAsm ) + 8 <= 132 );

void AsmExtensions::extendConsoleRenderLimit( std::ostream &rom, float worldScale ) {
	assert( worldScale >= 1.f );

	rom.seekp( 0xF71B4 );
	if( worldScale != 1.f ) {
		const ushort lui = (ushort)(reinterpret_cast<const uint&>( worldScale ) >> 16);
		const ushort setScaleAsm[] = {
			0x3C01_be16, htons( lui ), // LUI AT, <scale upper bits>
			0x4481_be16, 0x6000_be16, // MTC1 AT, F12
		};
		rom.write( (const char*)setScaleAsm, sizeof( setScaleAsm ) );
		rom.write( (const char*)s_toFixedMatrixAsm, sizeof( s_toFixedMatrixAsm ) );
	} else {
		BaseRom::revert( rom, 132 );
	}

	rom.seekp( 0x36E0A );
	rom.put( (char)0x79 );

	rom.seekp( 0x36E10 );
	const uint lodPatch[] = {
		0xC704BB20_be32, // L.S F4, 0xBB20 (T8)
		0x46002107_be32, // NEG.S F4, F4
		0x46002124_be32, // CVT.W.S F4, F4
		0x440C2000_be32, // MFC1 T4, F4
		0x8FAA0020_be32, // LW T2, 0x20 (SP)
		0x00000000_be32, // NOP
		0x00000000_be32, // NOP
		0x00000000_be32  // NOP
	};
	rom.write( (const char*)lodPatch, sizeof( lodPatch ) );

}
