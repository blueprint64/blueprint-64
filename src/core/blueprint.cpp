#include "src/core/blueprint.hpp"

#include <string>
#include <cstdio>
#include <iostream>
#include <ios>
#include <cassert>
#include <cstring>
#include <unordered_set>
#include <QMessageBox>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/random.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/json.hpp"
#include "src/core/tweak.hpp"
#include "src/core/storage.hpp"
#include "src/core/util.hpp"
#include "src/core/locations.hpp"
#include "src/core/hardcoded.hpp"
#include "src/core/extended-bounds.hpp"
#include "src/core/level-writer/master.hpp"
#include "src/core/asm-extensions.hpp"
#include "src/core/ending.hpp"
#include "src/core/baserom.hpp"
#include "src/core/background.hpp"
#include "src/core/music.hpp"
#include "src/core/alignment-fix.hpp"
#include "src/core/package.hpp"
#include "src/core/migration.hpp"
#include "src/core/serialization-helpers.hpp"
#include "src/core/blueprint-module-manager.hpp"
#include "src/ui/widgets/shift-jis-input.hpp"

using namespace std;
static const uint BBP_SIGNATURE = 0x4242503A;

static const uint DEFAULT_SFX_TABLE[16] = {
	0x50210081,
	0x502D0081,
	0x505F8091,
	0x503DA081,
	0x50410081,
	0x50480081,
	0x50584081,
	0x50388081,
	0x90694081,
	0x506F0081,
	0x30703081,
	0x701EFF81,
	0, 0, 0
};
static_assert( sizeof( DEFAULT_SFX_TABLE) == sizeof( uint ) * 16 );

namespace Migrate {

	inline static CustomMusicInfo MusicInfo_V1_0( const fs::path &tempDir, const Json &musicJson ) {
		CustomMusicInfo music;
		music.name = musicJson["name"].get<string>();
		for( const Json &bank : musicJson["banks"].array() ) {
			music.soundBanks.push_back( bank.get<ubyte>() );
		}

		int numBanks = (int)music.soundBanks.size();
		for( int i = 0; i < numBanks / 2; i++ ) {
			std::swap( music.soundBanks[i], music.soundBanks[numBanks - 1 - i] );
		}

		const fs::path musicPath = tempDir / "music" / (std::to_string( (int)musicJson["id"].get<ubyte>() ) + ".m64");

		MusicSequenceInfo seqInfo;
		if( MusicData::tryGetSequenceInfo( musicPath.u8string(), seqInfo ) ) {
			music.masterVolume = seqInfo.masterVolume;
			music.masterVolumeLocation = seqInfo.masterVolumeLocation;
			music.muteBehaviour = seqInfo.muteBehaviour;
			music.muteBehaviourLocation = seqInfo.muteBehaviourLocation;

			for( int i = numBanks; i < seqInfo.numSoundBanks; i++ ) {
				music.soundBanks.push_back( 37 );
			}
		} else {
			music.masterVolume = -1;
			music.masterVolumeLocation = 0;
			music.muteBehaviour = MuteBehaviour::Undefined;
			music.muteBehaviourLocation = 0;
		}

		return music;
	}

}

static void serializeDialogEntry( JsonWriter &jw, const DialogEntry &custom, const DialogEntry &vanilla ) {
	if( custom == vanilla ) {
		jw.writeNull();
		return;
	}

	jw.writeObjectStart();
		jw.writeProperty( "name", custom.name );
		jw.writeProperty( "halign", custom.halign );
		jw.writeProperty( "valign", custom.valign );
		jw.writeProperty( "hoffset", custom.hoffset );
		jw.writeProperty( "voffset", custom.voffset );
		jw.writeProperty( "page_size", custom.pageSize );
		jw.writeProperty( "sound_effect", custom.soundEffect );
		if( custom.text == vanilla.text ) {
			jw.writeProperty( "text", nullptr );
		} else {
			jw.writeProperty( "text", custom.text );
		}
	jw.writeObjectEnd();
}

static DialogEntry deserializeDialogEntry( const Json &json, const DialogEntry &vanilla ) {
	if( json.isNull() ) {
		return vanilla;
	}

	DialogEntry d;
	d.name = json["name"].get<string>();
	d.halign = json["halign"].get<HAlign>();
	d.valign = json["valign"].get<VAlign>();
	d.hoffset = json["hoffset"].get<ushort>();
	d.voffset = json["voffset"].get<ushort>();
	d.pageSize = json["page_size"].get<ubyte>();
	d.soundEffect = json["sound_effect"].get<sbyte>();
	d.text = json["text"].isNull() ? vanilla.text : json["text"].get<string>();
	return d;
}

static inline CameraTriggerType getTriggerType( const CameraOptions &options ) {
	switch( options.index() ) {
		case 0: return CameraTriggerType::Vanilla;
		case 1: return CameraTriggerType::Custom;
		case 2: return CameraTriggerType::Module;
	}
	assert( false );
	return CameraTriggerType::Vanilla;
}

static inline string areaMaskToHex( const array<ubyte,40> &mask ) {
	char hex[81];
	const uint *words = (const uint*)mask.data();
	for( int i = 0; i < 10; i++ ) {
		std::sprintf( &hex[i << 3], "%08X", ntohl( words[i] ) );
	}
	return string( hex );
}

static inline std::array<ubyte,40> parseAreaMask( const string &hex ) {
	std::array<ubyte,40> mask;
	uint *words = (uint*)mask.data();
	for( int i = 0; i < 10; i++ ) {
		words[i] = htonl( (uint)std::stoull( hex.substr( i << 3, 8 ), nullptr, 16 ) );
	}
	return mask;
}

static Blueprint *s_blueprint = nullptr;

Blueprint *Blueprint::currentEditable() {
	s_blueprint->m_dataChanged = true;
	return s_blueprint;
}

const Blueprint *Blueprint::current() {
	return s_blueprint;
}

void Blueprint::create( const CreateProjectOptions &options ) {
	std::array<uint,16> sfxTable;
	memcpy( sfxTable.data(), DEFAULT_SFX_TABLE, sizeof(DEFAULT_SFX_TABLE) );

	std::array<TerrainSoundPack, 7> defaultTerrainSets = TerrainSoundPack::Default();
	Blueprint *blueprint = new Blueprint(
		fs::to_path( options.projectPath ).make_preferred(),
		createTemporaryDirectory(),
		RomSpecs{
			options.internalRomName,
			!options.disableChecksum,
			options.horizontalBoundaries,
			RomSpecs::Default().startingLevel,
			RomSpecs::Default().worldScale,
			RomSpecs::Default().expandAudioHeap,
			RomSpecs::Default().cutoutDecal,
			RomSpecs::Default().pauseWarp
		},
		map<Uuid,Word>(),
		map<Uuid,AsmModule>(),
		map<string,CustomMarioAction>(),
		std::vector<DialogEntry>( VanillaText::dialog(), VanillaText::dialog() + 170 ),
		std::move( sfxTable ),
		VanillaText::levelNames()[25],
		VanillaText::starNames()[90],
		map<LevelId,LevelSpecs>(),
		map<ushort,AreaSpecs>(),
		map<ubyte,CustomMusicInfo>(),
		HashSet<WaterTextureInfo>(),
		std::map<ubyte,string>(),
		std::move( defaultTerrainSets ),
		fs::to_path( options.romPath ).make_preferred(),
		false,
		false
	);
	blueprint->syncTweakStore();

	static const Uuid DISABLE_DEMOS_TWEAK_ID = Uuid::parse( "fcaf2969-53a7-4680-a85b-29b112728a95" );
	static const Uuid DISABLE_INTRO_TWEAK_ID = Uuid::parse( "2172db30-ff5d-4d76-937c-51c4f4f3fdde" );
	static const Uuid REMOVE_BORDERS_TWEAK_ID = Uuid::parse( "96da2680-41cd-4094-86bc-c00c708aad08" );
	static const Uuid INFINITE_LIVES_TWEAK_ID = Uuid::parse( "b3890b05-940d-4670-9eb5-717da98cb6b4" );
	static const Uuid EXTEND_VERTICAL_BOUNDS_TWEAK_ID = Uuid::parse( "df1bc311-561b-438e-b759-39351251b9c2" );
	static const Uuid BOOT_TO_TWEAK_ID = Uuid::parse( "75533338-06d9-4012-aafb-e77db81f508e" );
	static const Uuid PATCH_HANDS_FREE_HOLDING_TWEAK_ID = Uuid::parse( "1e76a068-a806-42ba-b26e-8300c1ecd911" );

	const Word ENABLED = Word::fromUInt( 1 );
	if( options.disableDemos ) blueprint->setTweakValue( DISABLE_DEMOS_TWEAK_ID, ENABLED );
	if( options.disableIntro ) blueprint->setTweakValue( DISABLE_INTRO_TWEAK_ID, ENABLED );
	if( options.removeBorders ) blueprint->setTweakValue( REMOVE_BORDERS_TWEAK_ID, ENABLED );
	if( options.infiniteLives ) blueprint->setTweakValue( INFINITE_LIVES_TWEAK_ID, ENABLED );
	if( options.extendVerticalBoundaries ) blueprint->setTweakValue( EXTEND_VERTICAL_BOUNDS_TWEAK_ID, ENABLED );
	if( options.romEntry != 0 ) blueprint->setTweakValue( BOOT_TO_TWEAK_ID, Word::fromUInt( options.romEntry ) );
	if( options.patchHandsFreeHolding ) blueprint->setTweakValue( PATCH_HANDS_FREE_HOLDING_TWEAK_ID, ENABLED );

	/* Default Enabled Tweaks */
	static const Uuid FIX_TREE_PARTICLES_TWEAK_ID = Uuid::parse( "dcda688a-5a7c-4c06-8f6d-4b20f0177dba" );
	static const Uuid BOO_COIN_TWEAK_ID = Uuid::parse( "b1110ef0-2a8e-4b4e-95d4-5406934f4664" );
	static const Uuid RACE_MUSIC_FIX_TWEAK_ID = Uuid::parse( "3d51b7ea-4ef1-4baf-94d1-422189c0dfe0" );
	static const Uuid CULLING_RADIUS_FIX_TWEAK_ID = Uuid::parse( "1b190c62-a131-4eea-bbd9-d63d5fdccf02" );
	static const Uuid SEGMENT_TABLE_BOUNDS_CHECK_TWEAK_ID = Uuid::parse( "8d809d82-8dda-44cb-9646-c52e97c458c6" );
	static const Uuid FIX_BAD_DRAW_DISTANCE_CODE_TWEAK_ID = Uuid::parse( "5aaaed41-91c2-49de-aece-4d50b39065ff" );
	static const Uuid B_PARAM_4_FIX_TWEAK_ID = Uuid::parse( "a4413c5e-dbd0-488d-b253-e9ecf9811735" );
	static const Uuid SOLID_INTERACTION_FIX_TWEAK_ID = Uuid::parse( "90a4f3e4-7049-4880-a00e-ae7a94252567" );
	static const Uuid FIX_DEATH_WARP_SOFTLOCKS_TWEAK_ID = Uuid::parse( "31b58291-c6ad-46e8-83e5-c2ffb0d515fe" );
	static const Uuid REMOVE_12K_BOUNDS_CHECK_TWEAK_ID = Uuid::parse( "eb345e1d-27cc-4811-af11-d04c3e73c2da" );

	blueprint->setTweakValue( FIX_TREE_PARTICLES_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( BOO_COIN_TWEAK_ID, Word::fromUInt( 1 ) );
	blueprint->setTweakValue( RACE_MUSIC_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( CULLING_RADIUS_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( SEGMENT_TABLE_BOUNDS_CHECK_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( FIX_BAD_DRAW_DISTANCE_CODE_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( B_PARAM_4_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( SOLID_INTERACTION_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( FIX_DEATH_WARP_SOFTLOCKS_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( REMOVE_12K_BOUNDS_CHECK_TWEAK_ID, ENABLED );

	try {
		blueprint->save();
		RecentProjects::push( options.projectPath );
	} catch( ... ) {
		delete blueprint;
		throw;
	}

	if( s_blueprint != nullptr ) {
		delete s_blueprint;
	}
	s_blueprint = blueprint;

	string discard;
	BlueprintModules::tryAddModule(
		AsmModuleStore::allModules().at( Uuid::parse( "b9266649-01f5-42a9-a687-bf5c84741a24" ) ).at( Version{ 1, 0, 0 } ),
		discard
	);
}

void Blueprint::open( const fs::path &path ) {
	const fs::path tempDir = fs::to_path( createTemporaryDirectory() );

	bool collabMode = (path.extension() == ".bpc");
	if( collabMode ) {
		fs::copy( path.parent_path() / "blueprint", tempDir, fs::copy_options::recursive );
	} else {
		Package::Format packageFormat;
		if( !Package::extract( path, tempDir, packageFormat ) ) {
#ifdef _WIN32
			if( packageFormat == Package::Format::Tarball ) {
				throw BlueprintLoadError(
					"Failed to decompress and extract blueprint file.\n"
					"You are attempting to open an older blueprint file made "
					"in an earlier version of Bowser's Blueprints. You must "
					"have 7zip installed to open this bluerprint format."
				);
			}
#endif
			throw BlueprintLoadError( "Failed to decompress and extract blueprint file." );
		}
	}

	FileReadStream jsonStream = FileReadStream( ( tempDir / "blueprint.json" ).u8string() );
	if( jsonStream.bad() || jsonStream.eof() ) {
		throw BlueprintLoadError( "Failed to open blueprint file." );
	}

	const Json json = Json::parse( jsonStream );
	optional<Version> maybeVersion = Version::tryParse( json["version"].get<string>() );
	if( !maybeVersion.has_value() || maybeVersion.value().major > CurrentVersion::BlueprintSchema.major ) {
		throw BlueprintLoadError( "Blueprint file was created with a newer version of Bowser's Blueprints that is not compatible with this version." );
	}

	Blueprint::free();

	Version schemaVersion = maybeVersion.value();
	const auto versionScope = DeserializationContext::create( schemaVersion );

	map<Uuid,Word> tweaks;
	for( const Json &tweak : json["tweaks"].array() ) {
		const Uuid id = Uuid::parse( tweak["id"].get<string>() );
		const uint value = Util::parseHexWord( tweak["val"].get<string>() );
		tweaks[id] = Word::fromUInt( value );
	}

	vector<DialogEntry> dialogEntries;
	dialogEntries.reserve( 170 );
	if( json["dialog"].isArray() && json["dialog"].array().size() == 170 ) {
		const JArray &dialogJson = json["dialog"].array();
		for( int i = 0; i < 170; i++ ) {
			dialogEntries.push_back(
				deserializeDialogEntry( dialogJson[i], VanillaText::dialog()[i] )
			);
		}
	} else {
		dialogEntries.assign( VanillaText::dialog(), VanillaText::dialog() + 170 );
	}

	array<uint,16> dialogSfxTable;
	if( json["dialog_sfx_table"].isArray() && json["dialog_sfx_table"].array().size() == 16 ) {
		const JArray &jsonTable = json["dialog_sfx_table"].array();
		for( int i = 0; i < 16; i++ ) {
			dialogSfxTable[i] = jsonTable[i].get<uint>();
		}
	} else {
		memcpy( dialogSfxTable.data(), DEFAULT_SFX_TABLE, sizeof(DEFAULT_SFX_TABLE) );
	}

	map<ubyte,CustomMusicInfo> customMusic;
	if( json["music"].isArray() ) {
		for( const Json &musicJson : json["music"].array() ) {
			if( schemaVersion <= Version{ 1, 0, 0 } ) {
				customMusic[ musicJson["id"].get<ubyte>() ] = Migrate::MusicInfo_V1_0( tempDir, musicJson );
				continue;
			}

			CustomMusicInfo music;
			music.name = musicJson["name"].get<string>();
			for( const Json &bank : musicJson["banks"].array() ) {
				music.soundBanks.push_back( bank.get<ubyte>() );
			}
			music.masterVolume = musicJson["volume"].get<sbyte>();
			music.masterVolumeLocation = musicJson["volume_cmd"].get<ushort>();
			music.muteBehaviour = musicJson["mute_behaviour"].get<MuteBehaviour>();
			music.muteBehaviourLocation = musicJson["mute_cmd"].get<ushort>();

			customMusic[ musicJson["id"].get<ubyte>() ] = std::move( music );
		}
	}



	map<ubyte,string> customBackgrounds;
	if( json["backgrounds"].isArray() ) {
		for( const Json &bg : json["backgrounds"].array() ) {
			customBackgrounds[bg["slot"].get<ubyte>()] = bg["name"].get<string>();
		}
	}

	map<Uuid,AsmModule> asmModules;
	if( json["modules"].isArray() ) {
		for( const Json &module : json["modules"].array() ) {
			const AsmModuleDefinition definition = AsmModuleDefinition::loadFromFile(
				(tempDir / "modules" / module["id"].get<string>() / "module.json").u8string()
			);

			HashMap<string,ModulePropertyType> typeMap;
			for( const auto &i : definition.properties ) {
				typeMap[i.name] = i.type;
			}

			HashMap<string,std::variant<int,float,bool,string,ColourRGBA32,uint>> params;
			for( const Json &param : module["parameters"].array() ) {
				const string paramName = param["param"].get<string>();
				const auto ptype = typeMap.find( paramName );
				if( ptype == typeMap.end() ) continue;
				const Json &valueJson = param["value"];
				switch( ptype->second ) {
					case ModulePropertyType::Integer: {
						const std::optional<int> value = valueJson.tryGet<int>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Float:
					case ModulePropertyType::Angle: {
						const std::optional<float> value = valueJson.tryGet<float>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::String: {
						const std::optional<string> value = valueJson.tryGet<string>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Boolean: {
						const std::optional<bool> value = valueJson.tryGet<bool>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Colour: {
						try {
							params[paramName] = ColourRGBA32::parse( valueJson.get<string>() );
						} catch( ... ) {}
						break;
					}
					case ModulePropertyType::Pointer: {
						const std::optional<uint> value = valueJson.tryGet<uint>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
				}
			}

			asmModules[Uuid::parse( module["id"].get<string>() )] = {
				std::move( definition ),
				parseAreaMask( module["area_mask"].get<string>() ),
				std::move( params )
			};
		}
	}

	map<string,CustomMarioAction> customActions;
	if( json["custom_actions"].isArray() ) {
		for( const Json &action : json["custom_actions"].array() ) {
			const string name = action["name"].get<string>();
			customActions[name] = {
				name,
				action["label"].getOrDefault<string>( name + "_impl" ),
				action["category"].get<MarioActionCategory>(),
				action["flags"].get<MarioActionFlag>(),
				true
			};
		}
	}

	array<TerrainSoundPack,7> terrainSets;
	if( json["terrain_sets"].isArray() ) {
		const JArray &jArray = json["terrain_sets"].array();
		if( jArray.size() != 7 ) {
			terrainSets = TerrainSoundPack::Default();
		} else for( int i = 0; i < 7; i++ ) {
			terrainSets[i] = jArray[i].get<TerrainSoundPack>();
		}
	} else {
		terrainSets = TerrainSoundPack::Default();
	}

	RecentProjects::push( path.u8string() );
	s_blueprint = new Blueprint(
		collabMode ? path.parent_path() : path,
		tempDir.u8string(),
		json["rom"].get<RomSpecs>(),
		std::move( tweaks ),
		std::move( asmModules ),
		std::move( customActions ),
		std::move( dialogEntries ),
		std::move( dialogSfxTable ),
		json["generic_level_name"].getOrDefault<string>( VanillaText::levelNames()[25] ),
		json["generic_star_name"].getOrDefault<string>( VanillaText::starNames()[90] ),
		JsonSerializer::parseMap<LevelId,LevelSpecs>( json["levels"], "id", true ),
		JsonSerializer::parseMap<ushort,AreaSpecs>( json["areas"], "id", true ),
		std::move( customMusic ),
		JsonSerializer::parseHashSet<WaterTextureInfo>( json["water_textures"], true ),
		std::move( customBackgrounds ),
		std::move( terrainSets ),
		json["target_rom"].getOrDefault<fs::path>( fs::path() ).make_preferred(),
		json["end_screen_filtering"].getOrDefault<bool>( false ),
		collabMode
	);
	s_blueprint->syncTweakStore();

	if(
		schemaVersion < Version{ 4, 0, 1 } &&
		QMessageBox::question( nullptr, "Add Module?", "You are opening a blueprint made before the input lag patch was created. Would you like to add the Remove Emulator Input Lag asm module now?" ) == QMessageBox::Yes
	) {
		string discard;
		BlueprintModules::tryAddModule(
			AsmModuleStore::allModules().at( Uuid::parse( "b9266649-01f5-42a9-a687-bf5c84741a24" ) ).at( Version{ 1, 0, 0 } ),
			discard
		);
	}
}

void Blueprint::save() {
	removeUnusedTextures();
	const fs::path tempDir( m_tempDir );

	FileWriteStream jsonStream = FileWriteStream( (tempDir / "blueprint.json").u8string() );
	if( jsonStream.bad() ) {
		throw FileParserException( "Error opening blueprint data file." );
	}

	JsonWriter jw = JsonWriter( &jsonStream, true );
	jw.writeObjectStart();

	jw.writeProperty( "version", CurrentVersion::BlueprintSchema.toString() );
	jw.writePropertyName( "target_rom" );
	if( !m_romPath.empty() ) {
		jw.writeString( m_romPath.generic_string() );
	} else {
		jw.writeNull();
	}

	jw.writeProperty( "rom", m_romSpecs );

	jw.writePropertyName( "tweaks" );
	jw.writeArrayStart();
	for( const auto &tweak : m_tweaks ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", tweak.first.toString() );
		jw.writeProperty( "val", Util::toHexWord( tweak.second.asUInt() ) );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "dialog" );
	jw.writeArrayStart();
	for( int i = 0; i < 170; i++ ) {
		serializeDialogEntry( jw, m_dialogData[i], VanillaText::dialog()[i] );
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "dialog_sfx_table" );
	jw.writeArrayStart();
	for( int i = 0; i < 16; i++ ) {
		jw.writeNumber( (uint64)m_dialogSfxTable[i] );
	}
	jw.writeArrayEnd();

	jw.writeProperty( "generic_level_name", m_genericLevelName );
	jw.writeProperty( "generic_star_name", m_genericStarName );

	JsonSerializer::serializeDictionaryValues( jw, "levels", m_levels.begin(), m_levels.end() );
	JsonSerializer::serializeDictionaryValues( jw, "areas", m_areas.begin(), m_areas.end() );

	jw.writePropertyName( "music" );
	jw.writeArrayStart();
	for( const auto &i : m_customMusic ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", i.first );
		jw.writeProperty( "name", i.second.name );
		jw.writePropertyName( "banks" );
		jw.writeArrayStart();
		for( ubyte bank : i.second.soundBanks ) {
			jw.writeNumber( (uint64)bank );
		}
		jw.writeArrayEnd();
		jw.writeProperty( "mute_behaviour", i.second.muteBehaviour );
		jw.writeProperty( "volume", i.second.masterVolume );
		jw.writeProperty( "mute_cmd", i.second.muteBehaviourLocation );
		jw.writeProperty( "volume_cmd", i.second.masterVolumeLocation );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	JsonSerializer::serializeIterator( jw, "water_textures", m_waterTextures.begin(), m_waterTextures.end() );

	jw.writePropertyName( "backgrounds" );
	jw.writeArrayStart();
	for( const auto &i : m_customBackgrounds ) {
		jw.writeObjectStart();
		jw.writeProperty( "slot", i.first );
		jw.writeProperty( "name", i.second );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	JsonSerializer::serializeIterator( jw, "terrain_sets", m_terrainSets.begin(), m_terrainSets.end() );

	jw.writeProperty( "end_screen_filtering", m_endScreenFiltering );

	jw.writePropertyName( "modules" );
	jw.writeArrayStart();
	for( const auto &i : m_modules ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", i.first.toString() );
		jw.writeProperty( "area_mask", areaMaskToHex( i.second.areaMask ) );
		jw.writePropertyName( "parameters" );
		jw.writeArrayStart();
		for( const auto &j : i.second.parameters ) {
			jw.writeObjectStart();
			jw.writeProperty( "param", j.first );
			if( std::holds_alternative<int>( j.second ) ) {
				jw.writeProperty( "value", std::get<int>( j.second ) );
			} else if( std::holds_alternative<float>( j.second ) ) {
				jw.writeProperty( "value", std::get<float>( j.second ) );
			} else if( std::holds_alternative<bool>( j.second ) ) {
				jw.writeProperty( "value", std::get<bool>( j.second ) );
			} else if( std::holds_alternative<string>( j.second ) ) {
				jw.writeProperty( "value", std::get<string>( j.second ) );
			} else if( std::holds_alternative<ColourRGBA32>( j.second ) ) {
				jw.writeProperty( "value", std::get<ColourRGBA32>( j.second ).toString() );
			} else if( std::holds_alternative<uint>( j.second ) ) {
				jw.writeProperty( "value", std::get<uint>( j.second ) );
			} else assert( false );
			jw.writeObjectEnd();
		}
		jw.writeArrayEnd();
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "custom_actions" );
	jw.writeArrayStart();
	for( const auto &i : m_customActions ) {
		const CustomMarioAction &action = i.second;
		jw.writeObjectStart();
		jw.writeProperty( "name", action.variableName );
		jw.writeProperty( "label", action.functionLabel );
		jw.writeProperty( "category", action.category );
		jw.writeProperty( "flags", action.flags );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writeObjectEnd();
	jsonStream.put( '\n' );
	jsonStream.flush();
	jsonStream.close();

	syncBlueprintTweaks();

	if( m_collabMode ) {
		const fs::path blueprintDir = m_filePath / "blueprint";
		fs::forceDeleteRecursive( blueprintDir );
		fs::copy( tempDir, blueprintDir, fs::copy_options::recursive );
		clearReadOnlyFlagOnWindows( blueprintDir );
	} else if( !Package::create( m_filePath, tempDir ) ) {
		throw std::runtime_error( "Failed to save Blueprint file. Did you somehow manage to find a version of Linux that doesn't come with tar installed?" );
	}

	m_dataChanged = false;
}

inline static void assertGood( ios &rom, const string &message = "" ) {
	static const string defaultMessage = "An unknown I/O error occured.";
	if( !rom.good() ) {
		throw std::ios_base::failure( message.empty() ? defaultMessage : message );
	}
}

void Blueprint::writeAsmExtensions( std::ostream &rom, const RomMetadata &metadata ) const {
	if( metadata.romVersion < Version{ 0, 4, 2 } ) {
		rom.seekp( 0x3134 );
		BaseRom::revert( rom, 4 );
	}

	if( metadata.romVersion < Version{ 0, 4, 3 } ) {
		rom.seekp( 0x670 );
		BaseRom::revert( rom, 8 );
	}

	fixMisalignedSegments( rom );
	AsmExtensions::write( rom );
}

void Blueprint::applyRomSettingsInternal( iostream &rom ) const {
	assertGood( rom );

	const string encodedName = ShiftJisInput::toShiftJis( m_romSpecs.internalName );

	size_t nameLength = encodedName.size();
	rom.seekp( 0x20 );
	rom.write( encodedName.c_str(), nameLength < 20 ? nameLength : 20 );
	while( nameLength++ < 20 ) {
		rom.put( ' ' );
	}

	rom.seekp( 0x666B );
	rom.write( (char*)&m_romSpecs.pauseWarp.level, 1 );
	rom.seekp( 0x666F );
	rom.write( (char*)&m_romSpecs.pauseWarp.area, 1 );
	rom.seekp( 0x6673 );
	rom.write( (char*)&m_romSpecs.pauseWarp.warpId, 1 );

	const ubyte levelPatch1[4] = { 0x34, 0x05, 0x00, (ubyte)m_romSpecs.startingLevel }; // ORI A1, R0, startingLevel
	const ubyte levelPatch2[4] = { 0xAF, 0xA5, 0x00, 0x2C }; // SW A1, 0x2C (SP)
	rom.seekp( 0x6D68 );
	rom.write( (char*)levelPatch1, 4 );
	rom.seekp( 0x6D78 );
	rom.write( (char*)levelPatch2, 4 );

	CourseId startingCourse = levelIdToCourseId( m_romSpecs.startingLevel );
	if( startingCourse == CourseId::INVALID ) {
		startingCourse = CourseId::Overworld;
	}
	rom.seekp( 0x6DA9 );
	rom.put( (char)0x08 );
	const ubyte coursePatch1[4] = { 0x34, 0x09, 0x00, (ubyte)startingCourse }; // ORI T1, R0, startingCourse
	const ubyte coursePatch2[4] = { 0xA5, 0x09, 0xBA, 0xC6 }; // SH T1, 0xBAC6 (T0)
	rom.seekp( 0x6DAC );
	rom.write( (const char*)&coursePatch1, 4 );
	rom.seekp( 0x6DCC );
	rom.write( (const char*)&coursePatch2, 4 );

	const uint w1 = m_romSpecs.enableChecksum ? htonl( 0x14E80006 ) : 0;
	const uint w2 = m_romSpecs.enableChecksum ? htonl( 0x16080003 ) : 0;
	rom.seekp( 0x66C );
	rom.write( (char*)&w1, 4 );
	rom.seekp( 0x678 );
	rom.write( (char*)&w2, 4 );

	if( m_romSpecs.expandAudioHeap ) {
		const ubyte patch1[] = { 0x3c, 0x05, 0x80, 0x58, 0x34, 0xa5, 0x3f, 0xd0 };
		const ubyte patch2[] = { 0x3c, 0x0e, 0x80, 0x58, 0x35, 0xce, 0x3f, 0xd0 };
		const ubyte patch3[] = { 0x3c, 0x0f, 0x80, 0x58, 0x25, 0xe2, 0x3f, 0xd0 };
		const ubyte patch4[] = { 0xFF, 0x00 };
		const ubyte patch5[] = { 0x00, 0x07, 0x90, 0x70 };

		rom.seekp( 0xD2138 );
		rom.write( (const char*)patch1, 8 );
		rom.seekp( 0xD2158 );
		rom.write( (const char*)patch2, 8 );
		rom.seekp( 0xD4598 );
		rom.write( (const char*)patch3, 8 );
		rom.seekp( 0xEE2B2 );
		rom.write( (const char*)patch4, 2 );
		rom.seekp( 0xEFFFC );
		rom.write( (const char*)patch5, 4 );
	} else {
		rom.seekp( 0xD2138 );
		BaseRom::revert( rom, 8 );
		rom.seekp( 0xD2158 );
		BaseRom::revert( rom, 8 );
		rom.seekp( 0xD4598 );
		BaseRom::revert( rom, 8 );
		rom.seekp( 0xEE2B2 );
		BaseRom::revert( rom, 2 );
		rom.seekp( 0xEFFFC );
		BaseRom::revert( rom, 4 );
	}

	rom.seekp( 0xE8AA0 );
	for( const TerrainSoundPack &terrainSet : m_terrainSets ) {
		rom.put( (ubyte)terrainSet.normal );
		rom.put( (ubyte)terrainSet.hard );
		rom.put( (ubyte)terrainSet.slippery );
		rom.put( (ubyte)terrainSet.slide );
		rom.put( (ubyte)terrainSet.alternate );
		rom.put( (ubyte)terrainSet.slipperyAlternate );
	}

	setLevelBoundaries( rom, m_romSpecs.levelBoundaries );

	rom.seekp( 0xE8E9A );
	rom.put( m_romSpecs.cutoutDecal ? 0x3D : 0x2D );
	rom.seekp( 0xE8EDA );
	rom.put( m_romSpecs.cutoutDecal ? 0x3D : 0x2D );

	AsmExtensions::extendConsoleRenderLimit( rom, m_romSpecs.worldScale );

	assertGood( rom, "Failed to write ROM settings." );
}

void Blueprint::applyTweaksInternal( iostream &rom, RomMetadata &metadata, vector<string> &warnings ) const {
	assertGood( rom );

	vector<Uuid> unknownTweaks;
	const TweakStore &tweakStore = TweakStore::instance();
	for( const Uuid &tweakId : metadata.appliedTweaks ) {
		if( m_tweaks.find( tweakId ) == m_tweaks.end() ) {
			const ISingularTweakDefinition *tweak = tweakStore.tryGetTweak( tweakId );
			if( tweak == nullptr ) {
				unknownTweaks.push_back( tweakId );
				warnings.push_back( "The ROM has a tweak with ID "s + tweakId.toString() + " applied, but I don't know what that is. Unable to revert it."s );
			} else {
				tweak->revert( rom );
			}
		}
	}

	for( const auto &tweakValue : m_tweaks ) {
		const ISingularTweakDefinition *tweak = tweakStore.tryGetTweak( tweakValue.first );
		if( tweak == nullptr ) {
			warnings.push_back( "Unknown tweak with ID "s + tweakValue.first.toString() + ". Unable to apply this tweak."s );
		} else {
			tweak->apply( rom, tweakValue.second );
		}
	}

	assertGood( rom, "Failed to apply tweaks" );

	metadata.metadataVersion = CurrentVersion::MetadataFormat;
	metadata.appliedTweaks = unknownTweaks;
	metadata.appliedTweaks.reserve( unknownTweaks.size() + m_tweaks.size() );
	for( const auto &tweak : m_tweaks ) {
		metadata.appliedTweaks.push_back( tweak.first );
	}
}

void Blueprint::applyTextInternal( iostream &rom ) const {
	string levelNames[26];
	string starNames[97];

	const Uuid LOWER_CASE_STAR_HINTS_TWEAK_ID = Uuid::parse( "b3d03e80-95ff-409f-a56d-440d9bbcdb5a" );
	const bool supportsLowerCase = m_tweaks.count( LOWER_CASE_STAR_HINTS_TWEAK_ID );

	for( ubyte i = 0; i < 26; i++ ) {
		const LevelId levelId = courseIdToLevelId( (CourseId)(i + 1) );
		const auto levelIt = m_levels.find( levelId );
		if( levelIt != m_levels.end() ) {
			const LevelSpecs &L = levelIt->second;
			levelNames[i] = L.name;
			if( i < 15 ) {
				for( ubyte j = 0; j < 6; j++ ) {
					string starHint = L.starNames[j];
					if( !supportsLowerCase && L.enableStarSelect ) {
						for( char &c : starHint ) {
							c = std::toupper( (ubyte)c );
						}
					}
					starNames[(i*6)+j] = starHint;
				}
			}
		} else {
			levelNames[i] = VanillaText::levelNames()[i];
			if( i < 15 ) {
				for( ubyte j = 0; j < 6; j++ ) {
					starNames[(i*6)+j] = VanillaText::starNames()[(i*6)+j];
				}
			}
		}
	}

	levelNames[25] = m_genericLevelName;
	starNames[90] = m_genericStarName;

	Text::applyChangesToRom(
		rom,
		m_dialogData.data(),
		levelNames,
		starNames,
		m_dialogSfxTable.data()
	);
}

void Blueprint::applyBackgroundChangesInternal( ostream &rom ) const {
	for( const auto &i : m_customBackgrounds ) {
		const ubyte slot = i.first;
		rom.seekp( 0x0399C000 + ( (uint)slot * 0x20140 ) );
		Background::writeToRom( rom, getCustomBackgroundPath( slot ).u8string() );
	}
}

static void revertLevel( ostream &rom, LevelId levelId, RomMetadata &romInfo ) {
	if( romInfo.customLevelTable[(uint)levelId] != 0 ) {
		HardcodedLevelCode::restore( rom, levelId );
		rom.seekp( RomLocations::getLevelLocation( levelId ).scriptLoaderLocation );
		BaseRom::revert( rom, 16 );
		rom.seekp( 0xEE0C0u + ((uint)levelId * 3) );
		BaseRom::revert( rom, 3 );
		romInfo.customLevelTable[(uint)levelId] = 0;
	}
}

void Blueprint::buildLevelInternal(
	iostream &rom,
	LevelId levelId,
	const array<AreaActors,8> &actors,
	map<string,uint> &asmRefs,
	std::map<string,uint> &cameraTriggerRefs,
	RomMetadata &romInfo,
	std::vector<string> &warnings
) const {
	if( levelHasGeometry( levelId ) ) {
		LevelWriter::applyLevelAndAreaFlags( rom );
		LevelWriter::writeToRom( levelId, rom, getCameraModules(), actors, asmRefs, cameraTriggerRefs, m_romSpecs.cutoutDecal, m_romSpecs.worldScale, warnings );
		romInfo.customLevelTable[(uint)levelId] = 0x1;
	} else {
		warnings.push_back( "Level '"s + m_levels.at( levelId ).name + "' has no geometry. Skipping level." );
		revertLevel( rom, levelId, romInfo );
	}
}

void Blueprint::buildEndScreenInternal( ostream &rom ) const {
	if( EndScreen::hasCustomImage() ) {
		EndScreen::import( rom );
	} else {
		EndScreen::revert( rom );
	}
}

void Blueprint::revertDeletedLevels( iostream &rom, RomMetadata &romInfo ) const {
	uint signature;
	rom.seekg( 0x3FFFFF8 );
	rom.read( (char*)&signature, 4 );
	if( ntohl( signature ) != BBP_SIGNATURE ) {
		romInfo.customLevelTable.fill( 0 );
		return;
	}

	for( LevelId levelId : Enum::values<LevelId>() ) {
		if( !levelHasGeometry( levelId ) ) {
			revertLevel( rom, levelId, romInfo );
		}
	}
}

void RomMetadata::writeToRom( ostream &rom ) const {
	const uint signature = htonl( BBP_SIGNATURE );
	size_t metadataSize = 16 + (appliedTweaks.size() << 4) + (moduleRegions.size() << 3);
	const uint metadataStart = 0x3FFFFF8 - (uint)metadataSize;
	rom.seekp( 0x3FFFFF8 );
	rom.write( (char*)&signature, 4 );
	uint startBE = htonl( metadataStart );
	rom.write( (char*)&startBE, 4 );
	rom.seekp( metadataStart );
	metadataVersion.writeToRom( rom );
	romVersion.writeToRom( rom );

	ushort numTweaksBE = htons( (ushort)appliedTweaks.size() );
	rom.write( (char*)&numTweaksBE, 2 );
	for( const Uuid &tweakId : appliedTweaks ) {
		tweakId.writeToRom( rom );
	}

	const ushort numModuleRegionsBE = htons( (ushort)moduleRegions.size() );
	rom.write( (const char*)&numModuleRegionsBE, 2 );
	for( const std::pair<uint,uint> &region : moduleRegions ) {
		const uint regionBE[2] = { htonl( region.first ), htonl( region.second ) };
		rom.write( (const char*)regionBE, 8 );
	}

	assertGood( rom, "Failed to write metadata to ROM" );
	assert( (uint)rom.tellp() == 0x3FFFFF8u );

	rom.seekp( RomLocations::importedLevelsTable );
	rom.write( (const char*)customLevelTable.data(), 40 );
};

RomMetadata RomMetadata::readFromRom( istream &rom ) {
	RomMetadata metadata;
	uint metadataStart;
	uint signature;
	rom.seekg( 0x3FFFFF8 );
	rom.read( (char*)&signature, 4 );
	if( ntohl( signature ) != BBP_SIGNATURE ) {
		metadata.metadataVersion = CurrentVersion::MetadataFormat;
		metadata.romVersion = Version{ 0, 0, 0 };
		metadata.customLevelTable.fill( 0 );
		return metadata;
	}
	rom.read( (char*)&metadataStart, 4 );
	metadataStart = ntohl( metadataStart );
	rom.seekg( metadataStart );
	metadata.metadataVersion = Version::readFromRom( rom );
	metadata.romVersion = Version::readFromRom( rom );

	metadata.appliedTweaks.clear();
	ushort numAppliedTweaks;
	rom.read( (char*)&numAppliedTweaks, 2 );
	numAppliedTweaks = ntohs( numAppliedTweaks );
	metadata.appliedTweaks.reserve( numAppliedTweaks );
	for( uint i = 0; i < numAppliedTweaks; i++ ) {
		metadata.appliedTweaks.push_back( Uuid::readFromRom( rom ) );
	}

	metadata.moduleRegions.clear();
	if( metadata.metadataVersion >= Version{ 0, 1, 0 } ) {
		ushort numModuleRegions;
		rom.read( (char*)&numModuleRegions, 2 );
		numModuleRegions = ntohs( numModuleRegions );
		metadata.moduleRegions.reserve( numModuleRegions );
		for( uint i = 0; i < numModuleRegions; i++ ) {
			uint startBE, endBE;
			rom.read( (char*)&startBE, 4 );
			rom.read( (char*)&endBE, 4 );
			metadata.moduleRegions.push_back({ ntohl( startBE ), ntohl( endBE ) });
		}
	}

	assertGood( rom, "Failed to read metadata from ROM" );
	assert( (uint)rom.tellg() == 0x3FFFFF8u );

	if( metadata.romVersion < Version{ 0, 3, 0 } ) {
		rom.seekg( 0x399D3D8 );
	} else {
		rom.seekg( RomLocations::importedLevelsTable );
	}
	rom.read( (char*)metadata.customLevelTable.data(), 40 );

	metadata.metadataVersion = CurrentVersion::MetadataFormat;
	return metadata;
};

Blueprint::~Blueprint() {
	fs::tryDeleteRecursive( fs::to_path( m_tempDir ) );
}

void Blueprint::free() {
	if( s_blueprint != nullptr ) {
		delete s_blueprint;
		s_blueprint = nullptr;
	}
}

fs::path Blueprint::getLevelPath( LevelId levelId ) const {
	return fs::to_path( m_tempDir ) / "level" / to_string( (int)levelId );
}

fs::path Blueprint::getAreaPath( LevelId levelId, ubyte areaIndex ) const {
	return getLevelPath( levelId ) / "area" / to_string( (int)areaIndex );
}

fs::path Blueprint::getAreaActorsPath( LevelId levelId, ubyte areaIndex ) const {
	return fs::to_path( m_tempDir ) / ( "actors_" + to_string( (int)levelId ) + "_" + to_string( (int)areaIndex ) + ".json" );
}

fs::path Blueprint::getObjectPath( LevelId levelId, objectId objectId ) const {
	return getLevelPath( levelId ) / "object" / Util::toHexWord( objectId );
}

fs::path Blueprint::getUserDataPath( LevelId levelId, uint userDataId ) const {
	return getLevelPath( levelId ) / "user_data" / ( Util::toHexWord( userDataId ) + ".bin" );
}

fs::path Blueprint::getMusicPath( ubyte sequenceId ) const {
	return fs::to_path( m_tempDir ) / "music" / ( to_string( (uint)sequenceId ) + ".m64" );
}

fs::path Blueprint::getEndScreenPath() const {
	return fs::to_path( m_tempDir ) / "end_screen.png";
}

fs::path Blueprint::getTweakPackPath( const Uuid &packId ) const {
	return fs::to_path( m_tempDir ) / "tweak_packs" / (packId.toString() + ".json");
}

fs::path Blueprint::getWaterTexturePath() const {
	return fs::to_path( m_tempDir ) / "water";
}

fs::path Blueprint::getCustomBackgroundPath( ubyte slot ) const {
	return fs::to_path( m_tempDir ) / "backgrounds" / (to_string( (uint)slot ) + ".png");
}

fs::path Blueprint::getModulePath( const Uuid &moduleId ) const {
	return fs::to_path( m_tempDir ) / "modules" / moduleId.toString();
}

HashMap<string,MaterialSpecs> Blueprint::getAreaGeometry( LevelId levelId, ubyte areaIndex ) const {
	const fs::path areaDir = getAreaPath( levelId, areaIndex );
	HashMap<string,MaterialSpecs> materials;
	if( !fs::is_directory( areaDir ) ) {
		return materials;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( areaDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;
		if( !fs::exists( f.path().parent_path() / ( f.path().stem().u8string() + ".model" ) ) ) continue;

		FileReadStream specFile( f.path().u8string() );
		MaterialSpecs specs = Json::parse( specFile ).get<MaterialSpecs>();
		materials.insert_or_assign( f.path().stem().u8string(), std::move( specs ) );
	}

	return materials;
}

HashMap<string,MaterialSpecs> Blueprint::getObjectGeometry( LevelId levelId, objectId objectId ) const {
	const fs::path objectDir = getObjectPath( levelId, objectId );
	HashMap<string,MaterialSpecs> materials;
	if( !fs::is_directory( objectDir ) ) {
		return materials;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( objectDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;
		if( !fs::exists( f.path().parent_path() / ( f.path().stem().u8string() + ".model" ) ) ) continue;

		FileReadStream specFile( f.path().u8string() );
		MaterialSpecs specs = Json::parse( specFile ).get<MaterialSpecs>();
		materials.insert_or_assign( f.path().stem().u8string(), std::move( specs ) );
	}

	return materials;
}

void Blueprint::saveGeometry( const fs::path &dataDir, const HashMap<string,MaterialSpecs> &materials ) const {
	fs::create_directories( dataDir );

	for( const fs::directory_entry &f : fs::directory_iterator( dataDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;

		string extension = f.path().extension().u8string();
		if( materials.find( f.path().stem().u8string() ) != materials.end() ) continue;
		if( extension == ".verts" || extension == ".model" || extension == ".json" ) {
			fs::forceDelete( f.path() );
		}
	}

	for( const auto &i : materials ) {
		FileWriteStream specFile( (dataDir / ( i.first + ".json" )).u8string() );
		JsonWriter jsonWriter( &specFile, true );
		jsonWriter.writeValue( i.second );
	}

}

bool Blueprint::areaHasGeometry( LevelId levelId, ubyte areaIndex ) const {
	const fs::path areaDir = getAreaPath( levelId, areaIndex );
	if( !fs::exists( areaDir ) ) return false;

	for( const fs::directory_entry &f : fs::directory_iterator( areaDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		if( fs::exists( f.path().parent_path() / (f.path().stem().u8string() + ".model" ) ) ) {
			return true;
		}
	}

	return false;
}

bool Blueprint::objectHasGeometry( LevelId levelId, objectId objectId ) const {
	const fs::path objectDir = getObjectPath( levelId, objectId );
	if( !fs::exists( objectDir ) ) return false;

	for( const fs::directory_entry &f : fs::directory_iterator( objectDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		if( fs::exists( f.path().parent_path() / (f.path().stem().u8string() + ".model" ) ) ) {
			return true;
		}
	}

	return false;
}

static const set<ubyte> &vanillaUnusedModelIds() {
	static const set<ubyte> s_unusedModelIds({
		// Model IDs that are never assigned a geo layout in the base game
		0x1A,
		0x1E,
		0x21,
		0x28,
		0x2A, 0x2B,
		0x31, 0x32, 0x33, 0x34,
		0x51, 0x52,
		0x5B, 0x5C, 0x5D, 0x5E, 0x5F, 0x60, 0x61, 0x62, 0x63,
		0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73,
		0x7D, 0x7E,
		0x8D,
		0x92, 0x93,
		0x97, 0x98, 0x99, 0x9A, 0x9B,
		0x9D,
		0xA9,
		0xAB, 0xAC,
		0xB8,
		0xBD,
		0xC4, 0xC5, 0xC6,
		0xD3,
		0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF,
		0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF
	});
	return s_unusedModelIds;
}

set<ubyte> Blueprint::getFreeModelIds( LevelId levelId ) const {
	const LevelSpecs &levelSpecs = m_levels.at( levelId );

	set<ubyte> freeIds = vanillaUnusedModelIds();
	for( const auto &i : levelSpecs.simpleObjects ) {
		freeIds.erase( i.second.modelId );
	}
	for( const auto &i : levelSpecs.advancedObjects ) {
		freeIds.erase( i.second.modelId );
	}
	return freeIds;
}

bool Blueprint::hasFreeModelIds( LevelId levelId ) const {
	const LevelSpecs &levelSpecs = m_levels.at( levelId );
	return (levelSpecs.simpleObjects.size() + levelSpecs.advancedObjects.size()) < vanillaUnusedModelIds().size();
}

std::vector<TweakPack> Blueprint::getEmbeddedTweakPacks() const {
	const fs::path tweakDir = fs::to_path( m_tempDir ) / "tweak_packs";

	std::vector<TweakPack> packs;
	if( !fs::exists( tweakDir ) ) {
		return packs;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().u8string() );
			if( TweakStore::instance().tweakPacks().count( id ) > 0 ) {
				packs.push_back( TweakStore::instance().tweakPacks().at( id ) );
			} else {
				// Tweak pack failed to load when the blueprint was loaded
				packs.push_back({ id.toString(), id, Version{ 0, 0, 0 } });
			}
		} catch( ... ) {}
	}

	return packs;
}

void Blueprint::forceSaveTweakPack( const Uuid &packId ) {
	const fs::path sourcePath = AppData::dataDir() / "tweak_packs" / (packId.toString() + ".json");
	if( !fs::exists( sourcePath ) ) {
		return;
	}

	const fs::path savePath = getTweakPackPath( packId );
	fs::create_directories( savePath.parent_path() );
	fs::tryCopyFileOverwrite( sourcePath, savePath );
}

void Blueprint::syncTweakStore() const {
	TweakStore &store = TweakStore::instance();
	while( !store.tweakPacks().empty() ) {
		store.removeTweakPack( store.tweakPacks().begin()->first );
	}

	const fs::path tweakDir = fs::to_path( m_tempDir ) / "tweak_packs";
	if( fs::exists( tweakDir ) ) {
		for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
			if( f.status().type() != fs::file_type::regular ) continue;
			if( f.path().extension().u8string() != ".json" ) continue;

			try {
				Uuid::parse( f.path().stem().u8string() );
				FileReadStream tweakFile( f.path().u8string() );
				store.loadFileAndGetConflicts( tweakFile, false );
			} catch( ... ) {}
		}
	}

	store.loadInstalledTweakPacks();
}

void Blueprint::syncBlueprintTweaks() const {
	HashSet<Uuid> usedTweakPackIds;
	for( const auto &i : m_tweaks ) {
		const ISingularTweakDefinition *tweak = TweakStore::instance().tryGetTweak( i.first );
		if( tweak != nullptr && tweak->getSource() != nullptr ) {
			usedTweakPackIds.insert( tweak->getSource()->id );
		}
	}

	const fs::path tweakDir = fs::to_path( m_tempDir ) / "tweak_packs";
	if( usedTweakPackIds.empty() ) {
		fs::forceDeleteRecursive( tweakDir );
		return;
	}

	vector<fs::path> packsToDelete;
	fs::create_directories( tweakDir );
	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().u8string() );
			if( usedTweakPackIds.count( id ) > 0 ) {
				usedTweakPackIds.erase( id );
			} else {
				packsToDelete.push_back( f.path() );
			}
		} catch( ... ) {}
	}

	for( const fs::path &pack : packsToDelete ) {
		fs::forceDelete( pack );
	}

	for( const Uuid &packId : usedTweakPackIds ) {
		const fs::path sourcePath = AppData::dataDir() / "tweak_packs" / (packId.toString() + ".json");
		if( fs::exists( sourcePath ) ) {
			fs::copy_file( sourcePath, getTweakPackPath( packId ) );
		}
	}
}

void Blueprint::removeUnusedTextures() {
	for( LevelId levelId : m_texturesChanged ) {
		if( m_levels.count( levelId ) <= 0 ) continue;

		HashSet<string> usedTextures;
		for( ubyte i = 0; i < 8; i++ ) {
			for( const auto &j : getAreaGeometry( levelId, i ) ) {
				if( j.second.textureInfo.has_value() ) {
					usedTextures.insert( j.second.textureInfo.value().hash );
				}
			}
		}

		const LevelSpecs &level = m_levels.at( levelId );
		std::vector<objectId> objectIds;
		for( const auto &i : level.simpleObjects ) {
			objectIds.push_back( i.first );
		}
		for( const auto &i : level.partialObjects ) {
			objectIds.push_back( i.first );
		}

		for( objectId objectId : objectIds ) {
			for( const auto &i : getObjectGeometry( levelId, objectId ) ) {
				if( i.second.textureInfo.has_value() ) {
					usedTextures.insert( i.second.textureInfo.value().hash );
				}
			}
		}

		std::vector<fs::path> texturesToDelete;
		for( const fs::directory_entry &f : fs::directory_iterator( getLevelPath( levelId ) ) ) {
			if( f.status().type() != fs::file_type::regular ) continue;
			if( f.path().extension().u8string() != ".png" ) continue;

			if( usedTextures.count( f.path().stem().u8string() ) <= 0 ) {
				texturesToDelete.push_back( f.path() );
			}
		}

		for( const fs::path &texture : texturesToDelete ) {
			fs::forceDelete( texture );
		}
	}
	m_texturesChanged.clear();
}

bool Blueprint::levelHasGeometry( LevelId levelId ) const {
	if( m_levels.count( levelId ) == 0 ) {
		return false;
	}

	for( auto i = m_areas.lower_bound( AREA_ID( levelId, 0 ) ); i != m_areas.end() && i->first <= AREA_ID( levelId, 7 ); i++ ) {
		if( areaHasGeometry( levelId, i->first & 7 ) ) {
			return true;
		}
	}

	return false;
}

fs::path Blueprint::getAsmFilePath() const {
	if( m_collabMode ) {
		return m_filePath / (m_filePath.stem().u8string() + ".asm");
	} else {
		return fs::path( m_filePath ).replace_extension( ".asm" );
	}
}

fs::path Blueprint::getAsmDirectory() const {
	if( m_collabMode ) {
		return m_filePath / "asm";
	} else {
		return m_filePath.parent_path() / ( m_filePath.stem().u8string() + "-asm" );
	}
}

void Blueprint::switchToCollabMode( const fs::path &folderPath ) {
	fs::create_directories( folderPath );
	const fs::path oldPath = m_filePath;
	m_filePath = folderPath;
	const string bpcPath = (folderPath / (folderPath.stem().u8string() + ".bpc")).u8string();
	std::fstream( bpcPath, mode::write ).close();

	const fs::path oldAsmFolder = getAsmDirectory();
	const fs::path oldAsmFile = getAsmFilePath();

	m_collabMode = true;
	m_romPath = (m_romPath.empty() ? folderPath : m_romPath).stem();
	m_romPath += ".z64";
	if( fs::exists( oldAsmFolder ) ) {
		fs::rename( oldAsmFolder, getAsmDirectory() );
	}
	if( fs::exists( oldAsmFile ) ) {
		fs::rename( oldAsmFile, getAsmFilePath() );
	}

	save();
	RecentProjects::remove( oldPath.u8string() );
	RecentProjects::push( bpcPath );
	fs::forceDelete( oldPath.u8string() );
}

void Blueprint::switchToNormalMode( const fs::path &blueprintPath ) {
	m_filePath = blueprintPath;

	const fs::path oldAsmFolder = getAsmDirectory();
	const fs::path oldAsmFile = getAsmFilePath();

	m_collabMode = false;
	m_romPath = m_romPath.empty() ? fs::path( blueprintPath ).replace_extension( ".z64" ) : (blueprintPath.parent_path() / m_romPath);
	if( fs::exists( oldAsmFolder ) ) {
		fs::forceDeleteRecursive( getAsmDirectory() );
		fs::copy( oldAsmFolder, getAsmDirectory(), fs::copy_options::recursive );
	}
	if( fs::exists( oldAsmFile ) ) {
		fs::tryCopyFileOverwrite( oldAsmFile, getAsmFilePath() );
	}

	save();
	RecentProjects::push( blueprintPath.u8string() );
}

string Blueprint::targetRomName() const {
	return m_collabMode ? m_romPath.u8string() : m_romPath.filename().u8string();
}

fs::path Blueprint::targetRomPath() const {
	return m_collabMode ? m_filePath / m_romPath : m_romPath;
}

void Blueprint::generateModulesInternal( RomMetadata &metadata ) const {
	AsmModuleBuilder::generate(
		m_modules,
		m_customActions,
		getCameraModules(),
		fs::to_path( m_tempDir ) / "modules",
		getAsmDirectory() / "modules"
	);

	metadata.moduleRegions.clear();
	for( const auto &i : m_modules ) {
		for( const AbsoluteAsmFile &globalAsm : i.second.definition.globalFiles ) {
			metadata.moduleRegions.push_back({
				globalAsm.romStart,
				globalAsm.romEnd
			});
		}
		for( const AsmModuleHook &hook : i.second.definition.hooks ) {
			if( hook.type != ModuleHook::Custom ) continue;
			metadata.moduleRegions.push_back({
				hook.shimAddress,
				hook.shimAddress + 4
			});
		}
	}
}

void Blueprint::revertModuleRegions( std::ostream &rom, RomMetadata &metadata ) const {
	for( const std::pair<uint,uint> &region : metadata.moduleRegions ) {
		if( region.first >= region.second ) continue;
		rom.seekp( region.first );
		BaseRom::revert( rom, region.second - region.first );
	}
}

std::map<Uuid,CustomCamera> Blueprint::getCameraModules() const {
	short cameraId = 0x12;
	std::map<Uuid,CustomCamera> cameraModules;
	for( const auto &i : m_modules ) {
		for( const CameraModule &camera : i.second.definition.cameraModes ) {
			cameraModules[camera.id] = {
				cameraId++,
				&camera,
				i.first
			};
		}
	}
	return cameraModules;
}
