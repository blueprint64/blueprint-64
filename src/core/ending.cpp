#include "src/core/ending.hpp"

#include <fstream>
#include <algorithm>
#include <QImage>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/openmode.hpp"
#include "src/core/baserom.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/level-writer/f3d-macros.hpp"

using namespace EndScreen;

static constexpr uint SEGMENT_START = 0x0399C000 + (21u * 0x20140);
static constexpr uint SEGMENT_SIZE = (3 * 0x20140);
static constexpr uint SEGMENT_END = SEGMENT_START + SEGMENT_SIZE;
static constexpr uint TEXTURE_DATA_SIZE = 0x56000;
static constexpr uint TEXTURE_DATA_START = SEGMENT_END - TEXTURE_DATA_SIZE;

bool EndScreen::saveImage( const string &imgPath, [[maybe_unused]] ResizeMethod resizeMethod ) {
	QImage img( imgPath.c_str() );
	if( img.isNull() ) {
		return false;
	}

	if( img.width() != 320 || img.height() != 240 ) {
		//TODO: resize method
		img = img.scaled( 320, 240, Qt::IgnoreAspectRatio );
	}

	return img.save( Blueprint::currentEditable()->getEndScreenPath().u8string().c_str(), "PNG" );
}

void EndScreen::removeImage() {
	fs::forceDelete( Blueprint::currentEditable()->getEndScreenPath() );
}

bool EndScreen::hasCustomImage() {
	return fs::exists( Blueprint::current()->getEndScreenPath() );
}

struct Vertex { ubyte data[16]; };
struct Quad { Vertex data[4]; };
static_assert( sizeof( Vertex ) == 16 );
static_assert( sizeof( Quad ) == 64 );

static constexpr Vertex makeVertex( ushort x, ushort y, ushort u, ushort v ) {
	return Vertex{{
		(ubyte)(x >> 8), (ubyte)(x & 0xFF),
		(ubyte)(y >> 8), (ubyte)(y & 0xFF),
		0xFF, 0xFF,
		0x00, 0x00,
		(ubyte)(u >> 8), (ubyte)(u & 0xFF),
		(ubyte)(v >> 8), (ubyte)(v & 0xFF),
		0xFF, 0xFF, 0xFF, 0xFF
	}};
}

static constexpr Quad makeQuadStandard( ushort x, ushort y ) {
	return Quad{{
		makeVertex( x, y, 1 << 5, 1 << 5 ),
		makeVertex( x + 30, y, 31 << 5,  1 << 5 ),
		makeVertex( x + 30, y + 30, 31 << 5, 31 << 5 ),
		makeVertex( x, y + 30, 1 << 5, 31 << 5 ),
	}};
}

static constexpr Quad makeQuadThin( ushort y ) {
	return Quad{{
		makeVertex( 300, y, 1 << 5, 2 << 5 ),
		makeVertex( 314, y, 15 << 5, 2 << 5 ),
		makeVertex( 314, y + 60, 15 << 5, 62 << 5 ),
		makeVertex( 300, y + 60, 1 << 5, 62 << 5 ),
	}};
}

static constexpr Quad makeQuadVeryThin( ushort y ) {
	return Quad{{
		makeVertex( 314, y, 1 << 5, 4 << 5 ),
		makeVertex( 320, y, 7 << 5, 4 << 5 ),
		makeVertex( 320, y + 120, 7 << 5, 124 << 5 ),
		makeVertex( 314, y + 120, 1 << 5, 124 << 5 ),
	}};
}

#define makeStandardColumn( i ) \
	makeQuadStandard( i * 30, 0 ), \
	makeQuadStandard( i * 30, 30 ), \
	makeQuadStandard( i * 30, 60 ), \
	makeQuadStandard( i * 30, 90 ), \
	makeQuadStandard( i * 30, 120 ), \
	makeQuadStandard( i * 30, 150 ), \
	makeQuadStandard( i * 30, 180 ), \
	makeQuadStandard( i * 30, 210 )

static const Quad s_vertexData[] = {
	makeStandardColumn( 0 ),
	makeStandardColumn( 1 ),
	makeStandardColumn( 2 ),
	makeStandardColumn( 3 ),
	makeStandardColumn( 4 ),
	makeStandardColumn( 5 ),
	makeStandardColumn( 6 ),
	makeStandardColumn( 7 ),
	makeStandardColumn( 8 ),
	makeStandardColumn( 9 ),
	makeQuadThin( 0 ),
	makeQuadThin( 60 ),
	makeQuadThin( 120 ),
	makeQuadThin( 180 ),
	makeQuadVeryThin( 0 ),
	makeQuadVeryThin( 120 )
};

#undef makeStandardColumn

static constexpr uint VERTEX_DATA_START = TEXTURE_DATA_START - sizeof( s_vertexData );
static constexpr uint MAX_DL_SIZE = VERTEX_DATA_START - SEGMENT_START;
static_assert( MAX_DL_SIZE > 0 );

static inline void writePixel( std::ostream &rom, uint pixel ) {
	const uint data = htonl( (pixel << 8) | 0xFF );
	rom.write( (const char*)&data, 4 );
}

#define PIXEL_AT( x, y ) pixels[((239 - y) * 320) + x]

void EndScreen::import( std::ostream &rom ) {
	QImage img( Blueprint::current()->getEndScreenPath().u8string().c_str(), "PNG" );
	if( img.isNull() || img.width() != 320 || img.height() != 240 ) {
		throw BlueprintLoadError( "Missing background image file." );
		return;
	}

	rom.seekp( 0x4AC4B8 );
	writeWords( rom, { SEGMENT_START, SEGMENT_END });

	rom.seekp( 0x8D977 );
	rom.put( 0 );
	writeWordRaw( rom, 0 );

	rom.seekp( SEGMENT_START );

	RDP_PIPE_SYNC
	CLEAR_GEOMETRY_FLAGS( G_LIGHTING )
	SET_COMBINE( COMBINER_TEXTURE_ONLY_OPAQUE )
	SET_RENDER_MODE( 0x8840F, 0x2240F )
	SET_CYCLE_MODE( G_CYC_1_CYCLE )
	SET_TEXTURE_SCALE( 0xFFFF, 0xFFFF, true );

	writeWordRaw( rom, 0xBA000C02_be32 );
	if( Blueprint::current()->endScreenHasInterpolation() ) {
		writeWordRaw( rom, 0x00002000_be32 );
	} else {
		writeWordRaw( rom, 0x00000000_be32 );
	}

	SegmentedPointer vertexPtr = 0x07000000u + (VERTEX_DATA_START - SEGMENT_START);
	SegmentedPointer texturePtr = 0x07000000u + (TEXTURE_DATA_START - SEGMENT_START);

	SET_TILE_SIZE( 0, 32, 32 )
	for( int i = 0; i < 10; i++ ) {
		for( int j = 0; j < 8; j++ ) {
			if( j % 4 == 0 ) {
				LOAD_VTX( 16, vertexPtr )
				vertexPtr += 0x100;
			}
			LOAD_TEXTURE( rom, texturePtr, TextureFormat::RGBA32, 32, 32, TextureWrapping::Repeat, TextureWrapping::Repeat );
			texturePtr += 0x1000;
			const int v = (j << 2) & 0xF;
			const int vi1[] = { v, v+1, v+2 };
			const int vi2[] = { v, v+2, v+3 };
			RENDER_TRI( vi1 );
			RENDER_TRI( vi2 );
		}
	}

	LOAD_VTX( 16, vertexPtr );
	vertexPtr += 0x100;

	for( int i = 0; i < 4; i++ ) {
		LOAD_TEXTURE( rom, texturePtr, TextureFormat::RGBA32, 16, 64, TextureWrapping::Repeat, TextureWrapping::Repeat );
		texturePtr += 0x1000;
		const int v = i << 2;
		const int vi1[] = { v, v+1, v+2 };
		const int vi2[] = { v, v+2, v+3 };
		RENDER_TRI( vi1 );
		RENDER_TRI( vi2 );
	}

	LOAD_VTX( 8, vertexPtr );
	for( int i = 0; i < 2; i++ ) {
		LOAD_TEXTURE( rom, texturePtr, TextureFormat::RGBA32, 8, 128, TextureWrapping::Repeat, TextureWrapping::Repeat );
		texturePtr += 0x1000;
		const int v = i << 2;
		const int vi1[] = { v, v+1, v+2 };
		const int vi2[] = { v, v+2, v+3 };
		RENDER_TRI( vi1 );
		RENDER_TRI( vi2 );
	}

	RDP_PIPE_SYNC
	writeRaw<uint>( rom, { 0xD7000000_be32, 0xFFFFFFFF });
	SET_GEOMETRY_FLAGS( G_LIGHTING )
	SET_COMBINE( COMBINER_SHADE_ONLY )
	SET_RENDER_MODE_2( 0xAA40F )
	END_DL

	assert( (uint)rom.tellp() <= VERTEX_DATA_START );

	rom.seekp( VERTEX_DATA_START );
	rom.write( (const char*)s_vertexData, sizeof( s_vertexData ) );
	assert( (uint)rom.tellp() == TEXTURE_DATA_START );

	const uint *pixels = (const uint*)img.constBits();
	for( int i = 0; i < 10; i++ ) {
		for( int j = 0; j < 8; j++ ) {
			const int x0 = (i * 30) - 1;
			const int y0 = (j * 30) - 1;
			for( int yy = y0; yy < y0 + 32; yy++ ) {
				const int y = std::clamp( yy, 0, 239 );
				writePixel( rom, PIXEL_AT( std::max( x0, 0 ), y ) );
				for( int x = x0 + 1; x < x0 + 32; x++ ) {
					writePixel( rom, PIXEL_AT( x, y ) );
				}
			}
		}
	}

	for( int i = 0; i < 4; i++ ) {
		const int y0 = (i * 60) - 2;
		for( int yy = y0; yy < y0 + 64; yy++ ) {
			const int y = std::clamp( yy, 0, 239 );
			for( int x = 299; x < 315; x++ ) {
				writePixel( rom, PIXEL_AT( x, y ) );
			}
		}
	}

	for( int i = 0; i < 2; i++ ) {
		const int y0 = (i * 120) - 4;
		for( int yy = y0; yy < y0 + 128; yy++ ) {
			const int y = std::clamp( yy, 0, 239 );
			for( int x = 313; x < 320; x++ ) {
				writePixel( rom, PIXEL_AT( x, y ) );
			}
			writePixel( rom, PIXEL_AT( 319, y ) );
		}
	}

	assert( (uint)rom.tellp() == SEGMENT_END );
}

void EndScreen::revert( std::ostream &rom ) {
	rom.seekp( 0x4AC4B8 );
	const uint vanilla[] = { 0x010BA69A_be32, 0x010E19EA_be32 };
	rom.write( (const char*)vanilla, 8 );

	rom.seekp( 0x8D977 );
	BaseRom::revert( rom, 5 );
}
