#ifndef SRC_CORE_DIALOG_HPP_
#define SRC_CORE_DIALOG_HPP_

#include <iostream>
#include "src/core/enums.hpp"

struct DialogEntry {
	string name;
	HAlign halign;
	VAlign valign;
	ushort hoffset;
	ushort voffset;
	ubyte pageSize;
	sbyte soundEffect;
	string text;
};

inline bool operator==( const DialogEntry &a, const DialogEntry &b ) {
	return
		a.name == b.name &&
		a.halign == b.halign &&
		a.valign == b.valign &&
		a.hoffset == b.hoffset &&
		a.voffset == b.voffset &&
		a.pageSize == b.pageSize &&
		a.soundEffect == b.soundEffect &&
		a.text == b.text;
}

inline bool operator!=( const DialogEntry &a, const DialogEntry &b ) {
	return !(a == b);
}

namespace VanillaText {

	const DialogEntry *dialog();
	const string *levelNames();
	const string *starNames();
	void init();

}

enum DialogSideEffect {
	None = 0,
	PlayPuzzleSolvedJingle = 1,
	PlayRaceStartFanfare = 2,
	PlayBossMusic = 3,
	StopBossMusic = 4
};

namespace Text {
	void applyChangesToRom(
		std::iostream &rom,
		const DialogEntry *dialog,
		const string *levelNames,
		const string *starNames,
		const uint *sfxTable
	);

	DialogSideEffect getSideEffect( uint dialogId );
}

#endif /* SRC_CORE_DIALOG_HPP_ */
