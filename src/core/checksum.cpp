#include "src/core/checksum.hpp"

#include <ios>
#include <cstring>
#include "src/polyfill/byte-order.hpp"
#include "src/polyfill/buffer.hpp"

static const ubyte md5Shifts[] = {
	7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
	5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
	4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
	6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
};

static const uint md5Magic[] = {
	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
	0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
	0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
	0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
	0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
	0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391,
};

static void iterateMD5( uint *digest, char *chunk ) {
	uint *M = (uint*)chunk;
	uint A = digest[0];
	uint B = digest[1];
	uint C = digest[2];
	uint D = digest[3];
	for( uint i = 0; i < 64; i++ ) {
		uint F, j;
		if( i < 16 ) {
			F = (B & C) | ( (~B) & D );
			j = i;
		} else if( i < 32 ) {
			F = ( D & B ) | ( (~D) & C );
			j = ( 5*i + 1 ) % 16;
		} else if( i < 48 ) {
			F = B ^ C ^ D;
			j = ( 3*i + 5 ) % 16;
		} else {
			F = C ^ ( B | (~D) );
			j = 7*i % 16;
		}

		F += A + md5Magic[i] + M[j];

		A = D;
		D = C;
		C = B;
		B += ( F << md5Shifts[i] ) | ( F >> ( 32 - md5Shifts[i] ) );
	}

	digest[0] += A;
	digest[1] += B;
	digest[2] += C;
	digest[3] += D;
}


md5sum getChecksumMD5( std::istream &file ) {
	uint digest[] = { 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476 };
	char chunk[64];

	uint64 size = 0;
	while( !file.bad() ) {
		file.read( (char*)&chunk, 64 );
		const std::streamsize bytesRead = file.gcount();
		size += bytesRead << 3;
		if( bytesRead < 0 ) {
			throw std::ios_base::failure( "Failed to read from stream." );
		} else if( bytesRead < 56 ) {
			chunk[bytesRead] = (char)0x80;
			std::memset( &chunk[bytesRead+1], 0, 55 - bytesRead );
			std::memcpy( &chunk[56], &size, 8 );
			iterateMD5( digest, chunk );
			break;
		} else if( bytesRead < 64 ) {
			chunk[bytesRead] = (char)0x80;
			std::memset( &chunk[bytesRead+1], 0, 63 - bytesRead );
			iterateMD5( digest, chunk );
			std::memset( chunk, 0, 56 );
			std::memcpy( &chunk[56], &size, 8 );
			iterateMD5( digest, chunk );
			break;
		} else {
			iterateMD5( digest, chunk );
		}
	}

	if( file.bad() ) {
		throw std::ios_base::failure( "Failed to read from stream." );
	}

	md5sum result;
	std::memcpy( result.data(), &digest, 16 );

	return result;
}

uint64 computeCRC( std::istream &rom ) {

	uint upperCRC, lowerCRC;
	uint A, B, C, D;
	uint word = 0xF8CA4DDC;

	A = B = C = D = upperCRC = lowerCRC = word;

	rom.seekg( 0x1000 );
	Buffer data( 0x100000 );
	rom.read( data.data(), 0x100000 );
	for( uint i = 0; i < 0x100000 / 4; i++ ) {
		word = ntohl( ((uint*)data.data())[i] );
		if( upperCRC + word < upperCRC ) { //overflow
			A++;
		}

		const uint lower5bits = word & 0x1F;
		const uint temp = (word << lower5bits) | (word >> ( 32 - lower5bits ) );
		upperCRC += word;
		B ^= word;
		lowerCRC += temp;
		if( C < word ) {
			C ^= upperCRC ^ word;
		} else {
			C ^= temp;
		}
		D += word ^ lowerCRC;
	}

	upperCRC ^= A ^ B;
	lowerCRC ^= C ^ D;

	return (((uint64)upperCRC) << 32 ) | (uint64)lowerCRC;
}


void updateCRC( std::iostream &rom ) {
	const uint64 crcl = computeCRC( rom );
	uint crc[2] = { htonl( crcl >> 32 ), htonl( crcl & 0xFFFFFFFF ) };

	rom.seekp( 0x10 );
	rom.write( (char*)crc, 8 );
}
