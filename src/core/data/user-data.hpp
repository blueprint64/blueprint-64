#ifndef SRC_CORE_DATA_USER_DATA_HPP_
#define SRC_CORE_DATA_USER_DATA_HPP_

#include "src/core/json.hpp"

enum class LinkType : ubyte {
	None = 0,
	UserData = 1,
	PartialObject = 2
};

struct UserDataLink {
	LinkType linkType;
	uint targetId;
	uint targetOffset;
};

struct UserData {
	uint id;
	string name;
	std::map<uint,UserDataLink> links;
	bool asmRef;
	size_t bytes;
};

namespace JsonSerializer {
	template<> void serialize<UserData>( JsonWriter &jw, const UserData &obj );
	template<> UserData parse<UserData>( const Json &json );
}

#endif /* SRC_CORE_DATA_USER_DATA_HPP_ */
