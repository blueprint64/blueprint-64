#ifndef SRC_CORE_DATA_LEVEL_HPP_
#define SRC_CORE_DATA_LEVEL_HPP_

#include <array>
#include <unordered_map>
#include "src/core/data/object.hpp"
#include "src/core/data/user-data.hpp"
#include "src/core/data/shading.hpp"
#include "src/core/data/pause-warp.hpp"

struct LevelSpecs {

	LevelId id;

	string name;
	string starNames[6];

	struct {
		ubyte area;
		short x;
		short y;
		short z;
		ushort yaw;
	} marioSpawn;

	bool enableStarSelect;
	sbyte objectBanks[3];
	ushort maxSoundDistance;
	sbyte defaultEchoLevel;

	std::array<Vec3f,25> starSpawnLocations;
	std::array<std::vector<Vec3s>,22> waypoints;
	std::map<uint,ShadingSpecs> shadingValues;

	HashMap<objectId,ObjectSpecs> simpleObjects;
	HashMap<objectId,PartialObjectSpecs> partialObjects;
	HashMap<objectId,AdvancedObjectSpecs> advancedObjects;

	HashMap<uint,UserData> userData;

	bool overridePauseWarp;
	PauseWarp pauseWarp;

	static const LevelSpecs &Default();

};

namespace JsonSerializer {
	template<> void serialize<LevelSpecs>( JsonWriter &jw, const LevelSpecs &obj );
	template<> LevelSpecs parse<LevelSpecs>( const Json &json );
}


#endif /* SRC_CORE_DATA_LEVEL_HPP_ */
