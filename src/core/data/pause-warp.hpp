#ifndef SRC_CORE_DATA_PAUSE_WARP_HPP_
#define SRC_CORE_DATA_PAUSE_WARP_HPP_

#include "src/core/enums.hpp"
#include "src/core/json.hpp"

struct PauseWarp {
	LevelId level;
	ubyte area;
	ubyte warpId;

	static const PauseWarp &Default();
};

namespace JsonSerializer {
	template<> void serialize<PauseWarp>( JsonWriter &jw, const PauseWarp &obj );
	template<> PauseWarp parse<PauseWarp>( const Json &json );
}

#endif /* SRC_CORE_DATA_PAUSE_WARP_HPP_ */
