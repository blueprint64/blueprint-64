#include "src/core/data/pause-warp.hpp"

const PauseWarp &PauseWarp::Default() {
	static const PauseWarp s_default = {
		/* level */ LevelId::CastleInterior,
		/* area */ 1,
		/* warpId */ 31
	};
	return s_default;
};

static constexpr char P_LEVEL[] = "level";
static constexpr char P_AREA[] = "area";
static constexpr char P_WARP[] = "warp_id";

template<> void JsonSerializer::serialize<PauseWarp>( JsonWriter &jw, const PauseWarp &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_LEVEL, obj.level );
	jw.writeProperty( P_AREA, obj.area );
	jw.writeProperty( P_WARP, obj.warpId );
	jw.writeObjectEnd();
}

template<> PauseWarp JsonSerializer::parse<PauseWarp>( const Json &json ) {
	return PauseWarp {
		json[P_LEVEL].get<LevelId>(),
		json[P_AREA].get<ubyte>(),
		json[P_WARP].get<ubyte>()
	};
}
