#include "src/core/data/fog.hpp"

static constexpr char P_START[] = "fade_start";
static constexpr char P_END[] = "fade_end";
static constexpr char P_COLOUR[] = "colour";
static constexpr char P_ENABLED[] = "enabled";

template<> void JsonSerializer::serialize<FogSpecs>( JsonWriter &jw, const FogSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_START, obj.fadeStartDistance );
	jw.writeProperty( P_END, obj.fadeEndDistance );
	jw.writeProperty( P_COLOUR, obj.colour );
	jw.writeProperty( P_ENABLED, obj.enabled );
	jw.writeObjectEnd();
}

template<> FogSpecs JsonSerializer::parse<FogSpecs>( const Json &json ) {
	return FogSpecs {
		json[P_START].get<double>(),
		json[P_END].get<double>(),
		json[P_COLOUR].get<ColourRGB24>(),
		json[P_ENABLED].get<bool>()
	};
}
