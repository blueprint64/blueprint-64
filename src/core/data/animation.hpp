#ifndef SRC_CORE_DATA_ANIMATION_HPP_
#define SRC_CORE_DATA_ANIMATION_HPP_

#include <variant>
#include "src/core/json.hpp"

struct NullAnimationSpecs {};

struct LinearAnimationSpecs {
	int speedX;
	int speedY;
};

struct SineWaveAnimationSpecs {
	ushort angle;
	ushort period;
	ushort amplitude;
};

typedef std::variant<NullAnimationSpecs, LinearAnimationSpecs,SineWaveAnimationSpecs> AnimationSpecs;

namespace JsonSerializer {
	template<> void serialize<AnimationSpecs>( JsonWriter &jw, const AnimationSpecs &obj );
	template<> AnimationSpecs parse<AnimationSpecs>( const Json &json );
}


#endif /* SRC_CORE_DATA_ANIMATION_HPP_ */
