#include "src/core/data/animation.hpp"

#include "src/core/exceptions.hpp"

static constexpr char P_TYPE[] = "type";
static constexpr char P_SPEED_X[] = "speed_x";
static constexpr char P_SPEED_Y[] = "speed_y";
static constexpr char P_AMPLITUDE[] = "amplitude";
static constexpr char P_ANGLE[] = "angle";
static constexpr char P_PERIOD[] = "period";

template<> void JsonSerializer::serialize<AnimationSpecs>( JsonWriter &jw, const AnimationSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_TYPE, (int)obj.index() );
	switch( (int)obj.index() ) {
		case 1: {
			const LinearAnimationSpecs &props = std::get<LinearAnimationSpecs>( obj );
			jw.writeProperty( P_SPEED_X, props.speedX );
			jw.writeProperty( P_SPEED_Y, props.speedY );
			break;
		}
		case 2: {
			const SineWaveAnimationSpecs &props = std::get<SineWaveAnimationSpecs>( obj );
			jw.writeProperty( P_AMPLITUDE, props.amplitude );
			jw.writeProperty( P_ANGLE, props.angle );
			jw.writeProperty( P_PERIOD, props.period );
			break;
		}
		default: break;
	}
	jw.writeObjectEnd();
}

template<> AnimationSpecs JsonSerializer::parse<AnimationSpecs>( const Json &json ) {
	switch( json[P_TYPE].get<int>() ) {
		case 0: return NullAnimationSpecs();
		case 1: return LinearAnimationSpecs{
			json[P_SPEED_X].get<int>(),
			json[P_SPEED_Y].get<int>()
		};
		case 2: return SineWaveAnimationSpecs{
			json[P_ANGLE].get<ushort>(),
			json[P_PERIOD].get<ushort>(),
			json[P_AMPLITUDE].get<ushort>()
		};
		default: throw FileParserException( "Unknown animation type" );
	}
}
