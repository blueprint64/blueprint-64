#ifndef SRC_CORE_DATA_OBJECT_HPP_
#define SRC_CORE_DATA_OBJECT_HPP_

#include "src/core/enums.hpp"
#include "src/core/data/fog.hpp"
#include "src/core/data/behaviour.hpp"
#include "src/core/objectid.hpp"

struct ObjectSpecs {

	ubyte modelId;
	string name;

	struct {
		bool autoCompute;
		bool alwaysUpright;
		float maxScale;
		short maxFOV;
		ushort radius;
	} cullingRadius;

	struct {
		ShadowType shape;
		ushort size;
		ubyte opacity;
		bool enabled;
	} shadow;

	FogSpecs fog;

	bool useObjectAlpha;

	bool hasCollision;
	Behaviour behaviour;

	// These properties are cached from the model data and used to auto-compute the culling radius
	ushort max_xz;
	ushort max_xyz;

	static const ObjectSpecs &Default();

};

struct PartialObjectSpecs {
	string name;
	bool useObjectAlpha;
};

struct AdvancedObjectSpecs {
	ubyte modelId;
	string name;
	uint customDataId;
};

struct GenericObjectSpecs {
	objectId id;
	std::variant<PartialObjectSpecs,AdvancedObjectSpecs,ObjectSpecs> data;
};

struct ObjectSpecsRef {
	objectId id;
	union {
		const PartialObjectSpecs *partial;
		const AdvancedObjectSpecs *advanced;
		const ObjectSpecs *standard;
	} dataRef;
};

namespace JsonSerializer {
	template<> void serialize<ObjectSpecsRef>( JsonWriter &jw, const ObjectSpecsRef &obj );
	template<> void serialize<GenericObjectSpecs>( JsonWriter &jw, const GenericObjectSpecs &obj );
	template<> GenericObjectSpecs parse<GenericObjectSpecs>( const Json &json );
}

#endif /* SRC_CORE_DATA_OBJECT_HPP_ */
