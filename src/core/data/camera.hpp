#ifndef SRC_CORE_DATA_CAMERA_HPP_
#define SRC_CORE_DATA_CAMERA_HPP_

#include "src/core/enums.hpp"
#include "src/core/uuid.hpp"
#include "src/core/vector.hpp"

struct VanillaCameraOptions {
	CameraPreset mode;
	short frames;
	union {
		ushort baseAngle; // For 8-way camera
		uint behaviour; // For boss camera
		float centre[2]; // For radial camera
		short pivot[3]; // For elastic pivot camera
	} ext;
};

struct CustomCameraOptions {
	string functionLabel;
};

struct CameraModulePropertyValue {
	ModulePropertyType type;
	union {
		float floatValue;
		int signedValue;
		uint unsignedValue;
		bool booleanValue;
	};
};

struct ModuleCameraOptions {
	Uuid moduleId;
	short frames;
	std::map<string,CameraModulePropertyValue> moduleParams;
};

typedef std::variant<VanillaCameraOptions,CustomCameraOptions,ModuleCameraOptions> CameraOptions;

struct CameraTrigger {
	string name;
	CameraTriggerType type;
	Vec3s centre;
	Vec3s radius;
	ushort angle;
	CameraOptions options;
};

namespace JsonSerializer {
	template<> void serialize<CameraTrigger>( JsonWriter &jw, const CameraTrigger &obj );
	template<> CameraTrigger parse<CameraTrigger>( const Json &json );
}


#endif /* SRC_CORE_DATA_CAMERA_HPP_ */
