#include "src/core/data/material.hpp"

const MaterialSpecs &MaterialSpecs::Default() {
	static const MaterialSpecs s_default = {
		/* visible */ false,
		/* solid */ true,
		/* opacity */ 0xFF,
		/* removedTextureAlpha */ false,
		/* layer */ DrawingLayer::Opaque,
		/* format */ TextureFormat::RGBA16,
		/* filtering */ TextureFiltering::Bilinear,
		/* ditheredAlpha */ false,
		/* doubleSided */ false,
		/* reflection */ false,
		/* hwrap */ TextureWrapping::Repeat,
		/* vwrap */ TextureWrapping::Repeat,
		/* shadingRef */ 0,
		/* collision */ {
			/* type */ CollisionType::Normal,
			/* direction */ 0,
			/* force */ ForceIntensity::Medium
		},
		/* textureInfo */ std::nullopt,
		/* animation */ AnimationSpecs(),
		/* asmRefs */ {
			/* texData */ false,
			/* texPtr */ false,
			/* shadePtr */ false,
			/* opacity */ false
		}
	};
	return s_default;
};

static constexpr char P_VISIBLE[] = "visible";
static constexpr char P_SOLID[] = "solid";
static constexpr char P_OPACITY[] = "opacity";
static constexpr char P_REMOVED_ALPHA[] = "removed_alpha";
static constexpr char P_LAYER[] = "layer";
static constexpr char P_FORMAT[] = "format";
static constexpr char P_FILTERING[] = "filtering";
static constexpr char P_DITHERED[] = "dithered_alpha";
static constexpr char P_DOUBLE_SIDED[] = "double_sided";
static constexpr char P_REFLECTION[] = "reflection";
static constexpr char P_HWRAP[] = "hwrap";
static constexpr char P_VWRAP[] = "vwrap";
static constexpr char P_SHADE[] = "shade";
static constexpr char P_COLLISION[] = "collision";
static constexpr char P_TYPE[] = "type";
static constexpr char P_DIRECTION[] = "direction";
static constexpr char P_FORCE[] = "force";
static constexpr char P_TEX_INFO[] = "texture_info";
static constexpr char P_ASM_REF[] = "asm_refs";
static constexpr char P_TEX_DATA[] = "texture_data";
static constexpr char P_TEX_PTR[] = "texture_ptr";
static constexpr char P_SHADE_PTR[] = "shade_ptr";
static constexpr char P_OPACITY_DATA[] = "opacity_data";
static constexpr char P_ANIMATION[] = "animation";

template<> void JsonSerializer::serialize<MaterialSpecs>( JsonWriter &jw, const MaterialSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_VISIBLE, obj.visible );
	jw.writeProperty( P_SOLID, obj.solid );
	jw.writeProperty( P_OPACITY, obj.opacity );
	jw.writeProperty( P_REMOVED_ALPHA, obj.removedTextureAlpha );
	jw.writeProperty( P_LAYER, obj.layer );
	jw.writeProperty( P_FORMAT, obj.format );
	jw.writeProperty( P_FILTERING, obj.filtering );
	jw.writeProperty( P_DITHERED, obj.ditheredAlpha );
	jw.writeProperty( P_DOUBLE_SIDED, obj.doubleSided );
	jw.writeProperty( P_REFLECTION, obj.reflection );
	jw.writeProperty( P_HWRAP, obj.hwrap );
	jw.writeProperty( P_VWRAP, obj.vwrap );
	jw.writeProperty( P_SHADE, obj.shadingRef );
	jw.writePropertyName( P_COLLISION );
	jw.writeObjectStart();
		jw.writeProperty( P_TYPE, obj.collision.type );
		jw.writeProperty( P_DIRECTION, obj.collision.direction );
		jw.writeProperty( P_FORCE, obj.collision.force );
	jw.writeObjectEnd();
	jw.writePropertyName( P_TEX_INFO );
	if( obj.textureInfo.has_value() ) {
		serialize( jw, obj.textureInfo.value() );
	} else {
		jw.writeNull();
	}
	jw.writePropertyName( P_ASM_REF );
	jw.writeObjectStart();
		jw.writeProperty( P_TEX_DATA, obj.asmRefs.texData );
		jw.writeProperty( P_TEX_PTR, obj.asmRefs.texPtr );
		jw.writeProperty( P_SHADE_PTR, obj.asmRefs.shadePtr );
		jw.writeProperty( P_OPACITY_DATA, obj.asmRefs.opacity );
	jw.writeObjectEnd();
	jw.writeProperty( P_ANIMATION, obj.animation );
	jw.writeObjectEnd();
}

template<> MaterialSpecs JsonSerializer::parse<MaterialSpecs>( const Json &json ) {
	std::optional<TextureInfo> textureInfo;
	if( json[P_TEX_INFO].hasValue() && !json[P_TEX_INFO].isNull() ) {
		textureInfo = json[P_TEX_INFO].get<TextureInfo>();
	}

	return MaterialSpecs {
		json[P_VISIBLE].get<bool>(),
		json[P_SOLID].get<bool>(),
		json[P_OPACITY].getOrDefault<ubyte>( MaterialSpecs::Default().opacity ),
		json[P_REMOVED_ALPHA].getOrDefault<bool>( MaterialSpecs::Default().removedTextureAlpha ),
		json[P_LAYER].get<DrawingLayer>(),
		json[P_FORMAT].get<TextureFormat>(),
		json[P_FILTERING].getOrDefault<TextureFiltering>( MaterialSpecs::Default().filtering ),
		json[P_DITHERED].getOrDefault<bool>( MaterialSpecs::Default().ditheredAlpha ),
		json[P_DOUBLE_SIDED].getOrDefault<bool>( MaterialSpecs::Default().doubleSided ),
		json[P_REFLECTION].getOrDefault<bool>( MaterialSpecs::Default().reflection ),
		json[P_HWRAP].get<TextureWrapping>(),
		json[P_VWRAP].get<TextureWrapping>(),
		json[P_SHADE].get<uint>(),
		{
			json[P_COLLISION][P_TYPE].get<CollisionType>(),
			json[P_COLLISION][P_DIRECTION].getOrDefault<ubyte>( MaterialSpecs::Default().collision.direction ),
			json[P_COLLISION][P_FORCE].getOrDefault<ForceIntensity>( MaterialSpecs::Default().collision.force )
		},
		std::move( textureInfo ),
		json[P_ANIMATION].getOrDefault<AnimationSpecs>( MaterialSpecs::Default().animation ),
		{
			json[P_ASM_REF][P_TEX_DATA].get<bool>(),
			json[P_ASM_REF][P_TEX_PTR].get<bool>(),
			json[P_ASM_REF][P_SHADE_PTR].get<bool>(),
			json[P_ASM_REF][P_OPACITY_DATA].get<bool>()
		}
	};
}
