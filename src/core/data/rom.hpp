#ifndef SRC_CORE_DATA_ROM_HPP_
#define SRC_CORE_DATA_ROM_HPP_

#include "src/core/data/pause-warp.hpp"

struct RomSpecs {

	string internalName;
	bool enableChecksum;
	LevelBoundarySize levelBoundaries;
	LevelId startingLevel;
	float worldScale;
	bool expandAudioHeap;
	bool cutoutDecal;
	PauseWarp pauseWarp;

	static const RomSpecs &Default();

};

namespace JsonSerializer {
	template<> void serialize<RomSpecs>( JsonWriter &jw, const RomSpecs &obj );
	template<> RomSpecs parse<RomSpecs>( const Json &json );
}

#endif /* SRC_CORE_DATA_ROM_HPP_ */
