#include "src/core/data/terrain.hpp"

const std::array<TerrainSoundPack,7> &TerrainSoundPack::Default() {
	static const std::array<TerrainSoundPack,7> s_default = {{
		{
			"Grass",
			TerrainSound::Default,
			TerrainSound::Stone,
			TerrainSound::Grass,
			TerrainSound::Grass,
			TerrainSound::Grass,
			TerrainSound::Default
		},
		{
			"Stone",
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Grass,
			TerrainSound::Grass
		},
		{
			"Snow",
			TerrainSound::Snow,
			TerrainSound::Ice,
			TerrainSound::Snow,
			TerrainSound::Ice,
			TerrainSound::Stone,
			TerrainSound::Stone
			// Additional Effect: Mario gets stuck in non-hard terrain types instead of taking fall damage
			// Additional Effect: Mario shivers when idle instead of falling asleep
			// Additional Effect: Lava does not cause fire/smoke particles
			// Additional Effect: Lose health faster in water and don't recover health at water surface
		},
		{
			"Sand",
			TerrainSound::Sand,
			TerrainSound::Stone,
			TerrainSound::Sand,
			TerrainSound::Sand,
			TerrainSound::Stone,
			TerrainSound::Stone
			// Additional Effect: Mario gets stuck in non-hard terrain types instead of taking fall damage
		},
		{
			"Spooky",
			TerrainSound::Creaking,
			TerrainSound::Creaking,
			TerrainSound::Creaking,
			TerrainSound::Creaking,
			TerrainSound::Stone,
			TerrainSound::Stone
		},
		{
			"Water",
			TerrainSound::Default,
			TerrainSound::Stone,
			TerrainSound::Grass,
			TerrainSound::Ice,
			TerrainSound::Stone,
			TerrainSound::Ice
		},
		{
			"Slide",
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Stone,
			TerrainSound::Ice,
			TerrainSound::Ice
			// Additional Effect: Everything is very slippery!
		}
	}};
	return s_default;
};

static constexpr char P_NAME[] = "name";
static constexpr char P_NORMAL[] = "normal";
static constexpr char P_HARD[] = "hard";
static constexpr char P_SLIP[] = "slippery";
static constexpr char P_SLIDE[] = "slide";
static constexpr char P_ALT[] = "alternate";
static constexpr char P_ALT_SLIP[] = "alt_slippery";

template<> void JsonSerializer::serialize<TerrainSoundPack>( JsonWriter &jw, const TerrainSoundPack &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_NORMAL, obj.normal );
	jw.writeProperty( P_HARD, obj.hard );
	jw.writeProperty( P_SLIP, obj.slippery );
	jw.writeProperty( P_SLIDE, obj.slide );
	jw.writeProperty( P_ALT, obj.alternate );
	jw.writeProperty( P_ALT_SLIP, obj.slipperyAlternate );
	jw.writeObjectEnd();
}

template<> TerrainSoundPack JsonSerializer::parse<TerrainSoundPack>( const Json &json ) {
	return TerrainSoundPack {
		json[P_NAME].get<string>(),
		json[P_NORMAL].get<TerrainSound>(),
		json[P_HARD].get<TerrainSound>(),
		json[P_SLIP].get<TerrainSound>(),
		json[P_SLIDE].get<TerrainSound>(),
		json[P_ALT].get<TerrainSound>(),
		json[P_ALT_SLIP].get<TerrainSound>()
	};
}
