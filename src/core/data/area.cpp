#include "src/core/data/area.hpp"

#include <cstring>
#include "src/core/migration.hpp"
#include "src/core/serialization-helpers.hpp"

const AreaSpecs &AreaSpecs::Default() {
	static const AreaSpecs s_default = {
		/* areaId */ 0,
		/* backgroundImage */ BackgroundImage::None,
		/* backgroundColour */ { ColourRGBA5551( 0, 0, 0 ) },
		/* pauseCamera */ {
			/* position */ { 0, 2048, -16384 },
			/* focus */ { 0, 0, 0 },
			/* fixed */ false
		},
		/* defaultCamera */ CameraTrigger{
			"default",
			CameraTriggerType::Vanilla,
			{ 0, 0, 0 },
			{ 0, 0, 0 },
			0,
			VanillaCameraOptions{
				CameraPreset::SemiCardinal,
				0,
				{ 0 }
			}
		},
		/* overrideFOV */ false,
		/* fieldOfView */ 45,
		/* nearClippingPlane */ 128,
		/* farClippingPlane */ 0x7FFF,
		/* fog */ {
			/* fadeStartDistance */ 0.980,
			/* fadeEndDistance */ 1.0,
			/* colour */ ColourRGB24( 0, 0, 0 ),
			/* enabled */ false
		},
		/* environmentEffect */ EnvironmentEffect::None,
		/* terrainType */ 1,
		/* echo level */ 0x08,
		/* music */ 0,
		/* numObjects */ 100,
		/* numWarps */ 8,
		/* numInstantWarps */ 4,
		/* autoDeathFloor */ false,
		/* voidHeight */ -8950,
		/* waterBoxes */ std::vector<WaterBoxSpecs>(),
		/* cameraTriggers */ std::vector<CameraTrigger>()
	};
	return s_default;
};

static constexpr char P_ID[] = "id";
static constexpr char P_BKG_IMAGE[] = "background_image";
static constexpr char P_BKG_COLOUR[] = "background_colour";
static constexpr char P_PAUSE_CAM[] = "pause_camera";
static constexpr char P_POSITION[] = "position";
static constexpr char P_FOCUS[] = "focus";
static constexpr char P_FIXED[] = "fixed";
static constexpr char P_DEFAULT_CAM[] = "default_camera";
static constexpr char P_OVERRIDE_FOV[] = "override_fov";
static constexpr char P_FOV[] = "fov";
static constexpr char P_NEAR[] = "clip_near";
static constexpr char P_FAR[] = "clip_far";
static constexpr char P_FOG[] = "fog";
static constexpr char P_ENVFX[] = "envfx";
static constexpr char P_TERRAIN[] = "terrain";
static constexpr char P_ECHO[] = "echo";
static constexpr char P_MUSIC[] = "music";
static constexpr char P_NUM_OBJ[] = "num_objects";
static constexpr char P_NUM_WARPS[] = "num_warps";
static constexpr char P_NUM_INST_WARPS[] = "num_instant_warps";
static constexpr char P_DEATH_FLOOR[] = "death_floor";
static constexpr char P_VOID_HEIGHT[] = "void_height";
static constexpr char P_WATER_BOXES[] = "water_boxes";
static constexpr char P_CAM_TRIGGERS[] = "camera_triggers";

template<> void JsonSerializer::serialize<AreaSpecs>( JsonWriter &jw, const AreaSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_ID, obj.areaId );
	jw.writeProperty( P_BKG_IMAGE, obj.backgroundImage );
	jw.writeProperty( P_BKG_COLOUR, obj.backgroundColour.toString() );
	jw.writePropertyName( P_PAUSE_CAM );
	jw.writeObjectStart();
		jw.writeProperty( P_POSITION, obj.pauseCamera.position );
		jw.writeProperty( P_FOCUS, obj.pauseCamera.focus );
		jw.writeProperty( P_FIXED, obj.pauseCamera.fixed );
	jw.writeObjectEnd();
	jw.writeProperty( P_DEFAULT_CAM, obj.defaultCamera );
	jw.writeProperty( P_OVERRIDE_FOV, obj.overrideFOV );
	jw.writeProperty( P_FOV, obj.fieldOfView );
	jw.writeProperty( P_NEAR, obj.nearClippingPlane );
	jw.writeProperty( P_FAR, obj.farClippingPlane );
	jw.writeProperty( P_FOG, obj.fog );
	jw.writeProperty( P_ENVFX, obj.environmentEffect );
	jw.writeProperty( P_TERRAIN, obj.terrainType );
	jw.writeProperty( P_ECHO, obj.echoLevel );
	jw.writeProperty( P_MUSIC, obj.music );
	jw.writeProperty( P_NUM_OBJ, obj.numObjects );
	jw.writeProperty( P_NUM_WARPS, obj.numWarps );
	jw.writeProperty( P_NUM_INST_WARPS, obj.numInstantWarps );
	jw.writeProperty( P_DEATH_FLOOR, obj.autoDeathFloor );
	jw.writeProperty( P_VOID_HEIGHT, obj.voidHeight );
	serializeIterator( jw, P_WATER_BOXES, obj.waterBoxes.begin(), obj.waterBoxes.end() );
	serializeIterator( jw, P_CAM_TRIGGERS, obj.cameraTriggers.begin(), obj.cameraTriggers.end() );
	jw.writeObjectEnd();
}

template<> AreaSpecs JsonSerializer::parse<AreaSpecs>( const Json &json ) {
	AreaSpecs areaSpecs = {
		json[P_ID].get<ushort>(),
		json[P_BKG_IMAGE].get<BackgroundImage>(),
		json[P_BKG_COLOUR].get<ColourRGBA5551>(),
		{
			json[P_PAUSE_CAM][P_POSITION].get<Vec3s>(),
			json[P_PAUSE_CAM][P_FOCUS].get<Vec3s>(),
			json[P_PAUSE_CAM][P_FIXED].get<bool>()
		},
		json[P_DEFAULT_CAM].getOrDefault<CameraTrigger>( AreaSpecs::Default().defaultCamera ),
		json[P_OVERRIDE_FOV].getOrDefault<bool>( AreaSpecs::Default().overrideFOV ),
		json[P_FOV].get<short>(),
		json[P_NEAR].get<ushort>(),
		json[P_FAR].get<ushort>(),
		json[P_FOG].get<FogSpecs>(),
		json[P_ENVFX].get<EnvironmentEffect>(),
		json[P_TERRAIN].get<ubyte>(),
		json[P_ECHO].getOrDefault<sbyte>( AreaSpecs::Default().echoLevel ),
		json[P_MUSIC].get<ubyte>(),
		json[P_NUM_OBJ].get<ubyte>(),
		json[P_NUM_WARPS].get<ubyte>(),
		json[P_NUM_INST_WARPS].get<ubyte>(),
		json[P_DEATH_FLOOR].getOrDefault<bool>( AreaSpecs::Default().autoDeathFloor ),
		json[P_VOID_HEIGHT].getOrDefault<short>( AreaSpecs::Default().voidHeight ),
		std::vector<WaterBoxSpecs>(),
		std::vector<CameraTrigger>()
	};

	if( json[P_WATER_BOXES].isArray() ) {
		areaSpecs.waterBoxes = parseVector<WaterBoxSpecs>( json[P_WATER_BOXES] );
	}

	if( json[P_CAM_TRIGGERS].isArray() ) {
		areaSpecs.cameraTriggers = parseVector<CameraTrigger>( json[P_CAM_TRIGGERS] );
	}

	if( json["camera_type"].exists() ) {
		// Migrate from old schema
		VanillaCameraOptions cameraOptions;
		std::memset( &cameraOptions, 1, sizeof( VanillaCameraOptions ) );
		cameraOptions.mode = json["camera_type"].get<CameraPreset>();
		areaSpecs.defaultCamera.type = CameraTriggerType::Vanilla;
		areaSpecs.defaultCamera.options = cameraOptions;
	}

	if( DeserializationContext::schemaVersion() < Version{ 3, 1, 1 } && areaSpecs.echoLevel == 0x28 ) {
		areaSpecs.echoLevel = 0x08;
	}

	return areaSpecs;
}
