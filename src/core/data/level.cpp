#include "src/core/data/level.hpp"

#include "src/core/migration.hpp"
#include "src/core/serialization-helpers.hpp"

const LevelSpecs &LevelSpecs::Default() {
	static const LevelSpecs s_default = {
		/* id */ (LevelId)0,
		/* name */ "",
		/* starNames */ { "", "", "", "", "", "" },
		/* marioSpawn */ { 1, 0, 0, 0, 0 },
		/* enableStarSelect */ false,
		/* objectBanks */ { -1, -1, -1 },
		/* maxSoundDistance */ 19500,
		/* defaultEchoLevel */ 0x08,
		/* starSpawnLocations */ std::array<Vec3f,25>(),
		/* waypoints */ std::array<std::vector<Vec3s>,22>(),
		/* shadingValues */ {{ 0, ShadingSpecs::Default() }},
		/* simpleObjects */ HashMap<objectId,ObjectSpecs>(),
		/* partialObjects */ HashMap<objectId,PartialObjectSpecs>(),
		/* advancedObjects */ HashMap<objectId,AdvancedObjectSpecs>(),
		/* userData */ HashMap<uint,UserData>(),
		/* overridePauseWarp */ false,
		/* pauseWarp */ PauseWarp::Default()
	};
	return s_default;
};

static constexpr char P_ID[] = "id";
static constexpr char P_NAME[] = "name";
static constexpr char P_STAR_NAMES[] = "star_names";
static constexpr char P_STAR_SELECT[] = "star_select";
static constexpr char P_SPAWN[] = "spawn";
static constexpr char P_AREA[] = "area";
static constexpr char P_X[] = "x";
static constexpr char P_Y[] = "y";
static constexpr char P_Z[] = "z";
static constexpr char P_ANGLE[] = "angle";
static constexpr char P_OBJ_BANKS[] = "object_banks";
static constexpr char P_SOUND_DIST[] = "sound_carry_distance";
static constexpr char P_ECHO[] = "echo";
static constexpr char P_SHADING[] = "shading";
static constexpr char P_STAR_SPAWNS[] = "star_spawns";
static constexpr char P_WAYPOINTS[] = "waypoints";
static constexpr char P_OBJECTS[] = "custom_objects";
static constexpr char P_USER_DATA[] = "user_data";
static constexpr char P_PAUSE_WARP[] = "pause_warp";

template<> void JsonSerializer::serialize<LevelSpecs>( JsonWriter &jw, const LevelSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_ID, obj.id );
	jw.writeProperty( P_NAME, obj.name );
	jw.writePropertyName( P_STAR_NAMES );
	jw.writeArrayStart();
	for( int j = 0; j < 6; j++ ) {
		jw.writeString( obj.starNames[j] );
	}
	jw.writeArrayEnd();
	jw.writeProperty( P_STAR_SELECT, obj.enableStarSelect );
	jw.writePropertyName( P_SPAWN );
	jw.writeObjectStart();
		jw.writeProperty( P_AREA, obj.marioSpawn.area );
		jw.writeProperty( P_X, obj.marioSpawn.x );
		jw.writeProperty( P_Y, obj.marioSpawn.y );
		jw.writeProperty( P_Z, obj.marioSpawn.z );
		jw.writeProperty( P_ANGLE, obj.marioSpawn.yaw );
	jw.writeObjectEnd();
	jw.writePropertyName( P_OBJ_BANKS );
	jw.writeArrayStart();
	for( int j = 0; j < 3; j++ ) {
		jw.writeNumber( (int64)obj.objectBanks[j] );
	}
	jw.writeArrayEnd();
	jw.writeProperty( P_SOUND_DIST, obj.maxSoundDistance );
	jw.writeProperty( P_ECHO, obj.defaultEchoLevel );
	serializeDictionaryValues( jw, P_SHADING, obj.shadingValues.begin(), obj.shadingValues.end() );
	jw.writePropertyName( P_STAR_SPAWNS );
	jw.writeArrayStart();
	for( int i = 0; i < 25; i++ ) {
		const Vec3f &location = obj.starSpawnLocations[i];
		if( !location.zero() ) {
			jw.writeObjectStart();
			jw.writeProperty( P_ID, i );
			jw.writeProperty( P_X, location.x );
			jw.writeProperty( P_Y, location.y );
			jw.writeProperty( P_Z, location.z );
			jw.writeObjectEnd();
		}
	}
	jw.writeArrayEnd();
	jw.writePropertyName( P_WAYPOINTS );
	jw.writeArrayStart();
	for( int i = 0; i < 22; i++ ) {
		const std::vector<Vec3s> &waypoints = obj.waypoints[i];
		if( waypoints.empty() || (waypoints.size() == 1 && waypoints[0].zero()) ) continue;
		jw.writeObjectStart();
			jw.writeProperty( P_ID, i );
			jw.writePropertyName( P_WAYPOINTS );
			jw.writeArrayStart();
			for( const Vec3s &waypoint : waypoints ) {
				serialize( jw, waypoint );
			}
			jw.writeArrayEnd();
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();
	jw.writePropertyName( P_OBJECTS );
	jw.writeArrayStart();
	for( const auto &i : obj.simpleObjects ) {
		ObjectSpecsRef objRef;
		objRef.id = i.first;
		objRef.dataRef.standard = &i.second;
		serialize( jw, objRef );
	}
	for( const auto &i : obj.partialObjects ) {
		ObjectSpecsRef objRef;
		objRef.id = i.first;
		objRef.dataRef.partial = &i.second;
		serialize( jw, objRef );
	}
	for( const auto &i : obj.advancedObjects ) {
		ObjectSpecsRef objRef;
		objRef.id = i.first;
		objRef.dataRef.advanced = &i.second;
		serialize( jw, objRef );
	}
	jw.writeArrayEnd();
	serializeDictionaryValues( jw, P_USER_DATA, obj.userData.begin(), obj.userData.end() );
	if( obj.overridePauseWarp ) {
		jw.writeProperty( P_PAUSE_WARP, obj.pauseWarp );
	} else {
		jw.writeProperty( P_PAUSE_WARP, nullptr );
	}
	jw.writeObjectEnd();
}

template<> LevelSpecs JsonSerializer::parse<LevelSpecs>( const Json &json ) {
	const JArray &starNames = json[P_STAR_NAMES].array();
	const JArray &objectBanks = json[P_OBJ_BANKS].array();
	const Json &marioSpawn = json[P_SPAWN];
	LevelSpecs specs = {
		json[P_ID].get<LevelId>(),
		json[P_NAME].get<string>(),
		{
			starNames[0].getOrDefault<string>( "" ),
			starNames[1].getOrDefault<string>( "" ),
			starNames[2].getOrDefault<string>( "" ),
			starNames[3].getOrDefault<string>( "" ),
			starNames[4].getOrDefault<string>( "" ),
			starNames[5].getOrDefault<string>( "" )
		},
		{
			marioSpawn[P_AREA].get<ubyte>(),
			marioSpawn[P_X].get<short>(),
			marioSpawn[P_Y].get<short>(),
			marioSpawn[P_Z].get<short>(),
			(ushort)marioSpawn[P_ANGLE].get<float>()
		},
		json[P_STAR_SELECT].getOrDefault<bool>( LevelSpecs::Default().enableStarSelect ),
		{
			objectBanks[0].get<sbyte>(),
			objectBanks[1].get<sbyte>(),
			objectBanks[2].get<sbyte>()
		},
		json[P_SOUND_DIST].getOrDefault<ushort>( LevelSpecs::Default().maxSoundDistance ),
		json[P_ECHO].getOrDefault<sbyte>( LevelSpecs::Default().defaultEchoLevel ),
		std::array<Vec3f,25>(),
		std::array<std::vector<Vec3s>,22>(),
		parseMap<uint,ShadingSpecs>( json[P_SHADING] ),
		HashMap<objectId,ObjectSpecs>(),
		HashMap<objectId,PartialObjectSpecs>(),
		HashMap<objectId,AdvancedObjectSpecs>(),
		parseHashMap<uint,UserData>( json[P_USER_DATA] ),
		json[P_PAUSE_WARP].hasValue(),
		json[P_PAUSE_WARP].getOrDefault( PauseWarp::Default() )
	};

	specs.starSpawnLocations.fill( Vec3f() );
	for( const Json &starSpawn : json[P_STAR_SPAWNS].array() ) {
		specs.starSpawnLocations.at( starSpawn[P_ID].get<int>() ) = starSpawn.get<Vec3f>();
	}

	specs.waypoints.fill( std::vector<Vec3s>() );
	for( const Json &waypoint : json[P_WAYPOINTS].array() ) {
		std::vector<Vec3s> &trail = specs.waypoints.at( waypoint[P_ID].get<int>() );
		const JArray &jsonTrail = waypoint[P_WAYPOINTS].array();
		trail.reserve( jsonTrail.size() );
		for( const Json &point : jsonTrail ) {
			trail.push_back( point.get<Vec3s>() );
		}
	}

	for( const Json &jsonObject : json[P_OBJECTS].array() ) {
		GenericObjectSpecs object = jsonObject.get<GenericObjectSpecs>();
		switch( ObjectId::getType( object.id ) ) {
			case ObjectType::Simple:
				specs.simpleObjects[object.id] = std::move( std::get<ObjectSpecs>( object.data ) );
				break;
			case ObjectType::Partial:
				specs.partialObjects[object.id] = std::move( std::get<PartialObjectSpecs>( object.data ) );
				break;
			case ObjectType::Advanced:
				specs.advancedObjects[object.id] = std::move( std::get<AdvancedObjectSpecs>( object.data ) );
				break;
			default:
				throw JsonReaderException( "Unknown object type" );
		}
	}

	if( DeserializationContext::schemaVersion() < Version{ 3, 1, 1 } && specs.defaultEchoLevel == 0x28 ) {
		specs.defaultEchoLevel = 0x08;
	}

	return specs;
}
