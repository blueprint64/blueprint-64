#ifndef SRC_CORE_DATA_SHADING_HPP_
#define SRC_CORE_DATA_SHADING_HPP_

#include "src/core/colour.hpp"
#include "src/core/json.hpp"

struct ShadingSpecs {
	uint id;
	string name;
	ColourRGB24 ambient;
	ColourRGB24 diffuse;
	short lightAngleYaw;
	short lightAnglePitch;
	bool asmRef;

	static const ShadingSpecs &Default();
};

namespace JsonSerializer {
	template<> void serialize<ShadingSpecs>( JsonWriter &jw, const ShadingSpecs &obj );
	template<> ShadingSpecs parse<ShadingSpecs>( const Json &json );
}


#endif /* SRC_CORE_DATA_SHADING_HPP_ */
