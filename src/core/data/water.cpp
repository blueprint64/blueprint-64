#include "src/core/data/water.hpp"

const WaterBoxSpecs &WaterBoxSpecs::Default() {
	static const WaterBoxSpecs s_default = {
		/* name */ "Untitled Water Box",
		/* isPoisonFog */ false,
		/* isVisible */ true,
		/* asmRefTexturePtr */ false,
		/* asmRefColour */ false,
		/* xMin */ 0,
		/* xMax */ 0,
		/* zMin */ 0,
		/* zMax */ 0,
		/* y */ 0,
		/* scale */ 25,
		/* opacity */ 0xB4,
		/* colour */ ColourRGB24( 0xFF, 0xFF, 0x00 ),
		/* texture */ { "f615067efa00c1112ac503d006781c24", "", TextureFormat::RGBA16, false },
		/* animationSpecs */ AnimationSpecs()
	};
	return s_default;
};

static constexpr char P_HASH[] = "hash";
static constexpr char P_FORMAT[] = "format";
static constexpr char P_ASM_REF[] = "asm_ref";

template<> void JsonSerializer::serialize<WaterTextureInfo>( JsonWriter &jw, const WaterTextureInfo &obj ) {
	jw.writeObjectStart();
		jw.writeProperty( P_HASH, obj.textureHash );
		jw.writeProperty( P_FORMAT, obj.textureFormat );
		if( obj.asmRef ) {
			jw.writeProperty( P_ASM_REF, obj.name );
		}
	jw.writeObjectEnd();
}

template<> WaterTextureInfo JsonSerializer::parse<WaterTextureInfo>( const Json &json ) {
	return WaterTextureInfo{
		json[P_HASH].get<string>(),
		json[P_ASM_REF].getOrDefault<string>( "" ),
		json[P_FORMAT].get<TextureFormat>(),
		json[P_ASM_REF].exists()
	};
}

static constexpr char P_NAME[] = "name";
static constexpr char P_TEXTURE[] = "texture";
static constexpr char P_POISON[] = "poison_fog";
static constexpr char P_VISIBLE[] = "visible";
static constexpr char P_TEXTURE_REF[] = "asm_ref_texture";
static constexpr char P_COLOUR_REF[] = "asm_ref_colour";
static constexpr char P_X0[] = "x0";
static constexpr char P_X1[] = "x1";
static constexpr char P_Z0[] = "z0";
static constexpr char P_Z1[] = "z1";
static constexpr char P_Y[] = "y";
static constexpr char P_SCALE[] = "scale";
static constexpr char P_OPACITY[] = "opacity";
static constexpr char P_COLOUR[] = "colour";
static constexpr char P_ANIMATION[] = "animation";


template<> void JsonSerializer::serialize<WaterBoxSpecs>( JsonWriter &jw, const WaterBoxSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_TEXTURE, obj.texture );
	jw.writeProperty( P_POISON, obj.isPoisonFog );
	jw.writeProperty( P_VISIBLE, obj.isVisible );
	jw.writeProperty( P_TEXTURE_REF, obj.asmRefTexturePtr );
	jw.writeProperty( P_COLOUR_REF, obj.asmRefColour );
	jw.writeProperty( P_X0, obj.xMin );
	jw.writeProperty( P_X1, obj.xMax );
	jw.writeProperty( P_Z0, obj.zMin );
	jw.writeProperty( P_Z1, obj.zMax );
	jw.writeProperty( P_Y, obj.y );
	jw.writeProperty( P_SCALE, obj.scale );
	jw.writeProperty( P_OPACITY, obj.opacity );
	jw.writeProperty( P_COLOUR, obj.colour.toString() );
	jw.writeProperty( P_ANIMATION, obj.animation );
	jw.writeObjectEnd();
}

template<> WaterBoxSpecs JsonSerializer::parse<WaterBoxSpecs>( const Json &json ) {
	return WaterBoxSpecs {
		json[P_NAME].get<string>(),
		json[P_POISON].get<bool>(),
		json[P_VISIBLE].get<bool>(),
		json[P_TEXTURE_REF].get<bool>(),
		json[P_COLOUR_REF].get<bool>(),
		json[P_X0].get<short>(),
		json[P_X1].get<short>(),
		json[P_Z0].get<short>(),
		json[P_Z1].get<short>(),
		json[P_Y].get<short>(),
		json[P_SCALE].get<short>(),
		json[P_OPACITY].get<ubyte>(),
		json[P_COLOUR].get<ColourRGB24>(),
		json[P_TEXTURE].get<WaterTextureInfo>(),
		json[P_ANIMATION].get<AnimationSpecs>()
	};
}
