#ifndef SRC_CORE_DATA_AREA_HPP_
#define SRC_CORE_DATA_AREA_HPP_

#include "src/core/enums.hpp"
#include "src/core/colour.hpp"
#include "src/core/data/camera.hpp"
#include "src/core/data/water.hpp"
#include "src/core/data/fog.hpp"

struct AreaSpecs {

	ushort areaId;

	BackgroundImage backgroundImage;
	ColourRGBA5551 backgroundColour;

	struct {
		Vec3s position;
		Vec3s focus;
		bool fixed;
	} pauseCamera;
	CameraTrigger defaultCamera; // don't care about the trigger region for this

	bool overrideFOV;
	short fieldOfView;
	ushort nearClippingPlane;
	ushort farClippingPlane;
	FogSpecs fog;

	EnvironmentEffect environmentEffect;
	ubyte terrainType;
	sbyte echoLevel;
	ubyte music;

	ubyte numObjects;
	ubyte numWarps;
	ubyte numInstantWarps;
	//ubyte numPaintingWarps;

	bool autoDeathFloor;
	short voidHeight;

	std::vector<WaterBoxSpecs> waterBoxes;
	std::vector<CameraTrigger> cameraTriggers;

	static const AreaSpecs &Default();

};

namespace JsonSerializer {
	template<> void serialize<AreaSpecs>( JsonWriter &jw, const AreaSpecs &obj );
	template<> AreaSpecs parse<AreaSpecs>( const Json &json );
}


#endif /* SRC_CORE_DATA_AREA_HPP_ */
