#include "src/core/data/object.hpp"

const ObjectSpecs &ObjectSpecs::Default() {
	static const ObjectSpecs s_default = {
		/* modelId */ 0,
		/* name */ "New Object",
		/* cullingRadius */ {
			/* autoCompute */ true,
			/* alwaysUpright */ false,
			/* maxScale */ 1.0f,
			/* maxFOV */ 45,
			/* radius */ 300
		},
		/* shadow */ {
			/* shape */ ShadowType::Circle,
			/* size */ 100,
			/* opacity */ 0xB4,
			/* enabled */ false
		},
		/* fog */ {
			/* fadeStartDistance */ 0.98,
			/* fadeEndDistance */ 1.0,
			/* colour */ ColourRGB24( 0, 0, 0 ),
			/* enabled */ false
		},
		/* useObjectAlpha */ false,
		/* hasCollision */ false,
		/* behaviour */ {
			BehaviourType::AsmReference,
			0,
			SimpleBehaviour::Default(),
		},
		/* max_xz */ 0,
		/* max_xyz */ 0
	};
	return s_default;
};

static constexpr char P_ID[] = "id";
static constexpr char P_MODEL[] = "model_id";
static constexpr char P_NAME[] = "name";
static constexpr char P_CULLING[] = "culling";
static constexpr char P_AUTOCALC[] = "auto_compute";
static constexpr char P_UPRIGHT[] = "upright";
static constexpr char P_SCALE[] = "scale";
static constexpr char P_FOV[] = "fov";
static constexpr char P_RADIUS[] = "radius";
static constexpr char P_SHADOW[] = "shadow";
static constexpr char P_SHAPE[] = "shape";
static constexpr char P_SIZE[] = "size";
static constexpr char P_OPACITY[] = "opacity";
static constexpr char P_ENABLED[] = "enabled";
static constexpr char P_FOG[] = "fog";
static constexpr char P_USE_ALPHA[] = "use_object_alpha";
static constexpr char P_COLLISION[] = "has_collision";
static constexpr char P_BEHAVIOUR[] = "behaviour";
static constexpr char P_MAX_2D[] = "max_xz";
static constexpr char P_MAX_3D[] = "max_xyz";
static constexpr char P_DATA[] = "data_id";

template<> void JsonSerializer::serialize<ObjectSpecsRef>( JsonWriter &jw, const ObjectSpecsRef &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_ID, obj.id );
	switch( ObjectId::getType( obj.id ) ) {
		case ObjectType::Simple: {
			const ObjectSpecs *specs = obj.dataRef.standard;
			jw.writeProperty( P_MODEL, specs->modelId );
			jw.writeProperty( P_NAME, specs->name );
			jw.writePropertyName( P_CULLING );
			jw.writeObjectStart();
				jw.writeProperty( P_AUTOCALC, specs->cullingRadius.autoCompute );
				jw.writeProperty( P_UPRIGHT, specs->cullingRadius.alwaysUpright );
				jw.writeProperty( P_SCALE, specs->cullingRadius.maxScale );
				jw.writeProperty( P_FOV, specs->cullingRadius.maxFOV );
				jw.writeProperty( P_RADIUS, specs->cullingRadius.radius );
			jw.writeObjectEnd();
			jw.writePropertyName( P_SHADOW );
			jw.writeObjectStart();
				jw.writeProperty( P_SHAPE, specs->shadow.shape );
				jw.writeProperty( P_SIZE, specs->shadow.size );
				jw.writeProperty( P_OPACITY, specs->shadow.opacity );
				jw.writeProperty( P_ENABLED, specs->shadow.enabled );
			jw.writeObjectEnd();
			jw.writeProperty( P_FOG, specs->fog );
			jw.writeProperty( P_USE_ALPHA, specs->useObjectAlpha );
			jw.writeProperty( P_COLLISION, specs->hasCollision );
			jw.writeProperty( P_BEHAVIOUR, specs->behaviour );
			jw.writeProperty( P_MAX_2D, specs->max_xz );
			jw.writeProperty( P_MAX_3D, specs->max_xyz );
			break;
		}
		case ObjectType::Partial: {
			const PartialObjectSpecs *specs = obj.dataRef.partial;
			jw.writeProperty( P_NAME, specs->name );
			jw.writeProperty( P_USE_ALPHA, specs->useObjectAlpha );
			break;
		}
		case ObjectType::Advanced: {
			const AdvancedObjectSpecs *specs = obj.dataRef.advanced;
			jw.writeProperty( P_MODEL, specs->modelId );
			jw.writeProperty( P_NAME, specs->name );
			jw.writeProperty( P_DATA, specs->customDataId );
			break;
		}
		default:
			throw JsonWriterException( "Unknown object type" );
	}
	jw.writeObjectEnd();
}

template<> void JsonSerializer::serialize<GenericObjectSpecs>( JsonWriter &jw, const GenericObjectSpecs &obj ) {
	ObjectSpecsRef ref;
	ref.id = obj.id;
	switch( ObjectId::getType( obj.id ) ) {
		case ObjectType::Simple:
			ref.dataRef.standard = &std::get<ObjectSpecs>( obj.data );
			break;
		case ObjectType::Partial:
			ref.dataRef.partial = &std::get<PartialObjectSpecs>( obj.data );
			break;
		case ObjectType::Advanced:
			ref.dataRef.advanced = &std::get<AdvancedObjectSpecs>( obj.data );
			break;
		default:
			throw JsonWriterException( "Unknown object type" );
	}
	serialize( jw, ref );
}

template<> GenericObjectSpecs JsonSerializer::parse<GenericObjectSpecs>( const Json &json ) {
	GenericObjectSpecs specs;
	specs.id = json[P_ID].get<objectId>();
	switch( ObjectId::getType( specs.id ) ) {
		case ObjectType::Simple:
			specs.data = ObjectSpecs {
				json[P_MODEL].get<ubyte>(),
				json[P_NAME].get<string>(),
				{
					json[P_CULLING][P_AUTOCALC].get<bool>(),
					json[P_CULLING][P_UPRIGHT].get<bool>(),
					json[P_CULLING][P_SCALE].get<float>(),
					json[P_CULLING][P_FOV].get<short>(),
					json[P_CULLING][P_RADIUS].get<ushort>()
				},
				{
					json[P_SHADOW][P_SHAPE].get<ShadowType>(),
					json[P_SHADOW][P_SIZE].get<ushort>(),
					json[P_SHADOW][P_OPACITY].get<ubyte>(),
					json[P_SHADOW][P_ENABLED].get<bool>()
				},
				json[P_FOG].get<FogSpecs>(),
				json[P_USE_ALPHA].get<bool>(),
				json[P_COLLISION].get<bool>(),
				json[P_BEHAVIOUR].get<Behaviour>(),
				json[P_MAX_2D].get<ushort>(),
				json[P_MAX_3D].get<ushort>()
			};
			break;
		case ObjectType::Partial:
			specs.data = PartialObjectSpecs {
				json[P_NAME].get<string>(),
				json[P_USE_ALPHA].get<bool>()
			};
			break;
		case ObjectType::Advanced:
			specs.data = AdvancedObjectSpecs {
				json[P_MODEL].get<ubyte>(),
				json[P_NAME].get<string>(),
				json[P_DATA].get<uint>()
			};
			break;
		default:
			throw JsonReaderException( "Uknown object type" );
	}

	return specs;
}
