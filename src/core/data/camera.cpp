#include "src/core/data/camera.hpp"

static constexpr char P_NAME[] = "name";
static constexpr char P_TYPE[] = "type";
static constexpr char P_CENTRE[] = "centre";
static constexpr char P_RADIUS[] = "radius";
static constexpr char P_ANGLE[] = "angle";
static constexpr char P_MODE[] = "mode";
static constexpr char P_FRAMES[] = "frames";
static constexpr char P_BASE_ANGLE[] = "base_angle";
static constexpr char P_BOSS[] = "boss_behaviour";
static constexpr char P_POI_X[] = "poi_x";
static constexpr char P_POI_Z[] = "poi_z";
static constexpr char P_PIVOT_X[] = "pivot_x";
static constexpr char P_PIVOT_Y[] = "pivot_y";
static constexpr char P_PIVOT_Z[] = "pivot_z";
static constexpr char P_LABEL[] = "label";
static constexpr char P_ID[] = "id";
static constexpr char P_PARAMS[] = "params";
static constexpr char P_PROPERTY[] = "property";
static constexpr char P_VALUE[] = "value";

template<> void JsonSerializer::serialize<CameraTrigger>( JsonWriter &jw, const CameraTrigger &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_TYPE, obj.type );
	jw.writeProperty( P_CENTRE, obj.centre );
	jw.writeProperty( P_RADIUS, obj.radius );
	jw.writeProperty( P_ANGLE, obj.angle );
	switch( obj.type ) {
		case CameraTriggerType::Vanilla: {
			const VanillaCameraOptions &options = std::get<VanillaCameraOptions>( obj.options );
			jw.writeProperty( P_MODE, options.mode );
			jw.writeProperty( P_FRAMES, options.frames );
			if( options.mode == CameraPreset::SemiCardinal ) {
				jw.writeProperty( P_BASE_ANGLE, options.ext.baseAngle );
			} else if( options.mode == CameraPreset::BossFight ) {
				jw.writeProperty( P_BOSS, options.ext.behaviour );
			} else if( options.mode == CameraPreset::InnerRadial || options.mode == CameraPreset::OuterRadial ) {
				jw.writeProperty( P_POI_X, options.ext.centre[0] );
				jw.writeProperty( P_POI_Z, options.ext.centre[1] );
			} else if( options.mode == CameraPreset::ElasticPivot ) {
				jw.writeProperty( P_PIVOT_X, options.ext.pivot[0] );
				jw.writeProperty( P_PIVOT_Y, options.ext.pivot[1] );
				jw.writeProperty( P_PIVOT_Z, options.ext.pivot[2] );
			}
			break;
		}
		case CameraTriggerType::Custom: {
			const CustomCameraOptions &options = std::get<CustomCameraOptions>( obj.options );
			jw.writeProperty( P_LABEL, options.functionLabel );
			break;
		}
		case CameraTriggerType::Module: {
			const ModuleCameraOptions &options = std::get<ModuleCameraOptions>( obj.options );
			jw.writeProperty( P_ID, options.moduleId.toString() );
			jw.writeProperty( P_FRAMES, options.frames );
			jw.writePropertyName( P_PARAMS );
			jw.writeArrayStart();
			for( const auto &i : options.moduleParams ) {
				jw.writeObjectStart();
				jw.writeProperty( P_PROPERTY, i.first );
				jw.writeProperty( P_TYPE, i.second.type );
				switch( i.second.type ) {
					case ModulePropertyType::Integer:
						jw.writeProperty( P_VALUE, i.second.signedValue );
						break;
					case ModulePropertyType::Pointer:
						jw.writeProperty( P_VALUE, i.second.unsignedValue );
						break;
					case ModulePropertyType::Float:
					case ModulePropertyType::Angle:
						jw.writeProperty( P_VALUE, i.second.floatValue );
						break;
					case ModulePropertyType::Boolean:
						jw.writeProperty( P_VALUE, i.second.booleanValue );
						break;
					default:
						throw JsonWriterException();
				}
				jw.writeObjectEnd();
			}
			jw.writeArrayEnd();
			break;
		}
		default: throw JsonWriterException();
	}
	jw.writeObjectEnd();
}

template<> CameraTrigger JsonSerializer::parse<CameraTrigger>( const Json &json ) {
	CameraTrigger trigger;
	trigger.name = json[P_NAME].get<string>();
	trigger.type = json[P_TYPE].get<CameraTriggerType>();
	trigger.centre = json[P_CENTRE].get<Vec3s>();
	trigger.radius = json[P_RADIUS].get<Vec3s>();
	trigger.angle = json[P_ANGLE].get<ushort>();
	switch( trigger.type ) {
		case CameraTriggerType::Vanilla: {
			VanillaCameraOptions options;
			options.mode = json[P_MODE].get<CameraPreset>();
			options.frames = json[P_FRAMES].getOrDefault<short>( 0 );
			switch( options.mode ) {
				case CameraPreset::SemiCardinal:
					options.ext.baseAngle = json[P_BASE_ANGLE].getOrDefault<ushort>( 0 );
					break;
				case CameraPreset::BossFight:
					options.ext.behaviour = json[P_BOSS].get<uint>();
					break;
				case CameraPreset::InnerRadial:
				case CameraPreset::OuterRadial:
					options.ext.centre[0] = json[P_POI_X].getOrDefault<float>( (float)trigger.centre.x );
					options.ext.centre[1] = json[P_POI_Z].getOrDefault<float>( (float)trigger.centre.z );
					break;
				case CameraPreset::ElasticPivot:
					options.ext.pivot[0] = json[P_PIVOT_X].getOrDefault<short>( 646 );
					options.ext.pivot[1] = json[P_PIVOT_Y].getOrDefault<short>( 143 );
					options.ext.pivot[2] = json[P_PIVOT_Z].getOrDefault<short>( -1513 );
					break;
				default: break;
			}
			trigger.options = std::move( options );
			break;
		}
		case CameraTriggerType::LEGACY_Boss: {
			// Migration from older Blueprint files
			VanillaCameraOptions options;
			options.mode = CameraPreset::BossFight;
			options.frames = 0;
			options.ext.behaviour = json[P_BOSS].get<uint>();
			trigger.options = std::move( options );
			break;
		}
		case CameraTriggerType::Custom: {
			CustomCameraOptions options;
			options.functionLabel = json[P_LABEL].get<string>();
			trigger.options = std::move( options );
			break;
		}
		case CameraTriggerType::Module: {
			ModuleCameraOptions options;
			options.moduleId = Uuid::parse( json[P_ID].get<string>() );
			options.frames = json[P_FRAMES].get<short>();
			for( const Json &paramJson : json[P_PARAMS].array() ) {
				CameraModulePropertyValue prop;
				prop.type = paramJson[P_TYPE].get<ModulePropertyType>();
				switch( prop.type ) {
					case ModulePropertyType::Integer:
						prop.signedValue = paramJson[P_VALUE].get<int>();
						break;
					case ModulePropertyType::Pointer:
						prop.unsignedValue = paramJson[P_VALUE].get<uint>();
						break;
					case ModulePropertyType::Float:
					case ModulePropertyType::Angle:
						prop.floatValue = paramJson[P_VALUE].get<float>();
						break;
					case ModulePropertyType::Boolean:
						prop.booleanValue = paramJson[P_VALUE].get<bool>();
						break;
					default:
						throw JsonReaderException( "Failed to parse camera trigger property value" );
				}
				options.moduleParams[ paramJson[P_PROPERTY].get<string>() ] = prop;
			}
			trigger.options = std::move( options );
			break;
		}
		default: throw JsonReaderException( "Unknown camera trigger type" );
	}
	return trigger;
}
