#ifndef SRC_CORE_DATA_MATERIAL_HPP_
#define SRC_CORE_DATA_MATERIAL_HPP_

#include <optional>
#include "src/core/data/animation.hpp"
#include "src/core/texture-info.hpp"

struct MaterialSpecs {

	bool visible;
	bool solid;

	ubyte opacity;
	bool removedTextureAlpha;

	DrawingLayer layer;
	TextureFormat format;
	TextureFiltering filtering;
	bool ditheredAlpha;
	bool doubleSided;
	bool reflection;

	TextureWrapping hwrap;
	TextureWrapping vwrap;

	uint shadingRef;

	struct {
		CollisionType type;
		ubyte direction;
		ForceIntensity force;
	} collision;

	std::optional<TextureInfo> textureInfo;
	AnimationSpecs animation;

	struct {
		bool texData;
		bool texPtr;
		bool shadePtr;
		bool opacity;
	} asmRefs;

	static const MaterialSpecs &Default();

};

namespace JsonSerializer {
	template<> void serialize<MaterialSpecs>( JsonWriter &jw, const MaterialSpecs &obj );
	template<> MaterialSpecs parse<MaterialSpecs>( const Json &json );
}



#endif /* SRC_CORE_DATA_MATERIAL_HPP_ */
