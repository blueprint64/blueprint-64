#ifndef SRC_CORE_DATA_FOG_HPP_
#define SRC_CORE_DATA_FOG_HPP_

#include "src/core/colour.hpp"
#include "src/core/json.hpp"

struct FogSpecs {
	double fadeStartDistance;
	double fadeEndDistance;
	ColourRGB24 colour;
	bool enabled;
};

namespace JsonSerializer {
	template<> void serialize<FogSpecs>( JsonWriter &jw, const FogSpecs &obj );
	template<> FogSpecs parse<FogSpecs>( const Json &json );
}

#endif /* SRC_CORE_DATA_FOG_HPP_ */
