#include "src/core/data/shading.hpp"

const ShadingSpecs &ShadingSpecs::Default() {
	static const ShadingSpecs s_default = {
		/* id */ 0,
		/* name */ "Default",
		/* ambient */ ColourRGB24( 0x7F, 0x7F, 0x7F ),
		/* diffuse */ ColourRGB24( 0xFF, 0xFF, 0xFF ),
		/* lightAngleYaw */ -45,
		/* lightAnglePitch */ -35,
		/* asmRef */ false
	};
	return s_default;
};

static constexpr char P_ID[] = "id";
static constexpr char P_NAME[] = "name";
static constexpr char P_AMBIENT[] = "dark";
static constexpr char P_DIFFUSE[] = "light";
static constexpr char P_ANGLE[] = "light_angle";
static constexpr char P_YAW[] = "yaw";
static constexpr char P_PITCH[] = "pitch";
static constexpr char P_ASM_REF[] = "asm_ref";

template<> void JsonSerializer::serialize<ShadingSpecs>( JsonWriter &jw, const ShadingSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_ID, obj.id );
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_DIFFUSE, obj.diffuse );
	jw.writeProperty( P_AMBIENT, obj.ambient );
	jw.writePropertyName( P_ANGLE );
	jw.writeObjectStart();
		jw.writeProperty( P_YAW, obj.lightAngleYaw );
		jw.writeProperty( P_PITCH, obj.lightAnglePitch );
	jw.writeObjectEnd();
	jw.writeProperty( P_ASM_REF, obj.asmRef );
	jw.writeObjectEnd();
}

template<> ShadingSpecs JsonSerializer::parse<ShadingSpecs>( const Json &json ) {
	return ShadingSpecs {
		json[P_ID].get<uint>(),
		json[P_NAME].get<string>(),
		json[P_AMBIENT].get<ColourRGB24>(),
		json[P_DIFFUSE].get<ColourRGB24>(),
		json[P_ANGLE][P_YAW].getOrDefault<short>( ShadingSpecs::Default().lightAngleYaw ),
		json[P_ANGLE][P_PITCH].getOrDefault<short>( ShadingSpecs::Default().lightAnglePitch ),
		json[P_ASM_REF].getOrDefault<bool>( ShadingSpecs::Default().asmRef )
	};
}
