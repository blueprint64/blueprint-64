#ifndef SRC_CORE_DATA_WATER_HPP_
#define SRC_CORE_DATA_WATER_HPP_

#include "src/core/enums.hpp"
#include "src/core/colour.hpp"
#include "src/core/data/animation.hpp"

struct WaterTextureInfo {
	string textureHash;
	string name;
	TextureFormat textureFormat;
	bool asmRef;

	bool operator==( const WaterTextureInfo &other ) const {
		return(
			textureHash == other.textureHash &&
			textureFormat == other.textureFormat &&
			asmRef == other.asmRef &&
			(!asmRef || (name == other.name))
		);
	}

	inline bool operator!=( const WaterTextureInfo &other ) const {
		return !(*this == other);
	}
};

struct VanillaWaterTexture {
	uint segmentedPointer;
	TextureFormat textureFormat;
};

struct WaterBoxSpecs {
	string name;
	bool isPoisonFog;
	bool isVisible;
	bool asmRefTexturePtr;
	bool asmRefColour;
	short xMin, xMax;
	short zMin, zMax;
	short y;
	short scale;
	ubyte opacity;
	ColourRGB24 colour;
	WaterTextureInfo texture;
	AnimationSpecs animation;

	static const WaterBoxSpecs &Default();
};

namespace std {
	template <> struct hash<WaterTextureInfo> {
		size_t operator()(const WaterTextureInfo & x) const {
			static const hash<string> strHash;
			const size_t h = strHash( x.textureHash ) ^ (size_t)x.textureFormat;
			return x.asmRef ? (h ^ strHash( x.name )) : h;
		}
	};
}

namespace JsonSerializer {
	template<> void serialize<WaterTextureInfo>( JsonWriter &jw, const WaterTextureInfo &obj );
	template<> WaterTextureInfo parse<WaterTextureInfo>( const Json &json );

	template<> void serialize<WaterBoxSpecs>( JsonWriter &jw, const WaterBoxSpecs &obj );
	template<> WaterBoxSpecs parse<WaterBoxSpecs>( const Json &json );
}



#endif /* SRC_CORE_DATA_WATER_HPP_ */
