#ifndef SRC_CORE_DATA_TERRAIN_HPP_
#define SRC_CORE_DATA_TERRAIN_HPP_

#include <array>
#include "src/core/enums.hpp"
#include "src/core/json.hpp"

struct TerrainSoundPack {
	string name;
	TerrainSound normal;
	TerrainSound hard;
	TerrainSound slippery;
	TerrainSound slide;
	TerrainSound alternate;
	TerrainSound slipperyAlternate;

	static const std::array<TerrainSoundPack,7> &Default();
};

namespace JsonSerializer {
	template<> void serialize<TerrainSoundPack>( JsonWriter &jw, const TerrainSoundPack &obj );
	template<> TerrainSoundPack parse<TerrainSoundPack>( const Json &json );
}

#endif /* SRC_CORE_DATA_TERRAIN_HPP_ */
