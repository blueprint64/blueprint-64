#include "src/core/data/user-data.hpp"

static constexpr char P_ID[] = "id";
static constexpr char P_NAME[] = "name";
static constexpr char P_LINKS[] = "links";
static constexpr char P_ASM_REF[] = "asm_ref";
static constexpr char P_SIZE[] = "size";

static constexpr char P_POSITION[] = "offset";
static constexpr char P_TYPE[] = "type";
static constexpr char P_TARGET[] = "target";
static constexpr char P_OFFSET[] = "target_offset";

template<> void JsonSerializer::serialize<UserData>( JsonWriter &jw, const UserData &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_ID, obj.id );
	jw.writeProperty( P_NAME, obj.name );
	jw.writeProperty( P_SIZE, (uint)obj.bytes );
	jw.writeProperty( P_ASM_REF, obj.asmRef );
	jw.writePropertyName( P_LINKS );
	jw.writeArrayStart();
	for( const auto &j : obj.links ) {
		jw.writeObjectStart();
			jw.writeProperty( P_POSITION, j.first );
			jw.writeProperty( P_TYPE, j.second.linkType );
			if( j.second.linkType != LinkType::None ) {
				jw.writeProperty( P_TARGET, j.second.targetId );
				if( j.second.linkType == LinkType::UserData ) {
					jw.writeProperty( P_OFFSET, j.second.targetOffset );
				}
			}
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();
	jw.writeObjectEnd();
}

template<> UserData JsonSerializer::parse<UserData>( const Json &json ) {
	UserData data = {
		json[P_ID].get<uint>(),
		json[P_NAME].get<string>(),
		std::map<uint,UserDataLink>(),
		json[P_ASM_REF].get<bool>(),
		json[P_SIZE].get<uint>()
	};

	for( const Json &linkJson : json[P_LINKS].array() ) {
		data.links[ linkJson[P_POSITION].get<uint>() ] = {
			linkJson[P_TYPE].get<LinkType>(),
			linkJson[P_TARGET].getOrDefault<uint>( 0 ),
			linkJson[P_OFFSET].getOrDefault<uint>( 0 )
		};
	}

	return data;
}
