#include "src/core/data/rom.hpp"

const RomSpecs &RomSpecs::Default() {
	static const RomSpecs s_default = {
		/* internalName */ "SUPER MARIO 64",
		/* enableChecksum */ false,
		/* levelBoundaries */ LevelBoundarySize::Normal,
		/* startingLevel */ LevelId::CastleGrounds,
		/* worldScale */ 1.f,
		/* expandAudioHeap */ false,
		/* cutoutDecal */ false,
		/* pauseWarp */ PauseWarp::Default()
	};
	return s_default;
};

static constexpr char P_NAME[] = "name";
static constexpr char P_CHECKSUM[] = "checksum";
static constexpr char P_BOUNDARY[] = "boundary";
static constexpr char P_START_LEVEL[] = "start_level";
static constexpr char P_EXPAND_AUDIO[] = "expand_audio_heap";
static constexpr char P_CUTOUT_DECAL[] = "cutout_decal";
static constexpr char P_WORLD_SCALE[] = "world_scale";
static constexpr char P_PAUSE_WARP[] = "pause_warp";

template<> void JsonSerializer::serialize<RomSpecs>( JsonWriter &jw, const RomSpecs &obj ) {
	jw.writeObjectStart();
	jw.writeProperty( P_NAME, obj.internalName );
	jw.writeProperty( P_CHECKSUM, obj.enableChecksum );
	jw.writeProperty( P_BOUNDARY, obj.levelBoundaries );
	jw.writeProperty( P_START_LEVEL, obj.startingLevel );
	jw.writeProperty( P_WORLD_SCALE, obj.worldScale );
	jw.writeProperty( P_EXPAND_AUDIO, obj.expandAudioHeap );
	jw.writeProperty( P_CUTOUT_DECAL, obj.cutoutDecal );
	jw.writeProperty( P_PAUSE_WARP, obj.pauseWarp );
	jw.writeObjectEnd();
}

template<> RomSpecs JsonSerializer::parse<RomSpecs>( const Json &json ) {
	return RomSpecs {
		json[P_NAME].getOrDefault<string>( RomSpecs::Default().internalName ),
		json[P_CHECKSUM].getOrDefault<bool>( RomSpecs::Default().enableChecksum ),
		json[P_BOUNDARY].getOrDefault<LevelBoundarySize>( RomSpecs::Default().levelBoundaries ),
		json[P_START_LEVEL].getOrDefault<LevelId>( RomSpecs::Default().startingLevel ),
		json[P_WORLD_SCALE].getOrDefault<float>( RomSpecs::Default().worldScale ),
		json[P_EXPAND_AUDIO].getOrDefault<bool>( RomSpecs::Default().expandAudioHeap ),
		json[P_CUTOUT_DECAL].getOrDefault<bool>( RomSpecs::Default().cutoutDecal ),
		json[P_PAUSE_WARP].getOrDefault<PauseWarp>( RomSpecs::Default().pauseWarp )
	};
}
