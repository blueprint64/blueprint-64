#ifndef SRC_CORE_UNICODE_HPP_
#define SRC_CORE_UNICODE_HPP_

#include <string>

namespace Unicode {

	extern std::wstring toUTF16( const char *utf8 );
	extern std::string toUTF8( const wchar_t *utf16 );

	inline std::wstring toUTF16( const std::string &utf8 ) {
		return toUTF16( utf8.c_str() );
	}

	inline std::string toUTF8( const std::wstring &utf16 ) {
		return toUTF8( utf16.c_str() );
	}

#ifdef _WIN32
	extern std::string fixWindowsEncoding( const std::string &str );
#endif

}



#endif /* SRC_CORE_UNICODE_HPP_ */
