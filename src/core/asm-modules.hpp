#ifndef SRC_CORE_ASM_MODULES_HPP_
#define SRC_CORE_ASM_MODULES_HPP_

#include <variant>
#include <optional>
#include <array>
#include <map>
#include "src/core/enums.hpp"
#include "src/core/uuid.hpp"
#include "src/core/version.hpp"
#include "src/core/colour.hpp"
#include "src/polyfill/filesystem.hpp"

struct AsmModuleDependency {
	string name;
	Uuid id;
	Version minVersion;
	Version maxVersion;
};

typedef std::variant<int,float,bool,string,ColourRGBA32,uint> AsmModulePropertyValue;

struct AsmModuleProperty {
	string name;
	string label;
	string description;
	ModulePropertyType type;
	AsmModulePropertyValue defaultValue;
	union {
		int minInt;
		float minFloat;
		bool encoded;
		uint trueValue;
		bool hasAlpha;
		bool isVirtual;
	};
	union {
		int maxInt;
		float maxFloat;
		ushort maxLength;
		uint falseValue;
		ModuleColourFormat colourFormat;
	};
	union {
		int precision;
		bool multiLine;
		bool hex;
	};
	string units;
};

struct AsmModuleHook {
	ModuleHook type;
	string label;

	string actionRef;

	ShimType shimType;
	HookPriority priority;
	uint shimAddress;
	sbyte niceness;
	/* niceness can be from 0 to 4 (or -1 for not applicable)
	 * 4 = works with all other shims except those with niceness 0
	 * 3 = works will all shims having niceness 2 or higher
	 * 2 = only works with other shims that are nicer (3 or 4)
	 * 1 = only works with shims having niceness 4
	 * 0 = does not work with any other shims regardless of their niceness
	 */
};

struct CustomMarioAction {
	string variableName;
	string functionLabel;
	MarioActionCategory category;
	MarioActionFlag flags;
	bool isPublic;
};

struct AbsoluteAsmFile {
	string file;
	uint romStart;
	uint romEnd;
};

struct CameraModule {
	string name;
	Uuid id;
	string label;
	string initLabel;
	std::vector<AsmModuleProperty> parameters;
};

struct CustomCamera {
	short cameraId;
	const CameraModule *module;
	Uuid asmModuleId;
};

struct AsmModuleDefinition {
	string name;
	Uuid id;
	Version version;
	string author;
	string description;
	std::optional<bool> consoleCompatible;
	std::vector<AsmModuleDependency> dependencies;
	bool areaScoped;
	std::vector<AsmModuleProperty> properties;
	std::vector<string> definitionFiles;
	std::vector<string> injectedFiles;
	std::vector<AbsoluteAsmFile> globalFiles;
	std::vector<AsmModuleHook> hooks;
	std::vector<CustomMarioAction> actions;
	std::vector<CameraModule> cameraModes;

	static AsmModuleDefinition loadFromFile( const string &filePath );
};

struct AsmModule {
	AsmModuleDefinition definition;
	std::array<ubyte,40> areaMask;
	HashMap<string,AsmModulePropertyValue> parameters;
};

namespace AsmModuleBuilder {
	void generate(
		const std::map<Uuid,AsmModule> &modules,
		const std::map<string,CustomMarioAction> &customActions,
		const std::map<Uuid,CustomCamera> &cameraModules,
		const fs::path &sourceDir,
		const fs::path &outputDir
	);
}

namespace AsmModuleStore {

	void reload();
	const HashMap<Uuid,std::map<Version,AsmModuleDefinition>> &allModules();
	const std::map<Version,AsmModuleDefinition> &getModuleVersions( const Uuid &moduleId );
	const AsmModuleDefinition &installModule( const fs::path &modulePath );
	void uninstallModule( const Uuid &moduleId );
	void uninstallModule( const Uuid &moduleId, const Version &version );
	fs::path installedModulePath( const Uuid &module, const Version &version );

	inline bool hasModuleVersion( const Uuid &moduleId, const Version &version ) {
		return getModuleVersions( moduleId ).count( version ) > 0;
	}

	void installBuiltinModules();

}

#endif /* SRC_CORE_ASM_MODULES_HPP_ */
