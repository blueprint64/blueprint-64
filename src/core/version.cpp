#include "src/core/version.hpp"

#include <regex>
#include <limits>
#include "src/core/exceptions.hpp"

using namespace std;

static const regex VersionRegex(
	"(\\d{1,5}).(\\d{1,5}).(\\d{1,5})",
	regex_constants::ECMAScript | regex_constants::optimize
);

string Version::toString() const {
	return std::to_string( major ) + "." + std::to_string( minor ) + "." + std::to_string( patch );
}

optional<Version> Version::tryParse( const string &versionString ) {
	optional<Version> version;

	smatch matches;
	if( !regex_search( versionString, matches, VersionRegex ) || matches.size() != 4 ) {
		return version;
	}

	const int major = std::stoi( matches[1].str() );
	const int minor = std::stoi( matches[2].str() );
	const int patch = std::stoi( matches[3].str() );

	constexpr ushort MAX_VALUE = numeric_limits<ushort>::max();

	if( major > MAX_VALUE || minor > MAX_VALUE || patch > MAX_VALUE ) {
		return version;
	}

	version = { (ushort)major, (ushort)minor, (ushort)patch };
	return version;
}

Version Version::parse( const string &versionString ) {
	const optional<Version> maybeVersion = Version::tryParse( versionString );
	if( !maybeVersion.has_value() ) {
		throw VersionParseException( "Invalid version string: "s + versionString );
	}
	return maybeVersion.value();
}


const Version CurrentVersion::Application = { 0, 20, 0 };
const Version CurrentVersion::BlueprintSchema = { 4, 0, 1 };
const Version CurrentVersion::PreferencesSchema = { 3, 0, 0 };
const Version CurrentVersion::TweakSchema = { 1, 1, 0 };
const Version CurrentVersion::MetadataFormat = { 0, 1, 0 };
const Version CurrentVersion::RomVersion = { 0, 4, 3 };
