#ifndef SRC_CORE_ACTORS_HPP_
#define SRC_CORE_ACTORS_HPP_

#include <iostream>
#include <vector>
#include <array>
#include "src/types.hpp"
#include "src/core/vector.hpp"
#include "src/core/enums.hpp"

// 0x1F actually means "always" rather than "all acts except 6".
// I could fix that, but it would mess with Quad64, so I'll leave it alone for now.
static constexpr ubyte ALL_ACTS = 0x1F;

struct PlacedObject {
	ubyte actMask;
	ubyte modelId;
	Vec3s position;
	Vec3s rotation;
	ubyte params[4];
	uint behaviour;

	void writeToRom( std::ostream &rom ) const;
	bool isEmpty() const;

	static const PlacedObject &Default();
};

struct NormalWarp {
	ubyte sourceWarpId;
	LevelId levelId;
	ubyte areaIndex;
	ubyte targetWarpId;
	bool checkpoint;

	void writeToRom( std::ostream &rom ) const;
	bool isEmpty() const;

	static const NormalWarp &Default();
};

struct InstantWarp {
	CollisionType trigger;
	ubyte areaIndex;
	Vec3s offset;

	void writeToRom( std::ostream &rom ) const;
};

struct PaintingWarp {
	ubyte sourceWarpId;
	LevelId levelId;
	ubyte areaIndex;
	ubyte targetWarpId;
	bool checkpoint;

	void writeToRom( std::ostream &rom ) const;
};

struct AreaActors {
	std::vector<PlacedObject> objects;
	std::vector<NormalWarp> warps;
	std::vector<InstantWarp> instantWarps;
	std::vector<PaintingWarp> paintingWarps;

	inline bool empty() const {
		return objects.empty() && warps.empty() && instantWarps.empty() && paintingWarps.empty();
	}

	void saveToJson( const string &jsonFilePath ) const;

	static std::array<AreaActors,8> loadFromRom( std::istream &rom, LevelId levelId );
	static AreaActors loadFromJson( const string &jsonFilePath );
};



#endif /* SRC_CORE_ACTORS_HPP_ */
