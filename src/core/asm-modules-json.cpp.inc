template<typename E> inline HashMap<string,E> makeEnumNameMap() {
	HashMap<string,E> enumMap;
	for( E value : Enum::values<E>() ) {
		enumMap[Enum::toString<E>( value )] = value;
	}
	return enumMap;
}

template<typename E> E readByName( const Json &enumJson ) {
	static HashMap<string,E> s_enumMap = makeEnumNameMap<E>();
	const string name = enumJson.get<string>();
	auto e = s_enumMap.find( name );
	if( e == s_enumMap.end() ) {
		string message = "Invalid value '"s + name + "' for enumerable property. Valid values are: ";
		for( E i : Enum::values<E>() ) {
			message += (string)Enum::toString<E>( i ) + ", ";
		}
		message.erase( message.end() - 2 );
		throw JsonReaderException( message );
	}
	return e->second;
}

static AsmModuleProperty parseProperty( const Json &prop, bool cameraParam ) {
	const ModulePropertyType type = readByName<ModulePropertyType>( prop["type"] );
	AsmModuleProperty propData;
	propData.name = prop["name"].get<string>();
	if( !cameraParam ) {
		propData.label = prop["label"].get<string>();
	}
	propData.description = prop["description"].getOrDefault<string>( "" );
	propData.type = type;
	switch( type ) {
		case ModulePropertyType::Integer: {
			propData.defaultValue = prop["default"].get<int>();
			propData.minInt = prop["min"].get<int>();
			propData.maxInt = prop["max"].get<int>();
			propData.units = prop["units"].getOrDefault<string>( "" );
			propData.hex = prop["hex"].getOrDefault<bool>( false );
			break;
		}
		case ModulePropertyType::Float: {
			propData.defaultValue = prop["default"].get<float>();
			propData.minFloat = prop["min"].get<float>();
			propData.maxFloat = prop["max"].get<float>();
			propData.precision = prop["precision"].get<int>();
			propData.units = prop["units"].getOrDefault<string>( "" );
			if( propData.precision < 1 || propData.precision > 4 ) {
				throw JsonReaderException( "precision must be between 1 and 4" );
			}
			break;
		}
		case ModulePropertyType::Angle: {
			propData.defaultValue = prop["default"].get<float>();
			propData.minFloat = prop["min"].getOrDefault<float>( 0 );
			propData.maxFloat = prop["max"].getOrDefault<float>( 359.99f );
			propData.precision = 2;
			propData.units = "°";
			break;
		}
		case ModulePropertyType::Boolean: {
			propData.defaultValue = prop["default"].get<bool>();
			propData.trueValue = prop["true_value"].getOrDefault<uint>( 1 );
			propData.falseValue = prop["false_value"].getOrDefault<uint>( 0 );
			break;
		}
		case ModulePropertyType::String: {
			if( cameraParam ) throw JsonReaderException( "Unsupported camera property type" );
			propData.defaultValue = prop["default"].getOrDefault<string>( "" );
			propData.encoded = prop["encoded"].getOrDefault<bool>( false );
			propData.maxLength = prop["max_length"].getOrDefault<ushort>( 0xFFFF );
			propData.multiLine = prop["multi_line"].getOrDefault<bool>( false );
			break;
		}
		case ModulePropertyType::Colour: {
			if( cameraParam ) throw JsonReaderException( "Unsupported camera property type" );
			propData.defaultValue = ColourRGBA32::parse( prop["default"].get<string>() );
			propData.hasAlpha = prop["has_alpha"].getOrDefault<bool>( false );
			propData.colourFormat = readByName<ModuleColourFormat>( prop["format"] );
			break;
		}
		case ModulePropertyType::Pointer: {
			propData.defaultValue = (uint)std::stoul( prop["default"].get<string>(), nullptr, 16 );
			propData.isVirtual = prop["virtual"].getOrDefault<bool>( false );
			break;
		}
		default: throw JsonReaderException( cameraParam ? "Unknown camera property type" : "Unknown module property type" );
	}
	return propData;
}

AsmModuleDefinition AsmModuleDefinition::loadFromFile( const string &filePath ) {
	FileReadStream jsonFile( filePath );
	const Json &json = Json::parse( jsonFile );

	Version schemaVersion = Version::parse( json["schema_version"].get<string>() );
	if( schemaVersion.major != 1 || schemaVersion.minor > 2 ) {
		throw JsonReaderException( "ASM Module schema version is not supported by this version of Bowser's Blueprints." );
	}

	std::optional<bool> consoleCompatibility;
	if( schemaVersion >= Version{ 1, 2, 0 } && json["console_compatible"].hasValue() ) {
		consoleCompatibility = json["console_compatible"].get<bool>();
	}

	std::vector<AsmModuleDependency> dependencies;
	if( json["dependencies"].isArray() ) {
		for( const Json &dependency : json["dependencies"].array() ) {
			Version minVersion, maxVersion;
			if( dependency["version"].isObject() ) {
				minVersion = Version::parse( dependency["version"]["min"].get<string>() );
				if( dependency["version"]["max"].exists() ) {
					maxVersion = Version::parse( dependency["version"]["max"].get<string>() );
					if( maxVersion < minVersion ) {
						throw JsonReaderException( "max version may not be smaller than min version" );
					}
				} else {
					maxVersion = { minVersion.major, 0xFFFF, 0xFFFF };
				}
			} else {
				minVersion = Version::parse( dependency["version"].get<string>() );
				maxVersion = { minVersion.major, 0xFFFF, 0xFFFF };
			}

			dependencies.push_back({
				dependency["name"].get<string>(),
				Uuid::parse( dependency["uuid"].get<string>() ),
				minVersion,
				maxVersion
			});
		}
	}

	std::vector<AsmModuleProperty> properties;
	if( json["properties"].isArray() ) {
		for( const Json &prop : json["properties"].array() ) {
			properties.push_back( parseProperty( prop, false ) );
		}
	}

	std::vector<string> definitionFiles;
	if( json["entry_defines"].isArray() ) {
		for( const Json &file : json["entry_defines"].array() ) {
			definitionFiles.push_back( file.get<string>() );
		}
	} else if( json["entry_defines"].exists() ) {
		definitionFiles.push_back( json["entry_defines"].get<string>() );
	}

	std::vector<string> injectedFiles;
	if( json["entry_injected"].isArray() ) {
		for( const Json &file : json["entry_injected"].array() ) {
			injectedFiles.push_back( file.get<string>() );
		}
	} else if( json["entry_injected"].exists() ) {
		injectedFiles.push_back( json["entry_injected"].get<string>() );
	}

	std::vector<AbsoluteAsmFile> globalFiles;
	if( json["entry_global"].isArray() ) {
		for( const Json &file : json["entry_global"].array() ) {
			globalFiles.push_back({
				file["file"].get<string>(),
				Util::parseHexWord( file["start"].get<string>() ),
				Util::parseHexWord( file["end"].get<string>() )
			});
		}
	} else if( json["entry_global"].exists() ) {
		const Json &file = json["entry_global"];
		globalFiles.push_back({
			file["file"].get<string>(),
			Util::parseHexWord( file["start"].get<string>() ),
			Util::parseHexWord( file["end"].get<string>() ),
		});
	}

	HashSet<string> definedActions;
	std::vector<CustomMarioAction> actions;
	if( json["action_ids"].isArray() ) {
		for( const Json &action : json["action_ids"].array() ) {
			MarioActionFlag flags = MarioActionFlag::None;
			if( action["flags"].isArray() ) {
				for( const Json &flag : action["flags"].array() ) {
					flags |= readByName<MarioActionFlag>( flag );
				}
			}

			const string actionName = action["var_name"].get<string>();
			definedActions.insert( actionName );
			actions.push_back({
				actionName,
				"n/a",
				readByName<MarioActionCategory>( action["category"] ),
				flags,
				action["public"].getOrDefault<bool>( false )
			});
		}
	}

	std::vector<AsmModuleHook> hooks;
	if( json["hooks"].isArray() ) {
		for( const Json &hook : json["hooks"].array() ) {
			const ModuleHook hookType = readByName<ModuleHook>( hook["hook"] );
			string actionRef;
			ShimType shimType = ShimType::NotApplicable;
			HookPriority priority = HookPriority::Middle;
			uint shimAddress = 0;
			sbyte niceness = -1;

			if( hookType == ModuleHook::Custom ) {
				shimType = readByName<ShimType>( hook["shim_type"] );
				priority = readByName<HookPriority>( hook["priority"] );
				shimAddress = Util::parseHexWord( hook["shim_address"].get<string>() );
				niceness = hook["nice"].get<sbyte>();
				if( niceness < 0 || niceness > 4 ) {
					throw JsonReaderException( "niceness must be between 0 and 4 inclusive." );
				}
			} else if( hookType == ModuleHook::MarioAction ) {
				actionRef = hook["action"].get<string>();
				if( definedActions.count( actionRef ) == 0 ) {
					throw JsonReaderException( "action must reference an action variable name defined in action_ids" );
				}
			} else if( hook["priority"].exists() ) {
				priority = readByName<HookPriority>( hook["priority"] );
			}

			hooks.push_back({
				hookType,
				hook["label"].get<string>(),
				actionRef,
				shimType,
				priority,
				shimAddress,
				niceness
			});
		}
	}

	std::vector<CameraModule> cameraModes;
	if( schemaVersion >= Version{ 1, 1, 0 } && json["camera_modes"].isArray() ) {
		for( const Json &cameraJson : json["camera_modes"].array() ) {
			CameraModule camera;
			camera.name = cameraJson["display_name"].get<string>();
			camera.id = Uuid::parse( cameraJson["uuid"].get<string>() );
			camera.label = cameraJson["label"].get<string>();
			camera.initLabel = cameraJson["init_label"].getOrDefault<string>( "" );
			if( cameraJson["properties"].isArray() ) {
				for( const Json &paramJson : cameraJson["properties"].array() ) {
					camera.parameters.push_back( parseProperty( paramJson, true ) );
				}
			}
			cameraModes.push_back( std::move( camera ) );
		}
	}

	return AsmModuleDefinition{
		json["name"].get<string>(),
		Uuid::parse( json["uuid"].get<string>() ),
		Version::parse( json["version"].get<string>() ),
		json["author"].getOrDefault( "Anonymous"s ),
		json["description"].get<string>(),
		std::move( consoleCompatibility ),
		std::move( dependencies ),
		json["area_scoped"].getOrDefault<bool>( false ),
		std::move( properties ),
		std::move( definitionFiles ),
		std::move( injectedFiles ),
		std::move( globalFiles ),
		std::move( hooks ),
		std::move( actions ),
		std::move( cameraModes )
	};

}
