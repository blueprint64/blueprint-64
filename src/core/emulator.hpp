#ifndef SRC_CORE_EMULATOR_HPP_
#define SRC_CORE_EMULATOR_HPP_

#include <vector>
#include <system_error>
#include "src/types.hpp"
#include "src/core/json.hpp"
#include "src/core/version.hpp"

namespace Emulator {

	enum class Emulator : ubyte {
		RetroArch = 0,
		Mupen64Plus = 1,
		Other = 2,
		Project64 = 3,
		ParallelLauncher = 4
	};

	enum class CpuEmulation : ubyte {
		DynamicRecompiler = 0,
		CachedInterpreter = 1,
		PureInterpreter = 2
	};

	namespace RetroArch {

		enum class EmulatorCore : ubyte {
			ParallelN64 = 0,
			Mupen64plusNext = 1
		};

		enum class GraphicsPlugin : ubyte {
			ParaLLEl = 0,
			Angrylion = 1,
			GlideN64 = 2,
			Glide64 = 3,
			Rice = 4
		};

		struct Settings {
#ifdef _WIN32
			string exePath;
#else
			bool useFlatpakInstall;
#endif

			EmulatorCore core;
			CpuEmulation cpuCore;
			GraphicsPlugin gfxPlugin;

			bool fullscreen;
			ubyte resolutionScale;
			bool vsync;
			bool fullspeed;
			bool overclockVI;

			static Settings parseJSON( const Json &json );
			void writeJson( JsonWriter &jw ) const;

			string createConfigFile() const;
		};

#ifdef _WIN32
		inline constexpr bool hasFlatpakInstall() noexcept { return false; }
#else
		extern bool hasFlatpakInstall();
#endif
		extern bool hasParallelLauncherConfig();

	}

	namespace Mupen64Plus {

		struct Settings {
#ifdef _WIN32
			string exePath;
#endif
			string pluginDirPath;

			CpuEmulation cpuCore;
			string gfxPlugin;
			string rspPlugin;

			bool fullscreen;
			ubyte resolutionScale;
			bool onScreenDisplay;

			static Settings parseJSON( const Json &json );
			void writeJson( JsonWriter &jw ) const;
		};

		extern std::vector<string> getGfxPlugins( const string &pluginDir );
		extern std::vector<string> getRspPlugins( const string &pluginDir );

	}

#ifdef _WIN32
	namespace Project64 {

		struct Settings {
			string exePath;
			string fullVersion;
			ushort majorVersion;

			static Settings parseJSON( const Json &json );
			void writeJson( JsonWriter &jw ) const;
		};

		extern std::vector<Settings> getInstalledVersions();

	}
#endif

	struct EmulatorSettings {
		Emulator preferredEmulator;
		RetroArch::Settings retroSettings;
		Mupen64Plus::Settings mupenSettings;
#ifdef _WIN32
		Project64::Settings pj64Settings;
		string parallelLauncherPath;
#endif
		string otherEmulatorCmd;

		static EmulatorSettings parseJSON( const Json &json );
		void writeJson( JsonWriter &jw ) const;
	};

	extern bool launch( const string &romPath, std::error_code &err );
	extern bool emulatorInstalled();

}



#endif /* SRC_CORE_EMULATOR_HPP_ */
