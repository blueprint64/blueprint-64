#ifndef SRC_CORE_PACKAGE_HPP_
#define SRC_CORE_PACKAGE_HPP_

#include "src/polyfill/filesystem.hpp"

namespace Package {

	enum Format : unsigned char {
		Tarball = 0,
		BppPkg = 1,
		Unknown = 2
	};

	bool create(
		const fs::path &packagePath,
		const fs::path &sourceDirectory
	);

	bool extract(
		const fs::path &packagePath,
		const fs::path &destinationDirectory,
		Format &format
	);

	inline bool extract(
		const fs::path &packagePath,
		const fs::path &destinationDirectory
	) {
		Format discard;
		return extract( packagePath, destinationDirectory, discard );
	}

	Format getFormat( const fs::path &packagePath );

}

#endif /* SRC_CORE_PACKAGE_HPP_ */
