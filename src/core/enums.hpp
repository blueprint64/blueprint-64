#ifndef CORE_ENUMS_HPP_
#define CORE_ENUMS_HPP_

#include <sys/types.h>
#include <vector>
#include <limits>
#include "src/types.hpp"

enum class CameraPreset : ushort {
	None = 0,
	OuterRadial = 1,
	InnerRadial = 2,
	BehindMario = 3,
	CloseQarters = 4,
	FirstPerson = 6,
	WaterSurface = 8,
	Slide = 9,
	Cannon = 10,
	BossFight = 11,
	Parallel = 12,
	ElasticPivot = 13,
	SemiCardinal = 14,
	FreeRoam = 16,
	SpiralStairs = 17
};

enum class EnvironmentEffect : ushort {
	None = 0,
	Snow = 1,
	Blizzard = 3,
	LavaBubbles = 12,
	Water = 2,
	Flowers = 11
};

enum class TextureWrapping : ubyte {
	Repeat = 0,
	Mirror = 1,
	Clamp = 2
};

enum class TextureFiltering : ubyte {
	None = 0,
	Average = 3,
	Bilinear = 2
};

enum class DrawingLayer : ubyte {
	Basic = 0,
	Opaque = 1,
	Decal = 2,
	Intersecting = 3,
	Alphatest = 4,
	Translucent = 5,
	Shadow = 6,

	// For internal Use
	Alphatest2 = 0x14,
	Translucent2 = 0x15,
	Shadow2 = 0x16
};

enum class ForceIntensity : ubyte {
	Fast = 0,
	Medium = 1,
	Slow = 2,
	VerySlow = 3
};

enum class TextureFormat : ubyte {
	RGBA16 = 2,
	RGBA32 = 3,
	YUV16 = 6, // Not yet implemented
	CI4 = 8, // Not yet implemented
	CI8 = 9, // Not yet implemented
	IA4 = 12,
	IA8 = 13,
	IA16 = 14,
	I4 = 16,
	I8 = 17
};

enum class CollisionType : ubyte {
	Normal = 0x00,
	Lava = 0x01,
	HangableCeiling = 0x05,
	Slow = 0x09,
	DeathFloor = 0x0A,
	FlowingWater = 0x0E,
	NoCollision = 0x12,
	VerySlippery = 0x13,
	Slippery = 0x14,
	Climbable = 0x15,
	InstantWarp1 = 0x1B,
	InstantWarp2 = 0x1C,
	InstantWarp3 = 0x1D,
	InstantWarp4 = 0x1E,
	ShallowSinkingSand = 0x21,
	LethalSinkingSand = 0x22,
	Quicksand = 0x23,
	DeepFlowingSand = 0x24,
	ShallowFlowingSand = 0x25,
	FlowingSand = 0x27,
	AlternateSound = 0x29,
	SlipperyAlternateSound = 0x2A,
	HorizontalWind = 0x2C,
	MovingQuicksand = 0x2D,
	Ice = 0x2E,
	WingCapWarp = 0x2F,
	Hard = 0x30,
	Warp = 0x32,
	TimerStart = 0x33,
	TimerEnd = 0x34,
	HardSlippery = 0x35,
	HardVerySlippery = 0x36,
	HardNotSlippery = 0x37,
	VerticalWind = 0x38,
	CameraBoundary = 0x72,
	NoCameraCollision = 0x76,
	VanishCap = 0x7B
};

enum class LevelId : ubyte {
	CastleGrounds = 16,
	CastleInterior = 6,
	CastleCourtyard = 26,

	BobOmbBattlefield = 9,
	WhompsFortress = 24,
	JollyRodgerBay = 12,
	CoolCoolMountain = 5,
	BigBoosHaunt = 4,
	HazyMazeCave = 7,
	LethalLavaLand = 22,
	ShiftingSandLand = 8,
	DireDireDocks = 23,
	SnowmansLands = 10,
	WetDryWorld = 11,
	TallTallMountain = 36,
	TinyHugeIsland = 13,
	TickTockClock = 14,
	RainbowRide = 15,

	BowserInTheDarkWorld = 17,
	BowserInTheFireSea = 19,
	BowserInTheSky = 21,
	Bowser1 = 30,
	Bowser2 = 33,
	Bowser3 = 34,

	TowerOfTheWingCap = 29,
	CavernOfTheMetalCap = 28,
	VanishCapUnderTheMoat = 18,

	PeachsSecretSlide = 27,
	SecretAquarium = 20,
	WingedMarioOverTheRainbow = 31,

	EndScreen = 25,

	INVALID = 0xFF
};

enum class CourseId : ubyte {
	Overworld = 0,
	BobOmbBattlefield = 1,
	WhompsFortress = 2,
	JollyRodgerBay = 3,
	CoolCoolMountain = 4,
	BigBoosHaunt = 5,
	HazyMazeCave = 6,
	LethalLavaLand = 7,
	ShiftingSandLand = 8,
	DireDireDocks = 9,
	SnowmansLands = 10,
	WetDryWorld = 11,
	TallTallMountain = 12,
	TinyHugeIsland = 13,
	TickTockClock = 14,
	RainbowRide = 15,
	BowserInTheDarkWorld = 16,
	BowserInTheFireSea = 17,
	BowserInTheSky = 18,
	PeachsSecretSlide = 19,
	CavernOfTheMetalCap = 20,
	TowerOfTheWingCap = 21,
	VanishCapUnderTheMoat = 22,
	WingedMarioOverTheRainbow = 23,
	SecretAquarium = 24,
	EndScreen = 25,

	INVALID = 0x99
};

enum class HAlign : ubyte {
	Left = 0,
	Centre = 1,
	Right = 2
};

enum class VAlign : ubyte {
	Top = 0,
	Centre = 1,
	Bottom = 2
};

enum class BackgroundImage : ubyte {
	Ocean = 0,
	Fire = 1,
	SunkenCity = 2,
	AboveTheClouds = 3,
	Mountain = 4,
	Desert = 5,
	NightTime = 6,
	Cavern = 7,
	JrbClouds = 8,
	PurpleSky = 9,

	CustomBackgroundStart = 10, // 10 - 33 represent custom backgrounds

	None = 0xFF
};

enum class LevelBoundarySize : ubyte {
	Normal = 0,
	Doubled = 1,
	Maximum = 2
};

enum class ShadowType : ubyte {
	Circle = 1,
	Square = 11,
	Spindel = 50,
	Whomp = 51
};

enum class ObjectType : ubyte {
	Simple = 0,
	Partial = 1,
	Advanced = 2
};

enum class ModulePropertyType : ubyte {
	Integer = 0,
	Float = 1,
	Boolean = 2,
	String = 3,
	Colour = 4,
	Angle = 5,
	Pointer = 6
};

enum class ModuleColourFormat : ubyte {
	RGBA32 = 0,
	RGBA16 = 1,
	Components = 2
};

enum class ModuleHook : ubyte {
	MarioStepBegin = 0,
	MarioStepEnd = 1,
	LevelLoad = 2,
	AreaLoad = 3,
	GameStart = 4,
	RenderHUD = 5,
	FrameEnd = 6,
	MarioAction = 7,
	Custom = 8
};

enum class MarioActionCategory : ushort {
	Stationary = 0x000,
	Moving = 0x040,
	Airborne = 0x080,
	Submerged = 0x0C0,
	Cutscene = 0x100,
	Automatic = 0x140,
	Object = 0x180,

	NotApplicable = 0
};

enum class MarioActionFlag : uint {
	Stationary = 0x00000200, // Mario model is always high-poly, and swim speed is lowered every frame
	Moving = 0x00000400, // Decreases bouyancy, lets you continue moving out of a punch, and changes interaction with wind
	Air = 0x00000800,
	Intangible = 0x00001000,
	Swimming = 0x00002000,
	MetalWater = 0x00004000,
	ShortHitbox = 0x00008000,
	RidingShell = 0x00010000,
	Invulnerable = 0x00020000,
	Sliding = 0x00040000, // Only seems to affect Klepto's behaviour?
	Diving = 0x00080000,
	OnPole = 0x00100000,
	Hanging = 0x00200000,
	Idle = 0x00400000,
	Attacking = 0x00800000,
	AllowVerticalWind = 0x01000000,
	ControlJumpHeight = 0x02000000,
	AllowFirstPerson = 0x04000000,
	AllowExitCourse = 0x08000000,
	HandsOpen = 0x10000000, // Mario has open hands instead of a closed fist (used for swimming and flying actions)
	HeadPivot = 0x20000000,
	UserDefined = 0x40000000, // Unused
	ThrowingObject = 0x80000000,

	None = 0,
	NotApplicable = 0
};

enum class ShimType : ubyte {
	NotApplicable = 0,
	Head = 1,
	Tail = 2,
	Word = 3,
	Jump = 4
};

enum class HookPriority : ubyte {
	First = 0,
	Early = 1,
	Middle = 2,
	Late = 3,
	Last = 4
};

enum class MuteBehaviour : ubyte {
	Ignore = 0x00,
	Undefined = 0x10,
	Quiet = 0x20,
	Mute = 0x40,
	Pause = 0x80
};

enum class TerrainSound : ubyte {
	Default = 0,
	Grass = 1,
	Water = 2,
	Stone = 3,
	Creaking = 4,
	Snow = 5,
	Ice = 6,
	Sand = 7
};

enum class CameraTriggerType : ubyte {
	Vanilla = 0,
	LEGACY_Boss = 1, // Now unused
	Custom = 2,
	Module = 3
};

enum class BehaviourType : ubyte {
	Vanilla = 0,
	AsmReference = 1,
	Autogenerated = 2
};

enum class SimpleBehaviourInstruction : ubyte {
	Wait = 0x0,
	Move = 0x1,
	Rotate = 0x2,
	MoveAndRotate = 0x3
};

enum class CollisionDistanceMode : ubyte {
	Minimal,
	Suggested,
	Infinite,
	Manual
};

#define DECLARE_FLAG_OPERATIONS( FlagType, NativeType ) \
	inline FlagType operator|( FlagType a, FlagType b ) { \
		return static_cast<FlagType>( static_cast<NativeType>( a ) | static_cast<NativeType>( b ) ); \
	} \
\
	inline FlagType operator&( FlagType a, FlagType b ) { \
		return static_cast<FlagType>( static_cast<NativeType>( a ) & static_cast<NativeType>( b ) ); \
	} \
\
	inline FlagType operator^( FlagType a, FlagType b ) { \
		return static_cast<FlagType>( static_cast<NativeType>( a ) ^ static_cast<NativeType>( b ) ); \
	} \
\
	inline FlagType operator~( FlagType f ) { \
		return static_cast<FlagType>( std::numeric_limits<NativeType>::max() - static_cast<NativeType>( f ) ); \
	} \
\
	inline FlagType &operator|=( FlagType &a, FlagType b ) { \
		return a = a | b; \
	} \
\
	inline FlagType &operator&=( FlagType &a, FlagType b ) { \
		return a = a & b; \
	} \
\
	inline FlagType &operator^=( FlagType &a, FlagType b ) { \
		return a = a ^ b; \
	}

DECLARE_FLAG_OPERATIONS( MarioActionFlag, uint )
DECLARE_FLAG_OPERATIONS( MuteBehaviour, ubyte )
DECLARE_FLAG_OPERATIONS( SimpleBehaviourInstruction, ubyte )

#undef DECLARE_FLAG_OPERATIONS

namespace Enum {
	template<typename E> const char *toString( E );
	template<typename E> const std::vector<E> &values();

	template<> const char *toString<LevelId>( LevelId levelId );
	template<> const std::vector<LevelId> &values();

	template<> const char *toString<CameraPreset>( CameraPreset camera );
	template<> const std::vector<CameraPreset> &values();

	template<> const char *toString<EnvironmentEffect>( EnvironmentEffect envfx );
	template<> const std::vector<EnvironmentEffect> &values();

	template<> const char *toString<DrawingLayer>( DrawingLayer layer );
	template<> const std::vector<DrawingLayer> &values();

	template<> const char *toString<TextureFormat>( TextureFormat format );
	template<> const std::vector<TextureFormat> &values();

	template<> const char *toString<CollisionType>( CollisionType type );
	template<> const std::vector<CollisionType> &values();

	template<> const char *toString<ShadowType>( ShadowType type );
	template<> const std::vector<ShadowType> &values();

	template<> const char *toString<TextureFiltering>( TextureFiltering method );
	template<> const std::vector<TextureFiltering> &values();

	template<> const char *toString<ModulePropertyType>( ModulePropertyType type );
	template<> const std::vector<ModulePropertyType> &values();

	template<> const char *toString<ModuleColourFormat>( ModuleColourFormat format );
	template<> const std::vector<ModuleColourFormat> &values();

	template<> const char *toString<ModuleHook>( ModuleHook hook );
	template<> const std::vector<ModuleHook> &values();

	template<> const char *toString<MarioActionCategory>( MarioActionCategory category );
	template<> const std::vector<MarioActionCategory> &values();

	template<> const char *toString<MarioActionFlag>( MarioActionFlag flag );
	template<> const std::vector<MarioActionFlag> &values();

	template<> const char *toString<ShimType>( ShimType type );
	template<> const std::vector<ShimType> &values();

	template<> const char *toString<HookPriority>( HookPriority order );
	template<> const std::vector<HookPriority> &values();
}

namespace Collision {

	inline bool hasDirection( CollisionType type ) {
		switch( type ) {
			case CollisionType::FlowingWater:
			case CollisionType::DeepFlowingSand:
			case CollisionType::ShallowFlowingSand:
			case CollisionType::FlowingSand:
			case CollisionType::HorizontalWind:
			case CollisionType::MovingQuicksand:
				return true;
			default:
				return false;
		}
	}

}

#endif /* CORE_ENUMS_HPP_ */
