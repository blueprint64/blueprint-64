#ifndef SRC_CORE_ENDING_HPP_
#define SRC_CORE_ENDING_HPP_

#include <ostream>
#include "src/types.hpp"

namespace EndScreen {

	enum class ResizeMethod : ubyte {
		Centre = 0,
		Fill = 1,
		Stretch = 2
	};

	bool saveImage( const string &imgPath, ResizeMethod resizeMethod );
	void removeImage();
	bool hasCustomImage();

	void import( std::ostream &rom );
	void revert( std::ostream &rom );

}



#endif /* SRC_CORE_ENDING_HPP_ */
