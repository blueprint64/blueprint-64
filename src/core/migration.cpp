#include "src/core/migration.hpp"

#include <cassert>

static Version s_schemaVersion = Version{ 0xFFFF, 0xFFFF, 0xFFFF };
static bool s_hasContext = false;

DeserializationContext::DeserializationContext( const Version &schemaVersion ) {
	assert( !s_hasContext );
	s_schemaVersion = schemaVersion;
	s_hasContext = true;
}

DeserializationContext::~DeserializationContext() {
	assert( s_hasContext );
	s_schemaVersion = Version{ 0xFFFF, 0xFFFF, 0xFFFF };
	s_hasContext = false;
}

const Version &DeserializationContext::schemaVersion() {
	return s_schemaVersion;
}

DeserializationContext DeserializationContext::create( const Version &schemaVersion ) {
	return DeserializationContext( schemaVersion );
}
