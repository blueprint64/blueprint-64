#include "src/core/storage.hpp"

#include <cstdlib>
#include <iostream>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/core/json.hpp"
#include "src/core/exceptions.hpp"

using namespace std;

static const AppPreferences &defaultPrefs() {
	static const AppPreferences s_default = AppPreferences{
		/* prefVersion */ CurrentVersion::PreferencesSchema,
		/* openOnStart */ true,
		/* nativeMenuBar */ false,
		/* defaultScale */ 100.0,
		/* emuConfig */ Emulator::EmulatorSettings::parseJSON( Json() ),
	#if defined(_WIN32)
		/* theme */ "Fusion", // theme kind of sucks, but EVERY Windows theme is bad
		/* sevenZipPath */ "C:\\Program Files\\7-Zip\\7z.exe"
	#elif !defined(__linux__) && !defined(__unix__)
		/* theme */ "", // System default
		/* sevenZipPath */ "" // Look in $PATH
	#else
		/* theme */ "Breeze"
	#endif
	};
	return s_default;
}



static AppPreferences parsePreferences( istream &file ) {
	Json json = Json::parse( file );
	const optional<Version> version = Version::tryParse( json["version"].get<string>() );
	if( !version.has_value() || version.value().major > CurrentVersion::PreferencesSchema.major ) {
		throw VersionError( "Preferences file is from a newer version of Bowser's Blueprints that is not compatible with this version." );
	}

	AppPreferences prefs = AppPreferences{
		CurrentVersion::PreferencesSchema,
		json["openOnStart"].getOrDefault<bool>( defaultPrefs().openOnStart ),
		json["nativeMenuBar"].getOrDefault<bool>( defaultPrefs().nativeMenuBar ),
		json["defaultScale"].getOrDefault<double>( defaultPrefs().defaultScale ),
		Emulator::EmulatorSettings::parseJSON( json["emuConfig"] ),
		json["theme"].getOrDefault<string>( "Breeze" ),
#if !defined(__linux__) && !defined(__unix__)
		json["7zipPath"].getOrDefault<string>( defaultPrefs().sevenZipPath )
#endif
	};

	if( version < Version{ 3, 0, 0 } ) {
		prefs.emuConfig.preferredEmulator = Emulator::Emulator::ParallelLauncher;
	}

	return prefs;
};

AppPreferences AppPreferences::load() {
	const fs::path cfgPath = AppData::configDir() / "preferences.json";
	const fs::file_status status = fs::status( cfgPath );

	switch( status.type() ) {
		case fs::file_type::regular:
		case fs::file_type::symlink: {
			FileReadStream jsonStream( cfgPath.u8string() );
			if( !jsonStream.bad() && !jsonStream.fail() ) {
				try {
					return parsePreferences( jsonStream );
				} catch( const std::exception &ex ) {
					cerr << "Failed to parse preferences file: " << ex.what();
				}
			}
			[[fallthrough]];
		}
		case fs::file_type::none:
		case fs::file_type::not_found: {
			try {
				defaultPrefs().save();
			} catch( const std::exception &ex ) {
				cerr << "Failed to create preferences file: " << ex.what();
			}
			return defaultPrefs();
		}
		default: return defaultPrefs();
	}

}

void AppPreferences::save() const {
	fs::path cfgPath = AppData::configDir();
	fs::create_directories( cfgPath );
	cfgPath /= "preferences.json";

	FileWriteStream jsonStream( cfgPath.u8string() );
	JsonWriter jw( &jsonStream, true );

	jw.writeObjectStart();
		jw.writeProperty( "version", prefVersion.toString() );
		jw.writeProperty( "openOnStart", openOnStart );
		jw.writeProperty( "nativeMenuBar", nativeMenuBar );
		jw.writeProperty( "defaultScale", defaultScale );
		jw.writePropertyName( "emuConfig" );
		emuConfig.writeJson( jw );
		jw.writeProperty( "theme", theme );
#if !defined(__linux__) && !defined(__unix__)
		jw.writeProperty( "7zipPath", sevenZipPath );
#endif
	jw.writeObjectEnd();
	jsonStream.put( '\n' );
}

AppPreferences &AppPreferences::current() {
	static AppPreferences s_preferences = load();
	return s_preferences;
}

static const list<string> initHistory() noexcept {
	list<string> history;
	try {
		const fs::path path = AppData::dataDir() / "history.json";
		if( fs::exists( path ) ) {
			FileReadStream file( path.u8string() );
			JArray recent = Json::parse( file ).array();
			for( const Json &j : recent ) {
				history.push_back( j.get<string>() );
				if( history.size() >= 8 ) {
					return history;
				}
			}
		}
	} catch( ... ) {}
	return history;
}

static list<string> &getHistory() noexcept {
	static list<string> s_history = initHistory();
	return s_history;
}

const list<string> &RecentProjects::get() noexcept {
	return getHistory();
}

static bool removeFromHistory( const string &filePath ) {
	list<string> &s_history = getHistory();
	for( auto i = s_history.begin(); i != s_history.end(); i++ ) {
		if( filePath == *i ) {
			s_history.erase( i );
			return true;
		}
	}
	return false;
}

static void commitHistory() noexcept {
	list<string> &s_history = getHistory();
	try {
		fs::path path = AppData::dataDir();
		fs::create_directories( path );
		path /= "history.json";
		FileWriteStream file( path.u8string() );
		JsonWriter jw( &file, false );
		jw.writeArrayStart();
		for( const string &project : s_history ) {
			jw.writeString( project );
		}
		jw.writeArrayEnd();
	} catch( ... ) {
		std::cerr << "Failed to save file history." << std::endl << std::flush;
	}
}

void RecentProjects::remove( const string &filePath ) {
	if( removeFromHistory( filePath ) ) {
		commitHistory();
	}
}

void RecentProjects::push( const string &filePath ) {
	list<string> &s_history = getHistory();
	if( !s_history.empty() && s_history.front() == filePath ) {
		return;
	}

	removeFromHistory( filePath );
	if( s_history.size() >= 8 ) {
		s_history.pop_back();
	}
	s_history.push_front( filePath );
	commitHistory();
}
