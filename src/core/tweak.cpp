#include "src/core/tweak.hpp"

#include <regex>
#include <algorithm>
#include <cassert>
#include <cctype>

#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/music.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/enums.hpp"
#include "src/core/util.hpp"
#include "src/core/baserom.hpp"

using namespace std;

static void WriteStaticPatches( const vector<StaticPatch> &segments, ostream &rom, bool revert ) {
	for( const StaticPatch &patch : segments ) {
		rom.seekp( patch.seekPosn );
		if( revert ) {
			BaseRom::revert( rom, (uint)patch.patchData.size() );
		} else {
			rom.write( (char*)patch.patchData.data(), patch.patchData.size() );
		}
	}
};

void WriteIntegerValue( uint word, ValueType type, ostream &rom ) {
	switch( type ) {
		case ValueType::Byte: {
			const ubyte value = word & 0xFF;
			rom.write( (char*)&value, 1 );
			return;
		}
		case ValueType::NegatedByte: {
			const ubyte value = ( 0x100 - ( word & 0x7F ) ) & 0xFF;
			rom.write( (char*)&value, 1 );
			return;
		}
		case ValueType::Short: {
			HalfWord::fromUShort( word & 0xFFFF ).writeToRom( rom );
			return;
		}
		case ValueType::NegatedShort: {
			HalfWord hw = HalfWord::fromUShort( word & 0xFFFF );
			hw.asSShort() *= -1;
			hw.writeToRom( rom );
			return;
		}
		case ValueType::Degrees: {
			HalfWord::fromUShort( ( 0x10000 * ( word % 360 ) ) / 360 ).writeToRom( rom );
			return;
		}
		case ValueType::Int: {
			Word::fromUInt( word ).writeToRom( rom );
			return;
		}
		case ValueType::NegatedInt: {
			Word w = Word::fromUInt( word );
			w.asUInt() *= -1;
			w.writeToRom( rom );
			return;
		}
		case ValueType::Float32u: {
			HalfWord::fromUpperFloat( (float)word ).writeToRom( rom );
			return;
		}
		case ValueType::Float32l: {
			HalfWord::fromLowerFloat( (float)word ).writeToRom( rom );
			return;
		}
		case ValueType::Float32: {
			Word::fromFloat( (float)word ).writeToRom( rom );
			return;
		}
		case ValueType::Float64: {
			DoubleWord::fromDouble( (double)word ).writeToRom( rom );
			return;
		}
		default:
			throw FileParserException( "Internal Error: Invalid value type." );
	}
};

static ValueType ParseValueType( const string &typeString ) {
	if( typeString == "byte" ) {
		return ValueType::Byte;
	} else if( typeString == "byte.neg" ) {
		return ValueType::NegatedByte;
	} else if( typeString == "short" ) {
		return ValueType::Short;
	} else if( typeString == "short.neg" ) {
		return ValueType::NegatedShort;
	} else if( typeString == "int" ) {
		return ValueType::Int;
	} else if( typeString == "int.neg" ) {
		return ValueType::NegatedInt;
	} else if( typeString == "degrees" ) {
		return ValueType::Degrees;
	} else if( typeString == "float32.upper" || typeString == "float16" ) {
		return ValueType::Float32u;
	} else if( typeString == "float32.lower" ) {
		return ValueType::Float32l;
	} else if( typeString == "float32" ) {
		return ValueType::Float32;
	} else if( typeString == "float64" ) {
		return ValueType::Float64;
	} else if( typeString == "colour16" ) {
		return ValueType::Colour16;
	} else if( typeString == "colour24" ) {
		return ValueType::Colour24;
	} else if( typeString == "colour32" ) {
		return ValueType::Colour32;
	} else throw FileParserException( "Unknown value type '" + typeString + "'." );
}

void BasicTweakDefinition::applyOrRevert( ostream &rom, [[maybe_unused]] Word value, bool revert ) const {
	WriteStaticPatches( m_staticPatches, rom, revert );
}

void ITweakDefinitionWithTransforms::applyOrRevert( ostream &rom, Word value, bool revert ) const {
	WriteStaticPatches( m_staticPatches, rom, revert );
	for( const DynamicPatch &patch : m_dynamicPatches ) {
		if( revert && patch.noRevert ) {
			continue;
		}

		rom.seekp( patch.seekPosn );
		if( patch.transform.has_value() ) {
			if( patch.transform.value() >= m_transforms.size() ) {
				throw FileParserException( "Transform index out of range." );
			}
			const IntegerTransformChain &transform = m_transforms[patch.transform.value()];
			uint result = value.asUInt();
			for( const IntegerTransform &operation : transform ) {
				result = operation( result );
			}
			WriteIntegerValue( result, patch.type, rom );
		} else {
			WriteIntegerValue( value.asUInt(), patch.type, rom );
		}
	}
}

void FloatTweakDefinition::applyOrRevert( ostream &rom, Word value, bool revert ) const {
	WriteStaticPatches( m_staticPatches, rom, revert );
	for( const DynamicPatch &patch : m_dynamicPatches ) {
		if( revert && patch.noRevert ) {
			continue;
		}

		rom.seekp( patch.seekPosn );
		double transformedValue = value.asFloat();
		if( patch.transform.has_value() ) {
			if( patch.transform >= m_transforms.size() ) {
				throw FileParserException( "Transform index out of range." );
			}
			const FloatTransformChain &transform = m_transforms[patch.transform.value()];
			for( const FloatTransform &operation : transform ) {
				transformedValue = operation( transformedValue );
			}
		}

		switch( patch.type ) {
			case ValueType::Float32u: {
				HalfWord::fromUpperFloat( (float)transformedValue ).writeToRom( rom );
				break;
			}
			case ValueType::Float32l: {
				HalfWord::fromLowerFloat( (float)transformedValue ).writeToRom( rom );
				break;
			}
			case ValueType::Float32: {
				value.writeToRom( rom );
				break;
			}
			case ValueType::Float64: {
				DoubleWord::fromDouble( transformedValue ).writeToRom( rom );
				break;
			}
			default:
				WriteIntegerValue( (uint)transformedValue, patch.type, rom );
		}
	}
}

void ColourTweakDefinition::applyOrRevert( ostream &rom, Word value, bool revert ) const {
	WriteStaticPatches( m_staticPatches, rom, revert );
	const ColourRGBA32 colour32 = ColourRGBA32( value.asUInt() );
	const ColourRGB24 colour24 = ColourRGB24( colour32.red, colour32.green, colour32.blue );
	const ColourRGBA5551 colour16 = ColourRGBA5551(
		colour32.red >> 3,
		colour32.green >> 3,
		colour32.blue >> 3,
		m_supportsAlpha ? ( colour32.alpha > 0x7F ) : true
	);

	for( const DynamicPatch &patch : m_dynamicPatches ) {
		if( revert && patch.noRevert ) {
			continue;
		}

		rom.seekp( patch.seekPosn );
		switch( patch.type ) {
			case ValueType::Colour16:
				colour16.writeToRom( rom );
				break;
			case ValueType::Colour24:
				colour24.writeToRom( rom );
				break;
			case ValueType::Colour32:
				colour32.writeToRom( rom );
				break;
			default:
				throw FileParserException( "Cannot write non-colour type in colour tweak." );
		}
	}
}

inline static const JArray asOptionalArray( const Json &json ) {
	return json.exists() ? json.array() : JArray();
}

ITweakDefinition::ITweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	m_name( tweakJson["name"].get<string>() ),
	m_description( tweakJson["description"].get<string>() ),
	m_pack( pack ) {}

static inline bool isValidPatch( const string &patchString ) {
	if( patchString.empty() ) return false;

	static constexpr ubyte UPPER_NIBBLE = 0;
	//static constexpr ubyte LOWER_NIBBLE = 1;
	static constexpr ubyte EOF_OR_SPACE = 2;

	ubyte expectedToken = UPPER_NIBBLE;
	for( char c : patchString ) {
		if( expectedToken == EOF_OR_SPACE ) {
			if( !std::isspace( c ) ) return false;
		} else {
			if( !std::isxdigit( c ) ) return false;
		}
		expectedToken = (expectedToken + 1) % 3;
	}

	return expectedToken == EOF_OR_SPACE;
}

static void ParsePatchData( const string &patchString, vector<ubyte> &byteVector ) {
	assert( patchString.length() % 3 == 2 );
	for( size_t i = 0; i < patchString.length(); i += 3 ) {
		const string byteString = patchString.substr( i, 2 );
		byteVector.push_back( stoi( byteString, nullptr, 16 ) );
	}
};

ISingularTweakDefinition::ISingularTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	ITweakDefinition( tweakJson, pack ),
	m_id( Uuid::parse( tweakJson["uuid"].get<string>() ) )
{};

INonSwitchSingularTweakDefinition::INonSwitchSingularTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	ISingularTweakDefinition( tweakJson, pack )
{
	const JArray &staticPatches = asOptionalArray( tweakJson["patch_static"] );
	const JArray &valuePatches = asOptionalArray( tweakJson["patch_value"] );
	if( staticPatches.size() == 0 && valuePatches.size() == 0 ) {
		throw FileParserException( "Tweak has no patch data." );
	}

	m_staticPatches.reserve( staticPatches.size() );
	for( size_t i = 0; i < staticPatches.size(); i++ ) {
		const Json &patch = staticPatches[i];
		const uint addr = stoi( patch["seek"].get<string>(), nullptr, 16 );

		const string &applyPatch = patch["apply"].get<string>();
		if( !isValidPatch( applyPatch ) ) {
			throw FileParserException( "Invalid static patch data." );
		}

		m_staticPatches.push_back( StaticPatch{
			addr,
			vector<ubyte>()
		});
		m_staticPatches[i].patchData.reserve( (applyPatch.length() + 1) / 3 );
		ParsePatchData( applyPatch, m_staticPatches[i].patchData );
	}

	m_dynamicPatches.reserve( valuePatches.size() );
	for( size_t i = 0; i < valuePatches.size(); i++ ) {
		const Json &jPatch = valuePatches[i];
		DynamicPatch patch;

		patch.seekPosn = stoi( jPatch["seek"].get<string>(), nullptr, 16 );
		patch.type = ParseValueType( jPatch["type"].get<string>() );
		if( jPatch["transform"].exists() ) {
			patch.transform = jPatch["transform"].get<ubyte>();
		}
		if( jPatch["no_revert"].exists() ) {
			patch.noRevert = jPatch["no_revert"].get<bool>();
		} else {
			patch.noRevert = false;
		}

		m_dynamicPatches.push_back( patch );
	}
}

template<typename T> static void AddCommonTransform(
	vector<function<T(T)>> &transformOps,
	string opname,
	T opval
) {
	using namespace std::placeholders;
	if( opname == "add" ) {
		transformOps.push_back( bind( std::plus<T>(), _1, opval ) );
	} else if( opname == "subtract" ) {
		transformOps.push_back( bind( std::minus<T>(), _1, opval ) );
	} else if( opname == "subtract_other" ) {
			transformOps.push_back( bind( std::minus<T>(), opval, _1 ) );
	} else if( opname == "multiply" ) {
		transformOps.push_back( bind( std::multiplies<T>(), _1, opval ) );
	} else if( opname == "divide" ) {
		transformOps.push_back( bind( std::divides<T>(), _1, opval ) );
	} else if( opname == "modulo" ) {
		if constexpr( std::is_integral_v<T> ) {
			transformOps.push_back( bind( std::modulus<T>(), _1, opval ) );
		} else {
			transformOps.push_back( bind( std::fmod<T,T>, _1, opval ) );
		}
	} else if( opname == "min" ) {
		transformOps.push_back( [=](T x){ return x < opval ? x : opval; } );
	} else if( opname == "max" ) {
		transformOps.push_back( [=](T x){ return x > opval ? x : opval; } );
	} else {
		throw FileParserException( "Unknown operation '" + opname + "'." );
	}
}

ITweakDefinitionWithTransforms::ITweakDefinitionWithTransforms( const Json &tweakJson, const TweakPack *pack ) :
	INonSwitchSingularTweakDefinition( tweakJson, pack )
{
	const JArray &transforms = asOptionalArray( tweakJson["transforms"] );
	if( transforms.size() > 0 ) {
		m_transforms.reserve( transforms.size() );
		for( size_t i = 0; i < transforms.size(); i++ ) {
			const Json &transform = transforms[i];
			const JArray &operations = transform["ops"].array();
			m_transforms.push_back( IntegerTransformChain() );
			IntegerTransformChain &transformOps = m_transforms[i];
			transformOps.reserve( operations.size() );
			for( size_t j = 0; j < operations.size(); j++ ) {
				string op = operations[j]["op"].get<string>();
				uint value = operations[j]["val"].get<uint>();

				using namespace std::placeholders;
				if( op == "left_shift" ) {
					transformOps.push_back( [=](uint x){ return (uint)(x << value); } );
				} else if( op == "right_shift" ) {
					transformOps.push_back( [=](uint x){ return (uint)(x >> value); } );
				} else if( op == "and" ) {
					transformOps.push_back( bind( std::bit_and<uint>(), _1 , value ) );
				} else if( op == "or" ) {
					transformOps.push_back( bind( std::bit_or<uint>(), _1, value ) );
				} else if( op == "left_shift_other" ) {
					transformOps.push_back( [=](uint x){ return (uint)(value << x); } );
				} else {
					AddCommonTransform( transformOps, op, value );
				}
			}
		}
	}
}

FloatTweakDefinition::FloatTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	INonSwitchSingularTweakDefinition( tweakJson, pack ),
	m_minValue( tweakJson["min"].get<float>() ),
	m_maxValue( tweakJson["max"].get<float>() ),
	m_defaultValue( tweakJson["default"].get<float>() ),
	m_decimalPrecision( tweakJson["precision"].getOrDefault<ubyte>( 2 ) )
{
	const JArray &transforms = asOptionalArray( tweakJson["transforms"] );
	if( transforms.size() > 0 ) {
		m_transforms.reserve( transforms.size() );
		for( size_t i = 0; i < transforms.size(); i++ ) {
			const Json &transform = transforms[i];
			const JArray &operations = transform["ops"].array();
			m_transforms.push_back( FloatTransformChain() );
			FloatTransformChain &transformOps = m_transforms[i];
			transformOps.reserve( operations.size() );
			for( size_t j = 0; j < operations.size(); j++ ) {
				string op = operations[j]["op"].get<string>();
				double value = operations[j]["val"].get<double>();

				if( op == "left_shift" || op == "right_shift" ){
					throw FileParserException( "Operation '" + op + "' is not valid for floating point tweaks." );
				} else {
					AddCommonTransform( transformOps, op, value );
				}
			}
		}
	}
}

BasicTweakDefinition::BasicTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	INonSwitchSingularTweakDefinition( tweakJson, pack )
{
	if( m_dynamicPatches.size() != 0 ) {
		throw FileParserException( "Basic tweak cannot have value patches" );
	}
}

IntegerTweakDefinition::IntegerTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	ITweakDefinitionWithTransforms( tweakJson, pack ),
	m_minValue( tweakJson["min"].get<uint>() ),
	m_maxValue( tweakJson["max"].get<uint>() ),
	m_defaultValue( tweakJson["default"].get<uint>() ),
	m_hex( tweakJson["hex"].getOrDefault( false ) ){}

template<typename E> bool buildEnumFromPreset( uint defaultValue, std::vector<EnumEntry> &entries, EnumEntry &defaultOut ) {
	const std::vector<E> &enumValues = Enum::values<E>();
	entries.reserve( enumValues.size() );
	bool foundDefault = false;
	for( E enumVal : Enum::values<E>() ) {
		EnumEntry entry;
		entry.name = Enum::toString<E>( enumVal );
		entry.value = (uint)enumVal;
		entries.push_back( entry );
		if( entry.value == defaultValue ) {
			foundDefault = true;
			defaultOut = entry;
		}
	}
	return foundDefault;
}

UserDefinedEnumTweakDefinition::UserDefinedEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	EnumTweakDefinition( tweakJson, pack )
{
	bool foundDefault = false;
	const uint defaultValue = tweakJson["default"].get<uint>();

	const JArray &jArray = tweakJson["values"].array();
	m_enumValues.reserve( jArray.size() );
	for( size_t i = 0; i < jArray.size(); i++ ) {
		const Json &enumEntry = jArray[i];
		m_enumValues.push_back( EnumEntry{
			enumEntry["name"].get<string>(),
			enumEntry["value"].get<uint>()
		});

		if( m_enumValues[i].value == defaultValue ) {
			foundDefault = true;
			m_defaultValue = m_enumValues[i];
		}
	}

	if( !foundDefault ) {
		m_defaultValue = EnumEntry{ "Default", defaultValue };
		m_enumValues.reserve( m_enumValues.size() + 1 );
		m_enumValues.push_back( m_defaultValue );
	}
}

PresetEnumTweakDefinition::PresetEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	EnumTweakDefinition( tweakJson, pack ),
	m_defaultValue( tweakJson["default"].get<uint>() )
{}

class LevelEnumTweakDefinition : public PresetEnumTweakDefinition {

	private:
	static inline string getLevelName( LevelId levelId ) {
		assert( Blueprint::current() != nullptr );
		if( Blueprint::current()->hasLevel( levelId ) ) {
			return Blueprint::current()->levels().at( levelId ).name;
		} else {
			return Enum::toString<LevelId>( levelId );
		}
	}

	public:
	LevelEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
		PresetEnumTweakDefinition( tweakJson, pack )
	{
		if( m_defaultValue < 4 || m_defaultValue > 36 || m_defaultValue == 32 || m_defaultValue == 35 ) {
			throw FileParserException( "Default enum value out of range for preset 'music'" );
		}
	}

	std::vector<EnumEntry> getPossibleValues() const override {
		std::vector<EnumEntry> values;
		values.reserve( 31 );
		for( LevelId level : Enum::values<LevelId>() ) {
			values.push_back({
				getLevelName( level ),
				(uint)level
			});
		}
		return values;
	}

	const EnumEntry getDefaultValue() const override {
		return EnumEntry{
			getLevelName( (LevelId)m_defaultValue ),
			m_defaultValue
		};
	}

};

class MusicEnumTweakDefinition : public PresetEnumTweakDefinition {

	public:
	MusicEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
		PresetEnumTweakDefinition( tweakJson, pack )
	{
		if( m_defaultValue > 34 ) {
			throw FileParserException( "Default enum value out of range for preset 'music'" );
		}
	}

	std::vector<EnumEntry> getPossibleValues() const override {
		std::vector<EnumEntry> values;
		uint i = 0;
		for( const MusicInfo &music : MusicData::getMusicInfo() ) {
			if( !music.isPlaceholder ) {
				values.push_back({ music.name, i });
			}
			i++;
		}
		values[0].name = "None";
		return values;
	}

	const EnumEntry getDefaultValue() const override {
		if( Blueprint::current()->customMusic().count( (ubyte)m_defaultValue ) ) {
			return EnumEntry{ Blueprint::current()->customMusic().at( (ubyte)m_defaultValue ).name, m_defaultValue };
		} else {
			return EnumEntry{ MusicData::getVanillaMusicName( (ubyte)m_defaultValue ), m_defaultValue };
		}
	}

};

SwitchTweakDefinition::SwitchTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	ISingularTweakDefinition( tweakJson, pack ),
	m_defaultCase( tweakJson["default"].get<ushort>() )
{
	const JArray &seekJson = tweakJson["seeks"].array();
	m_seeks.reserve( seekJson.size() );
	for( const Json &seek : seekJson ) {
		m_seeks.push_back( stoi( seek.get<string>(), nullptr, 16 ) );
	}

	const JArray &cases = tweakJson["patch_case"].array();
	if( m_defaultCase >= cases.size() ) {
		throw FileParserException( "Default case out of range." );
	}

	m_cases.reserve( cases.size() );
	std::vector<size_t> patchLengths;

	int i = 0;
	for( const Json &jCase : cases ) {
		CasePatch casePatch;
		casePatch.name = jCase["name"].get<string>();
		const JArray &patches = jCase["patches"].array();
		casePatch.patches.reserve( patches.size() );
		int j = 0;
		for( const Json &patch : patches ) {
			vector<ubyte> patchBytes;
			if( i > 0 ) {
				patchBytes.reserve( patchLengths.at( j ) );
			}
			ParsePatchData( patch.get<string>(), patchBytes );
			if( i == 0 ) {
				patchLengths.push_back( patchBytes.size() );
			} else if( patchLengths.at( j ) != patchBytes.size() ) {
				throw FileParserException( "Cases may not have different patch lengths" );
			}
			casePatch.patches.push_back( std::move( patchBytes ) );
			j++;
		}
		m_cases.push_back( std::move( casePatch ) );
		i++;
	}
}

void SwitchTweakDefinition::applyOrRevert( ostream &rom, Word value, [[maybe_unused]] bool revert ) const {
	const CasePatch &chosenCase = m_cases.at( value.asUInt() );
	for( size_t i = 0; i < m_seeks.size(); i++ ) {
		rom.seekp( m_seeks[i] );
		rom.write( (char*)chosenCase.patches[i].data(), chosenCase.patches[i].size() );
	}
}

static const regex ColourRegex(
	"#[0-9a-fA-F]{6}(?:[0-9a-fA-F]{2})?",
	regex_constants::ECMAScript | regex_constants::optimize
);

static ColourRGBA32 ParseColour( const string &hexCode ) {
	if( !regex_match( hexCode, ColourRegex ) ) {
		throw FileParserException( "Failed to parse colour '" + hexCode + "'." );
	}

	return ColourRGBA32(
		stoi( hexCode.substr( 1, 2 ), nullptr, 16 ),
		stoi( hexCode.substr( 3, 2 ), nullptr, 16 ),
		stoi( hexCode.substr( 5, 2 ), nullptr, 16 ),
		hexCode.length() > 7 ? stoi( hexCode.substr( 7, 2 ), nullptr, 16 ) : 0xFF
	);
}

ColourTweakDefinition::ColourTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	INonSwitchSingularTweakDefinition( tweakJson, pack ),
	m_supportsAlpha( tweakJson["alpha"].get<bool>() ),
	m_defaultColour( ParseColour( tweakJson["default"].get<string>() ) ) {}

static ISingularTweakDefinition *ParseSingularTweak( const Json &tweakJson, const TweakPack *pack ) {
	const string typeString = tweakJson["type"].get<string>();
	if( typeString == "basic" ) {
		return new BasicTweakDefinition( tweakJson, pack );
	} else if( typeString == "integer" ) {
		return new IntegerTweakDefinition( tweakJson, pack );
	} else if( typeString == "float" ) {
		return new FloatTweakDefinition( tweakJson, pack );
	} else if( typeString == "enum" ) {
		if( !tweakJson["preset"].exists() ) {
			return new UserDefinedEnumTweakDefinition( tweakJson, pack );
		}

		const string preset = tweakJson["preset"].get<string>();
		if( preset == "music" ) {
			return new MusicEnumTweakDefinition( tweakJson, pack );
		} else if( preset == "level" ) {
			return new LevelEnumTweakDefinition( tweakJson, pack );
		} else throw FileParserException( "Invalid enum preset: " + preset );
	} else if( typeString == "switch" ) {
		return new SwitchTweakDefinition( tweakJson, pack );
	} else if( typeString == "colour" ) {
		return new ColourTweakDefinition( tweakJson, pack );
	} else if( typeString == "color" ) {
		throw FileParserException( "Unknown tweak type 'color'. Did you mean 'colour'?" );
	} else if( typeString == "composite" ) {
		throw FileParserException( "Composite tweaks may not contain other composite tweaks." );
	} else {
		throw FileParserException( "Unknown tweak type '" + typeString + "'." );
	}
}

CompositeTweakDefinition::CompositeTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
	ITweakDefinition( tweakJson, pack )
{
	const JArray &components = tweakJson["components"].array();

	const size_t numComponents = components.size();
	if( numComponents == 0 ) {
		throw FileParserException( "Composite tweak must have at least one child tweak." );
	}

	m_components.reserve( numComponents );
	try {
		for( size_t i = 0; i < components.size(); i++ ) {
			ISingularTweakDefinition *childTweak = ParseSingularTweak( components[i], pack );
			m_components.push_back( childTweak );
		}
	} catch(...) {
		for( ISingularTweakDefinition *childTweak : m_components ) {
			delete childTweak;
		}
		m_components.clear();
		throw;
	}
}

vector<Uuid> TweakStore::loadFileAndGetConflicts( istream &jsonFile, bool builtin ) {
	Json tweakData = Json::parse( jsonFile );

	const std::optional<Version> schemaVersion = Version::tryParse( tweakData["version"].get<string>() );
	if( !schemaVersion.has_value() ) {
		throw FileParserException( "Tweak schema version is missing or invalid." );
	}

	if( schemaVersion.value().major > 1 ) {
		throw FileParserException( "Tweak schema version "s + schemaVersion.value().toString() + " is not supported." );
	}

	TweakPack *pack = nullptr;
	if( !builtin ) {
		const Json &packInfo = tweakData["pack_info"];
		Uuid packId = Uuid::parse( packInfo["uuid"].get<string>() );
		string packName = trim( packInfo["name"].get<string>() );

		if( packName.empty() ) {
			throw FileParserException( "Tweak pack must have a name." );
		}

		std::optional<Version> packVersion = Version::tryParse( packInfo["version"].getOrDefault<string>( "1.0.0" ) );
		if( !packVersion.has_value() ) {
			throw FileParserException( "Tweak pack has invalid version." );
		}
		if( packVersion.value().major > CurrentVersion::TweakSchema.major ) {
			throw FileParserException( "Tweak pack version not supported." );
		}

		if( m_tweakPacks.count( packId ) > 0 ) {
			removeTweakPack( packId );
		}

		m_tweakPacks[packId] = {
			packName,
			packId,
			packVersion.value()
		};
		pack = &m_tweakPacks.at( packId );
	}

	vector<Uuid> conflicts;
	const JArray &tweaks = tweakData["tweaks"].array();
	for( size_t i = 0; i < tweaks.size(); i++ ) {
		const Json &tweakJson = tweaks[i];

		if( tweakJson["type"].get<string>() == "composite" ) {
			CompositeTweakDefinition *tweak = new CompositeTweakDefinition( tweakJson, pack );
			m_publicTweaks.push_back( tweak );
			for( auto i = tweak->getComponents().begin(); i != tweak->getComponents().end(); i++ ) {
				ISingularTweakDefinition *componentTweak = *i;
				const Uuid &id = componentTweak->getUniqueId();
				if( m_tweakMap.find( id ) == m_tweakMap.end() ) {
					m_tweakMap[id] = componentTweak;
				} else {
					conflicts.push_back( id );
					delete componentTweak;
					*i = m_tweakMap[id];
				}
			}
		} else {
			ISingularTweakDefinition *tweak = ParseSingularTweak( tweakJson, pack );
			if( m_tweakMap.find( tweak->getUniqueId() ) == m_tweakMap.end() ) {
				m_publicTweaks.push_back( tweak );
				m_tweakMap[tweak->getUniqueId()] = tweak;
			} else {
				conflicts.push_back( tweak->getUniqueId() );
				delete tweak;
			}
		}
	}

	return conflicts;
};

void TweakStore::clear() {
	for( ITweakDefinition *tweak : m_publicTweaks ) {
		CompositeTweakDefinition *compositeTweak = dynamic_cast<CompositeTweakDefinition*>( tweak );
		if( compositeTweak != nullptr ) {
			for( ISingularTweakDefinition *subtweak : compositeTweak->getComponents() ) {
				delete subtweak;
			}
		}
		delete tweak;
	}
	m_publicTweaks.clear();
	m_tweakMap.clear();
};

void TweakStore::removeTweakPack( Uuid packId ) {
	for( auto i = m_tweakMap.begin(); i != m_tweakMap.end(); ) {
		if( i->second->getSource() != nullptr && i->second->getSource()->id == packId ) {
			i = m_tweakMap.erase( i );
		} else i++;
	}

	for( ITweakDefinition *&tweak : m_publicTweaks ) {
		if( tweak->isBuiltIn() || tweak->getSource()->id != packId ) continue;

		CompositeTweakDefinition *compositeTweak = dynamic_cast<CompositeTweakDefinition*>( tweak );
		if( compositeTweak != nullptr ) {
			for( ISingularTweakDefinition *subtweak : compositeTweak->getComponents() ) {
				delete subtweak;
			}
		}
		delete tweak;
		tweak = nullptr;
	}

	m_publicTweaks.erase(
		std::remove_if( m_publicTweaks.begin(), m_publicTweaks.end(), [](ITweakDefinition *tweak){ return tweak == nullptr; } ),
		m_publicTweaks.end()
	);
	m_tweakPacks.erase( packId );
}

TweakStore &TweakStore::instance() {
	static TweakStore tweakStore;
	return tweakStore;
}

std::vector<TweakPack> TweakStore::getInstalledTweakPacks() {
	std::vector<TweakPack> packs;
	const fs::path tweakDir = AppData::dataDir() / "tweak_packs";
	if( !fs::exists( tweakDir ) ) {
		return packs;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().u8string() );
			if( TweakStore::instance().tweakPacks().count( id ) > 0 ) {
				packs.push_back( TweakStore::instance().tweakPacks().at( id ) );
			}
		} catch( ... ) {}
	}

	return packs;
}

std::vector<Uuid> TweakStore::installTweakPack( const string &filePath ) {
	TweakStore tempStore;
	FileReadStream jsonFile( filePath );
	tempStore.loadFileAndGetConflicts( jsonFile, false );
	const Uuid packId = tempStore.tweakPacks().begin()->first;

	jsonFile.clear();
	jsonFile.close();

	fs::create_directories( AppData::dataDir() / "tweak_packs" );
	const fs::path savePath = AppData::dataDir() / "tweak_packs" / (packId.toString() + ".json");
	fs::tryCopyFileOverwrite( fs::to_path( filePath ), savePath );

	jsonFile.open( savePath, mode::read );
	return TweakStore::instance().loadFileAndGetConflicts( jsonFile, false );
}

void TweakStore::loadInstalledTweakPacks() {
	const fs::path tweakDir = AppData::dataDir() / "tweak_packs";
	if( !fs::exists( tweakDir ) ) return;

	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().u8string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().u8string() );
			if( m_tweakPacks.count( id ) > 0 ) continue;

			try {
				FileReadStream tweakFile( f.path().u8string() );
				loadFileAndGetConflicts( tweakFile, false );
			} catch( ... ) {
				removeTweakPack( id );
			}
		} catch( ... ) {}
	}
}

void TweakStore::uninstallTweakPack( const Uuid &packId ) {
	instance().removeTweakPack( packId );
	fs::remove( AppData::dataDir() / "tweak_packs" / (packId.toString() + ".json") );
}
