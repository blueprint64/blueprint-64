#include "src/core/enums.hpp"

#include <cassert>

template<> const std::vector<LevelId> &Enum::values() {
	static const std::vector<LevelId> s_values({
		LevelId::CastleGrounds,
		LevelId::CastleInterior,
		LevelId::CastleCourtyard,
		LevelId::BobOmbBattlefield,
		LevelId::WhompsFortress,
		LevelId::JollyRodgerBay,
		LevelId::CoolCoolMountain,
		LevelId::BigBoosHaunt,
		LevelId::HazyMazeCave,
		LevelId::LethalLavaLand,
		LevelId::ShiftingSandLand,
		LevelId::DireDireDocks,
		LevelId::SnowmansLands,
		LevelId::WetDryWorld,
		LevelId::TallTallMountain,
		LevelId::TinyHugeIsland,
		LevelId::TickTockClock,
		LevelId::RainbowRide,
		LevelId::BowserInTheDarkWorld,
		LevelId::BowserInTheFireSea,
		LevelId::BowserInTheSky,
		LevelId::Bowser1,
		LevelId::Bowser2,
		LevelId::Bowser3,
		LevelId::TowerOfTheWingCap,
		LevelId::CavernOfTheMetalCap,
		LevelId::VanishCapUnderTheMoat,
		LevelId::PeachsSecretSlide,
		LevelId::SecretAquarium,
		LevelId::WingedMarioOverTheRainbow,
		LevelId::EndScreen
	});
	return s_values;
}

template<> const char *Enum::toString<LevelId>( LevelId levelId ) {
	switch( levelId ) {
		case LevelId::CastleGrounds: return "Castle Grounds";
		case LevelId::CastleInterior: return "Castle Interior";
		case LevelId::BobOmbBattlefield: return "Bob-Omb Battlefield";
		case LevelId::WhompsFortress: return "Whomp's Fortress";
		case LevelId::CastleCourtyard: return "Castle Courtyard";
		case LevelId::JollyRodgerBay: return "Jolly Rodger Bay";
		case LevelId::CoolCoolMountain: return "Cool Cool Mountain";
		case LevelId::BigBoosHaunt: return "Big Boo's Haunt";
		case LevelId::HazyMazeCave: return "Hazy Maze Cave";
		case LevelId::LethalLavaLand: return "Lethal Lava Land";
		case LevelId::ShiftingSandLand: return "Shifting Sand Land";
		case LevelId::DireDireDocks: return "Dire, Dire Docks";
		case LevelId::SnowmansLands: return "Snowman's Land";
		case LevelId::WetDryWorld: return "Wet Dry World";
		case LevelId::TallTallMountain: return "Tall, Tall Mountain";
		case LevelId::TinyHugeIsland: return "Tiny-Huge Island";
		case LevelId::TickTockClock: return "Tick Tock Clock";
		case LevelId::RainbowRide: return "Rainbow Ride";
		case LevelId::BowserInTheDarkWorld: return "Bowser in the Dark World";
		case LevelId::BowserInTheFireSea: return "Bowser in the Fire Sea";
		case LevelId::BowserInTheSky: return "Bowser in the Sky";
		case LevelId::Bowser1: return "Bowser Fight 1";
		case LevelId::Bowser2: return "Bowser Fight 2";
		case LevelId::Bowser3: return "Bowser Fight 3";
		case LevelId::TowerOfTheWingCap: return "Tower of the Wing Cap";
		case LevelId::CavernOfTheMetalCap: return "Cavern of the Metal Cap";
		case LevelId::VanishCapUnderTheMoat: return "Vanish Cap Under the Moat";
		case LevelId::PeachsSecretSlide: return "Peach's Secret Slide";
		case LevelId::SecretAquarium: return "Secret Aquarium";
		case LevelId::WingedMarioOverTheRainbow: return "Winged Mario over the Rainbow";
		case LevelId::EndScreen: return "End Screen";
		default: return "UNKNOWN";
	}
}

template<> const char *Enum::toString<CameraPreset>( CameraPreset camera ) {
	assert( camera <= CameraPreset::SpiralStairs );
	static const char *s_names[18] = {
		"None",
		"Outer Radial",
		"Inner Radial",
		"Behind Mario",
		"Close Quarters",
		nullptr,
		"C-Up",
		nullptr,
		"Water Surface",
		"Slide Cam",
		"Cannon",
		"Boss Fight",
		"Parallel",
		"Elastic Pivot",
		"Semi-Cardinal",
		nullptr,
		"Free Roam",
		"Spiral Stairs"
	};
	return s_names[(ubyte)camera];
}

template<> const std::vector<CameraPreset> &Enum::values() {
	static const std::vector<CameraPreset> s_values({
		CameraPreset::None,
		CameraPreset::OuterRadial,
		CameraPreset::InnerRadial,
		CameraPreset::BehindMario,
		CameraPreset::CloseQarters,
		CameraPreset::FirstPerson,
		CameraPreset::WaterSurface,
		CameraPreset::Slide,
		CameraPreset::Cannon,
		CameraPreset::BossFight,
		CameraPreset::Parallel,
		CameraPreset::ElasticPivot,
		CameraPreset::SemiCardinal,
		CameraPreset::FreeRoam,
		CameraPreset::SpiralStairs
	});
	return s_values;
};

template<> const char *Enum::toString<EnvironmentEffect>( EnvironmentEffect envfx ) {
	switch( envfx ) {
		case EnvironmentEffect::None: return "None";
		case EnvironmentEffect::Snow: return "Snow";
		case EnvironmentEffect::Blizzard: return "Blizzard";
		case EnvironmentEffect::LavaBubbles: return "Lava Bubbles";
		case EnvironmentEffect::Water: return "Water Bubbles";
		case EnvironmentEffect::Flowers: return "Flowers";
		default: return nullptr;
	}
}

template<> const std::vector<EnvironmentEffect> &Enum::values<EnvironmentEffect>() {
	static const std::vector<EnvironmentEffect> s_values = {
		EnvironmentEffect::None,
		EnvironmentEffect::Snow,
		EnvironmentEffect::Blizzard,
		EnvironmentEffect::LavaBubbles,
		EnvironmentEffect::Water,
		EnvironmentEffect::Flowers
	};
	return s_values;
}

template<> const char *Enum::toString<DrawingLayer>( DrawingLayer layer ) {
	static const char *s_names[8] = {
		"0 - Basic",
		"1 - Opaque",
		"2 - Decal",
		"3 - Intersecting",
		"4 - Alphatest",
		"5 - Translucent",
		"6 - Shadow"
	};
	assert( layer <= DrawingLayer::Shadow );
	return s_names[(ubyte)layer];
}
template<> const std::vector<DrawingLayer> &Enum::values() {
	static const std::vector<DrawingLayer> s_values = {
		DrawingLayer::Basic,
		DrawingLayer::Opaque,
		DrawingLayer::Decal,
		DrawingLayer::Intersecting,
		DrawingLayer::Alphatest,
		DrawingLayer::Translucent,
		DrawingLayer::Shadow
	};
	return s_values;
}

template<> const char *Enum::toString<TextureFormat>( TextureFormat format ) {
	static const char *s_names[16] = {
		"16-bit RGB + Alpha Bit",
		"32-bit RGBA",
		nullptr,
		nullptr,
		"16-bit YUV",
		nullptr,
		"4-bit Indexed",
		"8-bit Indexed",
		nullptr,
		nullptr,
		"4-bit Greyscale + Alpha Bit",
		"8-bit Greyscale + Alpha",
		"16-bit Greyscale + Alpha",
		nullptr,
		"4-bit Greyscale",
		"8-bit Greyscale"
	};
	assert( format >= TextureFormat::RGBA16 && format <= TextureFormat::I8 );
	return s_names[(ubyte)format - 2];
}
template<> const std::vector<TextureFormat> &Enum::values() {
	static const std::vector<TextureFormat> s_values = {
		TextureFormat::RGBA32,
		TextureFormat::RGBA16,
		TextureFormat::IA16,
		/* TextureFormat::YUV16, */
		/* TextureFormat::CI8, */
		TextureFormat::I8,
		TextureFormat::IA8,
		/* TextureFormat::CI4, */
		TextureFormat::I4,
		TextureFormat::IA4
	};
	return s_values;
}

template<> const char *Enum::toString<CollisionType>( CollisionType type ) {
	static const char *s_unknown = "Unknown";

	/* 0x1B .. 0x38 */
	static const char *s_chunk[30] = {
		"Instant Warp 1",
		"Instant Warp 2",
		"Instant Warp 3",
		"Instant Warp 4",
		s_unknown,
		s_unknown,
		"Shallow Sinking Sand",
		"Lethal Sinking Sand",
		"Quicksand",
		"Deep Flowing Sand",
		"Shallow Flowing Sand",
		s_unknown,
		"Flowing Sand",
		s_unknown,
		"Alternate Sound",
		"Slippery & Alternate Sound",
		s_unknown,
		"Horizontal Wind",
		"Moving Quicksand",
		"Ice",
		"TotWC Warp",
		"Hard Ground",
		s_unknown,
		"Warp Floor (WarpId 0xF3)",
		"Start Timer",
		"Stop Timer",
		"Hard & Slippery",
		"Hard & Very Slippery",
		"Hard & Not Slippery",
		"Vertical Wind"
	};

	if( type >= CollisionType::InstantWarp1 && type <= CollisionType::VerticalWind ) {
		return s_chunk[(ubyte)type - 0x1B];
	}

	switch( type ) {
		case CollisionType::Normal: return "Normal";
		case CollisionType::Lava: return "Lava";
		case CollisionType::HangableCeiling: return "Hangable Ceiling";
		case CollisionType::Slow: return "Slows Mario";
		case CollisionType::DeathFloor: return "Death Plane";
		case CollisionType::FlowingWater: return "Flowing Water";
		case CollisionType::NoCollision: return "No Collision";
		case CollisionType::VerySlippery: return "Very Slippery";
		case CollisionType::Climbable: return "Climbable Slope";
		case CollisionType::Slippery: return "Slippery";
		case CollisionType::CameraBoundary: return "Camera-Only Collision";
		case CollisionType::NoCameraCollision: return "No Camera Collision";
		case CollisionType::VanishCap: return "Vanish Cap Floor/Wall";
		default: return s_unknown;
	}

}

template<> const std::vector<CollisionType> &Enum::values() {
	static const std::vector<CollisionType> s_values = {
		CollisionType::Normal,
		CollisionType::Lava,
		CollisionType::HangableCeiling,
		CollisionType::Slow,
		CollisionType::DeathFloor,
		CollisionType::FlowingWater,
		CollisionType::NoCollision,
		CollisionType::VerySlippery,
		CollisionType::Slippery,
		CollisionType::Climbable,
		CollisionType::InstantWarp1,
		CollisionType::InstantWarp2,
		CollisionType::InstantWarp3,
		CollisionType::InstantWarp4,
		CollisionType::ShallowSinkingSand,
		CollisionType::LethalSinkingSand,
		CollisionType::Quicksand,
		CollisionType::DeepFlowingSand,
		CollisionType::ShallowFlowingSand,
		CollisionType::FlowingSand,
		CollisionType::AlternateSound,
		CollisionType::SlipperyAlternateSound,
		CollisionType::HorizontalWind,
		CollisionType::MovingQuicksand,
		CollisionType::Ice,
		CollisionType::WingCapWarp,
		CollisionType::Hard,
		CollisionType::Warp,
		CollisionType::TimerStart,
		CollisionType::TimerEnd,
		CollisionType::HardSlippery,
		CollisionType::HardVerySlippery,
		CollisionType::HardNotSlippery,
		CollisionType::VerticalWind,
		CollisionType::CameraBoundary,
		CollisionType::NoCameraCollision,
		CollisionType::VanishCap
	};
	return s_values;
}

template<> const char *Enum::toString<ShadowType>( ShadowType type ) {
	switch( type ) {
		case ShadowType::Circle: return "Circle";
		case ShadowType::Square: return "Square";
		case ShadowType::Whomp: return "Rectangle (Whomp)";
		case ShadowType::Spindel: return "Rectangle (Spindel)";
		default: return "Unknown";
	}
}
template<> const std::vector<ShadowType> &Enum::values() {
	static const std::vector<ShadowType> s_values = {
		ShadowType::Circle,
		ShadowType::Square,
		ShadowType::Whomp,
		ShadowType::Spindel
	};
	return s_values;
}

template<> const char *Enum::toString<TextureFiltering>( TextureFiltering method ) {
	switch( method ) {
		case TextureFiltering::None: return "None";
		case TextureFiltering::Average: return "Average";
		case TextureFiltering::Bilinear: return "Bilinear";
		default: return "Invalid";
	}
}

template<> const std::vector<TextureFiltering> &Enum::values() {
	static const std::vector<TextureFiltering> s_values = {
		TextureFiltering::None,
		TextureFiltering::Average,
		TextureFiltering::Bilinear
	};
	return s_values;
}

template<> const char *Enum::toString<ModulePropertyType>( ModulePropertyType type ) {
	assert( type <= ModulePropertyType::Pointer );
	static const char *s_names[7] = {
		"Integer",
		"Float",
		"Boolean",
		"String",
		"Colour",
		"Angle",
		"Pointer"
	};
	return s_names[(int)type];
}

template<> const std::vector<ModulePropertyType> &Enum::values() {
	static const std::vector<ModulePropertyType> s_values = {
		ModulePropertyType::Integer,
		ModulePropertyType::Float,
		ModulePropertyType::Boolean,
		ModulePropertyType::String,
		ModulePropertyType::Colour,
		ModulePropertyType::Angle,
		ModulePropertyType::Pointer
	};
	return s_values;
}

template<> const char *Enum::toString<ModuleColourFormat>( ModuleColourFormat format ) {
	assert( format <= ModuleColourFormat::Components );
	static const char *s_names[3] = { "RGBA32", "RGBA16", "Components" };
	return s_names[(int)format];
}

template<> const std::vector<ModuleColourFormat> &Enum::values() {
	static const std::vector<ModuleColourFormat> s_values = {
		ModuleColourFormat::RGBA32,
		ModuleColourFormat::RGBA16,
		ModuleColourFormat::Components
	};
	return s_values;
}

template<> const char *Enum::toString<ModuleHook>( ModuleHook hook ) {
	assert( hook <= ModuleHook::Custom );
	static const char *s_names[9] = {
		"MarioStepBegin",
		"MarioStepEnd",
		"LevelLoad",
		"AreaLoad",
		"GameStart",
		"RenderHUD",
		"FrameEnd",
		"MarioAction",
		"Custom"
	};
	return s_names[(int)hook];
}

template<> const std::vector<ModuleHook> &Enum::values() {
	static const std::vector<ModuleHook> s_values = {
		ModuleHook::MarioStepBegin,
		ModuleHook::MarioStepEnd,
		ModuleHook::LevelLoad,
		ModuleHook::AreaLoad,
		ModuleHook::GameStart,
		ModuleHook::RenderHUD,
		ModuleHook::FrameEnd,
		ModuleHook::MarioAction,
		ModuleHook::Custom
	};
	return s_values;
}

template<> const char *Enum::toString<MarioActionCategory>( MarioActionCategory category ) {
	assert( (uint)category % 0x40 == 0 && category <= MarioActionCategory::Object );
	static const char *s_names[7] = {
		"Stationary",
		"Moving",
		"Airborne",
		"Submerged",
		"Cutscene",
		"Automatic",
		"Object",
	};
	return s_names[(uint)category / 0x40];
}

template<> const std::vector<MarioActionCategory> &Enum::values() {
	static const std::vector<MarioActionCategory> s_values = {
		MarioActionCategory::Stationary,
		MarioActionCategory::Moving,
		MarioActionCategory::Airborne,
		MarioActionCategory::Submerged,
		MarioActionCategory::Cutscene,
		MarioActionCategory::Automatic,
		MarioActionCategory::Object,
	};
	return s_values;
}

template<> const char *Enum::toString<MarioActionFlag>( MarioActionFlag flag ) {
	switch( flag ) {
		case MarioActionFlag::None: return "(none)";
		case MarioActionFlag::Stationary: return "Stationary";
		case MarioActionFlag::Moving: return "Moving";
		case MarioActionFlag::Air: return "Air";
		case MarioActionFlag::Intangible: return "Intangible";
		case MarioActionFlag::Swimming: return "Swimming";
		case MarioActionFlag::MetalWater: return "MetalWater";
		case MarioActionFlag::ShortHitbox: return "ShortHitbox";
		case MarioActionFlag::RidingShell: return "RidingShell";
		case MarioActionFlag::Invulnerable: return "Invulnerable";
		case MarioActionFlag::Sliding: return "Sliding";
		case MarioActionFlag::Diving: return "Diving";
		case MarioActionFlag::OnPole: return "OnPole";
		case MarioActionFlag::Hanging: return "Hanging";
		case MarioActionFlag::Idle: return "Idle";
		case MarioActionFlag::Attacking: return "Attacking";
		case MarioActionFlag::AllowVerticalWind: return "AllowVerticalWind";
		case MarioActionFlag::ControlJumpHeight: return "ControlJumpHeight";
		case MarioActionFlag::AllowFirstPerson: return "AllowFirstPerson";
		case MarioActionFlag::AllowExitCourse: return "AllowExitCourse";
		case MarioActionFlag::HandsOpen: return "HandsOpen";
		case MarioActionFlag::HeadPivot: return "HeadPivot";
		case MarioActionFlag::UserDefined: return "UnusedFlag";
		case MarioActionFlag::ThrowingObject: return "ThrowingObject";
		default: return "(multiple flags)";
	}
}

template<> const std::vector<MarioActionFlag> &Enum::values() {
	static const std::vector<MarioActionFlag> s_values = {
		MarioActionFlag::Stationary,
		MarioActionFlag::Moving,
		MarioActionFlag::Air,
		MarioActionFlag::Intangible,
		MarioActionFlag::Swimming,
		MarioActionFlag::MetalWater,
		MarioActionFlag::ShortHitbox,
		MarioActionFlag::RidingShell,
		MarioActionFlag::Invulnerable,
		MarioActionFlag::Sliding,
		MarioActionFlag::Diving,
		MarioActionFlag::OnPole,
		MarioActionFlag::Hanging,
		MarioActionFlag::Idle,
		MarioActionFlag::Attacking,
		MarioActionFlag::AllowVerticalWind,
		MarioActionFlag::ControlJumpHeight,
		MarioActionFlag::AllowFirstPerson,
		MarioActionFlag::AllowExitCourse,
		MarioActionFlag::HandsOpen,
		MarioActionFlag::HeadPivot,
		MarioActionFlag::ThrowingObject,
		MarioActionFlag::UserDefined
	};
	return s_values;
}

template<> const char *Enum::toString<ShimType>( ShimType type ) {
	assert( type <= ShimType::Jump );
	static const char *s_names[5] = {
		"n/a",
		"Head",
		"Tail",
		"Word",
		"Jump"
	};
	return s_names[(int)type];
}

template<> const std::vector<ShimType> &Enum::values() {
	static const std::vector<ShimType> s_values = {
		ShimType::Head,
		ShimType::Tail,
		ShimType::Word,
		ShimType::Jump
	};
	return s_values;
}

template<> const char *Enum::toString<HookPriority>( HookPriority order ) {
	assert( order <= HookPriority::Last );
	static const char *s_names[5] = {
		"First",
		"Early",
		"Middle",
		"Late",
		"Last"
	};
	return s_names[(int)order];
}

template<> const std::vector<HookPriority> &Enum::values() {
	static const std::vector<HookPriority> s_values = {
		HookPriority::First,
		HookPriority::Early,
		HookPriority::Middle,
		HookPriority::Late,
		HookPriority::Last
	};
	return s_values;
}
