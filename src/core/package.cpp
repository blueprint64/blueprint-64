#include "src/core/package.hpp"

#include <zlib.h>
#include <vector>
#include <cstring>
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/polyfill/buffer.hpp"
#include "src/polyfill/tar.hpp"

static const char *const s_magicNumber = "BBPPKG";

struct FileInfo {
	string relativePath;
	uint packageAddress;
	uint compressedSize;
	uint uncompressedSize;

	static FileInfo read( std::istream &stream ) {
		FileInfo info;
		info.relativePath.reserve( 32 );
		for( int c = stream.get(); c > 0; c = stream.get() ) {
			info.relativePath.push_back( c == '\\' ? '/' : (char)c );
		}
		stream.read( (char*)&info.packageAddress, sizeof( uint ) );
		stream.read( (char*)&info.compressedSize, sizeof( uint ) );
		stream.read( (char*)&info.uncompressedSize, sizeof( uint ) );
		info.packageAddress = ntohl( info.packageAddress );
		info.compressedSize = ntohl( info.compressedSize );
		info.uncompressedSize = ntohl( info.uncompressedSize );
		return info;
	}

	void write( std::ostream &stream ) const {
		stream.write( relativePath.data(), relativePath.size() );
		stream.put( '\0' );
		uint data[3] = {
			htonl( packageAddress ),
			htonl( compressedSize ),
			htonl( uncompressedSize )
		};
		stream.write( (char*)data, sizeof( uint ) * 3 );
	}

};

static void getFiles(
	const fs::path &searchPath,
	const fs::path &relativePath,
	std::vector<FileInfo> &output
) {
	for( const auto &node : fs::directory_iterator( searchPath ) ) {
		if( node.is_directory() ) {
			getFiles(
				node.path(),
				relativePath / node.path().filename(),
				output
			);
		} else if( node.is_regular_file() ) {
			output.push_back({
				(relativePath / node.path().filename()).u8string(),
				0,
				0,
				(uint)node.file_size()
			});
		}
	}
}

bool Package::create(
	const fs::path &packagePath,
	const fs::path &sourceDirectory
) {
	std::vector<FileInfo> files;
	getFiles( sourceDirectory, fs::path(), files );

	FileWriteStream pkg( packagePath.u8string() );
	pkg.write( s_magicNumber, 6 );

	uint numFilesBE = htonl( (uint)files.size() );
	pkg.write( (char*)&numFilesBE, sizeof( uint ) );

	for( const FileInfo &info : files ) {
		info.write( pkg );
	}

	for( size_t i = 0; i < files.size(); i++ ) {
		files[i].packageAddress = (uint)pkg.tellp();
		FileReadStream file( (sourceDirectory / files[i].relativePath).u8string() );
		Buffer uncompressedData( files[i].uncompressedSize );
		file.read( uncompressedData.data(), files[i].uncompressedSize );
		file.close();

		ulong compressedSize = compressBound( files[i].uncompressedSize );
		Buffer compressedData( compressedSize );
		const int status = compress( (ubyte*)compressedData.data(), &compressedSize, (ubyte*)uncompressedData.data(), files[i].uncompressedSize );
		if( status != Z_OK ) return false;
		files[i].compressedSize = (uint)compressedSize;
		pkg.write( compressedData.data(), compressedSize );
	}

	pkg.seekp( 10 );
	for( const FileInfo &info : files ) {
		info.write( pkg );
	}

	return pkg.good();
}

static inline Package::Format checkFormat( const char *magicNumber ) {
	if( magicNumber[0] == 0x1F && magicNumber[1] == (char)0x8B ) {
		return Package::Format::Tarball;
	} else if( std::strncmp( s_magicNumber, magicNumber, 6 ) == 0 ) {
		return Package::Format::BppPkg;
	}

	return Package::Format::Unknown;
}

bool Package::extract(
	const fs::path &packagePath,
	const fs::path &destinationDirectory,
	Package::Format &format
) {
	char magicNumber[6];
	std::memset( magicNumber, 0, 6 );

	FileReadStream pkg( packagePath.u8string() );
	pkg.read( magicNumber, 6 );

	format = checkFormat( magicNumber );
	if( format == Package::Format::Tarball ) {
		return tarball::extract( packagePath.u8string(), destinationDirectory.u8string() );
	} else if( format != Package::Format::BppPkg ) {
		return false;
	}

	uint numFiles;
	pkg.read( (char*)&numFiles, sizeof( uint ) );
	numFiles = ntohl( numFiles );

	std::vector<FileInfo> fileInfos;
	fileInfos.reserve( numFiles );

	for( uint i = 0; i < numFiles; i++ ) {
		fileInfos.push_back( FileInfo::read( pkg ) );
	}

	for( const FileInfo &info : fileInfos ) {
		Buffer compressedData( info.compressedSize );
		pkg.seekg( info.packageAddress );
		pkg.read( compressedData.data(), info.compressedSize );

		Buffer uncompressedData( info.uncompressedSize );
		ulong fileSize = info.uncompressedSize;
		const int status = uncompress( (ubyte*)uncompressedData.data(), &fileSize, (ubyte*)compressedData.data(), info.compressedSize );
		if( status != Z_OK ) return false;

		fs::create_directories( (destinationDirectory / info.relativePath).parent_path() );
		FileWriteStream file( (destinationDirectory / info.relativePath).u8string() );
		file.write( uncompressedData.data(), fileSize );
		file.flush();
	}

	return pkg.good() || pkg.eof();
}

Package::Format Package::getFormat( const fs::path &packagePath ) {
	char magicNumber[6];
	std::memset( magicNumber, 0, 6 );

	std::ifstream file( packagePath, mode::read );
	file.read( magicNumber, 6 );

	return checkFormat( magicNumber );
}
