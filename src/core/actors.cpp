#include "src/core/actors.hpp"

#include <cstring>
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/locations.hpp"
#include "src/core/json.hpp"
#include "src/core/util.hpp"

static inline short readShort( std::istream &rom ) {
	ushort value;
	rom.read( (char*)&value, 2 );
	value = ntohs( value );
	short result = reinterpret_cast<const short&>( value );
	return result;
}

static inline void writeVector( std::ostream &rom, const Vec3s &v ) {
	BinaryWriter::writeSignedHalfwords( rom, { v.x, v.y, v.z });
}

static inline Vec3s readVector( std::istream &rom ) {
	const short x = readShort( rom );
	const short y = readShort( rom );
	const short z = readShort( rom );
	return Vec3s( x, y, z );
}

const PlacedObject &PlacedObject::Default() {
	static const PlacedObject s_default = {
		/* actMask */ 0,
		/* modelId */ 0,
		/* position */ { 0, 0, 0 },
		/* rotation */ { 0, 0, 0 },
		/* params */ { 0, 0, 0, 0 },
		/* behaviour */ 0x13001820
	};
	return s_default;
};

bool PlacedObject::isEmpty() const {
	return actMask == 0 && modelId == 0 && (behaviour == 0x13001820 || behaviour == 0x13000000 || behaviour == 0);
}

void PlacedObject::writeToRom( std::ostream &rom ) const {
	rom.put( 0x24 ); rom.put( 0x18 );
	rom.put( actMask );
	rom.put( modelId );
	writeVector( rom, position );
	writeVector( rom, rotation );
	rom.write( (const char*)params, 4 );
	const uint beh = htonl( behaviour );
	rom.write( (const char*)&beh, 4 );
}

const NormalWarp &NormalWarp::Default() {
	static const NormalWarp s_default = {
		/* sourceWarpId */ 0,
		/* levelId */ (LevelId)0,
		/* areaIndex */ 1,
		/* targetWarpId */ 0,
		/* checkpoint */ false
	};
	return s_default;
};

bool NormalWarp::isEmpty() const {
	return sourceWarpId == 0 && levelId == (LevelId)0 && areaIndex <= 1 && targetWarpId == 0 && !checkpoint;
}

void NormalWarp::writeToRom( std::ostream &rom ) const {
	const ubyte data[8] = {
		0x26, 0x08,
		sourceWarpId,
		(ubyte)levelId,
		areaIndex,
		targetWarpId,
		checkpoint ? (ubyte)0x80 : (ubyte)0x00,
		0x00
	};
	rom.write( (const char*)data, 8 );
}

void InstantWarp::writeToRom( std::ostream &rom ) const {
	rom.put( 0x28 ); rom.put( 0x0C );
	rom.put( (ubyte)trigger - (ubyte)CollisionType::InstantWarp1 );
	rom.put( areaIndex );
	writeVector( rom, offset );
	rom.put( 0x00 ); rom.put( 0x00 );
}

void PaintingWarp::writeToRom( std::ostream &rom ) const {
	const ubyte data[8] = {
		0x27, 0x08,
		sourceWarpId,
		(ubyte)levelId,
		areaIndex,
		targetWarpId,
		checkpoint ? (ubyte)0x80 : (ubyte)0x00,
		0x00
	};
	rom.write( (const char*)data, 8 );
}

static inline uint readWord( std::istream &rom ) {
	uint wordBE;
	rom.read( (char*)&wordBE, 4 );
	return ntohl( wordBE );
}

std::array<AreaActors,8> AreaActors::loadFromRom( std::istream &rom, LevelId levelId ) {
	std::array<AreaActors,8> areaActors;
	if( levelId == LevelId::EndScreen ) return areaActors;

	const RomLocations::LevelLocation &level = RomLocations::getLevelLocation( levelId );
	rom.seekg( level.scriptLoaderLocation + 0x4 );
	uint scriptStart;
	rom.read( (char*)&scriptStart, 4 );
	scriptStart = ntohl( scriptStart );
	if( scriptStart == level.vanillaScriptStart ) {
		return areaActors;
	}

	uint segment0eStart = 0;
	uint segment0eEnd = 0;
	bool inSegment0e = false;

	AreaActors *actors = nullptr;
	rom.seekg( scriptStart );
	while( rom.good() ) {
		if( inSegment0e && rom.tellg() >= segment0eEnd ) {
			throw RomError( "ROM Corrupt: Level script is not terminated." );
		}

		if( !inSegment0e && rom.tellg() > scriptStart + 0x2000 ) {
			throw RomError( "ROM Corrupt: Level script is not terminated." );
		}

		const ubyte commandType = rom.get();

		switch( commandType ) {
			case 0x17: { // Load Segment
				rom.seekg( 2, std::ios_base::cur );
				if( (ubyte)rom.get() == 0x0E ) {
					segment0eStart = readWord( rom );
					segment0eEnd = readWord( rom );
				} else {
					rom.seekg( 8, std::ios_base::cur );
				}
				continue;
			}
			case 0x05: { // Jump
				rom.seekg( 3, std::ios_base::cur );
				uint segPtr = readWord( rom );
				if( segPtr >> 24 == 0x19 ) {
					inSegment0e = false;
					rom.seekg( scriptStart + ( segPtr & 0x00FFFFFFu ) );
				} else if( segment0eStart && segPtr >> 24 == 0x0E ) {
					inSegment0e = true;
					rom.seekg( segment0eStart + ( segPtr & 0x00FFFFFFu ) );
				}
				continue;
			}
			case 0x1F: { // Area Start
				rom.get();
				const ubyte areaIndex = rom.get();
				if( areaIndex >= 8 ) {
					return std::array<AreaActors,8>();
				}
				actors = &areaActors[areaIndex];
				rom.seekg( 5, std::ios_base::cur );
				continue;
			}
			case 0x20: { // Area End
				actors = nullptr;
				rom.seekg( 3, std::ios_base::cur );
				continue;
			}
			case 0x02: { // End Level Script
				return areaActors;
			}
		}

		const ubyte commandLength = rom.get();
		if( actors == nullptr ) {
			rom.seekg( commandLength - 2, std::ios_base::cur );
			continue;
		}

		switch( commandType ) {
			case 0x24: { // Object
				actors->objects.push_back({
					(ubyte)rom.get(),
					(ubyte)rom.get(),
					readVector( rom ),
					readVector( rom ),
					{ (ubyte)rom.get(), (ubyte)rom.get(), (ubyte)rom.get(), (ubyte)rom.get() },
					readWord( rom )
				});
				break;
			}
			case 0x26: { // Warp
				actors->warps.push_back({
					(ubyte)rom.get(),
					(LevelId)rom.get(),
					(ubyte)rom.get(),
					(ubyte)rom.get(),
					rom.get() == 0x80
				});
				rom.get();
				break;
			}
			case 0x27: { // Painting Warp
				actors->paintingWarps.push_back({
					(ubyte)rom.get(),
					(LevelId)rom.get(),
					(ubyte)rom.get(),
					(ubyte)rom.get(),
					rom.get() == 0x80
				});
				rom.get();
				break;
			}
			case 0x28: { // Instant Warp
				actors->instantWarps.push_back({
					(CollisionType)(0x1B + rom.get()),
					(ubyte)rom.get(),
					readVector( rom )
				});
				rom.get();
				rom.get();
				break;
			}
			default: {
				rom.seekg( commandLength - 2, std::ios_base::cur );
			}
		}

	}

	return std::array<AreaActors,8>();
}

static void serializeVector3s( JsonWriter &jw, const Vec3s &v ) {
	jw.writeObjectStart();
		jw.writeProperty( "x", v.x );
		jw.writeProperty( "y", v.y );
		jw.writeProperty( "z", v.z );
	jw.writeObjectEnd();
}

static Vec3s deserializeVector3s( const Json &json ) {
	return Vec3s(
		json["x"].get<short>(),
		json["y"].get<short>(),
		json["z"].get<short>()
	);
}

void AreaActors::saveToJson( const string &jsonFilePath ) const {
	FileWriteStream jsonStream( jsonFilePath );
	JsonWriter jw( &jsonStream, true );

	jw.writeObjectStart();

	jw.writePropertyName( "objects" );
	jw.writeArrayStart();
	for( const PlacedObject &object : objects ) {
		if( object.isEmpty() ) continue;

		jw.writeObjectStart();
		jw.writeProperty( "act_mask", object.actMask );
		jw.writeProperty( "model_id", object.modelId );
		jw.writePropertyName( "position" );
		serializeVector3s( jw, object.position );
		jw.writePropertyName( "rotation" );
		serializeVector3s( jw, object.rotation );
		jw.writePropertyName( "params" );
		jw.writeArrayStart();
		for( int i = 0; i < 4; i++ ) {
			jw.writeNumber( (uint64)object.params[i] );
		}
		jw.writeArrayEnd();
		jw.writeProperty( "behaviour", Util::toHexWord( object.behaviour, true ) );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "warps" );
	jw.writeArrayStart();
	for( const NormalWarp &warp : warps ) {
		if( warp.isEmpty() ) continue;

		jw.writeObjectStart();
		jw.writeProperty( "source", warp.sourceWarpId );
		jw.writeProperty( "level", warp.levelId );
		jw.writeProperty( "area", warp.areaIndex );
		jw.writeProperty( "target", warp.targetWarpId );
		jw.writeProperty( "checkpoint", warp.checkpoint );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "instant_warps" );
	jw.writeArrayStart();
	for( const InstantWarp &warp : instantWarps ) {
		jw.writeObjectStart();
		jw.writeProperty( "trigger", warp.trigger );
		jw.writeProperty( "area", warp.areaIndex );
		jw.writePropertyName( "offset" );
		serializeVector3s( jw, warp.offset );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "paintings" );
	jw.writeArrayStart();
	for( const PaintingWarp &warp : paintingWarps ) {
		jw.writeObjectStart();
		jw.writeProperty( "source", warp.sourceWarpId );
		jw.writeProperty( "level", warp.levelId );
		jw.writeProperty( "area", warp.areaIndex );
		jw.writeProperty( "target", warp.targetWarpId );
		jw.writeProperty( "checkpoint", warp.checkpoint );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writeObjectEnd();
}

AreaActors AreaActors::loadFromJson( const string &jsonFilePath ) {
	AreaActors actors;
	FileReadStream jsonStream( jsonFilePath );
	const Json &json = Json::parse( jsonStream );

	for( const Json &object : json["objects"].array() ) {
		const JArray params = object["params"].array();
		actors.objects.push_back({
			object["act_mask"].get<ubyte>(),
			object["model_id"].get<ubyte>(),
			deserializeVector3s( object["position"] ),
			deserializeVector3s( object["rotation"] ),
			{ params.at(0).get<ubyte>(), params.at(1).get<ubyte>(), params.at(2).get<ubyte>(), params.at(3).get<ubyte>() },
			Util::parseHexWord( object["behaviour"].get<string>() )
		});
	}

	for( const Json &warp : json["warps"].array() ) {
		actors.warps.push_back({
			warp["source"].get<ubyte>(),
			warp["level"].get<LevelId>(),
			warp["area"].get<ubyte>(),
			warp["target"].get<ubyte>(),
			warp["checkpoint"].get<bool>()
		});
	}

	for( const Json &warp : json["instant_warps"].array() ) {
		actors.instantWarps.push_back({
			warp["trigger"].get<CollisionType>(),
			warp["area"].get<ubyte>(),
			deserializeVector3s( warp["offset"] )
		});
	}

	for( const Json &warp : json["paintings"].array() ) {
		actors.paintingWarps.push_back({
			warp["source"].get<ubyte>(),
			warp["level"].get<LevelId>(),
			warp["area"].get<ubyte>(),
			warp["target"].get<ubyte>(),
			warp["checkpoint"].get<bool>()
		});
	}

	return actors;
}
