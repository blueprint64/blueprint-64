#include "src/types.hpp"
#include "src/polyfill/filesystem.hpp"

class FileBackup {

	private:
	fs::path m_sourcePath;
	fs::path m_backupPath;
	bool m_discard;

	public:
	FileBackup( const fs::path &filePath );
	FileBackup( string filePath ) : FileBackup( fs::to_path( filePath ) ) {}
	~FileBackup();

	inline void discard() {
		m_discard = true;
	}

};
