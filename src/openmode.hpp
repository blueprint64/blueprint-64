#ifndef SRC_OPENMODE_HPP_
#define SRC_OPENMODE_HPP_

#include <ios>

namespace mode {
	static constexpr std::ios_base::openmode read = std::ios_base::in | std::ios_base::binary;
	static constexpr std::ios_base::openmode write = std::ios_base::out | std::ios_base::binary | std::ios_base::trunc;
	static constexpr std::ios_base::openmode readWrite = std::ios_base::in | std::ios_base::out | std::ios_base::binary;
}

#endif /* SRC_OPENMODE_HPP_ */
