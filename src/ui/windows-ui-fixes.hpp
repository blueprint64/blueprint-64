#ifndef SRC_UI_WINDOWS_UI_FIXES_HPP_
#define SRC_UI_WINDOWS_UI_FIXES_HPP_

#include <QApplication>

class WindowsIconFixer : public QObject {
	Q_OBJECT

	public:
	inline WindowsIconFixer( QApplication *app ) : QObject( app ) {}
	virtual ~WindowsIconFixer() {}

	bool eventFilter( QObject *object, QEvent *event ) override;
};

namespace WindowsUiFixes {
	extern void apply( QApplication &app );
}


#endif /* SRC_UI_WINDOWS_UI_FIXES_HPP_ */
