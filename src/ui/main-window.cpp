#include "src/ui/main-window.hpp"
#include "ui_main-window.h"

#include <QMessageBox>
#include <cstdlib>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/widgets/save-notification.hpp"
#include "src/ui/texture-splitter-dialog.hpp"
#include "src/core/baserom.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/storage.hpp"
#include "src/core/util.hpp"
#include "src/core/checksum.hpp"
#include "src/core/backup.hpp"
#include "src/core/emulator.hpp"
#include "src/core/exceptions.hpp"
#include "src/ui/util.hpp"

static const uint LOCATION_ROM_SETTINGS = 0;
static const uint LOCATION_TWEAKS = 1;
static const uint LOCATION_DIALOG_EDIT = 2;
static const uint LOCATION_LEVEL_EDIT = 3;
static const uint LOCATION_AREA_EDIT = 4;
static const uint LOCATION_AREA_MODEL_EDIT = 5;
static const uint LOCATION_MUSIC_EDIT = 6;
static const uint LOCATION_OBJECT_EDIT = 7;
static const uint LOCATION_OBJECT_MODEL_EDIT = 8;
static const uint LOCATION_WATER_EDIT = 9;
static const uint LOCATION_BACKGROUND_EDIT = 10;
static const uint LOCATION_ASM_MODULES_EDIT = 11;
static const uint LOCATION_MARIO_ACTIONS_EDIT = 12;
static const uint LOCATION_MODULE_DEV = 13;
static const uint LOCATION_SIMPLE_BEHAVIOUR_EDIT = 14;

using namespace std::string_literals;

MainWindow::MainWindow( QWidget *parent ) :
	QMainWindow( parent ),
	m_ui( new Ui::MainWindow ),
	m_currentLevel( LevelId::INVALID ),
	m_currentArea( 0 ),
	m_currentObject( 0 )
{
	m_ui->setupUi( this );
	syncHistory();

	m_ui->menubar->setNativeMenuBar( AppPreferences::current().nativeMenuBar );

	connect( m_ui->actionBuildAll, SIGNAL(triggered()), this, SLOT(buildAll()) );
	connect( m_ui->actionUpdateChecksum, SIGNAL(triggered()), this, SLOT(updateChecksum()) );
	connect( m_ui->actionTextureSplitter, &QAction::triggered, TextureSplitterDialog::run );

	connect( &m_preferencesDialog, &QDialog::accepted, this, [=](){
		m_ui->menubar->setNativeMenuBar( AppPreferences::current().nativeMenuBar );
	});
}

MainWindow::~MainWindow() {
	delete m_ui;
}

void MainWindow::showErrorDialog( string message ) {
	QMessageBox::critical(
		this,
		"Oh no!",
		message.c_str(),
		QMessageBox::Ok
	);
}

bool MainWindow::trySave() {
	Blueprint *blueprint = Blueprint::currentEditable();
	if( blueprint == nullptr ) return false;

	try {
		const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
		if( surfacesEditor != nullptr ) {
			surfacesEditor->saveData();
		}
		blueprint->save();
		new SaveNotification( this, m_ui->saveButton->geometry().center() );
		return true;
	} catch( const std::exception& ) {
		showErrorDialog( "An error occurred attempting to save your blueprint. Please report this bug." );
		return false;
	}
}

void MainWindow::saveAs() {
	Blueprint *blueprint = Blueprint::currentEditable();
	if( blueprint == nullptr || blueprint->isCollab() ) return;

	fs::path path = FileDialog::getFileSave( "Save As", { { "Blueprint Files", "*.bbp" }} );
	if( path.empty() ) return;

	path.replace_extension( ".bbp" );
	try {
		const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
		if( surfacesEditor != nullptr ) {
			surfacesEditor->saveData();
		}
		blueprint->saveAs( path );
		RecentProjects::push( path.u8string() );
	} catch( const std::exception& ) {
		showErrorDialog( "Failed to save blueprint to this location. Do you have permission to create files here?" );
	}
}

bool MainWindow::unsavedChangesPrompt() {
	if( Blueprint::current() == nullptr || !Blueprint::current()->hasUnsavedChanges() ) {
		return true;
	}

	QMessageBox::StandardButton answer = QMessageBox::question(
		this,
		"Save Changes?",
		"You have unsaved changes. Do you want to save now?",
		QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
		QMessageBox::Yes
	);

	switch( answer ) {
		case QMessageBox::Yes: return trySave();
		case QMessageBox::No: return true;
		default: return false;
	}
}

void MainWindow::loadBlueprint() {
	syncHistory();
	checkCanBuild();
	m_ui->breadcrumbTrail->clear();
	m_ui->breadcrumbTrail->push( "ROM Settings", LOCATION_ROM_SETTINGS );
	m_ui->activeView->setCurrentWidget( m_ui->romSettingsPage );
	m_ui->romSettingsPage->blueprintChanged();
	m_ui->tweakSettingsPage->blueprintChanged();
	m_ui->dialogEditorPage->blueprintChanged();
	m_ui->waterEditorPage->blueprintChanged();
	m_ui->moduleEditorPage->blueprintChanged();
	m_ui->actionSaveAs->setEnabled( !Blueprint::current()->isCollab() );
	show();
}

void MainWindow::checkCanBuild() {
	const bool &canBuild = Blueprint::current()->hasTargetRom();
	m_ui->buildMenu->setEnabled( canBuild );
	m_ui->applyAllButton->setEnabled( canBuild );
	m_ui->playRomButton->setEnabled( canBuild );
}

void MainWindow::syncHistory() {
	m_ui->openRecentMenu->clear();
	for( const string &filePath : RecentProjects::get() ) {
		QAction *action = new QAction( filePath.c_str(), m_ui->openRecentMenu );
		connect( action, &QAction::triggered, this, [=](){
			if( !unsavedChangesPrompt() ) return;
			try {
				Blueprint::open( fs::to_path( filePath ) );
				RecentProjects::push( filePath );
				loadBlueprint();
			} catch( ... ) {
				RecentProjects::remove( filePath );
				QMessageBox::critical(
					this,
					"File not found.",
					"Failed to open project. Has it been moved or deleted?",
					QMessageBox::Ok
				);
			}
			syncHistory();
		});
		m_ui->openRecentMenu->addAction( action );
	}
}

#define HANDLE_NAV( id, page ) case id: \
	m_ui->activeView->setCurrentWidget( m_ui->page ); \
	break;

void MainWindow::navigated( uint locationId ) {
	switch( locationId ) {
		case LOCATION_ROM_SETTINGS:
			m_ui->activeView->setCurrentWidget( m_ui->romSettingsPage );
			break;
		case LOCATION_TWEAKS:
			m_ui->activeView->setCurrentWidget( m_ui->tweakSettingsPage );
			break;
		case LOCATION_DIALOG_EDIT:
			m_ui->activeView->setCurrentWidget( m_ui->dialogEditorPage );
			break;
		case LOCATION_LEVEL_EDIT:
			m_ui->levelEditorPage->setLevel( m_currentLevel );
			m_ui->activeView->setCurrentWidget( m_ui->levelEditorPage );
			break;
		case LOCATION_AREA_EDIT:
			m_ui->areaEditorPage->setArea( m_currentLevel, m_currentArea );
			m_ui->activeView->setCurrentWidget( m_ui->areaEditorPage );
			break;
		case LOCATION_AREA_MODEL_EDIT:
			m_ui->areaModelPage->setArea( m_currentLevel, m_currentArea );
			m_ui->activeView->setCurrentWidget( m_ui->areaModelPage );
			break;
		case LOCATION_MUSIC_EDIT:
			m_ui->activeView->setCurrentWidget( m_ui->musicEditorPage );
			break;
		case LOCATION_OBJECT_EDIT:
			m_ui->objectEditorPage->setObject( m_currentLevel, m_currentObject );
			m_ui->activeView->setCurrentWidget( m_ui->objectEditorPage );
			break;
		case LOCATION_OBJECT_MODEL_EDIT:
			m_ui->objectModelPage->setObject( m_currentLevel, m_currentObject );
			m_ui->activeView->setCurrentWidget( m_ui->objectModelPage );
			break;
		case LOCATION_WATER_EDIT:
			m_ui->waterEditorPage->setArea( m_currentLevel,  m_currentArea );
			m_ui->activeView->setCurrentWidget( m_ui->waterEditorPage );
			break;
		case LOCATION_BACKGROUND_EDIT:
			m_ui->activeView->setCurrentWidget( m_ui->backgroundEditorPage );
			break;
		case LOCATION_ASM_MODULES_EDIT:
			m_ui->activeView->setCurrentWidget( m_ui->moduleEditorPage );
			break;
		case LOCATION_MARIO_ACTIONS_EDIT:
			m_ui->activeView->setCurrentWidget( m_ui->marioActionEditorPage );
			break;
		case LOCATION_MODULE_DEV:
			m_ui->activeView->setCurrentWidget( m_ui->moduleDevelopPage );
			break;
		case LOCATION_SIMPLE_BEHAVIOUR_EDIT:
			m_ui->simpleBehaviourEditorPage->setBehaviour( m_currentLevel, m_currentObject );
			m_ui->activeView->setCurrentWidget( m_ui->simpleBehaviourEditorPage );
			break;
	}
}

#undef HANDLE_NAV

void MainWindow::editTweaks() {
	m_ui->breadcrumbTrail->push( "Tweaks", LOCATION_TWEAKS );
	navigated( LOCATION_TWEAKS );
}

void MainWindow::editDialog() {
	m_ui->breadcrumbTrail->push( "Dialog", LOCATION_DIALOG_EDIT );
	navigated( LOCATION_DIALOG_EDIT );
}

void MainWindow::editLevel( LevelId levelId ) {
	m_currentLevel = levelId;
	m_ui->breadcrumbTrail->push( "Level Editor", LOCATION_LEVEL_EDIT );
	navigated( LOCATION_LEVEL_EDIT );
}

void MainWindow::editArea( ubyte areaIndex ) {
	m_currentArea = areaIndex;
	m_ui->breadcrumbTrail->push( "Area Editor", LOCATION_AREA_EDIT );
	navigated( LOCATION_AREA_EDIT );
}

void MainWindow::editAreaModel() {
	m_ui->breadcrumbTrail->push( "Surface Editor", LOCATION_AREA_MODEL_EDIT );
	navigated( LOCATION_AREA_MODEL_EDIT );
}

void MainWindow::editMusic() {
	m_ui->breadcrumbTrail->push( "Music Editor", LOCATION_MUSIC_EDIT );
	navigated( LOCATION_MUSIC_EDIT );
}

void MainWindow::editObject( objectId objectId ) {
	m_currentObject = objectId;
	m_ui->breadcrumbTrail->push( "Object Editor", LOCATION_OBJECT_EDIT );
	navigated( LOCATION_OBJECT_EDIT );
}

void MainWindow::editObjectModel() {
	m_ui->breadcrumbTrail->push( "Surface Editor", LOCATION_OBJECT_MODEL_EDIT );
	navigated( LOCATION_OBJECT_MODEL_EDIT );
}

void MainWindow::editObjectModel( objectId objectId ) {
	m_currentObject = objectId;
	editObjectModel();
}

void MainWindow::editWaterBoxes() {
	m_ui->breadcrumbTrail->push( "Water Box Editor", LOCATION_WATER_EDIT );
	navigated( LOCATION_WATER_EDIT );
}

void MainWindow::editBackgrounds() {
	m_ui->breadcrumbTrail->push( "Custom Background Editor", LOCATION_BACKGROUND_EDIT );
	navigated( LOCATION_BACKGROUND_EDIT );
}

void MainWindow::editAsmModules() {
	m_ui->breadcrumbTrail->push( "ASM Modules", LOCATION_ASM_MODULES_EDIT );
	navigated( LOCATION_ASM_MODULES_EDIT );
}

void MainWindow::editMarioActions() {
	m_ui->breadcrumbTrail->push( "Custom Mario Actions", LOCATION_MARIO_ACTIONS_EDIT );
	navigated( LOCATION_MARIO_ACTIONS_EDIT );
}

void MainWindow::developModule() {
	m_ui->breadcrumbTrail->push( "Develop and Test Modules", LOCATION_MODULE_DEV );
	navigated( LOCATION_MODULE_DEV );
}

void MainWindow::editSimpleBehaviour() {
	m_ui->breadcrumbTrail->push( "Simple Behaviour", LOCATION_SIMPLE_BEHAVIOUR_EDIT );
	navigated( LOCATION_SIMPLE_BEHAVIOUR_EDIT );
}

#define IS_ACTIVE( type ) dynamic_cast<type*>( m_ui->activeView->currentWidget() ) != nullptr

void MainWindow::buildClean() {
	build( true );
}

void MainWindow::buildAll() {
	if( IS_ACTIVE( ModuleDevelopView ) ) {
		m_ui->moduleDevelopPage->rebuildAll();
	} else {
		build( false );
	}
}

#undef IS_ACTIVE

static const char *CLEAN_BUILD_WARNING = ""
	"Performing a clean build will erase all changes to the ROM that are not "
	"part of the blueprint, INCLUDING OBJECT PLACEMENTS. Make sure you save "
	"all object placements to the ROM before continuing!";

void MainWindow::build( bool clean ) {
	const Blueprint *blueprint = Blueprint::current();
	if( !blueprint->hasTargetRom() ) return;

	if( clean && QMessageBox::warning( this, "Clean Build?", CLEAN_BUILD_WARNING, QMessageBox::Ok | QMessageBox::Abort, QMessageBox::Abort ) != QMessageBox::Ok ) {
		return;
	}

	const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
	if( surfacesEditor != nullptr ) {
		surfacesEditor->saveData();
	}

	const fs::path targetRomPath = blueprint->targetRomPath();

	bool useSavedObjectPlacements = clean;
	if( !fs::exists( targetRomPath ) ) {
		std::error_code err;
		fs::copy_file( BaseRom::filePath(), targetRomPath, err );
		useSavedObjectPlacements = true;
		if( err ) {
			QMessageBox::critical( this, "Build Failed", "Failed to create target ROM. Ensure that the path is valid and you have permission to create files here." );
			return;
		}
	}

	FileBackup backup( targetRomPath );
	if( clean ) {
		if( !fs::tryCopyFileOverwrite( BaseRom::filePath(), targetRomPath ) ) {
			QMessageBox::critical( this, "Build Failed", "Failed to replace the current ROM with a fresh one for a clean build." );
			return;
		}
	}

	FileStream rom = FileStream( targetRomPath.u8string() );
	try {
		const std::vector<string> warnings = blueprint->applyAll( rom, useSavedObjectPlacements );

		if( warnings.empty() ) {
			backup.discard();
			QMessageBox::information( this, "Changes Applied", "ROM built successfully." );
		} else if( m_buildWarningsDialog.getResponse( "Build completed with warnings:", warnings, true ) ) {
			backup.discard();
		}
	} catch( const ArmipsException &ex ) {
		if( m_armipsErrorDialog.getResponse( ex.what() ) ) {
			backup.discard();
			if( blueprint->romSpecs().enableChecksum ) {
				blueprint->updateChecksum( rom );
			}
		}
	} catch( const std::exception &ex ) {
		const string msg = "Failed to build ROM:\n"s + ex.what();
		QMessageBox::critical( this, "Build Failed", msg.c_str() );
	}
	rom.close();
}

void MainWindow::playRom() {
	if( !Blueprint::current()->hasTargetRom() ) return;

	if( !Emulator::emulatorInstalled() ) {
		QMessageBox::critical( this, "Emulator not Found", "Your preferred emulator does not appear to be installed. You can change your preferred emulator in Settings > Preferences." );
		return;
	}

	std::error_code err;
	if( !Emulator::launch( Blueprint::current()->targetRomPath().u8string(), err ) ) {
		QMessageBox::critical( this, err.category().name(), ("Failed to launch emulator: "s + err.message()).c_str(), QMessageBox::Ok );
	}
}

void MainWindow::updateChecksum() {
	const Blueprint *blueprint = Blueprint::current();
	if( !blueprint || !blueprint->hasTargetRom() ) return;
	FileStream rom( blueprint->targetRomPath().u8string() );
	updateCRC( rom );
}
