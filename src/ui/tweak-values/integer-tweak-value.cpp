#include "src/ui/tweak-values/integer-tweak-value.hpp"
#include "ui_tweak-value-integer.h"

#include "src/core/blueprint.hpp"

using std::optional;

IntegerTweakValue::IntegerTweakValue( QWidget *parent, const IntegerTweakDefinition *tweak, bool singular ) :
	TweakValueWidget( parent, tweak, singular ),
	m_ui( new Ui::IntegerTweakValue )
{
	m_ui->setupUi( this );
	m_ui->nameLabel->setText( singular ? "Value:" : tweak->getName().c_str() );
	m_ui->spinner->setMinimum( tweak->getMinValue() );
	m_ui->spinner->setMaximum( tweak->getMaxValue() );
	if( tweak->shouldDisplayAsHex() ) {
		m_ui->spinner->setDisplayIntegerBase( 16 );
		m_ui->spinner->setPrefix( "0x" );
	}
	refreshValue();
}

IntegerTweakValue::~IntegerTweakValue() {
	delete m_ui;
}

bool IntegerTweakValue::isDefault() const {
	return (uint)m_ui->spinner->value() == static_cast<const IntegerTweakDefinition*>( m_tweak )->getDefaultValue();
}

Word IntegerTweakValue::value() const {
	return Word::fromUInt( (uint)m_ui->spinner->value() );
}

void IntegerTweakValue::refreshValue() {
	optional<Word> currentValue = Blueprint::current()->tryGetTweakValue( m_tweak->getUniqueId() );
	if( currentValue.has_value() ) {
		m_ui->spinner->setValue( currentValue.value().asSInt() );
	} else {
		m_ui->spinner->setValue( static_cast<const IntegerTweakDefinition*>( m_tweak )->getDefaultValue() );
	}
}
