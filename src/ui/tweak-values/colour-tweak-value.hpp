#ifndef SRC_UI_TWEAK_VALUES_COLOUR_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_COLOUR_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class ColourTweakValue;
}

class ColourTweakValue final : public TweakValueWidget {

	private:
	Ui::ColourTweakValue *m_ui;

	public:
	explicit ColourTweakValue( QWidget *parent, const ColourTweakDefinition *tweak, bool singular );
	~ColourTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};

#endif /* SRC_UI_TWEAK_VALUES_COLOUR_TWEAK_VALUE_HPP_ */
