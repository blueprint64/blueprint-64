#ifndef SRC_UI_TWEAK_VALUES_TWEAK_VALUE_WIDGET_HPP_
#define SRC_UI_TWEAK_VALUES_TWEAK_VALUE_WIDGET_HPP_

#include <QWidget>
#include "src/core/tweak.hpp"

class TweakValueWidget : public QWidget {

	protected:
	const ISingularTweakDefinition *const m_tweak;

	public:
	TweakValueWidget( QWidget *parent, const ISingularTweakDefinition *tweak, bool singular ) :
		QWidget( parent ),
		m_tweak( tweak )
	{
		if( !singular ) {
			setToolTip( tweak->getDescription().c_str() );
		}
	}
	virtual ~TweakValueWidget() {}

	public:
	virtual bool isDefault() const = 0;
	virtual Word value() const = 0;
	virtual void refreshValue() = 0;

	inline Uuid tweakId() const {
		return m_tweak->getUniqueId();
	}

};

#endif /* SRC_UI_TWEAK_VALUES_TWEAK_VALUE_WIDGET_HPP_ */
