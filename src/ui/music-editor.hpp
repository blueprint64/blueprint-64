#ifndef SRC_UI_MUSIC_EDITOR_HPP_
#define SRC_UI_MUSIC_EDITOR_HPP_

#include <QWidget>
#include "src/core/music.hpp"

namespace Ui {
	class MusicEditor;
}

class MusicEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::MusicEditor *m_ui;
	std::vector<MusicInfo> m_music;
	bool m_initializing;

	void reload();
	void syncName( ubyte seqId );
	bool replaceMusicInternal( ubyte seqId );

	public:
	explicit MusicEditor( QWidget *parent = nullptr );
	~MusicEditor();

	virtual void showEvent( QShowEvent *event ) override {
		reload();
		QWidget::showEvent( event );
	}

	public slots:
	void trackSelected();
	void addMusic();
	void replaceMusic();
	void revertMusic();
	void nameChanged();
	void dataChanged();
	void setVolumeLabel( int volume );

};

#endif /* SRC_UI_MUSIC_EDITOR_HPP_ */
