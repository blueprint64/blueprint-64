#ifndef SRC_UI_ASM_MODULE_INFO_HPP_
#define SRC_UI_ASM_MODULE_INFO_HPP_

#include <QWidget>
#include "src/core/uuid.hpp"
#include "src/core/version.hpp"
#include "src/ui/module-area-scope-dialog.hpp"

namespace Ui {
	class AsmModuleInfo;
}

class AsmModuleInfo : public QWidget {
	Q_OBJECT

	Q_PROPERTY(bool devView MEMBER m_developMode)

	private:
	Ui::AsmModuleInfo *m_ui;
	Uuid m_moduleId;
	Version m_moduleVersion;
	ModuleAreaScopeDialog m_areaScopeDialog;
	bool m_developMode;

	public:
	explicit AsmModuleInfo( QWidget *parent = nullptr );
	~AsmModuleInfo();

	inline const Uuid &moduleId() const {
		return m_moduleId;
	}

	inline const Version &moduleVersion() const {
		return m_moduleVersion;
	}

	void setModule( const Uuid &moduleId, const Version &version );

	public slots:
	void editAreas();

	signals:
	void addOrRemove();
	void install();
	void update();

};



#endif /* SRC_UI_ASM_MODULE_INFO_HPP_ */
