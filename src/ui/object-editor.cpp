#include "src/ui/object-editor.hpp"
#include "ui_object-editor.h"

#include <cmath>
#include <algorithm>
#include "src/ui/model-import-helper.hpp"
#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

#ifndef M_PI
// Windows LUL
#define M_PI 3.14159265358979323846
#endif

struct BehaviourCollision {
	const char *name;
	uint collisionPointerLocation;
};

static const BehaviourCollision s_behaviours[] = {
	{ "[!] Box (0x13002250)", 0x21C05C },
	{ "120 Star Cannon Cover (0x13003CB8)", 0x21DAC4 },
	{ "Arrow Block (WDW) (0x13004314)", 0x21E120 },
	{ "Beta Trampoline Top (0x13001608)", 0x21B414 },
	{ "Blue Coin Switch (0x13002568)", 0x21C374 },
	{ "Blue Elevator Platform (RR) (0x13000CFC)", 0x21AB08 },
	{ "Bomp (Large) (0x13003940)", 0x21D74C },
	{ "Bomp (Small) (0x13003910)", 0x21D71C },
	{ "Bowser 2 Tilting Arena (0x13001920)", 0x21B72C },
	{ "Bowser puzzle piece (0x13002038)", 0x21BE44 },
	{ "Bowser's Sub (0x13002308)", 0x21C11C },
	{ "Breakable Cork Box (Normal) (0x130014E0)", 0x21B2EC },
	{ "Breakable Cork Box (Revealed by Purple Switch) (0x130014BC)", 0x21B2C8 },
	{ "Breakable Wall Left (0x13000638)", 0x21A440 },
	{ "Breakable Wall Right (0x13000624)", 0x21A42C },
	{ "Bullet Bill Cannon (0x13000600)", 0x21A40C },
	{ "Cannon Cover (0x13003274)", 0x21D080 },
	{ "Cap Switch (0x130001CC)", 0x219FD8 },
	{ "Cap Switch Base (0x130001AC)", 0x219FB8 },
	{ "Chain Chomp Gate (0x13004868)", 0x21E670 },
	{ "Checkerboard Platform (HMC) (0x13000D30)", 0x21AB3C },
	{ "Checkerboard Platform (Rotating Subobject) (0x13001B88)", 0x21B994 },
	{ "Clock Hand (TTC) (0x13004D64)", 0x21EB6C },
	{ "Coffin (BBH) (0x13005414)", 0x21F21C },
	{ "Controllable Platform (0x130041BC)", 0x21DFC8 },
	{ "Controllable Platform Button (0x130041F0)", 0x21DFFC },
	{ "Dire Dire Docks Door (0x130022D8)", 0x21C0E4 },
	{ "Door (0x13000B0C)", 0x21A928 },
	{ "Dorrie (0x13004F90)", 0x21ED98 },
	{ "Drawbridge (LLL) (0x130038E8)", 0x21D6F4 },
	{ "Elevator (BBH) (0x1300292C)", 0x21C738 },
	{ "Elevator (SSL) (0x13003B60)", 0x21D96C },
	{ "Elevator (TTC) (0x13004D28)", 0x21EB30 },
	{ "Exit Podium Warp (0x13000720)", 0x21A538 },
	{ "Express Elevator (Immobile) (WDW) (0x130021C0)", 0x21BFCC },
	{ "Express Elevator (WDW) (0x13002194)", 0x21BFA0 },
	{ "Falling Donut Block (RR) (0x13005504)", 0x21F30C },
	{ "Falling Rock Pillar Base (0x130043E0)", 0x21E1EC },
	{ "Fire Bar Spinner (LLL) (0x13001DA8)", 0x21BBB4 },
	{ "Floating Platform (JRB) (0x130042E4)", 0x21E0F0 },
	{ "Floating Rectangular Platform (WDW) (0x130042B4)", 0x21E0C0 },
	{ "Floating Square Platform (WDW) (0x13004284)", 0x21E090 },
	{ "Floating Wooden Plank (LLL) (0x13001E04)", 0x21BC10 },
	{ "Floor Trap (BitDW Entry) (0x13002A7C)", 0x21C888 },
	{ "Grindel (0x13000B58)", 0x21A964 },
	{ "Grindel (0x1300525C)", 0x21F064 },
	{ "Haunted Bookshelf (0x130028FC)", 0x21C708 },
	{ "Kickable Board (0x1300066C)", 0x21A478 },
	{ "Merry-Go-Round (0x13002968)", 0x21C774 },
	{ "Metal Mesh Moving Platform (LLL) (0x13001D78)", 0x21BB88 },
	{ "Moat Grill (Castle Grounds) (0x13001C58)", 0x21BA64 },
	{ "Moving Diamond Platform (BitDW) (0x13001408)", 0x21B214 },
	{ "Moving Pyramid Steps (SSL) (0x13003B30)", 0x21D93C },
	{ "Pendulum (TTC) (0x13004C24)", 0x21EA2C },
	{ "Pirate Ship (JRB) (0x130023EC)", 0x21C1F8 },
	{ "Piston (TTC) (0x13004C94)", 0x21EA9C },
	{ "Pushable Metal Box (0x13001518)", 0x21B324 },
	{ "Pyramid Top (SSL) (0x13003BB4)", 0x21D9C0 },
	{ "Rolling Log (LLL) (0x13003FA4)", 0x21DDB0 },
	{ "Rolling Log (TTM) (0x13003F40)", 0x21DD4C },
	{ "Rotating CCW (0x13001D14)", 0x21BB20 },
	{ "Rotating Hexagonal Platform (LLL) (0x13001D40)", 0x21BB4C },
	{ "Rotating Hexagonal Ring (LLL) (0x13001E6C)", 0x21BC78 },
	{ "Rotating Platform (TTC) (0x13004D90)", 0x21EB98 },
	{ "Rotating Platform with Fire (RR) (0x130010D8)", 0x21AEE4 },
	{ "Rotating Wooden Platform (WF) (0x130006E0)", 0x21A4EC },
	{ "Sign (0x130032E0)", 0x21D0EC },
	{ "Single Stair (Purple Switch) (0x1300286C)", 0x21C678 },
	{ "Sinking Cage Platform (BitFS) (0x13000FC8)", 0x21ADD4 },
	{ "Sinking Platforms (BitFS) (0x13000F9C)", 0x21ADA8 },
	{ "Sinking Rectangular Platform (LLL) (0x13001E94)", 0x21BCA0 },
	{ "Sinking Square Platform (LLL) (0x13001EC4)", 0x21BCD0 },
	{ "Sliding Jolly Rodger Box (0x130023A4)", 0x21C1B0 },
	{ "Sliding Platform (WF) (0x13003970)", 0x21D77C },
	{ "Snow Wave (SML) (0x13004244)", 0x21E050 },
	{ "Spindel (0x13003B00)", 0x21D90C },
	{ "Staircase (BBH) (0x13002898)", 0x21C6A4 },
	{ "Star Cage (0x13000F48)", 0x21AD58 },
	{ "Star Door (0x13000000)", 0x219E0C },
	{ "Static Platform (LLL) (0x13002018)", 0x21BE24 },
	{ "Static Rock (JRB) (0x130022B8)", 0x21C0C4 },
	{ "Stretching Platforms (BitFS) (0x13001064)", 0x21AE70 },
	{ "Sunken Ship Part 1 (JRB) (0x1300246C)", 0x21C274 },
	{ "Sunken Ship Part 2 (JRB) (0x13002480)", 0x21C288 },
	{ "Swinging Platform (0x130054B8)", 0x21F2C0 },
	{ "Thwomp 1 (0x13000BC8)", 0x21A9D0 },
	{ "Thwomp 2 (0x13000B8C)", 0x21A994 },
	{ "Tilting Inverted Pyramid (BitFS) (0x13001030)", 0x21AE3C },
	{ "Tilting Inverted Pyramid (LL) (0x13001EF8)", 0x21BD04 },
	{ "Top of THI (0x13000174)", 0x219F80 },
	{ "Tox Box (0x13001F90)", 0x21BD9C },
	{ "Trap Floor (BBH) (0x130028CC)", 0x21C6D8 },
	{ "Unused - Breaks on Ground Pound (0x130015E4)", 0x21B3F0 },
	{ "Unused Elevator (0x13000CC8)", 0x21AAD4 },
	{ "Unused Floating Box (0x13004400)", 0x21E20C },
	{ "Unused Static Platform (0x130029E4)", 0x21C7F0 },
	{ "Walking Penguin (SML) (0x13002E58)", 0x21CC64 },
	{ "Wall Trap (LLL Volcano) (0x13003F78)", 0x21DD84 },
	{ "Warp Pipe (0x130007A0)", 0x21A5B0 },
	{ "Water Level Pillar (0x13001C04)", 0x21BA10 },
	{ "Whomp (0x13002BCC)", 0x21C9E4 },
	{ "Whomp's Tower (0x130005D8)", 0x21A3E4 },
	{ "Whomp's Tower Door (0x130006A4)", 0x21A4B0 },
	{ "Whomp's Tower Elevator Platform (0x13001340)", 0x21B14C },
	{ "Whomp's Tower Sliding Platform (0x13001318)", 0x21B124 },
	{ "Whomp's Tower Solid Platform (0x13001368)", 0x21B174 },
	{ "Wooden Peg (0x1300481C)", 0x21E624 }
};
constexpr size_t NUM_BEHAVIOURS = sizeof( s_behaviours ) / sizeof( BehaviourCollision );

static inline uint getNewSimpleBehaviourAddress( LevelId levelId ) {
	HashSet<uint> usedAddresses;
	for( const auto &i : Blueprint::current()->levels().at( levelId ).simpleObjects ) {
		if( i.second.behaviour.type != BehaviourType::Autogenerated ) continue;
		usedAddresses.insert( i.second.behaviour.autoGen.segmentedAddress );
	}

	uint address = 0x0E150000 - 12;
	while( usedAddresses.count( address ) > 0 ) address -= 12;
	return address;
}

ObjectEditor::ObjectEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::ObjectEditor ),
	m_levelId( LevelId::INVALID ),
	m_objectId( 0 ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	for( size_t i = 0; i < NUM_BEHAVIOURS; i++ ) {
		m_ui->behaviourSelect->addItem( s_behaviours[i].name, QVariant::fromValue<uint>( s_behaviours[i].collisionPointerLocation ) );
	}
	for( ShadowType shape : Enum::values<ShadowType>() ) {
		m_ui->shadowShapeSelect->addItem( Enum::toString<ShadowType>( shape ), QVariant::fromValue<uint>( (uint)shape ) );
	}
	connect( &m_importModelDialog, SIGNAL(modelImported(ModelImportedEvent)), this, SLOT(objectModelImported(ModelImportedEvent)) );
	FORWARD_SIGNAL( m_ui->editButton, this, clicked(), openModelEditor() );
	FORWARD_SIGNAL( m_ui->editBehaviourButton, this, clicked(), openBehaviourEditor() );
	m_initializing = false;
}

ObjectEditor::~ObjectEditor() {
	delete m_ui;
}

void ObjectEditor::setObject( LevelId levelId, objectId objectId ) {
	m_initializing = true;

	m_levelId = levelId;
	m_objectId = objectId;

	const ObjectSpecs &object = Blueprint::current()->levels().at( levelId ).simpleObjects.at( objectId );

	m_ui->modelIdSelect->clear();
	for( ubyte modelId : Blueprint::current()->getFreeModelIds( levelId, object.modelId ) ) {
		m_ui->modelIdSelect->addItem( std::to_string( modelId ).c_str(), QVariant::fromValue<uint>( modelId ) );
	}

	m_ui->nameField->setText( object.name.c_str() );
	m_ui->modelIdSelect->setCurrentText( std::to_string( (uint)object.modelId ).c_str() );
	m_ui->geoTypeSelect->setCurrentIndex( object.shadow.enabled ? 1 : 0 );
	m_ui->cullGroup->setEnabled( !object.shadow.enabled );
	m_ui->autoCullRadio->setChecked( object.cullingRadius.autoCompute );
	m_ui->manualCullRadio->setChecked( !object.cullingRadius.autoCompute );
	m_ui->alwaysUprightCheckbox->setChecked( object.cullingRadius.alwaysUpright );
	m_ui->maxScaleSpinner->setValue( object.cullingRadius.maxScale );
	m_ui->widestFovSpinner->setValue( object.cullingRadius.maxFOV );
	m_ui->cullingRadiusSpinner->setValue( (int)object.cullingRadius.radius );
	m_ui->opacityGroup->setChecked( object.useObjectAlpha );
	m_ui->shadowGroup->setEnabled( object.shadow.enabled );
	m_ui->shadowShapeSelect->setCurrentText( Enum::toString<ShadowType>( object.shadow.shape ) );
	m_ui->shadowSizeSpinner->setValue( (int)object.shadow.size );
	m_ui->shadowOpacitySpinner->setValue( (double)object.shadow.opacity / 2.55 );
	m_ui->fogGroup->setChecked( object.fog.enabled );
	m_ui->fogColourSelect->setValue( object.fog.colour );
	m_ui->fogStartSpinner->setValue( object.fog.fadeStartDistance );
	m_ui->fogEndSpinner->setValue( object.fog.fadeEndDistance );
	m_ui->collisionGroup->setChecked( object.hasCollision );
	m_ui->asmRadio->setChecked( object.behaviour.type == BehaviourType::AsmReference );
	m_ui->behRadio->setChecked( object.behaviour.type == BehaviourType::Vanilla );
	m_ui->autogenerateRadio->setChecked( object.behaviour.type == BehaviourType::Autogenerated );
	m_ui->behaviourSelect->setEnabled( object.behaviour.type == BehaviourType::Vanilla );
	m_ui->editBehaviourButton->setEnabled( object.behaviour.type == BehaviourType::Autogenerated );
	m_ui->behaviourLabel->setVisible( object.behaviour.type == BehaviourType::Autogenerated );
	m_ui->objectOpacityCheckbox->setChecked( object.useObjectAlpha );

	char behaviourAddress[11];
	std::sprintf( behaviourAddress, "0x%08X", object.behaviour.autoGen.segmentedAddress );
	m_ui->behaviourLabel->setText( behaviourAddress );

	for( size_t i = 0; i < NUM_BEHAVIOURS; i++ ) {
		if( object.behaviour.romAddress == s_behaviours[i].collisionPointerLocation ) {
			m_ui->behaviourSelect->setCurrentIndex( (int)i );
			break;
		}
	}

	m_ui->editButton->setEnabled( Blueprint::current()->objectHasGeometry( levelId, objectId ) );

	m_initializing = false;
}

void ObjectEditor::dataChanged() {
	if( m_initializing ) return;

	ObjectSpecs &object = Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects.at( m_objectId );
	m_ui->cullingRadiusSpinner->setEnabled( m_ui->manualCullRadio->isChecked() );
	if( m_ui->autoCullRadio->isChecked() ) {
		const double maxDist = m_ui->alwaysUprightCheckbox->isChecked() ? object.max_xz : object.max_xyz;
		const double alpha = (double)m_ui->widestFovSpinner->value() * M_PI / 360.0;
		double scalingFactor = (4.0/3.0)*std::tan( alpha );
		scalingFactor = m_ui->maxScaleSpinner->value() * std::sqrt( 1.0 + ( scalingFactor * scalingFactor ) );
		const int offsideCullingRadius = (int)(0.5 + (scalingFactor * maxDist) );
		m_ui->cullingRadiusSpinner->setValue( std::max( offsideCullingRadius, (int)object.max_xyz ) );
	}

	object.modelId = (ubyte)m_ui->modelIdSelect->currentData( Qt::UserRole ).toUInt();
	object.name = m_ui->nameField->text().toStdString();
	object.cullingRadius.autoCompute = m_ui->autoCullRadio->isChecked();
	object.cullingRadius.alwaysUpright = m_ui->alwaysUprightCheckbox->isChecked();
	object.cullingRadius.maxScale = m_ui->maxScaleSpinner->value();
	object.cullingRadius.maxFOV = (short)m_ui->widestFovSpinner->value();
	object.cullingRadius.radius = (ushort)m_ui->cullingRadiusSpinner->value();
	object.shadow.shape = (ShadowType)m_ui->shadowShapeSelect->currentData( Qt::UserRole ).toUInt();
	object.shadow.size = (ushort)m_ui->shadowSizeSpinner->value();
	object.shadow.opacity = (ubyte)(m_ui->shadowOpacitySpinner->value() * 2.55);
	object.shadow.enabled = (m_ui->geoTypeSelect->currentIndex() == 1);
	object.fog.colour = (ColourRGB24)m_ui->fogColourSelect->value();
	object.fog.fadeStartDistance = m_ui->fogStartSpinner->value();
	object.fog.fadeEndDistance = m_ui->fogEndSpinner->value();
	object.fog.enabled = m_ui->fogGroup->isChecked();
	object.useObjectAlpha = m_ui->objectOpacityCheckbox->isChecked();
	object.hasCollision = m_ui->collisionGroup->isChecked();
	if( m_ui->autogenerateRadio->isChecked() ) {
		if( object.behaviour.type != BehaviourType::Autogenerated ) {
			object.behaviour.type = BehaviourType::Autogenerated;
			object.behaviour.autoGen.segmentedAddress = getNewSimpleBehaviourAddress( m_levelId );
		}
	} else if( m_ui->behRadio->isChecked() ) {
		object.behaviour.type = BehaviourType::Vanilla;
		object.behaviour.romAddress = m_ui->behaviourSelect->currentData( Qt::UserRole ).toUInt();
	} else {
		object.behaviour.type = BehaviourType::AsmReference;
	}

	m_ui->editBehaviourButton->setEnabled( object.behaviour.type == BehaviourType::Autogenerated );
	m_ui->behaviourLabel->setVisible( object.behaviour.type == BehaviourType::Autogenerated );

	char behaviourAddress[11];
	std::sprintf( behaviourAddress, "0x%08X", object.behaviour.autoGen.segmentedAddress );
	m_ui->behaviourLabel->setText( behaviourAddress );

	m_ui->cullGroup->setEnabled( !object.shadow.enabled );
	m_ui->shadowGroup->setEnabled( object.shadow.enabled );
	m_ui->behaviourSelect->setEnabled( object.behaviour.type == BehaviourType::Vanilla );
}

void ObjectEditor::importModel() {
	m_importModelDialog.open();
}

void ObjectEditor::objectModelImported( ModelImportedEvent event ) {
	ObjectSpecs &object = Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects.at( m_objectId );
	BoundingBox bounds = ObjectModelImportHelper( this, m_levelId, m_objectId ).handleImport( event );
	if( !bounds.isEmpty() ) {
		object.max_xz = (ushort)std::max({ (short)-bounds.min_x, bounds.max_x, (short)-bounds.min_z, bounds.max_z });
		object.max_xyz = (ushort)std::max({ (short)object.max_xz, (short)-bounds.min_y, bounds.max_y });
		emit openModelEditor();
	}
}
