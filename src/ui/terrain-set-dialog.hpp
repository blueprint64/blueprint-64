#ifndef SRC_UI_TERRAIN_SET_DIALOG_HPP_
#define SRC_UI_TERRAIN_SET_DIALOG_HPP_

#include <QDialog>

namespace Ui {
	class TerrainSetDialog;
}

class TerrainSetDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::TerrainSetDialog *m_ui;
	bool m_ignoreEvents;

	void initialize();

	public:
	explicit TerrainSetDialog( QWidget *parent = nullptr );
	~TerrainSetDialog();

	public slots:
	virtual void open() override {
		initialize();
		QDialog::open();
	}

	virtual int exec() override {
		initialize();
		return QDialog::exec();
	}

	void setSelected();
	void nameChanged();
	void soundChanged() const;

};



#endif /* SRC_UI_TERRAIN_SET_DIALOG_HPP_ */
