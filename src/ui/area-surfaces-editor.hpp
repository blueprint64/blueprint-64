#ifndef SRC_UI_AREA_SURFACES_EDITOR_HPP_
#define SRC_UI_AREA_SURFACES_EDITOR_HPP_

#include "src/ui/surfaces-editor.hpp"

class AreaSurfacesEditor : public SurfacesEditor {
	Q_OBJECT

	private:
	ubyte m_areaIndex;

	public:
	explicit AreaSurfacesEditor( QWidget *parent = nullptr );
	virtual ~AreaSurfacesEditor() {}

	void setArea( LevelId levelId, ubyte areaIndex );

	public slots:
	virtual void saveData() const override;
};


#endif /* SRC_UI_AREA_SURFACES_EDITOR_HPP_ */
