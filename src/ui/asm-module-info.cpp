#include "src/ui/asm-module-info.hpp"
#include "ui_asm-module-info.h"

#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QCheckBox>
#include <queue>
#include <cassert>
#include "src/core/blueprint.hpp"
#include "src/ui/widgets/module-property-select.hpp"
#include "src/ui/util.hpp"
#include "src/core/util.hpp"

AsmModuleInfo::AsmModuleInfo( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::AsmModuleInfo ),
	m_developMode( false )
{
	m_ui->setupUi( this );
	FORWARD_SIGNAL( m_ui->addOrRemoveButton, this, clicked(), addOrRemove() );
	FORWARD_SIGNAL( m_ui->installButton, this, clicked(), install() );
	FORWARD_SIGNAL( m_ui->updateButton, this, clicked(), update() );
	connect( &m_areaScopeDialog, &QDialog::finished, [=](){
		setModule( m_moduleId, m_moduleVersion );
	});
}

AsmModuleInfo::~AsmModuleInfo() {
	delete m_ui;
}

static inline const AsmModuleDefinition &getModule(
	const Uuid &moduleId,
	const Version &version,
	const AsmModule **activeModule
) {
	if( Blueprint::current()->hasModule( moduleId, version ) ) {
		*activeModule = &Blueprint::current()->asmModules().at( moduleId );
		return (*activeModule)->definition;
	} else {
		*activeModule = nullptr;
		return AsmModuleStore::getModuleVersions( moduleId ).at( version );
	}
}

static inline bool canUpdateModule( const Uuid &moduleId, const Version &version ) {
	const std::map<Version,AsmModuleDefinition> &installedVersions = AsmModuleStore::getModuleVersions( moduleId );
	return !installedVersions.empty() && installedVersions.rbegin()->first > version;
}

static inline void setTextWithColour( QLabel *label, const char *text, const QColor &colour ) {
	label->setText( text );
	QPalette palette = label->palette();
	palette.setColor( QPalette::WindowText, colour );
	palette.setColor( QPalette::Disabled, QPalette::WindowText, QColor::fromHsv( colour.hue(), colour.saturation() / 2, colour.value() ) );
	label->setPalette( palette );
}

void AsmModuleInfo::setModule( const Uuid &moduleId, const Version &version ) {
	m_moduleId = moduleId;
	m_moduleVersion = version;

	const AsmModule *activeModule;
	const AsmModuleDefinition &module = getModule( moduleId, version, &activeModule );
	const bool active = (activeModule != nullptr);

	m_ui->addOrRemoveButton->setIcon( QIcon::fromTheme( active ? "delete" : "list-add" ) );
	m_ui->addOrRemoveButton->setText( active ? "Remove Module" : "Add Module" );
	m_ui->addOrRemoveButton->setVisible( !m_developMode );

	m_ui->installButton->setVisible( !m_developMode && !AsmModuleStore::hasModuleVersion( moduleId, version ) );
	m_ui->updateButton->setVisible( false ); //TODO support this
	//m_ui->updateButton->setVisible( canUpdateModule( moduleId, version ) );

	m_ui->nameLabel->setText( module.name.c_str() );
	m_ui->versionLabel->setText( module.version.toString().c_str() );
	m_ui->authorLabel->setText( module.author.c_str() );
	m_ui->descriptionLabel->setText( module.description.c_str() );
	if( !module.consoleCompatible.has_value() ) {
		setTextWithColour( m_ui->compatibilityLabel, "UNKNOWN", QColor::fromRgb( 240, 80, 0 ) );
	} else if( module.consoleCompatible.value() ) {
		setTextWithColour( m_ui->compatibilityLabel, "YES", QColor::fromRgb( 0, 128, 0 ) );
	} else {
		setTextWithColour( m_ui->compatibilityLabel, "NO", QColor::fromRgb( 170, 0, 0 ) );
	}

	UiUtil::clearLayout( m_ui->dependencyList );
	if( module.dependencies.empty() ) {
		m_ui->dependencyContainer->setVisible( false );
	} else {
		m_ui->dependencyContainer->setVisible( true );

		for( const AsmModuleDependency &dependency : module.dependencies ) {
			string dString = "• "s + dependency.name + " (" + dependency.minVersion.toString() + " ≤ x ";
			if( dependency.maxVersion.patch == 0xFFFF ) {
				if( dependency.maxVersion.minor == 0xFFFF ) {
					dString += "< "s + std::to_string( dependency.maxVersion.major + 1 ) + ".0.0)";
				} else {
					dString += "< "s + std::to_string( dependency.maxVersion.major ) + "." + std::to_string( dependency.maxVersion.minor + 1 ) + ".0)";
				}
			} else {
				dString += "≤ "s + dependency.maxVersion.toString() + ")";
			}
			m_ui->dependencyList->addWidget( new QLabel( dString.c_str() ) );
		}
	}

	m_ui->areaContainer->setVisible( module.areaScoped );
	if( module.areaScoped ) {
		UiUtil::clearLayout( m_ui->areaList );
		m_ui->editAreasButton->setEnabled( activeModule != nullptr );
		if( activeModule == nullptr ) {
			m_ui->noAreaLabel->setVisible( true );
		} else {
			bool hasAreas = false;
			for( int i = 0; i < 40; i++ ) {
				const ubyte mask = activeModule->areaMask[i];

				if( mask == 0 ) continue;
				hasAreas = true;

				string aString;
				if( Blueprint::current()->hasLevel( (LevelId)i ) ) {
					aString = Blueprint::current()->levels().at( (LevelId)i ).name + " (";
				} else {
					aString = string( Enum::toString<LevelId>( (LevelId) i ) ) + " (";
				}

				if( mask == 0xFF ) {
					aString += "all areas)";
					m_ui->areaList->addWidget( new QLabel( aString.c_str() ) );
					continue;
				}

				std::queue<int> activeAreas;
				for( int j = 0; j < 8; j++ ) {
					if( mask & (1 << j) ) {
						activeAreas.push( j );
					}
				}

				assert( !activeAreas.empty() );
				assert( activeAreas.size() < 8 );

				if( activeAreas.size() == 1 ) {
					aString += "area "s + std::to_string( activeAreas.front() ) + ')';
					m_ui->areaList->addWidget( new QLabel( aString.c_str() ) );
					continue;
				}

				aString += "areas "s + std::to_string( activeAreas.front() );
				activeAreas.pop();
				while( activeAreas.size() > 1 ) {
					aString += ", "s + std::to_string( activeAreas.front() );
					activeAreas.pop();
				}

				assert( !activeAreas.empty() );
				aString += " and "s + std::to_string( activeAreas.front() ) + ')';
				m_ui->areaList->addWidget( new QLabel( aString.c_str() ) );
			}

			m_ui->noAreaLabel->setVisible( !hasAreas );
		}
	}

	UiUtil::clearLayout( m_ui->parameterList );
	if( module.properties.empty() ) {
		m_ui->propertiesLabel->setVisible( false );
	} else {
		m_ui->propertiesLabel->setVisible( true );
		for( size_t i = 0; i < module.properties.size(); i++ ) {
			const AsmModuleProperty &property = module.properties[i];

			ModulePropertySelect *field = new ModulePropertySelect( this, module.id, property );
			field->setEnabled( activeModule != nullptr );
			if( activeModule != nullptr ) {
				if( const auto currentValue = Util::tryGetValue( activeModule->parameters, property.name ) ) {
					field->setValue( *currentValue );
				} else {
					field->setValue( property.defaultValue );
				}
			} else {
				field->setValue( property.defaultValue );
			}

			m_ui->parameterList->addWidget( field );
		}
	}
}

void AsmModuleInfo::editAreas() {
	m_areaScopeDialog.open( m_moduleId );
}
