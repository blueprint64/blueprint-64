#include "src/ui/end-screen-dialog.hpp"
#include "ui_end-screen-dialog.h"

#include <QImage>
#include <QMessageBox>
#include "src/polyfill/file-dialog.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/ending.hpp"
#include "src/ui/util.hpp"

EndScreenDialog::EndScreenDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::EndScreenDialog )
{
	m_ui->setupUi( this );
}

EndScreenDialog::~EndScreenDialog() {
	delete m_ui;
}


void EndScreenDialog::replaceImage() {
	const fs::path imgPath = FileDialog::getFileOpen(
		"Select an Image (320x240)",
		{ { "Images", "*.png;*.bmp;*.gif;*.jpg;*.jpeg;*.xpm;*.xbm;*.pbm;*.pgm;*.ppm" } }
	);

	if( imgPath.empty() ) return;
	//TODO: choose resize method
	if( EndScreen::saveImage( imgPath.u8string().c_str(), EndScreen::ResizeMethod::Stretch ) ) {
		QMessageBox::information( this, "End Screen Imported", "End Screen was imported successfully.", QMessageBox::Ok );
	} else {
		QMessageBox::critical( this, "Importer Machine Broke", "Failed to import end screen. Is it a valid image format?", QMessageBox::Ok );
	}
	updateImage();
}

void EndScreenDialog::revertImage() {
	EndScreen::removeImage();
	QMessageBox::information( this, "End Screen Reverted", "End Screen has been reverted to its original form.", QMessageBox::Ok );
	close();
}

void EndScreenDialog::updateImage() {
	QImage image( Blueprint::current()->getEndScreenPath().u8string().c_str(), "PNG" );
	if( image.isNull() || image.width() != 320 || image.height() != 240 ) {
		m_ui->imagePreview->clearPixmap( 320, 240 );
	} else {
		m_ui->imagePreview->setPixmap( QPixmap::fromImage( image ), false );
	}
}

void EndScreenDialog::showEvent( QShowEvent *event ) {
	m_ui->interpolationCheckbox->setChecked( Blueprint::current()->endScreenHasInterpolation() );
	updateImage();
	QDialog::showEvent( event );
}

void EndScreenDialog::setInterpolation( bool useInterpolation ) {
	Blueprint::currentEditable()->setEndScreenInterpolation( useInterpolation );
}
