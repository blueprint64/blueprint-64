#include "src/ui/water-texture-import-dialog.hpp"
#include "ui_water-texture-import-dialog.h"

#include <QImage>
#include <QMessageBox>
#include <QStandardItemModel>

#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/fastmath.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/image-filter.hpp"
#include "src/core/util.hpp"
#include "src/core/enums.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/import/texture.hpp"
#include "src/ui/util.hpp"

WaterTextureImportDialog::WaterTextureImportDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::WaterTextureImportDialog )
{
	m_ui->setupUi( this );
	for( TextureFormat format : Enum::values<TextureFormat>() ) {
		m_ui->formatSelect->addItem( Enum::toString<TextureFormat>( format ), QVariant::fromValue<uint>( (uint)format ) );
	}
}

WaterTextureImportDialog::~WaterTextureImportDialog() {
	delete m_ui;
}

void WaterTextureImportDialog::setImage( QImage &image ) {
	const bool useIntegerScaling = (128 % image.width() == 0 && 128 % image.height() == 0);
	m_texture = QPixmap::fromImage( image.scaled( 128, 128, Qt::KeepAspectRatio, useIntegerScaling ? Qt::FastTransformation : Qt::SmoothTransformation ) );

	const TextureInfo info = TextureFile::getImageInfo( (const void*)&image );
	const QStandardItemModel *model = dynamic_cast<QStandardItemModel*>( m_ui->formatSelect->model() );
	const HashSet<TextureFormat> allowedFormats = info.getPossibleFormats();
	for( int i = 0; i < m_ui->formatSelect->count(); i++ ) {
		model->item( i )->setEnabled( allowedFormats.count( (TextureFormat)m_ui->formatSelect->itemData( i, Qt::UserRole ).toUInt() ) > 0 );
	}
	m_ui->formatSelect->setCurrentText( Enum::toString<TextureFormat>( info.suggestFormat() ) );

	m_ui->textureSettings->setEnabled( true );
	m_ui->dialogButtons->button( QDialogButtonBox::Ok )->setEnabled( true );
	m_ui->textureSize->setText( (std::to_string( image.width() ) + "x" + std::to_string( image.height() )).c_str() );
	formatChanged();
}

void WaterTextureImportDialog::browseButtonClicked() {
	const fs::path texturePath = FileDialog::getFileOpen(
		"Select Image",
		{ { "Images", "*.png;*.bmp;*.gif;*.jpg;*.jpeg;*.xpm;*.xbm;*.pbm;*.pgm;*.ppm" } }
	);
	if( texturePath.empty() ) return;

	QImage image( texturePath.u8string().c_str() );
	if( image.isNull() ) {
		QMessageBox::critical( this, "Invalid Image", "Failed to open image file.", QMessageBox::Ok );
		return;
	}

	if( image.width() * image.height() > 8192 ) {
		if( QMessageBox::warning( this, "Resize Texture?", "This texture is too large and will be resized.", QMessageBox::Ok | QMessageBox::Abort ) != QMessageBox::Ok ) {
			return;
		}
		ImageFilter::makeValidTextureSize( image );
	}

	if( !isPowerOf2( image.width() ) || !isPowerOf2( image.height() ) ) {
		if( QMessageBox::warning( this, "Resize Texture?", "This texture's dimensions are not powers of two, so it will be resized.", QMessageBox::Ok | QMessageBox::Abort ) != QMessageBox::Ok ) {
			return;
		}
		ImageFilter::makeValidTextureSize( image );
	}

	m_ui->filePath->setText( texturePath.u8string().c_str() );
	m_ui->nameField->setText( toAlphanumeric( texturePath.stem().u8string() ).c_str() );
	setImage( image );
}

void WaterTextureImportDialog::nameChanged() {
	m_ui->nameField->setText( toAlphanumeric( m_ui->nameField->text().toStdString() ).c_str() );
}

void WaterTextureImportDialog::formatChanged() {
	if( m_texture.isNull() ) return;
	const TextureFormat format = (TextureFormat)m_ui->formatSelect->currentData( Qt::UserRole ).toUInt();

	if( format == TextureFormat::RGBA32 ) {
		m_ui->texturePreview->setPixmap( m_texture, true );
		return;
	}

	QImage preview = m_texture.toImage();
	ImageFilter::applyColourFormat( preview, format );
	m_ui->texturePreview->setPixmap( QPixmap::fromImage( preview ), true );
}

void WaterTextureImportDialog::open() {
	m_texture = QPixmap();
	m_prevState = WaterTextureInfo();
	m_ui->filePath->clear();
	m_ui->filePath->setEnabled( true );
	m_ui->browseButton->setEnabled( true );
	m_ui->textureSettings->setEnabled( false );
	m_ui->dialogButtons->button( QDialogButtonBox::Ok )->setEnabled( false );
	m_ui->texturePreview->clearPixmap( 128, 128 );
	m_ui->textureSize->setText( "" );
	m_ui->dataAsmRef->setChecked( false );
	m_ui->nameGroup->setEnabled( false );
	m_ui->nameField->setText( "" );
	m_ui->formatSelect->setCurrentText( Enum::toString<TextureFormat>( TextureFormat::RGBA16 ) );
	QDialog::open();
	m_ui->texturePreview->repaint();
}

void WaterTextureImportDialog::open( const WaterTextureInfo &texture ) {
	m_prevState = texture;
	const string filePath = (Blueprint::current()->getWaterTexturePath() / (texture.textureHash + ".png")).u8string();
	m_ui->filePath->setText( filePath.c_str() );
	m_ui->filePath->setEnabled( false );
	m_ui->browseButton->setEnabled( false );
	m_ui->dialogButtons->button( QDialogButtonBox::Ok )->setEnabled( true );
	m_ui->filePath->setText( filePath.c_str() );
	m_ui->dataAsmRef->setChecked( texture.asmRef );
	m_ui->nameGroup->setEnabled( texture.asmRef );
	m_ui->nameField->setText( texture.asmRef ? texture.name.c_str() : texture.textureHash.c_str() );
	QImage img( filePath.c_str(), "PNG" );
	setImage( img );
	m_ui->formatSelect->setCurrentText( Enum::toString<TextureFormat>( texture.textureFormat ) );
	QDialog::open();
}

void WaterTextureImportDialog::accept() {
	TextureFile texture;
	if( m_ui->filePath->isEnabled() ) {
		if( TextureFile::tryOpen( m_ui->filePath->text().toStdString().c_str(), texture ) != TextureFile::Error ) {
			const string hash = texture.info().hash;

			fs::create_directories( Blueprint::current()->getWaterTexturePath() );
			texture.saveAsPng( (Blueprint::current()->getWaterTexturePath() / (hash + ".png")).u8string().c_str() );
			WaterTextureInfo textureInfo = {
				hash,
				m_ui->nameField->text().toStdString(),
				(TextureFormat)m_ui->formatSelect->currentData( Qt::UserRole ).toUInt(),
				m_ui->dataAsmRef->isChecked()
			};

			Blueprint::currentEditable()->waterTextures().insert( textureInfo );
			emit textureImported( textureInfo );
		}
	} else {
		HashSet<WaterTextureInfo> waterTextures = Blueprint::currentEditable()->waterTextures();
		WaterTextureInfo newState = {
			m_prevState.textureHash,
			m_ui->nameField->text().toStdString(),
			(TextureFormat)m_ui->formatSelect->currentData( Qt::UserRole ).toUInt(),
			m_ui->dataAsmRef->isChecked()
		};
		waterTextures.erase( m_prevState );
		waterTextures.insert( newState );
		for( auto &i : Blueprint::currentEditable()->areas() ) {
			for( WaterBoxSpecs &j : i.second.waterBoxes ) {
				if( j.texture == m_prevState ) {
					j.texture = newState;
				}
			}
		}
		emit textureChanged( m_prevState, newState );
	}
	QDialog::accept();
}
