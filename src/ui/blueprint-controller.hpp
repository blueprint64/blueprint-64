#ifndef UI_BLUEPRINT_CONTROLLER_HPP_
#define UI_BLUEPRINT_CONTROLLER_HPP_

#include <QObject>
#include "src/core/blueprint.hpp"

class BlueprintController : public QObject {
	Q_OBJECT

	public:
	explicit BlueprintController( QObject *parent = nullptr ) : QObject( parent ) {}
	~BlueprintController() {}

	signals:
	void blueprintLoaded();
	void loadFailed( string reason );

	public slots:
	void createProject( CreateProjectOptions options );
	void openProject( string filePath );

};



#endif /* UI_BLUEPRINT_CONTROLLER_HPP_ */
