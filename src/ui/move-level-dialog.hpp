#ifndef SRC_UI_MOVE_LEVEL_DIALOG_HPP_
#define SRC_UI_MOVE_LEVEL_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class MoveLevelDialog;
}

class MoveLevelDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::MoveLevelDialog *m_ui;
	LevelId m_levelId;

	public:
	explicit MoveLevelDialog( QWidget *parent = nullptr );
	~MoveLevelDialog();

	void open( LevelId levelId );

	public slots:
	virtual void accept() override;
	void levelSelected();

};

#endif /* SRC_UI_MOVE_LEVEL_DIALOG_HPP_ */
