#include "src/ui/simple-behaviour-editor.hpp"
#include "ui_simple-behaviour-editor.h"

#include <QPushButton>
#include <QCheckBox>
#include <algorithm>
#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

SimpleBehaviourEditor::SimpleBehaviourEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::SimpleBehaviourEditor ),
	m_loopInstruction( nullptr ),
	m_levelId( LevelId::INVALID ),
	m_objectId( 0 ),
	m_ignoreEvents( true )
{
	m_ui->setupUi( this );

	m_ui->collisionRadioGroup->setId( m_ui->collisionRadioMinimum, (int)CollisionDistanceMode::Minimal );
	m_ui->collisionRadioGroup->setId( m_ui->collisionRadioSuggested, (int)CollisionDistanceMode::Suggested );
	m_ui->collisionRadioGroup->setId( m_ui->collisionRadioInfinite, (int)CollisionDistanceMode::Infinite );
	m_ui->collisionRadioGroup->setId( m_ui->collisionRadioManual, (int)CollisionDistanceMode::Manual );

	CONNECT_CHECKBOX( m_ignoreEvents, getModel, movesMario, movesMarioCheckbox );
	CONNECT_INTEGER_SPINNER( m_ignoreEvents, getConstModel, getModel, collisionDistance, collisionDistanceSpinner, short );
	CONNECT_INTEGER_SPINNER( m_ignoreEvents, getConstModel, getModel, renderDistance, renderDistanceSpinner, short );

	connect( m_ui->collisionRadioGroup, SIGNAL( buttonToggled(int,bool) ), this, SLOT( collisionDistanceModeChanged(int,bool) ) );
	connect( m_ui->renderRadioGroup, SIGNAL( buttonToggled(QAbstractButton*,bool) ), this, SLOT( renderDistanceModeChanged() ) );

	connect( m_ui->angleDisplayRadioGroup, qOverload<QAbstractButton*,bool>( &QButtonGroup::buttonToggled ), this, [&](){
		for( int i = 0; i < m_ui->moveInstructions->layout()->count(); i++ ) {
			SimpleBehaviourStepPanel *instruction = dynamic_cast<SimpleBehaviourStepPanel*>( m_ui->moveInstructions->layout()->itemAt( i )->widget() );
			if( instruction == nullptr ) continue;
			instruction->setAnglesInHex( m_ui->unitsPerFrameRadio->isChecked() );
		}

	});

	m_ignoreEvents = false;
}

SimpleBehaviourEditor::~SimpleBehaviourEditor() {
	if( m_loopInstruction != nullptr ) {
		UiUtil::clearLayout( m_loopInstruction->layout() );
		m_loopInstruction->deleteLater();
	}
	delete m_ui;
}

SimpleBehaviourStepPanel *SimpleBehaviourEditor::createInstructionPanel( size_t index ) const {
	SimpleBehaviourStepPanel *panel = new SimpleBehaviourStepPanel( m_ui->moveInstructions, m_levelId, m_objectId, index );
	connect( panel, SIGNAL( moveUp(size_t) ), this, SLOT( moveUp(size_t) ) );
	connect( panel, SIGNAL( moveDown(size_t) ), this, SLOT( moveDown(size_t) ) );
	connect( panel, SIGNAL( remove(size_t) ), this, SLOT( removeInstruction(size_t) ) );
	panel->setAnglesInHex( m_ui->unitsPerFrameRadio->isChecked() );
	return panel;
}

void SimpleBehaviourEditor::setBehaviour( LevelId levelId, objectId objectId ) {
	m_ignoreEvents = true;
	m_levelId = levelId;
	m_objectId = objectId;

	if( m_loopInstruction != nullptr ) {
		UiUtil::clearLayout( m_loopInstruction->layout() );
		m_loopInstruction->deleteLater();
	}

	const ObjectSpecs &object = Blueprint::current()->levels().at( m_levelId ).simpleObjects.at( m_objectId );
	const SimpleBehaviour &behaviour = object.behaviour.autoGen;
	m_ui->collisionRadioGroup->button( (int)behaviour.collisionDistanceMode )->setChecked( true );
	m_ui->collisionDistanceSpinner->setEnabled( behaviour.collisionDistanceMode == CollisionDistanceMode::Manual );
	m_ui->collisionDistanceSpinner->setValue( behaviour.getComputedCollisionDistance( object.max_xyz ) );

	if( behaviour.renderDistance < 0 ) {
		m_ui->renderRadioInfinite->setChecked( true );
	} else {
		m_ui->renderRadioManual->setChecked( true );
	}
	m_ui->renderDistanceSpinner->setEnabled( behaviour.renderDistance >= 0 );
	m_ui->renderDistanceSpinner->setValue( behaviour.renderDistance );

	m_ui->movesMarioCheckbox->setEnabled( !behaviour.movementLoop.empty() );
	m_ui->movesMarioCheckbox->setChecked( behaviour.movesMario );

	UiUtil::clearLayout( m_ui->moveInstructions->layout() );
	QBoxLayout *instructionsLayout = dynamic_cast<QVBoxLayout*>( m_ui->moveInstructions->layout() );
	assert( instructionsLayout != nullptr );
	for( size_t i = 0; i < behaviour.movementLoop.size(); i++ ) {
		instructionsLayout->addWidget( createInstructionPanel( i ) );
	}

	QPushButton *addButton = new QPushButton( m_ui->moveInstructions );
	addButton->setIcon( QIcon::fromTheme( "list-add" ) );
	addButton->setText( "Add Step" );
	connect( addButton, SIGNAL(clicked()), this, SLOT(addInstruction()) );
	m_ui->moveInstructions->layout()->addWidget( addButton );

	m_loopInstruction = new QFrame( m_ui->moveInstructions );
	m_loopInstruction->setFrameStyle( QFrame::StyledPanel | QFrame::Raised );
	m_loopInstruction->setLineWidth( 1 );
	QVBoxLayout *layout = new QVBoxLayout( m_loopInstruction );
	m_loopInstruction->setLayout( layout );
	QLabel *loopLabel = new QLabel( "<b>Loop</b>", m_ui->moveInstructions );
	layout->addWidget( loopLabel, 0, Qt::AlignHCenter );
	QCheckBox *resetCheckbox = new QCheckBox( "Reset position and rotation", m_loopInstruction );
	layout->addWidget( resetCheckbox, 0, Qt::AlignLeft );
	resetCheckbox->setChecked( behaviour.resetAfterLoop );
	connect( resetCheckbox, &QCheckBox::toggled, this, [&](bool reset){
		getModel().resetAfterLoop = reset;
	});

	m_loopInstruction->setVisible( !behaviour.movementLoop.empty() );

	instructionsLayout->addWidget( addButton );
	instructionsLayout->addWidget( m_loopInstruction );
	instructionsLayout->addStretch( 1 );

	m_ignoreEvents = false;
}

const SimpleBehaviour &SimpleBehaviourEditor::getConstModel() const {
	return Blueprint::current()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).behaviour.autoGen;
}

SimpleBehaviour &SimpleBehaviourEditor::getModel() {
	return Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).behaviour.autoGen;
}

void SimpleBehaviourEditor::collisionDistanceModeChanged( int mode, bool enabled ) {
	if( !enabled ) return;
	const ObjectSpecs &object = Blueprint::current()->levels().at( m_levelId ).simpleObjects.at( m_objectId );
	SimpleBehaviour &behaviour = getModel();
	behaviour.collisionDistanceMode = (CollisionDistanceMode)mode;
	behaviour.collisionDistance = behaviour.getComputedCollisionDistance( object.max_xyz );
	m_ui->collisionDistanceSpinner->setEnabled( mode == (int)CollisionDistanceMode::Manual );
	m_ui->collisionDistanceSpinner->setValue( behaviour.collisionDistance );

}

static inline void refreshItem(  Ui::SimpleBehaviourEditor *ui, int index ) {
	QLayout *layout = ui->moveInstructions->layout();
	assert( layout->count() > index );
	SimpleBehaviourStepPanel *stepPanel = dynamic_cast<SimpleBehaviourStepPanel*>( layout->itemAt( index )->widget() );
	assert( stepPanel != nullptr );
	stepPanel->refresh();
}

void SimpleBehaviourEditor::renderDistanceModeChanged() {
	SimpleBehaviour &behaviour = getModel();
	if( m_ui->renderRadioManual->isChecked() ) {
		behaviour.renderDistance = 4000;
		m_ui->renderDistanceSpinner->setEnabled( true );
		m_ui->renderDistanceSpinner->setValue( 4000 );
	} else {
		behaviour.renderDistance = -1;
		m_ui->renderDistanceSpinner->setEnabled( false );
		m_ui->renderDistanceSpinner->setValue( -1 );
	}
}

void SimpleBehaviourEditor::addInstruction() {
	SimpleBehaviour &behaviour = getModel();
	behaviour.movementLoop.push_back( SimpleBehaviourMovement::Default() );

	dynamic_cast<QVBoxLayout*>( m_ui->moveInstructions->layout() )->insertWidget(
		(int)(behaviour.movementLoop.size() - 1),
		createInstructionPanel( behaviour.movementLoop.size() - 1 )
	);

	m_loopInstruction->setVisible( !behaviour.movementLoop.empty() );
	if( behaviour.movementLoop.size() > 1 ) {
		refreshItem( m_ui, (int)(behaviour.movementLoop.size() - 2) );
	}

	m_ui->movesMarioCheckbox->setEnabled( !behaviour.movementLoop.empty() );
}

void SimpleBehaviourEditor::removeInstruction( size_t index ) {
	SimpleBehaviour &behaviour = getModel();
	assert( behaviour.movementLoop.size() > index );
	behaviour.movementLoop.erase( behaviour.movementLoop.begin() + index );
	UiUtil::deleteItemAt( m_ui->moveInstructions->layout(), (int)index );
	for( size_t i = index; i < behaviour.movementLoop.size(); i++ ) {
		SimpleBehaviourStepPanel *step = dynamic_cast<SimpleBehaviourStepPanel*>( m_ui->moveInstructions->layout()->itemAt( (int)i )->widget() );
		if( step != nullptr ) step->decrementIndex();
	}

	m_loopInstruction->setVisible( !behaviour.movementLoop.empty() );
	m_ui->movesMarioCheckbox->setEnabled( !behaviour.movementLoop.empty() );
}

static void swapInstructions(
	std::vector<SimpleBehaviourMovement> &instructions,
	QLayout *layout,
	size_t a,
	size_t b
) {
	assert( a != b );
	assert( a < instructions.size() && b < instructions.size() );
	std::iter_swap( instructions.begin() + a, instructions.begin() + b );
	static_cast<SimpleBehaviourStepPanel*>( layout->itemAt( (int)a )->widget() )->refresh();
	static_cast<SimpleBehaviourStepPanel*>( layout->itemAt( (int)b )->widget() )->refresh();
}

void SimpleBehaviourEditor::moveUp( size_t index ) {
	assert( index > 0 );
	swapInstructions( getModel().movementLoop, m_ui->moveInstructions->layout(), index, index - 1 );
}

void SimpleBehaviourEditor::moveDown( size_t index ) {
	swapInstructions( getModel().movementLoop, m_ui->moveInstructions->layout(), index, index + 1 );
}
