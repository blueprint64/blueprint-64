#include "src/ui/texture-splitter-dialog.hpp"
#include "ui_texture-splitter-dialog.h"

#include <ios>
#include <vector>
#include <cctype>
#include <cassert>
#include <unordered_map>
#include <unordered_set>
#include <QMessageBox>
#include <QPainter>
#include "src/core/enums.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/fastmath.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/image-filter.hpp"
#include "src/ui/util.hpp"

namespace std {
	template <> struct hash<QPointF> {
		size_t operator()(const QPointF & point) const {
			const double x = point.x();
			const double y = point.y();

			const size_t A = std::hash<uint64>{}( reinterpret_cast<const uint64&>( x ) );
			const size_t B = std::hash<uint64>{}( reinterpret_cast<const uint64&>( y ) );

			return A ^ B;
		}
	};
}

#define ADD_FORMAT_OPTION( format ) m_ui->textureFormatSelect->addItem( Enum::toString( TextureFormat::format ), QVariant::fromValue<uint>( (uint)TextureFormat::format ) )

TextureSplitterDialog::TextureSplitterDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::TextureSplitterDialog ),
	m_hasAlpha( false ),
	m_ignoreEvents( true )
{
	m_ui->setupUi( this );
	ADD_FORMAT_OPTION( RGBA32 );
	ADD_FORMAT_OPTION( RGBA16 );
	ADD_FORMAT_OPTION( IA16 );
	ADD_FORMAT_OPTION( I8 );
	ADD_FORMAT_OPTION( IA8 );
	ADD_FORMAT_OPTION( I4 );
	ADD_FORMAT_OPTION( IA4 );
	m_ui->textureFormatSelect->setCurrentIndex( 0 );
	m_ignoreEvents = false;
	textureFormatChanged();
}

#undef ADD_FORMAT_OPTION

TextureSplitterDialog::~TextureSplitterDialog() {
	delete m_ui;
}

void TextureSplitterDialog::browse() {
	const fs::path imagePath = FileDialog::getFileOpen(
		"Select an Image",
		{ { "Images", "*.png;*.bmp;*.gif;*.jpg;*.jpeg;*.xpm;*.xbm;*.pbm;*.pgm;*.ppm" } }
	);

	if( imagePath.empty() ) return;

	m_image = QImage( imagePath.u8string().c_str() );
	if( m_image.isNull() ) {
		QMessageBox::critical( this, "Bad Image", "Failed to read image file." );
		m_ui->filePathDisplay->clear();
		m_ui->exportButton->setEnabled( false );
	}

	m_hasAlpha = false;
	for( int i = 0; i < m_image.width() && !m_hasAlpha; i++ ) {
		for( int j = 0; j < m_image.height(); j++ ) {
			if( m_image.pixelColor( i, j ).alpha() < 0xFF ) {
				m_hasAlpha = true;
				break;
			}
		}
	}

	m_ui->filePathDisplay->setText( imagePath.u8string().c_str() );
	m_ui->exportButton->setEnabled( true );

	m_ui->modelHeightSpinner->setValue( 10.0 );
	modelHeightChanged( 10.0 );
}

void TextureSplitterDialog::textureFormatChanged() {
	if( m_ignoreEvents ) return;
	const TextureFormat format = (TextureFormat)m_ui->textureFormatSelect->currentData( Qt::UserRole ).toUInt();
	m_ui->preserveQualityCheckbox->setEnabled( format != TextureFormat::RGBA32 );
}

void TextureSplitterDialog::modelWidthChanged( double width ) {
	if( m_ignoreEvents || m_image.isNull() ) return;

	const double ratio = (double)m_image.width() / (double)m_image.height();
	const double height = width / ratio;

	m_ignoreEvents = true;
	if( height > 50000.0 ) {
		m_ui->modelWidthSpinner->setValue( ratio * 50000.0 );
		m_ui->modelHeightSpinner->setValue( 50000.0 );
	} else if( height < 0.01 ) {
		m_ui->modelWidthSpinner->setValue( ratio * 0.01 );
		m_ui->modelHeightSpinner->setValue( 0.01 );
	} else {
		m_ui->modelHeightSpinner->setValue( height );
	}
	m_ignoreEvents = false;
}

void TextureSplitterDialog::modelHeightChanged( double height ) {
	if( m_ignoreEvents || m_image.isNull() ) return;

	const double ratio = (double)m_image.width() / (double)m_image.height();
	const double width = height * ratio;

	m_ignoreEvents = true;
	if( width > 50000.0 ) {
		m_ui->modelWidthSpinner->setValue( 50000.0 );
		m_ui->modelHeightSpinner->setValue( 50000.0 / ratio );
	} else if( width < 0.01 ) {
		m_ui->modelWidthSpinner->setValue( 0.01 );
		m_ui->modelHeightSpinner->setValue( 0.01 / ratio );
	} else {
		m_ui->modelWidthSpinner->setValue( width );
	}
	m_ignoreEvents = false;
}

static QSize getTargetTileSize( TextureFormat format ) {
	switch( format ) {
		case TextureFormat::RGBA32:
			return QSize( 30, 30 );
		case TextureFormat::RGBA16:
		case TextureFormat::IA16:
			return QSize( 62, 30 );
		case TextureFormat::I8:
		case TextureFormat::IA8:
			return QSize( 62, 62 );
		case TextureFormat::I4:
		case TextureFormat::IA4:
			return QSize( 126, 62 );
		default:
			assert( false );
	}
	return QSize();
}

static int getMaxPixelsForFormat( TextureFormat format ) {
	switch( format ) {
		case TextureFormat::RGBA32:
			return 1024;
		case TextureFormat::RGBA16:
		case TextureFormat::IA16:
			return 2048;
		case TextureFormat::I8:
		case TextureFormat::IA8:
			return 4096;
		case TextureFormat::I4:
		case TextureFormat::IA4:
			return 8192;
		default:
			assert( false );
	}
	return 0;
}

static QImage makeBorderedImage( const QImage &source, bool hwrap, bool vwrap ) {
	QImage borderedImage( source.width() + 2, source.height() + 2, source.format() );
	borderedImage.fill( 0u );

	QPainter painter( &borderedImage );
	painter.drawImage( QPoint( 1, 1 ), source );

	if( hwrap ) {
		painter.drawImage( QRect( 0, 1, 1, source.height() ), source, QRect( source.width() - 1, 0, 1, source.height() ) );
		painter.drawImage( QRect( source.width() + 1, 1, 1, source.height() ), source, QRect( 0, 0, 1, source.height() ) );
	} else {
		painter.drawImage( QRect( 0, 1, 1, source.height() ), source, QRect( 0, 0, 1, source.height() ) );
		painter.drawImage( QRect( source.width() + 1, 1, 1, source.height() ), source, QRect( source.width() - 1, 0, 1, source.height() ) );
	}

	if( vwrap ) {
		painter.drawImage( QRect( 1, 0, source.width(), 1 ), source, QRect( 0, source.height() - 1, source.width(), 1 ) );
		painter.drawImage( QRect( 1, source.height() + 1, source.width(), 1 ), source, QRect( 0, 1, source.width(), 1 ) );
	} else {
		painter.drawImage( QRect( 1, 0, source.width(), 1 ), source, QRect( 0, 0, source.width(), 1 ) );
		painter.drawImage( QRect( 1, source.height() + 1, source.width(), 1 ), source, QRect( 0, source.height() - 1, source.width(), 1 ) );
	}

	if( hwrap && vwrap ) {
		borderedImage.setPixelColor( 0, 0, source.pixelColor( source.width() - 1, source.height() - 1 ) );
		borderedImage.setPixelColor( source.width() + 1, 0, source.pixelColor( 0, source.height() - 1 ) );
		borderedImage.setPixelColor( 0, source.height() + 1, source.pixelColor( source.width() - 1, 0 ) );
		borderedImage.setPixelColor( source.width() + 1, source.height() + 1, source.pixelColor( 0, 0 ) );
	} else if( hwrap ) {
		borderedImage.setPixelColor( 0, 0, source.pixelColor( source.width() - 1, 0 ) );
		borderedImage.setPixelColor( source.width() + 1, 0, source.pixelColor( 0, 0 ) );
		borderedImage.setPixelColor( 0, source.height() + 1, source.pixelColor( source.width() - 1, source.height() - 1 ) );
		borderedImage.setPixelColor( source.width() + 1, source.height() + 1, source.pixelColor( 0, source.height() - 1 ) );
	} else if( vwrap ) {
		borderedImage.setPixelColor( 0, 0, source.pixelColor( 0, source.height() - 1 ) );
		borderedImage.setPixelColor( source.width() + 1, 0, source.pixelColor( source.width() - 1, source.height() - 1 ) );
		borderedImage.setPixelColor( 0, source.height() + 1, source.pixelColor( 0, 0 ) );
		borderedImage.setPixelColor( source.width() + 1, source.height() + 1, source.pixelColor( source.width() - 1, 0 ) );
	} else {
		borderedImage.setPixelColor( 0, 0, source.pixelColor( 0, 0 ) );
		borderedImage.setPixelColor( source.width() + 1, 0, source.pixelColor( source.width() - 1, 0 ) );
		borderedImage.setPixelColor( 0, source.height() + 1, source.pixelColor( 0, source.height() - 1 ) );
		borderedImage.setPixelColor( source.width() + 1, source.height() + 1, source.pixelColor( source.width() - 1, source.height() - 1 ) );
	}

	return borderedImage;
}

static inline uint roundUpToPowerOf2( uint x ) {
	x--;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	return x + 1;
}

struct TexTile {
	string texture;
	QRectF tile;
	QRectF uv;
};

static inline TexTile makeTile(
	const QImage &borderedImage,
	const fs::path &dir,
	TextureFormat format,
	double scale,
	bool hasAlpha,
	const string &name,
	int x,
	int y,
	int textureWidth,
	int textureHeight,
	int usedWidth,
	int usedHeight,
	int uStart,
	int uEnd,
	int vStart,
	int vEnd
) {
	if( uStart == 0 && vStart == 0 ) {
		QImage tileImage = borderedImage.copy( x, y, textureWidth + 2, textureHeight + 2 );
		if( !hasAlpha ) ImageFilter::removeAlpha( tileImage );
		ImageFilter::applyColourFormat( tileImage, format );
		tileImage.save( (dir / name).u8string().c_str() );
	}

	assert( textureWidth > 0 );
	assert( textureHeight > 0 );

	return TexTile{
		name,
		QRectF( scale * (double)x, scale * (double)y, scale * (double)usedWidth, scale * (double)usedHeight ),
		QRectF(
			QPointF( (double)(uStart + 1) / (double)(textureWidth + 2), (double)(textureHeight - vStart + 1) / (double)(textureHeight + 2) ),
			QPointF( (double)(uEnd + 1) / (double)(textureWidth + 2), (double)(textureHeight - vEnd + 1) / (double)(textureHeight + 2 ) )
		)
	};
}

static inline TexTile makeTile(
	const QImage &borderedImage,
	const fs::path &dir,
	TextureFormat format,
	double scale,
	bool hasAlpha,
	const string &name,
	int x,
	int y,
	int textureWidth,
	int textureHeight,
	int usedWidth,
	int usedHeight
) {
	return makeTile(
		borderedImage,
		dir,
		format,
		scale,
		hasAlpha,
		name,
		x,
		y,
		textureWidth,
		textureHeight,
		usedWidth,
		usedHeight,
		0,
		usedWidth,
		0,
		usedHeight
	);
}

static inline void addVertex(
	std::ostream &modelFile,
	HashMap<QPointF, int> &vertices,
	int &index,
	double modelHeight,
	const QPointF &point
) {
	if( vertices.find( point ) == vertices.end() ) {
		modelFile << "v " << point.x() << ' ' << (modelHeight - point.y()) << " 0.0" << std::endl;
		vertices[point] = index++;
	}
}

static inline void addVertices(
	std::ostream &modelFile,
	HashMap<QPointF, int> &vertices,
	int &index,
	double modelHeight,
	const QRectF &region
) {
	addVertex( modelFile, vertices, index, modelHeight, region.topLeft() );
	addVertex( modelFile, vertices, index, modelHeight, region.topRight() );
	addVertex( modelFile, vertices, index, modelHeight, region.bottomLeft() );
	addVertex( modelFile, vertices, index, modelHeight, region.bottomRight() );
}

static inline void addUV(
	std::ostream &modelFile,
	HashMap<QPointF, int> &uvs,
	int &index,
	const QPointF &point
) {
	if( uvs.find( point ) == uvs.end() ) {
		modelFile << "vt " << point.x() << ' ' << point.y() << std::endl;
		uvs[point] = index++;
	}
}

static inline void addUVs(
	std::ostream &modelFile,
	HashMap<QPointF, int> &uvs,
	int &index,
	const QRectF &region
) {
	addUV( modelFile, uvs, index, region.topLeft() );
	addUV( modelFile, uvs, index, region.topRight() );
	addUV( modelFile, uvs, index, region.bottomLeft() );
	addUV( modelFile, uvs, index, region.bottomRight() );
}

static inline string removeSpaces( string str ) {
	for( size_t i = 0; i < str.length(); i++ ) {
		if( std::isspace( str[i] ) ) {
			str[i] = '_';
		}
	}
	return str;
}


void TextureSplitterDialog::generateModel() {
	const fs::path sourcePath( m_ui->filePathDisplay->text().toStdString() );
	const fs::path outputDir = sourcePath.parent_path() / (sourcePath.stem().u8string() + "-tiles");
	std::error_code fsError;

	fs::tryDeleteRecursive( outputDir );
	fs::create_directories( outputDir, fsError );

	if( fsError ) {
		QMessageBox::critical( this, "Error", ("Failed to create output directory: "s + outputDir.u8string()).c_str() );
		return;
	}

	const string prefix = removeSpaces( sourcePath.stem().u8string() + '-' );
	const string extension = sourcePath.extension().u8string();

	const TextureFormat format = (TextureFormat)m_ui->textureFormatSelect->currentData( Qt::UserRole ).toUInt();
	const TextureFormat outputFormat = m_ui->preserveQualityCheckbox->isChecked() ? TextureFormat::RGBA32 : format;

	QSize tileSize = getTargetTileSize( format );
	if(
		tileSize.width() > m_image.width() ||
		tileSize.width() % m_image.height() == 0 ||
		tileSize.height() % m_image.width() == 0
	) {
		tileSize = QSize( tileSize.height(), tileSize.width() );
	}

	const int gridWidth = m_image.width() / tileSize.width();
	const int gridHeight = m_image.height() / tileSize.height();
	const int remainderWidth = m_image.width() % tileSize.width();
	const int remainderHeight = m_image.height() % tileSize.height();

	const bool wrapHorizontal = m_ui->loopHorizontalCheckbox->isChecked();
	const bool wrapVertical = m_ui->loopVerticalCheckbox->isChecked();

	const QImage borderedImage = makeBorderedImage( m_image, wrapHorizontal, wrapVertical );
	std::vector<TexTile> tiles;

	const double scale = m_ui->modelWidthSpinner->value() / (double)m_image.width();
	const double quadWidth = scale * (double)tileSize.width();
	const double quadHeight = scale * (double)tileSize.height();

	const QRectF quadUVs(
		QPointF( 1.0 / ((double)tileSize.width() + 2.0), ((double)tileSize.height() + 1.0) / ((double)tileSize.height() + 2.0) ),
		QPointF( ((double)tileSize.width() + 1.0) / ((double)tileSize.width() + 2.0), 1.0 / ((double)tileSize.height() + 2.0) )
	);

	for( int i = 0; i < gridWidth; i++ ) {
		for( int j = 0; j < gridHeight; j++ ) {
			const string tileName = prefix + std::to_string( i ) + '_' + std::to_string( j ) + extension;
			QImage tile = borderedImage.copy( i * tileSize.width(), j * tileSize.height(), tileSize.width() + 2, tileSize.height() + 2 );
			ImageFilter::applyColourFormat( tile, outputFormat );
			tile.save( (outputDir / tileName).u8string().c_str() );
			tiles.push_back({
				tileName,
				QRectF( quadWidth * (double)i, quadHeight * (double)j, quadWidth, quadHeight ),
				quadUVs
			});
		}
	}

	const int edgeWidth = (int)roundUpToPowerOf2( (uint)remainderWidth + 2 ) - 2;
	const int edgeHeight = (int)roundUpToPowerOf2( (uint)remainderHeight + 2 ) - 2;
	const int edgeX = m_image.width() - remainderWidth;
	const int edgeY = m_image.height() - remainderHeight;
	bool handledCorner = false;

	if( edgeHeight > 0 ) {
		const int maxWidth = (getMaxPixelsForFormat( format ) / (edgeHeight + 2)) - 2;
		assert( isPowerOf2( (uint)maxWidth + 2 ) );

		if( m_ui->goodVerticesCheckbox->isChecked() ) {
			const int span = maxWidth / tileSize.width();

			int i = 0; int j = 0;
			int lastX = 0;

			for( i = 0; lastX < edgeX; i++ ) {
				for( j = 0; j < span && lastX < edgeX; j++ ) {
					tiles.push_back( makeTile(
						borderedImage,
						outputDir,
						outputFormat,
						scale,
						m_hasAlpha,
						prefix + "bottom-" + std::to_string( i ) + extension,
						lastX,
						edgeY,
						maxWidth,
						edgeHeight,
						tileSize.width(),
						remainderHeight,
						j * tileSize.width(),
						(j + 1) * tileSize.width(),
						0,
						remainderHeight
					));
					lastX += tileSize.width();
				}
			}

			if( remainderWidth > 0 && j < span ) {
				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "bottom-" + std::to_string( i > 0 ? i - 1 : i ) + extension,
					lastX,
					edgeY,
					maxWidth,
					edgeHeight,
					remainderWidth,
					remainderHeight,
					j * tileSize.width(),
					(j * tileSize.width()) + remainderWidth,
					0,
					remainderHeight
				));
				handledCorner = true;
			}
		} else {
			int i = 0;
			while( (i + 1) * maxWidth <= edgeX ) {
				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "bottom-" + std::to_string( i ) + extension,
					i * maxWidth,
					edgeY,
					maxWidth,
					edgeHeight,
					maxWidth,
					remainderHeight
				));
				i++;
			}

			const int lastX0 = i * maxWidth;
			if( lastX0 != edgeX ) {
				int lastX1 = edgeX;
				if( lastX0 + maxWidth >= m_image.width() ) {
					lastX1 = m_image.width();
					handledCorner = true;
				}

				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "bottom-" + std::to_string( i ) + extension,
					lastX0,
					edgeY,
					(int)roundUpToPowerOf2( lastX1 - lastX0 + 2 ) - 2,
					edgeHeight,
					lastX1 - lastX0,
					remainderHeight
				));
			}
		}
	}

	if( edgeWidth > 0 ) {
		const int maxHeight = (getMaxPixelsForFormat( format ) / (edgeWidth + 2)) - 2;
		assert( isPowerOf2( (uint)maxHeight + 2 ) );

		if( m_ui->goodVerticesCheckbox->isChecked() ) {
			const int span = maxHeight / tileSize.height();

			int i = 0; int j = 0;
			int lastY = 0;

			for( i = 0; lastY < edgeY; i++ ) {
				for( j = 0; j < span && lastY < edgeY; j++ ) {
					tiles.push_back( makeTile(
						borderedImage,
						outputDir,
						outputFormat,
						scale,
						m_hasAlpha,
						prefix + "right-" + std::to_string( i ) + extension,
						edgeX,
						lastY,
						edgeWidth,
						maxHeight,
						remainderWidth,
						tileSize.height(),
						0,
						remainderWidth,
						j * tileSize.height(),
						(j + 1) * tileSize.height()
					));
					lastY += tileSize.height();
				}
			}

			if( remainderHeight > 0 && !handledCorner && j < span ) {
				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "right-" + std::to_string( i > 0 ? i - 1 : i ) + extension,
					edgeX,
					lastY,
					edgeWidth,
					maxHeight,
					remainderWidth,
					remainderHeight,
					0,
					remainderWidth,
					j * tileSize.height(),
					(j * tileSize.height()) + remainderHeight
				));
				handledCorner = true;
			}
		} else {
			int i = 0;
			while( (i + 1) * maxHeight <= edgeY ) {
				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "right-" + std::to_string( i ) + extension,
					edgeX,
					i * maxHeight,
					edgeWidth,
					maxHeight,
					remainderWidth,
					maxHeight
				));
				i++;
			}

			const int lastY0 = i * maxHeight;
			if( lastY0 != edgeY ) {
				int lastY1 = edgeY;
				if( !handledCorner && lastY0 + maxHeight >= m_image.width() ) {
					lastY1 = m_image.height();
					handledCorner = true;
				}

				tiles.push_back( makeTile(
					borderedImage,
					outputDir,
					outputFormat,
					scale,
					m_hasAlpha,
					prefix + "right" + std::to_string( i ) + extension,
					edgeX,
					lastY0,
					edgeWidth,
					(int)roundUpToPowerOf2( lastY1 - lastY0 + 2 ) - 2,
					remainderWidth,
					lastY1 - lastY0
				));
			}
		}

	}

	if( !handledCorner && edgeHeight > 0 && edgeWidth > 0 ) {
		tiles.push_back( makeTile(
			borderedImage,
			outputDir,
			outputFormat,
			scale,
			m_hasAlpha,
			prefix + "corner" + extension,
			edgeX,
			edgeY,
			(int)roundUpToPowerOf2( (uint)remainderWidth + 2 ) - 2,
			(int)roundUpToPowerOf2( (uint)remainderHeight + 2 ) - 2,
			remainderWidth,
			remainderHeight
		));
	}

	/* Export model */

	FileWriteStream modelFile( (outputDir / (prefix + "model.obj")).u8string(), std::ios_base::out | std::ios_base::trunc );
	modelFile << "mtllib " << prefix << "mat.mtl" << std::endl;
	modelFile << "o Image" << std::endl;
	HashMap<QPointF, int> vertexIndices;
	int currentIndex = 1;
	const double modelHeight = scale * m_image.height();
	for( const TexTile &tile : tiles ) {
		addVertices( modelFile, vertexIndices, currentIndex, modelHeight, tile.tile );
	}
	HashMap<QPointF, int> uvIndices;
	currentIndex = 1;
	for( const TexTile &tile : tiles ) {
		addUVs( modelFile, uvIndices, currentIndex, tile.uv );
	}
	modelFile << "vn 0.0 0.0 1.0" << std::endl;
	modelFile << "s off" << std::endl;
	if( m_ui->triangulateCheckbox->isChecked() ) {
		for( const TexTile &tile : tiles ) {
			modelFile << "usemtl " << tile.texture << std::endl;
			modelFile << "f "
				<< vertexIndices.at( tile.tile.bottomLeft() ) << '/'
				<< uvIndices.at( tile.uv.bottomLeft() ) << "/1 "

				<< vertexIndices.at( tile.tile.topRight() ) << '/'
				<< uvIndices.at( tile.uv.topRight() ) << "/1 "

				<< vertexIndices.at( tile.tile.topLeft() ) << '/'
				<< uvIndices.at( tile.uv.topLeft() ) << "/1"
			<< std::endl;
			modelFile << "f "
				<< vertexIndices.at( tile.tile.bottomLeft() ) << '/'
				<< uvIndices.at( tile.uv.bottomLeft() ) << "/1 "

				<< vertexIndices.at( tile.tile.bottomRight() ) << '/'
				<< uvIndices.at( tile.uv.bottomRight() ) << "/1 "

				<< vertexIndices.at( tile.tile.topRight() ) << '/'
				<< uvIndices.at( tile.uv.topRight() ) << "/1"
			<< std::endl;
		}
	} else {
		for( const TexTile &tile : tiles ) {
			modelFile << "usemtl " << tile.texture << std::endl;
			modelFile << "f "
				<< vertexIndices.at( tile.tile.topLeft() ) << '/'
				<< uvIndices.at( tile.uv.topLeft() ) << "/1 "

				<< vertexIndices.at( tile.tile.bottomLeft() ) << '/'
				<< uvIndices.at( tile.uv.bottomLeft() ) << "/1 "

				<< vertexIndices.at( tile.tile.bottomRight() ) << '/'
				<< uvIndices.at( tile.uv.bottomRight() ) << "/1 "

				<< vertexIndices.at( tile.tile.topRight() ) << '/'
				<< uvIndices.at( tile.uv.topRight() ) << "/1"
			<< std::endl;
		}
	}

	FileWriteStream materialsFile( (outputDir / (prefix + "mat.mtl")).u8string(), std::ios_base::out | std::ios_base::trunc );
	HashSet<string> materials;
	for( const TexTile &tile : tiles ) {
		if( materials.find( tile.texture ) == materials.end() ) {
			materialsFile << "newmtl " << tile.texture << std::endl;
			materialsFile << "map_Kd " << tile.texture << std::endl;
			materials.insert( tile.texture );
		}
	}

	QMessageBox::information( this, "Model Created", ("Saved models and textures to "s + outputDir.u8string()).c_str() );
}
