#ifndef SRC_UI_OBJECT_SURFACES_EDITOR_HPP_
#define SRC_UI_OBJECT_SURFACES_EDITOR_HPP_

#include "src/ui/surfaces-editor.hpp"
#include "src/core/objectid.hpp"

class ObjectSurfacesEditor : public SurfacesEditor {
	Q_OBJECT

	private:
	objectId m_objectId;

	public:
	explicit ObjectSurfacesEditor( QWidget *parent = nullptr );
	virtual ~ObjectSurfacesEditor() {}

	void setObject( LevelId levelId, objectId objectId );

	public slots:
	virtual void saveData() const override;
	virtual void objectDataChanged() override;
};



#endif /* SRC_UI_OBJECT_SURFACES_EDITOR_HPP_ */
