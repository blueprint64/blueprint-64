#include "src/ui/animation-dialog.hpp"
#include "ui_animation-dialog.h"

#include <cmath>

AnimationDialog::AnimationDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::AnimationDialog )
{
	m_ui->setupUi( this );
	QPalette dialPalette = m_ui->angleDial->palette();
	dialPalette.setColor( QPalette::Highlight, Qt::transparent );
	m_ui->angleDial->setPalette( dialPalette );

}

AnimationDialog::~AnimationDialog() {
	delete m_ui;
}

void AnimationDialog::open( const AnimationSpecs &animation ) {
	switch( animation.index() ) {
		case 0: { // None
			m_ui->noneRadio->setChecked( true );
			m_ui->linearSettings->setEnabled( false );
			m_ui->sineSettings->setEnabled( false );
			m_ui->xSpeedSelect->setValue( 0 );
			m_ui->ySpeedSelect->setValue( 0 );
			m_ui->angleDial->setValue( 53 );
			m_ui->amplitudeSelect->setValue( 32 );
			m_ui->periodSelect->setValue( 300 );
			break;
		}
		case 1: { // Linear
			const LinearAnimationSpecs &specs = std::get<LinearAnimationSpecs>( animation );
			m_ui->linearRadio->setChecked( true );
			m_ui->linearSettings->setEnabled( true );
			m_ui->sineSettings->setEnabled( false );
			m_ui->xSpeedSelect->setValue( (int)lround( ((double)specs.speedX) / (double)0x10000 * 7.5 ) );
			m_ui->ySpeedSelect->setValue( (int)lround( ((double)specs.speedY) / (double)0x10000 * 7.5 ) );
			m_ui->angleDial->setValue( 53 );
			m_ui->amplitudeSelect->setValue( 32 );
			m_ui->periodSelect->setValue( 300 );
			break;
		}
		case 2: { // Sine Wave
			const SineWaveAnimationSpecs &specs = std::get<SineWaveAnimationSpecs>( animation );
			const int direction = (int)lround( (double)specs.angle * 72.0 / (double)0x10000 );
			m_ui->sineRadio->setChecked( true );
			m_ui->linearSettings->setEnabled( false );
			m_ui->sineSettings->setEnabled( true );
			m_ui->xSpeedSelect->setValue( 0 );
			m_ui->ySpeedSelect->setValue( 0 );
			m_ui->angleDial->setValue( ((72 - direction) + 53) % 72 );
			m_ui->amplitudeSelect->setValue( (int)(specs.amplitude >> 2) );
			m_ui->periodSelect->setValue( (int)specs.period );
			break;
		}
	}
	directionChanged( m_ui->angleDial->value() );
	QDialog::open();
}

AnimationSpecs AnimationDialog::getAnimation() const {
	if( m_ui->linearRadio->isChecked() ) {
		LinearAnimationSpecs specs;
		specs.speedX = (int)lround( (double)(m_ui->xSpeedSelect->value() * 0x10000) / 7.5 );
		specs.speedY = (int)lround( (double)(m_ui->ySpeedSelect->value() * 0x10000) / 7.5 );
		return AnimationSpecs( specs );
	} else if( m_ui->sineRadio->isChecked() ) {
		SineWaveAnimationSpecs specs;
		specs.amplitude = (ushort)(m_ui->amplitudeSelect->value() << 2);
		specs.angle = (ushort)lround( (double)((125 - m_ui->angleDial->value()) % 72) / 72.0 * (double)0x10000 );
		specs.period = (ushort)m_ui->periodSelect->value();
		return AnimationSpecs( specs );
	}
	return AnimationSpecs();
}

void AnimationDialog::animationTypeChanged() {
	m_ui->linearSettings->setEnabled( m_ui->linearRadio->isChecked() );
	m_ui->sineSettings->setEnabled( m_ui->sineRadio->isChecked() );
}

void AnimationDialog::directionChanged( int value ) {
	const string degrees = std::to_string( 5 * ((125 - value) % 72) ) + "°";
	m_ui->degreesLabel->setText( degrees.c_str() );
}
