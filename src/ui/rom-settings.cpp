#include "src/ui/rom-settings.hpp"
#include "ui_rom-settings.h"

#include <QToolButton>
#include <QMessageBox>
#include <cstdio>
#include "src/core/blueprint.hpp"
#include "src/core/enums.hpp"
#include "src/core/ending.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/util.hpp"

RomSettingsWidget::RomSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::RomSettingsWidget ),
	m_ignoreUpdates( true )
{
	m_ui->setupUi( this );
	FORWARD_SIGNAL( m_ui->editTweaksButton, this, clicked(), editTweaks() );
	FORWARD_SIGNAL( m_ui->editDialogButton, this, clicked(), editDialog() );
	FORWARD_SIGNAL( m_ui->editMusicButton, this, clicked(), editMusic() );
	FORWARD_SIGNAL( m_ui->editBackgroundsButton, this, clicked(), editBackgrounds() );
	FORWARD_SIGNAL( m_ui->editModulesButton, this, clicked(), editModules() );
	FORWARD_SIGNAL( m_ui->editActionsButton, this, clicked(), editMarioActions() );
	for( LevelId levelId : Enum::values<LevelId>() ) {
		m_ui->pauseWarpLevelSelect->addItem( Enum::toString( levelId ), (uint)levelId );
		if( levelId != LevelId::EndScreen ) {
			m_ui->startingLevelSelect->addItem( Enum::toString( levelId ), (uint)levelId );
		}
	}
	// Enable the clear button on the ROM select even though its otherwise read-only
	QToolButton *unlinkRomButton = m_ui->targetRomField->findChild<QToolButton*>();
	if( unlinkRomButton != nullptr ) {
		unlinkRomButton->setEnabled( true );
		QObject::connect( unlinkRomButton, SIGNAL(clicked(bool)), this, SLOT(updateData()) );
		FORWARD_SIGNAL( unlinkRomButton, this, clicked(), romTargetChanged() );
	}
	m_ignoreUpdates = false;

	connect( &m_createLevelDialog, SIGNAL(levelCreated(LevelId)), this, SLOT(levelAdded(LevelId)) );
	connect( m_ui->importedLevelsList, &SimpleListWidget::itemMove, this, [=](){
		m_moveLevelDialog.open( (LevelId)m_ui->importedLevelsList->currentData<uint>() );
	});
	connect( &m_moveLevelDialog, SIGNAL(accepted()), this, SLOT(refreshLevelList()) );
	connect( m_ui->editTerrainButton, SIGNAL(clicked()), &m_terrainSetsDialog, SLOT(open()) );
}

RomSettingsWidget::~RomSettingsWidget() {
	delete m_ui;
}

static void addLevelToList( QListWidget *list, LevelId levelId ) {
	assert( Blueprint::current()->hasLevel( levelId ) );
	char hexId[6];
	std::sprintf( hexId, "0x%02X ", (ubyte)levelId );
	hexId[5] = '\0';
	const string idAndName = string() + hexId + Blueprint::current()->levels().find( levelId )->second.name;
	list->addItem( idAndName.c_str() );
	list->item( list->count() - 1 )->setData( Qt::UserRole, QVariant::fromValue<uint>( (uint)levelId ) );
}

void RomSettingsWidget::refreshLevelList() {
	m_ui->importedLevelsList->clear();
	for( const auto &i : Blueprint::current()->levels() ) {
		addLevelToList( m_ui->importedLevelsList, i.first );
	}
	m_ui->importedLevelsList->clearSelection();
	m_ui->editLevelButton->setEnabled( false );
}

void RomSettingsWidget::blueprintChanged() {
	m_ignoreUpdates = true;
	const Blueprint *blueprint = Blueprint::current();
	m_ui->targetRomField->setText( blueprint->isCollab() ? blueprint->targetRomName().c_str() : blueprint->targetRomPath().u8string().c_str() );

	m_ui->browseButton->setVisible( !blueprint->isCollab() );
	m_ui->targetRomField->setReadOnly( !blueprint->isCollab() );
	m_ui->modeSwitchButton->setText( blueprint->isCollab() ? "Switch to Single File Mode" : "Switch to Collaboration Mode" );
	m_ui->modeSwitchButton->setIcon( QIcon::fromTheme( blueprint->isCollab() ? "user-identities" : "system-users" ) );

	const RomSpecs &romSpecs = blueprint->romSpecs();
	m_ui->romNameField->setUtf8( romSpecs.internalName.c_str() );
	m_ui->levelBoundariesSelect->setCurrentIndex( (int)romSpecs.levelBoundaries );
	m_ui->startingLevelSelect->setCurrentText( Enum::toString( romSpecs.startingLevel ) );
	m_ui->pauseWarpLevelSelect->setCurrentText( Enum::toString( romSpecs.pauseWarp.level ));
	m_ui->pauseWarpAreaSelect->setValue( (int)romSpecs.pauseWarp.area );
	m_ui->pauseWarpIdSelect->setValue( (int)romSpecs.pauseWarp.warpId );
	m_ui->disableChecksumCheckbox->setChecked( !romSpecs.enableChecksum );
	m_ui->newLevelButton->setEnabled( blueprint->levels().size() < 30 );
	m_ui->cutoutDecalCheckbox->setChecked( romSpecs.cutoutDecal );
	m_ui->worldScaleSlider->setValue( (int)(4.f * romSpecs.worldScale - 3.5) );
	scaleSliderMoved( m_ui->worldScaleSlider->value() );
	refreshLevelList();
	updateAudioHeapCheckbox();
	m_ui->editLevelButton->setEnabled( false );
	m_ui->deleteLevelButton->setEnabled( false );

	m_ignoreUpdates = false;
}

void RomSettingsWidget::updateData() {
	if( m_ignoreUpdates ) return;
	RomSpecs &rom = Blueprint::currentEditable()->romSpecs();
	rom.internalName = m_ui->romNameField->getUtf8();
	rom.levelBoundaries = (LevelBoundarySize)m_ui->levelBoundariesSelect->currentIndex();
	rom.startingLevel = (LevelId)m_ui->startingLevelSelect->currentData().toUInt();
	rom.pauseWarp.level = (LevelId)m_ui->pauseWarpLevelSelect->currentData().toUInt();
	rom.pauseWarp.area = (ubyte)m_ui->pauseWarpAreaSelect->value();
	rom.pauseWarp.warpId = (ubyte)m_ui->pauseWarpIdSelect->value();
	rom.enableChecksum = !m_ui->disableChecksumCheckbox->isChecked();
	rom.expandAudioHeap = m_ui->expandAudioHeapCheckbox->isChecked();
	rom.cutoutDecal = m_ui->cutoutDecalCheckbox->isChecked();
	rom.worldScale = 1.f + (float)m_ui->worldScaleSlider->value() * 0.25f;
	scaleSliderMoved( m_ui->worldScaleSlider->value() );
	Blueprint::currentEditable()->setTargetRom( fs::to_path( m_ui->targetRomField->text().toStdString() ) );
}

void RomSettingsWidget::browseForRom() {
	fs::path targetRom = FileDialog::getFileSave(
		"Select a ROM",
		{ { "N64 ROMs", "*.z64" }, { "All Files", "*.*;*" } }
	);

	if( targetRom.empty() ) return;
	targetRom.replace_extension( ".z64" );

	m_ui->targetRomField->setText( targetRom.u8string().c_str() );
	Blueprint::currentEditable()->setTargetRom( targetRom );
	emit romTargetChanged();
}

void RomSettingsWidget::addLevel() {
	m_createLevelDialog.show();
}

void RomSettingsWidget::editSelectedLevel() {
	emit editLevel( (LevelId)m_ui->importedLevelsList->currentItem()->data( Qt::UserRole ).toUInt() );
}

void RomSettingsWidget::deleteSelectedLevel() {
	LevelId level = (LevelId)m_ui->importedLevelsList->currentItem()->data( Qt::UserRole ).toUInt();
	assert( Blueprint::current()->hasLevel( level ) );
	if( QMessageBox::question( this, "Delete Level?", "Are you sure you want to delete this level? This operation cannot be undone." ) == QMessageBox::Yes ) {
		Blueprint::currentEditable()->levels().erase( level );
		delete m_ui->importedLevelsList->takeItem( m_ui->importedLevelsList->currentRow() );
		m_ui->importedLevelsList->clearSelection();
		m_ui->editLevelButton->setEnabled( false );
		m_ui->deleteLevelButton->setEnabled( false );
		fs::forceDeleteRecursive( Blueprint::current()->getLevelPath( level ) );
		for( ubyte i = 0; i < 8; i++ ) {
			Blueprint::currentEditable()->areas().erase( AREA_ID( level, i ) );
			fs::forceDelete( Blueprint::current()->getAreaActorsPath( level, i ) );
		}
	}
}

void RomSettingsWidget::levelSelected() {
	m_ui->editLevelButton->setEnabled( true );
	m_ui->deleteLevelButton->setEnabled( true );
}

void RomSettingsWidget::levelAdded( LevelId levelId ) {
	addLevelToList( m_ui->importedLevelsList, levelId );
	emit editLevel( levelId );
}

void RomSettingsWidget::editEndScreen() {
	if( EndScreen::hasCustomImage() ) {
		m_endScreenDialog.open();
		return;
	}

	const fs::path imgFilePath = FileDialog::getFileOpen(
		"Select an Image (320x240)",
		{ { "Images", "*.png;*.bmp;*.gif;*.jpg;*.jpeg;*.xpm;*.xbm;*.pbm;*.pgm;*.ppm" } }
	);

	if( imgFilePath.empty() ) return;

	//TODO: choose resize method
	if( EndScreen::saveImage( imgFilePath.u8string(), EndScreen::ResizeMethod::Stretch ) ) {
		QMessageBox::information( this, "End Screen Imported", "End Screen was imported successfully.", QMessageBox::Ok );
	} else {
		QMessageBox::critical( this, "Importer Machine Broke", "Failed to import end screen. Is it a valid image format?", QMessageBox::Ok );
	}

}

static const char *SWITCH_TO_COLLAB_MESSAGE = ""
	"In collaboration mode, instead of saving your blueprint to a single "
	"compressed file, the components of your blueprint will be saved to a "
	"folder, making it more suitable for use with version control such as Git.";

static const char *SWITCH_TO_NORMAL_MESSAGE = ""
	"Your blueprint will be switched to saving to a single file. Your "
	"collaboration folder will not be deleted, but it will no longer be "
	"updated.";

void RomSettingsWidget::switchMode() {
	if( QMessageBox::information(
		this,
		"Switch Mode?",
		Blueprint::current()->isCollab() ? SWITCH_TO_NORMAL_MESSAGE : SWITCH_TO_COLLAB_MESSAGE,
		QMessageBox::Ok | QMessageBox::Cancel
	) != QMessageBox::Ok ) return;

	fs::path savePath;
	if( Blueprint::current()->isCollab() ) {
		savePath = FileDialog::getFileSave( "Save As", { { "Blueprint", "*.bbp"} } );
		savePath.replace_extension( ".bbp" );
	} else {
		savePath = FileDialog::getDirectory( "Select Collab Directory" );
	}

	if( savePath.empty() ) return;
	try {
		Blueprint::currentEditable()->switchMode( savePath.u8string() );
	} catch( ... ) {
		QMessageBox::critical( this, "File Error", "Failed to create files. Do you have write permissions?" );
	}
	blueprintChanged();
}

void RomSettingsWidget::romPathChanged() {
	if( !Blueprint::current()->isCollab() ) return;
	//TODO: restrict more characters on Windows
	const QString newName = m_ui->targetRomField->text().replace( "/", "" );
	m_ui->targetRomField->setText( newName );
	Blueprint::currentEditable()->setTargetRom( fs::to_path( newName.toStdString() ) );
}

void RomSettingsWidget::updateAudioHeapCheckbox() {
	const Blueprint *blueprint = Blueprint::current();
	for( const auto &i : blueprint->levels() ) {
		if( i.second.objectBanks[2] >= 0 ) {
			m_ui->expandAudioHeapCheckbox->setEnabled( false );
			m_ui->expandAudioHeapCheckbox->setChecked( false );
			return;
		}
	}

	m_ui->expandAudioHeapCheckbox->setEnabled( true );
	m_ui->expandAudioHeapCheckbox->setChecked( blueprint->romSpecs().expandAudioHeap );
}

void RomSettingsWidget::scaleSliderMoved( int tick ) {
	char label[8];
	std::snprintf( label, 8, "x%.2f", 1.f + (float)tick * 0.25f );
	m_ui->scaleLabel->setText( label );
}
