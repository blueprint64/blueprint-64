#ifndef SRC_UI_OBJECT_EDITOR_HPP_
#define SRC_UI_OBJECT_EDITOR_HPP_

#include <QWidget>
#include "src/ui/import-model-dialog.hpp"
#include "src/core/enums.hpp"
#include "src/core/objectid.hpp"

namespace Ui {
	class ObjectEditor;
}

class ObjectEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::ObjectEditor *m_ui;
	ImportModelDialog m_importModelDialog;
	LevelId m_levelId;
	objectId m_objectId;
	bool m_initializing;

	public:
	explicit ObjectEditor( QWidget *parent = nullptr );
	~ObjectEditor();

	void setObject( LevelId levelId, objectId objectId );

	public slots:
	void dataChanged();
	void importModel();

	private slots:
	void objectModelImported( ModelImportedEvent event );

	signals:
	void openModelEditor();
	void openBehaviourEditor();

};



#endif /* SRC_UI_OBJECT_EDITOR_HPP_ */
