#include "src/ui/preferences.hpp"
#include "ui_preferences.h"

#include <QStyleFactory>
#include "src/polyfill/file-dialog.hpp"
#include "src/polyfill/filesystem.hpp"

PreferencesDialog::PreferencesDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::PreferencesDialog )
{
	m_ui->setupUi( this );

	m_ui->emulatorSelect->addItem( "Parallel Launcher", QVariant::fromValue<uint>( (uint)Emulator::Emulator::ParallelLauncher ) );
	m_ui->emulatorSelect->addItem( "RetroArch", QVariant::fromValue<uint>( (uint)Emulator::Emulator::RetroArch ) );
	m_ui->emulatorSelect->addItem( "Mupen64Plus", QVariant::fromValue<uint>( (uint)Emulator::Emulator::Mupen64Plus ) );
#ifdef _WIN32
	m_ui->emulatorSelect->addItem( "Project 64", QVariant::fromValue<uint>( (uint)Emulator::Emulator::Project64 ) );
#endif
	m_ui->emulatorSelect->addItem( "Other", QVariant::fromValue<uint>( (uint)Emulator::Emulator::Other ) );

#ifndef _WIN32
	m_ui->tabWidget->removeTab( 3 );
	m_ui->tabWidget->removeTab( 0 );
#endif
}

PreferencesDialog::~PreferencesDialog() {
	delete m_ui;
}

void PreferencesDialog::showEvent( QShowEvent *event ) {
	m_ui->themeSelect->clear();
	m_ui->themeSelect->addItem( "System Default" );
	m_ui->themeSelect->addItems( QStyleFactory::keys() );
	m_ui->themeSelect->setCurrentIndex( 0 );

	const AppPreferences &data = AppPreferences::current();
	m_ui->openOnStartCheckbox->setChecked( data.openOnStart );
	m_ui->nativeMenuCheckbox->setChecked( data.nativeMenuBar );
	m_ui->importScaleSpinner->setValue( data.defaultScale );
	m_ui->retroarchTab->load();
	m_ui->mupenTab->load();
#ifdef _WIN32
	m_ui->pj64Tab->load();
#endif
	m_ui->otherTab->load();
	m_ui->emulatorSelect->setCurrentIndex(
		m_ui->emulatorSelect->findData( QVariant::fromValue<uint>( (uint)data.emuConfig.preferredEmulator ), Qt::UserRole )
	);
	m_ui->themeSelect->setCurrentText( data.theme.c_str() );

	QWidget::showEvent( event );
}

void PreferencesDialog::save() {
	AppPreferences &data = AppPreferences::current();
	data.openOnStart = m_ui->openOnStartCheckbox->isChecked();
	data.nativeMenuBar = m_ui->nativeMenuCheckbox->isChecked();
	data.defaultScale = m_ui->importScaleSpinner->value();
	data.theme = m_ui->themeSelect->currentIndex() > 0 ? m_ui->themeSelect->currentText().toStdString() : ""s;
	data.emuConfig.preferredEmulator = (Emulator::Emulator)m_ui->emulatorSelect->currentData( Qt::UserRole ).toUInt();
	m_ui->retroarchTab->save();
	m_ui->mupenTab->save();
#ifdef _WIN32
	m_ui->pj64Tab->save();
#endif
	m_ui->otherTab->save();
	data.save();
	accept();
}

void PreferencesDialog::browse() {
#ifdef _WIN32
	const fs::path path = FileDialog::getFileOpen(
		"Provide Parallel Launcher Path",
		{ { "Parallel Launcher", "parallel-launcher.exe" } }
	);

	if( path.empty() ) return;

	m_ui->parallelPath->setText( path.u8string().c_str() );
	AppPreferences &data = AppPreferences::current();
	data.emuConfig.parallelLauncherPath = path.u8string();
	data.save();
#endif
}

void PreferencesDialog::emulatorSelected( int index ) {
#ifdef _WIN32
	m_ui->tabWidget->setCurrentIndex( index );
#else
	if( index == 0 ) {
		m_ui->tabWidget->setCurrentIndex( 0 );
	} else {
		m_ui->tabWidget->setCurrentIndex( index - 1 );
	}
#endif
}
