#ifndef SRC_UI_MODEL_IMPORT_HELPER_HPP_
#define SRC_UI_MODEL_IMPORT_HELPER_HPP_

#include "src/ui/import-model-dialog.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/core/enums.hpp"
#include "src/core/objectid.hpp"
#include "src/core/util.hpp"
#include "src/core/data/material.hpp"

#include <QWidget>

class IModelImportHelper {

	protected:
	QWidget *m_parent;
	LevelId m_levelId;

	IModelImportHelper( QWidget *parent, LevelId levelId ) :
		m_parent( parent ),
		m_levelId( levelId )
	{}
	virtual ~IModelImportHelper() {}

	virtual HashMap<string,MaterialSpecs> getGeometry() const = 0;
	virtual void saveGeometry( const HashMap<string,MaterialSpecs> &specsMap ) const = 0;
	virtual fs::path getImportDir() const = 0;

	public:
	BoundingBox handleImport( const ModelImportedEvent &importEvent ) const;

};

class AreaModelImportHelper : public IModelImportHelper {

	private:
	ubyte m_areaIndex;

	public:
	AreaModelImportHelper( QWidget *parent, LevelId levelId, ubyte areaIndex ) :
		IModelImportHelper( parent, levelId ),
		m_areaIndex( areaIndex )
	{}
	~AreaModelImportHelper() {}

	virtual HashMap<string,MaterialSpecs> getGeometry() const override;
	virtual void saveGeometry( const HashMap<string,MaterialSpecs> &specsMap ) const override;
	virtual fs::path getImportDir() const override;

};

class ObjectModelImportHelper : public IModelImportHelper {

	private:
	objectId m_objectId;

	public:
	ObjectModelImportHelper( QWidget *parent, LevelId levelId, objectId objectId ) :
		IModelImportHelper( parent, levelId ),
		m_objectId( objectId )
	{}
	~ObjectModelImportHelper() {}

	virtual HashMap<string,MaterialSpecs> getGeometry() const override;
	virtual void saveGeometry( const HashMap<string,MaterialSpecs> &specsMap ) const override;
	virtual fs::path getImportDir() const override;

};


#endif /* SRC_UI_MODEL_IMPORT_HELPER_HPP_ */
