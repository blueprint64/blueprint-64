#include "src/ui/object-placement-dialog.hpp"
#include "ui_object-placement-dialog.h"

#include <QMessageBox>
#include "src/polyfill/filestream.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/actors.hpp"

ObjectPlacementDialog::ObjectPlacementDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ObjectPlacementDialog ),
	m_levelId( LevelId::INVALID ),
	m_areaIndex( 0xFF ),
	m_canSaveArea( false ),
	m_canApplyArea( false ),
	m_canSaveLevel( false ),
	m_canApplyLevel( false )
{
	m_ui->setupUi( this );
}

ObjectPlacementDialog::~ObjectPlacementDialog() {
	delete m_ui;
}

void ObjectPlacementDialog::open( LevelId levelId, ubyte areaIndex ) {
	m_levelId = levelId;
	m_areaIndex = areaIndex;

	const Blueprint *blueprint = Blueprint::current();
	FileReadStream rom( blueprint->targetRomPath().u8string() );
	std::array<AreaActors,8> levelActors = AreaActors::loadFromRom( rom, levelId );

	m_canSaveArea = !levelActors[areaIndex].empty();
	if( m_canSaveArea ) {
		m_canSaveLevel = true;
	} else {
		m_canSaveLevel = false;
		for( const AreaActors &areaActors : levelActors ) {
			if( !areaActors.empty() ) {
				m_canSaveLevel = true;
				break;
			}
		}
	}

	m_canApplyArea = fs::exists( blueprint->getAreaActorsPath( levelId, areaIndex) ) && blueprint->areaHasGeometry( levelId, areaIndex );
	if( m_canApplyArea ) {
		m_canApplyLevel = true;
	} else {
		m_canApplyLevel = false;
		for( ubyte i = 0; i < 8; i++ ) {
			if( fs::exists( blueprint->getAreaActorsPath( levelId, i) ) && blueprint->areaHasGeometry( levelId, i ) ) {
				m_canApplyLevel = true;
				break;
			}
		}
	}

	m_ui->applyAreaRadio->setEnabled( m_canSaveArea || m_canApplyArea );
	m_ui->applyLevelRadio->setEnabled( m_canSaveLevel || m_canApplyLevel );

	if( m_canSaveArea || m_canApplyArea ) {
		m_ui->applyAreaRadio->setChecked( true );
	} else if( m_canSaveLevel || m_canApplyLevel ) {
		m_ui->applyLevelRadio->setChecked( true );
	} else {
		m_ui->applyAllRadio->setChecked( true );
	}

	scopeChanged();
	QDialog::open();
}

void ObjectPlacementDialog::scopeChanged() {
	if( m_ui->applyAreaRadio->isChecked() ) {
		m_ui->savePlacementsButton->setEnabled( m_canSaveArea );
		m_ui->applyPlacementsButton->setEnabled( m_canApplyArea );
	} else if( m_ui->applyLevelRadio->isChecked() ) {
		m_ui->savePlacementsButton->setEnabled( m_canSaveLevel );
		m_ui->applyPlacementsButton->setEnabled( m_canApplyLevel );
	} else {
		m_ui->savePlacementsButton->setEnabled( true );
		m_ui->applyPlacementsButton->setEnabled( true );
	}
}

void ObjectPlacementDialog::saveLevelActorsToBlueprint( std::ifstream &rom, LevelId levelId ) {
	const std::array<AreaActors,8> actors = AreaActors::loadFromRom( rom, levelId );
	for( ubyte i = 0; i < 8; i++ ) {
		const fs::path jsonPath = Blueprint::current()->getAreaActorsPath( levelId, i );
		if( actors[i].empty() ) {
			fs::forceDelete( jsonPath );
		} else {
			actors[i].saveToJson( jsonPath.u8string() );
			m_canApplyLevel = true;
			m_canApplyArea |= (m_areaIndex == i);
		}
	}
}

void ObjectPlacementDialog::saveToBlueprint() {
	FileReadStream rom( Blueprint::currentEditable()->targetRomPath().u8string() );
	if( m_ui->applyAllRadio->isChecked() ) {
		for( LevelId levelId : Enum::values<LevelId>() ) {
			if( levelId == LevelId::EndScreen ) continue;
			saveLevelActorsToBlueprint( rom, levelId );
		}
	} else if( m_ui->applyLevelRadio->isChecked() ) {
		saveLevelActorsToBlueprint( rom, m_levelId );
		m_ui->applyPlacementsButton->setEnabled( true );
	} else {
		const AreaActors actors = AreaActors::loadFromRom( rom, m_levelId )[m_areaIndex];
		actors.saveToJson( Blueprint::current()->getAreaActorsPath( m_levelId, m_areaIndex ).u8string() );
		m_ui->applyPlacementsButton->setEnabled( true );
		m_canApplyArea = true;
		m_canApplyLevel = true;
	}
	QMessageBox::information( this, "Object Placements Saved", "Object placements saved to blueprint.", QMessageBox::Ok );
}

void ObjectPlacementDialog::applyToRom() {
	try {
		const Blueprint *blueprint = Blueprint::current();
		FileStream rom( blueprint->targetRomPath().u8string() );
		if( m_ui->applyAllRadio->isChecked() ) {
			blueprint->applyAll( rom, true );
		} else {
			std::array<AreaActors,8> actors = AreaActors::loadFromRom( rom, m_levelId );
			if( m_ui->applyLevelRadio->isChecked() ) {
				for( ubyte i = 0; i < 8; i++ ) {
					const fs::path actorsPath = blueprint->getAreaActorsPath( m_levelId, i );
					if( !fs::exists( actorsPath ) ) continue;
					AreaActors areaActors = AreaActors::loadFromJson( actorsPath.u8string() );
					if( !areaActors.empty() ) {
						actors[i] = areaActors;
					}
				}
			} else {
				AreaActors areaActors = AreaActors::loadFromJson( blueprint->getAreaActorsPath( m_levelId, m_areaIndex ).u8string() );
				if( !areaActors.empty() ) {
					actors[m_areaIndex] = areaActors;
				}
			}
			blueprint->applyLevelChanges( rom, m_levelId, actors );
		}
		QMessageBox::information( this, "Changes Applied", "ROM built successfully." );
	} catch( const std::exception &ex ) {
		const string msg = string("Failed to build ROM:\n") + ex.what();
		QMessageBox::critical( this, "Build Failed", msg.c_str() );
	}
}
