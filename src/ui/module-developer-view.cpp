#include "src/ui/module-developer-view.hpp"
#include "ui_module-developer-view.h"

#include <QMessageBox>
#include <fstream>
#include "src/core/blueprint.hpp"
#include "src/core/asm-modules.hpp"
#include "src/core/package.hpp"
#include "src/core/util.hpp"
#include "src/core/blueprint-module-manager.hpp"
#include "src/core/backup.hpp"
#include "src/core/exceptions.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/util.hpp"

ModuleDevelopView::ModuleDevelopView( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::ModuleDevelopView )
{
	m_ui->setupUi( this );
}

ModuleDevelopView::~ModuleDevelopView() {
	delete m_ui;
}

void ModuleDevelopView::showEvent( QShowEvent *event ) {
	m_ui->modulePathField->clear();
	m_ui->armipsOutput->clear();
	m_ui->rebuildButton->setEnabled( false );
	m_ui->moduleInfo->setVisible( false );
	QWidget::showEvent( event );
}

static bool tryLoadModule( QWidget *widget, const string &modulePath, const AsmModule **moduleOut = nullptr ) {
	const AsmModuleDefinition *moduleDef;
	try {
		moduleDef = &AsmModuleStore::installModule( fs::to_path( modulePath ) );
	} catch( const std::exception &ex ) {
		QMessageBox::critical( widget, "Module Definition Error", ("Failed to load ASM module:\n"s + ex.what()).c_str(), QMessageBox::Ok );
		return false;
	}

	HashMap<string,AsmModulePropertyValue> oldArgs;
	std::array<ubyte,40> oldScope;
	if( const AsmModule *activeModule = Util::tryGetValue( Blueprint::current()->asmModules(), moduleDef->id ) ) {
		oldArgs = activeModule->parameters;
		oldScope = activeModule->areaMask;
	} else {
		oldScope.fill( 0 );
	}

	string conflict;
	if( BlueprintModules::tryAddModule( *moduleDef, conflict ) ) {
		AsmModule &module = Blueprint::currentEditable()->asmModules().at( moduleDef->id );
		module.areaMask = oldScope;
		module.parameters.clear();
		for( const AsmModuleProperty &prop : module.definition.properties ) {
			if( const AsmModulePropertyValue *value = Util::tryGetValue( oldArgs, prop.name ) ) {
				switch( prop.type ) {
					case ModulePropertyType::Integer: {
						if( !std::holds_alternative<int>( *value ) ) continue;
						const int intVal = std::get<int>( *value );
						if( intVal >= prop.minInt && intVal <= prop.maxInt ) {
							module.parameters[prop.name] = *value;
						}
						break;
					}
					case ModulePropertyType::Float:
					case ModulePropertyType::Angle: {
						if( !std::holds_alternative<float>( *value ) ) continue;
						const float floatVal = std::get<float>( *value );
						if( floatVal >= prop.minFloat && floatVal <= prop.maxFloat ) {
							module.parameters[prop.name] = *value;
						}
						break;
					}
					case ModulePropertyType::Boolean: {
						if( std::holds_alternative<bool>( *value ) ) {
							module.parameters[prop.name] = *value;
						}
						break;
					}
					case ModulePropertyType::String: {
						if( !std::holds_alternative<string>( *value ) ) continue;
						const string &stringVal = std::get<string>( *value );
						if( stringVal.length() > prop.maxLength ) continue;
						if( !prop.multiLine && stringVal.find( "\n" ) != string::npos ) continue;
						module.parameters[prop.name] = *value;
						break;
					}
					case ModulePropertyType::Colour: {
						if( !std::holds_alternative<ColourRGBA32>( *value ) ) continue;
						if( !prop.hasAlpha ) {
							ColourRGBA32 colour = std::get<ColourRGBA32>( *value );
							colour.alpha = 0xFF;
							module.parameters[prop.name] = colour;
						} else {
							module.parameters[prop.name] = *value;
						}
						break;
					}
					default: continue;
				}
			}
		}

		if( moduleOut != nullptr ) {
			*moduleOut = &module;
		}
		return true;
	}

	QMessageBox::critical( widget, "Failed to Add Module", ("ASM Module could not be added to blueprint:\n\n"s + conflict).c_str(), QMessageBox::Ok );
	return false;
}

void ModuleDevelopView::rebuild( bool everything ) {
	QString modulePath = m_ui->modulePathField->text();
	m_ui->armipsOutput->clear();

	if( modulePath.isNull() || modulePath.isEmpty() ) {
		QMessageBox::information( this, "No Module Selected", "You must select a module to build", QMessageBox::Ok );
		return;
	}

	const AsmModule *module;
	if( tryLoadModule( this, modulePath.toStdString(), &module ) ) {
		m_ui->moduleInfo->setVisible( true );
		m_ui->moduleInfo->setModule( module->definition.id, module->definition.version );
	} else {
		m_ui->moduleInfo->setVisible( false );
		return;
	}

	const fs::path romPath = Blueprint::current()->targetRomPath();
	if( romPath.empty() ) {
		QMessageBox::information( this, "No Target ROM", "Could not build because no target ROM is set.", QMessageBox::Ok );
		return;
	}

	FileBackup backup( romPath );
	FileStream rom( romPath.u8string() );
	try {
		if( everything ) {
			Blueprint::current()->applyAll( rom );
		} else {
			Blueprint::current()->applyAsmModules( rom );
		}
	} catch( const ArmipsException &ex ) {
		m_ui->armipsOutput->setPlainText( ex.what() );
		rom.close();
		return;
	} catch( const std::exception &ex ) {
		QMessageBox::critical( this, "Build Error", ("Failed to build ROM:\n"s + ex.what()).c_str(), QMessageBox::Ok );
		rom.close();
		return;
	}

	backup.discard();
	m_ui->armipsOutput->setPlainText( "Build successful." );
	emit launchEmulator();
}

void ModuleDevelopView::rebuildAll() {
	rebuild( true );
}

void ModuleDevelopView::rebuildModule() {
	rebuild( false );
}

void ModuleDevelopView::browse() {
	fs::path modulePath = FileDialog::getFileOpen(
		"Select a Module", {
		{ "All Modules", "*.bbpmod;*.tar.gz;module.json" },
		{ "Extracted Modules", "module.json" },
		{ "Packaged Modules", "*.bbpmod;*.tar.gz" }
	});

	if( modulePath.empty() ) return;

	if( modulePath.stem().u8string() != "module.json" ) {
		const fs::path extractTo = FileDialog::getDirectory( "Extract Module To..." );
		if( extractTo.empty() ) return;
		Package::extract( modulePath, extractTo );
		//TODO: handle errors

		modulePath = extractTo / "module.json";
	}

	m_ui->armipsOutput->clear();
	m_ui->modulePathField->setText( modulePath.u8string().c_str() );
	m_ui->rebuildButton->setEnabled( true );
	m_ui->packageButton->setEnabled( true );

	const AsmModule *module;
	if( tryLoadModule( this, modulePath.u8string(), &module ) ) {
		m_ui->moduleInfo->setVisible( true );
		m_ui->moduleInfo->setModule( module->definition.id, module->definition.version );
	} else {
		m_ui->moduleInfo->setVisible( false );
	}
}

void ModuleDevelopView::package() {
	const fs::path modulePath = fs::to_path( m_ui->modulePathField->text().toStdString() ).parent_path();
	fs::path saveLocation = FileDialog::getFileSave( "Save As", { { "Packaged Modules", "*.bbpmod" } } );
	if( saveLocation.empty() ) return;

	saveLocation.replace_extension( ".bbpmod" );
	Package::create( saveLocation, modulePath );
}
