#ifndef SRC_UI_USER_DATA_LINK_WIDGET_HPP_
#define SRC_UI_USER_DATA_LINK_WIDGET_HPP_

#include <QWidget>
#include "src/core/enums.hpp"
#include "src/core/data/user-data.hpp"

struct PotentialLink {
	string name;
	LinkType type;
	uint targetId;
};

namespace Ui {
	class UserDataLinkWidget;
}

class UserDataLinkWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::UserDataLinkWidget *m_ui;
	LevelId m_levelId;
	const std::vector<PotentialLink> *m_availableLinks;
	UserDataLink *m_link;
	bool m_initializing;

	void refreshOffset();

	public:
	UserDataLinkWidget(
		QWidget *parent,
		LevelId levelId,
		uint linkOffset,
		const std::vector<PotentialLink> *availableLinks,
		UserDataLink *link
	);
	virtual ~UserDataLinkWidget();

	public slots:
	void linkTargetChanged();
	void targetOffsetChanged();

};

#endif /* SRC_UI_USER_DATA_LINK_WIDGET_HPP_ */
