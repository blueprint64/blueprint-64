#ifndef SRC_UI_SOUND_SELECT_DIALOG_HPP_
#define SRC_UI_SOUND_SELECT_DIALOG_HPP_

#include <QDialog>
#include <map>

namespace Ui {
	class SoundSelectDialog;
}

class SoundSelectDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::SoundSelectDialog *m_ui;
	std::map<short,int> m_indexLookup;
	uint m_sound;
	bool m_inEventHandler;

	void updateSelectBox();
	void setSoundUsingComboBox();
	void setSoundUsingManualInput();

	public slots:
	void soundSelected();
	void soundIdChanged();
	void formatSoundId();

	public:
	explicit SoundSelectDialog( QWidget *parent = nullptr );
	~SoundSelectDialog();

	inline uint sound() const { return m_sound; }
	void setSound( uint sound );

};

#endif /* SRC_UI_SOUND_SELECT_DIALOG_HPP_ */
