#ifndef SRC_UI_WATER_EDITOR_HPP_
#define SRC_UI_WATER_EDITOR_HPP_

#include <QWidget>
#include <QButtonGroup>

#include "src/core/enums.hpp"
#include "src/ui/widgets/dynamic-grid-widget.hpp"
#include "src/ui/water-texture-import-dialog.hpp"
#include "src/ui/animation-dialog.hpp"

Q_DECLARE_METATYPE(WaterTextureInfo)

namespace Ui {
	class WaterEditor;
}

class WaterEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::WaterEditor *m_ui;
	AnimationDialog m_animationDialog;
	WaterTextureImportDialog m_textureDialog;
	QButtonGroup m_textureGroup;
	LevelId m_levelId;
	ubyte m_areaIndex;
	bool m_initializing;

	void syncEnabled();
	void addTextureButton(
		DynamicGridWidget *section,
		const WaterTextureInfo &texture,
		bool select
	);

	public:
	explicit WaterEditor( QWidget *parent = nullptr );
	~WaterEditor();

	void setArea( LevelId levelId, ubyte areaIndex );

	public slots:
	void dataChanged();
	void editAnimation();
	void regionSelected();
	void addRegion();
	void removeRegion();
	void addTexture();
	void removeTexture();
	void editTexture();
	void blueprintChanged();
	void regionRenamed();

	private slots:
	void buttonDestroyed( QObject *button );
	void animationChanged();
	void textureImported( WaterTextureInfo texture );
	void textureChanged( WaterTextureInfo previous, WaterTextureInfo updated );

};


#endif /* SRC_UI_WATER_EDITOR_HPP_ */
