#ifndef UI_PREFERENCES_HPP_
#define UI_PREFERENCES_HPP_

#include <QDialog>
#include <string>

#include "src/core/storage.hpp"

namespace Ui {
	class PreferencesDialog;
}

class PreferencesDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::PreferencesDialog *m_ui;

	public slots:
	void save();
	void browse();
	void emulatorSelected( int index );

	public:
	explicit PreferencesDialog( QWidget *parent = nullptr );
	~PreferencesDialog();

	void showEvent( QShowEvent *event ) override;

};


#endif /* UI_PREFERENCES_HPP_ */
