#include "src/ui/base-rom.hpp"

#include <QMessageBox>
#include <QImage>
#include <cstdlib>
#include <array>
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/core/baserom.hpp"
#include "src/core/checksum.hpp"
#include "src/core/text.hpp"
#include "src/ui/util.hpp"

using namespace std;

static const char *needBaseRomMessage = ""
	"<p>Welcome to Bowser's Blueprints! Before you get started, you will need to "
	"provide an extended ROM. You will only need to do this once.\n\n"
	"Please provide a ROM that has been extended using <a href=\""
	"http://pabloscorner.akawah.net/sm64editorarchive\">SM64 Editor 2.0.8</a>. "
	"To extend a ROM using SM64 Editor, simply load an unmodified ROM of the "
	"US version of Super Mario 64 in SM64 Editor, then accept the patches it "
	"asks you to supply, but make no further changes after this.</p>";

static const char *invalidBaseRomMessage = ""
	"The provided ROM did not match the checksum. Verify that:\n"
	" * You used the US version of Super Mario 64\n"
	" * The ROM file uses the big endian byte order (.z64 extension)\n"
	" * The ROM was extended using version 2.0.8 of SM64 Editor\n"
	" * You clicked Okay when asked to apply patches upon loading the ROM\n"
	" * You made no further changes to the ROM after loading it";

static const md5sum expectedChecksum = { 0xd0, 0x12, 0x3c, 0xbb, 0x52, 0x4a, 0x73, 0x58, 0xd9, 0x9d, 0x97, 0xa0, 0xce, 0x94, 0xd7, 0x42 };

static bool validBaseRom( const fs::path &romPath ) {
	try {
		if( !fs::exists( romPath ) ) return false;
		FileReadStream rom( romPath.u8string() );
		return getChecksumMD5( rom ) == expectedChecksum;
	} catch( ... ) {
		return false;
	}
}

static void cacheBaseRom() {
	const fs::path &cachedRomPath = BaseRom::filePath();
	if( validBaseRom( cachedRomPath ) ) return;
	if( QMessageBox::information( nullptr, "First Launch Setup", needBaseRomMessage, QMessageBox::Open | QMessageBox::Abort ) != QMessageBox::Open ) {
		exit( 0 );
	}

	while( true ) {
		const fs::path romPath = FileDialog::getFileOpen(
			"Select ROM",
			{ { "N64 ROMs", "*.z64" }, { "All Files", "*.*;*" } }
		);

		if( romPath.empty() ) exit( 0 );
		if( validBaseRom( romPath ) ) {
			if ( !fs::tryCopyFileOverwrite( romPath, cachedRomPath ) ) {
				QMessageBox::critical( nullptr, "Fatal Error", "Failed to copy ROM to cache", QMessageBox::Ok );
				exit( 1 );
			}
			return;
		}

		if( QMessageBox::critical( nullptr, "Invalid ROM", invalidBaseRomMessage, QMessageBox::Retry | QMessageBox::Abort ) != QMessageBox::Retry ) {
			exit( 0 );
		}
	}
}

static void extractVanillaWaterTextures() {
	FileReadStream rom( BaseRom::filePath().u8string() );
	for( const auto &i : BaseRom::waterTextures() ) {
		const fs::path imgPath = AppData::cacheDir() / (i.first + ".png");
		if( fs::exists( imgPath ) ) continue;
		QImage texture( 32, 32, QImage::Format_ARGB32 );

		std::array<ushort,1024> encodedPixels;
		rom.seekg( i.second.segmentedPointer - 0x17FCEAA );
		rom.read( (char*)encodedPixels.data(), 2048 );

		uint *pixels = (uint*)texture.bits();
		if( i.second.textureFormat == TextureFormat::IA16 ) {
			for( int j = 0; j < 1024; j++ ) {
				uint value = ((const ubyte*)&encodedPixels[j])[0];
				uint alpha = ((const ubyte*)&encodedPixels[j])[1];
				pixels[j] = (
					(alpha << 24) |
					(value << 16) |
					(value << 8) |
					value
				);
			}
		} else {
			assert( i.second.textureFormat == TextureFormat::RGBA16 );
			for( int j = 0; j < 1024; j++ ) {
				const ColourRGBA5551 colour( ntohs( encodedPixels[j] ) );
				const uint rgba = ((ColourRGBA32)colour).toInt();
				pixels[j] = ((rgba & 0xFF) << 24) | (rgba >> 8);
			}
		}

		texture.save( imgPath.u8string().c_str(), "PNG" );
	}
}

void BaseRom::init() {
	cacheBaseRom();
	VanillaText::init();
	extractVanillaWaterTextures();
}
