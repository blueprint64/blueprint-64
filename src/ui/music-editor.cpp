#include "src/ui/music-editor.hpp"
#include "ui_music-editor.h"

#include <cstdio>
#include <cassert>
#include <QMessageBox>
#include <QFormLayout>
#include <QComboBox>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/music.hpp"
#include "src/ui/util.hpp"

static const char *s_volumeDisabledTooltip = "Volume cannot be changed because the sequence file does not set the master volume exactly once.";
static const char *s_volumeEnabledTooltip = "Set the master volume. Vanilla music volumes range between 65 and 80.";
static const char *s_muteDisabledTooltip = "Mute behaviour cannot be changed because the sequence file does not set it exactly once.";
static const char *s_muteEnabledTooltip = "Set how the sequence actually behaves when instructed to mute/pause (eg. when pausing the game)";


static const char *const s_bankNames[38] = {
	"00 - SFX Misc 1",
	"01 - SFX Footsteps",
	"02 - SFX Water",
	"03 - SFX Sand",
	"04 - SFX Misc 2",
	"05 - SFX Misc 3",
	"06 - SFX Misc 4",
	"07 - SFX Misc 5",
	"08 - SFX Mario",
	"09 - SFX UI",
	"10 - SFX Voice",
	"11 - Snow Levels",
	"12 - Unused",
	"13 - TTC/RR/Slides",
	"14 - Inside Castle",
	"15 - SLL/LLL",
	"16 - Spooky (HMC)",
	"17 - Title Screen",
	"18 - Bowser Fight",
	"19 - Water Levels",
	"20 - Lullaby",
	"21 - HMC/WDW",
	"22 - Star Select",
	"23 - Wing/Vanish",
	"24 - Metal Cap",
	"25 - Bowser Course",
	"26 - Fanfares",
	"27 - Stage Boss",
	"28 - Infinite Stairs",
	"29 - Final Battle",
	"30 - Peach",
	"31 - Course Clear",
	"32 - Toad",
	"33 - Merry-Go-Round",
	"34 - Main Theme",
	"35 - Ending",
	"36 - File Select",
	"37 - Credits"
};

MusicEditor::MusicEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::MusicEditor ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	m_ui->infoIcon->setPixmap( QIcon::fromTheme( "emblem-information" ).pixmap( 16, 16 ) );
	m_initializing = false;
}

MusicEditor::~MusicEditor() {
	delete m_ui;
}

void MusicEditor::reload() {
	m_initializing = true;
	m_ui->musicList->clear();

	m_music = MusicData::getMusicInfo();
	for( uint i = 1; i < m_music.size(); i++ ) {
		const string displayName = std::to_string( i ) + " - " + m_music[i].name;
		m_ui->musicList->addItem( displayName.c_str() );
	}

	m_ui->musicList->setCurrentRow( 0 );
	m_ui->addMusicButton->setEnabled( m_music.size() < 255 );

	m_initializing = false;
	trackSelected();
}

void MusicEditor::trackSelected() {
	if( m_initializing || m_music.size() < 2 || m_ui->musicList->currentRow() < 0 ) return;

	m_initializing = true;
	const MusicInfo &music = m_music.at( m_ui->musicList->currentRow() + 1 );
	m_ui->musicInfoPane->setEnabled( !music.isPlaceholder && !music.isVanilla );
	m_ui->trackName->setText( music.name.c_str() );
	if( music.isPlaceholder ) {
		m_ui->trackSize->setText( "n/a" );
	} else if( music.numBytes < 1024 ) {
		char sizeStr[11];
		std::sprintf( sizeStr, "%d bytes", music.numBytes );
		m_ui->trackSize->setText( sizeStr );
	} else if( music.numBytes < 1024 * 1024 ) {
		char sizeStr[10];
		std::sprintf( sizeStr, "%.1f kB", (double)music.numBytes / 1024.0 );
		m_ui->trackSize->setText( sizeStr );
	} else {
		char sizeStr[8];
		std::sprintf( sizeStr, "%.2f MB", (double)music.numBytes / (1024.0 * 1024.0) );
		m_ui->trackSize->setText( sizeStr );
	}
	m_ui->banksUsed->setText( QString::number( (int)music.soundBanks.size() ) );
	m_ui->volumeGroup->setEnabled( music.volume >= 0 );
	m_ui->volumeGroup->setToolTip( music.volume >= 0 ? s_volumeEnabledTooltip : s_volumeDisabledTooltip );
	if( music.volume >= 0 ) {
		m_ui->volumeSlider->setValue( music.volume );
		setVolumeLabel( music.volume );
	} else {
		m_ui->volumeSlider->setValue( 80 );
		m_ui->volumeLabel->setText( "/" );
	}
	m_ui->muteGroup->setEnabled( true );
	m_ui->muteGroup->setToolTip( s_muteEnabledTooltip );
	switch( music.muteBehaviour ) {
		case MuteBehaviour::Ignore:
			m_ui->muteIgnoreRadio->setChecked( true ); break;
		case MuteBehaviour::Quiet:
			m_ui->muteQuietRadio->setChecked( true ); break;
		case MuteBehaviour::Pause:
			m_ui->mutePauseRadio->setChecked( true ); break;
		case MuteBehaviour::Mute:
			m_ui->muteMuteRadio->setChecked( true ); break;
		default: {
			QAbstractButton *selectedRadio = m_ui->muteRadioGroup->checkedButton();
			if( selectedRadio != nullptr ) {
				selectedRadio->setChecked( false );
			}
			m_ui->muteGroup->setEnabled( false );
			m_ui->muteGroup->setToolTip( s_muteDisabledTooltip );
			break;
		}

	}

	UiUtil::clearLayout( m_ui->banksGroup->layout() );
	int bankIndex = 0;
	for( ubyte bankId : music.soundBanks ) {
		QFormLayout *bankList = dynamic_cast<QFormLayout*>( m_ui->banksGroup->layout() );
		assert( bankList != nullptr );

		const QString label = QString( "Bank " ) + QString::number( bankIndex ) + QString( ": " );
		QComboBox *selectBox = new QComboBox( m_ui->banksGroup );
		for( int i = 0; i < 38; i++ ) {
			selectBox->addItem( QString( s_bankNames[i] ) );
		}
		selectBox->setCurrentIndex( (int)bankId );
		selectBox->setSizePolicy( QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Fixed );

		const int bi = bankIndex;
		connect( selectBox, QOverload<int>::of( &QComboBox::currentIndexChanged ), this, [=](int newBank){
			Blueprint::currentEditable()->customMusic().at( m_ui->musicList->currentRow() + 1 ).soundBanks.at( bi ) = newBank;
		});

		bankList->addRow( label, selectBox );
		bankIndex++;
	}

	m_ui->expandAudioHeapInfo->setVisible( music.numBytes > 12 * 1024 && !Blueprint::current()->romSpecs().expandAudioHeap );

	m_ui->deleteOrRevertButton->setText( m_ui->musicList->currentRow() < 35 ? "Revert" : "Delete" );
	m_ui->deleteOrRevertButton->setEnabled( !music.isPlaceholder );
	m_initializing = false;
}

void MusicEditor::syncName( ubyte seqId ) {
	const string displayName = std::to_string( (uint)seqId ) + " - " + m_music[seqId].name;
	m_ui->musicList->item( seqId - 1 )->setText( displayName.c_str() );
}

void MusicEditor::dataChanged() {
	if( m_initializing || m_ui->musicList->selectedItems().empty() ) return;
	const ubyte seqId = (ubyte)m_ui->musicList->currentRow() + 1;

	CustomMusicInfo &musicInfo = Blueprint::currentEditable()->customMusic().at( seqId );

	if( musicInfo.masterVolumeLocation != 0 ) {
		musicInfo.masterVolume = (sbyte)m_ui->volumeSlider->value();
		m_music.at( seqId ).volume = musicInfo.masterVolume;
	}

	if( musicInfo.muteBehaviourLocation != 0 ) {
		if( m_ui->muteIgnoreRadio->isChecked() ) {
			musicInfo.muteBehaviour = MuteBehaviour::Ignore;
		} else if( m_ui->muteQuietRadio->isChecked() ) {
			musicInfo.muteBehaviour = MuteBehaviour::Quiet;
		} else if( m_ui->mutePauseRadio->isChecked() ) {
			musicInfo.muteBehaviour = MuteBehaviour::Pause;
		} else if( m_ui->muteMuteRadio->isChecked() ) {
			musicInfo.muteBehaviour = MuteBehaviour::Mute;
		}
		m_music.at( seqId ).muteBehaviour = musicInfo.muteBehaviour;
	}
}

void MusicEditor::nameChanged() {
	if( m_initializing ) return;
	const ubyte seqId = m_ui->musicList->currentRow() + 1;
	const string name = m_ui->trackName->text().toStdString();
	m_music.at( seqId ).name = name;
	Blueprint::currentEditable()->customMusic().at( seqId ).name = name;
	syncName( seqId );
}

void MusicEditor::setVolumeLabel( int volume ) {
	m_ui->volumeLabel->setText( QString::number( volume ) );
}


bool MusicEditor::replaceMusicInternal( ubyte seqId ) {
	fs::path filePath = FileDialog::getFileOpen(
		"Open Sequence File",
		{ { "N64 Sequence", "*.m64" }, { "All Files", "*.*;*" } }
	);

	if( filePath.empty() ) return false;

	MusicSequenceInfo seqInfo;
	if( !MusicData::tryGetSequenceInfo( filePath.u8string(), seqInfo ) ) {
		return false;
	}

	if( seqInfo.fileSize >= 2 * 1024 * 1024 ) {
		QMessageBox::critical( this, "File too big!", "File must be smaller than 2MB", QMessageBox::Ok );
		return false;
	}

	const string musicName = filePath.stem().u8string();
	fs::create_directories( Blueprint::current()->getMusicPath( 0 ).parent_path() );
	fs::tryCopyFileOverwrite( filePath, Blueprint::current()->getMusicPath( seqId ) );

	std::vector<ubyte> soundBanks;
	soundBanks.reserve( seqInfo.numSoundBanks );
	soundBanks.push_back( seqInfo.soundBank0 );
	for( int i = 1; i < seqInfo.numSoundBanks; i++ ) {
		soundBanks.push_back( 37 );
	}

	Blueprint::currentEditable()->customMusic()[seqId] = CustomMusicInfo{
		musicName,
		soundBanks,
		seqInfo.muteBehaviour,
		seqInfo.masterVolume,
		seqInfo.muteBehaviourLocation,
		seqInfo.masterVolumeLocation
	};

	m_music[seqId] = {
		musicName,
		std::move( soundBanks ),
		(uint)seqInfo.fileSize,
		false,
		false,
		seqInfo.muteBehaviour,
		seqInfo.masterVolume
	};

	return true;
}

void MusicEditor::addMusic() {
	for( ubyte i = 1; i < m_music.size(); i++ ) {
		if( m_music[i].isPlaceholder ) {
			replaceMusicInternal( i );
			syncName( i );
			return;
		}
	}

	m_music.push_back( MusicInfo{ "", {}, 0, false, true, MuteBehaviour::Undefined, -1 });
	if( replaceMusicInternal( (ubyte)(m_music.size() - 1) ) ) {
		m_ui->musicList->addItem( m_music[m_music.size()-1].name.c_str() );
		trackSelected();
	} else {
		m_music.pop_back();
	}
}

void MusicEditor::replaceMusic() {
	const ubyte seqId = m_ui->musicList->currentRow() + 1;
	replaceMusicInternal( seqId );
	syncName( seqId );
	trackSelected();
}

void MusicEditor::revertMusic() {
	const ubyte seqId = (ubyte)m_ui->musicList->currentRow() + 1;
	string confirmMessage;
	if( m_music[seqId].isVanilla ) {
		confirmMessage = "Are you sure to want to revert '" + m_music[seqId].name + "' to vanilla?";
	} else {
		confirmMessage = "Are you sure to want to delete '" + m_music[seqId].name + "'?";
	}

	if( QMessageBox::question( this, "Confirm", confirmMessage.c_str() ) != QMessageBox::Yes ) {
		return;
	}

	Blueprint::currentEditable()->customMusic().erase( seqId );
	reload();
}
