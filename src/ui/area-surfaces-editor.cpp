#include "src/ui/area-surfaces-editor.hpp"
#include "ui_surfaces-editor.h"

#include "src/core/blueprint.hpp"

AreaSurfacesEditor::AreaSurfacesEditor( QWidget *parent ) :
	SurfacesEditor( parent ),
	m_areaIndex( 0 )
{
	m_ui->objectOptions->setVisible( false );
}

void AreaSurfacesEditor::setArea( LevelId levelId, ubyte areaIndex ) {
	m_initializing = true;

	m_levelId = levelId;
	m_areaIndex = areaIndex;

	m_ui->shadingSelect->setLevel( levelId );

	const Blueprint *blueprint = Blueprint::current();
	assert( blueprint->hasLevel( levelId ) );
	assert( blueprint->hasArea( levelId, areaIndex ) );

	m_ui->shadingSelect->setLevel( levelId );

	m_materialMap = std::move( blueprint->getAreaGeometry( levelId, areaIndex) );
	m_textureMap.clear();

	onMaterialsChanged();

	m_initializing = false;
	materialSelected();
}

void AreaSurfacesEditor::saveData() const {
	Blueprint::currentEditable()->saveAreaGeometry( m_levelId, m_areaIndex, m_materialMap );
}
