#include "src/ui/object-surfaces-editor.hpp"
#include "ui_surfaces-editor.h"

#include "src/core/blueprint.hpp"

ObjectSurfacesEditor::ObjectSurfacesEditor( QWidget *parent ) :
	SurfacesEditor( parent ),
	m_objectId( 0 )
{}

void ObjectSurfacesEditor::setObject( LevelId levelId, objectId objectId ) {
	m_initializing = true;

	m_levelId = levelId;
	m_objectId = objectId;

	m_ui->shadingSelect->setLevel( levelId );

	const Blueprint *blueprint = Blueprint::current();
	assert( blueprint->hasLevel( levelId ) );

	m_ui->shadingSelect->setLevel( levelId );

	m_materialMap = std::move( blueprint->getObjectGeometry( levelId, objectId ) );
	m_textureMap.clear();

	onMaterialsChanged();
	if( ObjectId::getType( objectId ) == ObjectType::Simple ) {
		m_ui->nameField->setText( Blueprint::current()->levels().at( levelId ).simpleObjects.at( objectId ).name.c_str() );
		m_ui->objectAlphaCheckbox->setVisible( false );
	} else {
		const PartialObjectSpecs &object = Blueprint::current()->levels().at( levelId ).partialObjects.at( objectId );
		m_ui->objectAlphaCheckbox->setVisible( true );
		m_ui->nameField->setText( object.name.c_str() );
		m_ui->objectAlphaCheckbox->setChecked( object.useObjectAlpha );
	}

	m_initializing = false;
	materialSelected();
}

void ObjectSurfacesEditor::saveData() const {
	Blueprint::currentEditable()->saveObjectGeometry( m_levelId, m_objectId, m_materialMap );
}

void ObjectSurfacesEditor::objectDataChanged() {
	if( ObjectId::getType( m_objectId ) == ObjectType::Simple ) {
		Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).name = m_ui->nameField->text().toStdString();
	} else {
		PartialObjectSpecs &object = Blueprint::currentEditable()->levels().at( m_levelId ).partialObjects.at( m_objectId );
		object.name = m_ui->nameField->text().toStdString();
		object.useObjectAlpha = m_ui->objectAlphaCheckbox->isChecked();
	}
}
