#include "src/ui/module-area-scope-dialog.hpp"
#include "ui_module-area-scope-dialog.h"

#include <QAbstractTableModel>
#include "src/core/blueprint.hpp"

static const ubyte s_vanillaNumAreas[] = {
	1, // CastleGrounds
	3, // CastleInterior
	1, // BobOmbBattlefield
	1, // WhompsFortress
	1, // CastleCourtyard
	2, // JollyRodgerBay
	2, // CoolCoolMountain
	1, // BigBoosHaunt
	1, // HazyMazeCave
	2, // LethalLavaLand
	3, // ShiftingSandLand
	2, // DireDireDocks
	2, // SnowmansLands
	2, // WetDryWorld
	4, // TallTallMountain
	3, // TinyHugeIsland
	1, // TickTockClock
	1, // RainbowRide
	1, // BowserInTheDarkWorld
	1, // BowserInTheFireSea
	1, // BowserInTheSky
	1, // Bowser1
	1, // Bowser2
	1, // Bowser3
	1, // TowerOfTheWingCap
	1, // CavernOfTheMetalCap
	1, // VanishCapUnderTheMoat
	1, // PeachsSecretSlide
	1, // SecretAquarium
	1, // WingedMarioOverTheRainbow
};

class AreaScopeModel final : public QAbstractTableModel {

	private:
	const Uuid m_moduleId;
	std::vector<LevelId> m_levels;

	public:
	AreaScopeModel( const Uuid &moduleId, bool showVanilla ) :
		m_moduleId( moduleId )
	{
		setVanillaShown( showVanilla );
	}

	~AreaScopeModel() {}

	inline void setVanillaShown( bool showVanilla ) {
		beginResetModel();
		if( showVanilla ) {
			m_levels = Enum::values<LevelId>();
		} else {
			m_levels.clear();
			for( const auto &i : Blueprint::current()->levels() ) {
				m_levels.push_back( i.first );
			}
		}
		endResetModel();
	}

	virtual int columnCount( [[maybe_unused]] const QModelIndex &parent) const override {
		return 10;
	}

	virtual int rowCount( [[maybe_unused]] const QModelIndex &parent ) const override {
		return (int)m_levels.size();
	}

	virtual QVariant headerData( int column, Qt::Orientation orientation, int role ) const override {
		if( role != Qt::DisplayRole || orientation != Qt::Horizontal ) {
			return QVariant();
		}

		static const char* s_headers[10] = {
			"Level",
			"All Areas",
			"0", "1", "2", "3", "4", "5", "6", "7"
		};

		return QString( s_headers[column] );
	}

	virtual QVariant data( const QModelIndex &cell, int role ) const override {
		switch( role ) {
			case Qt::CheckStateRole: {
				if( cell.column() <= 0 ) {
					return QVariant();
				}

				const ubyte areaMask = Blueprint::current()->asmModules().at( m_moduleId ).areaMask.at( (int)m_levels.at( cell.row() ) );
				if( cell.column() == 1 ) {
					switch( areaMask ) {
						case 0x00: return Qt::Unchecked;
						case 0xFF: return Qt::Checked;
						default: return Qt::PartiallyChecked;
					}
				}

				return (areaMask & (1 << (cell.column() - 2)) ) ? Qt::Checked : Qt::Unchecked;
			}
			case Qt::DisplayRole: {
				if( cell.column() != 0 ) {
					return QVariant();
				}

				const auto i = Blueprint::current()->levels().find( m_levels.at( cell.row() ) );
				if( i == Blueprint::current()->levels().end() ) {
					return QString( Enum::toString<LevelId>( m_levels[cell.row()] ) );
				}

				return QString( i->second.name.c_str() );
			}
			default: return QVariant();
		}
	}

	virtual Qt::ItemFlags flags( const QModelIndex &cell ) const override {
		if( cell.column() <= 0 ) {
			return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
		} else if( cell.column() == 1 ) {
			return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsUserCheckable;
		}

		const LevelId level = m_levels.at( cell.row() );
		if( !Blueprint::current()->hasLevel( level ) ) {
			if( cell.column() > 2 && cell.column() - 3 < s_vanillaNumAreas[cell.row()] ) {
				return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsUserCheckable;
			}
		} else if( Blueprint::current()->hasArea( level, (ubyte)cell.column() - 2 ) ) {
			return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsUserCheckable;
		}

		return Qt::ItemNeverHasChildren;
	}

	virtual bool setData( const QModelIndex &cell, const QVariant &value, int role ) override {
		if( cell.column() <= 0 || role != Qt::CheckStateRole ) {
			return QAbstractTableModel::setData( cell, value, role );
		}

		const bool checked = (value.value<Qt::CheckState>() == Qt::Checked);
		ubyte &areaMask = Blueprint::currentEditable()->asmModules().at( m_moduleId ).areaMask.at( (int)m_levels.at( cell.row() ) );
		if( cell.column() == 1 ) {
			areaMask = checked ? 0xFF : 0x00;
			emit dataChanged( cell.sibling( cell.row(), 1 ), cell.sibling( cell.row(), 9 ), { role } );
			return true;
		}

		if( checked ) {
			areaMask |= 1 << (cell.column() - 2);
		} else {
			areaMask &= 0xFF - (1 << (cell.column() - 2));
		}

		emit dataChanged( cell.sibling( cell.row(), 1 ), cell.sibling( cell.row(), 1 ), { role } );
		emit dataChanged( cell, cell, { role } );
		return true;
	}

};

ModuleAreaScopeDialog::ModuleAreaScopeDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ModuleAreaScopeDialog )
{
	m_ui->setupUi( this );
}

ModuleAreaScopeDialog::~ModuleAreaScopeDialog() {
	if( m_ui->areaTable->model() != nullptr ) {
		m_ui->areaTable->model()->deleteLater();
	}
	delete m_ui;
}

void ModuleAreaScopeDialog::open( const Uuid &moduleId ) {
	if( m_ui->areaTable->model() != nullptr ) {
		m_ui->areaTable->model()->deleteLater();
	}
	m_ui->areaTable->setModel( new AreaScopeModel( moduleId, m_ui->vanillaCheckbox->isChecked() ) );
	m_ui->areaTable->horizontalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );
	m_ui->areaTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );
	QDialog::open();
}

void ModuleAreaScopeDialog::filterChanged() {
	if( m_ui->areaTable->model() == nullptr ) return;
	static_cast<AreaScopeModel*>( m_ui->areaTable->model() )->setVanillaShown( m_ui->vanillaCheckbox->isChecked() );
}
