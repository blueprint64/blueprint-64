#ifndef UI_UTIL_HPP_
#define UI_UTIL_HPP_

#include <iostream>
#include <string>
#include <QWidget>
#include <QListWidget>
#include <QGridLayout>
#include <QComboBox>

#define FORWARD_SIGNAL( source, target, signal1, signal2 ) \
	QObject::connect( source, SIGNAL(signal1), target, SIGNAL(signal2) )

class QResourceStream : public std::istream {

	public:
	QResourceStream( const char *resourcePath );
	~QResourceStream() {
		delete rdbuf();
	}

};

namespace UiUtil {

	void clearLayout( QLayout *layout );
	void deleteItemAt( QLayout *layout, int index );
	void deleteItemAt( QGridLayout *layout, int row, int col );
	void freeLayoutItem( QLayoutItem *item );
#ifdef _WIN32
	QString toNativePath( const QString &posixPath );
#else
	inline QString toNativePath( const QString &posixPath ) {
		return posixPath;
	}
#endif
	void sortListEntries( QListWidget *list, bool(*compareFunc)(const QListWidgetItem&, const QListWidgetItem&) );
	void setItemEnabled( QComboBox *comboBox, int index, bool isEnabled );

}

namespace ListSorters {

	extern bool Lexiographic( const QListWidgetItem &x, const QListWidgetItem &y );
	extern bool NaturalString( const QListWidgetItem &x, const QListWidgetItem &y );

}

#define CONNECT_INTEGER_SPINNER( ignoreEvents, constModelGetter, modelGetter, property, spinner, type ) \
	connect( m_ui->spinner, &QSpinBox::editingFinished, this, [&](){ \
		if( ignoreEvents || constModelGetter().property == (type)m_ui->spinner->value() ) return; \
		modelGetter().property = (type)m_ui->spinner->value(); \
	});

#define CONNECT_FLOAT_SPINNER( ignoreEvents, constModelGetter, modelGetter, property, spinner ) \
	connect( m_ui->spinner, &QDoubleSpinBox::editingFinished, this, [&](){ \
		if( ignoreEvents || constModelGetter().property == (float)m_ui->spinner->value() ) return; \
		modelGetter().property = (float)m_ui->spinner->value(); \
	});

#define CONNECT_CHECKBOX( ignoreEvents, modelGetter, property, checkbox ) \
	connect( m_ui->checkbox, &QCheckBox::toggled, this, [&](bool __value){ \
		if( ignoreEvents ) return; \
		modelGetter().property = __value; \
	});

#define CONNECT_LINE_EDIT( ignoreEvents, constModelGetter, mutableModelGetter, property, textEditor ) \
	connect( m_ui->textEditor, &QLineEdit::editingFinished, this, [&](){ \
		if( ignoreEvents ) return; \
		const std::string __value = m_ui->textEditor->text().toStdString(); \
		if( constModelGetter().property != __value ) { \
			mutableModelGetter().property = __value; \
		} \
	});

#define CONNECT_RADIO_GROUP( ignoreEvents, modelGetter, property, radioGroup, type ) \
	connect( m_ui->radioGroup, qoverload<int,bool>( &QButtonGroup::buttonToggled ), this, [&](int __value, bool __enabled){ \
		if( __enabled && !ignoreEvents ) { \
			modelGetter().property = (type)__value; \
		} \
	});

#endif /* UI_UTIL_HPP_ */
