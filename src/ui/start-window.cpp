#include "src/ui/start-window.hpp"
#include "ui_start-window.h"

#include <QObject>
#include <QMessageBox>
#include "src/ui/util.hpp"
#include "src/polyfill/file-dialog.hpp"

StartWindow::StartWindow( QWidget *parent ) :
	QMainWindow( parent ),
	m_ui( new Ui::StartWindow )
{
	m_ui->setupUi( this );
#ifdef _WIN32
	m_ui->newProjectButton->setStyleSheet( "QPushButton { qproperty-iconSize: 400px 400px; }" );
	m_ui->openProjectButton->setStyleSheet( "QPushButton { qproperty-iconSize: 400px 400px; }" );
#endif
	QObject::connect(
		&m_createDialog, SIGNAL(projectCreated(CreateProjectOptions)),
		this, SIGNAL(projectCreated(CreateProjectOptions))
	);
}

StartWindow::~StartWindow() {
	delete m_ui;
}

void StartWindow::openProject() {
	const fs::path filePath = FileDialog::getFileOpen(
		"Open Project",
		{ { "Blueprint", "*.bbp;*.bpc" }, { "All Files", "*.*;*" } }
	);
	if( !filePath.empty() ) {
		emit projectOpened( filePath.u8string() );
	}
}

void StartWindow::showErrorDialog( string message ) {
	if( !isVisible() ) return;
	QMessageBox::critical(
		this,
		QString( "Error Loading Project" ),
		QString( message.c_str() ),
		QMessageBox::Ok
	);
}
