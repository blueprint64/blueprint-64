#ifndef SRC_UI_OBJECT_PLACEMENT_DIALOG_HPP_
#define SRC_UI_OBJECT_PLACEMENT_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class ObjectPlacementDialog;
}

class ObjectPlacementDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ObjectPlacementDialog *m_ui;
	LevelId m_levelId;
	ubyte m_areaIndex;
	bool m_canSaveArea;
	bool m_canApplyArea;
	bool m_canSaveLevel;
	bool m_canApplyLevel;

	void saveLevelActorsToBlueprint( std::ifstream &rom, LevelId levelId );

	public:
	explicit ObjectPlacementDialog( QWidget *parent = nullptr );
	~ObjectPlacementDialog();

	void open( LevelId levelId, ubyte areaIndex );

	public slots:
	void saveToBlueprint();
	void applyToRom();
	void scopeChanged();

};

#endif /* SRC_UI_OBJECT_PLACEMENT_DIALOG_HPP_ */
