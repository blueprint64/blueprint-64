#ifndef SRC_UI_CUSTOM_ACTION_EDITOR_HPP_
#define SRC_UI_CUSTOM_ACTION_EDITOR_HPP_

#include <QWidget>
#include <QAbstractButton>

namespace Ui {
	class CustomActionEditor;
}

class CustomActionEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::CustomActionEditor *m_ui;
	bool m_initializing;

	public:
	explicit CustomActionEditor( QWidget *parent = nullptr );
	~CustomActionEditor();

	public slots:
	void addAction();
	void removeAction();
	void nameChanged();
	void categoryChanged();
	void labelChanged();
	void flagToggled(QAbstractButton*, bool);
	void actionSelected();

	protected:
	virtual void showEvent( QShowEvent *event ) override;

};


#endif /* SRC_UI_CUSTOM_ACTION_EDITOR_HPP_ */
