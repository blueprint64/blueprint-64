#ifndef SRC_UI_USER_DATA_DIALOG_HPP_
#define SRC_UI_USER_DATA_DIALOG_HPP_

#include <QDialog>
#include "src/ui/user-data-link-widget.hpp"
#include "src/core/enums.hpp"

namespace Ui {
	class UserDataDialog;
}

class UserDataDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::UserDataDialog *m_ui;
	LevelId m_levelId;
	std::vector<PotentialLink> m_availableLinks;
	bool m_initializing;

	void reload();

	public:
	explicit UserDataDialog( QWidget *parent = nullptr );
	~UserDataDialog();

	inline void open( LevelId levelId ) {
		m_levelId = levelId;
		reload();
		QDialog::open();
	}

	public slots:
	void addData();
	void updateData();
	void deleteData();
	void selectionChanged();
	void nameChanged();
	void asmRefChanged();

};


#endif /* SRC_UI_USER_DATA_DIALOG_HPP_ */
