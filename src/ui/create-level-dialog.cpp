#include "src/ui/create-level-dialog.hpp"
#include "ui_create-level-dialog.h"

#include "src/core/enums.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/util.hpp"

CreateLevelDialog::CreateLevelDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::CreateLevelDialog )
{
	m_ui->setupUi( this );
}

CreateLevelDialog::~CreateLevelDialog() {
	delete m_ui;
}

void CreateLevelDialog::levelSelected() {
	LevelId level = (LevelId)m_ui->levelSelect->currentData().toUInt();
	CourseId course = levelIdToCourseId( level );
	m_ui->levelNameField->setText( Enum::toString<LevelId>( level ) );
	if( course <= CourseId::RainbowRide ) {
		m_ui->starSelectCheckbox->setEnabled( true );
	} else {
		m_ui->starSelectCheckbox->setChecked( false );
		m_ui->starSelectCheckbox->setEnabled( false );
	}
}

void CreateLevelDialog::accept() {
	const LevelId levelId = (LevelId)m_ui->levelSelect->currentData().toUInt();

	LevelSpecs levelSpecs = LevelSpecs::Default();
	levelSpecs.id = levelId;
	levelSpecs.name = m_ui->levelNameField->text().toStdString();
	levelSpecs.enableStarSelect = m_ui->starSelectCheckbox->isChecked();

	const CourseId courseId = levelIdToCourseId( levelId );
	if( courseId <= CourseId::RainbowRide ) {
		const int offset = 6*((int)courseId - 1 );
		for( int i = 0; i < 6; i++ ) {
			levelSpecs.starNames[i] = VanillaText::starNames()[ offset + i ];
		}
	}

	Blueprint::currentEditable()->levels()[levelId] = std::move( levelSpecs );

	emit levelCreated( levelId );
	QDialog::accept();
}

void CreateLevelDialog::showEvent( QShowEvent *event ) {
	m_ui->levelSelect->clear();
	for( LevelId levelId : Enum::values<LevelId>() ) {
		if( !Blueprint::current()->hasLevel( levelId ) && levelId != LevelId::EndScreen ) {
			m_ui->levelSelect->addItem( Enum::toString<LevelId>( levelId ), QVariant::fromValue<uint>( (uint)levelId ) );
		}
	}
	m_ui->levelSelect->setCurrentIndex( 0 );
	levelSelected();
	QDialog::showEvent( event );
}
