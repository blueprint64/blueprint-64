#ifndef SRC_UI_MODULE_DEVELOPER_VIEW_HPP_
#define SRC_UI_MODULE_DEVELOPER_VIEW_HPP_

#include <QWidget>

namespace Ui {
	class ModuleDevelopView;
}

class ModuleDevelopView : public QWidget {
	Q_OBJECT

	private:
	Ui::ModuleDevelopView *m_ui;

	public:
	explicit ModuleDevelopView( QWidget *parent = nullptr );
	~ModuleDevelopView();

	protected:
	virtual void showEvent( QShowEvent *event ) override;

	private:
	void rebuild( bool everything );

	public slots:
	void rebuildAll();
	void rebuildModule();
	void browse();
	void package();

	signals:
	void launchEmulator();

};

#endif /* SRC_UI_MODULE_DEVELOPER_VIEW_HPP_ */
