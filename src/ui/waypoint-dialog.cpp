#include "src/ui/waypoint-dialog.hpp"
#include "ui_waypoint-dialog.h"

#include <QSpinBox>
#include <cassert>
#include "src/ui/widgets/integer-spinner.hpp"
#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

WaypointDialog::WaypointDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::WaypointDialog ),
	m_levelId( LevelId::INVALID ),
	m_numWaypoints( 0 ),
	m_waypointId( 0 ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	m_initializing = false;
}

WaypointDialog::~WaypointDialog() {
	delete m_ui;
}

void WaypointDialog::open( LevelId levelId ) {
	m_initializing = true;
	m_levelId = levelId;
	m_ui->waypointsList->setCurrentRow( 0 );
	m_waypointId = 0;
	loadWaypoints();
	m_initializing = false;
	QDialog::open();
}

static QSpinBox *makeSpinner( short value ) {
	QSpinBox *spinner = new IntegerSpinner();
	spinner->setMaximum( 0x7FFF );
	spinner->setMinimum( -0x8000 );
	spinner->setSingleStep( 10 );
	spinner->setValue( value );
	return spinner;
}

void WaypointDialog::addRow( short x, short y, short z ) {
	m_numWaypoints++;
	m_ui->waypointLayout->addWidget( makeSpinner( x ), m_numWaypoints, 0 );
	m_ui->waypointLayout->addWidget( makeSpinner( y ), m_numWaypoints, 1 );
	m_ui->waypointLayout->addWidget( makeSpinner( z ), m_numWaypoints, 2 );

	QPushButton *deleteButton = new QPushButton();
	deleteButton->setIcon( QIcon::fromTheme( "delete" ) );
	deleteButton->setAccessibleName( "Delete" );
	deleteButton->setFlat( true );
	deleteButton->setVisible( m_numWaypoints > 1 );
	m_ui->waypointLayout->addWidget( deleteButton, m_numWaypoints, 3 );

	QObject::connect( deleteButton, &QPushButton::clicked, this, [=](){
		int row, _scratch;
		m_ui->waypointLayout->getItemPosition( m_ui->waypointLayout->indexOf( deleteButton ), &row, &_scratch, &_scratch, &_scratch );
		removeRow( row );
	});

	if( m_numWaypoints > 1 ) {
		static_cast<QPushButton*>( m_ui->waypointLayout->itemAtPosition( 1, 3 )->widget() )->setVisible( true );
	}
}

void WaypointDialog::loadWaypoints() {
	assert( m_levelId != LevelId::INVALID );

	for( int i = m_numWaypoints; i > 0; i-- ) {
		for( int j = 0; j < 4; j++ ) {
			UiUtil::deleteItemAt( m_ui->waypointLayout, i, j );
		}
	}
	m_numWaypoints = 0;

	const std::vector<Vec3s> &waypoints = Blueprint::current()->levels().at( m_levelId ).waypoints.at( m_waypointId );
	if( waypoints.empty() ) {
		addRow( 0, 0, 0 );
		return;
	}

	for( size_t i = 0; i < waypoints.size(); i++ ) {
		const Vec3s &waypoint = waypoints[i];
		addRow( waypoint.x, waypoint.y, waypoint.z );
	}

}

void WaypointDialog::saveWaypoints() const {
	assert( m_levelId != LevelId::INVALID );
	assert( Blueprint::current()->hasLevel( m_levelId ) );
	std::vector<Vec3s> &waypoints = Blueprint::currentEditable()->levels()[m_levelId].waypoints[m_waypointId];
	waypoints.clear();

	for( int i = 1; i <= m_numWaypoints; i++ ) {
		waypoints.push_back({
			(short)static_cast<const QSpinBox*>( m_ui->waypointLayout->itemAtPosition( i, 0 )->widget() )->value(),
			(short)static_cast<const QSpinBox*>( m_ui->waypointLayout->itemAtPosition( i, 1 )->widget() )->value(),
			(short)static_cast<const QSpinBox*>( m_ui->waypointLayout->itemAtPosition( i, 2 )->widget() )->value()
		});
	}
}

void WaypointDialog::removeRow( int rowIndex ) {
	assert( rowIndex > 0 );
	assert( rowIndex <= m_numWaypoints );

	for( int i = 0; i < 4; i++ ) {
		UiUtil::deleteItemAt( m_ui->waypointLayout, rowIndex, i );
	}

	while( rowIndex++ < m_numWaypoints ) {
		for( int i = 0; i < 4; i++ ) {
			QLayoutItem *item = m_ui->waypointLayout->itemAtPosition( rowIndex, i );
			m_ui->waypointLayout->removeItem( item );
			m_ui->waypointLayout->addItem( item, rowIndex - 1, i );
		}
	}

	if( --m_numWaypoints == 1 ) {
		static_cast<QPushButton*>( m_ui->waypointLayout->itemAtPosition( 1, 3 )->widget() )->setVisible( false );
	}
}

void WaypointDialog::selectedWaypointUser() {
	if( m_initializing ) return;
	m_initializing = true;
	saveWaypoints();
	m_waypointId = m_ui->waypointsList->currentRow();
	loadWaypoints();
	m_initializing = false;
}
