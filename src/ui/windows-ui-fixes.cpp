#include "src/ui/windows-ui-fixes.hpp"

#include <QFont>
#include <QPalette>
#include <QColor>
#include <QIcon>
#include <QLibrary>
#include <QPluginLoader>
#include <QMessageBox>
#include <QAbstractButton>
#include <QDialogButtonBox>
#include <iostream>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/appdata.hpp"

static constexpr QMessageBox::StandardButtons s_positiveButtons =
	QMessageBox::Ok | QMessageBox::Apply | QMessageBox::Yes | QMessageBox::YesToAll;

static constexpr QMessageBox::StandardButtons s_negativeButtons =
	QMessageBox::Cancel | QMessageBox::Abort | QMessageBox::No | QMessageBox::NoToAll | QMessageBox::Close;

bool WindowsIconFixer::eventFilter( QObject *object, QEvent *event ) {
	if( event->type() != QEvent::Show ) {
		return QObject::eventFilter( object, event );
	}

	QMessageBox *dialog = dynamic_cast<QMessageBox*>( object );
	if( dialog != nullptr ) {
		for( QAbstractButton *dialogButton : dialog->buttons() ) {
			const QMessageBox::StandardButtons buttonType = dialog->standardButton( dialogButton );
			if( buttonType & s_positiveButtons ) {
				dialogButton->setIcon( QIcon::fromTheme( "dialog-ok" ) );
			} else if( buttonType & s_negativeButtons ) {
				dialogButton->setIcon( QIcon::fromTheme( "dialog-cancel" ) );
			} else if( buttonType & (QMessageBox::Save | QMessageBox::SaveAll) ) {
				dialogButton->setIcon( QIcon::fromTheme( "document-save" ) );
			} else if( buttonType & QMessageBox::Open ) {
				dialogButton->setIcon( QIcon::fromTheme( "document-open" ) );
			}
		}
	}

	QDialogButtonBox *buttonTray = dynamic_cast<QDialogButtonBox*>( object );
	if( buttonTray != nullptr ) {
		for( QAbstractButton *dialogButton : buttonTray->buttons() ) {
			const QDialogButtonBox::StandardButton buttonType = buttonTray->standardButton( dialogButton );
			switch( buttonType ) {
				case QDialogButtonBox::StandardButton::Ok:
				case QDialogButtonBox::StandardButton::Apply:
				case QDialogButtonBox::StandardButton::Yes:
				case QDialogButtonBox::StandardButton::YesToAll:
					dialogButton->setIcon( QIcon::fromTheme( "dialog-ok" ) ); break;
				case QDialogButtonBox::StandardButton::Open:
					dialogButton->setIcon( QIcon::fromTheme( "document-open" ) ); break;
				case QDialogButtonBox::StandardButton::Save:
				case QDialogButtonBox::StandardButton::SaveAll:
					dialogButton->setIcon( QIcon::fromTheme( "document-save" ) ); break;
				case QDialogButtonBox::StandardButton::Cancel:
					dialogButton->setIcon( QIcon::fromTheme( "dialog-cancel" ) ); break;
				case QDialogButtonBox::StandardButton::Close:
				case QDialogButtonBox::StandardButton::No:
				case QDialogButtonBox::StandardButton::NoToAll:
				case QDialogButtonBox::StandardButton::Abort:
					dialogButton->setIcon( QIcon::fromTheme( "dialog-close" ) ); break;
				case QDialogButtonBox::StandardButton::Reset:
				case QDialogButtonBox::StandardButton::Retry:
					dialogButton->setIcon( QIcon::fromTheme( "view-refresh" ) ); break;
				default: break;
			}
		}
	}

	return QObject::eventFilter( object, event );
};

#define PALETTE_DEF1( role, colour ) \
	palette.setColor( QPalette::role, colour );
#define PALETTE_DEF2( role, colour1, colour2 ) \
	palette.setColor( QPalette::role, colour1 ); \
	palette.setColor( QPalette::Disabled, QPalette::role, colour2 );
#define PALETTE_DEF3( role, colour1, colour2, colour3 ) \
	palette.setColor( QPalette::Active, QPalette::role, colour1 ); \
	palette.setColor( QPalette::Inactive, QPalette::role, colour2 ); \
	palette.setColor( QPalette::Disabled, QPalette::role, colour3 );

static inline void useBreezePalette( QApplication &app ) {
	const QColor backgroundColour = QColor::fromRgb( 239, 240, 241 );
	const QColor textColour = QColor::fromRgb( 35, 38, 39 );
	const QColor disabledText = QColor::fromRgb( 160, 162, 162 );
	const QColor disabledBackground = QColor::fromRgb( 227, 229, 231 );
	const QColor white = QColor::fromRgb( 255, 255, 255 );
	const QColor offwhite = QColor::fromRgb( 252, 252, 252 );

	QPalette palette = app.palette();
	PALETTE_DEF2( WindowText, textColour, disabledText )
	PALETTE_DEF2( Button, backgroundColour, disabledBackground )
	PALETTE_DEF1( Light, white )
	PALETTE_DEF2( Midlight, QColor::fromRgb( 247, 247, 248 ), QColor::fromRgb( 236, 237, 238 ) )
	PALETTE_DEF2( Dark, QColor::fromRgb( 136, 142, 147 ), QColor::fromRgb( 136, 142, 147 ) )
	PALETTE_DEF2( Mid, QColor::fromRgb( 196, 201, 205 ), QColor::fromRgb( 188, 192, 197 ) )
	PALETTE_DEF2( Text, textColour, QColor::fromRgb( 168, 169, 269 ) )
	PALETTE_DEF1( BrightText, white )
	PALETTE_DEF2( ButtonText, textColour, disabledText )
	PALETTE_DEF2( Base, offwhite, QColor::fromRgb( 241, 241, 241 ) )
	PALETTE_DEF2( Window, backgroundColour, disabledBackground )
	PALETTE_DEF1( Shadow, QColor::fromRgb( 71, 74, 76 ) )
	PALETTE_DEF3( Highlight, QColor::fromRgb( 61, 174, 233 ), QColor::fromRgb( 194, 224, 245 ), disabledBackground )
	PALETTE_DEF3( HighlightedText, offwhite, textColour, disabledText )
	PALETTE_DEF2( Link, QColor::fromRgb( 41, 128, 185 ), QColor::fromRgb( 162, 200, 224 ) )
	PALETTE_DEF2( LinkVisited, QColor::fromRgb( 127, 140, 141 ), QColor::fromRgb( 199, 203, 203 ) )
	PALETTE_DEF2( AlternateBase, backgroundColour, disabledBackground )
	app.setPalette( palette );
}

#undef PALETTE_DEF1
#undef PALETTE_DEF2
#undef PALETTE_DEF3

static inline void setMinimumFontSize( QApplication &app ) {
	QFont defaultFont = app.font();
	if( defaultFont.pointSize() < 10 ) {
		defaultFont.setPointSize( 10 );
		app.setFont( defaultFont );
	}
}

static inline void loadThemePlugins( QApplication &app ) {
	fs::directory_iterator pluginDir( AppData::programDir() / "styles" );
	for( const auto &plugin : pluginDir ) {
		const QString pluginPath = plugin.path().u8string().c_str();
		if( !QLibrary::isLibrary( pluginPath ) ) continue;

		QPluginLoader *pluginLoader = new QPluginLoader( pluginPath, &app );
		if( !pluginLoader->load() ) {
			std::cerr << "Failed to load style plugin " << plugin.path().filename().u8string() << std::endl << std::flush;
			pluginLoader->deleteLater();
		}
	}
}

static const char *s_cssTweaks = R"THEME_DEF(

QPushButton, QToolButton {
	padding: 7px;
}

QComboBox {
	padding: 5px;
}

QSpinBox, QDoubleSpinBox {
	padding: 4px 2px;
}

QLineEdit {
	padding: 4px;
}

QListWidget::item, QTreeView::item {
	padding: 4px 1px;
}

QMessageBox QAbstractButton, QDialogButtonBox QAbstractButton {
	min-width: 66px;
}

QGroupBox::title {
	subcontrol-position: top center;
}

QPushButton {
	qproperty-iconSize: 16px 16px;
}

QMainWindow#MainWindow QPushButton#refreshButton,
QMainWindow#MainWindow QPushButton#controllerConfigButton,
QMainWindow#MainWindow QToolButton#menuButton {
	qproperty-iconSize: 24px 24px;
}

)THEME_DEF";

void WindowsUiFixes::apply( QApplication &app ) {
	loadThemePlugins( app );
	setMinimumFontSize( app );
	useBreezePalette( app );
	app.installEventFilter( new WindowsIconFixer( &app ) );
	app.setStyleSheet( s_cssTweaks );
}
