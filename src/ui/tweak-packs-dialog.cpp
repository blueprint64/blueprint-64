#include "src/ui/tweak-packs-dialog.hpp"
#include "ui_tweak-packs-dialog.h"

#include <unordered_set>
#include <QMessageBox>
#include "src/core/blueprint.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/ui/util.hpp"

TweakPacksDialog::TweakPacksDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::TweakPacksDialog ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	m_initializing = false;
}

TweakPacksDialog::~TweakPacksDialog() {
	delete m_ui;
}

void TweakPacksDialog::reload() {
	m_initializing = true;
	m_installedTweakPacks = TweakStore::getInstalledTweakPacks();
	m_blueprintTweakPacks = Blueprint::current()->getEmbeddedTweakPacks();

	m_installedLookup.clear();
	for( const TweakPack &pack : m_installedTweakPacks ) {
		m_installedLookup[pack.id] = pack.version;
	}

	m_blueprintLookup.clear();
	for( const TweakPack &pack : m_blueprintTweakPacks ) {
		m_blueprintLookup[pack.id] = pack.version;
	}

	m_ui->installedPacksTable->clear();
	m_ui->installedPacksTable->setRowCount( 0 );
	for( const TweakPack &pack : m_installedTweakPacks ) {
		QTableWidget *table = m_ui->installedPacksTable;
		const int row = table->rowCount();
		table->insertRow( row );
		table->setItem( row, 0, new QTableWidgetItem( pack.name.c_str() ) );
		table->setItem( row, 1, new QTableWidgetItem( pack.version.toString().c_str() ) );
		if( m_blueprintLookup.count( pack.id ) > 0 ) {
			table->setItem( row, 2, new QTableWidgetItem( pack.version == m_blueprintLookup.at( pack.id ) ? "✓" : "≠" ) );
		} else {
			table->setItem( row, 2, new QTableWidgetItem( " " ) );
		}
	}
	m_ui->installedPacksTable->selectRow( -1 );
	m_ui->installedPacksTable->clearSelection();

	m_ui->blueprintPacksTable->clear();
	m_ui->blueprintPacksTable->setRowCount( 0 );
	for( const TweakPack &pack : m_blueprintTweakPacks ) {
		QTableWidget *table = m_ui->blueprintPacksTable;
		const int row = table->rowCount();
		table->insertRow( row );
		table->setItem( row, 0, new QTableWidgetItem( pack.name.c_str() ) );
		table->setItem( row, 1, new QTableWidgetItem( pack.version.toString().c_str() ) );
		if( m_installedLookup.count( pack.id ) > 0 ) {
			table->setItem( row, 2, new QTableWidgetItem( pack.version == m_installedLookup.at( pack.id ) ? "✓" : "≠" ) );
		} else {
			table->setItem( row, 2, new QTableWidgetItem( " " ) );
		}
	}
	m_ui->blueprintPacksTable->selectRow( -1 );
	m_ui->blueprintPacksTable->clearSelection();

	m_ui->updateButton->setEnabled( false );
	m_ui->updateFromBlueprintButton->setEnabled( false );
	m_ui->installFromBlueprintButton->setEnabled( false );
	m_ui->uninstallButton->setEnabled( false );

	m_ui->blueprintPacksTable->setHorizontalHeaderLabels({ "Name", "Version", "Installed" });
	m_ui->blueprintPacksTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );
	m_ui->blueprintPacksTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::ResizeToContents );
	m_ui->blueprintPacksTable->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::ResizeToContents );

	m_ui->installedPacksTable->setHorizontalHeaderLabels({ "Name", "Version", "Used" });
	m_ui->installedPacksTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );
	m_ui->installedPacksTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::ResizeToContents );
	m_ui->installedPacksTable->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::ResizeToContents );

	m_initializing = false;
}

void TweakPacksDialog::installedPackSelected() {
	if( m_initializing || m_ui->installedPacksTable->currentRow() < 0 ) return;

	m_initializing = true;
	m_ui->blueprintPacksTable->selectRow( -1 );
	m_ui->blueprintPacksTable->clearSelection();
	m_ui->installFromBlueprintButton->setEnabled( false );
	m_ui->updateButton->setEnabled( false );
	m_ui->uninstallButton->setEnabled( true );

	const TweakPack &pack = m_installedTweakPacks.at( m_ui->installedPacksTable->currentRow() );
	m_ui->updateFromBlueprintButton->setEnabled(
		m_blueprintLookup.count( pack.id ) > 0 &&
		m_blueprintLookup.at( pack.id ) > pack.version
	);

	m_initializing = false;
}

void TweakPacksDialog::blueprintPackSelected() {
	if( m_initializing || m_ui->blueprintPacksTable->currentRow() < 0 ) return;

	m_initializing = true;
	m_ui->installedPacksTable->selectRow( -1 );
	m_ui->installedPacksTable->clearSelection();
	m_ui->updateFromBlueprintButton->setEnabled( false );
	m_ui->uninstallButton->setEnabled( false );

	const TweakPack &pack = m_blueprintTweakPacks.at( m_ui->blueprintPacksTable->currentRow() );
	if( m_installedLookup.count( pack.id ) > 0 ) {
		m_ui->installFromBlueprintButton->setEnabled( false );
		m_ui->updateButton->setEnabled( m_installedLookup.at( pack.id ) > pack.version );
	} else {
		m_ui->installFromBlueprintButton->setEnabled( true );
		m_ui->updateButton->setEnabled( false );
	}

	m_ui->updateFromBlueprintButton->setEnabled(
		m_blueprintLookup.count( pack.id ) > 0 &&
		m_blueprintLookup.at( pack.id ) > pack.version
	);

	m_initializing = false;
}

void TweakPacksDialog::updateFromBlueprint() {
	if( QMessageBox::question( this, "Update?", "Update your installed tweak pack to the newer version embedded in this blueprint?" ) != QMessageBox::Yes ) {
		return;
	}

	const TweakPack &pack = m_blueprintTweakPacks.at( m_ui->blueprintPacksTable->currentRow() );
	try {
		TweakStore::installTweakPack( Blueprint::current()->getTweakPackPath( pack.id ).u8string() );
	} catch( const std::exception &ex ) {
		QMessageBox::critical( this, "Update Failed", (string("Failed to update tweak pack:\n") + ex.what()).c_str(), QMessageBox::Ok );
		return;
	}

	QMessageBox::information( this, "Tweak Pack Updated", ("Tweak pack has been updated to version " + pack.version.toString()).c_str() , QMessageBox::Ok );
	reload();
}

void TweakPacksDialog::updateFromInstalled() {
	if( QMessageBox::question( this, "Update?", "Update this blueprint to use the newer version of the tweak pack that you have installed?" ) != QMessageBox::Yes ) {
		return;
	}

	const TweakPack &pack = m_installedTweakPacks.at( m_ui->installedPacksTable->currentRow() );
	Blueprint::currentEditable()->forceSaveTweakPack( pack.id );
	QMessageBox::information( this, "Tweak Pack Updated", ("Tweak pack has been updated to version " + pack.version.toString()).c_str() , QMessageBox::Ok );

	reload();
}

void TweakPacksDialog::installFromBlueprint() {
	if( QMessageBox::question( this, "Install?", "Install the tweak pack used by this blueprint?" ) != QMessageBox::Yes ) {
		return;
	}

	const TweakPack &pack = m_blueprintTweakPacks.at( m_ui->blueprintPacksTable->currentRow() );
	try {
		TweakStore::installTweakPack( Blueprint::current()->getTweakPackPath( pack.id ).u8string() );
	} catch( const std::exception &ex ) {
		QMessageBox::critical( this, "Install Failed", (string("Failed to install tweak pack:\n") + ex.what()).c_str(), QMessageBox::Ok );
		return;
	}

	QMessageBox::information( this, "Tweak Pack Installed", "Tweak pack was installed successfully.", QMessageBox::Ok );
	reload();
}

void TweakPacksDialog::installNew() {
	const fs::path filePath = FileDialog::getFileOpen(
		"Select Tweak Pack",
		{ { "Tweak Packs", "*.json" }, { "All Files", "*.*;*" } }
	);
	if( filePath.empty() ) return;

	try {
		TweakStore::installTweakPack( filePath.u8string() );
	} catch( const std::exception &ex ) {
		QMessageBox::critical( this, "Install Failed", (string("Failed to install tweak pack:\n") + ex.what()).c_str(), QMessageBox::Ok );
		return;
	}

	QMessageBox::information( this, "Tweak Pack Installed", "Tweak pack was installed successfully.", QMessageBox::Ok );
	reload();
}

void TweakPacksDialog::uninstall() {
	if( QMessageBox::question( this, "Uninstall?", "Are you sure you want to uninstall this tweak pack?" ) != QMessageBox::Yes ) {
		return;
	}

	const TweakPack &pack = m_installedTweakPacks.at( m_ui->installedPacksTable->currentRow() );
	TweakStore::uninstallTweakPack( pack.id );
	reload();
}
