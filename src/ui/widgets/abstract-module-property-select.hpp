/*
 * abstract-module-property-select.hpp
 *
 *  Created on: Jul. 21, 2020
 *      Author: matt
 */

#ifndef SRC_UI_WIDGETS_ABSTRACT_MODULE_PROPERTY_SELECT_HPP_
#define SRC_UI_WIDGETS_ABSTRACT_MODULE_PROPERTY_SELECT_HPP_

#include <QFrame>
#include <QLabel>
#include <QLayout>
#include <functional>
#include "src/core/asm-modules.hpp"

class AbstractModulePropertySelect : public QFrame {
	Q_OBJECT

	private:
	QLabel *m_label;
	QLayout *m_sublayout;
	bool m_noSignal;

	protected:
	QWidget *m_select;
	std::function<AsmModulePropertyValue(const QWidget*)> m_getter;
	std::function<void(QWidget*, const AsmModulePropertyValue &value)> m_setter;

	public:
	explicit AbstractModulePropertySelect(
		QWidget *parent,
		const AsmModuleProperty &property
	);
	virtual ~AbstractModulePropertySelect();

	inline void setValue( const AsmModulePropertyValue &value ) {
		m_noSignal = true;
		m_setter( m_select, value );
		m_noSignal = false;
	}

	protected slots:
	virtual void valueChanged() const = 0;

};

#endif /* SRC_UI_WIDGETS_ABSTRACT_MODULE_PROPERTY_SELECT_HPP_ */
