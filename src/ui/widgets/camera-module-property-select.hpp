#ifndef SRC_UI_WIDGETS_CAMERA_MODULE_PROPERTY_SELECT_HPP_
#define SRC_UI_WIDGETS_CAMERA_MODULE_PROPERTY_SELECT_HPP_

#include "src/ui/widgets/abstract-module-property-select.hpp"
#include "src/core/data/camera.hpp"

class CameraModulePropertySelect : public AbstractModulePropertySelect {
	Q_OBJECT

	private:
	std::map<string,CameraModulePropertyValue> *const m_moduleParams;
	const ModulePropertyType m_type;
	const string m_propertyName;

	public:
	explicit CameraModulePropertySelect(
		QWidget *parent,
		std::map<string,CameraModulePropertyValue> *moduleParams,
		const AsmModuleProperty &property
	);
	inline virtual ~CameraModulePropertySelect() {}

	protected slots:
	virtual void valueChanged() const override;

};



#endif /* SRC_UI_WIDGETS_CAMERA_MODULE_PROPERTY_SELECT_HPP_ */
