#include "src/ui/widgets/level-select.hpp"

#include "src/core/blueprint.hpp"
#include <cassert>
#include <map>

static inline std::map<LevelId,int> buildLevelMap() {
	int index = 0;
	std::map<LevelId,int> map;
	for( LevelId levelId : Enum::values<LevelId>() ) {
		map[levelId] = index++;
	}
	return map;
}

static const std::map<LevelId,int> &levelMap() {
	static const std::map<LevelId,int> s_map = buildLevelMap();
	return s_map;
}

LevelSelect::LevelSelect( QWidget *parent ) :
	QComboBox( parent ),
	m_includeEndScreen( false )
{
	for( LevelId levelId : Enum::values<LevelId>() ) {
		addItem( Enum::toString<LevelId>( levelId ) );
	}
	assert( count() == 31 );

	updateLevelNames();

	connect( this, QOverload<int>::of( &LevelSelect::currentIndexChanged ), this, &LevelSelect::emitlevelSelected );
}

LevelId LevelSelect::selectedLevel() const {
	if( currentIndex() < 0 ) return LevelId::INVALID;
	return Enum::values<LevelId>().at( currentIndex() );
}

void LevelSelect::setLevel( LevelId level ) {
	const auto i = levelMap().find( level );
	setCurrentIndex( i == levelMap().end() ? -1 : i->second );
}

void LevelSelect::showEvent( QShowEvent *event ) {
	updateLevelNames();
	QComboBox::showEvent( event );
}

void LevelSelect::updateLevelNames() {
	if( Blueprint::current() == nullptr ) return;

	for( int i = 0; i < 30; i++ ) {
		const LevelId levelId = Enum::values<LevelId>()[i];
		if( Blueprint::current()->hasLevel( levelId ) ) {
			setItemText( i, Blueprint::current()->levels().at( levelId ).name.c_str() );
		} else {
			setItemText( i, Enum::toString<LevelId>( levelId ) );
		}
	}

	if( count() == 30 && m_includeEndScreen ) {
		addItem( Enum::toString<LevelId>( LevelId::EndScreen ) );
	} else if( count() == 31 && !m_includeEndScreen ) {
		removeItem( 30 );
	}
}

void LevelSelect::emitlevelSelected() {
	emit levelSelected( (int)selectedLevel() );
}
