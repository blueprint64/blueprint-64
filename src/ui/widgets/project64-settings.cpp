#include "src/ui/widgets/project64-settings.hpp"
#include "ui_project64-settings.h"

#include "src/core/storage.hpp"

Project64SettingsWidget::Project64SettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::Project64SettingsWidget )
{
	m_ui->setupUi( this );
	m_ui->warningIcon->setText( QString() );
	m_ui->warningIcon->setPixmap( QIcon::fromTheme( "emblem-warning" ).pixmap( 16, 16 ) );
}

Project64SettingsWidget::~Project64SettingsWidget() {
	delete m_ui;
}

void Project64SettingsWidget::save() const {
#ifdef _WIN32
	auto &settings = AppPreferences::current().emuConfig.pj64Settings;
	if( m_installedVersions.empty() ) {
		settings.exePath = "";
		settings.fullVersion = "";
		settings.majorVersion = 0;
	} else {
		settings = m_installedVersions.at( m_ui->versionSelect->currentIndex() );
	}
#endif
}

void Project64SettingsWidget::load() {
#ifdef _WIN32
	m_installedVersions = std::move( Emulator::Project64::getInstalledVersions() );
	m_ui->versionSelect->clear();

	if( m_installedVersions.empty() ) {
		m_ui->versionSelect->setEnabled( false );
		m_ui->versionSelect->addItem( "--Not Installed--" );
		return;
	}

	const string &currentPath = AppPreferences::current().emuConfig.pj64Settings.exePath;
	int selectedIndex = 0;

	m_ui->versionSelect->setEnabled( true );
	for( const Emulator::Project64::Settings &version : m_installedVersions ) {
		m_ui->versionSelect->addItem( version.fullVersion.c_str() );
		if( version.exePath == currentPath ) {
			selectedIndex = m_ui->versionSelect->count() - 1;
		}
	}

	m_ui->versionSelect->setCurrentIndex( selectedIndex );
#endif
}
