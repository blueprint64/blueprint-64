#ifndef SRC_UI_WIDGETS_SIMPLE_BEHAVIOUR_STEP_PANEL_HPP_
#define SRC_UI_WIDGETS_SIMPLE_BEHAVIOUR_STEP_PANEL_HPP_

#include <QFrame>
#include "src/core/enums.hpp"
#include "src/core/data/behaviour.hpp"

namespace Ui {
	class SimpleBehaviourStepPanel;
}

class SimpleBehaviourStepPanel : public QFrame {
	Q_OBJECT

	private:
	Ui::SimpleBehaviourStepPanel *const m_ui;
	const LevelId m_levelId;
	const uint m_objectId;
	size_t m_index;
	bool m_ignoreEvents;

	SimpleBehaviourMovement &getModel();
	const SimpleBehaviourMovement &getConstModel() const;
	const SimpleBehaviourMovement &getConstModel( size_t &numSteps ) const;

	public:
	explicit SimpleBehaviourStepPanel(
		QWidget *parent,
		LevelId levelId,
		uint objectId,
		size_t index
	);
	~SimpleBehaviourStepPanel();

	void refresh();
	inline void decrementIndex() {
		m_index--;
		refresh();
	}

	void setAnglesInHex( bool useHex );

	private slots:
	void actionChanged( int action );

	signals:
	void moveUp( size_t index );
	void moveDown( size_t index );
	void remove( size_t index );

};



#endif /* SRC_UI_WIDGETS_SIMPLE_BEHAVIOUR_STEP_PANEL_HPP_ */
