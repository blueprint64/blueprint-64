#include "src/ui/widgets/simple-behaviour-step-panel.hpp"
#include "ui_simple-behaviour-step-panel.h"

#include <utility>
#include "src/ui/util.hpp"
#include "src/core/blueprint.hpp"

#define CONNECT_ANGLE_SPINNER( property, spinner ) \
	connect( m_ui->spinner, &AngleSpinner::editingFinished, this, [&](short __value){ \
		if( m_ignoreEvents ) return; \
		getModel().property = __value; \
	});

SimpleBehaviourStepPanel::SimpleBehaviourStepPanel(
	QWidget *parent,
	LevelId levelId,
	uint objectId,
	size_t index
) : QFrame( parent ),
	m_ui( new Ui::SimpleBehaviourStepPanel ),
	m_levelId( levelId ),
	m_objectId( objectId ),
	m_index( index ),
	m_ignoreEvents( false )
{
	m_ui->setupUi( this );

	CONNECT_INTEGER_SPINNER( m_ignoreEvents, getConstModel, getModel, frames, durationSpinner, short );

	CONNECT_FLOAT_SPINNER( m_ignoreEvents, getConstModel, getModel, speed.x, speedX );
	CONNECT_FLOAT_SPINNER( m_ignoreEvents, getConstModel, getModel, speed.y, speedY );
	CONNECT_FLOAT_SPINNER( m_ignoreEvents, getConstModel, getModel, speed.z, speedZ );

	CONNECT_ANGLE_SPINNER( angularVelocity.pitch, pitchVel );
	CONNECT_ANGLE_SPINNER( angularVelocity.yaw, yawVel );
	CONNECT_ANGLE_SPINNER( angularVelocity.roll, rollVel );

	connect( m_ui->moveUpButton, &QPushButton::clicked, this, [&](){ emit moveUp( m_index ); });
	connect( m_ui->moveDownButton, &QPushButton::clicked, this, [&](){ emit moveDown( m_index ); });
	connect( m_ui->deleteButton, &QPushButton::clicked, this, [&](){ emit remove( m_index ); });

	refresh();
}

SimpleBehaviourStepPanel::~SimpleBehaviourStepPanel() {
	delete m_ui;
}

void SimpleBehaviourStepPanel::refresh() {
	size_t numSteps;
	const SimpleBehaviourMovement &model = getConstModel( numSteps );
	m_ignoreEvents = true;
	m_ui->actionSelect->setCurrentIndex( (int)model.type );
	m_ui->durationSpinner->setValue( model.frames );
	m_ui->translationOptions->setVisible( (int)(model.type & SimpleBehaviourInstruction::Move) );
	m_ui->speedX->setValue( model.speed.x );
	m_ui->speedY->setValue( model.speed.y );
	m_ui->speedZ->setValue( model.speed.z );
	m_ui->rotationOptions->setVisible( (int)(model.type & SimpleBehaviourInstruction::Rotate) );
	m_ui->pitchVel->setValue( model.angularVelocity.pitch );
	m_ui->yawVel->setValue( model.angularVelocity.yaw );
	m_ui->rollVel->setValue( model.angularVelocity.roll );
	m_ui->moveUpButton->setEnabled( m_index > 0 );
	m_ui->moveDownButton->setEnabled( m_index + 1 < numSteps );
	m_ignoreEvents = false;
}

void SimpleBehaviourStepPanel::actionChanged( int action ) {
	if( m_ignoreEvents ) return;
	getModel().type = (SimpleBehaviourInstruction)action;
	m_ui->translationOptions->setVisible( action & (int)SimpleBehaviourInstruction::Move );
	m_ui->rotationOptions->setVisible( action & (int)SimpleBehaviourInstruction::Rotate );
}


SimpleBehaviourMovement &SimpleBehaviourStepPanel::getModel() {
	return Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).behaviour.autoGen.movementLoop.at( m_index );
}

const SimpleBehaviourMovement &SimpleBehaviourStepPanel::getConstModel() const {
	return Blueprint::current()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).behaviour.autoGen.movementLoop.at( m_index );
}

const SimpleBehaviourMovement &SimpleBehaviourStepPanel::getConstModel( size_t &numSteps ) const {
	const std::vector<SimpleBehaviourMovement> &steps = Blueprint::current()->levels().at( m_levelId ).simpleObjects.at( m_objectId ).behaviour.autoGen.movementLoop;
	numSteps = steps.size();
	return steps.at( m_index );
}

void SimpleBehaviourStepPanel::setAnglesInHex( bool useHex ) {
	m_ui->pitchVel->setHexMode( useHex );
	m_ui->yawVel->setHexMode( useHex );
	m_ui->rollVel->setHexMode( useHex );
}
