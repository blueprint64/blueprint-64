#ifndef SRC_UI_WIDGETS_RETROARCH_SETTINGS_HPP_
#define SRC_UI_WIDGETS_RETROARCH_SETTINGS_HPP_

#include <QWidget>

namespace Ui {
	class RetroarchSettingsWidget;
}

class RetroarchSettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::RetroarchSettingsWidget *m_ui;

	public:
	explicit RetroarchSettingsWidget( QWidget *parent = nullptr );
	~RetroarchSettingsWidget();

	public slots:
	void save() const;
	void load();
	void coreChanged( int coreIndex );

};

#endif /* SRC_UI_WIDGETS_RETROARCH_SETTINGS_HPP_ */
