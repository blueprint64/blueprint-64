#include "src/ui/widgets/other-emulator-settings.hpp"
#include "ui_other-emulator-settings.h"

#include "src/core/storage.hpp"

OtherEmulatorSettingsWidget::OtherEmulatorSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::OtherEmulatorSettingsWidget )
{
	m_ui->setupUi( this );
}

OtherEmulatorSettingsWidget::~OtherEmulatorSettingsWidget() {
	delete m_ui;
}

void OtherEmulatorSettingsWidget::load() {
	m_ui->launchCommand->setText( AppPreferences::current().emuConfig.otherEmulatorCmd.c_str() );
}

void OtherEmulatorSettingsWidget::save() const {
	AppPreferences::current().emuConfig.otherEmulatorCmd = m_ui->launchCommand->text().toStdString();
}
