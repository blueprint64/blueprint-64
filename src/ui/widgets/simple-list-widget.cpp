#include "src/ui/widgets/simple-list-widget.hpp"

#include <QMouseEvent>
#include <QMenu>

using std::string;

SimpleListWidget::SimpleListWidget( QWidget *parent ) :
	QListWidget( parent ),
	m_hasDeleteAction( false ),
	m_hasEditAction( false ),
	m_hasMoveAction( false )
{
	connect( this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(onRightClick(const QPoint &)) );
	setContextMenuPolicy( Qt::CustomContextMenu );
}

void SimpleListWidget::mouseDoubleClickEvent( QMouseEvent *event ) {
	if( m_hasEditAction ) {
		QListWidgetItem *item = itemAt( event->pos() );
		if( item != nullptr ) {
			setCurrentItem( item );
			emit itemEdit();
		}
	}
	QListWidget::mouseDoubleClickEvent( event );
}

void SimpleListWidget::onRightClick( const QPoint &point ) {
	QListWidgetItem *item = itemAt( point );
	if( item == nullptr ) return;
	setCurrentItem( item );

	QMenu contextMenu( this );

	QAction editAction( "Edit" );
	if( m_hasEditAction ) {
		connect( &editAction, SIGNAL(triggered()), this, SIGNAL(itemEdit()) );
		contextMenu.addAction( &editAction );
	}

	QAction moveAction( "Move" );
	if( m_hasMoveAction ) {
		connect( &moveAction, SIGNAL(triggered()), this, SIGNAL(itemMove()) );
		contextMenu.addAction( &moveAction );
	}

	QAction deleteAction( "Delete" );
	if( m_hasDeleteAction ) {
		connect( &deleteAction, SIGNAL(triggered()), this, SIGNAL(itemDelete()) );
		contextMenu.addAction( &deleteAction );
	}

	if( !contextMenu.actions().empty() ) {
		contextMenu.exec( mapToGlobal( point ) );
	}
}
