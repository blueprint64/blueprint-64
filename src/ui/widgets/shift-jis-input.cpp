#include "src/ui/widgets/shift-jis-input.hpp"

#include <QValidator>
#include <array>
#include <map>

static const std::map<ushort,char> s_kanaMap = {
	                        { 0x3002, (char)0xA1 }, { 0x300C, (char)0xA2 }, { 0x300D, (char)0xA3 },
	{ 0x3001, (char)0xA4 }, { 0x30FB, (char)0xA5 }, { 0x30F2, (char)0xA6 }, { 0x30A1, (char)0xA7 },
	{ 0x30A3, (char)0xA8 }, { 0x30A5, (char)0xA9 }, { 0x30A7, (char)0xAA }, { 0x30A9, (char)0xAB },
	{ 0x30E3, (char)0xAC }, { 0x30E5, (char)0xAD }, { 0x30E7, (char)0xAE }, { 0x30C3, (char)0xAF },

	{ 0x30FC, (char)0xB0 }, { 0x30A2, (char)0xB1 }, { 0x30A4, (char)0xB2 }, { 0x30A6, (char)0xB3 },
	{ 0x30A8, (char)0xB4 }, { 0x30AA, (char)0xB5 }, { 0x30AB, (char)0xB6 }, { 0x30AD, (char)0xB7 },
	{ 0x30AF, (char)0xB8 }, { 0x30B1, (char)0xB9 }, { 0x30B3, (char)0xBA }, { 0x30B5, (char)0xBB },
	{ 0x30B7, (char)0xBC }, { 0x30B9, (char)0xBD }, { 0x30BB, (char)0xBE }, { 0x30BD, (char)0xBF },

	{ 0x30BF, (char)0xC0 }, { 0x30C1, (char)0xC1 }, { 0x30C4, (char)0xC2 }, { 0x30C6, (char)0xC3 },
	{ 0x30C8, (char)0xC4 }, { 0x30CA, (char)0xC5 }, { 0x30CB, (char)0xC6 }, { 0x30CC, (char)0xC7 },
	{ 0x30CD, (char)0xC8 }, { 0x30CE, (char)0xC9 }, { 0x30CF, (char)0xCA }, { 0x30D2, (char)0xCB },
	{ 0x30D5, (char)0xCC }, { 0x30D8, (char)0xCD }, { 0x30DB, (char)0xCE }, { 0x30DE, (char)0xCF },

	{ 0x30DF, (char)0xD0 }, { 0x30E0, (char)0xD1 }, { 0x30E1, (char)0xD2 }, { 0x30E2, (char)0xD3 },
	{ 0x30E4, (char)0xD4 }, { 0x30E6, (char)0xD5 }, { 0x30E8, (char)0xD6 }, { 0x30E9, (char)0xD7 },
	{ 0x30EA, (char)0xD8 }, { 0x30EB, (char)0xD9 }, { 0x30EC, (char)0xDA }, { 0x30ED, (char)0xDB },
	{ 0x30EF, (char)0xDC }, { 0x30F3, (char)0xDD }, { 0x309B, (char)0xDE }, { 0x309C, (char)0xDF }
};

static const std::array<ushort, 63> s_kanaList = {
	0x3002,	0x300C,	0x300D,	0x3001,	0x30FB,	0x30F2,	0x30A1,	0x30A3,
	0x30A5,	0x30A7,	0x30A9,	0x30E3,	0x30E5,	0x30E7,	0x30C3,	0x30FC,
	0x30A2,	0x30A4,	0x30A6,	0x30A8,	0x30AA,	0x30AB,	0x30AD,	0x30AF,
	0x30B1,	0x30B3,	0x30B5,	0x30B7,	0x30B9,	0x30BB,	0x30BD,	0x30BF,
	0x30C1,	0x30C4,	0x30C6,	0x30C8,	0x30CA,	0x30CB,	0x30CC,	0x30CD,
	0x30CE,	0x30CF,	0x30D2,	0x30D5,	0x30D8,	0x30DB,	0x30DE,	0x30DF,
	0x30E0,	0x30E1,	0x30E2,	0x30E4,	0x30E6,	0x30E8,	0x30E9,	0x30EA,
	0x30EB,	0x30EC,	0x30ED,	0x30EF,	0x30F3,	0x309B,	0x309C
};

inline static bool isFullWidth( const QChar &c ) {
	return s_kanaMap.count( c.unicode() ) > 0;
}

inline static bool isHalfWidth( const QChar &c ) {
	const ushort code = c.unicode();
	return code >= 0xFF61 && code <= 0xFF9F;
}

inline static bool isLatin( const QChar &c ) {
	const ushort code = c.unicode();
	return (code >= 0x20 && code <= 0x7D && code != 0x5C) || (code == 0xA5 || code == 0x203E);
}

inline static QChar halfToFull( const QChar &c ) {
	return QChar( s_kanaList.at( c.unicode() - 0xFF61 ) );
}

class ShiftJisValidator : public QValidator {

	public:
	explicit ShiftJisValidator( ShiftJisInput *parent ) : QValidator( parent ) {}
	virtual ~ShiftJisValidator() {}

	private:
	void truncate( QString &input ) const {
		int maxBytes = static_cast<const ShiftJisInput*>( parent() )->getMaxBytes();
		const string utf8 = input.toStdString();
		if( utf8.length() > (size_t)maxBytes ) {
			while( maxBytes > 0 && ((ubyte)utf8[maxBytes - 1] >> 6 == 0x10) ) maxBytes--;
			input = QString( utf8.substr( 0, maxBytes ).c_str() );
		}
	}

	public:

	virtual QValidator::State validate( QString &input, int &pos ) const override {
		const int prevLength = input.length();
		for( const QChar &c : input ) {
			if( !isLatin( c ) && !isFullWidth( c ) ) {
				fixup( input );
				pos += input.length() - prevLength;
				return QValidator::State::Acceptable;
			}
		}

		truncate( input );
		pos += input.length() - prevLength;
		return QValidator::State::Acceptable;
	}

	virtual void fixup( QString &input ) const {
		const QString original = input;
		input.clear();
		for( const QChar &c : original ) {
			if( isLatin( c ) || isFullWidth( c ) ) {
				input.append( c );
			} else if( isHalfWidth( c ) ) {
				input.append( halfToFull( c ) );
			}
		}

		truncate( input );
	}

};

ShiftJisInput::ShiftJisInput( QWidget *parent, int maxBytes ) :
	QLineEdit( parent ),
	m_maxBytes( maxBytes )
{
	QValidator *validator = new ShiftJisValidator( this );
	setValidator( validator );
}

ShiftJisInput::ShiftJisInput( QWidget *parent ) : ShiftJisInput( parent, 0xFFFF ) {}

ShiftJisInput::~ShiftJisInput() {
	delete validator();
	setValidator( nullptr );
}

void ShiftJisInput::setUtf8( const string &str ) {
	QString text( str.c_str() );
	validator()->fixup( text );
	setText( text );
}

string ShiftJisInput::getUtf8() const {
	return text().toStdString();
}

static string covertToShiftJis( const QString &text ) {
	string sjis;
	sjis.reserve( text.length() );

	for( const QChar &c : text ) {
		const auto kana = s_kanaMap.find( c.unicode() );
		if( kana != s_kanaMap.end() ) {
			sjis += kana->second;
		} else switch( c.unicode() ) {
			case 0x00A5: sjis += '\\'; break;
			case 0x203E: sjis += '~'; break;
			default: sjis += (char)c.unicode();
		}
	}

	return sjis;
}

string ShiftJisInput::getShiftJis() const {
	return covertToShiftJis( text() );
}

string ShiftJisInput::toShiftJis( const string &utf8 ) {
	return covertToShiftJis( QString( utf8.c_str() ) );
}
