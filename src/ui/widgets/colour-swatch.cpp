#include "src/ui/widgets/colour-swatch.hpp"
#include "ui_colour-swatch.h"

#include <QColorDialog>
#include <QMouseEvent>
#include <QPainter>

ColourSwatch::ColourSwatch( QWidget *parent ) :
	QFrame( parent ),
	m_ui( new Ui::ColourSwatch ),
	m_colour( ColourRGBA32( 0, 0, 0, 0xFF ) ),
	m_readOnly( false ),
	m_alphaSelect( false )
{
	m_ui->setupUi( this );
}

ColourSwatch::~ColourSwatch() {
	delete m_ui;
}

inline static QColor toQColor( const ColourRGBA32 &colour ) {
	return QColor::fromRgb( colour.red, colour.green, colour.blue, colour.alpha );
}

inline static ColourRGBA32 fromQColor( const QColor &colour ) {
	return ColourRGBA32( colour.red(), colour.green(), colour.blue(), colour.alpha() );
}

void ColourSwatch::paintEvent( QPaintEvent *event ) {
	QPainter canvas( this );
	canvas.fillRect( 0, 0, width(), height(), QBrush( toQColor( m_colour ), isEnabled() ? Qt::SolidPattern : Qt::Dense5Pattern ) );
	canvas.setPen( QPen( QBrush( Qt::black, Qt::SolidPattern ), 1.0, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin ) );
	canvas.drawRect( 0, 0, width() - 1, height() - 1 );
	event->accept();
}

void ColourSwatch::openPicker() {
	QColor colour = QColorDialog::getColor(
		toQColor( m_colour ),
		this,
		"Choose a Colour",
		m_alphaSelect ? QColorDialog::ShowAlphaChannel : (QColorDialog::ColorDialogOption)0
	);

	if( colour.isValid() ) {
		setValue( fromQColor( colour ) );
		emit colourSelected();
	}
}

void ColourSwatch::mousePressEvent( QMouseEvent *event ) {
	event->accept();
	if( m_readOnly || !isEnabled() ) return;
	QMetaObject::invokeMethod( this, "openPicker", Qt::QueuedConnection );
}

void ColourSwatch::setValue( const ColourRGBA32 &colour ) {
	m_colour = colour;
	emit colourChanged();
	update();
}
