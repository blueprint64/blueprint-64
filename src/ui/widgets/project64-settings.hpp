#ifndef SRC_UI_WIDGETS_PROJECT64_SETTINGS_HPP_
#define SRC_UI_WIDGETS_PROJECT64_SETTINGS_HPP_

#include <vector>
#include <QWidget>
#include "src/core/emulator.hpp"

namespace Ui {
	class Project64SettingsWidget;
}

class Project64SettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::Project64SettingsWidget *m_ui;
#ifdef _WIN32
	std::vector<Emulator::Project64::Settings> m_installedVersions;
#endif

	public:
	explicit Project64SettingsWidget( QWidget *parent = nullptr );
	~Project64SettingsWidget();


	public slots:
	void save() const;
	void load();

};



#endif /* SRC_UI_WIDGETS_PROJECT64_SETTINGS_HPP_ */
