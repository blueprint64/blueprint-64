#ifndef SRC_UI_WIDGETS_POINTER_INPUT_HPP_
#define SRC_UI_WIDGETS_POINTER_INPUT_HPP_

#include <QLineEdit>
#include <QValidator>

class PointerInput : public QLineEdit {
	Q_OBJECT

	Q_PROPERTY(bool isVirtual MEMBER m_virtual)

	private:
	bool m_virtual;

	public:
	explicit PointerInput( QWidget *parent = nullptr );
	explicit PointerInput( QWidget *parent, bool isVirtual );
	virtual ~PointerInput();

	inline bool getVirtual() const { return m_virtual; }

	void setValue( uint value );
	inline uint value() const {
		return (uint)std::stoul( text().toStdString(), nullptr, 16 );
	};

};


#endif /* SRC_UI_WIDGETS_POINTER_INPUT_HPP_ */
