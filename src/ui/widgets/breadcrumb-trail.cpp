#include "src/ui/widgets/breadcrumb-trail.hpp"
#include "ui_breadcrumb-trail.h"

#include <QIcon>
#include <QLabel>
#include <iostream>
#include "src/ui/util.hpp"

class Breadcrumb : public QLabel {

	private:
	const QString m_text;
	uint m_id;

	public:
	Breadcrumb( QWidget *parent, const QString &text, uint locationId ) :
		QLabel( text, parent ),
		m_text( text ),
		m_id( locationId ) {}

	~Breadcrumb() {}

	void makeClickable( bool clickable ) {
		static const QString LINK_PREFIX = "<a href=\"#\">";
		static const QString LINK_SUFFIX = "</a>";

		if( clickable ) {
			setText( LINK_PREFIX + m_text + LINK_SUFFIX );
			setTextFormat( Qt::RichText );
			setTextInteractionFlags( Qt::TextBrowserInteraction );
		} else {
			setText( m_text );
			setTextFormat( Qt::PlainText );
			setTextInteractionFlags( Qt::NoTextInteraction );
		}
	}

	uint locationId() const {
		return m_id;
	}

};

BreadcrumbTrailWidget::BreadcrumbTrailWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::BreadcrumbTrailWidget )
{
	m_ui->setupUi( this );
}

BreadcrumbTrailWidget::~BreadcrumbTrailWidget() {
	delete m_ui;
}

void BreadcrumbTrailWidget::clear() {
	UiUtil::clearLayout( m_ui->layout );
}

void BreadcrumbTrailWidget::push( const QString &name, uint locationId ) {
	Breadcrumb *breadcrumb = new Breadcrumb( this, name, locationId );
	if( m_ui->layout->count() > 0 ) {
		static_cast<Breadcrumb*>( m_ui->layout->itemAt( m_ui->layout->count() -1 )->widget() )->makeClickable( true );
		QLabel *arrow = new QLabel( QString(), this );
		arrow->setPixmap( QIcon::fromTheme( "arrow-right" ).pixmap( 16, 16 ) );
		m_ui->layout->addWidget( arrow );
	}
	m_ui->layout->addWidget( breadcrumb );
	QObject::connect( breadcrumb, &QLabel::linkActivated, this, [=](){
		this->jumpTo( breadcrumb->locationId() );
	});
}

static inline void deleteFrom( QHBoxLayout *layout, int index ) {
	while( layout->count() > index ) {
		UiUtil::deleteItemAt( layout, index );
	}
}

void BreadcrumbTrailWidget::jumpTo( uint locationId ) {
	const int numWidgets = m_ui->layout->count();
	for( int i = 0; i < numWidgets; i++ ) {
		if( Breadcrumb* breadcrumb = dynamic_cast<Breadcrumb*>( m_ui->layout->itemAt( i )->widget() ) ) {
			if( breadcrumb->locationId() != locationId ) continue;
			breadcrumb->makeClickable( false );
			deleteFrom( m_ui->layout, ++i );
			emit navigated( locationId );
			break;
		}
	}
}
