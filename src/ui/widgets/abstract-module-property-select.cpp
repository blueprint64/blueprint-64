#include "src/ui/widgets/abstract-module-property-select.hpp"

#include <cassert>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QVBoxLayout>
#include "src/ui/widgets/integer-spinner.hpp"
#include "src/ui/widgets/colour-swatch.hpp"
#include "src/ui/widgets/multi-line-edit.hpp"
#include "src/ui/widgets/pointer-input.hpp"
#include "src/core/blueprint.hpp"

static AsmModulePropertyValue getBool( const QWidget *select ) {
	return static_cast<const QCheckBox*>( select )->isChecked();
}

static void setBool( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<QCheckBox*>( select )->setChecked( std::get<bool>( value ) );
}

static AsmModulePropertyValue getInt( const QWidget *select ) {
	return static_cast<const IntegerSpinner*>( select )->value();
}

static void setInt( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<IntegerSpinner*>( select )->setValue( std::get<int>( value ) );
}

static AsmModulePropertyValue getUInt( const QWidget *select ) {
	return static_cast<const PointerInput*>( select )->value();
}

static void setUInt( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<PointerInput*>( select )->setValue( std::get<uint>( value ) );
}

static AsmModulePropertyValue getFloat( const QWidget *select ) {
	return (float)static_cast<const QDoubleSpinBox*>( select )->value();
}

static void setFloat( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<QDoubleSpinBox*>( select )->setValue( std::get<float>( value ) );
}

static AsmModulePropertyValue getLine( const QWidget *select ) {
	return static_cast<const QLineEdit*>( select )->text().toStdString();
}

static void setLine( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<QLineEdit*>( select )->setText( std::get<string>( value ).c_str() );
}

static AsmModulePropertyValue getText( const QWidget *select ) {
	return static_cast<const QPlainTextEdit*>( select )->toPlainText().toStdString();
}

static void setText( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<QPlainTextEdit*>( select )->setPlainText( std::get<string>( value ).c_str() );
}

static AsmModulePropertyValue getColour( const QWidget *select ) {
	return static_cast<const ColourSwatch*>( select )->value();
}

static void setColour( QWidget *select, const AsmModulePropertyValue &value ) {
	static_cast<ColourSwatch*>( select )->setValue( std::get<ColourRGBA32>( value ) );
}

AbstractModulePropertySelect::AbstractModulePropertySelect(
	QWidget *parent,
	const AsmModuleProperty &property
) : QFrame( parent ),
	m_label( nullptr ),
	m_sublayout( nullptr ),
	m_noSignal( true ),
	m_select( nullptr )
{
	setFrameStyle( QFrame::NoFrame );
	if( !property.description.empty() ) {
		setToolTip( property.description.c_str() );
	}

	QVBoxLayout *layout = new QVBoxLayout( this );
	layout->setSpacing( 0 );
	layout->setContentsMargins( 0, 0, 0, 0 );

	if( property.type != ModulePropertyType::Boolean ) {
		m_label = new QLabel( property.name.c_str(), this );
		layout->addWidget( m_label );
	}

	switch( property.type ) {
		case ModulePropertyType::Integer: {
			IntegerSpinner *spinner = new IntegerSpinner( this );
			spinner->setMinimum( property.minInt );
			spinner->setMaximum( property.maxInt );
			spinner->setValue( std::get<int>( property.defaultValue ) );
			spinner->setDisplayIntegerBase( property.hex ? 16 : 10 );
			if( !property.units.empty() ) {
				spinner->setSuffix( property.units.c_str() );
			}
			m_getter = getInt;
			m_setter = setInt;
			m_select = spinner;
			connect( spinner, SIGNAL( editingFinished() ), this, SLOT( valueChanged() ) );
			m_sublayout = new QHBoxLayout();
			break;
		}
		case ModulePropertyType::Float:
		case ModulePropertyType::Angle: {
			QDoubleSpinBox *spinner = new QDoubleSpinBox( this );
			spinner->setMinimum( property.minFloat );
			spinner->setMaximum( property.maxFloat );
			spinner->setDecimals( property.precision );
			spinner->setValue( std::get<float>( property.defaultValue ) );
			if( !property.units.empty() ) {
				spinner->setSuffix( property.units.c_str() );
			}
			m_getter = getFloat;
			m_setter = setFloat;
			m_select = spinner;
			connect( spinner, SIGNAL( editingFinished() ), this, SLOT( valueChanged() ) );
			m_sublayout = new QHBoxLayout();
			break;
		}
		case ModulePropertyType::String: {
			if( property.multiLine ) {
				MultiLineEdit *textEdit = new MultiLineEdit( this );
				textEdit->setWordWrapMode( QTextOption::WrapAtWordBoundaryOrAnywhere );
				textEdit->setMinimumHeight( 200 );
				textEdit->setMaximumBlockCount( property.maxLength );
				textEdit->setPlainText( std::get<string>( property.defaultValue ).c_str() );
				m_getter = getText;
				m_setter = setText;
				m_select = textEdit;
				connect( textEdit, SIGNAL( editingFinished() ), this, SLOT( valueChanged() ) );
			} else {
				QLineEdit *textEdit = new QLineEdit( this );
				textEdit->setMaxLength( property.maxLength );
				textEdit->setText( std::get<string>( property.defaultValue ).c_str() );
				m_getter = getLine;
				m_setter = setLine;
				m_select = textEdit;
				connect( textEdit, SIGNAL( editingFinished() ), this, SLOT( valueChanged() ) );
			}
			break;
		}
		case ModulePropertyType::Boolean: {
			QCheckBox *checkbox = new QCheckBox( this );
			checkbox->setText( property.name.c_str() );
			checkbox->setChecked( std::get<bool>( property.defaultValue ) );
			m_getter = getBool;
			m_setter = setBool;
			m_select = checkbox;
			connect( checkbox, SIGNAL( toggled(bool) ), this, SLOT( valueChanged() ) );
			break;
		}
		case ModulePropertyType::Colour: {
			ColourSwatch *swatch = new ColourSwatch( this );
			swatch->setValue( std::get<ColourRGBA32>( property.defaultValue ) );
			swatch->setAlphaEnabled( property.hasAlpha );
			swatch->setMinimumHeight( 20 );
			m_getter = getColour;
			m_setter = setColour;
			m_select = swatch;
			connect( swatch, SIGNAL( colourSelected() ), this, SLOT( valueChanged() ) );
			m_sublayout = new QHBoxLayout();
			break;
		}
		case ModulePropertyType::Pointer: {
			PointerInput *input = new PointerInput( this, property.isVirtual );
			input->setValue( std::get<uint>( property.defaultValue ) );
			m_getter = getUInt;
			m_setter = setUInt;
			m_select = input;
			connect( input, SIGNAL( editingFinished() ), this, SLOT( valueChanged() ) );
			break;
		}
		default: assert( false );
	}

	if( m_label != nullptr ) {
		m_label->setBuddy( m_select );
	}

	if( m_sublayout != nullptr ) {
		m_select->setSizePolicy( QSizePolicy::MinimumExpanding, m_select->sizePolicy().horizontalPolicy() );
		m_sublayout->setSpacing( 0 );
		m_sublayout->setContentsMargins( 0, 0, 0, 0 );
		m_sublayout->addWidget( m_select );
		static_cast<QHBoxLayout*>( m_sublayout )->addStretch();
		layout->addLayout( m_sublayout );
	} else {
		layout->addWidget( m_select );
	}

	m_noSignal = false;
}

AbstractModulePropertySelect::~AbstractModulePropertySelect() {
	m_select->deleteLater();
	if( m_label != nullptr ) {
		m_label->deleteLater();
	}
	if( m_sublayout != nullptr ) {
		m_sublayout->deleteLater();
	}
	layout()->deleteLater();
}
