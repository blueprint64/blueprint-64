#include "src/ui/widgets/retroarch-settings.hpp"
#include "ui_retroarch-settings.h"

#include <QStandardItemModel>
#include "src/core/storage.hpp"

using namespace Emulator;

RetroarchSettingsWidget::RetroarchSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::RetroarchSettingsWidget )
{
	m_ui->setupUi( this );
#ifdef _WIN32
	m_ui->flatpakCheckbox->setVisible( false );
#else
	m_ui->exePathLabel->setVisible( false );
	m_ui->windowsExePath->setVisible( false );
#endif
}

RetroarchSettingsWidget::~RetroarchSettingsWidget() {
	delete m_ui;
}

void RetroarchSettingsWidget::load() {
	const bool hasFlatpakInstall = RetroArch::hasFlatpakInstall();
	m_ui->flatpakCheckbox->setEnabled( hasFlatpakInstall );

	const RetroArch::Settings &config = AppPreferences::current().emuConfig.retroSettings;
	m_ui->emulatorCore->setCurrentIndex( (int)config.core );
	m_ui->cpuEmulation->setCurrentIndex( (int)config.cpuCore );
	m_ui->resolutionScale->setValue( (int)config.resolutionScale );
	m_ui->fullscreen->setChecked( config.fullscreen );
	m_ui->vsync->setChecked( config.vsync );
	m_ui->fullspeed->setChecked( config.fullspeed );
	m_ui->gfxPlugin->setCurrentIndex( (int)config.gfxPlugin );
	m_ui->overclockVI->setChecked( config.overclockVI );
#ifdef _WIN32
	m_ui->windowsExePath->setText( config.exePath.c_str() );
#else
	m_ui->flatpakCheckbox->setChecked( config.useFlatpakInstall && hasFlatpakInstall );
#endif

	coreChanged( (int)config.core );
}

void RetroarchSettingsWidget::save() const {
	RetroArch::Settings &config = AppPreferences::current().emuConfig.retroSettings;
	config.core = (RetroArch::EmulatorCore)m_ui->emulatorCore->currentIndex();
	config.cpuCore = (CpuEmulation)m_ui->cpuEmulation->currentIndex();
	config.resolutionScale = (ubyte)m_ui->resolutionScale->value();
	config.fullscreen = m_ui->fullscreen->isChecked();
	config.vsync = m_ui->vsync->isChecked();
	config.fullspeed = m_ui->fullspeed->isChecked();
	config.gfxPlugin = (RetroArch::GraphicsPlugin)m_ui->gfxPlugin->currentIndex();
	config.overclockVI = m_ui->overclockVI->isChecked();
#ifdef _WIN32
	config.exePath = m_ui->windowsExePath->text().toStdString();
#else
	config.useFlatpakInstall = m_ui->flatpakCheckbox->isChecked();
#endif
}

void RetroarchSettingsWidget::coreChanged( int coreIndex ) {
	QStandardItemModel *model = dynamic_cast<QStandardItemModel*>( m_ui->gfxPlugin->model() );
	model->item( 3, 0 )->setEnabled( coreIndex == 0 );
	model->item( 4, 0 )->setEnabled( coreIndex == 0 );
	if( coreIndex > 0 && m_ui->gfxPlugin->currentIndex() > 2 ) {
		m_ui->gfxPlugin->setCurrentIndex( 2 );
	}
}
