#ifndef SRC_UI_WIDGETS_MANAGE_SHADING_DIALOG_HPP_
#define SRC_UI_WIDGETS_MANAGE_SHADING_DIALOG_HPP_

#include <QDialog>
#include <unordered_set>
#include "src/core/enums.hpp"

namespace Ui {
	class ManageShadingDialog;
}

class ManageShadingDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ManageShadingDialog *m_ui;
	HashSet<uint> m_usedShades;
	LevelId m_levelId;
	bool m_initializing;

	public slots:
	void shadeChanged();
	void shadeSelected();
	void shadeDeleted();
	void shadeRenamed();
	void addShade();

	void open( LevelId levelId );

	public:
	explicit ManageShadingDialog( QWidget *parent = nullptr );
	~ManageShadingDialog();

	uint getSelectedShadeId() const;

};



#endif /* SRC_UI_WIDGETS_MANAGE_SHADING_DIALOG_HPP_ */
