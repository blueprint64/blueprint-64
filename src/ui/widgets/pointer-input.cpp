#include "./src/ui/widgets/pointer-input.hpp"

#include <cstdio>
#include <regex>

static const std::regex s_formattedRegex(
	"0x[0-9A-Z]{8}",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static const std::regex s_formattedVirtualRegex(
	"0x80[0-9A-Z]{6}",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static const std::regex s_validHexRegex(
	"(?:0[xX])?[0-9a-zA-Z]*",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

class PointerValidator : public QValidator {

	private:
	inline bool isVirtual() const {
		return static_cast<const PointerInput*>( parent() )->getVirtual();
	}

	public:
	explicit PointerValidator( QWidget *parent ) : QValidator( parent ) {}
	virtual ~PointerValidator() {}

	virtual QValidator::State validate( QString &input, [[maybe_unused]] int &pos ) const override {
		if( std::regex_match( input.toStdString(), isVirtual() ? s_formattedVirtualRegex : s_formattedRegex ) ) {
			return QValidator::State::Acceptable;
		} else if( std::regex_match( input.toStdString(), s_validHexRegex ) ) {
			return QValidator::State::Intermediate;
		} else {
			return QValidator::State::Invalid;
		}
	}

	virtual void fixup( QString &input ) const {
		try {
			unsigned long parsedValue = std::stoul( input.toStdString(), nullptr, 16 );
			if constexpr( sizeof( unsigned long ) > sizeof( unsigned int ) ) {
				parsedValue &= 0xFFFFFFFFul;
			}
			if( isVirtual() ) {
				parsedValue = 0x80000000ul | (parsedValue & 0xFFFFFFul);
			}
			char hexString[11];
			std::sprintf( hexString, "0x%08X", (uint)parsedValue );
			input = hexString;
		} catch( ... ) {
			input = "0x00000000";
		}
	}

};

PointerInput::PointerInput( QWidget *parent, bool isVirtual ) :
	QLineEdit( parent ),
	m_virtual( isVirtual )
{
	setText( "0x00000000" );
	setValidator( new PointerValidator( this ) );
}

PointerInput::PointerInput( QWidget *parent ) :
	PointerInput( parent, false ) {}

PointerInput::~PointerInput() {
	delete validator();
}

void PointerInput::setValue( uint value ) {
	char hexString[11];
	std::sprintf( hexString, "0x%08X", value );
	setText( hexString );
}
