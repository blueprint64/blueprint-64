#include "src/ui/widgets/integer-spinner.hpp"

#include <cstdio>
#include <cmath>
#include <string>

#include <iostream>

int IntegerSpinner::maxDigits() const {
	const int base = displayIntegerBase();
	long long val = std::max( abs((long long)minimum()), abs((long long)maximum()) );

	int d = 1;
	while( val >= base ) {
		val /= base;
		d++;
	}

	return d;
}

QString IntegerSpinner::stripped( const QString &text ) const {
	const bool negative = text.startsWith( '-' );

	const QString _prefix = negative ? (QString( "-" ) + prefix() ) : prefix();
	const QString _suffix = suffix();
	QString middle = text;

	if( _prefix.size() > 0 && middle.startsWith( _prefix ) ) {
		middle = middle.mid( _prefix.size() );
	}

	if( _suffix.size() > 0 && middle.endsWith( _suffix ) ) {
		middle.chop( _suffix.size() );
	}

	return negative ? (QString( "-" ) + middle) : middle;
}

QString IntegerSpinner::textFromValue( int value ) const {
	if( displayIntegerBase() == 16 ) {
		const int digits = maxDigits();
		char buffer[12];
		if( value < 0 ) {
			std::sprintf( buffer, "-0x%0*X", digits, -value );
		} else {
			std::sprintf( buffer, "0x%0*X", digits, value );
		}
		return prefix() + QString( buffer ) + suffix();
	}
	return QSpinBox::textFromValue( value );
}

int IntegerSpinner::valueFromText( const QString &text ) const {
	return std::stoi( stripped( text ).toStdString(), nullptr, displayIntegerBase() );
}

void IntegerSpinner::fixup( QString &input ) const {
	try {
		const long long inputValue = std::stoll( stripped( input ).toStdString(), nullptr, displayIntegerBase() );
		if( inputValue > (long long)maximum() ) {
			input = textFromValue( maximum() );
		} else if( inputValue < (long long)minimum() ) {
			input = textFromValue( minimum() );
		} else {
			input = textFromValue( (int)inputValue );
		}
	} catch( ... ) {
		input = textFromValue( value() );
	}
}

QValidator::State IntegerSpinner::validate( QString &text, int &pos ) const {
	const QString middle = stripped( text );
	if( middle.isNull() || middle.isEmpty() ) {
		pos = 0;
		return QValidator::Intermediate;
	}

	if( middle == "-" && minimum() < 0 ) {
		return QValidator::Intermediate;
	}

	QValidator::State state = QValidator::Acceptable;
	try {
		long long inputValue = std::stoll( middle.toStdString(), nullptr, displayIntegerBase() );
		if( minimum() >= 0 && inputValue < 0 ) {
			inputValue = -inputValue;
		}

		const long long maxIntermediate = std::pow( displayIntegerBase(), maxDigits() ) - 1;
		if( std::abs( inputValue ) > maxIntermediate ) {
			state = QValidator::Invalid;
			fixup( text );
		} else if( inputValue < minimum() || inputValue > maximum() ) {
			state = QValidator::Intermediate;
		} else {
			state = QValidator::Acceptable;
		}
	} catch( ... ) {
		text = value();
		state = QValidator::Invalid;
	}

	if( pos > (int)text.size() ) {
		pos = (int)text.size();
	}

	return state;
}
