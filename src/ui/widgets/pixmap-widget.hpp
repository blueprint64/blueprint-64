#ifndef SRC_UI_WIDGETS_PIXMAP_WIDGET_HPP_
#define SRC_UI_WIDGETS_PIXMAP_WIDGET_HPP_

#include <QFrame>
#include <QPalette>
#include <QPixmap>

class PixmapWidget : public QFrame {
	Q_OBJECT

	private:
	QPixmap m_pixmap;

	void forceResize( int width, int height );

	public:
	explicit PixmapWidget( QWidget *parent ) : QFrame( parent ) {
		clearPixmap();
	}
	~PixmapWidget() {}

	void setPixmap( const QPixmap &pixmap, bool resize );
	void setPixmap( QPixmap &&pixmap, bool resize );
	void clearPixmap( int width, int height );
	void clearPixmap();

	protected:
	void paintEvent( QPaintEvent *paintEvent ) override;

};


#endif /* SRC_UI_WIDGETS_PIXMAP_WIDGET_HPP_ */
