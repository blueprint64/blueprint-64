#include "src/ui/widgets/manage-shading-dialog.hpp"
#include "ui_manage-shading-dialog.h"

#include <QPainter>
#include <cassert>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/random.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/json.hpp"

ManageShadingDialog::ManageShadingDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ManageShadingDialog ),
	m_levelId( LevelId::INVALID ),
	m_initializing( false )
{
	m_ui->setupUi( this );
}

ManageShadingDialog::~ManageShadingDialog() {
	delete m_ui;
}

static QIcon makeIcon( const ShadingSpecs &shades ) {
	const QColor dark( shades.ambient.red, shades.ambient.green, shades.ambient.blue );
	const QColor light( shades.diffuse.red, shades.diffuse.green, shades.diffuse.blue );

	QPixmap img( 32, 16 );
	img.fill( dark );
	QPainter( &img ).fillRect( 16, 0, 16, 16, light );

	return QIcon( img );
}

void ManageShadingDialog::open( LevelId levelId ) {
	m_initializing = true;
	m_levelId = levelId;

	assert( Blueprint::current()->hasLevel( levelId ) );
	m_ui->shadeList->clear();
	for( const auto &i : Blueprint::current()->levels().find( levelId )->second.shadingValues ) {
		const ShadingSpecs &shade = i.second;
		m_ui->shadeList->addItem( shade.name.c_str() );
		QListWidgetItem *item = m_ui->shadeList->item( m_ui->shadeList->count() - 1 );
		item->setIcon( makeIcon( shade ) );
		item->setData( Qt::UserRole, i.first );
	}

	m_usedShades.clear();
	fs::path levelPath = Blueprint::current()->getLevelPath( m_levelId );
	if( fs::exists( levelPath ) ) {
		for( const auto &f : fs::recursive_directory_iterator( levelPath ) ) {
			if( f.status().type() != fs::file_type::regular ) continue;
			if( f.path().extension().u8string() != ".json" ) continue;

			try {
				FileReadStream jsonFile( f.path().u8string() );
				const Json &json = Json::parse( jsonFile );
				if( !json["shade"].hasValue() ) continue;
				m_usedShades.insert( json["shade"].get<uint>() );
			} catch( ... ) {
				std::cerr << "Error reading material JSON file" << std::endl;
			}

		}
	}

	m_ui->shadeList->setCurrentRow( 0 );
	m_initializing = false;

	shadeSelected();
	QDialog::open();
}

void ManageShadingDialog::shadeChanged() {
	if( m_initializing ) return;

	m_initializing = true;
	assert( Blueprint::current()->hasLevel( m_levelId ) );
	const uint shadeId = m_ui->shadeList->currentItem()->data( Qt::UserRole ).toUInt();
	ShadingSpecs &shade = Blueprint::currentEditable()->levels()[m_levelId].shadingValues[shadeId];

	shade.name = m_ui->shadeName->text().toStdString();
	shade.ambient = (ColourRGB24)m_ui->darkShade->value();
	shade.diffuse = (ColourRGB24)m_ui->lightShade->value();
	shade.asmRef = m_ui->asmRefCheckbox->isChecked();
	shade.lightAngleYaw = (short)m_ui->angleYaw->value();
	shade.lightAnglePitch = (short)m_ui->anglePitch->value();

	m_ui->shadeList->currentItem()->setIcon( makeIcon( shade ) );
	m_initializing = false;
}

void ManageShadingDialog::shadeSelected() {
	if( m_initializing ) return;

	assert( Blueprint::current()->hasLevel( m_levelId ) );
	const std::map<uint,ShadingSpecs> &shadeMap = Blueprint::current()->levels().find( m_levelId )->second.shadingValues;
	const uint shadeId = m_ui->shadeList->currentItem()->data( Qt::UserRole ).toUInt();
	assert( shadeMap.find( shadeId ) != shadeMap.end() );
	const ShadingSpecs &shade = shadeMap.find( shadeId )->second;

	m_ui->shadeName->setText( shade.name.c_str() );
	m_ui->lightShade->setValue( shade.diffuse );
	m_ui->darkShade->setValue( shade.ambient );
	m_ui->asmRefCheckbox->setChecked( shade.asmRef );
	m_ui->angleYaw->setValue( shade.lightAngleYaw );
	m_ui->anglePitch->setValue( shade.lightAnglePitch );
	if( shadeId == 0 ) {
		m_ui->deleteButton->setEnabled( false );
		m_ui->noDeleteReason->setVisible( true );
		m_ui->noDeleteReason->setText( "Default shade." );
	} else if( m_usedShades.count( shadeId ) > 0 ) {
		m_ui->deleteButton->setEnabled( false );
		m_ui->noDeleteReason->setVisible( true );
		m_ui->noDeleteReason->setText( "Shade is in use." );
	} else {
		m_ui->deleteButton->setEnabled( true );
		m_ui->noDeleteReason->setVisible( false );
	}

}

void ManageShadingDialog::shadeDeleted() {
	m_initializing = true;
	const uint shadeId = m_ui->shadeList->currentItem()->data( Qt::UserRole ).toUInt();
	Blueprint::currentEditable()->levels()[m_levelId].shadingValues.erase( shadeId );
	delete m_ui->shadeList->takeItem( m_ui->shadeList->currentRow() );
	m_ui->shadeList->setCurrentRow( 0 );
	m_initializing = false;
	shadeSelected();
}

void ManageShadingDialog::shadeRenamed() {
	m_ui->shadeList->currentItem()->setText( m_ui->shadeName->text() );
}

void ManageShadingDialog::addShade() {
	m_initializing = true;
	std::map<uint,ShadingSpecs> &shadeMap = Blueprint::currentEditable()->levels()[m_levelId].shadingValues;

	uint shadeId;
	do {
		shadeId = Pseudorandom::getUInt();
	} while( shadeId == 0 || shadeMap.find( shadeId ) != shadeMap.end() );

	shadeMap[shadeId] = shadeMap[0];
	shadeMap[shadeId].id = shadeId;
	shadeMap[shadeId].name = "Untitled Shade";

	m_ui->shadeList->addItem( "Untitled Shade" );
	QListWidgetItem *item = m_ui->shadeList->item( m_ui->shadeList->count() - 1 );
	item->setIcon( makeIcon( shadeMap[shadeId] ) );
	item->setData( Qt::UserRole, shadeId );
	m_ui->shadeList->setCurrentItem( item );
	m_initializing = false;

	shadeSelected();
	m_ui->shadeName->setFocus();
	m_ui->shadeName->selectAll();
}

uint ManageShadingDialog::getSelectedShadeId() const {
	return m_ui->shadeList->currentItem()->data( Qt::UserRole ).toUInt();
}
