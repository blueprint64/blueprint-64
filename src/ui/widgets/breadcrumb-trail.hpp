#ifndef SRC_UI_WIDGETS_BREADCRUMB_TRAIL_HPP_
#define SRC_UI_WIDGETS_BREADCRUMB_TRAIL_HPP_

#include <QWidget>

namespace Ui {
	class BreadcrumbTrailWidget;
}

class BreadcrumbTrailWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::BreadcrumbTrailWidget *m_ui;

	public:
	explicit BreadcrumbTrailWidget( QWidget *parent = nullptr );
	~BreadcrumbTrailWidget();

	void clear();
	void push( const QString &name, uint locationId );

	public slots:
	void jumpTo( uint locationId );

	signals:
	void navigated( uint locationId );

};

#endif /* SRC_UI_WIDGETS_BREADCRUMB_TRAIL_HPP_ */
