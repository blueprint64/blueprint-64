#ifndef SRC_UI_WIDGETS_LEVEL_SELECT_HPP_
#define SRC_UI_WIDGETS_LEVEL_SELECT_HPP_

#include <QComboBox>
#include "src/core/enums.hpp"

class LevelSelect : public QComboBox {
	Q_OBJECT

	Q_PROPERTY(bool includeEndScreen MEMBER m_includeEndScreen)

	private:
	bool m_includeEndScreen;

	public:
	LevelSelect( QWidget *parent = nullptr );
	inline ~LevelSelect() {}

	LevelId selectedLevel() const;
	void setLevel( LevelId level );

	protected:
	virtual void showEvent( QShowEvent *event );

	private slots:
	void emitlevelSelected();

	public slots:
	void updateLevelNames();

	signals:
	void levelSelected( int );

};



#endif /* SRC_UI_WIDGETS_LEVEL_SELECT_HPP_ */
