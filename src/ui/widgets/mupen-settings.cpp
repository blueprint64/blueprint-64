#include "src/ui/widgets/mupen-settings.hpp"
#include "ui_mupen-settings.h"

#include <QIcon>
#include "src/polyfill/filesystem.hpp"
#include "src/core/storage.hpp"

using namespace Emulator;

MupenSettingsWidget::MupenSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::MupenSettingsWidget )
{
	m_ui->setupUi( this );
	m_ui->badPathIcon->setText( QString() );
	m_ui->badPathIcon->setPixmap( QIcon::fromTheme( "emblem-important" ).pixmap( 32, 32 ) );
	m_ui->badPathIcon->setVisible( false );
#ifndef _WIN32
	m_ui->exePathLabel->setVisible( false );
	m_ui->windowsExePath->setVisible( false );
#endif
}

MupenSettingsWidget::~MupenSettingsWidget() {
	delete m_ui;
}

void MupenSettingsWidget::load() {
	const Mupen64Plus::Settings &config = AppPreferences::current().emuConfig.mupenSettings;
	m_ui->pluginPath->setText( config.pluginDirPath.c_str() );
	m_ui->cpuEmulation->setCurrentIndex( (int)config.cpuCore );
	m_ui->resolutionScale->setValue( (int)config.resolutionScale );
	m_ui->fullscreen->setChecked( config.fullscreen );
	m_ui->onScreenDisplay->setChecked( config.onScreenDisplay );
#ifdef _WIN32
	m_ui->windowsExePath->setText( config.exePath.c_str() );
#endif
	pluginPathChanged();
}

void MupenSettingsWidget::save() const {
	Mupen64Plus::Settings &config = AppPreferences::current().emuConfig.mupenSettings;
	config.pluginDirPath = m_ui->pluginPath->text().toStdString();
	config.cpuCore = (CpuEmulation)m_ui->cpuEmulation->currentIndex();
	config.gfxPlugin = m_ui->gfxPlugin->currentText().toStdString();
	config.rspPlugin = m_ui->rspPlugin->currentText().toStdString();
	config.resolutionScale = (ubyte)m_ui->resolutionScale->value();
	config.fullscreen = m_ui->fullscreen->isChecked();
	config.onScreenDisplay = m_ui->onScreenDisplay->isChecked();
#ifdef _WIN32
	config.exePath = m_ui->windowsExePath->text().toStdString();
#endif
}

static void updateComboBox( QComboBox &comboBox, const std::vector<string> &options, const string &fallbackOption ) {
	const string prevOption = comboBox.currentText().isNull() ? fallbackOption : comboBox.currentText().toStdString();
	comboBox.clear();

	if( options.empty() ) {
		comboBox.addItem( prevOption.c_str() );
		comboBox.setCurrentIndex( 0 );
		comboBox.setEnabled( false );
		return;
	}

	int index = 0;
	for( const string &option : options ) {
		comboBox.addItem( option.c_str() );
		if( option == prevOption ) {
			index = comboBox.count() - 1;
		}
	}

	comboBox.setCurrentIndex( index );
	comboBox.setEnabled( true );
}

void MupenSettingsWidget::pluginPathChanged() {
	bool directoryExists = true;
	m_ui->badPathIcon->setVisible( false );
	m_ui->badPathIcon->setToolTip( QString() );

	fs::path pluginPath = fs::to_path( m_ui->pluginPath->text().toStdString() );
	if( !fs::exists( pluginPath ) || fs::status( pluginPath ).type() == fs::file_type::regular ) {
		pluginPath = pluginPath.parent_path();
		if( fs::exists( pluginPath ) ) {
			m_ui->pluginPath->setText( pluginPath.u8string().c_str() );
		} else {
			directoryExists = false;
			m_ui->badPathIcon->setVisible( true );
			m_ui->badPathIcon->setToolTip( "Plugin directory not found." );
		}
	}

	std::vector<string> gfxPlugins;
	std::vector<string> rspPlugins;
	if( directoryExists ) {
		gfxPlugins = std::move( Mupen64Plus::getGfxPlugins( pluginPath.u8string() ) );
		rspPlugins = std::move( Mupen64Plus::getRspPlugins( pluginPath.u8string() ) );

		if( rspPlugins.empty() ) {
			m_ui->badPathIcon->setVisible( true );
			m_ui->badPathIcon->setToolTip( "No RSP plugins found in directory." );
		}

		if( gfxPlugins.empty() ) {
			m_ui->badPathIcon->setVisible( true );
			m_ui->badPathIcon->setToolTip( "No graphics plugins found in directory." );
		}
	}

	const Mupen64Plus::Settings &savedConfig = AppPreferences::current().emuConfig.mupenSettings;
	updateComboBox( *m_ui->rspPlugin, rspPlugins, savedConfig.rspPlugin );
	updateComboBox( *m_ui->gfxPlugin, gfxPlugins, savedConfig.gfxPlugin );
}
