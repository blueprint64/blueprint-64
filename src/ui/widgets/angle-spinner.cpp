#include "src/ui/widgets/angle-spinner.hpp"

static inline short clampHex( int value ) {
	return (short)(((value + 0x8000) % 0x10000) - 0x8000);
}

static constexpr double DEGREES_PER_UNIT = 360.0 / (double)0x10000;

static inline double hexToDegrees( short units ) {
	return (double)units * DEGREES_PER_UNIT;
}

static inline short degreesToHex( double degrees ) {
	return clampHex( degrees / DEGREES_PER_UNIT );
}

AngleSpinner::AngleSpinner(
	QWidget *parent,
	short value,
	bool hexMode
) : QStackedWidget( parent ),
	m_hexSpinner( new IntegerSpinner( this ) ),
	m_degreeSpinner( new QDoubleSpinBox( this ) ),
	m_ignoreEvents( false )
{
	addWidget( m_degreeSpinner );
	addWidget( m_hexSpinner );

	m_hexSpinner->setMinimum( -0xFFFF );
	m_hexSpinner->setMaximum( 0xFFFF );
	m_hexSpinner->setValue( value );
	m_hexSpinner->setDisplayIntegerBase( 16 );

	m_degreeSpinner->setMinimum( DEGREES_PER_UNIT - 360.0 );
	m_degreeSpinner->setMaximum( 360.0 - DEGREES_PER_UNIT );
	m_degreeSpinner->setValue( hexToDegrees( value ) );
	m_degreeSpinner->setDecimals( 3 );
	m_degreeSpinner->setSuffix( "°" );

	setCurrentIndex( hexMode ? 1 : 0 );

	connect( m_hexSpinner, SIGNAL( valueChanged( int ) ), this, SLOT( onHexValueChanged( int ) ) );
	connect( m_degreeSpinner, SIGNAL( valueChanged( double ) ), this, SLOT( onDegreeValueChanged( double ) ) );
	connect( m_hexSpinner, SIGNAL( editingFinished() ), this, SLOT( onBlur() ) );
	connect( m_degreeSpinner, SIGNAL( editingFinished() ), this, SLOT( onBlur() ) );
}

AngleSpinner::AngleSpinner( QWidget *parent ) :
	AngleSpinner( parent, 0, false ) {}

short AngleSpinner::value() const {
	return clampHex( m_hexSpinner->value() );
}

bool AngleSpinner::isHexMode() const {
	return currentIndex() > 0;
}

void AngleSpinner::setValue( short value ) {
	m_hexSpinner->setValue( value );
	m_ignoreEvents = true;
	m_degreeSpinner->setValue( hexToDegrees( value ) );
	m_ignoreEvents = false;
}

void AngleSpinner::setHexMode( bool hexMode ) {
	setCurrentIndex( hexMode ? 1 : 0 );
}

void AngleSpinner::onHexValueChanged( int value ) {
	if( m_ignoreEvents ) return;
	const short newValue = clampHex( value );
	m_ignoreEvents = true;
	m_degreeSpinner->setValue( hexToDegrees( value ) );
	m_ignoreEvents = false;
	emit valueChanged( newValue );
}

void AngleSpinner::onDegreeValueChanged( double value ) {
	if( m_ignoreEvents ) return;
	const short newValue = degreesToHex( value );
	m_ignoreEvents = true;
	m_hexSpinner->setValue( newValue );
	m_ignoreEvents = false;
	emit valueChanged( newValue );
}

void AngleSpinner::onBlur() {
	const short newValue = value();
	m_ignoreEvents = true;
	m_hexSpinner->setValue( newValue );
	m_degreeSpinner->setValue( hexToDegrees( newValue ) );
	m_ignoreEvents = false;
	emit editingFinished( newValue );
}
