#include "src/ui/widgets/camera-module-property-select.hpp"

#include <cassert>
#include "src/core/blueprint.hpp"

CameraModulePropertySelect::CameraModulePropertySelect(
	QWidget *parent,
	std::map<string,CameraModulePropertyValue> *moduleParams,
	const AsmModuleProperty &property
) : AbstractModulePropertySelect( parent, property ),
	m_moduleParams( moduleParams ),
	m_type( property.type ),
	m_propertyName( property.name )
{
	if( moduleParams->count( property.name ) > 0 ) {
		const CameraModulePropertyValue &currentValue = moduleParams->at( property.name );
		if( property.type != currentValue.type ) {
			valueChanged();
			return;
		}
		AsmModulePropertyValue value;
		switch( currentValue.type ) {
			case ModulePropertyType::Integer:
				value = currentValue.signedValue; break;
			case ModulePropertyType::Pointer:
				value = currentValue.unsignedValue; break;
			case ModulePropertyType::Float:
			case ModulePropertyType::Angle:
				value = currentValue.floatValue; break;
			case ModulePropertyType::Boolean:
				value = currentValue.booleanValue; break;
			default:
				assert( false );
		}
		m_setter( m_select, value );
	} else {
		valueChanged();
	}
}

void CameraModulePropertySelect::valueChanged() const {
	const AsmModulePropertyValue &value = m_getter( m_select );
	CameraModulePropertyValue result;
	result.type = m_type;
	if( std::holds_alternative<int>( value ) ) {
		result.signedValue = std::get<int>( value );
	} else if( std::holds_alternative<uint>( value ) ) {
		result.unsignedValue = std::get<uint>( value );
	} else if( std::holds_alternative<float>( value ) ) {
		result.floatValue = std::get<float>( value );
	} else if( std::holds_alternative<bool>( value ) ) {
		result.booleanValue = std::get<bool>( value );
	} else assert( false );

	(*m_moduleParams)[m_propertyName] = result;
}
