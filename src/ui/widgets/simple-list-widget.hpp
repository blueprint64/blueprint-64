#ifndef SRC_UI_WIDGETS_SIMPLE_LIST_WIDGET_HPP_
#define SRC_UI_WIDGETS_SIMPLE_LIST_WIDGET_HPP_

#include <QListWidget>
#include <string>

class SimpleListWidget : public QListWidget {
	Q_OBJECT

	Q_PROPERTY(bool canDelete MEMBER m_hasDeleteAction)
	Q_PROPERTY(bool canEdit MEMBER m_hasEditAction)
	Q_PROPERTY(bool canMove MEMBER m_hasMoveAction)

	private:
	bool m_hasDeleteAction;
	bool m_hasEditAction;
	bool m_hasMoveAction;

	public:
	explicit SimpleListWidget( QWidget *parent = nullptr );
	~SimpleListWidget() {}

	template<typename T> inline QListWidgetItem *addValueItem( const std::string &name, const T &value ) {
		QListWidgetItem *item = new QListWidgetItem( name.c_str() );
		item->setData( Qt::UserRole, QVariant::fromValue<T>( value ) );
		addItem( item );
		return item;
	}

	template<typename T> inline QListWidgetItem *addValueItem( const std::string &name, const T &value, const QIcon &icon ) {
		QListWidgetItem *item = new QListWidgetItem( name.c_str() );
		item->setData( Qt::UserRole, QVariant::fromValue<T>( value ) );
		item->setIcon( icon );
		addItem( item );
		return item;
	}

	template<typename T> inline T currentData() const { return currentItem()->data( Qt::UserRole ).value<T>(); }

	protected:
	virtual void mouseDoubleClickEvent( QMouseEvent *event ) override;

	private slots:
	void onRightClick( const QPoint &point );

	signals:
	void itemDelete();
	void itemEdit();
	void itemMove();

};



#endif /* SRC_UI_WIDGETS_SIMPLE_LIST_WIDGET_HPP_ */
