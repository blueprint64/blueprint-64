#include "src/ui/widgets/dynamic-grid-widget.hpp"

#include <QLayout>
#include <vector>
#include <algorithm>

class FlowGridLayout : public QLayout {

	private:
	std::vector<QLayoutItem*> m_items;
	static constexpr int PADDING = 6;

	int getCellSize() const {
		int size = 0;
		for( const QLayoutItem* i : m_items ) {
			const QSize itemSize = i->minimumSize();
			size = std::max({ size, itemSize.width(), itemSize.height() });
		}
		return size;
	}

	public:
	explicit FlowGridLayout() : QLayout() {
		setContentsMargins( 0, 0, 0, 0 );
	}

	~FlowGridLayout() {
		clear();
	}

	void clear() {
		for( QLayoutItem *i : m_items ) {
			i->widget()->deleteLater();
			delete i;
		}
		m_items.clear();
	}

	void addItem( QLayoutItem *item ) override {
		m_items.push_back( item );
	}

	QLayoutItem *itemAt( int index ) const override {
		return index < (int)m_items.size() ? m_items.at( index ) : nullptr;
	}

	QLayoutItem *takeAt( int index ) override {
		QLayoutItem *item = itemAt( index );
		if( item != nullptr ) {
			m_items.erase( m_items.begin() + index );
		}
		return item;
	}

	int count() const override {
		return (int)m_items.size();
	}

	Qt::Orientations expandingDirections() const override {
		return Qt::Horizontal | Qt::Vertical;
	}

	QSize minimumSize() const override {
		const int s = getCellSize();
		return QSize( s, s );
	}

	QSize sizeHint() const override {
		return minimumSize();
	}

	bool hasHeightForWidth() const override {
		return true;
	}

	int heightForWidth( int width ) const override {
		const int cellSize = getCellSize();
		if( cellSize == 0 ) return 0;

		const int itemsPerRow = (width + PADDING) / (cellSize + PADDING);
		if( itemsPerRow == 0 ) {
			return m_items.empty() ? 0 : width;
		}

		const int numRows = ( count() + itemsPerRow - 1 ) / itemsPerRow;
		return (numRows * (cellSize + PADDING)) - PADDING;
	}

	void setGeometry(const QRect &rect) override {
		QLayout::setGeometry( rect );
		const int cellSize = getCellSize();
		const int x0 = rect.x();

		int x = x0;
		int y = rect.y();
		for( QLayoutItem *item : m_items ) {
			if( x + cellSize > rect.right() ) {
				x = x0;
				y += cellSize + PADDING;
			}

			item->setGeometry( QRect( x, y, cellSize, cellSize ) );
			x += cellSize + PADDING;
		}
	}

};

DynamicGridWidget::DynamicGridWidget( QWidget *parent ) :
	QFrame( parent )
{
	setLayout( new FlowGridLayout );
}

DynamicGridWidget::~DynamicGridWidget() {
	layout()->deleteLater();
}

int DynamicGridWidget::count() const {
	return layout()->count();
}

QWidget *DynamicGridWidget::widgetAt( int pos ) {
	return layout()->itemAt( pos )->widget();
}

void DynamicGridWidget::deleteAll() {
	static_cast<FlowGridLayout*>( layout() )->clear();
}

void DynamicGridWidget::addWidget( QWidget *widget ) {
	layout()->addWidget( widget );
	widget->setParent( this );
}

QWidget *DynamicGridWidget::takeWidget( QWidget *widget ) {
	layout()->removeWidget( widget );
	return widget;
}
