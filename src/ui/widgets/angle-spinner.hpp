#ifndef SRC_UI_WIDGETS_ANGLE_SPINNER_HPP_
#define SRC_UI_WIDGETS_ANGLE_SPINNER_HPP_

#include <QStackedWidget>
#include <QDoubleSpinBox>
#include "src/ui/widgets/integer-spinner.hpp"

class AngleSpinner : public QStackedWidget {
	Q_OBJECT

	private:
	IntegerSpinner *const m_hexSpinner;
	QDoubleSpinBox *const m_degreeSpinner;
	bool m_ignoreEvents;

	public:
	explicit AngleSpinner( QWidget *parent = nullptr );
	AngleSpinner( QWidget *parent, short value, bool hexMode = false );

	inline ~AngleSpinner() {
		delete m_hexSpinner;
		delete m_degreeSpinner;
	}

	short value() const;
	bool isHexMode() const;

	private slots:
	void onHexValueChanged( int value );
	void onDegreeValueChanged( double value );
	void onBlur();

	public slots:
	void setValue( short value );
	void setHexMode( bool hexMode );

	signals:
	void editingFinished( short value );
	void valueChanged( short value );

};



#endif /* SRC_UI_WIDGETS_ANGLE_SPINNER_HPP_ */
