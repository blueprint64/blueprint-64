#ifndef SRC_UI_WIDGETS_SHIFT_JIS_INPUT_HPP_
#define SRC_UI_WIDGETS_SHIFT_JIS_INPUT_HPP_

#include <QLineEdit>
#include "src/types.hpp"

class ShiftJisInput : public QLineEdit {
	Q_OBJECT

	Q_PROPERTY(int maxBytes MEMBER m_maxBytes)

	private:
	int m_maxBytes;

	public:
	ShiftJisInput( QWidget *parent = nullptr );
	ShiftJisInput( QWidget *parent, int maxBytes );
	~ShiftJisInput();

	inline void setMaxBytes( int maxBytes ) noexcept { m_maxBytes = maxBytes; }
	inline int getMaxBytes() const noexcept { return m_maxBytes; }

	void setUtf8( const string &str );

	string getUtf8() const;
	string getShiftJis() const;

	static string toShiftJis( const string &utf8 );
};



#endif /* SRC_UI_WIDGETS_SHIFT_JIS_INPUT_HPP_ */
