#include "src/ui/widgets/module-property-select.hpp"

#include "src/core/blueprint.hpp"

ModulePropertySelect::ModulePropertySelect(
	QWidget *parent,
	const Uuid &moduleId,
	const AsmModuleProperty &property
) : AbstractModulePropertySelect( parent, property ),
	m_moduleId( moduleId ),
	m_propertyName( property.name )
{}

void ModulePropertySelect::valueChanged() const {
	Blueprint::currentEditable()->asmModules().at( m_moduleId ).parameters[m_propertyName] = m_getter( m_select );
}
