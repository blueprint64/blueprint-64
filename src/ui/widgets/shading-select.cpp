#include "src/ui/widgets/shading-select.hpp"

#include <cassert>
#include <QPixmap>
#include <QPainter>
#include "src/core/blueprint.hpp"

void ShadingSelectWidget::setLevel( LevelId levelId ) {
	assert( Blueprint::current()->hasLevel( levelId ) );
	m_initializing = true;
	m_level = levelId;
	clear();
	const std::map<uint,ShadingSpecs> &shades = Blueprint::current()->levels().find( levelId )->second.shadingValues;
	for( const auto &shade : shades ) {
		const QColor ambient( shade.second.ambient.red, shade.second.ambient.green, shade.second.ambient.blue );
		const QColor diffuse( shade.second.diffuse.red, shade.second.diffuse.green, shade.second.diffuse.blue );

		QPixmap img( 32, 16 );
		img.fill( ambient );
		QPainter( &img ).fillRect( 16, 0, 16, 16, diffuse );

		addItem( QIcon( img ), shade.second.name.c_str(), QVariant::fromValue<uint>( shade.first ) );
	}

	addItem( "Add/Remove Shades" );
	m_initializing = false;
}

void ShadingSelectWidget::itemSelected( int newIndex ) {
	if( m_initializing || newIndex < 0 || m_level == LevelId::INVALID ) return;
	assert( Blueprint::current()->hasLevel( m_level ) );
	assert( (int)Blueprint::current()->levels().find( m_level )->second.shadingValues.size() == count() - 1 );
	if( newIndex < count() - 1 ) {
		emit shadeSelected( currentData().toUInt() );
		return;
	}

	emit dialogWillOpen();
	m_dialog.open( m_level );
}

void ShadingSelectWidget::dialogClosed() {
	setLevel( m_level );
	setSelectedShadeId( m_dialog.getSelectedShadeId() );
	emit shadeSelected( currentData().toUInt() );
}

void ShadingSelectWidget::setSelectedShadeId( uint id ) {
	for( int i = 0; i < count(); i++ ) {
		if( itemData( i, Qt::UserRole ).toUInt() == id ) {
			setCurrentIndex( i );
			return;
		}
	}
}

uint ShadingSelectWidget::getSelectedShadeId() const {
	return currentData().toUInt();
}
