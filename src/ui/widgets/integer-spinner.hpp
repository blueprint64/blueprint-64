#ifndef SRC_UI_WIDGETS_INTEGER_SPINNER_HPP_
#define SRC_UI_WIDGETS_INTEGER_SPINNER_HPP_

#include <QSpinBox>

class IntegerSpinner : public QSpinBox {
	Q_OBJECT

	private:
	int maxDigits() const;
	QString stripped( const QString &text ) const;

	public:
	explicit IntegerSpinner( QWidget *parent = nullptr ) : QSpinBox( parent ) {}
	virtual ~IntegerSpinner() {}

	protected:
	virtual QString textFromValue( int value ) const override;
	virtual int valueFromText( const QString &text ) const override;

	virtual void fixup( QString &input ) const override;
	virtual QValidator::State validate( QString &text, int &pos ) const override;

};



#endif /* SRC_UI_WIDGETS_INTEGER_SPINNER_HPP_ */
