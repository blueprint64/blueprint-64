#ifndef SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_
#define SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_

#include "src/ui/widgets/abstract-module-property-select.hpp"

class ModulePropertySelect : public AbstractModulePropertySelect {
	Q_OBJECT

	private:
	const Uuid m_moduleId;
	const string m_propertyName;

	public:
	explicit ModulePropertySelect(
		QWidget *parent,
		const Uuid &moduleId,
		const AsmModuleProperty &property
	);
	inline virtual ~ModulePropertySelect() {}

	protected slots:
	virtual void valueChanged() const override;

};

#endif /* SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_ */
