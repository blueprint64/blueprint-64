#include "src/ui/camera-options-dialog.hpp"
#include "ui_camera-options-dialog.h"

#include <cstring>
#include <cassert>
#include "src/ui/util.hpp"
#include "src/ui/widgets/camera-module-property-select.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/asm-modules.hpp"

CameraOptionsDialog::CameraOptionsDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::CameraOptionsDialog )
{
	m_ui->setupUi( this );
}

CameraOptionsDialog::~CameraOptionsDialog() {
	delete m_ui;
}

void CameraOptionsDialog::setVanillaOptions( const VanillaCameraOptions &options ) {
	switch( options.mode ) {
		case CameraPreset::SemiCardinal:
			m_ui->directionSelect->setCurrentIndex( options.ext.baseAngle / 0x2000 );
			break;
		case CameraPreset::BossFight:
			m_ui->behaviourInput->setValue( options.ext.behaviour );
			break;
		case CameraPreset::InnerRadial:
		case CameraPreset::OuterRadial:
			m_ui->xCentreSpinner->setValue( options.ext.centre[0] );
			m_ui->zCentreSpinner->setValue( options.ext.centre[1] );
			break;
		default:
			break;
	}
}

void CameraOptionsDialog::updateOptions( CameraTrigger &trigger ) const {
	if( trigger.type == CameraTriggerType::Vanilla ) {
		VanillaCameraOptions &options = std::get<VanillaCameraOptions>( trigger.options );
		std::memset( &options.ext, 0, sizeof( options.ext ) );
		switch( options.mode ) {
			case CameraPreset::SemiCardinal:
				options.ext.baseAngle = (ushort)m_ui->directionSelect->currentIndex() * (ushort)0x2000;
				break;
			case CameraPreset::BossFight:
				options.ext.behaviour = m_ui->behaviourInput->value();
				break;
			case CameraPreset::InnerRadial:
			case CameraPreset::OuterRadial:
				options.ext.centre[0] = (float)m_ui->xCentreSpinner->value();
				options.ext.centre[1] = (float)m_ui->zCentreSpinner->value();
				break;
			default:
				break;
		}
	}
}

void CameraOptionsDialog::run( CameraTrigger &trigger ) {
	m_ui->semicardinalOptions->setVisible( false );
	m_ui->bossOptions->setVisible( false );
	m_ui->radialOptions->setVisible( false );
	m_ui->moduleOptions->setVisible( false );

	m_ui->dialogButtonTray->setStandardButtons( QDialogButtonBox::Ok | QDialogButtonBox::Cancel );
	if( trigger.type == CameraTriggerType::Module ) {
		ModuleCameraOptions &options = std::get<ModuleCameraOptions>( trigger.options );
		const CameraModule *module = Blueprint::current()->getCameraModules().at( options.moduleId ).module;

		m_ui->moduleOptions->setVisible( true );
		m_ui->dialogButtonTray->setStandardButtons( QDialogButtonBox::Ok );
		UiUtil::clearLayout( m_ui->moduleParameters->layout() );
		for( const AsmModuleProperty &prop : module->parameters ) {
			m_ui->moduleParameters->layout()->addWidget(
				new CameraModulePropertySelect( m_ui->moduleParameters, &options.moduleParams, prop )
			);
		}
	} else switch( std::get<VanillaCameraOptions>( trigger.options ).mode ) {
		case CameraPreset::SemiCardinal:
			m_ui->semicardinalOptions->setVisible( true ); break;
		case CameraPreset::BossFight:
			m_ui->bossOptions->setVisible( true ); break;
		case CameraPreset::InnerRadial:
		case CameraPreset::OuterRadial:
			m_ui->radialOptions->setVisible( true ); break;
		default:
			assert( false );
	}

	if( exec() == QDialog::Accepted ) {
		updateOptions( trigger );
	} else if( trigger.type == CameraTriggerType::Vanilla ) {
		setVanillaOptions( std::get<VanillaCameraOptions>( trigger.options ) );
	}
}

void CameraOptionsDialog::showEvent( QShowEvent *event ) {
	QDialog::showEvent( event );
	QRect geo = geometry();
	geo.setHeight( 0 );
	setGeometry( geo );
}
