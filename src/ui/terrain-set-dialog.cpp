#include "src/ui/terrain-set-dialog.hpp"
#include "ui_terrain-set-dialog.h"

#include <QFormLayout>
#include <QLabel>
#include <cassert>
#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

TerrainSetDialog::TerrainSetDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::TerrainSetDialog ),
	m_ignoreEvents( false )
{
	m_ui->setupUi( this );
}

TerrainSetDialog::~TerrainSetDialog() {
	delete m_ui;
}

void TerrainSetDialog::initialize() {
	m_ignoreEvents = true;
	const std::array<TerrainSoundPack,7> &terrainSets = Blueprint::current()->terrainSets();
	for( int i = 0; i < 7; i++ ) {
		m_ui->terrainSetList->item( i )->setText( terrainSets[i].name.c_str() );
	}
	m_ui->terrainSetList->setCurrentRow( 0 );
	m_ignoreEvents = false;
	setSelected();
}

static void addEffect(
	QFormLayout *effectsLayout,
	const char *name,
	const char *description
) {
	QLabel *nameLabel = new QLabel( name, effectsLayout->parentWidget() );
	QLabel *descriptionLabel = new QLabel( description, effectsLayout->parentWidget() );
	QFont labelFont = nameLabel->font();
	labelFont.setBold( true );
	nameLabel->setFont( labelFont );
	nameLabel->setTextFormat( Qt::TextFormat::PlainText );
	nameLabel->setAlignment( Qt::AlignTop );
	descriptionLabel->setWordWrap( true );
	descriptionLabel->setTextFormat( Qt::TextFormat::PlainText );
	descriptionLabel->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Minimum );
	effectsLayout->addRow( nameLabel, descriptionLabel );
}

void TerrainSetDialog::setSelected() {
	if( m_ignoreEvents ) return;
	m_ignoreEvents = true;
	assert( !m_ui->terrainSetList->selectedItems().empty() );
	const TerrainSoundPack &terrainSet = Blueprint::current()->terrainSets().at( m_ui->terrainSetList->currentRow() );
	m_ui->setName->setText( terrainSet.name.c_str() );
	m_ui->normalSound->setCurrentIndex( (int)terrainSet.normal );
	m_ui->hardSound->setCurrentIndex( (int)terrainSet.hard );
	m_ui->slipperySound->setCurrentIndex( (int)terrainSet.slippery );
	m_ui->verySlipperySound->setCurrentIndex( (int)terrainSet.slide );
	m_ui->alternateSound->setCurrentIndex( (int)terrainSet.alternate );
	m_ui->alternateSlipperySound->setCurrentIndex( (int)terrainSet.slipperyAlternate );

	QFormLayout *effectsLayout = dynamic_cast<QFormLayout*>( m_ui->effectsGroup->layout() );
	assert( effectsLayout != nullptr );
	UiUtil::clearLayout( effectsLayout );
	switch( m_ui->terrainSetList->currentRow() ) {
		case 2: {// Snow
			addEffect( effectsLayout, "Soft Ground", "Mario gets stuck in non-hard surface types instead of taking damage" );
			addEffect( effectsLayout, "Chilly", "Mario shivers when idle" );
			addEffect( effectsLayout, "Cold Water", "Mario loses health faster while in water and doesn't recover health when at the surface" );
			addEffect( effectsLayout, "Frigid Water", "Lava does not set Mario on fire" );
			break;
		}
		case 3: { // Sand
			addEffect( effectsLayout, "Soft Ground", "Mario gets stuck in non-hard surface types instead of taking damage" );
			break;
		}
		case 6: { // Slide
			addEffect( effectsLayout, "Slide", "Every surface type is slippery" );
			break;
		}
		default: {
			addEffect( effectsLayout, "", "None" );
			break;
		}
	}
	m_ignoreEvents = false;
}

void TerrainSetDialog::nameChanged() {
	if( m_ignoreEvents ) return;
	Blueprint::currentEditable()->terrainSets().at( m_ui->terrainSetList->currentRow() ).name = m_ui->setName->text().toStdString();
	m_ui->terrainSetList->currentItem()->setText( m_ui->setName->text() );
}

void TerrainSetDialog::soundChanged() const {
	if( m_ignoreEvents ) return;
	TerrainSoundPack &terrainSet = Blueprint::currentEditable()->terrainSets().at( m_ui->terrainSetList->currentRow() );
	terrainSet.normal = (TerrainSound)m_ui->normalSound->currentIndex();
	terrainSet.hard = (TerrainSound)m_ui->hardSound->currentIndex();
	terrainSet.slippery = (TerrainSound)m_ui->slipperySound->currentIndex();
	terrainSet.slide = (TerrainSound)m_ui->verySlipperySound->currentIndex();
	terrainSet.alternate = (TerrainSound)m_ui->alternateSound->currentIndex();
	terrainSet.slipperyAlternate = (TerrainSound)m_ui->alternateSlipperySound->currentIndex();
}
