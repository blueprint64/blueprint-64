#ifndef UI_ROM_SETTINGS_HPP_
#define UI_ROM_SETTINGS_HPP_

#include <QWidget>
#include "src/ui/create-level-dialog.hpp"
#include "src/ui/end-screen-dialog.hpp"
#include "src/ui/move-level-dialog.hpp"
#include "src/ui/terrain-set-dialog.hpp"

namespace Ui {
	class RomSettingsWidget;
}

class RomSettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::RomSettingsWidget *m_ui;
	CreateLevelDialog m_createLevelDialog;
	EndScreenDialog m_endScreenDialog;
	MoveLevelDialog m_moveLevelDialog;
	TerrainSetDialog m_terrainSetsDialog;
	bool m_ignoreUpdates;

	void updateAudioHeapCheckbox();

	public slots:
	void updateData();
	void browseForRom();
	void addLevel();
	void levelSelected();
	void editSelectedLevel();
	void deleteSelectedLevel();
	void levelAdded( LevelId levelId );
	void editEndScreen();
	void switchMode();
	void romPathChanged();
	void scaleSliderMoved( int tick );

	private slots:
	void refreshLevelList();

	protected:
	virtual void showEvent( QShowEvent *event ) override {
		QWidget::showEvent( event );
		refreshLevelList();
		updateAudioHeapCheckbox();
	}

	signals:
	void romTargetChanged();
	void editDialog();
	void editTweaks();
	void editLevel( LevelId );
	void editModules();
	void editMarioActions();
	void editMusic();
	//TODO: editSoundEffects
	//TODO: editObjectBanks
	void editBackgrounds();

	public:
	explicit RomSettingsWidget( QWidget *parent = nullptr );
	~RomSettingsWidget();

	void blueprintChanged();
};

#endif /* UI_ROM_SETTINGS_HPP_ */
