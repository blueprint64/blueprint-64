#include "src/ui/model-import-helper.hpp"

#include <QMessageBox>
#include "src/ui/warnings-dialog.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/import/texture.hpp"

BoundingBox IModelImportHelper::handleImport( const ModelImportedEvent &importEvent ) const {
	BoundingBox boundingBox = { 0, 0, 0, 0, 0, 0 };

	const fs::path levelPath = Blueprint::current()->getLevelPath( m_levelId );
	const fs::path importDir = getImportDir();
	fs::create_directories( importDir );

	HashMap<string,MaterialSpecs> specsMap;
	const HashMap<string,MaterialSpecs> oldSpecsMap = getGeometry();
	int fileNum = 0;

	bool yesToAll = false;
	bool noToAll = false;
	for( const ModelFileImportInfo &model : importEvent.models ) {
		string extension = fs::to_path( model.filePath ).extension().u8string();
		const IModelImporter *importer = IModelImporter::tryGetImporter( extension );
		if( importer == nullptr ) {
			QMessageBox::warning( m_parent, "Import Skipped", ("File extension '"s + extension + "' is not supported.").c_str(), QMessageBox::Ok );
			continue;
		}

		const string prefix = importEvent.models.size() > 1 ? (std::to_string( ++fileNum ) + '_') : "";
		HashMap<string,MaterialFaces> materials;
		try {
			ImportedModel importResult = importer->import( model.filePath, importEvent.scale, levelPath, importDir, prefix );
			materials = std::move( importResult.materials );
			boundingBox.merge( importResult.boundingBox );

			if( !importResult.warnings.empty() ) {
				WarningsDialog( m_parent ).getResponse( "Import completed with warnings:", importResult.warnings, false );
			}
		} catch( const FileParserException &fileParserError ) {
			QMessageBox::critical( m_parent, "Parsing Error", fileParserError.what(), QMessageBox::Ok );
			specsMap.clear();
			break;
		} catch( ... ) {
			QMessageBox::critical( m_parent, "Unexpected Error", "An unexpected error occurred. Please report this bug", QMessageBox::Ok );
			specsMap.clear();
			break;
		}
		specsMap.reserve( materials.size() );
		for( const auto &i : materials ) {

			if( oldSpecsMap.find( i.first ) != oldSpecsMap.end() ) {
				const MaterialSpecs &oldSpecs = oldSpecsMap.find( i.first )->second;
				MaterialSpecs specs = oldSpecs;

				if( i.second.textureInfo.has_value() ) {
					const TextureInfo &newTexture = i.second.textureInfo.value();
					if( oldSpecs.textureInfo.has_value() ) {
						if( !newTexture.supportsFormat( oldSpecs.format ) ) {
							specs.format = newTexture.suggestFormat();
						}
					} else {
						specs.visible = model.visible && newTexture.averageAlpha > 0;
						specs.layer = newTexture.suggestLayer();
						specs.format = newTexture.suggestFormat();
					}
					specs.textureInfo = newTexture;
					specs.removedTextureAlpha = false;
					if( newTexture.hasFullAlpha && !newTexture.hasVaryingAlpha && oldSpecs.removedTextureAlpha && !noToAll ) {
						bool removeAlpha = yesToAll;
						if( !removeAlpha ) {
							const QMessageBox::StandardButton response = QMessageBox::question(
								m_parent,
								"Remove Alpha?",
								("You previously chose to remove the alpha channel from the texture for material '" + i.first +
								"' in favour of using the opacity select instead. Would you like to do this again?").c_str(),
								QMessageBox::Yes | QMessageBox::No | QMessageBox::YesAll | QMessageBox::NoAll,
								QMessageBox::Yes
							);

							if( response == QMessageBox::YesAll ) {
								yesToAll = true;
								removeAlpha = true;
							} else if( response == QMessageBox::Yes ) {
								removeAlpha = true;
							} else if( response == QMessageBox::NoAll ) {
								noToAll = true;
							}
						}

						TextureFile texFile;
						if( removeAlpha && TextureFile::tryOpen( (levelPath / ( newTexture.hash + ".png" ) ).u8string().c_str(), texFile ) != TextureFile::Error ) {
							//TODO: show warning if texture resized
							texFile.removeAlpha();
							texFile.saveAsPng( ( levelPath / (texFile.info().hash + ".png")).u8string().c_str() );
							specs.textureInfo = texFile.info();
							specs.removedTextureAlpha = true;
						}
					}
				} else {
					specs.visible = false;
				}
				specsMap.insert_or_assign( i.first, specs );
			} else {
				MaterialSpecs specs = MaterialSpecs::Default();
				specs.textureInfo = i.second.textureInfo;
				specs.visible = model.visible && specs.textureInfo.has_value() && specs.textureInfo.value().averageAlpha > 0;
				specs.solid = model.solid;
				if( i.second.textureInfo.has_value() ) {
					const TextureInfo &info = i.second.textureInfo.value();
					specs.layer = info.suggestLayer();
					specs.format = info.suggestFormat();
				}
				specsMap.insert_or_assign( i.first, specs );
			}
		}
	}

	if( !specsMap.empty() ) {
		saveGeometry( specsMap );
		return boundingBox;
	}

	return { 0, 0, 0, 0, 0, 0 };
}

HashMap<string,MaterialSpecs> AreaModelImportHelper::getGeometry() const {
	return Blueprint::current()->getAreaGeometry( m_levelId, m_areaIndex );
}

void AreaModelImportHelper::saveGeometry( const HashMap<string,MaterialSpecs> &specsMap ) const {
	Blueprint::currentEditable()->saveAreaGeometry( m_levelId, m_areaIndex, specsMap );
}

fs::path AreaModelImportHelper::getImportDir() const {
	return Blueprint::current()->getAreaPath( m_levelId, m_areaIndex );
}

HashMap<string,MaterialSpecs> ObjectModelImportHelper::getGeometry() const {
	return Blueprint::current()->getObjectGeometry( m_levelId, m_objectId );
}

void ObjectModelImportHelper::saveGeometry( const HashMap<string,MaterialSpecs> &specsMap ) const {
	Blueprint::currentEditable()->saveObjectGeometry( m_levelId, m_objectId, specsMap );
}

fs::path ObjectModelImportHelper::getImportDir() const {
	return Blueprint::current()->getObjectPath( m_levelId, m_objectId );
}
