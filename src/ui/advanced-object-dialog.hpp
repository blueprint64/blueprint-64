#ifndef SRC_UI_ADVANCED_OBJECT_DIALOG_HPP_
#define SRC_UI_ADVANCED_OBJECT_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"
#include "src/core/objectid.hpp"

namespace Ui {
	class AdvancedObjectDialog;
}

class AdvancedObjectDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::AdvancedObjectDialog *m_ui;
	LevelId m_levelId;
	objectId m_objectId;

	void setup( LevelId levelId, objectId objectId );

	public:
	explicit AdvancedObjectDialog( QWidget *parent = nullptr );
	~AdvancedObjectDialog();

	inline void open( LevelId levelId, objectId objectId ) {
		setup( levelId, objectId );
		QDialog::open();
	}

	inline int exec( LevelId levelId, objectId objectId ) {
		setup( levelId, objectId );
		return QDialog::exec();
	}

	virtual void accept() override;

};

#endif /* SRC_UI_ADVANCED_OBJECT_DIALOG_HPP_ */
