#ifndef SRC_UI_CREATE_LEVEL_DIALOG_HPP_
#define SRC_UI_CREATE_LEVEL_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class CreateLevelDialog;
}

class CreateLevelDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::CreateLevelDialog *m_ui;

	public slots:
	void levelSelected();
	void accept() override;

	signals:
	void levelCreated( LevelId );

	public:
	explicit CreateLevelDialog( QWidget *parent = nullptr );
	~CreateLevelDialog();

	void showEvent( QShowEvent *event ) override;

};

#endif /* SRC_UI_CREATE_LEVEL_DIALOG_HPP_ */
