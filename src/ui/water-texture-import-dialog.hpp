#ifndef SRC_UI_WATER_TEXTURE_IMPORT_DIALOG_HPP_
#define SRC_UI_WATER_TEXTURE_IMPORT_DIALOG_HPP_

#include <QDialog>
#include <QPixmap>
#include <string>
#include "src/core/data/water.hpp"

namespace Ui {
	class WaterTextureImportDialog;
}

class WaterTextureImportDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::WaterTextureImportDialog *m_ui;
	WaterTextureInfo m_prevState;
	QPixmap m_texture;

	void setImage( QImage &image );

	public:
	explicit WaterTextureImportDialog( QWidget *parent = nullptr );
	~WaterTextureImportDialog();

	virtual void open() override;
	void open( const WaterTextureInfo &texture );

	public slots:
	void browseButtonClicked();
	void formatChanged();
	void nameChanged();
	virtual void accept() override;

	signals:
	void textureImported(WaterTextureInfo);
	void textureChanged(WaterTextureInfo previous, WaterTextureInfo updated);

};

#endif /* SRC_UI_WATER_TEXTURE_IMPORT_DIALOG_HPP_ */
