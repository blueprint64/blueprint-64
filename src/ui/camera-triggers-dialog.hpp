#ifndef SRC_UI_CAMERA_TRIGGERS_DIALOG_HPP_
#define SRC_UI_CAMERA_TRIGGERS_DIALOG_HPP_

#include <QDialog>

namespace Ui {
	class CameraTriggersDialog;
}

class CameraTriggersDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::CameraTriggersDialog *m_ui;
	ushort m_areaId;
	bool m_ignoreEvents;

	public:
	explicit CameraTriggersDialog( QWidget *parent = nullptr );
	~CameraTriggersDialog();

	private:
	void refreshSettingsTab();

	public:
	void open( ushort areaId );

	public slots:
	void addTrigger();
	void deleteTrigger();
	void nameChanged();
	void regionChanged();
	void effectChanged();
	void effectSettingsChanged();
	void triggerSelected();
	void cameraModuleSelected();

};


#endif /* SRC_UI_CAMERA_TRIGGERS_DIALOG_HPP_ */
