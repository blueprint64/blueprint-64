#ifndef UI_MAIN_WINDOW_HPP_
#define UI_MAIN_WINDOW_HPP_

#include <QMainWindow>
#include <QCloseEvent>
#include <QAction>

#include "src/ui/create-dialog.hpp"
#include "src/ui/warnings-dialog.hpp"
#include "src/ui/armips-error-dialog.hpp"
#include "src/ui/preferences.hpp"
#include "src/core/enums.hpp"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow {
	Q_OBJECT

	private:
	Ui::MainWindow *m_ui;
	PreferencesDialog m_preferencesDialog;
	WarningsDialog m_buildWarningsDialog;
	ArmipsErrorDialog m_armipsErrorDialog;
	LevelId m_currentLevel;
	ubyte m_currentArea;
	objectId m_currentObject;

	void syncHistory();
	bool unsavedChangesPrompt();
	bool trySave();
	void build( bool clean );

	signals:
	void openNew();
	void createNew();

	protected:
	void closeEvent( QCloseEvent *event ) override {
		unsavedChangesPrompt() ? event->accept() : event->ignore();
	}

	public slots:
	void showErrorDialog( string message );
	void loadBlueprint();
	void save() { trySave(); }
	void saveAs();
	void updateChecksum();
	void quit() { unsavedChangesPrompt() && close(); }
	void load() { if( unsavedChangesPrompt() ) emit openNew(); }
	void loadNew() { if( unsavedChangesPrompt() ) emit createNew(); }
	void showSettings() { m_preferencesDialog.show(); }
	void checkCanBuild();
	void navigated( uint locationId );
	void buildAll();
	void buildClean();
	void playRom();
	void editTweaks();
	void editDialog();
	void editLevel( LevelId levelId );
	void editArea( ubyte areaIndex );
	void editAreaModel();
	void editMusic();
	void editObject( objectId objectId );
	void editObjectModel();
	void editObjectModel( objectId objectId );
	void editSimpleBehaviour();
	void editWaterBoxes();
	void editBackgrounds();
	void editAsmModules();
	void editMarioActions();
	void developModule();

	public:
	explicit MainWindow( QWidget *parent = nullptr );
	~MainWindow();
};

#endif /* UI_MAIN_WINDOW_HPP_ */
