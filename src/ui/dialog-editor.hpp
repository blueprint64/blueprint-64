#ifndef SRC_UI_DIALOG_EDITOR_HPP_
#define SRC_UI_DIALOG_EDITOR_HPP_

#include <QWidget>
#include <QPushButton>
#include "src/types.hpp"
#include "src/ui/sound-select-dialog.hpp"

#include <iostream>

namespace Ui {
	class DialogEditor;
}

class DialogEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::DialogEditor *m_ui;
	QPushButton *m_specialCharacterButtons[17];
	ubyte m_dialogId;
	bool m_nonUserUpdate;
	SoundSelectDialog m_soundSelectDialog;

	public slots:
	void nameChanged();
	void dialogEntryChanged();
	void dialogLinesChanged();
	void dialogSelected();
	void textChanged();
	void soundTableEntrySelected();
	void editSoundTableItem();
	void soundTableItemChanged();
	void genericNameChanged();

	public:
	explicit DialogEditor( QWidget *parent = nullptr );
	~DialogEditor();

	void blueprintChanged();

	void showEvent( QShowEvent *event ) override {
		blueprintChanged();
		QWidget::showEvent( event );
	}

};



#endif /* SRC_UI_DIALOG_EDITOR_HPP_ */
