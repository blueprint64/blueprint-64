#include "src/ui/util.hpp"

#include <cassert>

#include <QStandardItemModel>
#include <QFormLayout>
#include <QFile>
#include <vector>
#include <algorithm>
#include <cstring>
#include <locale>
#ifdef _WIN32
#include "src/polyfill/filesystem.hpp"
#endif

using namespace std;

static constexpr size_t BUFFER_SIZE = 1024;

class QFileStreamBuffer : public streambuf {

	private:
	QFile m_file;
	char m_buffer[BUFFER_SIZE];

	public:
	QFileStreamBuffer( const char *resourcePath ) :
		streambuf(),
		m_file( QFile( resourcePath ) )
	{
		m_file.open( QIODevice::ReadOnly );
	}

	~QFileStreamBuffer() {
		m_file.close();
	}

	protected:
	int underflow() override {
		if( m_file.atEnd() ) {
			return char_traits<char>::eof();
		}

		qint64 bytesRead = m_file.read( m_buffer, BUFFER_SIZE );
		if( bytesRead < 0 ) {
			return char_traits<char>::eof();
		}

		setg( m_buffer, m_buffer, &m_buffer[bytesRead] );
		return m_buffer[0];
	}

	pos_type seekpos( pos_type pos, [[maybe_unused]] ios_base::openmode which ) override {
		m_file.seek( pos );
		underflow();
		return m_file.pos();
	}

	pos_type seekoff( off_type off, ios_base::seekdir dir, ios_base::openmode which ) override {
		qint64 seekTo = 0;
		switch( dir ) {
			case ios_base::beg: seekTo = off; break;
			case ios_base::end: seekTo = m_file.size() + off; break;
			case ios_base::cur: seekTo = m_file.pos() + off; break;
			default: return -1;
		}

		if( seekTo < 0 || seekTo > m_file.size() ) {
			return -1;
		}

		return seekpos( seekTo, which );
	}

};

QResourceStream::QResourceStream( const char *resourcePath )
	: std::istream( new QFileStreamBuffer( resourcePath ) ) {}

void UiUtil::clearLayout( QLayout *layout ) {
	QFormLayout *formLayout = dynamic_cast<QFormLayout*>( layout );
	if( formLayout != nullptr ) {
		while( formLayout->rowCount() > 0 ) formLayout->removeRow( 0 );
		return;
	}

	while( QLayoutItem *item = layout->takeAt( 0 ) ) {
		UiUtil::freeLayoutItem( item );
	}
}

void UiUtil::deleteItemAt( QLayout *layout, int index ) {
	UiUtil::freeLayoutItem( layout->takeAt( index ) );
}

void UiUtil::deleteItemAt( QGridLayout *layout, int row, int col ) {
	QLayoutItem *item = layout->itemAtPosition( row, col );
	layout->removeItem( item );
	UiUtil::freeLayoutItem( item );
}

void UiUtil::freeLayoutItem( QLayoutItem *item ) {
	if( item->layout() ) {
		assert( item->layout() != item );
		UiUtil::freeLayoutItem( item->layout() );
	}

	if( item->widget() ) {
		item->widget()->deleteLater();
	}

	delete item;
}

#ifdef _WIN32
QString UiUtil::toNativePath( const QString &posixPath ) {
	if( posixPath.isNull() || posixPath.isEmpty() ) return posixPath;
	return QString( fs::to_path( posixPath.toStdString() ).make_preferred().u8string().c_str() );
}
#endif

void UiUtil::sortListEntries( QListWidget *list, bool(*compareFunc)(const QListWidgetItem&, const QListWidgetItem&) ) {
	std::vector<QListWidgetItem*> entries;
	entries.reserve( list->count() );

	while( QListWidgetItem *entry = list->takeItem( 0 ) ) {
		entries.push_back( entry );
	}

	std::sort( entries.begin(), entries.end(), [compareFunc](const QListWidgetItem* x, const QListWidgetItem *y) -> bool {
		return compareFunc( *x, *y );
	});

	for( QListWidgetItem* entry : entries ) {
		list->addItem( entry );
	}
}

static inline std::locale safeGetLocale() {
	try {
		// Windows is so hilariously broken that this can actually fail
		return std::locale( "" );
	} catch( ... ) {}

	try {
		return std::locale( "en_US.UTF-8" );
	} catch( ... ) {}

	return std::locale::classic();
}

bool ListSorters::Lexiographic( const QListWidgetItem &A, const QListWidgetItem &B ) {
	static auto locale = safeGetLocale();
	return locale( A.text().toStdString(), B.text().toStdString() );
}

bool ListSorters::NaturalString( const QListWidgetItem &A, const QListWidgetItem &B ) {
	static auto locale = safeGetLocale();
	const std::collate<char> &collation = std::use_facet<std::collate<char>>( locale );

	const string x = A.text().toStdString();
	const string y = B.text().toStdString();

	if( x.empty() || y.empty() ) {
		return x.length() < y.length();
	}

	size_t x0, x1, y0, y1;

	x0 = 0;
	y0 = 0;
	while( true ) {
		/* Compare non-numeric characters */
		for( x1 = x0; x1 < x.length() && (x[x1] < '0' || x[x1] > '9'); x1++ );
		for( y1 = y0; y1 < y.length() && (y[y1] < '0' || y[y1] > '9'); y1++ );

		if( y0 == y.length() ) {
			return false;
		} else if( x0 == x.length() ) {
			return true;
		} else if( (x0 == x1) != (y0 == y1) ) {
			return collation.compare( &x[x0], &x[x0+1], &y[y0], &y[y0+1] ) < 0;
		}

		const int sc = collation.compare( &x[x0], &x[x1], &y[y0], &y[y1] );
		if( sc < 0 ) return true;
		if( sc > 0 ) return false;

		x0 = x1;
		y0 = y1;

		/* Compare numbers */
		for( x1 = x0; x1 < x.length() && x[x1] >= '0' && x[x1] <= '9'; x1++ );
		for( y1 = y0; y1 < y.length() && y[y1] >= '0' && y[y1] <= '9'; y1++ );

		if( y0 == y1 ) {
			return false;
		} else if( x0 == x1 ) {
			return true;
		}

		while( x[x0] == '0' && x0 < x1 - 1 ) x0++;
		while( y[y0] == '0' && y0 < y1 - 1 ) y0++;

		if( y1 - y0 == x1 - x0 ) {
			const int nc = std::strncmp( &x[x0], &y[y0], x1 - x0 );
			if( nc < 0 ) return true;
			if( nc > 0 ) return false;
		} else {
			return x1 - x0 < y1 - y0;
		}

		x0 = x1;
		y0 = y1;
	}

}

void UiUtil::setItemEnabled( QComboBox *comboBox, int index, bool isEnabled ) {
	qobject_cast<QStandardItemModel*>( comboBox->model() )->item( index )->setEnabled( isEnabled );
}
