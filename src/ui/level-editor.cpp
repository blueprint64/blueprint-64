#include "src/ui/level-editor.hpp"
#include "ui_level-edit.h"

#include <QMessageBox>
#include "src/core/blueprint.hpp"
#include "src/core/util.hpp"
#include "src/core/objectbanks.hpp"
#include "src/core/enums.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/ui/model-import-helper.hpp"
#include "src/ui/util.hpp"

LevelEditor::LevelEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::LevelEditor ),
	m_levelId( LevelId::INVALID ),
	m_initializing( false )
{
	m_ui->setupUi( this );
	connect( &m_objectBanksDialog, SIGNAL(accepted()), this, SLOT(refreshObjectBanks()) );
	connect( &m_createAreaDialog, SIGNAL(accepted()), this, SLOT(areaCreated()) );
	connect( &m_createObjectDialog, SIGNAL(accepted()), this, SLOT(objectCreated()) );
	connect( &m_importModelDialog, SIGNAL(modelImported(ModelImportedEvent)), this, SLOT(objectModelImported(ModelImportedEvent)) );
}

LevelEditor::~LevelEditor() {
	delete m_ui;
}

void LevelEditor::starNameChanged() {
	assert( m_levelId != LevelId::INVALID );
	string *starNames = Blueprint::currentEditable()->levels()[m_levelId].starNames;
	starNames[0] = m_ui->starNameField1->text().toStdString();
	starNames[1] = m_ui->starNameField2->text().toStdString();
	starNames[2] = m_ui->starNameField3->text().toStdString();
	starNames[3] = m_ui->starNameField4->text().toStdString();
	starNames[4] = m_ui->starNameField5->text().toStdString();
	starNames[5] = m_ui->starNameField6->text().toStdString();
}

void LevelEditor::levelDataChanged() {
	if( m_initializing ) return;
	LevelSpecs &level = Blueprint::currentEditable()->levels()[m_levelId];
	level.name = m_ui->levelNameField->text().toStdString();
	level.enableStarSelect = m_ui->starSelectCheckbox->isChecked() && m_ui->starSelectCheckbox->isEnabled();
	level.marioSpawn.area = (ubyte)m_ui->spawnArea->value();
	level.marioSpawn.x = (short)m_ui->spawnX->value();
	level.marioSpawn.y = (short)m_ui->spawnY->value();
	level.marioSpawn.z = (short)m_ui->spawnZ->value();
	level.marioSpawn.yaw = (ushort)m_ui->spawnAngle->value();
	level.maxSoundDistance = 3 * (ushort)m_ui->soundCarryDistanceSpinner->value();
	level.defaultEchoLevel = (sbyte)m_ui->echoLevelSpinner->value();
	level.overridePauseWarp = m_ui->pauseWarpGroup->isChecked();
	level.pauseWarp.level = m_ui->pauseWarpLevelSelect->selectedLevel();
	level.pauseWarp.area = (ubyte)m_ui->pauseWarpAreaSpinner->value();
	level.pauseWarp.warpId = (ubyte)m_ui->pauseWarpWarpSpinner->value();
}

void LevelEditor::selectObjectBanks() {
	m_objectBanksDialog.open( m_levelId );
}

void LevelEditor::areaSelected() {
	if( m_initializing ) return;
	m_ui->editAreaButton->setEnabled( true );
	m_ui->deleteAreaButton->setEnabled( true );
}


void LevelEditor::objectSelected() {
	if( m_initializing ) return;
	m_ui->editObjectButton->setEnabled( true );
	m_ui->deleteObjectButton->setEnabled( true );
}

void LevelEditor::addArea() {
	m_createAreaDialog.show();
}

void LevelEditor::editArea(){
	emit gotoAreaEdit( (ubyte)m_ui->areaList->currentItem()->data( Qt::UserRole ).toUInt() );
}

void LevelEditor::deleteArea() {
	const ubyte areaIndex = (ubyte)m_ui->areaList->currentItem()->data( Qt::UserRole ).toUInt();
	if( QMessageBox::question( this, "Delete Area?", "Are you sure you want to delete this area?", QMessageBox::Yes | QMessageBox::No, QMessageBox::NoButton ) == QMessageBox::Yes ) {
		Blueprint::currentEditable()->areas().erase( AREA_ID( m_levelId, areaIndex ) );
		assert( !Blueprint::current()->hasArea( m_levelId, areaIndex ) );
		delete m_ui->areaList->takeItem( m_ui->areaList->currentRow() );
		m_ui->areaList->clearSelection();
		m_ui->deleteAreaButton->setEnabled( false );
		m_ui->editAreaButton->setEnabled( false );
		m_ui->addAreaButton->setEnabled( true );
		m_createAreaDialog.setLevel( m_levelId );
		fs::forceDeleteRecursive( Blueprint::current()->getAreaPath( m_levelId, areaIndex ) );
		fs::forceDelete( Blueprint::current()->getAreaActorsPath( m_levelId, areaIndex ) );
	}
}

void LevelEditor::addObject() {
	m_createObjectDialog.open( m_levelId );
}

void LevelEditor::editObject() {
	const objectId objectId = m_ui->objectList->currentItem()->data( Qt::UserRole ).toUInt();
	switch( ObjectId::getType( objectId ) ) {
		case ObjectType::Simple: {
			emit gotoObjectEdit( objectId );
			break;
		}
		case ObjectType::Partial: {
			emit gotoObjectModelEdit( objectId );
			break;
		}
		case ObjectType::Advanced: {
			m_advancedObjectDialog.open( m_levelId, objectId );
			break;
		}
		default: break;
	}
}

void LevelEditor::deleteObject() {
	const objectId objectId = m_ui->objectList->currentItem()->data( Qt::UserRole ).toUInt();
	LevelSpecs &levelSpecs = Blueprint::currentEditable()->levels().at( m_levelId );

	if( ObjectId::getType( objectId ) == ObjectType::Partial ) {
		foreach_cvalue( userData, levelSpecs.userData ) {
			foreach_cvalue( link, userData.links ) {
				if( link.linkType == LinkType::PartialObject && link.targetId == objectId ) {
					const string message = "You cannot delete this object because it is being referenced by your custom user data: "s + userData.name;
					QMessageBox::critical( this, "Partial object in use", message.c_str() );
					return;
				}
			}
		}
	}

	if( QMessageBox::question( this, "Confirm Delete", "Are you sure you want to delete this object?" ) != QMessageBox::Yes ) {
		return;
	}

	switch( ObjectId::getType( objectId ) ) {
		case ObjectType::Simple: levelSpecs.simpleObjects.erase( objectId ); break;
		case ObjectType::Partial: levelSpecs.partialObjects.erase( objectId ); break;
		case ObjectType::Advanced: levelSpecs.advancedObjects.erase( objectId ); break;
		default: return;
	}
	fs::forceDeleteRecursive( Blueprint::current()->getObjectPath( m_levelId, objectId ) );
	QListWidgetItem *entry = m_ui->objectList->currentItem();
	m_ui->objectList->removeItemWidget( entry );
	delete entry;
	if( m_ui->objectList->count() == 0 ) {
		m_ui->editObjectButton->setEnabled( false );
		m_ui->deleteObjectButton->setEnabled( false );
	}
}

void LevelEditor::objectCreated() {
	switch( m_createObjectDialog.getSelection() ) {
		case ObjectType::Simple: {
			const objectId objectId = ObjectId::generateNew( ObjectType::Simple );
			ObjectSpecs &newObject = Blueprint::currentEditable()->levels().at( m_levelId ).simpleObjects[objectId];
			newObject = ObjectSpecs::Default();
			newObject.modelId = *Blueprint::current()->getFreeModelIds( m_levelId ).begin();
			emit gotoObjectEdit( objectId );
			return;
		}
		case ObjectType::Partial: {
			m_importModelDialog.open();
			return;
		}
		case ObjectType::Advanced: {
			const objectId objectId = ObjectId::generateNew( ObjectType::Advanced );
			AdvancedObjectSpecs &newObject = Blueprint::currentEditable()->levels().at( m_levelId ).advancedObjects[objectId];
			newObject.name = "New Object";
			newObject.modelId = *Blueprint::current()->getFreeModelIds( m_levelId ).begin();
			newObject.customDataId = Blueprint::current()->levels().at( m_levelId ).userData.begin()->first;
			if( m_advancedObjectDialog.exec( m_levelId, objectId ) == QDialog::Accepted ) {
				m_ui->objectList->addValueItem<uint>( newObject.name, objectId );
			} else {
				Blueprint::currentEditable()->levels().at( m_levelId ).advancedObjects.erase( objectId );
			}
			return;
		}
		default: return;
	}
}

void LevelEditor::objectModelImported( ModelImportedEvent event ) {
	const objectId objectId = ObjectId::generateNew( ObjectType::Partial );
	Blueprint::currentEditable()->levels().at( m_levelId ).partialObjects[objectId] = { "New Sub-Object", false };
	if( !ObjectModelImportHelper( this, m_levelId, objectId ).handleImport( event ).isEmpty() ) {
		emit gotoObjectModelEdit( objectId );
	} else {
		Blueprint::currentEditable()->levels().at( m_levelId ).partialObjects.erase( objectId );
		fs::forceDeleteRecursive( Blueprint::current()->getObjectPath( m_levelId, objectId ) );
	}
}

void LevelEditor::areaCreated() {
	const ushort areaId = AREA_ID( m_levelId, m_createAreaDialog.areaIndex() );
	AreaSpecs &newArea = Blueprint::currentEditable()->areas()[areaId];
	newArea = AreaSpecs::Default();
	newArea.areaId = areaId;
	newArea.echoLevel = (ubyte)m_ui->echoLevelSpinner->value();
	emit gotoAreaEdit( m_createAreaDialog.areaIndex() );
}

void LevelEditor::setLevel( LevelId levelId ) {
	m_initializing = true;
	m_levelId = levelId;
	assert( Blueprint::current()->hasLevel( levelId ) );
	const LevelSpecs &level = Blueprint::current()->levels().find( levelId )->second;
	m_ui->levelNameField->setText( level.name.c_str() );
	m_ui->starSelectCheckbox->setChecked( level.enableStarSelect );
	m_ui->starSelectCheckbox->setEnabled( levelIdToCourseId( levelId ) <= CourseId::RainbowRide );
	m_ui->starNamesGroup->setEnabled( levelIdToCourseId( levelId ) <= CourseId::RainbowRide );
	m_ui->starNameField1->setText( level.starNames[0].c_str() );
	m_ui->starNameField2->setText( level.starNames[1].c_str() );
	m_ui->starNameField3->setText( level.starNames[2].c_str() );
	m_ui->starNameField4->setText( level.starNames[3].c_str() );
	m_ui->starNameField5->setText( level.starNames[4].c_str() );
	m_ui->starNameField6->setText( level.starNames[5].c_str() );
	refreshObjectBanks();

	m_ui->areaList->clear();
	const std::map<ushort,AreaSpecs> &areas = Blueprint::current()->areas();
	for( auto i = areas.lower_bound( AREA_ID( levelId, 0 ) ); i != areas.end() && i->first <= AREA_ID( levelId, 7 ); i++ ) {
		m_ui->areaList->addValueItem<uint>( "Area "s + std::to_string( i->first & 7 ), i->first & 7 );
	}
	m_ui->areaList->clearSelection();
	m_ui->addAreaButton->setEnabled( m_ui->areaList->count() < 8 );
	m_ui->editAreaButton->setEnabled( false );
	m_ui->deleteAreaButton->setEnabled( false );
	m_ui->editObjectButton->setEnabled( false );
	m_ui->deleteObjectButton->setEnabled( false );
	m_createAreaDialog.setLevel( levelId );

	m_ui->spawnArea->setValue( level.marioSpawn.area );
	m_ui->spawnX->setValue( level.marioSpawn.x );
	m_ui->spawnY->setValue( level.marioSpawn.y );
	m_ui->spawnZ->setValue( level.marioSpawn.z );
	m_ui->spawnAngle->setValue( (int)level.marioSpawn.yaw );

	m_ui->soundCarryDistanceSpinner->setValue( level.maxSoundDistance / 3 );
	m_ui->echoLevelSpinner->setValue( level.defaultEchoLevel );

	m_ui->objectList->clear();
	for( const auto &i : level.simpleObjects ) {
		m_ui->objectList->addValueItem<uint>( i.second.name, i.first );
	}
	for( const auto &i : level.partialObjects ) {
		m_ui->objectList->addValueItem<uint>( i.second.name, i.first );
	}
	for( const auto &i : level.advancedObjects ) {
		m_ui->objectList->addValueItem<uint>( i.second.name, i.first );
	}
	m_ui->objectList->sortItems();

	m_ui->pauseWarpGroup->setChecked( level.overridePauseWarp );
	m_ui->pauseWarpLevelSelect->setLevel( level.pauseWarp.level );
	m_ui->pauseWarpAreaSpinner->setValue( (int)level.pauseWarp.area );
	m_ui->pauseWarpWarpSpinner->setValue( (int)level.pauseWarp.warpId );

	m_initializing = false;
}

void LevelEditor::refreshObjectBanks() {
	assert( Blueprint::current()->hasLevel( m_levelId ) );
	const LevelSpecs &level = Blueprint::current()->levels().find( m_levelId )->second;
	m_ui->bank1label->setText( level.objectBanks[0] < 0 ? "None" : objectBank1Names[level.objectBanks[0]] );
	m_ui->bank2label->setText( level.objectBanks[1] < 0 ? "None" : objectBank2Names[level.objectBanks[1]] );
	if( Blueprint::current()->romSpecs().expandAudioHeap ) {
		m_ui->bank3label->setText( "(Expanded Audio Heap)" );
	} else {
		m_ui->bank3label->setText( level.objectBanks[2] < 0 ? "None" : objectBank3Names[level.objectBanks[2]] );
	}

}

void LevelEditor::manageUserData() {
	m_userDataDialog.open( m_levelId );
}
