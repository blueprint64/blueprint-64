#include "src/ui/camera-triggers-dialog.hpp"
#include "ui_camera-triggers-dialog.h"

#include <cassert>
#include <cstring>
#include "src/core/blueprint.hpp"
#include "src/ui/widgets/camera-module-property-select.hpp"
#include "src/ui/util.hpp"

static const CameraPreset s_validModes[] = {
	CameraPreset::OuterRadial,
	CameraPreset::InnerRadial,
	CameraPreset::CloseQarters,
	CameraPreset::Slide,
	CameraPreset::SemiCardinal,
	CameraPreset::FreeRoam,
	CameraPreset::BossFight,
	//CameraPreset::ElasticPivot
};

static const std::map<CameraPreset,int> s_modeIndex = {{
	{ CameraPreset::OuterRadial, 0 },
	{ CameraPreset::InnerRadial, 1 },
	{ CameraPreset::CloseQarters, 2 },
	{ CameraPreset::Slide, 3 },
	{ CameraPreset::SemiCardinal, 4 },
	{ CameraPreset::FreeRoam, 5 },
	{ CameraPreset::BossFight, 6 },
	//{ CameraPreset::ElasticPivot, 7 }
}};

static CameraTrigger s_defaultTrigger = CameraTrigger{
	"",
	CameraTriggerType::Vanilla,
	Vec3s{ 0, 0, 0 },
	Vec3s{ 0, 0, 0 },
	0,
	VanillaCameraOptions{
		CameraPreset::FreeRoam,
		15,
		{ 0 }
	}
};

inline static string sanitizeLabel( const string label ) {
	string safeLabel = "";
	for( char c : label ) {
		if( c == ' ' || c == '_' ) {
			safeLabel += '_';
		} else if(
			(c >= 'a' && c <= 'z') ||
			(c >= 'A' && c <= 'Z') ||
			(c >= '0' && c <= '9')
		) {
			safeLabel += c;
		}
	}
	return safeLabel;
}

static inline QString serializeIds( const Uuid &moduleId, const Uuid &cameraModuleId ) {
	return QString( (moduleId.toString() + '_' + cameraModuleId.toString()).c_str() );
}

static inline std::pair<Uuid,Uuid> deserializeIds( const QString &str ) {
	assert( str.length() == 73 );
	std::string cppStr = str.toStdString() + '\0';
	cppStr[36] = '\0';
	return std::make_pair(
		Uuid::parse( &cppStr.data()[0] ),
		Uuid::parse( &cppStr.data()[37] )
	);
}

CameraTriggersDialog::CameraTriggersDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::CameraTriggersDialog ),
	m_areaId( 0 ),
	m_ignoreEvents( true )
{
	m_ui->setupUi( this );
	for( size_t i = 0; i < s_modeIndex.size(); i++ ) {
		m_ui->modeSelect->addItem( Enum::toString( s_validModes[i] ) );
	}
	m_ignoreEvents = false;
}

inline static CameraTrigger &getTrigger( ushort areaId, Ui::CameraTriggersDialog *ui ) {
	assert( ui->triggerList->currentRow() >= 0 );
	return Blueprint::currentEditable()->areas().at( areaId ).cameraTriggers.at( ui->triggerList->currentRow() );
}

inline static const CameraTrigger &getTriggerImmutable( ushort areaId, Ui::CameraTriggersDialog *ui ) {
	assert( ui->triggerList->currentRow() >= 0 );
	return Blueprint::current()->areas().at( areaId ).cameraTriggers.at( ui->triggerList->currentRow() );
}

CameraTriggersDialog::~CameraTriggersDialog() {
	delete m_ui;
}

void CameraTriggersDialog::open( ushort areaId ) {
	m_areaId = areaId;
	m_ignoreEvents = true;
	m_ui->triggerList->clear();
	for( const CameraTrigger &trigger : Blueprint::current()->areas().at( m_areaId ).cameraTriggers ) {
		m_ui->triggerList->addItem( trigger.name.c_str() );
	}

	const std::map<Uuid,CustomCamera> cameraModules = Blueprint::current()->getCameraModules();
	UiUtil::setItemEnabled( m_ui->effectSelect, 2, !cameraModules.empty() );
	m_ui->moduleSelect->clear();
	for( const auto &i : cameraModules ) {
		m_ui->moduleSelect->addItem(
			i.second.module->name.c_str(),
			QVariant::fromValue( serializeIds( i.second.asmModuleId, i.first ) )
		);
	}

	m_ignoreEvents = false;
	triggerSelected();
	QDialog::open();
}

void CameraTriggersDialog::refreshSettingsTab() {
	const CameraTrigger &trigger = m_ui->triggerList->selectedItems().empty() ? s_defaultTrigger : getTriggerImmutable( m_areaId, m_ui );
	m_ignoreEvents = true;
	switch( trigger.type ) {
		case CameraTriggerType::Vanilla: {
			m_ui->effectSettings->setCurrentIndex( 0 );
			const VanillaCameraOptions &options = std::get<VanillaCameraOptions>( trigger.options );
			m_ui->modeSelect->setCurrentIndex( s_modeIndex.at( options.mode ) );
			m_ui->framesSpinner->setValue( options.frames );

			m_ui->baseAngleOption->setVisible( false );
			m_ui->bossOptions->setVisible( false );
			m_ui->radialOptions->setVisible( false );
			m_ui->pivotOptions->setVisible( false );
			m_ui->transitionOptions->setVisible( true );

			switch( options.mode ) {
				case CameraPreset::SemiCardinal:
					m_ui->baseAngleOption->setVisible( true );
					m_ui->baseAngleSpinner->setValue( (double)options.ext.baseAngle * 360.0 / (double)0x10000 );
					break;
				case CameraPreset::BossFight:
					m_ui->transitionOptions->setVisible( false );
					m_ui->bossOptions->setVisible( true );
					char behaviourString[11];
					std::sprintf( behaviourString, "0x%08x", options.ext.behaviour );
					m_ui->bossBehaviour->setText( behaviourString );
					break;
				case CameraPreset::InnerRadial:
				case CameraPreset::OuterRadial:
					m_ui->radialOptions->setVisible( true );
					m_ui->xCentreSpinner->setValue( options.ext.centre[0] );
					m_ui->zCentreSpinner->setValue( options.ext.centre[1] );
					break;
				case CameraPreset::ElasticPivot:
					m_ui->transitionOptions->setVisible( false );
					m_ui->pivotOptions->setVisible( true );
					m_ui->xPivotSpinner->setValue( options.ext.pivot[0] );
					m_ui->yPivotSpinner->setValue( options.ext.pivot[1] );
					m_ui->zPivotSpinner->setValue( options.ext.pivot[2] );
					break;
				default: break;
			}
			break;
		}
		case CameraTriggerType::Custom: {
			m_ui->effectSettings->setCurrentIndex( 1 );
			const CustomCameraOptions &options = std::get<CustomCameraOptions>( trigger.options );
			m_ui->functionLabel->setText( options.functionLabel.c_str() );
			break;
		}
		case CameraTriggerType::Module: {
			m_ui->effectSettings->setCurrentIndex( 2 );
			const ModuleCameraOptions &options = std::get<ModuleCameraOptions>( trigger.options );
			assert( m_ui->modeSelect->count() > 0 );
			m_ui->moduleFramesSpinner->setValue( options.frames );
			bool moduleFound = false;
			for( int i = 0; i < m_ui->moduleSelect->count(); i++ ) {
				if( options.moduleId == deserializeIds( m_ui->moduleSelect->itemData( i, Qt::UserRole ).toString() ).second ) {
					m_ui->moduleSelect->setCurrentIndex( i );
					moduleFound = true;
					break;
				}
			}

			m_ui->moduleSelect->setEnabled( moduleFound );
			m_ui->moduleFramesSpinner->setEnabled( moduleFound );

			if( moduleFound ) {
				m_ignoreEvents = false;
				cameraModuleSelected();
			} else {
				m_ui->moduleSelect->setCurrentIndex( -1 );
				m_ui->moduleSelect->setCurrentText( "-- Module Removed --;");
			}
			break;
		}
		default: assert( false );
	}
	m_ignoreEvents = false;
}

void CameraTriggersDialog::addTrigger() {
	std::vector<CameraTrigger> &areaTriggers = Blueprint::currentEditable()->areas().at( m_areaId ).cameraTriggers;
	CameraTrigger trigger = s_defaultTrigger;
	trigger.name = "New Camera Trigger "s + std::to_string( areaTriggers.size() + 1 );
	areaTriggers.push_back( trigger );
	m_ignoreEvents = true;
	m_ui->triggerList->addItem( trigger.name.c_str() );
	m_ui->triggerList->setCurrentRow( m_ui->triggerList->count() - 1 );
	m_ignoreEvents = false;
	triggerSelected();
}

void CameraTriggersDialog::deleteTrigger() {
	std::vector<CameraTrigger> &areaTriggers = Blueprint::currentEditable()->areas().at( m_areaId ).cameraTriggers;
	const int index = m_ui->triggerList->currentRow();
	areaTriggers.erase( areaTriggers.begin() + index );
	m_ignoreEvents = true;
	delete m_ui->triggerList->takeItem( index );
	m_ui->triggerList->setCurrentRow( index );
	m_ignoreEvents = false;
	triggerSelected();
}

void CameraTriggersDialog::nameChanged() {
	if( m_ignoreEvents ) return;
	getTrigger( m_areaId, m_ui ).name = m_ui->triggerName->text().toStdString();
	m_ui->triggerList->currentItem()->setText( m_ui->triggerName->text() );
}

void CameraTriggersDialog::regionChanged() {
	CameraTrigger &trigger = getTrigger( m_areaId, m_ui );
	trigger.centre.x = (short)m_ui->regionX->value();
	trigger.centre.y = (short)m_ui->regionY->value();
	trigger.centre.z = (short)m_ui->regionZ->value();
	trigger.radius.x = (short)(m_ui->regionW->value() / 2);
	trigger.radius.y = (short)(m_ui->regionH->value() / 2);
	trigger.radius.z = (short)(m_ui->regionL->value() / 2);
	trigger.angle = (ushort)((m_ui->regionYaw->value() / 360.0) * (double)0x10000);
}

void CameraTriggersDialog::effectChanged() {
	if( m_ignoreEvents ) return;
	CameraTriggerType newType;
	switch( m_ui->effectSelect->currentIndex() ) {
		case 0: newType = CameraTriggerType::Vanilla; break;
		case 1: newType = CameraTriggerType::Custom; break;
		case 2: newType = CameraTriggerType::Module; break;
		default: assert( false );
	}

	CameraTrigger &trigger = getTrigger( m_areaId, m_ui );
	if( trigger.type != newType ) {
		trigger.type = newType;
		// Reset defaults
		switch( newType ) {
			case CameraTriggerType::Vanilla: {
				trigger.options = VanillaCameraOptions{
					CameraPreset::FreeRoam,
					15,
					{ 0 }
				};
				break;
			}
			case CameraTriggerType::Custom: {
				trigger.options = CustomCameraOptions{ "" };
				break;
			}
			case CameraTriggerType::Module: {
				assert( m_ui->modeSelect->count() > 0 );
				trigger.options = ModuleCameraOptions{
					deserializeIds( m_ui->moduleSelect->itemData( 0, Qt::UserRole ).toString() ).second,
					15,
					std::map<string,CameraModulePropertyValue>()
				};
				break;
			}
			default:
				assert( false );
		}
	}
	refreshSettingsTab();
}

void CameraTriggersDialog::effectSettingsChanged() {
	if( m_ignoreEvents ) return;

	CameraTrigger &trigger = getTrigger( m_areaId, m_ui );
	switch( trigger.type ) {
		case CameraTriggerType::Vanilla: {
			VanillaCameraOptions &options = std::get<VanillaCameraOptions>( trigger.options );
			const CameraPreset newMode = s_validModes[m_ui->modeSelect->currentIndex()];

			m_ui->baseAngleOption->setVisible( false );
			m_ui->bossOptions->setVisible( false );
			m_ui->radialOptions->setVisible( false );
			m_ui->pivotOptions->setVisible( false );
			m_ui->transitionOptions->setVisible( true );

			options.mode = newMode;
			options.frames = (short)m_ui->framesSpinner->value();

			switch( options.mode ) {
				case CameraPreset::SemiCardinal:
					m_ui->baseAngleOption->setVisible( true );
					options.ext.baseAngle = (ushort)((m_ui->baseAngleSpinner->value() / 360.0) * (double)0x10000);
					break;
				case CameraPreset::BossFight:
					m_ui->transitionOptions->setVisible( false );
					m_ui->bossOptions->setVisible( true );
					options.ext.behaviour = m_ui->bossBehaviour->value();
					break;
				case CameraPreset::InnerRadial:
				case CameraPreset::OuterRadial:
					m_ui->radialOptions->setVisible( true );
					options.ext.centre[0] = (float)m_ui->xCentreSpinner->value();
					options.ext.centre[1] = (float)m_ui->zCentreSpinner->value();
					break;
				case CameraPreset::ElasticPivot:
					m_ui->transitionOptions->setVisible( false );
					m_ui->pivotOptions->setVisible( true );
					options.ext.pivot[0] = (float)m_ui->xPivotSpinner->value();
					options.ext.pivot[1] = (float)m_ui->yPivotSpinner->value();
					options.ext.pivot[2] = (float)m_ui->zPivotSpinner->value();
					break;
				default: break;
			}
			break;
		}
		case CameraTriggerType::Custom: {
			CustomCameraOptions &options = std::get<CustomCameraOptions>( trigger.options );
			const string enteredName = m_ui->functionLabel->text().toStdString();
			options.functionLabel = sanitizeLabel( enteredName );
			if( options.functionLabel != enteredName ) {
				m_ignoreEvents = true;
				m_ui->functionLabel->setText( options.functionLabel.c_str() );
				m_ignoreEvents = false;
			}
			break;
		}
		case CameraTriggerType::Module: {
			ModuleCameraOptions &options = std::get<ModuleCameraOptions>( trigger.options );
			options.frames = (short)m_ui->moduleFramesSpinner->value();
			break;
		}
		default: assert( false );
	}
}

void CameraTriggersDialog::triggerSelected() {
	if( m_ignoreEvents ) return;

	const bool triggerSelected = !m_ui->triggerList->selectedItems().empty();
	const CameraTrigger &trigger = triggerSelected ? getTriggerImmutable( m_areaId, m_ui ) : s_defaultTrigger;
	m_ui->deleteTriggerButton->setEnabled( triggerSelected );

	m_ignoreEvents = true;
	m_ui->triggerOptions->setEnabled( triggerSelected );
	m_ui->triggerName->setText( trigger.name.c_str() );
	m_ui->regionX->setValue( trigger.centre.x );
	m_ui->regionY->setValue( trigger.centre.y );
	m_ui->regionZ->setValue( trigger.centre.z );
	m_ui->regionW->setValue( (int)trigger.radius.x * 2 );
	m_ui->regionH->setValue( (int)trigger.radius.y * 2 );
	m_ui->regionL->setValue( (int)trigger.radius.z * 2 );
	m_ui->regionYaw->setValue( (double)trigger.angle * 360.0 / (double)0x10000 );
	switch( trigger.type ) {
		case CameraTriggerType::Vanilla: m_ui->effectSelect->setCurrentIndex( 0 ); break;
		case CameraTriggerType::Custom: m_ui->effectSelect->setCurrentIndex( 1 ); break;
		case CameraTriggerType::Module: m_ui->effectSelect->setCurrentIndex( 2 ); break;
		default: assert( false );
	}
	m_ignoreEvents = false;
	refreshSettingsTab();
}

void CameraTriggersDialog::cameraModuleSelected() {
	if( m_ignoreEvents || m_ui->moduleSelect->count() == 0 ) return;

	const std::pair<Uuid,Uuid> moduleIds = deserializeIds( m_ui->moduleSelect->currentData( Qt::UserRole ).toString() );
	CameraTrigger &currentTrigger = Blueprint::currentEditable()->areas().at( m_areaId ).cameraTriggers.at( m_ui->triggerList->currentRow() );
	assert( currentTrigger.type == CameraTriggerType::Module );

	UiUtil::clearLayout( m_ui->moduleParameters->layout() );
	ModuleCameraOptions &options = std::get<ModuleCameraOptions>( currentTrigger.options );
	if( options.moduleId != moduleIds.second ) {
		options.moduleId = moduleIds.second;
		options.moduleParams.clear();
	}

	for( const CameraModule &module : Blueprint::current()->asmModules().at( moduleIds.first ).definition.cameraModes ) {
		if( module.id != moduleIds.second ) continue;
		for( const AsmModuleProperty &prop : module.parameters ) {
			m_ui->moduleParameters->layout()->addWidget(
				new CameraModulePropertySelect( m_ui->moduleParameters, &options.moduleParams, prop )
			);
		}
		break;
	}

	static_cast<QVBoxLayout*>( m_ui->moduleParameters->layout() )->addStretch( 1 );
}
