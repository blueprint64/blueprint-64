#ifndef SRC_UI_ASM_MODULE_EDITOR_HPP_
#define SRC_UI_ASM_MODULE_EDITOR_HPP_

#include <QWidget>
#include "src/core/asm-modules.hpp"

namespace Ui {
	class AsmModuleEditor;
}

class AsmModuleEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::AsmModuleEditor *m_ui;
	bool m_initializing;

	void reloadAvailableModulesList();
	void reloadActiveModulesList();

	inline void reloadModuleLists() {
		reloadAvailableModulesList();
		reloadActiveModulesList();
	}

	public:
	explicit AsmModuleEditor( QWidget *parent = nullptr );
	~AsmModuleEditor();

	protected:
	virtual void showEvent( QShowEvent *event ) override {
		reloadModuleLists();
		QWidget::showEvent( event );
	}

	public slots:
	void availableSelected();
	void activeSelected();
	void addOrRemoveModule();
	void removeModule();
	void updateModule();
	void installModule();
	void uninstallModule();
	void install();
	void showAllToggled( bool allVersions );

	void blueprintChanged();

	signals:
	void developModule();

};

#endif /* SRC_UI_ASM_MODULE_EDITOR_HPP_ */
