#ifndef SRC_UI_TWEAK_SETTINGS_HPP_
#define SRC_UI_TWEAK_SETTINGS_HPP_

#include <QWidget>
#include "src/core/tweak.hpp"

namespace Ui {
	class TweakSettingsWidget;
}

class TweakSettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::TweakSettingsWidget *m_ui;
	bool m_disableEvents;

	void refresh();

	public slots:
	void addTweak();
	void removeTweak();
	void availableTweakSelected();
	void appliedTweakSelected();
	void lockLists();
	void unlockLists();
	void manageTweakPacks();

	public:
	explicit TweakSettingsWidget( QWidget *parent = nullptr );
	~TweakSettingsWidget();

	void showEvent( QShowEvent *event ) override {
		refresh();
		QWidget::showEvent( event );
	}

	inline void blueprintChanged() { refresh(); }

};


#endif /* SRC_UI_TWEAK_SETTINGS_HPP_ */
