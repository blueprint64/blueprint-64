#include "src/ui/image-filter.hpp"

#include <algorithm>
#include "src/polyfill/fastmath.hpp"

class IPixelTransform {
	public:
	virtual uint apply( uint pixel ) const = 0;
	virtual ~IPixelTransform() {}
};

static inline ubyte getValue( uint pixel ) {
	return (ubyte)std::max({ pixel & 0xFF, (pixel >> 8) & 0xFF, (pixel >> 16) & 0xFF });
}

static inline ubyte precision5( ubyte value ) {
	return ((value >> 3) << 3) | (value >> 5);
}

static inline ubyte precision4( ubyte value ) {
	const ubyte v4 = value >> 4;
	return (v4 << 4) | v4;
}

static inline ubyte precision3( ubyte value ) {
	return ((value >> 5) << 5) | ((value >> 3) & 0x1C) | (value >> 6);
}

static inline ubyte multiply( ubyte a, ubyte b ) {
	return (ubyte)((((ushort)a + 1) * (ushort)b) >> 8);
}

static inline uint argb( ubyte a, ubyte r, ubyte g, ubyte b ) {
	return ((uint)a << 24) | ((uint)r << 16) | ((uint)g << 8) | (uint)b;
}

static inline ubyte getAlpha( uint colour ) {
	return (ubyte)(colour >> 24);
}

static inline ubyte getRed( uint colour ) {
	return (ubyte)((colour >> 16) & 0xFF);
}

static inline ubyte getGreen( uint colour ) {
	return (ubyte)((colour >> 8) & 0xFF);
}

static inline ubyte getBlue( uint colour ) {
	return (ubyte)(colour & 0xFF);
}

class PT_FormatRGBA16 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		return argb(
			getAlpha( pixel ) >= 0x80 ? 0xFF : 0x00,
			precision5( getRed( pixel ) ),
			precision5( getGreen( pixel ) ),
			precision5( getBlue( pixel ) )
		);
	}
};

class PT_FormatIA16 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		const ubyte value = getValue( pixel );
		return argb( getAlpha( pixel ), value, value, value );
	}
};

class PT_FormatIA8 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		const ubyte value = precision4( getValue( pixel ) );
		return argb( precision4( getAlpha( pixel ) ), value, value, value );
	}
};

class PT_FormatIA4 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		const ubyte value = precision3( getValue( pixel ) );
		return argb( getAlpha( pixel ) >= 0x80 ? 0xFF : 0x00, value, value, value );
	}
};

class PT_FormatI8 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		const ubyte value = getValue( pixel );
		return argb( 0xFF, value, value, value );
	}
};

class PT_FormatI4 final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		const ubyte value = precision4( getValue( pixel ) );
		return argb( 0xFF, value, value, value );
	}
};

class PT_colourize final : public IPixelTransform {
	private:
	ColourRGB24 m_colour;

	public:
	PT_colourize( const ColourRGB24 &colour ) : m_colour( colour ) {}

	uint apply( uint pixel ) const override {
		return argb(
			getAlpha( pixel ),
			multiply( getRed( pixel ), m_colour.red ),
			multiply( getGreen( pixel ), m_colour.green ),
			multiply( getBlue( pixel ), m_colour.blue )
		);
	}
};

class PT_removeAlpha final : public IPixelTransform {
	public:
	uint apply( uint pixel ) const override {
		return pixel | 0xFF000000;
	}
};

static void transformPixels( const IPixelTransform &transform, QImage &image ) {
	if( image.format() != QImage::Format_ARGB32 ) {
		image = image.convertToFormat( QImage::Format_ARGB32 );
	}

	uint *pixels = (uint*)image.bits();
	const size_t numPixels = (size_t)image.width() * (size_t)image.height();
	for( size_t i = 0; i < numPixels; i++ ) {
		pixels[i] = transform.apply( pixels[i] );
	}
}

void ImageFilter::applyColourFormat( QImage &image, TextureFormat format ) {
	if( image.isNull() ) return;

	switch( format ) {
		case TextureFormat::RGBA16: transformPixels( PT_FormatRGBA16(), image ); break;
		case TextureFormat::IA16: transformPixels( PT_FormatIA16(), image ); break;
		case TextureFormat::IA8: transformPixels( PT_FormatIA8(), image ); break;
		case TextureFormat::IA4: transformPixels( PT_FormatIA4(), image ); break;
		case TextureFormat::I8: transformPixels( PT_FormatI8(), image ); break;
		case TextureFormat::I4: transformPixels( PT_FormatI4(), image ); break;
		default: break;
	}
}

void ImageFilter::colourize( QImage &image, const ColourRGB24 &colour ) {
	if( !image.isNull() ) {
		transformPixels( PT_colourize( colour ), image );
	}
}

void ImageFilter::removeAlpha( QImage &image ) {
	if( !image.isNull() ) {
		transformPixels( PT_removeAlpha(), image );
	}
}

static inline uint nearestPowerOf2( uint n ) {
	const uint L = 2 << ilog2( n );
	const uint H = L << 1;
	return ((n - L) <= (H - n)) ? L : H;
}

void ImageFilter::makeValidTextureSize( QImage &image ) {
	if( image.isNull() || image.width() == 0 || image.height() == 0 ) {
		return;
	}

	if( isPowerOf2( image.width() ) && isPowerOf2( image.height() ) && image.width() * image.height() <= 8192 ) {
		return;
	}

	uint targetWidth = nearestPowerOf2( (uint)image.width() );
	uint targetHeight = nearestPowerOf2( (uint)image.height() );

	while( targetWidth * targetHeight > 8192 ) {
		if( targetWidth > 2 ) {
			targetWidth >>= 1;
		}
		if( targetHeight > 1 ) {
			targetHeight >>= 1;
		}
	}

	while( targetHeight > 1 ) {
		const uint maxBitsPerPixel = 2 << ilog2( (0x8000u / ((uint)targetWidth * (uint)targetHeight)) );
		if( maxBitsPerPixel * targetWidth < 64 ) {
			targetWidth <<= 1;
			targetHeight >>= 1;
		} else break;
	}

	image = image.scaled( targetWidth, targetHeight, Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
}
