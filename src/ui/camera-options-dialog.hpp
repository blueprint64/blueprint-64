#include <QDialog>
#include "src/core/data/camera.hpp"

namespace Ui {
	class CameraOptionsDialog;
}

class CameraOptionsDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::CameraOptionsDialog *m_ui;

	protected:
	virtual void showEvent( QShowEvent *event ) override;

	public:
	explicit CameraOptionsDialog( QWidget *parent = nullptr );
	virtual ~CameraOptionsDialog();

	void setVanillaOptions( const VanillaCameraOptions &options );

	void updateOptions( CameraTrigger &trigger ) const;
	void run( CameraTrigger &trigger );

};
