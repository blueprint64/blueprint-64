#include "src/ui/asm-module-editor.hpp"
#include "ui_asm-module-editor.h"

#include <QMessageBox>
#include <deque>
#include "src/ui/asm-module-info.hpp"
#include "src/ui/util.hpp"
#include "src/core/blueprint.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/polyfill/tar.hpp"
#include "src/core/util.hpp"
#include "src/core/blueprint-module-manager.hpp"
#include "src/core/package.hpp"

AsmModuleEditor::AsmModuleEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::AsmModuleEditor ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	FORWARD_SIGNAL( m_ui->developButton, this, clicked(), developModule() );
	m_initializing = false;
}

AsmModuleEditor::~AsmModuleEditor() {
	delete m_ui;
}

static inline string getDisplayName( const AsmModuleDefinition &module ) {
	return module.name + " (v" + module.version.toString() + ')';
}

static inline QString serializeIdAndVersion( const Uuid &id, const Version &version ) {
	return (id.toString() + '_' + version.toString()).c_str();
}

static inline std::pair<Uuid,Version> deserializeIdAndVersion( const QString &str ) {
	const string idVer = str.toStdString();
	return {
		Uuid::parse( idVer.substr( 0, 36 ) ),
		Version::parse( idVer.substr( 37 ) )
	};
}

void AsmModuleEditor::blueprintChanged() {
	m_initializing = true;
	m_ui->availableModuleList->clearSelection();
	m_ui->activeModuleList->clearSelection();
	m_initializing = false;

	m_ui->moduleInfo->setEnabled( false );
	m_ui->moduleInfo->setVisible( false );

	AsmModuleStore::reload();
	reloadModuleLists();
}

void AsmModuleEditor::reloadAvailableModulesList() {
	QString previousSelection = m_ui->availableModuleList->selectedItems().empty() ? QString() : m_ui->availableModuleList->currentData<QString>();

	m_initializing = true;
	m_ui->availableModuleList->clear();
	const bool allVersions = m_ui->showAllCheckbox->isChecked();
	for( const auto &i : AsmModuleStore::allModules() ) {
		if( allVersions ) {
			for( const auto &j : i.second ) {
				if( Blueprint::current()->hasModule( i.first, j.first ) ) continue;
				m_ui->availableModuleList->addValueItem<QString>(
					getDisplayName( j.second ),
					serializeIdAndVersion( i.first, j.first )
				);
			}
		} else if( !i.second.empty() ) {
			const AsmModuleDefinition &latest = i.second.rbegin()->second;
			if( Blueprint::current()->hasModule( latest.id, latest.version ) ) continue;
			m_ui->availableModuleList->addValueItem<QString>(
				getDisplayName( latest ),
				serializeIdAndVersion( latest.id, latest.version )
			);
			//TODO: show in red if missing dependencies
		}
	}

	m_ui->availableModuleList->sortItems();
	m_ui->availableModuleList->clearSelection();

	if( previousSelection.isNull() ) {
		m_initializing = false;
		return;
	}

	for( int i = 0; i < m_ui->availableModuleList->count(); i++ ) {
		if( m_ui->availableModuleList->item( i )->data( Qt::UserRole ).toString() == previousSelection ) {
			m_ui->availableModuleList->setCurrentRow( i );
			break;
		}
	}

	m_initializing = false;
	availableSelected();
}

void AsmModuleEditor::reloadActiveModulesList() {
	QString previousSelection = m_ui->activeModuleList->selectedItems().empty() ? QString() : m_ui->activeModuleList->currentData<QString>();

	m_initializing = true;
	m_ui->activeModuleList->clear();
	for( const auto &i : Blueprint::current()->asmModules() ) {
		m_ui->activeModuleList->addValueItem(
			getDisplayName( i.second.definition ),
			serializeIdAndVersion( i.first, i.second.definition.version )
		);
	}

	m_ui->activeModuleList->sortItems();
	m_ui->activeModuleList->clearSelection();

	if( previousSelection.isNull() ) {
		m_initializing = false;
		return;
	}

	for( int i = 0; i < m_ui->activeModuleList->count(); i++ ) {
		if( m_ui->activeModuleList->item( i )->data( Qt::UserRole ).toString() == previousSelection ) {
			m_ui->activeModuleList->setCurrentRow( i );
			break;
		}
	}

	m_initializing = false;
	activeSelected();
}

void AsmModuleEditor::availableSelected() {
	if( m_initializing ) return;

	if( m_ui->availableModuleList->selectedItems().empty() ) {
		m_ui->moduleInfo->setEnabled( false );
		m_ui->moduleInfo->setVisible( false );
		return;
	} else {
		m_initializing = true;
		m_ui->activeModuleList->clearSelection();
		m_initializing = false;

		const auto mod = deserializeIdAndVersion( m_ui->availableModuleList->currentData<QString>() );
		m_ui->moduleInfo->setEnabled( true );
		m_ui->moduleInfo->setVisible( true );
		m_ui->moduleInfo->setModule( mod.first, mod.second );
	}
}

void AsmModuleEditor::activeSelected() {
	if( m_initializing ) return;

	if( m_ui->activeModuleList->selectedItems().empty() ) {
		m_ui->moduleInfo->setEnabled( false );
		m_ui->moduleInfo->setVisible( false );
		return;
	} else {
		m_initializing = true;
		m_ui->availableModuleList->clearSelection();
		m_initializing = false;

		const auto mod = deserializeIdAndVersion( m_ui->activeModuleList->currentData<QString>() );
		m_ui->moduleInfo->setModule( mod.first, mod.second );
		m_ui->moduleInfo->setEnabled( true );
		m_ui->moduleInfo->setVisible( true );
	}
}

void AsmModuleEditor::addOrRemoveModule() {
	if( m_ui->availableModuleList->selectedItems().empty() ) {
		removeModule();
		return;
	}

	const auto mod = deserializeIdAndVersion( m_ui->availableModuleList->currentData<QString>() );
	const AsmModuleDefinition &module = AsmModuleStore::getModuleVersions( mod.first ).at( mod.second );

	string conflictMessage;
	if( BlueprintModules::tryAddModule( module, conflictMessage ) ) {
		reloadModuleLists();
	} else {
		QMessageBox::critical( this, "Failed to Add Module", ("ASM Module could not be added to blueprint:\n\n"s + conflictMessage).c_str(), QMessageBox::Ok );
	}

}

void AsmModuleEditor::removeModule() {
	if( m_ui->activeModuleList->selectedItems().empty() ) {
		return;
	}

	const auto mod = deserializeIdAndVersion( m_ui->activeModuleList->currentItem()->data( Qt::UserRole ).toString() );
	m_ui->activeModuleList->clearSelection();

	const Uuid &moduleId = mod.first;
	const Version &version = mod.second;

	if( !AsmModuleStore::hasModuleVersion( moduleId, version ) ) {
		if( QMessageBox::warning( this, "Warning", "Since you do not have this version of the module installed to your module library, you will not "
			"be able to restore it after removing it. Are you sure you want to remove this module?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No
		) != QMessageBox::Yes ) {
			return;
		}
	}

	HashSet<Uuid> modulesToRemove;
	modulesToRemove.insert( moduleId );

	bool cascade = true;
	while( cascade ) {
		cascade = false;
		for( const auto &i : Blueprint::current()->asmModules() ) {
			if( modulesToRemove.count( i.first ) > 0 ) continue;
			for( const AsmModuleDependency &d : i.second.definition.dependencies ) {
				if( modulesToRemove.count( d.id ) > 0 ) {
					modulesToRemove.insert( i.first );
					cascade = true;
					break;
				}
			}
		}
	}

	if( modulesToRemove.size() > 1 ) {
		string message = "The following modules will be removed:";
		for( const Uuid &id : modulesToRemove ) {
			message += "\n • ";
			message += Blueprint::current()->asmModules().at( id ).definition.name;
		}

		if( QMessageBox::information( this, "Confirm Remove", message.c_str(), QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel ) != QMessageBox::Ok ) {
			return;
		}
	}

	for( const Uuid &id : modulesToRemove ) {
		Blueprint::currentEditable()->asmModules().erase( id );
		const fs::path modulePath = Blueprint::current()->getModulePath( id );
		fs::forceDeleteRecursive( modulePath );
	}

	reloadModuleLists();
}

void AsmModuleEditor::updateModule() {
	//TODO
}

static inline bool tryInstallModule( QWidget *parent, const fs::path &modulePath ) {
	try {
		AsmModuleStore::installModule( modulePath );
		return true;
	} catch( const std::exception &ex ) {
		QMessageBox::critical( parent, "Installation Failed", ("Failed to install ASM module:\n"s + ex.what()).c_str(), QMessageBox::Ok );
		return false;
	}
}

void AsmModuleEditor::installModule() {
	assert( !m_ui->activeModuleList->selectedItems().empty() );

	const fs::path &modulePath = Blueprint::current()->getModulePath(
		deserializeIdAndVersion( m_ui->activeModuleList->currentData<QString>() ).first
	);

	if( tryInstallModule( this, modulePath ) ) {
		reloadAvailableModulesList();
	}
}

void AsmModuleEditor::uninstallModule() {
	if( QMessageBox::question( this, "Uninstall Module", "Are you sure you want to uninstall this module?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No ) != QMessageBox::Yes ) {
		return;
	}

	const auto mod =  deserializeIdAndVersion( m_ui->availableModuleList->currentData<QString>() );
	AsmModuleStore::uninstallModule( mod.first, mod.second );
	reloadAvailableModulesList();
}

void AsmModuleEditor::install() {
	const fs::path modulePath = FileDialog::getFileOpen(
		"Install ASM Modules", {
		{ "ASM Modules", "*.bbpmod;*.tar.gz;module.json" },
		{ "Compressed Modules", "*.bbpmod;*.tar.gz" },
		{ "Extracted Modules", "module.json" }
	});

	if( modulePath.empty() ) {
		return;
	}

	if( modulePath.filename().u8string() != "module.json" ) {
		const fs::path tempDir = fs::to_path( createTemporaryDirectory() );
		if( !Package::extract( modulePath, tempDir ) ) {
			QMessageBox::critical( this, "Installation Failed", "Failed to install ASM module:\nError extracting archive.", QMessageBox::Ok );
			return;
		}

		if( !fs::exists( tempDir / "module.json" ) ) {
			QMessageBox::critical( this, "Installation Failed", "Failed to install ASM module:\nMissing module.json file.", QMessageBox::Ok );
			return;
		}

		if( tryInstallModule( this, tempDir / "module.json" ) ) {
			reloadAvailableModulesList();
		}
		fs::forceDeleteRecursive( tempDir );
	} else if( tryInstallModule( this, modulePath ) ) {
		reloadAvailableModulesList();
	}

}

void AsmModuleEditor::showAllToggled( [[maybe_unused]] bool allVersions ) {
	reloadAvailableModulesList();
}
