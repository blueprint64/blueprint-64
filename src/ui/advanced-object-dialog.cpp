#include "src/ui/advanced-object-dialog.hpp"
#include "ui_advanced-object-dialog.h"

#include "src/core/blueprint.hpp"

AdvancedObjectDialog::AdvancedObjectDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::AdvancedObjectDialog ),
	m_levelId( LevelId::INVALID ),
	m_objectId( 0 )
{
	m_ui->setupUi( this );
}

AdvancedObjectDialog::~AdvancedObjectDialog() {
	delete m_ui;
}

void AdvancedObjectDialog::setup( LevelId levelId, objectId objectId ) {
	m_levelId = levelId;
	m_objectId = objectId;

	const AdvancedObjectSpecs &object = Blueprint::current()->levels().at( levelId ).advancedObjects.at( objectId );
	int modelIndex = 0;
	int dataIndex = 0;

	int i = 0;
	m_ui->modelIdSelect->clear();
	for( ubyte modelId : Blueprint::current()->getFreeModelIds( levelId, object.modelId ) ) {
		m_ui->modelIdSelect->addItem( std::to_string( (uint)modelId ).c_str(), QVariant::fromValue<uint>( modelId ) );
		if( modelId == object.modelId ) {
			modelIndex = i;
		}
		i++;
	}

	i = 0;
	m_ui->dataSelect->clear();
	for( const auto &j : Blueprint::current()->levels().at( levelId ).userData ) {
		m_ui->dataSelect->addItem( j.second.name.c_str(), QVariant::fromValue<uint>( j.first ) );
		if( j.first == object.customDataId ) {
			dataIndex = i;
		}
		i++;
	}

	m_ui->nameField->setText( object.name.c_str() );
	m_ui->modelIdSelect->setCurrentIndex( modelIndex );
	m_ui->dataSelect->setCurrentIndex( dataIndex );
}

void AdvancedObjectDialog::accept() {
	AdvancedObjectSpecs &object = Blueprint::currentEditable()->levels().at( m_levelId ).advancedObjects.at( m_objectId );
	object.name = m_ui->nameField->text().toStdString();
	object.modelId = (ubyte)m_ui->modelIdSelect->currentData( Qt::UserRole ).toUInt();
	object.customDataId = m_ui->dataSelect->currentData( Qt::UserRole ).toUInt();
	QDialog::accept();
}
