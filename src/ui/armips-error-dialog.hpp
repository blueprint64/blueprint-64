#ifndef SRC_UI_ARMIPS_ERROR_DIALOG_HPP_
#define SRC_UI_ARMIPS_ERROR_DIALOG_HPP_

#include <QDialog>
#include <string>

namespace Ui {
	class ArmipsErrorDialog;
}

class ArmipsErrorDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ArmipsErrorDialog *m_ui;

	public:
	explicit ArmipsErrorDialog( QWidget *parent = nullptr );
	~ArmipsErrorDialog();

	bool getResponse( const std::string &armipsOutput );

};



#endif /* SRC_UI_ARMIPS_ERROR_DIALOG_HPP_ */
