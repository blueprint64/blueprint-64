#include "src/ui/import-model-dialog.hpp"
#include "ui_import-model-dialog.h"

#include <QLineEdit>
#include <QCheckBox>
#include <cassert>
#include "src/polyfill/file-dialog.hpp"
#include "src/core/storage.hpp"
#include "src/core/import/model-importer.hpp"
#include "src/ui/util.hpp"

ImportModelDialog::ImportModelDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ImportModelDialog ),
	m_rows( 0 )
{
	m_ui->setupUi( this );
}

ImportModelDialog::~ImportModelDialog() {
	delete m_ui;
}

void ImportModelDialog::import() {
	accept();

	ModelImportedEvent event;
	event.scale = m_ui->scaleSelect->value();
	event.models.reserve( m_rows );
	for( int i = 1; i <= m_rows; i++ ) {
		QString path = static_cast<const QLineEdit*>( m_ui->importTable->itemAtPosition( i, 0 )->widget() )->text();
		if( path.isNull() || path.isEmpty() ) continue;
		event.models.push_back( ModelFileImportInfo{
			path.toStdString(),
			static_cast<const QCheckBox*>( m_ui->importTable->itemAtPosition( i, 2 )->widget() )->isChecked(),
			static_cast<const QCheckBox*>( m_ui->importTable->itemAtPosition( i, 3 )->widget() )->isChecked()
		});
	}
	emit modelImported( event );
}

void ImportModelDialog::showEvent( QShowEvent *event ) {
	QGridLayout *layout = m_ui->importTable;
	for( ; m_rows > 0; m_rows-- ) {
		for( int i = 0; i < 5; i++ ) {
			UiUtil::deleteItemAt( layout, m_rows, i );
		}
	}
	addFile();
	m_ui->scaleSelect->setValue( AppPreferences::current().defaultScale );
	m_ui->importButton->setEnabled( false );
	QDialog::showEvent( event );
}

void ImportModelDialog::addFile() {
	QLineEdit *filePathField = new QLineEdit( this );
	QPushButton *browseButton = new QPushButton( this );
	QCheckBox *isVisibleCheckbox = new QCheckBox( this );
	QCheckBox *isSolidCheckbox = new QCheckBox( this );
	QPushButton *deleteButton = new QPushButton( this );

	m_rows++;
	m_ui->importTable->addWidget( filePathField, m_rows, 0 );
	m_ui->importTable->addWidget( browseButton, m_rows, 1 );
	m_ui->importTable->addWidget( isVisibleCheckbox, m_rows, 2 );
	m_ui->importTable->addWidget( isSolidCheckbox, m_rows, 3 );
	m_ui->importTable->addWidget( deleteButton, m_rows, 4 );

	m_ui->importTable->setAlignment( isVisibleCheckbox, Qt::AlignHCenter );
	m_ui->importTable->setAlignment( isSolidCheckbox, Qt::AlignHCenter );

	filePathField->setReadOnly( true );
	browseButton->setText( "Browse" );
	isVisibleCheckbox->setChecked( true );
	isSolidCheckbox->setChecked( true );
	deleteButton->setIcon( QIcon::fromTheme( "delete" ) );
	deleteButton->setAccessibleName( "Delete" );
	deleteButton->setFlat( true );
	deleteButton->setVisible( m_rows > 1 );

	QObject::connect( browseButton, &QPushButton::clicked, this, [=](){
		const fs::path modelFile = FileDialog::getFileOpen( "Import Model", {{ "Wavefront", "*.obj" }} );
		if( !modelFile.empty() ) {
			filePathField->setText( modelFile.u8string().c_str() );
		}
		checkCanImport();
	});

	QObject::connect( deleteButton, &QPushButton::clicked, this, [=](){
		int row, _scratch;
		m_ui->importTable->getItemPosition( m_ui->importTable->indexOf( deleteButton ), &row, &_scratch, &_scratch, &_scratch );
		removeRow( row );
	});

	if( m_rows > 1 ) {
		static_cast<QPushButton*>( m_ui->importTable->itemAtPosition( 1, 4 )->widget() )->setVisible( true );
	}
}

void ImportModelDialog::removeRow( int row ) {
	assert( row > 0 && row <= m_rows );
	for( int i = 0; i < 5; i++ ) {
		m_ui->importTable->itemAtPosition( row, i )->widget()->deleteLater();
	}

	for( int i = row; i < m_rows; i++ ) {
		for( int j = 0; j < 5; j++ ) {
			QLayoutItem *item = m_ui->importTable->itemAtPosition( i, j );
			m_ui->importTable->removeItem( item );
			delete item;
			m_ui->importTable->addWidget( m_ui->importTable->itemAtPosition( i+1, j )->widget(), i, j );
		}
		m_ui->importTable->setAlignment( m_ui->importTable->itemAtPosition( i, 2 )->widget(), Qt::AlignHCenter );
		m_ui->importTable->setAlignment( m_ui->importTable->itemAtPosition( i, 3 )->widget(), Qt::AlignHCenter );
	}

	for( int i = 0; i < 5; i++ ) {
		QLayoutItem *item = m_ui->importTable->itemAtPosition( m_rows, i );
		m_ui->importTable->removeItem( item );
		delete item;
	}

	if( --m_rows == 1 ) {
		static_cast<QPushButton*>( m_ui->importTable->itemAtPosition( 1, 4 )->widget() )->setVisible( false );
	}

	checkCanImport();
}

void ImportModelDialog::checkCanImport() {
	for( int i = 1; i <= m_rows; i++ ) {
		if( !static_cast<QLineEdit*>( m_ui->importTable->itemAtPosition( i, 0 )->widget() )->text().isEmpty() ) {
			m_ui->importButton->setEnabled( true );
			return;
		}
	}
	m_ui->importButton->setEnabled( false );
}
