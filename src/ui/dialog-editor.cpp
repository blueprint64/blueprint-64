#include "src/ui/dialog-editor.hpp"
#include "ui_dialog-editor.h"

#include <QFontDatabase>
#include <QHBoxLayout>
#include "src/core/text.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/sound.hpp"

DialogEditor::DialogEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::DialogEditor ),
	m_dialogId( 0 ),
	m_nonUserUpdate( false )
{
	m_ui->setupUi( this );
	m_ui->textPreview->document()->setDocumentMargin( 4 );

	QHBoxLayout *trays[3] = {
		m_ui->specialCharsRow1,
		m_ui->specialCharsRow2,
		m_ui->specialCharsRow3
	};

	const int fontId = QFontDatabase::addApplicationFont( ":/fonts/sm64.otb" );
	const QFont sm64font = (fontId >= 0) ? QFont( QFontDatabase::applicationFontFamilies( fontId ).at( 0 ) ) : QFont( "Blueprint SM64" );

	m_ui->textPreview->setFont( sm64font );
	const char specialCharacters[] = "[]|{}@`\"*+#$^=<>_";
	for( int i = 0; i < 17; i++ ) {
		const char c[2] = { specialCharacters[i], '\0' };
		QPushButton *button = new QPushButton( c, this );
		button->setFlat( true );
		button->setCursor( Qt::PointingHandCursor );
		button->setBaseSize( 16, 24 );
		button->setMaximumSize( 16, 24 );
		button->setMinimumSize( 16, 24 );
		button->setFont( sm64font );
		connect( button, &QPushButton::clicked, this, [=](){
			m_ui->textEditor->insertPlainText( c );
		});
		m_specialCharacterButtons[i] = button;
		trays[i/6]->addWidget( button );
	}

	for( int i = 0; i < 170; i++ ) {
		m_ui->dialogList->addItem(
			(std::to_string( i + 1 ) + ". " + VanillaText::dialog()[i].name ).c_str()
		);
	}

	connect( &m_soundSelectDialog, SIGNAL(accepted()), this, SLOT(soundTableItemChanged()) );
	m_ui->soundTable->setCurrentRow( 0 );
	m_ui->sideEffectSection->setVisible( false );
}

DialogEditor::~DialogEditor() {
	delete m_ui;
	for( int i = 0; i < 17; i++ ) {
		delete m_specialCharacterButtons[i];
	}
}

void DialogEditor::blueprintChanged() {
	const Blueprint *blueprint = Blueprint::current();
	const std::vector<DialogEntry> &dialogs = blueprint->dialogs();
	for( int i = 0; i < 170; i++ ) {
		m_ui->dialogList->item( i )->setText(
			(std::to_string( i ) + ". " + dialogs[i].name).c_str()
		);
	}
	const std::array<uint,16> &sfxTable = blueprint->dialogSfxTable();
	for( int i = 0; i < 16; i++ ) {
		QListWidgetItem *row = m_ui->soundTable->item( i );
		row->setData( Qt::UserRole, QVariant::fromValue<uint>( sfxTable[i] ) );
		const string soundName = std::to_string( i ) + " - " + getSoundName( sfxTable[i] );
		row->setText( soundName.c_str() );
		m_ui->sfxSelect->setItemText( i+1, soundName.c_str() );
	}
	m_ui->genericLevelName->setText( blueprint->genericLevelName().c_str() );
	m_ui->genericStarName->setText( blueprint->genericStarName().c_str() );
	dialogSelected();
}

void DialogEditor::nameChanged() {
	m_ui->dialogList->item( m_dialogId )->setText(
		(std::to_string( m_dialogId ) + ". " + m_ui->dialogName->text().toStdString()).c_str()
	);
}

void DialogEditor::textChanged() {
	string filteredText;
	filteredText.reserve( m_ui->textEditor->toPlainText().length() );

	bool hasInvalidChars = false;
	for( QChar character : m_ui->textEditor->toPlainText() ) {
		const ushort c = character.unicode();
		if( c != 0xA && ( c > 0x7E || c < 0x20 || c == 0x2F || c == 0x3B || c == 0x5C ) ) {
			hasInvalidChars = true;
		} else {
			filteredText += (char)c;
		}
	}

	if( hasInvalidChars ) {
		m_ui->textEditor->setPlainText( filteredText.c_str() );
	}

	m_ui->textPreview->setPlainText( filteredText.c_str() );
	dialogEntryChanged();
}

void DialogEditor::dialogSelected() {
	m_dialogId = m_ui->dialogList->currentRow();
	if( m_dialogId == 0xFF ) {
		m_dialogId = 0;
		m_ui->dialogList->setCurrentRow( 0 );
	}
	const DialogEntry &dialog = Blueprint::current()->dialogs().at( m_dialogId );
	m_nonUserUpdate = true;
	m_ui->dialogName->setText( dialog.name.c_str() );
	m_ui->halignSelect->setCurrentIndex( (int)dialog.halign );
	m_ui->valignSelect->setCurrentIndex( (int)dialog.valign );
	m_ui->hoffsetSelect->setValue( (int)dialog.hoffset );
	m_ui->voffsetSelect->setValue( (int)dialog.voffset );
	m_ui->pageSizeSelect->setValue( (int)dialog.pageSize );
	m_ui->sfxSelect->setCurrentIndex( dialog.soundEffect + 1 );
	m_ui->textEditor->setPlainText( dialog.text.c_str() );
	m_ui->textPreview->setPlainText( dialog.text.c_str() );

	m_ui->hoffsetSelect->setEnabled( dialog.halign != HAlign::Centre );
	m_ui->voffsetSelect->setEnabled( dialog.valign != VAlign::Centre );

	DialogSideEffect sideEffect = Text::getSideEffect( m_dialogId );
	m_ui->sideEffectSection->setVisible( sideEffect != DialogSideEffect::None );
	m_ui->sideEffectIcon->setPixmap( QIcon::fromTheme(
		sideEffect == DialogSideEffect::StopBossMusic ? "audio-volume-muted" : "audio-volume-high"
	).pixmap( 16 ) );
	switch( sideEffect ) {
		case DialogSideEffect::PlayPuzzleSolvedJingle:
			m_ui->sideEffectLabel->setText( "Plays the puzzle solved jingle." ); break;
		case DialogSideEffect::PlayRaceStartFanfare:
			m_ui->sideEffectLabel->setText( "Plays the race start fanfare" ); break;
		case DialogSideEffect::PlayBossMusic:
			m_ui->sideEffectLabel->setText( "Starts playing boss music" ); break;
		case DialogSideEffect::StopBossMusic:
			m_ui->sideEffectLabel->setText( "Stops the background music" ); break;
		default: break;
	}

	m_nonUserUpdate = false;
}

void DialogEditor::dialogLinesChanged() {
	m_ui->voffsetSelect->setMaximum( 232 - 15*m_ui->pageSizeSelect->value() );
	if( m_ui->voffsetSelect->value() > m_ui->voffsetSelect->maximum() ) {
		m_ui->voffsetSelect->setValue( m_ui->voffsetSelect->maximum() );
	}
	dialogEntryChanged();
}

void DialogEditor::dialogEntryChanged() {
	if( m_nonUserUpdate ) return;
	DialogEntry &dialog = Blueprint::currentEditable()->dialogs().at( m_dialogId );
	dialog.name = m_ui->dialogName->text().toStdString();
	dialog.halign = (HAlign)m_ui->halignSelect->currentIndex();
	dialog.valign = (VAlign)m_ui->valignSelect->currentIndex();
	dialog.hoffset = (ushort)m_ui->hoffsetSelect->value();
	dialog.voffset = (ushort)m_ui->voffsetSelect->value();
	dialog.pageSize = (ubyte)m_ui->pageSizeSelect->value();
	dialog.soundEffect = (sbyte)(m_ui->sfxSelect->currentIndex() - 1);
	dialog.text = m_ui->textEditor->toPlainText().toStdString();

	m_ui->hoffsetSelect->setEnabled( dialog.halign != HAlign::Centre );
	m_ui->voffsetSelect->setEnabled( dialog.valign != VAlign::Centre );
}

void DialogEditor::soundTableEntrySelected() {
	m_ui->changeSoundButton->setEnabled( m_ui->soundTable->currentItem() != nullptr );
}

void DialogEditor::editSoundTableItem() {
	m_soundSelectDialog.setSound( m_ui->soundTable->currentItem()->data( Qt::UserRole ).toUInt() );
	m_soundSelectDialog.show();
}

void DialogEditor::soundTableItemChanged() {
	const uint soundId = m_soundSelectDialog.sound();
	QListWidgetItem *tableEntry = m_ui->soundTable->currentItem();
	const int i = m_ui->soundTable->currentRow();
	const string soundName = std::to_string( i ) + " - " + getSoundName( soundId );

	tableEntry->setData( Qt::UserRole, QVariant::fromValue<uint>( soundId ) );
	tableEntry->setText( soundName.c_str() );
	m_ui->sfxSelect->setItemText( m_ui->soundTable->currentRow() + 1, soundName.c_str() );
	Blueprint::currentEditable()->dialogSfxTable()[i] = soundId;
}

void DialogEditor::genericNameChanged() {
	Blueprint::currentEditable()->genericLevelName() = m_ui->genericLevelName->text().toStdString();
	Blueprint::currentEditable()->genericStarName() = m_ui->genericStarName->text().toStdString();
}
