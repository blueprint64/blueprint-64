#ifndef SRC_UI_TWEAK_PACKS_DIALOG_HPP_
#define SRC_UI_TWEAK_PACKS_DIALOG_HPP_

#include <QDialog>
#include "src/core/tweak.hpp"

namespace Ui {
	class TweakPacksDialog;
}

class TweakPacksDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::TweakPacksDialog *m_ui;
	std::vector<TweakPack> m_installedTweakPacks;
	std::vector<TweakPack> m_blueprintTweakPacks;
	std::map<Uuid,Version> m_installedLookup;
	std::map<Uuid,Version> m_blueprintLookup;
	bool m_initializing;

	void reload();

	public:
	explicit TweakPacksDialog( QWidget *parent = nullptr );
	~TweakPacksDialog();

	protected:
	virtual void showEvent( QShowEvent *event ) override {
		reload();
		QDialog::showEvent( event );
	}

	public slots:
	void updateFromBlueprint();
	void updateFromInstalled();
	void installFromBlueprint();
	void installedPackSelected();
	void blueprintPackSelected();
	void installNew();
	void uninstall();

};

#endif /* SRC_UI_TWEAK_PACKS_DIALOG_HPP_ */
