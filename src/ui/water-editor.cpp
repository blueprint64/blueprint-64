#include "src/ui/water-editor.hpp"
#include "ui_water-editor.h"

#include <cassert>
#include <QMessageBox>
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/baserom.hpp"
#include "src/ui/image-filter.hpp"

WaterEditor::WaterEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::WaterEditor ),
	m_levelId( LevelId::INVALID ),
	m_areaIndex( 0 ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	m_textureGroup.setExclusive( true );

	connect( &m_animationDialog, SIGNAL(accepted()), this, SLOT(animationChanged()) );
	connect( &m_textureDialog, SIGNAL(textureImported(WaterTextureInfo)), this, SLOT(textureImported(WaterTextureInfo)) );
	connect( &m_textureDialog, SIGNAL(textureChanged(WaterTextureInfo,WaterTextureInfo)), this, SLOT(textureChanged(WaterTextureInfo,WaterTextureInfo)) );

	for( const auto &i : BaseRom::waterTextures() ) {
		addTextureButton(
			m_ui->vanillaTextures,
			WaterTextureInfo{
				i.first,
				"",
				i.second.textureFormat,
				false
			},
			false
		);
	}

	m_initializing = false;
}

WaterEditor::~WaterEditor() {
	delete m_ui;
}

void WaterEditor::addTextureButton(
	DynamicGridWidget *section,
	const WaterTextureInfo &texture,
	bool select
) {
	QPushButton *button = new QPushButton();
	fs::path texturePath;
	if( section == m_ui->vanillaTextures ) {
		texturePath = AppData::cacheDir();
	} else if( section == m_ui->customTextures ) {
		texturePath = Blueprint::current()->getWaterTexturePath();
	}
	assert( !texturePath.empty() );
	texturePath /= texture.textureHash + ".png";
	QImage image( texturePath.u8string().c_str(), "PNG" );
	if( image.isNull() ) {
		delete button;
		return;
	} else if( section != m_ui->vanillaTextures ) {
		ImageFilter::applyColourFormat( image, texture.textureFormat );
	}
	button->setIcon( QIcon( QPixmap::fromImage( image.scaled( 64, 64, Qt::KeepAspectRatio, Qt::FastTransformation ) ) ) );
	button->setIconSize( QSize( 64, 64 ) );
	button->setText( "" );
	button->setProperty( "texture", QVariant::fromValue<WaterTextureInfo>( texture ) );
	button->setProperty( "canRemove", QVariant::fromValue<bool>( section == m_ui->customTextures ) );
	button->setFlat( true );
	button->setCheckable( true );
	button->setChecked( select );
	button->setStyleSheet( "QPushButton{ padding: 11px; } QPushButton:checked{ background-color: palette(highlight); border: 0; }" );

	m_textureGroup.addButton( button );
	section->addWidget( button );
	connect( button, SIGNAL(toggled(bool)), this, SLOT(dataChanged()) );
	connect( button, SIGNAL(destroyed(QObject*)), this, SLOT(buttonDestroyed(QObject*)) );

	if( select ) dataChanged();
}

void WaterEditor::setArea( LevelId levelId, ubyte areaIndex ) {
	m_levelId = levelId;
	m_areaIndex = areaIndex;

	m_initializing = true;
	m_ui->regionList->clear();

	const std::vector<WaterBoxSpecs> &waterBoxes = Blueprint::current()->areas().at( AREA_ID( levelId, areaIndex ) ).waterBoxes;
	for( const WaterBoxSpecs &waterBox : waterBoxes ) {
		m_ui->regionList->addItem( waterBox.name.c_str() );
	}

	if( waterBoxes.empty() ) {
		m_ui->regionList->clearSelection();
	} else {
		m_ui->regionList->setCurrentRow( 0 );
	}

	m_initializing = false;
	regionSelected();
}

void WaterEditor::blueprintChanged() {
	m_ui->customTextures->deleteAll();
	for( const WaterTextureInfo &i : Blueprint::current()->waterTextures() ) {
		addTextureButton( m_ui->customTextures, i, false );
	}
}

void WaterEditor::buttonDestroyed( QObject *button ) {
	m_textureGroup.removeButton( static_cast<QAbstractButton*>( button ) );
}

void WaterEditor::syncEnabled() {
	m_ui->addRegionButton->setEnabled( m_ui->regionList->count() < 20 );

	const bool regionSelected = !m_ui->regionList->selectedItems().empty();
	m_ui->removeRegionButton->setEnabled( regionSelected );
	m_ui->regionSettings->setEnabled( regionSelected );
	m_ui->volumeSettings->setEnabled( regionSelected );
	m_ui->visualSettings->setEnabled( regionSelected );
	m_ui->nameLabel->setEnabled( regionSelected );
	m_ui->nameEdit->setEnabled( regionSelected );
	if( !regionSelected ) {
		m_ui->nameEdit->setText( "" );
	}

	m_ui->texturePanel->setEnabled( regionSelected && m_ui->visualSettings->isChecked() );
	QAbstractButton *selectedTexture = m_textureGroup.checkedButton();
	if( selectedTexture == nullptr ) {
		m_ui->editTextureButton->setEnabled( false );
		return;
	}

	const WaterTextureInfo textureInfo = selectedTexture->property( "texture" ).value<WaterTextureInfo>();
	const bool enableColourSelect = (textureInfo.textureFormat >= TextureFormat::IA4);
	const bool enableOpacitySelect = (textureInfo.textureFormat != TextureFormat::IA8 && textureInfo.textureFormat != TextureFormat::IA16);
	m_ui->colourLabel->setEnabled( enableColourSelect );
	m_ui->colourSelect->setEnabled( enableColourSelect );
	m_ui->colourRef->setEnabled( enableColourSelect );
	m_ui->opacityLabel->setEnabled( enableOpacitySelect );
	m_ui->opacitySelect->setEnabled( enableOpacitySelect );

	const bool canEdit = regionSelected && selectedTexture->property( "canRemove" ).toBool();
	m_ui->editTextureButton->setEnabled( canEdit );
	m_ui->removeTextureButton->setEnabled( canEdit );
	for( const auto &i : Blueprint::current()->areas() ) {
		int index = 0;
		for( const WaterBoxSpecs &waterBox : i.second.waterBoxes ) {
			if( i.first == AREA_ID( m_levelId, m_areaIndex ) && index++ == m_ui->regionList->currentRow() ) continue;
			if( waterBox.texture == textureInfo ) {
				m_ui->removeTextureButton->setEnabled( false );
				break;
			}
		}
	}

	if( !enableColourSelect ) {
		m_ui->colourRef->setChecked( false );
	}
}

void WaterEditor::dataChanged() {
	if( m_initializing || m_ui->regionList->selectedItems().empty() ) return;

	assert( m_textureGroup.checkedButton() != nullptr );

	const short x0 = (short)m_ui->xMin->value();
	const short x1 = (short)m_ui->xMax->value();
	const short z0 = (short)m_ui->zMin->value();
	const short z1 = (short)m_ui->zMax->value();

	m_initializing = true;
	WaterBoxSpecs &waterBox = Blueprint::currentEditable()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes.at( m_ui->regionList->currentRow() );
	waterBox.name = m_ui->nameEdit->text().toStdString();
	waterBox.isPoisonFog = m_ui->poisonRadio->isChecked();
	waterBox.isVisible = m_ui->visualSettings->isChecked();
	waterBox.xMin = x0 < x1 ? x0 : x1;
	waterBox.xMax = x0 > x1 ? x0 : x1;
	waterBox.zMin = z0 < z1 ? z0 : z1;
	waterBox.zMax = z0 > z1 ? z0 : z1;
	waterBox.y = (short)m_ui->yHeight->value();
	waterBox.scale = (short)m_ui->scaleSelect->value();
	waterBox.opacity = (ubyte)(uint)(0.5 + m_ui->opacitySelect->value() * 2.55);
	waterBox.colour = (ColourRGB24)m_ui->colourSelect->value();
	waterBox.texture = m_textureGroup.checkedButton()->property( "texture" ).value<WaterTextureInfo>();
	waterBox.asmRefTexturePtr = m_ui->textureRef->isChecked();
	waterBox.asmRefColour = m_ui->colourRef->isChecked();

	syncEnabled();
	m_initializing = false;
}

void WaterEditor::editAnimation() {
	const WaterBoxSpecs &waterBox = Blueprint::current()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes.at( m_ui->regionList->currentRow() );
	m_animationDialog.open( waterBox.animation );
}

void WaterEditor::animationChanged() {
	WaterBoxSpecs &waterBox = Blueprint::currentEditable()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes.at( m_ui->regionList->currentRow() );
	waterBox.animation = m_animationDialog.getAnimation();
	m_ui->editAnimationButton->setText( waterBox.animation.index() > 0 ? "Edit Animation" : "Add Animation" );
}

static inline void selectTexture( const QList<QAbstractButton*> &buttons, const WaterTextureInfo &texture ) {
	assert( !buttons.empty() );
	for( QAbstractButton *button : buttons ) {
		if( button->property( "texture" ).value<WaterTextureInfo>() == texture ) {
			button->setChecked( true );
			return;
		}
	}
	buttons.first()->setChecked( true );
}

void WaterEditor::regionSelected() {
	if( m_initializing ) return;

	if( m_ui->regionList->selectedItems().empty() ) {
		syncEnabled();
		return;
	}

	m_initializing = true;
	const WaterBoxSpecs &waterBox = Blueprint::current()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes.at( m_ui->regionList->currentRow() );
	m_ui->nameEdit->setText( waterBox.name.c_str() );
	m_ui->waterRadio->setChecked( !waterBox.isPoisonFog );
	m_ui->poisonRadio->setChecked( waterBox.isPoisonFog );
	m_ui->xMin->setValue( waterBox.xMin );
	m_ui->xMax->setValue( waterBox.xMax );
	m_ui->zMin->setValue( waterBox.zMin );
	m_ui->zMax->setValue( waterBox.zMax );
	m_ui->yHeight->setValue( waterBox.y );
	m_ui->visualSettings->setChecked( waterBox.isVisible );
	m_ui->scaleSelect->setValue( waterBox.scale );
	m_ui->opacitySelect->setValue( (double)waterBox.opacity / 2.55 );
	m_ui->colourSelect->setValue( waterBox.colour );
	m_ui->editAnimationButton->setText( waterBox.animation.index() > 0 ? "Edit Animation" : "Add Animation" );
	m_ui->textureRef->setChecked( waterBox.asmRefTexturePtr );
	m_ui->colourRef->setChecked( waterBox.asmRefColour );
	selectTexture( m_textureGroup.buttons(), waterBox.texture );
	syncEnabled();
	m_initializing = false;
}

void WaterEditor::addRegion() {
	uint numLevelWaterBoxes = 0;
	const std::map<ushort,AreaSpecs> &areas = Blueprint::current()->areas();
	for( auto i = areas.lower_bound( AREA_ID( m_levelId, 0 ) ); i != areas.end() && i->first <= AREA_ID( m_levelId, 7 ); i++ ) {
		numLevelWaterBoxes += (uint)i->second.waterBoxes.size();
	}

	if( numLevelWaterBoxes >= 20 ) {
		QMessageBox::critical( this, "Level Too Wet", "Currently, each level can have at most 20 water boxes.", QMessageBox::Ok );
		return;
	}

	std::vector<WaterBoxSpecs> &areaWaterBoxes = Blueprint::currentEditable()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes;
	areaWaterBoxes.push_back( WaterBoxSpecs::Default() );
	const string name = "Untitled Water Box "s + std::to_string( (int)areaWaterBoxes.size() );
	areaWaterBoxes.at( areaWaterBoxes.size() - 1 ).name = name;
	m_ui->regionList->addItem( name.c_str() );
	m_ui->regionList->setCurrentRow( m_ui->regionList->count() - 1 );
	regionSelected();
}

void WaterEditor::removeRegion() {
	m_initializing = true;
	std::vector<WaterBoxSpecs> &areaWaterBoxes = Blueprint::currentEditable()->areas().at( AREA_ID( m_levelId, m_areaIndex ) ).waterBoxes;
	areaWaterBoxes.erase( areaWaterBoxes.begin() + m_ui->regionList->currentRow() );
	delete m_ui->regionList->takeItem( m_ui->regionList->currentRow() );
	if( m_ui->regionList->count() > 0 ) {
		m_ui->regionList->setCurrentRow( 0 );
	} else {
		m_ui->regionList->clearSelection();
	}
	m_initializing = false;
	regionSelected();
}

void WaterEditor::addTexture() {
	m_textureDialog.open();
}

void WaterEditor::textureImported( WaterTextureInfo texture ) {
	addTextureButton( m_ui->customTextures, texture, true );
}

void WaterEditor::textureChanged( WaterTextureInfo previous, WaterTextureInfo updated ) {
	if( previous == updated ) return;
	for( QAbstractButton *button : m_textureGroup.buttons() ) {
		if( !button->property( "canRemove" ).toBool() ) continue;
		WaterTextureInfo texture = button->property( "texture" ).value<WaterTextureInfo>();

		if( texture == previous ) {
			button->setProperty( "texture", QVariant::fromValue<WaterTextureInfo>( updated ) );

			fs::path texturePath = Blueprint::current()->getWaterTexturePath() / (updated.textureHash + ".png");
			QImage image = QImage( texturePath.u8string().c_str(), "PNG" );
			ImageFilter::applyColourFormat( image, updated.textureFormat );
			button->setIcon( QIcon( QPixmap::fromImage( image.scaled( 64, 64, Qt::KeepAspectRatio, Qt::FastTransformation ) ) ) );
		} else if( texture == updated ) {
			m_ui->customTextures->deleteWidget( button );
		}
	}
}

void WaterEditor::removeTexture() {
	m_initializing = true;
	WaterTextureInfo texture = m_textureGroup.checkedButton()->property( "texture" ).value<WaterTextureInfo>();
	Blueprint::currentEditable()->waterTextures().erase( texture );

	bool textureUsed = false;
	for( const WaterTextureInfo &t : Blueprint::current()->waterTextures() ) {
		if( t.textureHash == texture.textureHash ) {
			textureUsed = true;
			break;
		}
	}

	if( !textureUsed ) {
		fs::forceDelete( Blueprint::current()->getWaterTexturePath() / ( texture.textureHash + ".png" ) );
	}

	m_ui->customTextures->takeWidget( m_textureGroup.checkedButton() )->deleteLater();
	static_cast<QPushButton*>( m_ui->vanillaTextures->widgetAt( 4 ) )->setChecked( true );
	m_initializing = false;
	dataChanged();
}

void WaterEditor::editTexture() {
	m_textureDialog.open( m_textureGroup.checkedButton()->property( "texture" ).value<WaterTextureInfo>() );
}

void WaterEditor::regionRenamed() {
	if( m_initializing || m_ui->regionList->selectedItems().empty() ) return;
	m_ui->regionList->currentItem()->setText( m_ui->nameEdit->text() );
}
