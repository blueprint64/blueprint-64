#include "src/ui/move-level-dialog.hpp"
#include "ui_move-level-dialog.h"

#include <cstdio>
#include <QPushButton>
#include "src/core/blueprint.hpp"
#include "src/polyfill/filesystem.hpp"

MoveLevelDialog::MoveLevelDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::MoveLevelDialog ),
	m_levelId( LevelId::INVALID )
{
	m_ui->setupUi( this );

	const std::vector<LevelId> levels = Enum::values<LevelId>();
	m_ui->levelTable->setColumnCount( 3 );
	m_ui->levelTable->setRowCount( 30 );

	m_ui->levelTable->setHorizontalHeaderLabels({ "ID", "Vanilla Name", "Custom Level" });
	m_ui->levelTable->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::ResizeToContents );
	m_ui->levelTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
	m_ui->levelTable->horizontalHeader()->setSectionResizeMode( 2, QHeaderView::Stretch );

	for( int row = 0; row < 30; row++ ) {
		char id[5];
		std::sprintf( id, "0x%02X", (int)levels[row] );
		m_ui->levelTable->setItem( row, 0, new QTableWidgetItem( id ) );
		m_ui->levelTable->setItem( row, 1, new QTableWidgetItem( Enum::toString( levels[row] ) ) );
		m_ui->levelTable->setItem( row, 2, new QTableWidgetItem( "" ) );
	}
}

MoveLevelDialog::~MoveLevelDialog() {
	delete m_ui;
}

void MoveLevelDialog::open( LevelId levelId ) {
	m_levelId = levelId;
	const std::vector<LevelId> levels = Enum::values<LevelId>();
	for( int row = 0; row < 30; row++ ) {
		const auto customLevel = Blueprint::current()->levels().find( levels[row] );
		if( customLevel == Blueprint::current()->levels().end() ) {
			m_ui->levelTable->item( row, 2 )->setText( "" );
		} else {
			m_ui->levelTable->item( row, 2 )->setText( customLevel->second.name.c_str() );
		}

		if( levels[row] == levelId ) {
			m_ui->levelTable->setCurrentCell( row, 0 );
		}
	}

	QPushButton *acceptButton = m_ui->dialogButtons->button( QDialogButtonBox::Ok );
	acceptButton->setText( "Swap" );
	acceptButton->setIcon( QIcon::fromTheme( "document-swap" ) );
	acceptButton->setEnabled( false );

	QDialog::open();
}

void MoveLevelDialog::levelSelected() {
	if( m_ui->levelTable->selectedItems().empty() ) return;

	const LevelId selectedLevel = Enum::values<LevelId>().at( m_ui->levelTable->currentRow() );
	const bool swap = Blueprint::current()->hasLevel( selectedLevel );

	QPushButton *acceptButton = m_ui->dialogButtons->button( QDialogButtonBox::Ok );
	acceptButton->setText( swap ? "Swap" : "Move" );
	acceptButton->setIcon( QIcon::fromTheme( swap ? "document-swap" : "document-export" ) );
	acceptButton->setEnabled( selectedLevel != m_levelId );
}

static void moveLevel( LevelId from, LevelId to ) {
	Blueprint *blueprint = Blueprint::currentEditable();

	if( blueprint->levels().at( from ).name == Enum::toString<LevelId>( from ) ) {
		blueprint->levels().at( from ).name = Enum::toString<LevelId>( to );
	}

	blueprint->levels()[to] = std::move( blueprint->levels().at( from ) );
	blueprint->levels()[to].id = to;
	blueprint->levels().erase( from );

	fs::rename( blueprint->getLevelPath( from ), blueprint->getLevelPath( to ) );

	for( ubyte i = 0; i < 8; i++ ) {
		if( blueprint->hasArea( from, i ) ) {
			blueprint->areas()[AREA_ID( to, i )] = std::move( blueprint->areas().at( AREA_ID( from, i ) ) );
			blueprint->areas()[AREA_ID( to, i )].areaId = AREA_ID( to, i );
			blueprint->areas().erase( AREA_ID( from, i ) );
		}

		const fs::path actorPath = blueprint->getAreaActorsPath( from, i );
		if( fs::exists( actorPath ) ) {
			fs::rename( actorPath, blueprint->getAreaActorsPath( to, i ) );
		}
	}

}

void MoveLevelDialog::accept() {
	const LevelId targetLevel = Enum::values<LevelId>().at( m_ui->levelTable->currentRow() );
	Blueprint *blueprint = Blueprint::currentEditable();
	if( blueprint->hasLevel( targetLevel ) ) {
		constexpr LevelId TEMP_LEVEL = (LevelId)0;
		moveLevel( targetLevel, TEMP_LEVEL );
		moveLevel( m_levelId, targetLevel );
		moveLevel( TEMP_LEVEL, m_levelId );
	} else {
		moveLevel( m_levelId, targetLevel );
	}
	QDialog::accept();
}
