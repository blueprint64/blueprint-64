#ifndef SRC_UI_ANIMATION_DIALOG_HPP_
#define SRC_UI_ANIMATION_DIALOG_HPP_

#include <QDialog>
#include "src/core/data/animation.hpp"

namespace Ui {
	class AnimationDialog;
}

class AnimationDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::AnimationDialog *m_ui;

	public:
	explicit AnimationDialog( QWidget *parent = nullptr );
	~AnimationDialog();

	void open( const AnimationSpecs &animation );
	AnimationSpecs getAnimation() const;

	public slots:
	void animationTypeChanged();
	void directionChanged(int);

};



#endif /* SRC_UI_ANIMATION_DIALOG_HPP_ */
