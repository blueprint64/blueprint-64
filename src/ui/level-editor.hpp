#ifndef SRC_UI_LEVEL_EDITOR_HPP_
#define SRC_UI_LEVEL_EDITOR_HPP_

#include <QWidget>
#include "src/ui/object-banks-dialog.hpp"
#include "src/ui/create-area-dialog.hpp"
#include "src/ui/star-spawn-dialog.hpp"
#include "src/ui/waypoint-dialog.hpp"
#include "src/ui/import-model-dialog.hpp"
#include "src/ui/object-create-dialog.hpp"
#include "src/ui/advanced-object-dialog.hpp"
#include "src/ui/user-data-dialog.hpp"
#include "src/core/enums.hpp"
#include "src/core/objectid.hpp"

namespace Ui {
	class LevelEditor;
}

class LevelEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::LevelEditor *m_ui;
	ObjectBanksDialog m_objectBanksDialog;
	CreateAreaDialog m_createAreaDialog;
	StarSpawnDialog m_starSpawnDialog;
	WaypointDialog m_waypointDialog;
	ObjectCreateDialog m_createObjectDialog;
	ImportModelDialog m_importModelDialog;
	AdvancedObjectDialog m_advancedObjectDialog;
	UserDataDialog m_userDataDialog;
	LevelId m_levelId;
	bool m_initializing;

	public slots:
	void starNameChanged();
	void levelDataChanged();
	void areaSelected();
	void objectSelected();
	void addArea();
	void editArea();
	void deleteArea();
	void areaCreated();
	void objectCreated();
	void addObject();
	void editObject();
	void deleteObject();
	void selectObjectBanks();
	void refreshObjectBanks();
	void manageUserData();

	void editStarSpawns() {
		m_starSpawnDialog.open( m_levelId );
	}

	void editWaypoints() {
		m_waypointDialog.open( m_levelId );
	}

	private slots:
	void objectModelImported( ModelImportedEvent event );

	signals:
	void gotoAreaEdit( ubyte );
	void gotoObjectEdit( objectId );
	void gotoObjectModelEdit( objectId );

	public:
	explicit LevelEditor( QWidget *parent = nullptr );
	~LevelEditor();

	void setLevel( LevelId levelId );

};

#endif /* SRC_UI_LEVEL_EDITOR_HPP_ */
