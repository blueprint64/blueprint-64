#include "src/ui/blueprint-controller.hpp"

#include <QMessageBox>
#include <string>
#include "src/polyfill/filesystem.hpp"
#include "src/core/baserom.hpp"

using namespace std::string_literals;

inline static string appendErrorDetails( const string &message, const std::exception &ex ) {
	return message + "\n\nError Details:\n" + ex.what();
}

void BlueprintController::createProject( CreateProjectOptions options ) {
	if( !options.romPath.empty() ) {
		std::error_code err;
		fs::copy_file( BaseRom::filePath(), fs::to_path( options.romPath ), err );
		if( err ) {
			QMessageBox::critical( nullptr, "Failed to Create ROM", ("Failed to create a ROM at the target path: "s + err.message()).c_str(), QMessageBox::Ok );
			options.romPath.clear();
		}
	}

	try {
		Blueprint::create( options );
		emit blueprintLoaded();
	} catch( const std::exception &ex ) {
		emit loadFailed( appendErrorDetails( "Failed to create project file. Do you have permissions to create files in this directory?", ex ) );
	}
}

void BlueprintController::openProject( string filePath ) {
	try {
		Blueprint::open( fs::to_path( filePath ) );
		emit blueprintLoaded();
	} catch( const std::exception &ex ) {
		emit loadFailed( appendErrorDetails( "Error loading project file.", ex ) );
	}
}
