#ifndef SRC_UI_BACKGROUND_EDITOR_HPP_
#define SRC_UI_BACKGROUND_EDITOR_HPP_

#include <QWidget>

namespace Ui {
	class BackgroundEditor;
}

class BackgroundEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::BackgroundEditor *m_ui;
	bool m_initializing;

	public:
	explicit BackgroundEditor( QWidget *parent = nullptr );
	~BackgroundEditor();

	public slots:
	void backgroundSelected();
	void addBackground();
	void deleteBackground();
	void replaceBackground();
	void renameBackground();

	protected:
	virtual void showEvent( QShowEvent *event ) override;

};



#endif /* SRC_UI_BACKGROUND_EDITOR_HPP_ */
