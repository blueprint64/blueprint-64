#include "src/ui/armips-error-dialog.hpp"
#include "ui_armips-error-dialog.h"

ArmipsErrorDialog::ArmipsErrorDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ArmipsErrorDialog )
{
	m_ui->setupUi( this );
}

ArmipsErrorDialog::~ArmipsErrorDialog() {
	delete m_ui;
}

bool ArmipsErrorDialog::getResponse( const std::string &armipsOutput ) {
	m_ui->armipsOutput->setPlainText( armipsOutput.c_str() );
	return exec() == QDialog::Accepted;
}
