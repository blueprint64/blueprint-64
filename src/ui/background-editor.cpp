#include "src/ui/background-editor.hpp"
#include "ui_background-editor.h"

#include <QMessageBox>
#include <cassert>
#include "src/polyfill/file-dialog.hpp"
#include "src/core/import/texture.hpp"
#include "src/core/blueprint.hpp"
#include "src/ui/image-filter.hpp"
#include "src/ui/util.hpp"

#define MAX_CUSTOM_BACKGROUNDS 21

BackgroundEditor::BackgroundEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::BackgroundEditor ),
	m_initializing( false )
{
	m_ui->setupUi( this );
}

BackgroundEditor::~BackgroundEditor() {
	delete m_ui;
}

void BackgroundEditor::showEvent( QShowEvent *event ) {
	m_initializing = true;
	m_ui->backgroundList->clear();
	for( const auto &i : Blueprint::current()->customBackgrounds() ) {
		m_ui->backgroundList->addValueItem<uint>( i.second, i.first );
	}
	m_ui->backgroundList->clearSelection();
	const uint numBackgrounds = m_ui->backgroundList->count();
	if( numBackgrounds > 0 ) {
		m_ui->backgroundList->setCurrentRow( 0 );
	}
	m_ui->addButton->setEnabled( numBackgrounds < MAX_CUSTOM_BACKGROUNDS );
	m_initializing = false;
	backgroundSelected();
	QWidget::showEvent( event );
}

void BackgroundEditor::backgroundSelected() {
	if( m_initializing ) return;

	if( m_ui->backgroundList->selectedItems().empty() ) {
		m_ui->backgroundPane->setEnabled( false );
		m_ui->nameField->clear();
		m_ui->preview->clearPixmap( 248, 248 );
		return;
	}

	const ubyte backgroundSlot = (ubyte)m_ui->backgroundList->currentData<uint>();
	m_ui->backgroundPane->setEnabled( true );
	m_ui->nameField->setText( Blueprint::current()->customBackgrounds().at( backgroundSlot ).c_str() );

	QImage preview( Blueprint::current()->getCustomBackgroundPath( backgroundSlot ).u8string().c_str(), "PNG" );
	ImageFilter::applyColourFormat( preview, TextureFormat::RGBA16 );
	ImageFilter::removeAlpha( preview );
	m_ui->preview->setPixmap( QPixmap::fromImage( std::move( preview ) ), true );
}

static QImage tryUploadImageFile( QWidget *parent ) {
	const fs::path imagePath = FileDialog::getFileOpen(
		"Select an Image (248x248)",
		{ { "Images", "*.png;*.bmp;*.gif;*.jpg;*.jpeg;*.xpm;*.xbm;*.pbm;*.pgm;*.ppm" } }
	);

	if( imagePath.empty() ) return QImage();

	QImage image( imagePath.u8string().c_str() );
	if( image.isNull() ) {
		QMessageBox::critical( parent, "Image Load Error", "Error reading image file.", QMessageBox::Ok );
		return QImage();
	}

	if( image.width() != 248 || image.height() != 248 ) {
		if( QMessageBox::information( parent, "Resize Image", "The selected image will be resized to 248x248 pixels.", QMessageBox::Ok | QMessageBox::Cancel ) != QMessageBox::Ok ) {
			return tryUploadImageFile( parent );
		}
		image = image.scaled( 248, 248, Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
	}

	return image;
}

void BackgroundEditor::addBackground() {
	QImage image = tryUploadImageFile( this );
	if( image.isNull() ) return;

	ubyte slot = 0;
	while( Blueprint::current()->customBackgrounds().count( slot ) > 0 ) slot++;
	assert( slot < MAX_CUSTOM_BACKGROUNDS );

	const fs::path backgroundPath = Blueprint::current()->getCustomBackgroundPath( slot );
	fs::create_directories( backgroundPath.parent_path() );
	if( !image.save( backgroundPath.u8string().c_str(), "PNG" ) ) {
		QMessageBox::critical( this, "Unexpected I/O Error", "Failed to save image to blueprint.", QMessageBox::Ok );
		return;
	}

	m_initializing = true;
	m_ui->backgroundList->addValueItem<uint>( "Untitled Background", slot );
	m_ui->backgroundList->setCurrentRow( m_ui->backgroundList->count() - 1 );
	m_initializing = false;

	m_ui->backgroundPane->setEnabled( true );
	m_ui->addButton->setEnabled( m_ui->backgroundList->count() < MAX_CUSTOM_BACKGROUNDS );

	Blueprint::currentEditable()->customBackgrounds()[slot] = "Untitled Background";

	ImageFilter::applyColourFormat( image, TextureFormat::RGBA16 );
	ImageFilter::removeAlpha( image );
	m_ui->preview->setPixmap( QPixmap::fromImage( std::move( image ) ), true );
	m_ui->nameField->setText( "Untitled Background" );
	m_ui->nameField->setFocus();
	m_ui->nameField->selectAll();
}

void BackgroundEditor::deleteBackground() {
	const ubyte slot = (ubyte)m_ui->backgroundList->currentData<uint>();

	HashSet<ushort> areasUsed;
	for( const auto &i : Blueprint::current()->areas() ) {
		if( (ubyte)i.second.backgroundImage == slot + (ubyte)BackgroundImage::CustomBackgroundStart ) {
			areasUsed.insert( i.first );
		}
	}

	string confirmMessage = "Are you sure you want to delete this background?";
	if( !areasUsed.empty() ) {
		confirmMessage += " It is used in "s + std::to_string( areasUsed.size() ) + " areas.";
	}

	if( QMessageBox::question( this, "Confirm Delete", confirmMessage.c_str() ) != QMessageBox::Yes ) {
		return;
	}

	fs::remove( Blueprint::current()->getCustomBackgroundPath( slot ) );
	Blueprint::currentEditable()->customBackgrounds().erase( slot );
	for( ushort i : areasUsed ) {
		Blueprint::currentEditable()->areas().at( i ).backgroundImage = BackgroundImage::None;
	}

	delete m_ui->backgroundList->takeItem( m_ui->backgroundList->currentRow() );
}

void BackgroundEditor::replaceBackground() {
	QImage image = tryUploadImageFile( this );
	if( image.isNull() ) return;

	const ubyte slot = (ubyte)m_ui->backgroundList->currentData<uint>();
	if( !image.save( Blueprint::current()->getCustomBackgroundPath( slot ).u8string().c_str(), "PNG" ) ) {
		QMessageBox::critical( this, "Unexpected I/O Error", "Failed to save image to blueprint.", QMessageBox::Ok );
		return;
	}

	ImageFilter::applyColourFormat( image, TextureFormat::RGBA16 );
	ImageFilter::removeAlpha( image );
	m_ui->preview->setPixmap( QPixmap::fromImage( std::move( image ) ), true );
}

void BackgroundEditor::renameBackground() {
	const QString newName = m_ui->nameField->text();
	if( newName.isEmpty() ) return;
	m_ui->backgroundList->currentItem()->setText( newName );

	const ubyte slot = (ubyte)m_ui->backgroundList->currentData<uint>();
	Blueprint::currentEditable()->customBackgrounds().at( slot ) = newName.toStdString();
}
