#ifndef SRC_UI_AREA_EDITOR_HPP_
#define SRC_UI_AREA_EDITOR_HPP_

#include <vector>
#include <QWidget>
#include "src/core/enums.hpp"
#include "src/ui/import-model-dialog.hpp"
#include "src/ui/object-placement-dialog.hpp"
#include "src/ui/camera-triggers-dialog.hpp"
#include "src/ui/camera-options-dialog.hpp"
#include "src/core/asm-modules.hpp"

namespace Ui {
	class AreaEditor;
}

class AreaEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::AreaEditor *m_ui;
	ImportModelDialog m_importDialog;
	ObjectPlacementDialog m_objectPlacementDialog;
	CameraTriggersDialog m_cameraTriggersDialog;
	CameraOptionsDialog m_cameraOptionsDialog;
	std::vector<CustomCamera> m_cameraModules;
	bool m_initializing;
	LevelId m_level;
	ubyte m_area;

	private slots:
	void modelImported( ModelImportedEvent event );

	public slots:
	void areaDataChanged();
	void clippingPlanePresetChanged();
	void manageObjectPlacements();
	void fogHelpClicked();
	void editCameraOptions();

	signals:
	void openModelEditor();
	void openWaterEditor();

	public:
	explicit AreaEditor( QWidget *parent = nullptr );
	~AreaEditor();

	void setArea( LevelId levelId, ubyte areaIndex );

};

#endif /* SRC_UI_AREA_EDITOR_HPP_ */
