#ifndef SRC_UI_TEXTURE_SPLITTER_DIALOG_HPP_
#define SRC_UI_TEXTURE_SPLITTER_DIALOG_HPP_

#include <QDialog>
#include <QImage>

namespace Ui {
	class TextureSplitterDialog;
}

class TextureSplitterDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::TextureSplitterDialog *m_ui;
	QImage m_image;
	bool m_hasAlpha;
	bool m_ignoreEvents;

	public:
	explicit TextureSplitterDialog( QWidget *parent = nullptr );
	virtual ~TextureSplitterDialog();

	inline static void run() {
		TextureSplitterDialog dialog;
		dialog.exec();
	}

	public slots:
	void browse();
	void textureFormatChanged();
	void modelWidthChanged( double );
	void modelHeightChanged( double );
	void generateModel();

};

#endif /* SRC_UI_TEXTURE_SPLITTER_DIALOG_HPP_ */
