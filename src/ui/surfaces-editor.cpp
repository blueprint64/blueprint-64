#include "src/ui/surfaces-editor.hpp"
#include "ui_surfaces-editor.h"

#include <QStandardItemModel>
#include <QMessageBox>
#include <cstdio>
#include <unordered_set>
#include <unordered_map>
#include "src/polyfill/filesystem.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/import/texture.hpp"
#include "src/ui/image-filter.hpp"
#include "src/ui/util.hpp"
#include "src/core/util.hpp"

enum FixableWarningType : ubyte {
	ConstantOpacity = 0,
	FormatLosesAlpha = 1,
	LayerLosesAlpha = 2,
	FormatLosesColour = 3,
	UseIA16 = 4,
	NotFixable = 0xFF
};

static HashMap<TextureFormat,int> makeFormatLookup() {
	HashMap<TextureFormat,int> lookup;
	int index = 0;
	for( TextureFormat format : Enum::values<TextureFormat>() ) {
		lookup.insert_or_assign( format, index++ );
	}
	return lookup;
}

static HashMap<CollisionType,int> makeCollisionTypeLookup() {
	HashMap<CollisionType,int> lookup;
	int index = 0;
	for( CollisionType format : Enum::values<CollisionType>() ) {
		lookup.insert_or_assign( format, index++ );
	}
	return lookup;
}

static int getFormatIndex( TextureFormat format ) {
	static HashMap<TextureFormat,int> s_lookup = makeFormatLookup();
	return s_lookup.find( format )->second;
}

static int getCollisionTypeIndex( CollisionType format ) {
	static HashMap<CollisionType,int> s_lookup = makeCollisionTypeLookup();
	const int *index = Util::tryGetValue( s_lookup, format );
	return (index != nullptr) ? *index : -1;
}

static bool canSelectOpacity( const MaterialSpecs &material ) {
	return material.textureInfo.has_value() && material.layer >= DrawingLayer::Alphatest;
}

static inline bool supports( const HashSet<TextureFormat> &possibleFormats, TextureFormat desiredFormat ) {
	return possibleFormats.find( desiredFormat ) != possibleFormats.end();
}

SurfacesEditor::SurfacesEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::SurfacesEditor ),
	m_initializing( true ),
	m_levelId( LevelId::INVALID ),
	m_fixType( 0xFF )
{
	m_ui->setupUi( this );
	m_ui->warningBubble->setVisible( false );
	m_ui->warningIcon->setPixmap( QIcon::fromTheme( "emblem-warning" ).pixmap( 16, 16 ) );
	m_ui->lightingIcon->setPixmap( QIcon::fromTheme( "lighttable" ).pixmap( 16, 16 ) );
	for( DrawingLayer layer : Enum::values<DrawingLayer>() ) {
		m_ui->layerSelect->addItem( Enum::toString( layer ) );
	}
	for( TextureFormat format : Enum::values<TextureFormat>() ) {
		m_ui->formatSelect->addItem( Enum::toString( format ), QVariant::fromValue( (uint)format ) );
	}
	for( CollisionType ctype : Enum::values<CollisionType>() ) {
		char prefix[8];
		std::sprintf( prefix, "0x%02X - ", (ubyte)ctype );
		const string name = string(prefix) + Enum::toString( ctype );
		m_ui->collisionTypeSelect->addItem( name.c_str(), QVariant::fromValue( (uint)ctype ) );
	}
	m_ui->collisionTypeSelect->addItem( "Other", QVariant::fromValue( (uint)0x100 ) );
	connect( m_ui->helpButton, SIGNAL(clicked()), &m_drawingLayerHelp, SLOT(open()) );
	connect( &m_animationDialog, SIGNAL(accepted()), this, SLOT(animationChanged()) );
	m_initializing = false;
}

SurfacesEditor::~SurfacesEditor() {
	delete m_ui;
}

void SurfacesEditor::collisionTypeSelected() {
	if( m_initializing ) return;
	uint typeId = m_ui->collisionTypeSelect->currentData().toUInt();

	m_ui->collisionTypeSpinner->setVisible( typeId > 0xFF );
	const CollisionType type = (typeId <= 0xFF) ?
		(CollisionType)typeId :
		(CollisionType)m_ui->collisionTypeSpinner->value();

	const bool directional = Collision::hasDirection( type );
	m_ui->directionSelect->setEnabled( directional );
	m_ui->forceSelect->setEnabled( directional && type != CollisionType::HorizontalWind );

	const string materialName = m_ui->materialsList->currentItem()->text().toStdString();
	m_materialMap.at( materialName ).collision.type = type;
}

void SurfacesEditor::dataChanged() {
	if( m_initializing ) return;

	const string materialName = m_ui->materialsList->currentItem()->text().toStdString();
	MaterialSpecs &mat = m_materialMap.at( materialName );
	mat.visible = m_ui->displaySettings->isChecked() && mat.textureInfo.has_value();
	mat.solid = m_ui->collisionSettings->isChecked();

	if( mat.textureInfo.has_value() ) {
		mat.layer = (DrawingLayer)m_ui->layerSelect->currentIndex();
		mat.format = (TextureFormat)m_ui->formatSelect->currentData().toUInt();
		mat.filtering = m_ui->textureFilteringCheckbox->isChecked() ? TextureFiltering::Bilinear : TextureFiltering::None;
		mat.doubleSided = m_ui->doubleSidedCheckbox->isChecked();
		mat.hwrap = m_ui->repeatH->isChecked() ? TextureWrapping::Repeat : ( m_ui->mirrorH->isChecked() ? TextureWrapping::Mirror : TextureWrapping::Clamp );
		mat.vwrap = m_ui->repeatV->isChecked() ? TextureWrapping::Repeat : ( m_ui->mirrorV->isChecked() ? TextureWrapping::Mirror : TextureWrapping::Clamp );
		mat.shadingRef = m_ui->shadingSelect->getSelectedShadeId();
		mat.asmRefs.texData = m_ui->asmRefTexture->isChecked();
		mat.asmRefs.texPtr = m_ui->asmRefTexturePtr->isChecked();
		mat.asmRefs.shadePtr = m_ui->asmRefShadingPtr->isChecked();
		mat.asmRefs.opacity = m_ui->asmRefOpacity->isChecked();
		if( mat.layer < DrawingLayer::Alphatest ) {
			mat.ditheredAlpha = false;
		} else if( mat.layer == DrawingLayer::Alphatest ) {
			mat.ditheredAlpha = true;
		} else {
			mat.ditheredAlpha = m_ui->ditheredCheckbox->isChecked();
		}

		bool enableOpacitySelect = canSelectOpacity( mat );
		mat.opacity = enableOpacitySelect ? (ubyte)((m_ui->opacitySelect->value() * 2.55) + 0.5) : 0xFF;
		m_ui->opacitySelect->setEnabled( enableOpacitySelect );
		m_ui->asmRefOpacity->setEnabled( enableOpacitySelect );
		m_ui->ditheredCheckbox->setEnabled( mat.layer >= DrawingLayer::Translucent );

		if( mat.layer < DrawingLayer::Translucent ) {
			m_initializing = true;
			m_ui->ditheredCheckbox->setChecked( mat.layer == DrawingLayer::Alphatest );
			m_initializing = false;
		}
	}

	if( m_ui->collisionTypeSpinner->isVisible() ) {
		mat.collision.type = (CollisionType)m_ui->collisionTypeSpinner->value();
	} else {
		mat.collision.type = (CollisionType)m_ui->collisionTypeSelect->currentData().toUInt();
	}

	mat.collision.direction = (ubyte)(m_ui->directionSelect->value() * 0x100 / 360 );
	mat.collision.force = (ForceIntensity)m_ui->forceSelect->currentIndex();

	checkForWarning();
}

void SurfacesEditor::reflectionChanged() {
	if( m_initializing ) return;

	const string materialName = m_ui->materialsList->currentItem()->text().toStdString();
	MaterialSpecs &mat = m_materialMap.at( materialName );
	mat.reflection = m_ui->reflectionCheckbox->isChecked();

	m_ui->wrappingGroup->setEnabled( !mat.reflection );
	m_ui->editAnimationButton->setEnabled( !mat.reflection );
}

void SurfacesEditor::onMaterialsChanged() {
	assert( m_materialMap.size() > 0 );
	m_ui->materialsList->clear();
	for( const auto &i : m_materialMap ) {
		m_ui->materialsList->addItem( i.first.c_str() );

		const MaterialSpecs &specs = i.second;
		if( specs.textureInfo.has_value() ) {
			const TextureInfo &texInfo = specs.textureInfo.value();
			if( m_textureMap.find( texInfo.hash ) == m_textureMap.end() ) {
				fs::path texturePath = Blueprint::current()->getLevelPath( m_levelId ) / ( texInfo.hash + ".png" );
				QImage original( texturePath.u8string().c_str(), "PNG" );
				if( original.isNull() ) continue;

				QPixmap preview;
				if( original.width() / original.height() > 2 ) {
					preview = QPixmap::fromImage( original.scaledToWidth( 256, 256 % original.width() == 0 ? Qt::FastTransformation : Qt::SmoothTransformation ) );
				} else {
					preview = QPixmap::fromImage( original.scaledToHeight( 128, 128 % original.height() == 0 ? Qt::FastTransformation : Qt::SmoothTransformation ) );
				}

				const Qt::TransformationMode iconScalingMode =
					((16 % original.width() == 0) && (16 % original.height() == 0)) ?
					Qt::FastTransformation : Qt::SmoothTransformation;

				m_textureMap.insert_or_assign( texInfo.hash, std::move( preview ) );
				m_ui->materialsList->item( m_ui->materialsList->count() - 1 )->setIcon(
					QIcon( QPixmap::fromImage( original.scaled( 16, 16, Qt::KeepAspectRatio, iconScalingMode ) ) )
				);
			} else {
				m_ui->materialsList->item( m_ui->materialsList->count() - 1 )->setIcon(
					QIcon( m_textureMap.at( texInfo.hash ).scaled( 16, 16, Qt::KeepAspectRatio, Qt::FastTransformation ) )
				);
			}

		}
	}
	UiUtil::sortListEntries( m_ui->materialsList, ListSorters::NaturalString );
	m_ui->materialsList->setCurrentRow( 0 );
}

void SurfacesEditor::materialSelected() {
	if( m_ui->materialsList->selectedItems().empty() ) return;

	m_initializing = true;
	const MaterialSpecs &material = m_materialMap.find( m_ui->materialsList->currentItem()->text().toStdString() )->second;
	if( material.textureInfo.has_value() ) {
		const TextureInfo &texInfo = material.textureInfo.value();
		const auto &texRef = m_textureMap.find( texInfo.hash );
		if( texRef == m_textureMap.end() ) goto no_texture;

		setPreview( texRef->second, material );
		m_ui->textureSize->setText( (std::to_string( texInfo.width ) + " x " + std::to_string( texInfo.height )).c_str() );
		m_ui->displaySettings->setEnabled( true );
		m_ui->displaySettings->setChecked( material.visible );

		QStandardItemModel *model = dynamic_cast<QStandardItemModel*>( m_ui->formatSelect->model() );
		assert( model != nullptr );
		for( int i = 0; i < m_ui->formatSelect->count(); i++ ) {
			model->item( i )->setEnabled( supports( texInfo.getPossibleFormats(), (TextureFormat)model->item( i )->data( Qt::UserRole ).toUInt() ) );
		}
		m_ui->formatSelect->setCurrentIndex( getFormatIndex( material.format ) );
		m_ui->textureFilteringCheckbox->setChecked( material.filtering != TextureFiltering::None );
		m_ui->doubleSidedCheckbox->setChecked( material.doubleSided );
		m_ui->reflectionCheckbox->setChecked( material.reflection );

		m_ui->editAnimationButton->setEnabled( !material.reflection );
		m_ui->editAnimationButton->setText( material.animation.index() > 0 ? "Edit Animation" : "Add Animation" );
		m_ui->ditheredCheckbox->setEnabled( material.layer >= DrawingLayer::Translucent );

		m_ui->wrappingGroup->setEnabled( !material.reflection );
	} else {
		no_texture:
		m_ui->texturePreview->clearPixmap( 128, 128 );
		m_ui->textureSize->setText( "No Texture" );
		m_ui->displaySettings->setEnabled( false );
		m_ui->displaySettings->setChecked( false );
		m_ui->editAnimationButton->setText( "Add Animation" );
		m_ui->editAnimationButton->setEnabled( false );
		m_ui->ditheredCheckbox->setEnabled( false );
	}

	bool enableOpacitySelect = canSelectOpacity( material );

	m_ui->collisionSettings->setChecked( material.solid );
	int cti = getCollisionTypeIndex( material.collision.type );
	if( cti >= 0 ) {
		m_ui->collisionTypeSelect->setCurrentIndex( getCollisionTypeIndex( material.collision.type ) );
		m_ui->collisionTypeSpinner->setVisible( false );
	} else {
		m_ui->collisionTypeSelect->setCurrentIndex( m_ui->collisionTypeSelect->count() - 1 );
		m_ui->collisionTypeSpinner->setVisible( true );
	}
	m_ui->collisionTypeSpinner->setValue( (int)material.collision.type );
	m_ui->directionSelect->setEnabled( Collision::hasDirection( material.collision.type ) );
	m_ui->directionSelect->setValue( material.collision.direction * 360 / 0x100 );
	m_ui->forceSelect->setCurrentIndex( (int)material.collision.force );
	m_ui->forceSelect->setEnabled( material.collision.type != CollisionType::HorizontalWind && Collision::hasDirection( material.collision.type ) );
	m_ui->layerSelect->setCurrentIndex( (int)material.layer );
	m_ui->opacitySelect->setValue( enableOpacitySelect ? ((double)material.opacity / 2.55) : 100.0 );
	m_ui->opacitySelect->setEnabled( enableOpacitySelect );
	m_ui->ditheredCheckbox->setChecked( material.ditheredAlpha );
	m_ui->doubleSidedCheckbox->setChecked( material.doubleSided );
	switch( material.hwrap ) {
		case TextureWrapping::Repeat: m_ui->repeatH->setChecked( true ); break;
		case TextureWrapping::Mirror: m_ui->mirrorH->setChecked( true ); break;
		case TextureWrapping::Clamp: m_ui->clampH->setChecked( true ); break;
	}
	switch( material.vwrap ) {
		case TextureWrapping::Repeat: m_ui->repeatV->setChecked( true ); break;
		case TextureWrapping::Mirror: m_ui->mirrorV->setChecked( true ); break;
		case TextureWrapping::Clamp: m_ui->clampV->setChecked( true ); break;
	}
	m_ui->shadingSelect->setSelectedShadeId( material.shadingRef );
	m_ui->asmRefTexture->setChecked( material.asmRefs.texData );
	m_ui->asmRefTexturePtr->setChecked( material.asmRefs.texPtr );
	m_ui->asmRefShadingPtr->setChecked( material.asmRefs.shadePtr );
	m_ui->asmRefOpacity->setChecked( material.asmRefs.opacity );
	m_ui->asmRefOpacity->setEnabled( enableOpacitySelect );

	checkForWarning();
	m_initializing = false;
}

void SurfacesEditor::checkForWarning() {
	const MaterialSpecs &material = m_materialMap.find( m_ui->materialsList->currentItem()->text().toStdString() )->second;
	if( !material.textureInfo.has_value() || !material.visible ) {
		m_ui->warningBubble->setVisible( false );
		return;
	}

	const TextureInfo &tex = material.textureInfo.value();
	const HashSet<TextureFormat> possibleFormats = tex.getPossibleFormats();

	if( tex.hasFullAlpha && !tex.isGreyscale && !supports( possibleFormats, TextureFormat::RGBA32 ) ) {
		if( tex.hasVaryingAlpha ) {
			setWarning( NotFixable, "This texture is too large to support a format with colour and translucency." );
			return;
		} else if( supports( possibleFormats, TextureFormat::RGBA16 ) ) {
			setWarning( ConstantOpacity, "Alpha channel will be lost. Use opaque texture with opacity set instead." );
			return;
		}
	}

	if( tex.hasFullAlpha && !supports( possibleFormats, TextureFormat::IA8 ) ) {
		if( tex.hasVaryingAlpha ) {
			setWarning( NotFixable, "This texture is too large to support a format with translucency." );
			return;
		}
		setWarning( ConstantOpacity, "Alpha channel will be lost. Use opaque texture with opacity set instead." );
		return;
	}

	if( !tex.isGreyscale && supports( possibleFormats, TextureFormat::RGBA16 ) && material.format != TextureFormat::RGBA16 && material.format != TextureFormat::RGBA32 ) {
		setWarning( FormatLosesColour, "Texture format loses colour. Use RGBA format instead." );
		return;
	}

	if( tex.hasFullAlpha && material.layer < DrawingLayer::Translucent ) {
		setWarning( LayerLosesAlpha, "The selected drawing layer does not support translucency." );
		return;
	}

	if( tex.hasEmptyPixels && material.layer < DrawingLayer::Translucent && material.layer != DrawingLayer::Alphatest ) {
		setWarning( LayerLosesAlpha, "The selected drawing layer does not support transparency." );
		return;
	}

	if( tex.hasFullAlpha && material.format != TextureFormat::RGBA32 && material.format != TextureFormat::IA16 && material.format != TextureFormat::IA8 && supports( possibleFormats, TextureFormat::IA8 ) ) {
		setWarning( FormatLosesAlpha, "Texture format loses alpha channel." );
		return;
	}

	if( tex.hasEmptyPixels && (material.format == TextureFormat::I4 || material.format == TextureFormat::I8) ) {
		setWarning( FormatLosesAlpha, "Texture format does not support transparent pixels." );
		return;
	}

	if( material.opacity != 0xFF && material.ditheredAlpha ) {
		setWarning( NotFixable, "Dithered alpha may not work correctly on inaccurate graphics plugins." );
		return;
	}

	m_ui->warningBubble->setVisible( false );
}

void SurfacesEditor::setWarning( ubyte fixType, const string &message ) {
	m_ui->warningBubble->setVisible( true );
	m_ui->warningText->setText( message.c_str() );
	m_ui->fixButton->setVisible( fixType != NotFixable );
	m_fixType = fixType;
}

void SurfacesEditor::fixIssue() {
	MaterialSpecs &mat = m_materialMap[m_ui->materialsList->currentItem()->text().toStdString()];
	switch( m_fixType ) {
		case ConstantOpacity: {
			TextureFile texture;
			if( TextureFile::tryOpen( getTexturePath( mat.textureInfo.value().hash ).c_str(), texture ) == TextureFile::Error ) {
				QMessageBox::critical( this, "Texture not found", "Error opening texture file", QMessageBox::Ok );
				return;
			}

			const string oldHash = mat.textureInfo.value().hash;

			mat.opacity = mat.textureInfo.value().averageAlpha;
			m_ui->opacitySelect->setValue( (double)mat.opacity / 2.55 );

			texture.removeAlpha();
			mat.textureInfo = texture.info();
			mat.removedTextureAlpha = true;

			QImage *sourceImage = (QImage*)texture.image();
			if( m_textureMap.find( texture.info().hash ) == m_textureMap.end() ) {
				texture.saveAsPng( getTexturePath( texture.info().hash ).c_str() );
				QPixmap preview;
				if( sourceImage->width() / sourceImage->height() > 2 ) {
					preview = QPixmap::fromImage( sourceImage->scaledToWidth( 256, 256 % sourceImage->width() == 0 ? Qt::FastTransformation : Qt::SmoothTransformation ) );
				} else {
					preview = QPixmap::fromImage( sourceImage->scaledToHeight( 128, 128 % sourceImage->height() == 0 ? Qt::FastTransformation : Qt::SmoothTransformation ) );
				}
				m_textureMap.insert_or_assign( texture.info().hash, std::move( preview ) );
			}

			bool oldTextureStillUsed = false;
			for( const auto &i : m_materialMap ) {
				if( i.second.textureInfo.has_value() && i.second.textureInfo.value().hash == oldHash ) {
					oldTextureStillUsed = true;
					break;
				}
			}

			if( !oldTextureStillUsed ) {
				fs::forceDelete( getTexturePath( oldHash ) );
				m_textureMap.erase( oldHash );
			}

			const Qt::TransformationMode iconScalingMode =
				((16 % sourceImage->width() == 0) && (16 % sourceImage->height() == 0)) ?
				Qt::FastTransformation : Qt::SmoothTransformation;

			setPreview( m_textureMap.find( texture.info().hash )->second, mat );
			m_ui->materialsList->currentItem()->setIcon(
				QIcon( QPixmap::fromImage( ((QImage*)texture.image())->scaled( 16, 16, Qt::KeepAspectRatio, iconScalingMode ) ) )
			);

			if( mat.layer < DrawingLayer::Translucent ) {
				mat.layer = DrawingLayer::Translucent;
				m_ui->layerSelect->setCurrentIndex( (int)DrawingLayer::Translucent );
			}

			saveData();
			break;
		}
		case FormatLosesAlpha: {
			const HashSet<TextureFormat> possibleFormats = mat.textureInfo.value().getPossibleFormats();
			if( mat.textureInfo.value().hasFullAlpha ) {
				if( mat.format == TextureFormat::RGBA16 && supports( possibleFormats, TextureFormat::RGBA32 ) ) {
					mat.format = TextureFormat::RGBA32;
				} else if( supports( possibleFormats, TextureFormat::IA16 ) ) {
					mat.format = TextureFormat::IA16;
				} else {
					mat.format = TextureFormat::IA8;
				}
			} else {
				if( mat.format == TextureFormat::I4 ) {
					if( supports( possibleFormats, TextureFormat::IA8 ) ) {
						mat.format = TextureFormat::IA8;
					} else {
						mat.format = TextureFormat::IA4;
					}
				} else {
					if( supports( possibleFormats, TextureFormat::IA16 ) ) {
						mat.format = TextureFormat::IA16;
					} else {
						mat.format = TextureFormat::IA8;
					}
				}
			}
			m_ui->formatSelect->setCurrentIndex( getFormatIndex( mat.format ) );
			break;
		}
		case LayerLosesAlpha: {
			if( mat.textureInfo.value().hasFullAlpha ) {
				mat.layer = DrawingLayer::Translucent;
				m_ui->layerSelect->setCurrentIndex( (int)DrawingLayer::Translucent );
			} else {
				mat.layer = DrawingLayer::Alphatest;
				m_ui->layerSelect->setCurrentIndex( (int)DrawingLayer::Alphatest );
			}
			break;
		}
		case FormatLosesColour: {
			if( mat.textureInfo.value().hasFullAlpha && supports( mat.textureInfo.value().getPossibleFormats(), TextureFormat::RGBA32 ) ) {
				mat.format = TextureFormat::RGBA32;
			} else {
				mat.format = TextureFormat::RGBA16;
			}
			m_ui->formatSelect->setCurrentIndex( getFormatIndex( mat.format ) );
			break;
		}
		case UseIA16: {
			mat.format = TextureFormat::IA16;
			m_ui->formatSelect->setCurrentIndex( getFormatIndex( TextureFormat::IA16 ) );
			break;
		}
		default: return;
	}
	bool enableOpacitySelect = canSelectOpacity( mat );
	m_ui->opacitySelect->setEnabled( enableOpacitySelect );
	m_ui->asmRefOpacity->setEnabled( enableOpacitySelect );
	if( !enableOpacitySelect ) {
		mat.opacity = 0xFF;
		m_ui->opacitySelect->setValue( 100.0 );
		m_ui->asmRefOpacity->setChecked( false );
	}
	checkForWarning();
}

string SurfacesEditor::getTexturePath( const string &textureHash ) const {
	return ( Blueprint::current()->getLevelPath( m_levelId ) / ( textureHash + ".png" ) ).u8string();
}

void SurfacesEditor::editAnimation() {
	m_animationDialog.open( m_materialMap.at( m_ui->materialsList->currentItem()->text().toStdString() ).animation );
}

void SurfacesEditor::animationChanged() {
	MaterialSpecs &mat = m_materialMap.at( m_ui->materialsList->currentItem()->text().toStdString() );
	mat.animation = m_animationDialog.getAnimation();
	m_ui->editAnimationButton->setText( mat.animation.index() > 0 ? "Edit Animation" : "Add Animation" );
}

inline ubyte addCC( ubyte a, ubyte b ) {
	const ushort sum = (ushort)a + (ushort)b;
	return sum > 0xFF ? 0xFF : (ubyte)sum;
}

void SurfacesEditor::setPreview( const QPixmap &pixmap, const MaterialSpecs &material ) {
	QImage preview = pixmap.toImage();
	ImageFilter::applyColourFormat( preview, material.format );

	const ShadingSpecs &shades = Blueprint::current()->levels().at( m_levelId ).shadingValues.at( material.shadingRef );
	const float lightLevel = (float)m_ui->lightingSlider->value() / 100.0f;
	const ColourRGB24 lighting = ColourRGB24(
		addCC( shades.ambient.red, lightLevel * shades.diffuse.red ),
		addCC( shades.ambient.green, lightLevel * shades.diffuse.green ),
		addCC( shades.ambient.blue, lightLevel * shades.diffuse.blue )
	);

	ImageFilter::colourize( preview, lighting );
	m_ui->texturePreview->setPixmap( QPixmap::fromImage( std::move( preview ) ), true );
}

void SurfacesEditor::refreshPreview() {
	const string materialName = m_ui->materialsList->currentItem()->text().toStdString();
	if( m_materialMap.count( materialName ) == 0 ) return;
	const MaterialSpecs &mat = m_materialMap.at( materialName );
	if( !mat.textureInfo.has_value() ) return;
	const TextureInfo &texInfo = mat.textureInfo.value();
	setPreview( m_textureMap.at( texInfo.hash ), mat );
}

void SurfacesEditor::previewChanged() {
	if( m_initializing ) return;
	dataChanged();
	refreshPreview();
}
