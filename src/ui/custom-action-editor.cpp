#include "src/ui/custom-action-editor.hpp"
#include "ui_custom-action-editor.h"

#include <QCheckBox>
#include "src/core/blueprint.hpp"
#include "src/core/util.hpp"

CustomActionEditor::CustomActionEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::CustomActionEditor ),
	m_initializing( true )
{
	m_ui->setupUi( this );

	m_ui->flagGroup->removeButton( m_ui->flagPlaceholder );
	m_ui->actionFlagContainer->layout()->removeWidget( m_ui->flagPlaceholder );
	m_ui->flagPlaceholder->deleteLater();

	for( MarioActionCategory group : Enum::values<MarioActionCategory>() ) {
		m_ui->categorySelect->addItem( Enum::toString<MarioActionCategory>( group ), QVariant::fromValue<uint>( (uint)group ) );
	}

	int i = 0;
	QGridLayout *flagLayout = static_cast<QGridLayout*>( m_ui->actionFlagContainer->layout() );
	for( MarioActionFlag flag : Enum::values<MarioActionFlag>() ) {
		QCheckBox *checkbox = new QCheckBox( m_ui->actionFlagContainer );
		checkbox->setText( Enum::toString<MarioActionFlag>( flag ) );
		checkbox->setProperty( "flag", QVariant::fromValue<uint>( (uint)flag ) );
		m_ui->flagGroup->addButton( checkbox );
		flagLayout->addWidget( checkbox, i % 12, i / 12 );
		i++;
	}
	m_initializing = false;
}

CustomActionEditor::~CustomActionEditor() {
	for( QAbstractButton *checkbox : m_ui->flagGroup->buttons() ) {
		checkbox->deleteLater();
	}
	delete m_ui;
}

void CustomActionEditor::actionSelected() {
	if( m_initializing ) return;

	m_initializing = true;
	if( m_ui->actionList->selectedItems().empty() ) {
		m_ui->actionEditor->setEnabled( false );
		m_ui->deleteButton->setEnabled( false );
		m_ui->nameField->clear();
		m_ui->labelField->clear();
		m_ui->categorySelect->setCurrentIndex( 0 );
		for( QAbstractButton *checkbox : m_ui->flagGroup->buttons() ) {
			checkbox->setChecked( false );
		}
	} else {
		m_ui->actionEditor->setEnabled( true );
		m_ui->deleteButton->setEnabled( true );
		m_ui->nameField->setText( m_ui->actionList->currentItem()->text() );
		const CustomMarioAction &action = Blueprint::current()->customMarioActions().at( m_ui->nameField->text().toStdString() );
		m_ui->labelField->setText( action.functionLabel.c_str() );
		m_ui->categorySelect->setCurrentIndex( (int)action.category / 0x40 );
		for( QAbstractButton *checkbox : m_ui->flagGroup->buttons() ) {
			checkbox->setChecked( ((uint)action.flags & checkbox->property( "flag" ).value<uint>()) != 0 );
		}
	}

	m_initializing = false;
}

void CustomActionEditor::showEvent( QShowEvent *event ) {
	m_initializing = true;
	m_ui->actionList->clear();
	for( const auto &i : Blueprint::current()->customMarioActions() ) {
		m_ui->actionList->addItem( i.first.c_str() );
	}
	if( m_ui->actionList->count() > 0 ) {
		m_ui->actionList->setCurrentRow( 0 );
	}
	m_initializing = false;
	actionSelected();
	QWidget::showEvent( event );
}

static string sanitizeLabel( const string label ) {
	string safeLabel = "";
	for( char c : label ) {
		if( c == ' ' || c == '_' ) {
			safeLabel += '_';
		} else if(
			(c >= 'a' && c <= 'z') ||
			(c >= 'A' && c <= 'Z') ||
			(c >= '0' && c <= '9')
		) {
			safeLabel += c;
		}
	}
	return safeLabel;
}

static string fixupName( const string &proposedName ) {
	const string name = sanitizeLabel( proposedName );

	if( Blueprint::current()->customMarioActions().count( name ) <= 0 ) {
		return name;
	}

	int i = 1;
	while( true ) {
		const string altName = name + '_' + std::to_string( ++i );
		if( Blueprint::current()->customMarioActions().count( altName ) <= 0 ) {
			return altName;
		}
	}
}

void CustomActionEditor::addAction() {
	const string placeholderName = fixupName( "ACT_CUSTOM" );
	Blueprint::currentEditable()->customMarioActions()[placeholderName] = CustomMarioAction{
		placeholderName,
		placeholderName + "_impl",
		MarioActionCategory::Stationary,
		MarioActionFlag::None,
		true
	};

	m_initializing = true;
	m_ui->actionList->addItem( placeholderName.c_str() );
	QListWidgetItem *newEntry = m_ui->actionList->item( m_ui->actionList->count() - 1 );
	m_ui->actionList->clearSelection();
	m_ui->actionList->sortItems();
	m_ui->actionList->setCurrentItem( newEntry );
	m_initializing = false;
	actionSelected();
}

void CustomActionEditor::removeAction() {
	m_initializing = true;
	Blueprint::currentEditable()->customMarioActions().erase( m_ui->actionList->currentItem()->text().toStdString() );
	delete m_ui->actionList->takeItem( m_ui->actionList->currentRow() );
	m_ui->actionList->clearSelection();
	m_initializing = false;
	actionSelected();
}

void CustomActionEditor::nameChanged() {
	if( m_initializing ) return;

	const string oldName = m_ui->actionList->currentItem()->text().toStdString();
	string newName = fixupName( m_ui->nameField->text().toStdString() );

	if( newName.empty() || m_ui->nameField->text().toStdString() == oldName ) {
		m_ui->nameField->setText( oldName.c_str() );
		return;
	}

	std::map<string,CustomMarioAction> &customActions = Blueprint::currentEditable()->customMarioActions();
	const CustomMarioAction &oldAction = customActions.at( oldName );
	customActions[newName] = {
		newName,
		oldAction.functionLabel,
		oldAction.category,
		oldAction.flags,
		true
	};
	customActions.erase( oldName );

	m_ui->actionList->currentItem()->setText( newName.c_str() );
}

void CustomActionEditor::categoryChanged() {
	if( m_initializing ) return;

	CustomMarioAction &action = Blueprint::currentEditable()->customMarioActions().at( m_ui->actionList->currentItem()->text().toStdString() );
	action.category = (MarioActionCategory)m_ui->categorySelect->currentData( Qt::UserRole ).toUInt();
}

void CustomActionEditor::labelChanged() {
	if( m_initializing ) return;

	CustomMarioAction &action = Blueprint::currentEditable()->customMarioActions().at( m_ui->actionList->currentItem()->text().toStdString() );
	string newLabel = sanitizeLabel( m_ui->labelField->text().toStdString() );

	if( newLabel.empty() || newLabel == action.functionLabel ) {
		m_ui->labelField->setText( action.functionLabel.c_str() );
		return;
	}

	action.functionLabel = newLabel;
	m_ui->labelField->setText( newLabel.c_str() );
}

void CustomActionEditor::flagToggled( QAbstractButton *checkbox, bool enabled ) {
	if( m_initializing ) return;

	const MarioActionFlag flag = (MarioActionFlag)checkbox->property( "flag" ).value<uint>();
	CustomMarioAction &action = Blueprint::currentEditable()->customMarioActions().at( m_ui->actionList->currentItem()->text().toStdString() );

	action.flags = enabled ? (action.flags | flag) : (action.flags & ~flag);
}

