#ifndef SRC_UI_SURFACES_EDITOR_HPP_
#define SRC_UI_SURFACES_EDITOR_HPP_

#include <QWidget>
#include <QPixmap>
#include <unordered_map>
#include "src/ui/drawing-layer-help-dialog.hpp"
#include "src/ui/animation-dialog.hpp"
#include "src/core/enums.hpp"
#include "src/core/data/material.hpp"
#include "src/types.hpp"

namespace Ui {
	class SurfacesEditor;
}

class SurfacesEditor : public QWidget {
	Q_OBJECT

	protected:
	Ui::SurfacesEditor *m_ui;
	DrawingLayerHelpDialog m_drawingLayerHelp;
	AnimationDialog m_animationDialog;
	HashMap<string,MaterialSpecs> m_materialMap;
	HashMap<string,QPixmap> m_textureMap;
	bool m_initializing;
	LevelId m_levelId;
	ubyte m_fixType;

	SurfacesEditor( QWidget *parent = nullptr );
	virtual ~SurfacesEditor();

	void checkForWarning();
	void setWarning( ubyte fixType, const string &message );
	string getTexturePath( const string &textureHash ) const;

	void onMaterialsChanged();

	private:
	void setPreview( const QPixmap &pixmap, const MaterialSpecs &material );

	public slots:
	void materialSelected();
	void collisionTypeSelected();
	void dataChanged();
	void reflectionChanged();
	void previewChanged();
	void refreshPreview();
	void fixIssue();
	void editAnimation();
	virtual void saveData() const = 0;
	virtual void objectDataChanged() {}

	private slots:
	void animationChanged();

	protected:
	virtual void hideEvent( QHideEvent *event ) final override {
		saveData();
		m_textureMap.clear();
		QWidget::hideEvent( event );
	}

};


#endif /* SRC_UI_SURFACES_EDITOR_HPP_ */
