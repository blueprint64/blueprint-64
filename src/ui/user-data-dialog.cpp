#include "src/ui/user-data-dialog.hpp"
#include "ui_user-data-dialog.h"

#include <cstdio>
#include <algorithm>
#include <vector>
#include <QMessageBox>
#include <QFrame>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/filestream.hpp"
#include "src/polyfill/random.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

UserDataDialog::UserDataDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::UserDataDialog ),
	m_levelId( LevelId::INVALID ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	m_initializing = false;
}

UserDataDialog::~UserDataDialog() {
	delete m_ui;
}

static bool linkSorter( const PotentialLink &a, const PotentialLink &b ) {
	return a.name < b.name;
}

static std::map<uint,UserDataLink> getLinks( std::ifstream &data, const std::map<uint,UserDataLink> &oldLinks ) {
	auto i = oldLinks.begin();
	std::map<uint,UserDataLink> links;
	ubyte placeholder[] = { 0xDE, 0xAD, 0xBE, 0xEF };
	ubyte matchPos = 0;
	for( char c = data.get(); data.good() && !data.eof(); c = data.get() ) {
		if( (ubyte)c == placeholder[matchPos] ) {
			if( ++matchPos == 4 ) {
				matchPos = 0;
				if( i != oldLinks.end() ) {
					links[(uint)data.tellg() - 4] = i->second;
					i++;
				} else {
					links[(uint)data.tellg() - 4] = { LinkType::None, 0, 0 };
				}
			}
		} else {
			matchPos = ((ubyte)c == 0xDE) ? 1 : 0;
		}
	}
	return links;
}

void UserDataDialog::reload() {
	m_initializing = true;
	m_availableLinks.clear();
	m_ui->userDataList->clear();
	const LevelSpecs &levelSpecs = Blueprint::current()->levels().at( m_levelId );
	for( const auto &i : levelSpecs.userData ) {
		m_ui->userDataList->addValueItem<uint>( i.second.name, i.first );
		m_availableLinks.push_back({
			i.second.name,
			LinkType::UserData,
			i.first
		});
	}
	m_ui->userDataList->sortItems();
	m_ui->userDataList->setCurrentRow( m_ui->userDataList->count() > 0 ? 0 : -1 );
	m_initializing = false;

	for( const auto &i : levelSpecs.partialObjects ) {
		m_availableLinks.push_back({
			i.second.name,
			LinkType::PartialObject,
			i.first
		});
	}

	std::sort( m_availableLinks.begin(), m_availableLinks.end(), linkSorter );

	selectionChanged();
}

void UserDataDialog::addData() {
	const fs::path filePath = FileDialog::getFileOpen( "Import Binary File" );
	if( filePath.empty() || !fs::exists( filePath ) ) return;

	std::error_code error;
	size_t fileSize = fs::file_size( filePath, error );
	if( error ) {
		return;
	}

	if( fileSize >= 0x150000 ) {
		QMessageBox::critical( this, "File too large!", "This file is much too large and cannot fit in the level data segment.", QMessageBox::Ok );
		return;
	}

	LevelSpecs &levelSpecs = Blueprint::currentEditable()->levels().at( m_levelId );

	uint userDataId;
	do {
		userDataId = Pseudorandom::getUInt();
	} while( levelSpecs.userData.count( userDataId ) > 0 );


	fs::path savedPath = Blueprint::current()->getUserDataPath( m_levelId, userDataId );
	fs::create_directories( savedPath.parent_path() );
	fs::tryCopyFileOverwrite( filePath, savedPath );

	FileReadStream binFile( savedPath.u8string() );
	const string name = filePath.stem().u8string();
	levelSpecs.userData[userDataId] = {
		userDataId,
		name,
		getLinks( binFile, std::map<uint,UserDataLink>() ),
		true,
		fileSize
	};

	m_availableLinks.push_back({
		name,
		LinkType::UserData,
		userDataId
	});
	std::sort( m_availableLinks.begin(), m_availableLinks.end(), linkSorter );

	m_initializing = true;
	m_ui->userDataList->setCurrentItem( m_ui->userDataList->addValueItem<uint>( name, userDataId ) );
	m_initializing = false;
	selectionChanged();
}

void UserDataDialog::updateData() {
	const fs::path filePath = FileDialog::getFileOpen( "Import Binary File" );
	if( filePath.empty() || !fs::exists( filePath ) ) return;

	std::error_code error;
	size_t fileSize = fs::file_size( filePath, error );
	if( error ) {
		return;
	}

	if( fileSize >= 0x150000 ) {
		QMessageBox::critical( this, "File too large!", "This file is much too large and cannot fit in the level data segment.", QMessageBox::Ok );
		return;
	}

	LevelSpecs &levelSpecs = Blueprint::currentEditable()->levels().at( m_levelId );

	const uint userDataId = m_ui->userDataList->currentItem()->data( Qt::UserRole ).toUInt();
	fs::path savedPath = Blueprint::current()->getUserDataPath( m_levelId, userDataId );
	fs::tryCopyFileOverwrite( filePath, savedPath );

	FileReadStream binFile( savedPath.u8string() );
	const string name = filePath.stem().u8string();
	levelSpecs.userData[userDataId].links = getLinks( binFile, levelSpecs.userData[userDataId].links );
	levelSpecs.userData[userDataId].bytes = fileSize;

	selectionChanged();
}

void UserDataDialog::deleteData() {
	if( QMessageBox::question( this, "Confirm Delete", "Are you sure you want to delete this data? Any advanced objects linked to this data will also be deleted.", QMessageBox::Ok ) != QMessageBox::Ok ) {
		return;
	}

	LevelSpecs &level = Blueprint::currentEditable()->levels().at( m_levelId );
	const uint dataId = m_ui->userDataList->currentItem()->data( Qt::UserRole ).toUInt();

	std::vector<objectId> dependantObjects;
	for( const auto &i : level.advancedObjects ) {
		if( i.second.customDataId == dataId ) {
			dependantObjects.push_back( i.first );
		}
	}

	for( objectId objectId : dependantObjects ) {
		level.advancedObjects.erase( objectId );
	}

	for( auto &i : level.userData ) {
		for( auto &j : i.second.links ) {
			if( j.second.linkType == LinkType::UserData && j.second.targetId == dataId ) {
				j.second.linkType = LinkType::None;
				j.second.targetId = 0;
				j.second.targetOffset = 0;
			}
		}
	}

	level.userData.erase( dataId );
	reload();
}

void UserDataDialog::asmRefChanged() {
	if( m_initializing ) return;

	uint userDataId = m_ui->userDataList->currentItem()->data( Qt::UserRole ).toUInt();
	UserData &userData = Blueprint::currentEditable()->levels().at( m_levelId ).userData.at( userDataId );
	userData.asmRef = m_ui->asmRefCheckbox->isChecked();
}

void UserDataDialog::nameChanged() {
	if( m_initializing ) return;

	uint userDataId = m_ui->userDataList->currentItem()->data( Qt::UserRole ).toUInt();
	UserData &userData = Blueprint::currentEditable()->levels().at( m_levelId ).userData.at( userDataId );
	userData.name = m_ui->nameEdit->text().toStdString();

	m_initializing = true;
	QListWidgetItem *listEntry = m_ui->userDataList->currentItem();
	listEntry->setText( m_ui->nameEdit->text() );
	m_ui->userDataList->sortItems();
	m_ui->userDataList->setCurrentItem( listEntry );
	m_initializing = false;

	for( PotentialLink &link : m_availableLinks ) {
		if( link.type == LinkType::UserData && link.targetId == userDataId ) {
			link.name = userData.name;
			break;
		}
	}
	std::sort( m_availableLinks.begin(), m_availableLinks.end(), linkSorter );
	selectionChanged();
}

static const char *s_instructions = ""
	"To link binary data to other resources, use the byte string 0xDEADBEEF as a placeholder. These placeholders will then be "
	"replaced with segmented pointers to the resource you link to. If the data happens to have the byte sequence 0xDEADBEEF and "
	"you do not want it replaced with a placeholder, do not select a link target, and it will be left alone.";

void UserDataDialog::selectionChanged() {
	if( m_initializing ) return;

	while( m_ui->linkContainer->layout()->itemAt( 0 ) != nullptr ) {
		QWidget *child = m_ui->linkContainer->layout()->itemAt( 0 )->widget();
		m_ui->linkContainer->layout()->removeWidget( child );
		child->deleteLater();
	}

	m_initializing = true;
	if( m_ui->userDataList->count() <= 0 || m_ui->userDataList->currentRow() < 0 ) {
		m_ui->dataSizeLabel->setVisible( false );
		m_ui->nameEdit->setEnabled( false );
		m_ui->nameEdit->setText( "" );
		m_ui->asmRefCheckbox->setEnabled( false );
		m_ui->asmRefCheckbox->setChecked( false );
		m_ui->updateButton->setEnabled( false );
		m_ui->deleteButton->setEnabled( false );

		QLabel *instructions = new QLabel( s_instructions, m_ui->userDataList );
		instructions->setWordWrap( true );
		m_ui->linkContainer->layout()->addWidget( instructions );
		static_cast<QBoxLayout*>( m_ui->linkContainer->layout() )->addStretch();

		m_initializing = false;
		return;
	}

	m_ui->nameEdit->setEnabled( true );
	m_ui->asmRefCheckbox->setEnabled( true );
	m_ui->dataSizeLabel->setVisible( true );
	m_ui->updateButton->setEnabled( true );
	m_ui->deleteButton->setEnabled( true );

	uint userDataId = m_ui->userDataList->currentItem()->data( Qt::UserRole ).toUInt();
	UserData &userData = Blueprint::currentEditable()->levels().at( m_levelId ).userData.at( userDataId );
	m_ui->nameEdit->setText( userData.name.c_str() );
	m_ui->asmRefCheckbox->setChecked( userData.asmRef );
	m_initializing = false;

	char sizeText[26];
	std::sprintf( sizeText, "%d bytes (%.1f kB)", (int)userData.bytes, (float)userData.bytes / 1024.f );
	m_ui->dataSizeLabel->setText( sizeText );

	bool firstLink = true;
	for( auto &i : userData.links ) {
		if( !firstLink ) {
			QFrame *line = new QFrame( m_ui->linkContainer );
			line->setFrameShape( QFrame::HLine );
			line->setFrameShadow( QFrame::Sunken );
			m_ui->linkContainer->layout()->addWidget( line );
		}
		UserDataLinkWidget *link = new UserDataLinkWidget( m_ui->linkContainer, m_levelId, i.first, &m_availableLinks, &i.second );
		m_ui->linkContainer->layout()->addWidget( link );
		firstLink = false;
	}

	if( firstLink ) {
		QLabel *instructions = new QLabel( s_instructions, m_ui->userDataList );
		instructions->setWordWrap( true );
		m_ui->linkContainer->layout()->addWidget( instructions );
	}

	static_cast<QBoxLayout*>( m_ui->linkContainer->layout() )->addStretch();

}
