#include "src/ui/object-banks-dialog.hpp"
#include "ui_object-banks-dialog.h"

#include <cassert>
#include "src/core/objectbanks.hpp"
#include "src/core/blueprint.hpp"

void ObjectBanksDialog::bank1changed( int index ) {
	const sbyte selectedBank = (sbyte)index - 1;
	assert( selectedBank < 11 );
	m_ui->bank1objectList->clear();
	if( selectedBank < 0 ) return;
	for( const char *object : objectBank1Contents[selectedBank] ) {
		m_ui->bank1objectList->addItem( object );
	}
}

void ObjectBanksDialog::bank2changed( int index ) {
	const sbyte selectedBank = (sbyte)index - 1;
	assert( selectedBank < 6 );
	m_ui->bank2objectList->clear();
	if( selectedBank < 0 ) return;
	for( const char *object : objectBank2Contents[selectedBank] ) {
		m_ui->bank2objectList->addItem( object );
	}
}

void ObjectBanksDialog::bank3changed( int index ) {
	const sbyte selectedBank = (sbyte)index - 1;
	assert( selectedBank < 30 );
	m_ui->bank3objectList->clear();
	if( selectedBank < 0 ) return;
	for( const char *object : objectBank3Contents[selectedBank] ) {
		m_ui->bank3objectList->addItem( object );
	}
}

ObjectBanksDialog::ObjectBanksDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ObjectBanksDialog ),
	m_levelId( LevelId::INVALID )
{
	m_ui->setupUi( this );

	m_ui->bank1select->addItem( "None" );
	for( int i = 0; i < 11; i++ ) {
		m_ui->bank1select->addItem( objectBank1Names[i] );
	}

	m_ui->bank2select->addItem( "None" );
	for( int i = 0; i < 6; i++ ) {
		m_ui->bank2select->addItem( objectBank2Names[i] );
	}

	m_ui->bank3select->addItem( "None" );
	for( int i = 0; i < 30; i++ ) {
		m_ui->bank3select->addItem( objectBank3Names[i] );
	}
}

ObjectBanksDialog::~ObjectBanksDialog() {
	delete m_ui;
}

void ObjectBanksDialog::open( LevelId levelId ) {
	m_levelId = levelId;
	assert( Blueprint::current()->hasLevel( levelId ) );
	const sbyte *banks = Blueprint::current()->levels().find( levelId )->second.objectBanks;
	m_ui->bank1select->setCurrentIndex( banks[0] + 1 );
	m_ui->bank2select->setCurrentIndex( banks[1] + 1 );
	m_ui->bank3select->setCurrentIndex( banks[2] + 1 );

	const bool useExpandedAudioHeap = Blueprint::current()->romSpecs().expandAudioHeap;
	m_ui->label_3->setEnabled( !useExpandedAudioHeap );
	m_ui->bank3select->setEnabled( !useExpandedAudioHeap );
	m_ui->bank3objectList->setEnabled( !useExpandedAudioHeap );

	QDialog::open();
}

void ObjectBanksDialog::accept() {
	assert( Blueprint::current()->hasLevel( m_levelId ) );
	sbyte *banks = Blueprint::currentEditable()->levels()[m_levelId].objectBanks;
	banks[0] = (sbyte)m_ui->bank1select->currentIndex() - 1;
	banks[1] = (sbyte)m_ui->bank2select->currentIndex() - 1;
	banks[2] = (sbyte)m_ui->bank3select->currentIndex() - 1;
	QDialog::accept();
}
