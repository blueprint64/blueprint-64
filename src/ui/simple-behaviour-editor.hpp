#ifndef SRC_UI_SIMPLE_BEHAVIOUR_EDITOR_HPP_
#define SRC_UI_SIMPLE_BEHAVIOUR_EDITOR_HPP_

#include <QWidget>
#include <QFrame>
#include "src/core/enums.hpp"
#include "src/core/objectid.hpp"
#include "src/core/data/behaviour.hpp"
#include "src/ui/widgets/simple-behaviour-step-panel.hpp"

namespace Ui {
	class SimpleBehaviourEditor;
}

class SimpleBehaviourEditor : public QWidget {
	Q_OBJECT

	private:
	Ui::SimpleBehaviourEditor *const m_ui;
	QFrame *m_loopInstruction;
	LevelId m_levelId;
	objectId m_objectId;
	bool m_ignoreEvents;

	const SimpleBehaviour &getConstModel() const;
	SimpleBehaviour &getModel();

	SimpleBehaviourStepPanel *createInstructionPanel( size_t index ) const;

	private slots:
	void addInstruction();
	void removeInstruction( size_t index );
	void moveUp( size_t index );
	void moveDown( size_t index );
	void collisionDistanceModeChanged( int mode, bool enabled );
	void renderDistanceModeChanged();

	public:
	explicit SimpleBehaviourEditor( QWidget *parent = nullptr );
	~SimpleBehaviourEditor();

	void setBehaviour( LevelId levelId, objectId objectId );

};



#endif /* SRC_UI_SIMPLE_BEHAVIOUR_EDITOR_HPP_ */
