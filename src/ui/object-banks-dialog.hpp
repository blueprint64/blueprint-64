#ifndef SRC_UI_OBJECT_BANKS_DIALOG_HPP_
#define SRC_UI_OBJECT_BANKS_DIALOG_HPP_

#include <QWidget>
#include <QDialog>
#include "src/types.hpp"
#include "src/core/enums.hpp"

namespace Ui {
	class ObjectBanksDialog;
}

class ObjectBanksDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ObjectBanksDialog *m_ui;
	LevelId m_levelId;

	public slots:
	void bank1changed( int index );
	void bank2changed( int index );
	void bank3changed( int index );

	public:
	explicit ObjectBanksDialog( QWidget *parent = nullptr );
	~ObjectBanksDialog();

	void open( LevelId levelId );
	void accept() override;

};

#endif /* SRC_UI_OBJECT_BANKS_DIALOG_HPP_ */
