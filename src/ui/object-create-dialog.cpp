#include "src/ui/object-create-dialog.hpp"
#include "ui_object-create-dialog.h"

#include "src/core/blueprint.hpp"

ObjectCreateDialog::ObjectCreateDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::ObjectCreateDialog )
{
	m_ui->setupUi( this );
}

ObjectCreateDialog::~ObjectCreateDialog() {
	delete m_ui;
}

void ObjectCreateDialog::open( LevelId levelId ) {
	if( Blueprint::current()->hasFreeModelIds( levelId ) ) {
		m_ui->simpleObjectRadio->setEnabled( true );
		m_ui->advancedObjectRadio->setEnabled( !Blueprint::current()->levels().at( levelId ).userData.empty() );
		m_ui->simpleObjectRadio->setChecked( true );
	} else {
		m_ui->simpleObjectRadio->setEnabled( false );
		m_ui->advancedObjectRadio->setEnabled( false );
		m_ui->partialObjectRadio->setChecked( true );
	}
	QDialog::open();
}

ObjectType ObjectCreateDialog::getSelection() const {
	if( m_ui->simpleObjectRadio->isChecked() ) {
		return ObjectType::Simple;
	} else if( m_ui->partialObjectRadio->isChecked() ) {
		return ObjectType::Partial;
	} else {
		return ObjectType::Advanced;
	}
}
