#include <iostream>
#include <cassert>

#include <QApplication>
#include <QMessageBox>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#if !defined(__linux__) && !defined(__unix__)
#include "src/polyfill/window-slit.hpp"
#include <QFile>
#endif

#ifdef _WIN32
#include "src/ui/windows-ui-fixes.hpp"
#endif

#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/file-dialog.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/ui/start-window.hpp"
#include "src/ui/main-window.hpp"
#include "src/ui/blueprint-controller.hpp"
#include "src/ui/util.hpp"
#include "src/ui/base-rom.hpp"
#include "src/ui/tooltip-hack.hpp"
#include "src/core/tweak.hpp"
#include "src/core/storage.hpp"
#include "src/core/armips.hpp"
#include "src/core/version.hpp"
#include "src/core/asm-modules.hpp"

static void updateCheckAsync() {
	QNetworkAccessManager *web = new QNetworkAccessManager();
	QNetworkReply *response = web->get(
		QNetworkRequest( QUrl( "https://blueprint64.ca/version/latest" ) )
	);

	QObject::connect( response, QOverload<QNetworkReply::NetworkError>::of(
#if QT_VERSION >= QT_VERSION_CHECK( 5, 15, 0 )
		/* Way to make a breaking change in a minor version. Very cool. */
		&QNetworkReply::errorOccurred
#else
		&QNetworkReply::error
#endif
	), [=](QNetworkReply::NetworkError error) {
		(void)error;
		response->deleteLater();
		web->deleteLater();
	});

	QObject::connect( response, &QNetworkReply::finished, [=](){
		QByteArray rawData = response->readAll();
		if( !rawData.isNull() ) {
			std::optional<Version> latest = Version::tryParse( rawData.toStdString() );
			if( latest.has_value() && latest.value() > CurrentVersion::Application ) {
				QMessageBox::information( nullptr, "Update Available", "A new version of Bowser's Blueprints is available. Download the latest version at <a href=\"https://blueprint64.ca\">https://blueprint64.ca</a>", QMessageBox::Ok );
			}
		}

		response->deleteLater();
		web->deleteLater();
	});
}

static void loadTweakDataFile( const char *tweakFile ) {
	QResourceStream resourceStream( tweakFile );
	std::vector<Uuid> conflicts = TweakStore::instance().loadFileAndGetConflicts( resourceStream, true );
	for( const Uuid &conflict : conflicts ) {
		std::cout << conflict.toString() << std::endl << std::flush;
	}
	assert( conflicts.size() == 0 );
}

static inline void loadTweakDefinitions() {
	loadTweakDataFile( ":/tweaks/default.json" );
	loadTweakDataFile( ":/tweaks/basic.json" );
	loadTweakDataFile( ":/tweaks/numeric.json" );
	loadTweakDataFile( ":/tweaks/composite.json" );
	loadTweakDataFile( ":/tweaks/enemies.json" );
	loadTweakDataFile( ":/tweaks/switch.json" );
	loadTweakDataFile( ":/tweaks/extend-vertical.json" );
	loadTweakDataFile( ":/tweaks/community-contributions.json" );
}

static inline void connectSignalsAndSlots(
	BlueprintController *fileController,
	StartWindow *startupWindow,
	MainWindow *mainWindow
) {
	QObject::connect(
		startupWindow, SIGNAL(projectCreated(CreateProjectOptions)),
		fileController, SLOT(createProject(CreateProjectOptions))
	);

	QObject::connect(
		startupWindow, SIGNAL(projectOpened(string)),
		fileController, SLOT(openProject(string))
	);

	QObject::connect(
		fileController, SIGNAL(blueprintLoaded()),
		mainWindow, SLOT(loadBlueprint())
	);

	QObject::connect(
		fileController, SIGNAL(blueprintLoaded()),
		startupWindow, SLOT(close())
	);

	QObject::connect(
		fileController, SIGNAL(loadFailed(string)),
		startupWindow, SLOT(showErrorDialog(string))
	);

	QObject::connect(
		mainWindow, SIGNAL(openNew()),
		startupWindow, SLOT(openProject())
	);

	QObject::connect(
		mainWindow, SIGNAL(createNew()),
		startupWindow, SLOT(newProject())
	);

	QObject::connect(
		fileController, SIGNAL(loadFailed(string)),
		mainWindow, SLOT(showErrorDialog(string))
	);
}

static int run( QApplication &app, const string &blueprintPath ) {
	loadTweakDefinitions();

	BlueprintController fileController;
	StartWindow startupWindow;
	MainWindow mainWindow;

	if( !Armips::exists() ) {
		#ifdef _WIN32
		const string ARMIPS_EXE = "armips.exe";
		const fs::path bundledArmipsPath = currentProgramPath().parent_path() / "armips" / ARMIPS_EXE;
		#else
		const string ARMIPS_EXE = "armips";
		const fs::path bundledArmipsPath = fs::to_path( "/usr/share/blueprint64/armips-default" );
		#endif

		if( fs::exists( bundledArmipsPath ) ) {
			fs::create_directories( AppData::dataDir() );
			fs::copy( bundledArmipsPath, AppData::dataDir() / ARMIPS_EXE );
		} else {
			QMessageBox::information( nullptr, "Missing Armips", "The Armips executable bundled with Bowser's Blueprints could not be found. You will need to provide the path to Armips now." );
			const fs::path armipsPath = FileDialog::getFileOpen( "Where is Armips?" );
			if( armipsPath.empty() ) {
				return 1;
			}
			fs::create_directories( AppData::dataDir() );
			fs::copy( armipsPath, AppData::dataDir() / ARMIPS_EXE );
		}
	}

	connectSignalsAndSlots( &fileController, &startupWindow, &mainWindow );
	AppPreferences::current().save();

	bool loadedBlueprint = false;
	try {
		if( !blueprintPath.empty() && fs::exists( fs::to_path( blueprintPath ) ) ) {
			Blueprint::open( fs::to_path( blueprintPath ) );
			loadedBlueprint = true;
		} else if( AppPreferences::current().openOnStart && !RecentProjects::get().empty() ) {
			Blueprint::open( fs::to_path( RecentProjects::get().front() ) );
			loadedBlueprint = true;
		}
	} catch( ... ) {
		std::cerr << "Failed to load most recent project." << std::endl << std::flush;
	}

	loadedBlueprint ? mainWindow.loadBlueprint() : startupWindow.show();
	AsmModuleStore::installBuiltinModules();
	updateCheckAsync();

	return app.exec();
}

#ifdef _WIN32
static inline void clearTemporaryFiles() {
	for( const auto &t : fs::directory_iterator( Windows::getKnownFolderPath( Windows::KnownFolder::Temp ) ) ) {
		if( t.path().stem().u8string().compare( 0, 13, "bbp_blueprint" ) != 0 ) {
			continue;
		}

		std::error_code err;
		fs::tryDeleteRecursive( t.path() );
	}
}
#endif

int main( int argc, char *argv[] ) {
	const string arg1 = (argc > 1) ? string( argv[1] ) : string();

	fs::create_directories( AppData::cacheDir() );
	fs::create_directories( AppData::configDir() );
	fs::create_directories( AppData::dataDir() );

	QApplication app( argc, argv );
#ifdef _WIN32
	// Windows doesn't actually clear temporary files because of course it doesn't
	if( !Windows::isAlreadyRunning() ) {
		clearTemporaryFiles();
	}

	// Make Windows version a little less ugly
	WindowsUiFixes::apply( app );
#endif

	const string &theme = AppPreferences::current().theme;
	if( !theme.empty() ) {
		QApplication::setStyle( theme.c_str() );
	}

	if( !QIcon::themeName().toLower().contains( "breeze" ) ) {
		QIcon::setThemeName( "breeze-fallback" );
	}

	TooltipHack tooltipHack( &app );
	app.installEventFilter( &tooltipHack );
	BaseRom::init();

	int exitCode = run( app, arg1 );
	Blueprint::free();
	return exitCode;
}
