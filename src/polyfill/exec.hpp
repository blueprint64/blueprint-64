#ifndef SRC_POLYFILL_EXEC_HPP_
#define SRC_POLYFILL_EXEC_HPP_

#include <string>
#include <system_error>

namespace Command {

	extern bool execSync( const std::string &cmd, std::error_code &err );
	extern bool execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output, std::error_code &err );
	extern bool execDetached( const std::string &cmd, std::error_code &err );

}



#endif /* SRC_POLYFILL_EXEC_HPP_ */
