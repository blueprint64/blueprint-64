#ifndef POLYFILL_TAR_HPP_
#define POLYFILL_TAR_HPP_

#include <string>
#include <system_error>

namespace tarball {
	extern bool create( const std::string &outPath, const std::string &dir, std::error_code &err );
	extern bool extract( const std::string &tarPath, const std::string &outDir, std::error_code &err );

	inline bool create( const std::string &outPath, const std::string &dir ) {
		std::error_code err;
		return create( outPath, dir, err ) && !err;
	}

	inline bool extract( const std::string &tarPath, const std::string &outDir ) {
		std::error_code err;
		return extract( tarPath, outDir, err ) && !err;
	}

#if defined(__linux__) || defined(__unix__)
	constexpr bool toolInstalled() { return true; }
#else
	extern bool toolInstalled();
#endif
}

#endif /* POLYFILL_TAR_HPP_ */
