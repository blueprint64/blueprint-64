#include "src/polyfill/appdata.hpp"

#ifdef _WIN32

#include "src/polyfill/window-slit.hpp"

inline static fs::path getLocalAppDataPath() {
	const fs::path dataPath = Windows::getKnownFolderPath( Windows::KnownFolder::LocalAppData );
	fs::create_directories( dataPath );
	return dataPath;
}

inline static const fs::path &baseDir() {
	static const fs::path dir = getLocalAppDataPath() / "bowsers-blueprints";
	return dir;
}

const fs::path AppData::configDir() {
	return baseDir() / "config";
}

const fs::path AppData::cacheDir() {
	return baseDir() / "cache";
}

const fs::path AppData::dataDir() {
	return baseDir() / "data";
}

const fs::path AppData::programDir() {
	return Windows::getKnownFolderPath( Windows::KnownFolder::CurrentProgram );
}

#else

#include <cstdlib>

using namespace std;

inline static string xdg( const char *varName, const string &defaultRelative ) {
	char *xdgVal = getenv( varName );
	if( xdgVal != nullptr && xdgVal[0] != '\0' ) {
		return string( xdgVal );
	}
	return string( getenv( "HOME" ) ) + "/" + defaultRelative;
}

const fs::path AppData::configDir() {
	static const string dir = xdg( "XDG_CONFIG_HOME", ".config" ) + "/bowsers-blueprints";
	return fs::path( dir );
}

const fs::path AppData::cacheDir() {
	static const string dir = xdg( "XDG_CACHE_HOME", ".cache" ) + "/bowsers-blueprints";
	return fs::path( dir );
}

const fs::path AppData::dataDir() {
	static const string dir = xdg( "XDG_DATA_HOME", ".local/share" ) + "/bowsers-blueprints";
	return fs::path( dir );
}

const fs::path AppData::programDir() {
	return fs::read_symlink( "/proc/self/exe" ).parent_path();
}

#endif
