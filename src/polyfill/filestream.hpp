#ifndef SRC_POLYFILL_FILESTREAM_HPP_
#define SRC_POLYFILL_FILESTREAM_HPP_

#include <fstream>
#include <string>
#include "src/openmode.hpp"
#include "src/types.hpp"

class FileStream : public std::fstream {

	private:
	char *m_buffer;

	public:
	FileStream();
	FileStream( FileStream &&other ) = delete;
	FileStream( const FileStream &&other ) = delete;
	explicit FileStream( const char *filename, std::ios_base::openmode mode = mode::readWrite );
	explicit FileStream( const string &filename, std::ios_base::openmode mode = mode::readWrite );
	virtual ~FileStream();

};

class FileReadStream : public std::ifstream {

	private:
	char *m_buffer;

	public:
	FileReadStream();
	FileReadStream( FileReadStream &&other ) = delete;
	FileReadStream( const FileReadStream &&other ) = delete;
	explicit FileReadStream( const char *filename, std::ios_base::openmode mode = mode::read );
	explicit FileReadStream( const string &filename, std::ios_base::openmode mode = mode::read );
	virtual ~FileReadStream();

};

class FileWriteStream : public std::ofstream {

	private:
	char *m_buffer;

	public:
	FileWriteStream();
	FileWriteStream( FileWriteStream &&other ) = delete;
	FileWriteStream( const FileWriteStream &&other ) = delete;
	explicit FileWriteStream( const char *filename, std::ios_base::openmode mode = mode::write );
	explicit FileWriteStream( const string &filename, std::ios_base::openmode mode = mode::write );
	virtual ~FileWriteStream();

};


#endif /* SRC_POLYFILL_FILESTREAM_HPP_ */
