#include "src/polyfill/filestream.hpp"

/*
 * In the standard library used when compiling on Windows, std::fstream and
 * friends do not use a buffer. These classes force it to use an 8kB buffer,
 * which is the default size on Linux.
 */

static constexpr size_t BUFFER_SIZE = 8096;

FileStream::FileStream() :
	std::fstream(),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileStream::FileStream( const char *filename, std::ios_base::openmode mode ) :
	std::fstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileStream::FileStream( const string &filename, std::ios_base::openmode mode ) :
	std::fstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileStream::~FileStream() {
	if( m_buffer != nullptr ) {
		flush();
		rdbuf()->pubsetbuf( nullptr, 0 );
		delete[] m_buffer;
		m_buffer = nullptr;
	}
}


FileReadStream::FileReadStream() :
	std::ifstream(),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileReadStream::FileReadStream( const char *filename, std::ios_base::openmode mode ) :
	std::ifstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileReadStream::FileReadStream( const string &filename, std::ios_base::openmode mode ) :
	std::ifstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileReadStream::~FileReadStream() {
	if( m_buffer != nullptr ) {
		rdbuf()->pubsetbuf( nullptr, 0 );
		delete[] m_buffer;
		m_buffer = nullptr;
	}
}


FileWriteStream::FileWriteStream() :
	std::ofstream(),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileWriteStream::FileWriteStream( const char *filename, std::ios_base::openmode mode ) :
	std::ofstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileWriteStream::FileWriteStream( const string &filename, std::ios_base::openmode mode ) :
	std::ofstream( filename, mode ),
	m_buffer( new char[BUFFER_SIZE] )
{
	rdbuf()->pubsetbuf( m_buffer, BUFFER_SIZE );
}

FileWriteStream::~FileWriteStream() {
	if( m_buffer != nullptr ) {
		flush();
		rdbuf()->pubsetbuf( nullptr, 0 );
		delete[] m_buffer;
		m_buffer = nullptr;
	}
}
