#ifndef SRC_POLYFILL_FASTMATH_HPP_
#define SRC_POLYFILL_FASTMATH_HPP_

#include "src/types.hpp"

inline uint ilog2( uint x ) {
#ifdef _WIN32
    ulong result;
    _BitScanReverse( &result, x );
    return result;
#else
    uint result;
	asm(
		"bsr %1, %0;"
		: "=r"(result)
		: "r"(x)
	);
    return result;
#endif
}

inline int bitcount( uint flags ) {
#ifdef _WIN32
	int count = 0;
	while( flags != 0 ) {
		flags &= flags - 1;
		count++;
	}
	return count;
#else
	return __builtin_popcount( flags );
#endif
}

inline bool isPowerOf2( uint n ) {
	return n != 0 && ( n & ( n - 1 ) ) == 0;
}


#endif /* SRC_POLYFILL_FASTMATH_HPP_ */
