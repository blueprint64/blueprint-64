#include "src/polyfill/tar.hpp"

#include <cstdlib>
#include "src/core/util.hpp"

#if defined(__linux__) || defined(__unix__)
	using std::string;

	bool tarball::create( const string &outPath, const string &dir, std::error_code &err ) {
		const string cmd = "tar -c -z -f " + quoteAndEscape( outPath ) + " -C " + quoteAndEscape( dir ) + " .";
		err = std::make_error_code( (std::errc)std::system( cmd.c_str() ) );
		return !err;
	}

	bool tarball::extract( const string &tarPath, const string &outDir, std::error_code &err ) {
		const string cmd = "tar -x -z -f " + quoteAndEscape( tarPath ) + " -C " + quoteAndEscape( outDir );
		err = std::make_error_code( (std::errc)std::system( cmd.c_str() ) );
		return !err;
	}

#else
	#include "src/core/storage.hpp"
	#include "src/polyfill/filesystem.hpp"
	#include "src/polyfill/window-slit.hpp"

	static inline string get7zCmd() {
		const string &path = AppPreferences::current().sevenZipPath;
		if( path.empty() ) return "7z";
		return quoteAndEscape( path );
	}

	bool tarball::create( const string &outPath, const string &dir, std::error_code &err ) {
		const string zipExe = get7zCmd();

		const fs::path tempDir = fs::to_path( createTemporaryDirectory() );
		const string tempTar = (tempDir / "temp.tar").u8string();

		if( !Windows::execSync( zipExe + " a " + quoteAndEscape( tempTar ) + " " + quoteAndEscape( (fs::to_path( dir ) / "*").u8string() ) + " -ttar -r", err ) ) {
			fs::tryDeleteRecursive( tempDir );
			return false;
		}

		const fs::path outputPath = fs::to_path( outPath );
		if( fs::exists( outputPath ) ) {
			clearReadOnlyFlagOnWindows( outputPath );
			fs::remove( outputPath, err );
			if( err ) return false;
		}

		Windows::execSync( zipExe + " a " + quoteAndEscape( outPath ) + " " + quoteAndEscape( tempTar ) + " -tgzip", err );
		fs::tryDeleteRecursive( tempDir );
		return !err;
	}

	bool tarball::extract( const string &tarPath, const string &outDir, std::error_code &err ) {
		const string unzipExe = get7zCmd();

		const fs::path tempDir = fs::to_path( createTemporaryDirectory() );
		if( !Windows::execSync( unzipExe + " x " + quoteAndEscape( tarPath ) + " -tgzip -aoa -y -o" + quoteAndEscape( tempDir.u8string() ), err ) ) {
			fs::tryDeleteRecursive( tempDir );
			return false;
		}

		// Get the only file in the directory
		clearReadOnlyFlagOnWindows( tempDir );
		for( const auto &tarFile : fs::directory_iterator( tempDir ) ) {
			if( !Windows::execSync( unzipExe + " x " + quoteAndEscape( tarFile.path().u8string() ) + " -ttar -aoa -y -o" + quoteAndEscape( outDir ), err ) ) {
				fs::tryDeleteRecursive( tempDir );
				return false;
			}

			fs::tryDeleteRecursive( tempDir );
			return true;
		}

		// No tarchive was extracted
		fs::tryDeleteRecursive( tempDir );
		err = std::make_error_code( (std::errc)101 );
		return false;
	}

	bool tarball::toolInstalled() {
		const string _7z = AppPreferences::current().sevenZipPath;
		if( _7z.empty() ) {
	#ifdef _WIN32
			return std::system( "where 7z" ) == 0;
	#else
			return std::system( "which 7z" ) == 0;
	#endif
		} else {
			return fs::exists( fs::to_path( _7z ) );
		}
	}
#endif
