#include "src/polyfill/window-slit.hpp"

#ifdef _WIN32
#include <exception>
#include <system_error>
#include <windows.h>
#include <knownfolders.h>
#include <libloaderapi.h>
#include <shlobj.h>
#include <winnt.h>
#include <string>
#include <locale>
#include <codecvt>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/buffer.hpp"
#include "src/core/unicode.hpp"

class winapi_category : public std::error_category {

	public:
	winapi_category() noexcept : std::error_category() {}

	virtual const char *name() const noexcept override {
		return "Windows API Error";
	}

	virtual std::string message( int condition ) const override {
		if( condition < 0 ) return "Unknown error";

		char *msg = nullptr;
		if( FormatMessageA(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			nullptr,
			(DWORD)condition,
			0,
			(LPSTR)&msg,
			0,
			nullptr
		) == 0 ) {
			return "Unknown error";
		}

		const std::string msgStr = msg;
		LocalFree( msg );

		return msgStr;
	}

};

class process_category : public std::error_category {

	public:
	process_category() noexcept : std::error_category() {}

	virtual const char *name() const noexcept override {
		return "External Program Error";
	}

	virtual std::string message( int condition ) const override {
		return std::string( "External process exited with code " ) + std::to_string( condition );
	}

};

static const winapi_category s_winApiErr;
static const process_category s_extProcErr;

static inline void setError( std::error_code &err ) {
	const DWORD errorCode = GetLastError();
	if( errorCode != 0 ) {
		err.assign( errorCode < 0x80000000ull ? (int)errorCode : -1, s_winApiErr );
	}
}

static std::filesystem::path _getAbsoluteDirectory(
	REFKNOWNFOLDERID folderId
) {
	wchar_t *buffer;
	if( SHGetKnownFolderPath(
		folderId,
		KF_FLAG_NO_ALIAS | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) == S_OK ) {
		const std::filesystem::path result = std::filesystem::path( buffer );
		CoTaskMemFree( buffer );
		return result;
	}

	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	throw std::runtime_error( "Failed to get known directory location from Windows." );
}

static std::filesystem::path getDataDirectory(
	REFKNOWNFOLDERID folderId,
	const char *defaultRelativePath
) {
	wchar_t *buffer;
	if( SHGetKnownFolderPath(
		folderId,
		KF_FLAG_CREATE | KF_FLAG_NO_ALIAS | KF_FLAG_DONT_VERIFY | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) == S_OK ) {
		const std::string result = Unicode::toUTF8( buffer );
		CoTaskMemFree( buffer );
		return result;
	}

	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	return fs::path( _getAbsoluteDirectory( FOLDERID_Profile ) ) / defaultRelativePath;
}

static inline std::filesystem::path getProgramDirectory() {
	wchar_t currentExePath[261];
	if( GetModuleFileNameW( nullptr, currentExePath, 261 ) == 0 ) {
		throw std::runtime_error( "Failed to get current executable path from Windows." );
	}

	return fs::path( currentExePath ).parent_path();
}

std::filesystem::path Windows::getKnownFolderPath( Windows::KnownFolder folder ) {
	switch( folder ) {
		case Windows::KnownFolder::Home:
			return _getAbsoluteDirectory( FOLDERID_Profile );
		case Windows::KnownFolder::ProgramFiles32:
			return _getAbsoluteDirectory( FOLDERID_ProgramFilesX86 );
		case Windows::KnownFolder::LocalAppData:
			return getDataDirectory( FOLDERID_LocalAppData, "AppData\\Local" );
		case Windows::KnownFolder::RoamingAppData:
			return getDataDirectory( FOLDERID_RoamingAppData, "AppData\\Roaming" );
		case Windows::KnownFolder::Temp:
#ifdef _WIN64
			return fs::temp_directory_path();
#else
			return getDataDirectory( FOLDERID_LocalAppData, "AppData\\Local" ) / "Temp");
#endif
		case Windows::KnownFolder::CurrentProgram:
			return getProgramDirectory();
		default:
			throw std::domain_error( "Invalid folder" );
	}
}

static inline void clearFileAttributes( const std::filesystem::path &path ) {
	SetFileAttributesW( path.wstring().c_str(), FILE_ATTRIBUTE_NORMAL );
}

void Windows::clearFileAttributesRecursive( const fs::path &path ) {
	if( !fs::exists( path ) ) return;

	clearFileAttributes( path );
	if( fs::status( path ).type() != fs::file_type::directory ) {
		return;
	}

	for( const auto &f : fs::recursive_directory_iterator( path ) ) {
		clearFileAttributes( f.path() );
	}
}

std::filesystem::path Windows::getProgramPath() {
	wchar_t buffer[MAX_PATH];
	if( GetModuleFileName( nullptr, buffer, MAX_PATH - 1 ) <= 0 ) {
		throw std::runtime_error( "Failed to get program path." );
	}
	return std::filesystem::path( buffer );
}

bool Windows::execSync( const std::string &cmd, std::error_code &err ) {
	_STARTUPINFOW si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);

	std::wstring lpCommandLine = Unicode::toUTF16( cmd );
	if( !CreateProcessW(
		nullptr,
		lpCommandLine.data(),
		nullptr,
		nullptr,
		true,
		CREATE_NO_WINDOW,
		nullptr,
		nullptr,
		&si,
		&pi
	)) {
		setError( err );
		return false;
	}

	if( WaitForSingleObject( pi.hProcess, INFINITE ) == WAIT_FAILED ) {
		setError( err );
		return false;
	}

	DWORD exitCode;
	if( !GetExitCodeProcess( pi.hProcess, &exitCode ) ) {
		setError( err );
		return false;
	}

	if( !CloseHandle( pi.hProcess ) ) {
		setError( err );
		return false;
	}

	if( !CloseHandle( pi.hThread ) ) {
		setError( err );
		return false;
	}

	if( exitCode != 0 ) {
		err.assign( (int)exitCode, s_extProcErr );
		return false;
	}

	return true;
}

bool Windows::execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output, std::error_code &err ) {
	_STARTUPINFOW si;
	PROCESS_INFORMATION pi;

	HANDLE stdOutRead = nullptr;
	HANDLE stdOutWrite = nullptr;

	SECURITY_ATTRIBUTES security;
	security.nLength = sizeof( SECURITY_ATTRIBUTES );
	security.bInheritHandle = true;
	security.lpSecurityDescriptor = nullptr;

	if( !CreatePipe( &stdOutRead, &stdOutWrite, &security, 1024 ) ) {
		setError( err );
		return false;
	}

	if( !SetHandleInformation( stdOutRead, HANDLE_FLAG_INHERIT, 0 ) ) {
		setError( err );
		return false;
	}

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);
	si.hStdOutput = stdOutWrite;
	si.dwFlags |= STARTF_USESTDHANDLES;

	std::wstring lpCommandLine = Unicode::toUTF16( cmd );
	std::wstring lpCurrentDirectory = Unicode::toUTF16( workingDirectory );

	output = "";
	if( !CreateProcessW(
		nullptr,
		lpCommandLine.data(),
		nullptr,
		nullptr,
		true,
		CREATE_NO_WINDOW,
		nullptr,
		lpCurrentDirectory.data(),
		&si,
		&pi
	)) {
		setError( err );
		return false;
	}

	if( !CloseHandle( stdOutWrite ) ) {
		setError( err );
		return false;
	}

	DWORD bytesRead = 0;
	char buffer[1025];

	while( ReadFile( stdOutRead, buffer, 1024, &bytesRead, nullptr ) && bytesRead > 0 ) {
		buffer[bytesRead] = '\0';
		output += buffer;
	}

	if( !CloseHandle( stdOutRead ) ) {
		setError( err );
		return false;
	}

	DWORD exitCode;
	if( !GetExitCodeProcess( pi.hProcess, &exitCode ) ) {
		setError( err );
		return false;
	}

	if( !CloseHandle( pi.hProcess ) ) {
		setError( err );
		return false;
	}

	if( !CloseHandle( pi.hThread ) ) {
		setError( err );
		return false;
	}

	if( exitCode != 0 ) {
		err.assign( (int)exitCode, s_extProcErr );
		return false;
	}

	return true;
}

bool Windows::execDetached( const std::string &cmd, std::error_code &err ) {
	static _STARTUPINFOW si;
	static PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);

	std::wstring lpCommandLine = Unicode::toUTF16( cmd );
	if( !CreateProcessW(
		nullptr,
		lpCommandLine.data(),
		nullptr,
		nullptr,
		true,
		DETACHED_PROCESS,
		nullptr,
		nullptr,
		&si,
		&pi
	)) {
		setError( err );
		return false;
	}

	return true;
}

bool Windows::isAlreadyRunning() {
	return (CreateMutexA( nullptr, true, "Global_blueprint64" ) == nullptr) && (GetLastError() == ERROR_ALREADY_EXISTS);
}

bool Windows::tryWidenString( const char *str, std::wstring &utf16 ) {
	const int numWideChars = MultiByteToWideChar( CP_ACP, 0, str, -1, nullptr, 0 );
	Buffer wideBuffer( numWideChars * 2 );
	if( MultiByteToWideChar( CP_ACP, 0, str, -1, (wchar_t*)wideBuffer.data(), numWideChars ) ) {
		utf16 = std::wstring( (wchar_t*)wideBuffer.data() );
		return true;
	}
	return false;
}

#endif
