#ifndef POLYFILL_APPDATA_HPP_
#define POLYFILL_APPDATA_HPP_

#include "src/polyfill/filesystem.hpp"

namespace AppData {
	const fs::path configDir();
	const fs::path cacheDir();
	const fs::path dataDir();
	const fs::path programDir();
}


#endif /* POLYFILL_APPDATA_HPP_ */
