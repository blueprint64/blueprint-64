#ifndef SRC_POLYFILL_WINDOW_SLIT_HPP_
#define SRC_POLYFILL_WINDOW_SLIT_HPP_

#ifdef _WIN32

#include <string>
#include <system_error>
#include <filesystem>

/* The Windows API includes a bunch of dumb poorly named macros that cause collisions with
 * other code. To avoid this, I only include the Windows API header in one file, then expose
 * only the methods I need through the header.
 *
 * Windows is dumb.
 */
namespace Windows {

	enum class KnownFolder : unsigned char {
		LocalAppData,
		RoamingAppData,
		Home,
		Temp,
		ProgramFiles32,
		CurrentProgram
	};

	extern std::filesystem::path getKnownFolderPath( KnownFolder folder );
	extern std::filesystem::path getProgramPath();

	extern void clearFileAttributesRecursive( const std::filesystem::path &path );

	extern bool execSync( const std::string &cmd, std::error_code &err );
	extern bool execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output, std::error_code &err );
	extern bool execDetached( const std::string &cmd, std::error_code &err );

	extern bool tryWidenString( const char *str, std::wstring &utf16 );

	extern bool isAlreadyRunning();

}

#endif

#endif /* SRC_POLYFILL_WINDOW_SLIT_HPP_ */
