#ifndef SRC_POLYFILL_BUFFER_HPP_
#define SRC_POLYFILL_BUFFER_HPP_

// Windows compilers don't support variable length arrays >:(
#include <cstddef>
#include <cstring>

class Buffer {

	private:
	char *m_data;

	public:
	inline Buffer( size_t size ) : m_data( new char[size] ) {}
	inline Buffer( size_t size, const void *data ) : m_data( new char[size] ) {
		std::memcpy( m_data, data, size );
	}

	Buffer( const Buffer &other ) = delete;
	inline Buffer( Buffer &&other ) : m_data( other.m_data ) {
		other.m_data = nullptr;
	}

	~Buffer() {
		if( m_data != nullptr ) delete[] m_data;
	}

	inline char *data() noexcept { return m_data; }
	inline const char *data() const noexcept { return m_data; }

};



#endif /* SRC_POLYFILL_BUFFER_HPP_ */
