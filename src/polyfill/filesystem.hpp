#ifndef POLYFILL_FILESYSTEM_HPP_
#define POLYFILL_FILESYSTEM_HPP_

#include <string>
#include <filesystem>

#ifdef _WIN32
	#include "src/core/unicode.hpp"
#else
	#include <cstdlib>
#endif

#include "src/core/json.hpp"

namespace fs = std::filesystem;

extern std::string createTemporaryDirectory();
extern fs::path currentProgramPath();

#ifdef _WIN32
	extern fs::path getHomeDirectory();
	extern void clearReadOnlyFlagOnWindows( const fs::path &path );
#else
	inline fs::path getHomeDirectory() { return fs::path( std::getenv( "HOME" ) ); }
	inline void clearReadOnlyFlagOnWindows( [[maybe_unused]] const fs::path &path ) {}
#endif

namespace std::filesystem {

	// More dumb Windows workarounds

	inline void forceDelete( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		remove( filename );
	}

	inline void forceDeleteRecursive( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		remove_all( filename );
	}

	inline bool tryDelete( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		std::error_code error;
		remove( filename, error );
		return !error;
	}

	inline bool tryDeleteRecursive( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		std::error_code error;
		remove_all( filename, error );
		return !error;
	}

#ifdef _WIN32
	extern bool tryCopyFileOverwrite( const path &source, const path &dest );

	inline path to_path( const std::string &str ) {
		return path( Unicode::toUTF16( str ) );
	}
#else
	inline bool tryCopyFileOverwrite( const path &source, const path &dest ) {
		std::error_code error;
		return copy_file( source, dest, copy_options::overwrite_existing, error ) && !error;
	}

	inline path to_path( const std::string &str ) {
		return path( str );
	}
#endif

}

namespace JsonSerializer {
	template<> void serialize<fs::path>( JsonWriter &jw, const fs::path &obj );
	template<> fs::path parse<fs::path>( const Json &json );
}

#endif /* POLYFILL_FILESYSTEM_HPP_ */
