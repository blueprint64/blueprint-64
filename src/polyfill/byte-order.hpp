#ifndef POLYFILL_BIT_ORDER_HPP_
#define POLYFILL_BIT_ORDER_HPP_

#ifdef _WIN32
	# if __BYTE_ORDER == __LITTLE_ENDIAN

		inline unsigned short htons( unsigned short value ) {
			return _byteswap_ushort( value );
		}

		inline unsigned short ntohs( unsigned short value ) {
			return _byteswap_ushort( value );
		}

		inline unsigned int htonl( unsigned int value ) {
			return _byteswap_ulong( value ); // "long" is always 32-bit on Windows (lol)
		}

		inline unsigned int ntohl( unsigned int value ) {
			return _byteswap_ulong( value );
		}

	# else
		inline unsigned short htons( unsigned short value ) { return value; }
		inline unsigned short ntohs( unsigned int value ) { return value; }
		inline unsigned short htonl( unsigned short value ) { return value; }
		inline unsigned short ntohl( unsigned int value ) { return value; }
	#endif
#else
	#include <netinet/in.h>
#endif

constexpr unsigned short operator"" _be16( unsigned long long x ) {
	# if __BYTE_ORDER == __LITTLE_ENDIAN
		return ((x >> 8) & 0xFF) | ((x << 8 ) & 0xFF00 );
	#else
		return x;
	#endif
}

constexpr unsigned int operator"" _be32( unsigned long long x ) {
	# if __BYTE_ORDER == __LITTLE_ENDIAN
		return(
			((x << 24) & 0xFF000000 ) |
			((x << 8)  & 0x00FF0000 ) |
			((x >> 8)  & 0x0000FF00 ) |
			((x >> 24) & 0x000000FF )
		);
	#else
		return x;
	#endif
}

#endif /* POLYFILL_BIT_ORDER_HPP_ */
