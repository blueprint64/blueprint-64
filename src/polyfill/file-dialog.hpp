#ifndef SRC_POLYFILL_FILE_DIALOG_HPP_
#define SRC_POLYFILL_FILE_DIALOG_HPP_

#include <initializer_list>
#include <filesystem>
#include <vector>

namespace FileDialog {

	struct FileFilter {
		const char *name;
		const char *patterns;
	};

	std::filesystem::path getDirectory( const char *dialogTitle );
	std::filesystem::path getFileOpen( const char *dialogTitle );
	std::filesystem::path getFileSave( const char *dialogTitle );
	std::vector<std::filesystem::path> getFilesOpen( const char *dialogTitle );
	std::filesystem::path getFileOpen( const char *dialogTitle, const std::initializer_list<FileFilter> &filters );
	std::filesystem::path getFileSave( const char *dialogTitle, const std::initializer_list<FileFilter> &filters );
	std::vector<std::filesystem::path> getFilesOpen( const char *dialogTitle, const std::initializer_list<FileFilter> &filters );

}

#endif /* SRC_POLYFILL_FILE_DIALOG_HPP_ */
