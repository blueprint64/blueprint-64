#include "src/polyfill/exec.hpp"

#ifdef _WIN32

#include "src/polyfill/window-slit.hpp"

bool Command::execSync( const std::string &cmd, std::error_code &err ) {
	return Windows::execSync( cmd, err );
}

bool Command::execSync(
	const std::string &cmd,
	const std::string &workingDirectory,
	std::string &output,
	std::error_code &err
) {
	return Windows::execSync( cmd, workingDirectory, output, err );
}

bool Command::execDetached( const std::string &cmd, std::error_code &err ) {
	return Windows::execDetached( cmd, err );
}

#else

#include <cstdlib>
#include <cstdio>
#include "src/types.hpp"
#include "src/core/util.hpp"

bool Command::execSync( const string &cmd, std::error_code &err ) {
	err = std::make_error_code( (std::errc)std::system( cmd.c_str() ) );
	return !err;
}

bool Command::execSync(
	const std::string &cmd,
	const std::string &workingDirectory,
	std::string &output,
	std::error_code &err
) {
	const std::string fullCmd = "cd "s + quoteAndEscape( workingDirectory ) + "; " + cmd;
	output = "";

	FILE *proc = popen( fullCmd.c_str(), "r" );
	if( proc == nullptr ) return false;

	char buffer[1024];
	while( fgets( buffer, 1024, proc ) != nullptr ) {
		output += buffer;
	}

	err = std::make_error_code( (std::errc)pclose( proc ) );
	return !err;
}

bool Command::execDetached( const string &cmd, std::error_code &err ) {
	const string fullCmd = "setsid "s + cmd + " &";
	//FIXME : setsid will always return a 0 exit code when running asynchronously
	err = std::make_error_code( (std::errc)std::system( fullCmd.c_str() ) );
	return !err;
}

#endif
