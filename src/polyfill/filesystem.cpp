#include "src/polyfill/filesystem.hpp"

#ifdef _WIN32
	#include "src/polyfill/random.hpp"
	#include "src/polyfill/window-slit.hpp"
	#include "src/types.hpp"
	#include "src/core/migration.hpp"
	#include "src/core/unicode.hpp"
#else
	#include <cstdlib>
	#include <unistd.h>
#endif

using std::string;

#ifdef _WIN32

typedef std::uint_fast64_t fast64;

string createTemporaryDirectory() {
	wstring basePath = (Windows::getKnownFolderPath( Windows::KnownFolder::Temp ) / "bbp_blueprint.").wstring();
	wchar_t rchars[9];

	rchars[8] = '\0';
	while( true ) {
		uint64 r = (uint64)Pseudorandom::getUInt() + ((uint64)Pseudorandom::getByte() << 32);
		for( size_t i = 0; i < 8; i++ ) {
			ubyte c = r & 0x1f;
			rchars[i] = c < 24 ? ( L'a' + (wchar_t)c ) : ( L'0' + (wchar_t)( c - 24 ) );
			r >>= 5;
		}

		fs::path fullPath( basePath + rchars );
		if( !fs::exists( fullPath ) ) {
			fs::create_directories( fullPath );
			return fullPath.u8string();
		}
	}
}

fs::path currentProgramPath() {
	return Windows::getProgramPath();
}

fs::path getHomeDirectory() {
	static const fs::path homeDir( Windows::getKnownFolderPath( Windows::KnownFolder::Home ) );
	return homeDir;
}

void clearReadOnlyFlagOnWindows( const fs::path &path ) {
	Windows::clearFileAttributesRecursive( path );
	//Windows::execSync( "attrib -r \""s + path + "\" /s /d" ); // Doesn't work >:(
}

bool fs::tryCopyFileOverwrite( const fs::path &source, const fs::path &dest ) {
	// Windows ignores the overwrite_existing option because it's stupid
	std::error_code error;
	if( !exists( source ) || status( source ).type() != file_type::regular ) {
		return false;
	} else if( !exists( dest ) ) {
		return copy_file( source, dest, error ) && !error;
	} else if( status( dest ).type() == file_type::regular ) {
		return tryDelete( dest ) && copy_file( source, dest, error ) && !error;
	}
	return false;
}
#else

string createTemporaryDirectory() {
	string pathTemplate = fs::temp_directory_path() / "bbp_blueprint.XXXXXX";
	if( mkdtemp( pathTemplate.data() ) == nullptr ) {
		throw std::runtime_error( "Failed to create temporary directory." );
	}
	return pathTemplate;
}

fs::path currentProgramPath() {
	char buffer[4096];
	if( readlink( "/proc/self/exe", buffer, 4095 ) < 0 ) {
		throw std::runtime_error( "Failed to get program path." );
	}
	return fs::path( buffer );
}
#endif

template<> void JsonSerializer::serialize<fs::path>( JsonWriter &jw, const fs::path &obj ) {
	jw.writeString( obj.u8string() );
}

template<> fs::path JsonSerializer::parse<fs::path>( const Json &json ) {
#ifdef _WIN32
	string pathString = json.get<string>();
	if( DeserializationContext::schemaVersion() < Version{ 4, 0, 0 } ) {
		pathString = Unicode::fixWindowsEncoding( pathString );
	}
	return fs::to_path( pathString );
#else
	return fs::path( json.get<string>() );
#endif
}
