#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_

#include <cstddef>
#include <sys/types.h>
#include <typeinfo>
#include <iostream>

#include "src/polyfill/byte-order.hpp"

using std::string;
using std::wstring;
using namespace std::string_literals;

#define HashMap std::unordered_map
#define HashSet std::unordered_set

#define foreach_key( i, map ) for( [[maybe_unused]] const auto &[i,_discard##__LINE__] : map )
#define foreach_cvalue( i, map ) for( [[maybe_unused]] const auto &[_discard##__LINE__,i] : map )
#define foreach_mvalue( i, map ) for( [[maybe_unused]] auto &[_discard##__LINE__,i] : map )

typedef signed char sbyte;
typedef unsigned char ubyte;

#ifdef _WIN32
typedef unsigned short ushort;
typedef unsigned uint;
typedef unsigned long ulong;
#endif

typedef long long int64;
typedef unsigned long long uint64;

static_assert( sizeof( sbyte ) == 1 );
static_assert( sizeof( ubyte ) == 1 );
static_assert( sizeof( char ) == 1 );
static_assert( sizeof( short ) == 2 );
static_assert( sizeof( ushort ) == 2 );
static_assert( sizeof( int ) == 4 );
static_assert( sizeof( uint ) == 4 );
static_assert( sizeof( float ) == 4 );
static_assert( sizeof( long long ) == 8 );
static_assert( sizeof( unsigned long long ) == 8 );
static_assert( sizeof( double ) == 8 );

struct Word final {
	private:
	uint m_data;

	Word( uint data ) : m_data( data ) {}

	public:
	Word() : m_data( 0 ) {}

	inline uint &asUInt() { return m_data; }
	inline int &asSInt() { return reinterpret_cast<int&>( m_data ); }
	inline float &asFloat() { return reinterpret_cast<float&>( m_data ); }

	inline const uint &asUInt() const { return m_data; }
	inline const int &asSInt() const { return reinterpret_cast<const int &>( m_data ); }
	inline const float &asFloat() const { return reinterpret_cast<const float&>( m_data ); }

	static inline Word fromUInt( uint data ) {
		return Word( data );
	}
	static inline Word fromSInt( int data ) {
		return Word( reinterpret_cast<const uint&>( data ) );
	}
	static inline Word fromFloat( float data ) {
		return Word( reinterpret_cast<const uint&>( data ) );
	}

	inline void writeToRom( std::ostream &rom ) const {
		const uint data = htonl( m_data );
		rom.write( (char*)&data, 4 );
	}

	static inline Word readFromRom( std::istream &rom ) {
		uint data;
		rom.read( (char*)&data, 4 );
		return Word( ntohl( data ) );
	}
};

struct HalfWord final {
	private:
	ushort m_data;

	HalfWord( ushort data ) : m_data( data ) {}

	public:
	HalfWord() : m_data( 0 ) {}

	inline ushort &asUShort() { return m_data; }
	inline short &asSShort() { return reinterpret_cast<short&>( m_data ); }
	inline void setFromUpperFloat( float data ) {
		m_data = reinterpret_cast<const uint&>( data ) >> 16;
	}
	inline void setFromLowerFloat( float data ) {
		m_data = reinterpret_cast<const uint&>( data ) & 0xFFFF;
	}

	inline const ushort &asUShort() const { return m_data; }
	inline const short &asSShort() const { return reinterpret_cast<const short &>( m_data ); }
	inline float getAsUpperFloat() const {
		const uint data = ((uint)m_data) << 16;
		return reinterpret_cast<const float&>( data );
	}

	static inline HalfWord fromUShort( ushort data ) {
		return HalfWord( data );
	}
	static inline HalfWord fromSShort( short data ) {
		return HalfWord( reinterpret_cast<const ushort&>( data ) );
	}
	static inline HalfWord fromUpperFloat( float data ) {
		return HalfWord( reinterpret_cast<const uint&>( data ) >> 16 );
	}
	static inline HalfWord fromLowerFloat( float data ) {
		return HalfWord( reinterpret_cast<const uint&>( data ) & 0xFFFF );
	}

	inline void writeToRom( std::ostream &rom ) const {
		const ushort data = htons( m_data );
		rom.write( (char*)&data, 2 );
	}

	static inline HalfWord readFromRom( std::istream &rom ) {
		ushort data;
		rom.read( (char*)&data, 2 );
		return HalfWord( ntohs( data ) );
	}

};

struct DoubleWord final {
	private:
	uint64 m_data;

	DoubleWord( uint64 data ) : m_data( data ) {}

	public:
	DoubleWord() : m_data( 0 ) {}

	inline uint64 &asULong() { return m_data; }
	inline int64 &asSLong() { return reinterpret_cast<int64&>( m_data ); }
	inline double &asDouble() { return reinterpret_cast<double&>( m_data ); }

	inline const uint64 &asULong() const { return m_data; }
	inline const int64 &asSLong() const { return reinterpret_cast<const int64&>( m_data ); }
	inline const double &asDouble() const { return reinterpret_cast<const double&>( m_data ); }

	static inline DoubleWord fromULong( uint64 data ) {
		return DoubleWord( data );
	}
	static inline DoubleWord fromSLong( int64 data ) {
		return DoubleWord( reinterpret_cast<const uint64&>( data ) );
	}
	static inline DoubleWord fromDouble( double data ) {
		return DoubleWord( reinterpret_cast<const uint64&>( data ) );
	}

	inline void writeToRom( std::ostream &rom ) const {
		uint data = htonl( (uint)( m_data >> 32 ));
		rom.write( (char*)&data, 4 );
		data = htonl( (uint)( m_data & 0xFFFFFFFFull ) );
		rom.write( (char*)&data, 4 );
	}

	static inline DoubleWord readFromRom( std::istream &rom ) {
		uint data1, data2;
		rom.read( (char*)&data1, 4 );
		rom.read( (char*)&data2, 4 );
		return DoubleWord( (((uint64)ntohl( data1 )) << 32) + (uint64)ntohl( data2 ) );
	}

};

static_assert( sizeof( HalfWord ) == 2 );
static_assert( sizeof( Word ) == 4 );
static_assert( sizeof( DoubleWord ) == 8 );

#endif /* GLOBALS_HPP_ */
