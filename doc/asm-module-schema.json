{
	"$schema": "http://json-schema.org/draft-07/schema#",
	"title": "ASM Module definition schema",
	"type": "object",
	"definitions": {
		"global_entry": {
			"type": "object",
			"properties": {
				"file": {
					"type": "string",
					"title": "The relative file path",
					"pattern": "^[^<>:\"\\\\\\|\\?\\*]+\\.asm"
				},
				"start": {
					"type": "string",
					"title": "The start of the changes in the ROM as a hex address",
					"pattern": "^(?:0[xX])?[0-9a-fA-F]{1,8}$"
				},
				"end": {
					"type": "string",
					"title": "The end of the changes in the ROM as a hex address (address of the byte after the last change)",
					"pattern": "^(?:0[xX])?[0-9a-fA-F]{1,8}$"
				}
			},
			"required": [ "file", "start", "end" ]
		}
	},
	"properties": {
		"schema_version": {
			"type": "string",
			"enum": [ "1.0.0", "1.1.0", "1.2.0" ]
		},
		"name": {
			"type": "string",
			"title": "The name of the module"
		},
		"uuid": {
			"type": "string",
			"title": "A version 4 Universally Unique Identifier",
			"pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
		},
		"version": {
			"type": "string",
			"title": "The version of the module, following semantic versioning",
			"pattern": "^\\d{1,5}\\.\\d{1,5}\\.\\d{1,5}$"
		},
		"author": {
			"type": "string",
			"title": "Name of the author(s)",
			"default": "Anonymous"
		},
		"description": {
			"type": "string",
			"title": "A description of what the module does"
		},
		"console_compatible": {
			"type": "boolean",
			"title": "Does the module work on console?",
			"default": null
		},
		"dependencies": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"name": {
						"type": "string",
						"title": "The name of the dependency to show if the user doesn't have it installed"
					},
					"uuid": {
						"type": "string",
						"title": "The ID of the dependency",
						"pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
					},
					"version": {
						"oneOf": [{
							"type": "string",
							"title": "The minimum version of the dependency required",
							"pattern": "^\\d{1,5}\\.\\d{1,5}\\.\\d{1,5}$"
						}, {
							"type": "object",
							"properties": {
								"min": {
									"type": "string",
									"title": "The minimum version of the dependency required",
									"pattern": "^\\d{1,5}\\.\\d{1,5}\\.\\d{1,5}$"
								},
								"max": {
									"type": "string",
									"title": "The maximum version of the dependency required",
									"pattern": "^\\d{1,5}\\.\\d{1,5}\\.\\d{1,5}$"
								}
							},
							"required": [ "min", "max" ]
						}]
					}
				},
				"required": [ "name", "uuid", "version" ]
			}
		},
		"area_scoped": {
			"type": "boolean",
			"title": "Set to TRUE to allow the user to select areas that the code should apply to. Generates an $area_mask label.",
			"default": false
		},
		"properties": {
			"type": "array",
			"title": "User configurable properties for the module",
			"items": {
				"type": "object",
				"oneOf": [{
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Integer"
						},
						"hex": {
							"type": "boolean",
							"title": "Set to TRUE to display the value in hexidecimal in the UI",
							"default": false
						},
						"min": {
							"type": "integer"
						},
						"max": {
							"type": "integer"
						},
						"units": {
							"type": "string"
						},
						"default": {
							"type": "integer",
							"title": "The default value of the property"
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"min",
						"max",
						"default"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Float"
						},
						"min": {
							"type": "number"
						},
						"max": {
							"type": "number"
						},
						"units": {
							"type": "string"
						},
						"precision": {
							"type": "integer",
							"title": "How many decimal points of precision to use",
							"minimum": 1,
							"maximum": 4
						},
						"default": {
							"type": "number",
							"title": "The default value of the property"
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"min",
						"max",
						"precision",
						"default"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Angle"
						},
						"min": {
							"type": "number",
							"default": 0,
							"minimum": 0
						},
						"max": {
							"type": "number",
							"default": 359.99,
							"maximum": 359.99
						},
						"default": {
							"type": "number",
							"title": "The default value of the property"
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"default"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Boolean"
						},
						"true_value": {
							"type": "integer",
							"title": "The value of the variable to use if the property is turned on",
							"default": 1
						},
						"false_value": {
							"type": "integer",
							"title": "The value of the variable to use if the property is turned off",
							"default": 0
						},
						"default": {
							"type": "boolean",
							"title": "The default value of the property"
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"default"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "String"
						},
						"encoded": {
							"type": "boolean",
							"title": "If TRUE, stores the string in the encoded format. Otherwise, uses plain ASCII",
							"default": false
						},
						"max_length": {
							"type": "integer",
							"title": "The maximum number of characters allowed",
							"default": 65535,
							"minimum": 0,
							"maximum": 65535
						},
						"multi_line": {
							"type": "boolean",
							"title": "Allow line feed characters",
							"default": false
						},
						"default": {
							"type": "string",
							"title": "The default value of the property",
							"default": ""
						}
					},
					"required": [
						"name",
						"label",
						"type"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Colour"
						},
						"has_alpha": {
							"type": "boolean",
							"title": "Allow alpha selection",
							"default": false
						},
						"format": {
							"type": "string",
							"title": "The colour format to use",
							"enum": [ "RGBA32", "RGBA16", "Components" ]
						},
						"default": {
							"type": "string",
							"title": "The default value of the property",
							"pattern": "^#[a-fA-F0-9]{6}(?:[a-fA-F0-9]{2})?$"
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"format",
						"default"
					]
				}, {
					"properties": {
						"name": {
							"type": "string",
							"title": "The name of the property to show in the UI"
						},
						"label": {
							"type": "string",
							"title": "The variable name to use in the ASM code",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"description": {
							"type": "string",
							"title": "A description of the property to show in the tooltip"
						},
						"type": {
							"type": "string",
							"const": "Pointer"
						},
						"default": {
							"type": "string",
							"title": "The default value for the property as a hexidecimal number",
							"pattern": "^(?:0x)?[_a-zA-Z0-9]{1,8}$"
						},
						"virtual": {
							"type": "boolean",
							"title": "If true, the value is required to lie between 0x80000000 and 0x80FFFFFF",
							"default": false
						}
					},
					"required": [
						"name",
						"label",
						"type",
						"default"
					]
				}]
			}
		},
		"entry_defines": {
			"title": "The file or files containing definitions to be loaded early on in the master ASM file",
			"oneOf": [{
				"type": "string"
			}, {
				"type": "array",
				"items": {
					"type": "string",
					"pattern": "^[^<>:\"\\\\\\|\\?\\*]+\\.asm"
				}
			}]
		},
		"entry_injected": {
			"title": "The file or files to be injected into free space in RAM",
			"oneOf": [{
				"type": "string"
			}, {
				"type": "array",
				"items": {
					"type": "string",
					"pattern": "^[^<>:\"\\\\\\|\\?\\*]+\\.asm"
				}
			}]
		},
		"entry_global": {
			"title": "The file or files that overwrite arbitrary locations in the ROM",
			"oneOf": [{
				"$ref": "#/definitions/global_entry"
			}, {
				"type": "array",
				"items": { "$ref": "#/definitions/global_entry" }
			}]
		},
		"hooks": {
			"type": "array",
			"title": "A list of code hooks used to inject the asm code",
			"items": [{
				"type": "object",
				"oneOf": [{
					"properties": {
						"hook": {
							"type": "string",
							"title": "Where to inject the ASM code",
							"enum": [
								"MarioStepBegin",
								"MarioStepEnd",
								"LevelLoad",
								"AreaLoad",
								"GameStart",
								"RenderHUD",
								"FrameEnd"
							]
						},
						"label": {
							"type": "string",
							"title": "The label of the function to call",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"priority": {
							"type": "string",
							"title": "How early or late this code should be called relative to other code using the same hook",
							"enum": [ "First", "Early", "Middle", "Late", "Last" ],
							"default": "Middle"
						}
					},
					"required": [ "hook", "label" ]
				}, {
					"properties": {
						"hook": {
							"type": "string",
							"title": "Where to inject the ASM code",
							"const": "MarioAction"
						},
						"label": {
							"type": "string",
							"title": "The label of the function to call",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"action": {
							"type": "string",
							"title": "The variable name of the action (must exist in action_ids)"
						}
					},
					"required": [ "hook", "label", "action" ]
				}, {
					"properties": {
						"hook": {
							"type": "string",
							"title": "Where to inject the ASM code",
							"const": "Custom"
						},
						"label": {
							"type": "string",
							"title": "The label of the function to call",
							"pattern": "^[_a-zA-Z0-9]+$"
						},
						"shim_type": {
							"type": "string",
							"title": "The type of shim to create",
							"enum": [ "Head", "Tail", "Word", "Jump" ]
						},
						"priority": {
							"type": "string",
							"title": "How early or late this code should be called relative to other code using the same hook",
							"enum": [ "First", "Early", "Middle", "Late", "Last" ],
							"default": "Middle"
						},
						"shim_address": {
							"type": "string",
							"title": "The hex address of the shim location in the ROM",
							"pattern": "^(?:0[xX])?[0-9a-fA-F]{1,8}$"
						},
						"nice": {
							"type": "integer",
							"title": "The niceness of the shim (see documentation)",
							"minimum": 0,
							"maximum": 4
						}
					},
					"required": [ "hook", "label", "shim_type", "priority", "shim_address", "nice" ]
				}]
			}]
		},
		"action_ids": {
			"type": "array",
			"title": "Custom Mario actions created by the module",
			"items": {
				"type": "object",
				"properties": {
					"var_name": {
						"type": "string",
						"title": "The name of the label holding the action ID",
						"pattern": "^[_a-zA-Z][_a-zA-Z0-9]*$"
					},
					"category": {
						"type": "string",
						"title": "The group that the action belongs to",
						"enum": [
							"Stationary",
							"Moving",
							"Airborne",
							"Submerged",
							"Cutscene",
							"Automatic",
							"Object"
						]
					},
					"flags": {
						"type": "array",
						"title": "The flags affecting the action's behaviour",
						"items": {
							"type": "string",
							"enum": [
								"Stationary",
								"Moving",
								"Air",
								"Intangible",
								"Swimming",
								"MetalWater",
								"ShortHitbox",
								"RidingShell",
								"Invulnerable",
								"Sliding",
								"Diving",
								"OnPole",
								"Hanging",
								"Idle",
								"Attacking",
								"AllowVerticalWind",
								"ControlJumpHeight",
								"AllowFirstPerson",
								"AllowExitCourse",
								"HandsOpen",
								"HeadPivot",
								"ThrowingObject"
							]
						}
					},
					"public": {
						"type": "boolean",
						"title": "If set to FALSE, the action label is module scoped. Otherwise, it's global scoped.",
						"default": true
					}
				},
				"required": [ "var_name", "category" ]
			}
		},
		"camera_modes": {
			"type": "array",
			"title": "Custom camera modes defined by the module",
			"items": {
				"type": "object",
				"properties": {
					"display_name": {
						"type": "string",
						"title": "The name of the camera mode to display in the camera triggers dialog"
					},
					"uuid": {
						"type": "string",
						"title": "A unique UUID for the camera mode",
						"pattern": "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
					},
					"label": {
						"type": "string",
						"title": "The name of the label marking the start of your function that runs the camera code.",
						"pattern": "^[_a-zA-Z0-9]+$"
					},
					"init_label": {
						"type": "string",
						"title": "The name of the label marking the start of your function that initializes the camera with any custom properties.",
						"pattern": "^[_a-zA-Z0-9]+$"
					},
					"properties": {
						"type": "array",
						"title": "User configurable properties for the module that are passed to the init function",
						"items": {
							"type": "object",
							"oneOf": [{
								"properties": {
									"name": {
										"type": "string",
										"title": "The name of the property to show in the UI"
									},
									"label": {
										"type": "string",
										"title": "The variable name to use in the ASM code",
										"pattern": "^[_a-zA-Z0-9]+$"
									},
									"description": {
										"type": "string",
										"title": "A description of the property to show in the tooltip"
									},
									"type": {
										"type": "string",
										"const": "Integer"
									},
									"hex": {
										"type": "boolean",
										"title": "Set to TRUE to display the value in hexidecimal in the UI",
										"default": false
									},
									"min": {
										"type": "integer"
									},
									"max": {
										"type": "integer"
									},
									"units": {
										"type": "string"
									},
									"default": {
										"type": "integer",
										"title": "The default value of the property"
									}
								},
								"required": [
									"name",
									"label",
									"type",
									"min",
									"max",
									"default"
								]
							}, {
								"properties": {
									"name": {
										"type": "string",
										"title": "The name of the property to show in the UI"
									},
									"label": {
										"type": "string",
										"title": "The variable name to use in the ASM code",
										"pattern": "^[_a-zA-Z0-9]+$"
									},
									"description": {
										"type": "string",
										"title": "A description of the property to show in the tooltip"
									},
									"type": {
										"type": "string",
										"const": "Float"
									},
									"min": {
										"type": "number"
									},
									"max": {
										"type": "number"
									},
									"units": {
										"type": "string"
									},
									"precision": {
										"type": "integer",
										"title": "How many decimal points of precision to use",
										"minimum": 1,
										"maximum": 4
									},
									"default": {
										"type": "number",
										"title": "The default value of the property"
									}
								},
								"required": [
									"name",
									"label",
									"type",
									"min",
									"max",
									"precision",
									"default"
								]
							}, {
								"properties": {
									"name": {
										"type": "string",
										"title": "The name of the property to show in the UI"
									},
									"label": {
										"type": "string",
										"title": "The variable name to use in the ASM code",
										"pattern": "^[_a-zA-Z0-9]+$"
									},
									"description": {
										"type": "string",
										"title": "A description of the property to show in the tooltip"
									},
									"type": {
										"type": "string",
										"const": "Angle"
									},
									"min": {
										"type": "number",
										"default": 0,
										"minimum": 0
									},
									"max": {
										"type": "number",
										"default": 359.99,
										"maximum": 359.99
									},
									"default": {
										"type": "number",
										"title": "The default value of the property"
									}
								},
								"required": [
									"name",
									"label",
									"type",
									"default"
								]
							}, {
								"properties": {
									"name": {
										"type": "string",
										"title": "The name of the property to show in the UI"
									},
									"label": {
										"type": "string",
										"title": "The variable name to use in the ASM code",
										"pattern": "^[_a-zA-Z0-9]+$"
									},
									"description": {
										"type": "string",
										"title": "A description of the property to show in the tooltip"
									},
									"type": {
										"type": "string",
										"const": "Boolean"
									},
									"true_value": {
										"type": "integer",
										"title": "The value of the variable to use if the property is turned on",
										"default": 1
									},
									"false_value": {
										"type": "integer",
										"title": "The value of the variable to use if the property is turned off",
										"default": 0
									},
									"default": {
										"type": "boolean",
										"title": "The default value of the property"
									}
								},
								"required": [
									"name",
									"label",
									"type",
									"default"
								]
							}, {
								"properties": {
									"name": {
										"type": "string",
										"title": "The name of the property to show in the UI"
									},
									"label": {
										"type": "string",
										"title": "The variable name to use in the ASM code",
										"pattern": "^[_a-zA-Z0-9]+$"
									},
									"description": {
										"type": "string",
										"title": "A description of the property to show in the tooltip"
									},
									"type": {
										"type": "string",
										"const": "Pointer"
									},
									"default": {
										"type": "string",
										"title": "The default value for the property as a hexidecimal number",
										"pattern": "^(?:0x)?[_a-zA-Z0-9]{1,8}$"
									},
									"virtual": {
										"type": "boolean",
										"title": "If true, the value is required to lie between 0x80000000 and 0x80FFFFFF",
										"default": false
									}
								},
								"required": [
									"name",
									"label",
									"type",
									"default"
								]
							}]
						}
					}
				},
				"required": [ "display_name", "uuid", "label" ]
			}
		}
	},
	"required": [
		"schema_version",
		"name",
		"uuid",
		"version",
		"description"
	]
}
