<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Bowser's Blueprints ASM Module Specification</title>
		<style>
			body {
				font-family: sans-serif;
				max-width: 1200px;
				margin: auto;
				padding-bottom: 4rem;
			}
			
			h2 {
				text-decoration: underline;
			}
			
			pre {
				color: black;
				font-family: monospace;
			}
			
			pre > a {
				color: #26802f;
				text-decoration: none;
			}
			
			pre > a:hover {
				text-decoration: underline dotted;
			}
			
			pre > span {
				color: #707070;
			}
			
			div.props {
				color: black;
			}
			
			div.props > b {
				font-family: monospace;
				font-size: 1.25em;
			}
			
			div.props > div {
				margin-left: 4ch;
			}
			
			.asm {
				background-color: #eeeeee;
				padding: 0.5em;
			}
			
			.asm > span.label {
				color: #b08000;
			}
			
			.asm > span.numeral {
				color: #ff0000;
			}
			
			.asm > span.comment {
				color: #006e28;
			}
			
			.asm > span.register {
				color: #7f0055;
			}
			
			.asm > b.directive {
				color: #004000;
			}
		</style>
	</head>
	<body>
	
	<h1>ASM Module Developer's Guide</h1>
	<p>This document describes how to write ASM modules for Bowser's Blueprints. An ASM module is composed to two parts: a JSON
	file named <code>module.json</code> that provides information about the module, and a collection of ASM files containing the
	actual code to be assembled by Armips after being preprocessed by Bowser's Blueprints to provide additonal features.</p>
	
	<p>For examples of ASM modules, see the <a href="https://gitlab.com/blueprint64/blueprint-64/-/tree/master/asm%2Fmodules">
	asm/modules directory</a> of the Bowser's Blueprints source code.</p>
	
	<h2>Armips Extensions</h2>
	<p>ASM Modules are preprocessed by Bowser's Blueprints before being assembled by Armips. The preprocessor
	adds the following extensions to the Armips syntax:<p>
	
	<h3>Module-Scoped Identifiers</h3>
	<p>Armips allows labels and <b>equ</b> name identifiers to have a global, static (file), or local scope. Bowser's
	Blueprints adds a fourth scope that covers the current module. You can make an identifier have module scope by
	prefixing it with a <code>$</code>. The preprocessor will convert this to a global label with a unique prefix
	for the current module.
	<br><br>
	Unless you specifically want to make a label accessible to user code, you should prefer to use module scoped
	idenitifiers over globally scoped ones in order to avoid conflicts with user code or other modules.</p>
	<pre class="asm">
<span class="label">myGlobalLabel</span> <b>equ</b> <span class="numeral">1</span> <span class="comment">; global identifier usable anywhere</span>
<span class="label">@myStaticLabel</span> <b>equ</b> <span class="numeral">2</span> <span class="comment">; static identifier usable only in the current file</span>
<span class="label">@@myLocalLabel</span> <b>equ</b> <span class="numeral">3</span> <span class="comment">; local identifier usable only until the next non-local label</span>
<span class="label">$myModuleLabel</span> <b>equ</b> <span class="numeral">4</span> <span class="comment">; module-scoped identifier usable only in the current ASM Module (BBP Extension)</span></pre>
	
	<h3>Module Directives</h3>
	<p>Bowser's Blueprints adds additional special commands that can be invoked with the <code>#</code> character.
	Currently, there are two supported commands:</p>
	
	<b>#continue &lt;shimLabel&gt;</b>
	<p>This command inserts a jump (<code>J</code>) instruction to the memory address of the function that should next be called
	to continue from the shim identified by <code>&lt;shimLabel&gt;</code>. If the shim is a tail shim, and this is the last shim
	in the chain, it instead inserts a <code>JR RA</code> instruction. This is only valid when <code>&lt;shimLabel&gt;</code> is
	the label of a custom hook. You should always use this to continue from a shim instead of hard-coding the address of the
	original jump the shim replaced. Using this command allows multiple shims from different modules to be chained together
	without conflicts. Do not include the <code>$</code> prefix in the label name.</p>
	
	<pre class="asm">
<span class="label">$my_shim</span>:
<b>ADDIU</b> <span class="register">SP</span>, <span class="register">SP</span>, <span class="numeral">0xFFE8</span>
<b>SW</b> <span class="register">RA</span>, <span class="numeral">0x14</span> (<span class="register">SP</span>)

<span class="comment">; do stuff</span>
...

<b>LW</b> <span class="register">RA</span>, <span class="numeral">0x14</span> (<span class="register">SP</span>)
<b class="directive">#continue</b> <span class="label">my_shim</span>
<b>ADDIU</b> <span class="register">SP</span>, <span class="register">SP</span>, <span class="numeral">0x14</span></pre>

	<br>
	<b>#await &lt;shimLabel&gt;</b>
	<p>This command inserts a function call (<code>JAL</code>) to the next shim in the chain (or to the original function if the
	shim is the last one in the chain). You can use this to run code after the original function and all other shims complete. If
	the shim is a tail shim, a <code>NOP</code> is instead inserted in the last shim in the chain. This is only valid when
	<code>&lt;shimLabel&gt;</code> is the label of a custom hook. Do not include the <code>$</code> prefix in the label name.</p>
	
		<pre class="asm">
<span class="label">$my_shim</span>:
<b>ADDIU</b> <span class="register">SP</span>, <span class="register">SP</span>, <span class="numeral">0xFFE8</span>
<b>SW</b> <span class="register">RA</span>, <span class="numeral">0x14</span> (<span class="register">SP</span>)

<span class="comment">; do stuff</span>
...

<b class="directive">#await</b> <span class="label">my_shim</span>
<b>NOP</b>

<span class="comment">; do more stuff</span>
...

<b>LW</b> <span class="register">RA</span>, <span class="numeral">0x14</span> (<span class="register">SP</span>)
<b>JR</b> <span class="register">RA</span>
<b>ADDIU</b> <span class="register">SP</span>, <span class="register">SP</span>, <span class="numeral">0x14</span></pre>

	<h2>Module JSON Specification</h2>
	<p>Every ASM module must contain a JSON file named <i>module.json</i>. This format of this file is described below. You can
	click on green text in type	definitions to be taken to the definition of the referenced type. A schema document is also
	provided for you to	validate your tweak definitions against (asm-module-schema.json).</p>
	
	<pre>
{
    "schema_version": "1.2.0",
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "version": <a href="#version">(version)</a>,
    "author": <span>[Optional] (string)</span>,
    "description": <span>(string)</span>,
    "console_compatible": <span>[Optional] (boolean|null)</span>,
    "dependencies": <span>[Optional]</span> [
        <a href="#Dependency">&lt;Dependency&gt;</a>,
        ...
    ],
    "area_scoped": <span>[Optional] (boolean)</span>,
    "properties": <span>[Optional]</span> [
        <a href="#Property">&lt;Property&gt;</a>,
        ...
    ],
    "entry_defines": <span>[Optional] (string </span><b>OR</b><span> Array[string])</span>,
    "entry_injected": <span>[Optional] (string </span><b>OR</b><span> Array[string])</span>,
    "entry_global": <span>[Optional] </span><a href="#AbsoluteAsmFile">&lt;AbsoluteAsmFile&gt;</a> <b>OR</b> <span>Array[</span><a href="#AbsoluteAsmFile">&lt;AbsoluteAsmFile&gt;</a><span>]</span>,
    "hooks": <span>[Optional]</span> [
        <a href="#Hook">&lt;Hook&gt;</a>,
        ...
    ],,
    "action_ids": <span>[Optional]</span> [
        <a href="#MarioAction">&lt;MarioAction&gt;</a>,
        ...
    ],
    "camera_modes": <span>[Optional]</span> [
        <a href="#CustomCamera">&lt;CustomCamera&gt;</a>,
        ...
    ]
}
	</pre>
	
	<div class="props">
		<b>shema_version</b> - string, requried
		<div>The version of the ASM Module JSON Schema being used. Up-to-date modules should use <code>"1.2.0"</code>.</div>
		<b>name</b> - string, required
		<div>The name of the module</div>
		<b>uuid</b> - string (uuid), required
		<div>A unique id (see below) for the module. This id should <b>not</b> change between versions.</div>
		<b>version</b> - string (version), required
		<div>A version string for the current revision of the module.</div>
		<b>author</b> - string, optional
		<div>The name of the author or authors of the module. Defaults to <code>"Anonymous"</code>.</code></div>
		<b>description</b> - string, required
		<div>A description of what the module does</div>
		<b>console_compatible</b> - boolean | null, optional
		<div>Whether or not the module works on console. If you don't know, put <code>null</code> or leave out this property. See <a href="#console_compat">Console Compatibility</a></div>
		<b>dependencies</b> - Array[Dependency], required
		<div>An array of modules that must be installed for this module to function</div>
		<b>area_scoped</b> - boolean, optional
		<div>If <code>true</code>, the module allows the user to choose which levels or areas the module should be applied to. An
		<code style="color: #b08000;">$area_mask</code> label will be autogenerated pointing to a region of memory containing 40
		bytes where the <i>n</i>th lowest bit of the <i>m</i>th byte is 1 iff the user has indicated that area <i>n</i> in the
		level with id <i>m</i> should have the module applied. Defaults to <code>false</code></div>
		<b>properties</b> - Array[Property], optional
		<div>A list of parameters the user can choose to tweak the module's behaviour</div>
		<b>entry_defines</b> - string | Array[String], optional
		<div>An ASM file or list of ASM files to import early on. These asm file can define labels, equ identifiers, and macros,
		but cannot contain data or instructions to write to the ROM. If the files are in a subdirectory, use a POSIX (*nix/Mac)
		style directory separator (forward slash, not backslash).</div>
		<b>entry_injected</b> - string | Array[String], optional
		<div>An ASM file or list of ASM files to import into free space in the ROM. This is where most of your code will go. You
		should only specify the top level files (i.e., do not include files there that are .include'd by other files you have
		already specified otherwise they will be imported twice)</div>
		<b>entry_global</b> - AbsoluteAsmFile | Array[AbsoluteAsmFile], optional
		<div>An ASM file or list of ASM files to write to a specific address in the ROM.</div>
		<b>hooks</b> - Array[Hook], optional
		<div>An array of code hooks that will call your code</div>
		<b>actions_ids</b> - Array[MarioAction], optional
		<div>An array of custom mario action IDs to generate</div>
	</div>
	
	<h2>Some Type Definitions</h2>
	<p>This section describes the JSON spec for some objects and special string types used by various modules definitions.</p>
	
	<h3 id="uuid">uuid</h3>
	<p>A <i>uuid</i> is a <b>string</b> representing a version 4 <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier">Universally Unique_Identifier</a>.
	You can generate a uuid on Linux easily using the command <code>uuidgen -r</code>. On Windows,
	the equivalent command is <code>powershell -Command "[Guid]::NewGuid().toString()"</code>.</p>
	
	<h3 id="version">version</h3>
	<p>A version string of the format <code>[major].[minor].[patch]</code> where <i>major</i>, <i>minor</i>, and <i>patch</i> are
	numbers between 0 and 65535. The version should follow the semantic versioning convention where incrementing the major version
	indicates a breaking change, incrementing the minor version indicates the addition of new features, and incrementing the patch
	version indicates a bugfix.</p>
	
	<h3 id="Dependency">Dependency</h3>
	<p>A <i>Dependency</i> specifies another module that must be added for this module to work.</p>
	<pre>
{
    "uuid": <a href="#uuid">(uuid)</a>,
    "name": <span>(string)</span>,
    "version": <a href="#version">(version)</a> <b>OR</b> {
        "min": <a href="#version">(version)</a>,
        "max": <span>[Optional]</span> <a href="#version">(version)</a>
    }
}
	</pre>
	<div class="props">
		<b>uuid</b> - uuid, required
		<div>The unique identifier of the required module</div>
		<b>name</b> - string, required
		<div>The name of the required module. If the user has the module installed, it will be displayed using the name given in
		that module's <code>module.json</code> file. If it is not installed, the name provided here will be shown instead.</div>
		<b>version</b> - version | object, required
		<div>This property can be an object or a string. Providing a string is equivalent to providing an object with only the
		<i>min</i> property set:</div>
		<br>
		<div class="props">
			<b>min</b> - version, required
			<div>The minimum allowed version of the required module</div>
			<b>max</b> - version, optional
			<div>The maximum allowed version of the required module. If not specified, default to the largest version number that
			has the same major version as the given minimum version</div>
		</div>
	</div>
	
	<h3 id="AbsoluteAsmFile">AbsoluteAsmFile</h3>
	<p>An <i>AbsoluteAsmFile</i> defines a region of the ROM that will be overwritten with custom ASM code.</p>
	<pre>
{
    "file": <span>(string)</span>,
    "start": <span>(string)</span>,
    "end": <span>(string)</span>
}
	</pre>
	
	<div class="props">
		<b>file</b> - string, required
		<div>The name of the file containing the asm code to write</div>
		<b>start</b> - string, required
		<div>The position in the ROM to start writing data, formatted in hexidecimal</div>
		<b>end</b> - string, required
		<div>The position in the ROM to stop writing data, formatted in hexidecimal (the position of the first byte after your code)</div>
	</div>
	
	<h3 id="Hook">Hook</h3>
	<p>A <i>Hook</i> specifies an injection point where your custom code will be called from.</p>
	<pre>
{
    "hook": <span>(string)</span>,
    "label": <span>(string)</span>,
    "priority": <span>[Optional]</span><sup>1</sup> <span>(string)</span>,

    <span style="color: #006e28;">// The following property only applies to MarioAction hooks: (see below)</span>
    "action": <span>(string)</span>,
    
    <span style="color: #006e28;">// The following properties only apply to Custom hooks: (see below)</span>
    "shim_type": <span>(string)</span>,
    "shim_address": <span>(string)</span>,
    "nice": <span>(number)</span>
}
	</pre>
	<sup><sup>1</sup> <i>Required for <code>Custom</code> hooks</i></sup>
	<br><br>
	
	<div class="props">
		<b>hook</b> - string (enum), required
		<div>The injection point for your code. Valid values are:<br>
			<code>"MarioStepBegin"</code> - Runs your code just before each iteration of Mario's behaviour script<br>
			<code>"MarioStepEnd"</code> - Runs your code just after each iteration of Mario's behaviour script<br>
			<code>"LevelLoad"</code> - Runs your code when a new level loads<br>
			<code>"AreaLoad"</code> - Runs your code when an area loads<br>
			<code>"GameStart"</code> - Runs your code once when the game starts<br>
			<code>"RenderHUD"</code> - Runs your code just after the HUD renders<br>
			<code>"FrameEnd"</code> - Runs your code at the end of each frame<br>
			<code>"MarioAction"</code> - Runs your code to execute a custom Mario action<br>
			<code>"Custom"</code> - Define a custom shim to run your code before or after an arbitrary function<br>
		</div>
		<b>label</b> - string, required
		<div>The name of the label marking the start of your function to execute. Do not include the $ character.</div>
		<b>priority</b> - string (enum), optional (required for <code>"Custom"</code> type hooks)
		<div>When to run your code relative to other code using the same hook or shim. Valid values are:<br>
			<code>"First"</code>, <code>"Early"</code>, <code>"Middle"</code>, <code>"Late"</code>, <code>"Last"</code><br>
			Defaults to <code>"Middle"</code>. Multiple hooks may have the same priority, but the order that hooks with the
			same priority will run in is not defined.
		</div>
		<b>action</b> - string, required (<code>"Action"</code> type hooks only)
		<div>The custom Mario action that this code handles. Must match the <code>var_name</code> of a
		<pre style="display: inline;"><a href="#MarioAction">MarioAction</a></pre></div>
		<b>shim_type</b> - string (enum), required (<code>"Custom"</code> type hooks only)
		<div>The type of shim to create. Valid values are:<br>
			<code>"Head"</code> - Use this if you are overwriting a <code>JAL</code> instruction<br>
			<code>"Tail"</code> - Use this if you are overwriting a <code>JR RA</code> instruction<br>
			<code>"Jump"</code> - Use this if you are overwriting a <code>J</code> instruction<br>
			<code>"Word"</code> - Use this if you are overwriting a function pointer in a jump table<br>
			If you are replacing another type of instruction, you will need to manually create your shim using an
			<pre style="display: inline;"><a href="#AbsoluteAsmFile">AbsoluteAsmFile</a></pre> in <code>entry_global</code>
			rather than using a hook.
		</div>
		<b>shim_address</b> - string, required (<code>"Custom"</code> type hooks only)
		<div>The ROM address of the instruction to overwrite with the jump to your code, formatted in hexidecimal.</div>
		<b>nice</b> - number, required (<code>"Custom"</code> type hooks only)
		<div>The niceness of a shim is an indication of how likely it is to interfere with other shims on the same
		address. The higher the value, the less likely it is to interfere with other shims on the same function.
		The niceness is an integer from 0 to 4 inclusive:<br>
			<code>4</code> - Does not conflict with any shim except for shims with nicess 0. If your shim always continues and
			does not care about its arguments, you should use this. Even if you do use the function arguments, this may still
			be appropriate if it should work fine with any well-behaved shim.<br>
			<code>3</code> - Conflicts with shims with niceness 0 or 1. This might be appropriate if your shim can end without
			continuing to other shims in certain cases or if you alter the function arguments or return value, but your code
			will still behave correctly if another shim also alters the arguments or return value.<br>
			<code>2</code> - Conflicts with all shims having the same or lower niceness. This might be approriate if your code
			replaces a function argument or return value with a value independant of its original value.<br>
			<code>1</code> - Conflicts with all shims not having niceness 4. This is appropriate if your shim significantly
			alters the behaviour of the function in such as way that values passed to other shims may not be meaningful.<br>
			<code>0</code> - Exclusive. Conflicts with any module that shims the same function. This should be used sparingly.
		</div>
	</div>
	
	<h3 id="MarioAction">MarioAction</h3>
	<p>A <i>MarioAction</i> defines how to generate an action ID for a custom Mario action.</p>
	<pre>
{
    "var_name": <span>(string)</span>,
    "category": <span>(string)</span>,
    "flags": <span>[Optional]</span> [
        <span>(string)</span>,
        ...
    ],
    "public": <span>[Optional] (boolean)</span>
}
	</pre>
	
	<div class="props">
		<b>var_name</b> - string, required
		<div>The name of the identifier to generate containing the action ID. Do not include the $ prefix.</div>
		<b>category</b> - string (enum), required
		<div>The action category. Valid value are:<br>
			<code>"Stationary"</code>,
			<code>"Moving"</code>,
			<code>"Airborne"</code>,
			<code>"Submerged"</code>,
			<code>"Cutscene"</code>,
			<code>"Automatic"</code>,
			<code>"Object"</code>
		</div>
		<b>flags</b> - Array[string (enum)], optional
		<div>An array of action flags that alter the action's behaviour. Valid values are:<br>
			<code>"Stationary"</code>,<br>
			<code>"Moving"</code>,<br>
			<code>"Air"</code>,<br>
			<code>"Intangible"</code>,<br>
			<code>"Swimming"</code>,<br>
			<code>"MetalWater"</code>,<br>
			<code>"ShortHitbox"</code>,<br>
			<code>"RidingShell"</code>,<br>
			<code>"Invulnerable"</code>,<br>
			<code>"Sliding"</code>,<br>
			<code>"Diving"</code>,<br>
			<code>"OnPole"</code>,<br>
			<code>"Hanging"</code>,<br>
			<code>"Idle"</code>,<br>
			<code>"Attacking"</code>,<br>
			<code>"AllowVerticalWind"</code>,<br>
			<code>"ControlJumpHeight"</code>,<br>
			<code>"AllowFirstPerson"</code>,<br>
			<code>"AllowExitCourse"</code>,<br>
			<code>"HandsOpen"</code>,<br>
			<code>"HeadPivot"</code>,<br>
			<code>"ThrowingObject"</code>
		</div>
		<b>public</b> - boolean, optional
		<div>If <code>true</code>, generated a global label for the action so that user code and other modules may use it.
		A module scoped label with the same name is still generated. Defaults to <code>false</code>.</div>
	</div>
	
	<h3 id="CustomCamera">CustomCamera</h3>
	<p>A <i>CustomCamera</i> defines a custom camera mode that can be activated with a camera trigger.</p>
	<pre>
{
    "display_name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "label": <span>(string)</span>,
    "init_label": <span>[Optional] (string)</span>
    "properties": <span>[Optional]</span> [
        <a href="#CameraProperty">&lt;CameraProperty&gt;</a>,
        ...
    ]
}
	</pre>
	
	<div class="props">
		<b>display_name</b> - string, required
		<div>The name of the camera mode to display in the camera triggers dialog.</div>
		<b>uuid</b> - string (uuid), required
		<div>A unique id for the camera mode. This id should <b>not</b> change between versions.</div>
		<b>label</b> - string (enum), required
		<div>The name of the label marking the start of your function that runs the camera code.</div>
		<b>init_label</b> - string (enum), required only if the custom camera has properties
		<div>The name of the label marking the start of the function to call when switching to your camera code</div>
		<b>properties</b> - Array[CameraProperty], optional
		<div>Any properties that can be set on the camera trigger and passed into the init function. The init function will be called with <code>A0</code> holding a pointer to the camera and <code>A1</code> holding a pointer to an array of parameters (4 bytes per parameter).</div>
	</div>
	
	<h3 id="Property">Property</h3>
	<p>A <i>Property</i> is a configurable parameter than a user can use to tweak the behaviour of your module. A property is any
	of the following types:<br>
		<pre style="display: inline;"><a href="#IntegerProperty">IntegerProperty</a></pre>,
		<pre style="display: inline;"><a href="#FloatProperty">FloatProperty</a></pre>,
		<pre style="display: inline;"><a href="#AngleProperty">AngleProperty</a></pre>,
		<pre style="display: inline;"><a href="#BooleanProperty">BooleanProperty</a></pre>,
		<pre style="display: inline;"><a href="#StringProperty">StringProperty</a></pre>,
		<pre style="display: inline;"><a href="#ColourProperty">ColourProperty</a></pre>,
		<pre style="display: inline;"><a href="#PointerProperty">PointerProperty</a></pre>
	</p>
	
	<h3 id="CameraProperty">CameraProperty</h3>
	<p>A <i>CameraProperty</i> is a configurable parameter than a user can pass to a camera mode in a camera trigger. It can be any of the following types:<br>
		<pre style="display: inline;"><a href="#IntegerProperty">IntegerProperty</a></pre>,
		<pre style="display: inline;"><a href="#FloatProperty">FloatProperty</a></pre>,
		<pre style="display: inline;"><a href="#AngleProperty">AngleProperty</a></pre>,
		<pre style="display: inline;"><a href="#BooleanProperty">BooleanProperty</a></pre>,
		<pre style="display: inline;"><a href="#PointerProperty">PointerProperty</a></pre>
	</p>
	
	<h3 id="IntegerProperty">IntegerProperty</h3>
	<p>A signed integer value. Generates an identifier that expands to this value. For example:<br>
	<code>$MY_PROPERTY equ 6447728</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Integer",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(number)</span>,
    "min": <span>[Optional] (number)</span>,
    "max": <span>[Optional] (number)</span>,
    "units": <span>[Optional] (string)</span>,
    "hex": <span>[Optional] (boolean)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - number, required
		<div>The default value for the property</div>
		<b>min</b> - number, optional
		<div>The minimum allowed value. Default is -2147483648.</div>
		<b>max</b> - number, optional
		<div>The maximum allowed value. Default is 2147483647.</div>
		<b>units</b> - string, optional
		<div>The suffix to show in the number spinner</div>
		<b>hex</b> - boolean, optional
		<div>If <code>true</code> the number spinner in the UI displays the value in hexidecimal.</div>
	</div>
	
	<h3 id="PointerProperty">PointerProperty</h3>
	<p>An unsigned 32-bit integer value. Generates an identifier that expands to this value. For example:<br>
	<code>$MY_PROPERTY equ 0xDEADBEEF</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Pointer",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(string)</span>,
    "virtual": <span>[Optional] (boolean)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - string, required
		<div>The default value for the property as a hexidecimal number</div>
		<b>virtual</b> - boolean, optional
		<div>If <code>true</code>, the value is required to lie between <code>0x80000000</code> and <code>0x80FFFFFF</code>.</div>
	</div>
	
	<h3 id="FloatProperty">FloatProperty</h3>
	<p>A floating point value. Generates an identifier that expands to this value. For example:<br>
	<code>$MY_PROPERTY equ -6.9375</code><br>
	If you need to store the value as bytes in floating point format, you can use the Armips <code>float()</code> macro.</p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Float",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(number)</span>,
    "min": <span>[Optional] (number)</span>,
    "max": <span>[Optional] (number)</span>,
    "units": <span>[Optional] (string)</span>,
    "precision": <span>[Optional] (number)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - number, required
		<div>The default value for the property</div>
		<b>min</b> - number, optional
		<div>The minimum allowed value.</div>
		<b>max</b> - number, optional
		<div>The maximum allowed value.</div>
		<b>units</b> - string, optional
		<div>The suffix to show in the number spinner</div>
		<b>precision</b> - number, optional
		<div>The number of decimal places to show. Must be between 1 and 4. Default is 2.</div>
	</div>
	
	<h3 id="AngleProperty">AngleProperty</h3>
	<p>A value representing an angle. It is entered in the UI as a floating point value giving the number of degrees, but
	is written to ASM code in the 16-bit integer format the game usually stores angles in.<br>
	<code>$MY_PROPERTY equ 7646 ; 7646 = 0x1DDE = 42 degrees</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Angle",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(number)</span>,
    "min": <span>[Optional] (number)</span>,
    "max": <span>[Optional] (number)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - number, required
		<div>The default value for the property</div>
		<b>min</b> - number, optional
		<div>The minimum allowed value. Default is 0.</div>
		<b>max</b> - number, optional
		<div>The maximum allowed value. Default is 359.99</div>
	</div>
	
	<h3 id="BooleanProperty">BooleanProperty</h3>
	<p>A value representing a boolean value. Generates an identifier that expands to either <code>true_value</code> or
	<code>false_value</code> (<code>1</code> and <code>0</code> respectively by default).<br>
	<code>$MY_PROPERTY equ 1</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Boolean",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(boolean)</span>,
    "true_value": <span>[Optional] (number)</span>,
    "false_value": <span>[Optional] (number)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - boolean, required
		<div>The default value for the property</div>
		<b>true_value</b> - number, optional
		<div>An integer value to assign to the identifier when checked. Default is 1.</div>
		<b>false_value</b> - number, optional
		<div>An integer value to assign to the identifier when unchecked. Default is 0.</div>
	</div>
	
	<h3 id="StringProperty">StringProperty</h3>
	<p>Generates a label pointing to a null-terminated string of text.<br>
	<code>$MY_PROPERTY:<br>.asciiz "Hello, world"</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "String",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>[Optional] (string)</span>,
    "encoded": <span>[Optional] (boolean)</span>,
    "max_length": <span>[Optional] (number)</span>,
    "multi_line": <span>[Optional] (boolean)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - boolean, optional
		<div>The default value for the property. Defaults to the empty string.</div>
		<b>encoded</b> - boolean, optional
		<div>If <code>true</code>, the text is encoded using the text encoding table used for menu text and dialog.
		If <code>false</code>, the text is encoded in ASCII format. Defaults to <code>false</code> (ASCII).</div>
		<b>max_length</b> - number, optional
		<div>The maximum length of the string. Defaults to 65535.</div>
		<b>multi_line</b> - boolean, optional
		<div>If <code>true</code>, allows the text to contain newline characters. Defaults to <code>false</code>.</div>
	</div>
	
	<h3 id="ColourProperty">ColourProperty</h3>
	<p>Generates an identifier or four that exapnds to the colour or colour channel value<br>
	<code>$MY_PROPERTY equ 0xFF8000FF</code>
	<br>or</br>
	<code>$MY_PROPERTY_red equ 0xFF<br>
	$MY_PROPERTY_green equ 0x80<br>
	$MY_PROPERTY_blue equ 0x00<br>
	$MY_PROPERTY_alpha equ 0xFF</code></p>
	<pre>
{
    "name": <span>(string)</span>,
    "type": "Colour",
    "label": <span>(string)</span>,
    "description": <span>[Optional] (string)</span>,
    "default": <span>(string)</span>,
    "format": <span>(string)</span>,
    "has_alpha": <span>[Optional] (boolean)</span>
}
	</pre>
	<div class="props">
		<b>name</b> - string, required
		<div>The name of the property to show in Bowser's Blueprints</div>
		<b>label</b> - string, required
		<div>The name of the identifier to generate in ASM code. Do not include the <code>$</code> prefix.</div>
		<b>description</b> - string, optional
		<div>Descriptive text to show in the tooltip</div>
		<b>default</b> - string, required
		<div>The default colour, formatted as a hash (<code>#</code>) followed by either 6 or 8 hexidecimal digits
		(#RRGGBB[AA])</div>
		<b>format</b> - string, required
		<div>How the colour should be repesented in asm. Valid values are:<br>
			<code>"RGBA32"</code> - A 4-byte integer in 0xRRGGBBAA format<br>
			<code>"RGBA16"</code> - A 2-byte integer in RGBA5551 format<br>
			<code>"Components"</code> - generates four identifiers, with each one giving the value of each component as a byte.
			Identifier names are <code>label</code> with a <code>_red</code>, <code>_green</code>, <code>_blue</code>, or
			<code>_alpha</code> suffix.
		</div>
		<b>has_alpha</b> - boolean, optional
		<div>If <code>true</code>, allows the user to select the opacity. Otherwise assumes the colour to be fully opaque.
		Defaults to <code>false</code>.</div>
	</div>
	
	<h2 id="console_compat">Console Compatibility</h2>
	<p>When designing an ASM module for console compatibility, keep these rules in mind:</p>
	<ul>
		<li>After using an <code>MFLO</code> or <code>MFHI</code> instruction, the next <b>two</b> instructions must not set the LO or HI registers. (This means no integer multiplication or division instructions allowed)</li>
		<li>After copying a value to another coprocessor with <code>MTC1</code> or <code>MFC1</code>, you must not use the destination register in the next instruction.</li>
		<li>Fast3d instructions, as well as vertices and textures, should all be doubleword aligned: this means aligned to an 8-byte boundary. (Use <code>.align 8</code>)</li>
		<li>If you perform a DMA copy, the ROM address must be halfword aligned (2-byte boundary), and the RAM address must be doubleword aligned (8-byte boundary).</li>
		<li>Do not use odd-numbered floating point registers. Alternatively, you can set the FR bit of the status register to enable direct access to the odd-numbered floating point registers:<br>
		<pre class="asm">
<b>MFC0</b> <span class="register">T0</span>, <span class="register">SR</span>
<b>LUI</b> <span class="register">AT</span>, <span class="numeral">0x0400</span>
<b>OR</b> <span class="register">T0</span>, <span class="register">T0</span>, <span class="register">AT</span>
<b>MTC0</b> <span class="register">T0</span>, <span class="register">SR</span></pre>
		</li>
	</ul>
	
	</body>
</html>
