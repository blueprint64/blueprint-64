This folder contains fallback icons for operating systems that do not support
themes, or for themes missing the icons used by Bowser's Blueprints. These
fallback icons are all taken from the KDE Breeze theme, the license for which
can be found in the LICENSE file in this directory.
