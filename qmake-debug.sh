#!/bin/sh
if [ -n "$(which qtchooser)" ]; then
	qmake -qt=qt5 app.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug
else
	qmake app.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug
fi
