#!/bin/sh
if [ -n "$(which qtchooser)" ]; then
	qmake -qt=qt5 app.pro -spec linux-g++
else
	qmake app.pro -spec linux-g++
fi
