Name:		blueprint64
Version:	0.20.0
Release:	0%{?dist}
Summary:	A general-purpose ROMhacking tool for creating Super Mario 64 ROMhacks.
License:	GPLv2
Group:		devel
URL:		https://blueprint64.ca/
Source:		https://gitlab.com/blueprint64/blueprint-64/-/archive/Alpha_10.0/blueprint-64-Alpha_10.0.tar.bz2
Vendor:		Matt Pharoah

BuildRequires: (gcc >= 9 or gcc9 or gcc-toolset-9-gcc)
BuildRequires: (gcc-c++ >= 9 or gcc9-c++ or gcc-toolset-9-gcc-c++)
BuildRequires: (libqt5-qtbase-common-devel or qt5-qtbase-devel)
BuildRequires: (libqt5-qtdeclarative-devel or qt5-qtdeclarative-devel)
BuildRequires: zlib-devel

Requires: glibc
Requires: (libstdc++6 or libstdc++)
Requires: (libgcc_s1 or libgcc)
Requires: ((libQt5Core5 and libQt5Widgets5 and libQt5Network5) or (qt5-qtbase and qt5-qtbase-common and qt5-qtdeclarative and qt5-qtsvg))
Requires: (libQt5Gui5 or qt5-qtbase-gui)
Requires: tar
Requires: which
Requires: (libz1 or zlib)

%define debug_package %{nil}

%description
A general-purpose ROMhacking tool for creating Super Mario 64 ROMhacks.

%prep
%setup -q

%build
if [ `g++ -dumpversion` -lt 9 ]; then
	if [ `which g++-9` ]; then
		qmake-qt5 app.pro -spec linux-g++ QMAKE_CXX="g++-9"
	else
		echo "WARNING: g++ is out-of-date. You may need to update."
		qmake-qt5 app.pro -spec linux-g++
	fi
else
	qmake-qt5 app.pro -spec linux-g++
fi
make -j `nproc`

%install
install -D blueprint64 %{buildroot}/usr/bin/blueprint64
install -D blueprint64.desktop %{buildroot}/usr/share/applications/blueprint64.desktop
install -D armips/armips %{buildroot}/usr/share/blueprint64/armips-default
install -D bbp-mime.xml %{buildroot}/usr/share/blueprint64/bbp-mime.xml
install -D data/mime/icon16.png %{buildroot}/usr/share/icons/hicolor/16x16/mimetypes/application-x-bbp-blueprint.png
install -D data/mime/icon22.png %{buildroot}/usr/share/icons/hicolor/22x22/mimetypes/application-x-bbp-blueprint.png
install -D data/mime/icon24.png %{buildroot}/usr/share/icons/hicolor/24x24/mimetypes/application-x-bbp-blueprint.png
install -D data/mime/icon32.png %{buildroot}/usr/share/icons/hicolor/32x32/mimetypes/application-x-bbp-blueprint.png
install -D data/mime/icon48.png %{buildroot}/usr/share/icons/hicolor/48x48/mimetypes/application-x-bbp-blueprint.png
install -D data/mime/icon64.png %{buildroot}/usr/share/icons/hicolor/64x64/mimetypes/application-x-bbp-blueprint.png

%files
/usr/bin/blueprint64
/usr/share/applications/blueprint64.desktop
/usr/share/blueprint64/armips-default
/usr/share/blueprint64/bbp-mime.xml
/usr/share/icons/hicolor/16x16/mimetypes/application-x-bbp-blueprint.png
/usr/share/icons/hicolor/22x22/mimetypes/application-x-bbp-blueprint.png
/usr/share/icons/hicolor/24x24/mimetypes/application-x-bbp-blueprint.png
/usr/share/icons/hicolor/32x32/mimetypes/application-x-bbp-blueprint.png
/usr/share/icons/hicolor/48x48/mimetypes/application-x-bbp-blueprint.png
/usr/share/icons/hicolor/64x64/mimetypes/application-x-bbp-blueprint.png

%preun
if [ $1 -eq 0 ] ; then
	xdg-mime uninstall /usr/share/blueprint64/bbp-mime.xml 2> /dev/null || true
fi
exit 0

%postun
if [ $1 -eq 0 ] ; then
	xdg-icon-resource forceupdate 2> /dev/null || true
	update-desktop-database /usr/share/applications 2> /dev/null || true
fi
exit 0

%post
if [ $1 -eq 1 ] ; then
	xdg-icon-resource forceupdate 2> /dev/null || true
	xdg-mime install /usr/share/blueprint64/bbp-mime.xml
	update-desktop-database /usr/share/applications 2> /dev/null || true
fi
exit 0
