# Building on Linux

## Setup
- Install the dev dependencies.
  - Debian/Ubuntu/Mint: `sudo apt install build-essential qt5-qmake qtdeclarative5-dev zlib1g-dev debianutils`
  - OpenSUSE: `sudo zypper install gcc9-c++ libqt5-qtbase-common-devel libqt5-qtdeclarative-devel zlib-devel which`
  - Fedora/CentOS: `sudo yum install gcc9-c++ libqt5-qtbase-common-devel libqt5-qtdeclarative-devel zlib-devel which`
  - Arch/Manjaro: `sudo pacman -S gcc qt5-declarative make lzlib which`
- If you are editing the UI, you will also want to install either Qt Designer or Qt Creator
  
## Building
- Build the makefile with `./qmake-release.sh` (or `./qmake-debug.sh` for the debug build)
- Run `make` to build

# Building on Windows

## Setup
- Install Visual Studio 2019
- Install "Desktop development with C++" from the Visual Studio installer
- Run `C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat`
- Download Qt from http://download.qt.io/official_releases/qt/5.12/5.12.10/qt-opensource-windows-x86-5.12.10.exe
- Run the installer and select the MSVC 2017 component to install as well as the defaults
- Run `qtenv2.bat`
- In Qt Creator, go to Tools -> Options, then set your cmake location if it was not set automatically
- Open the `app.pro` file in Qt Creator
- Set the build directory to your checkout directory and set your build configuration to release (or debug)
- Build the project for the first time in Qt Creator
- Run `windeployqt release/blueprint64.exe`
- Copy the C++ runtimes DLLs from your system to the `release` directory (they will be located in a directory path something like `C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Redist\MSVC\14.28.29325\onecore\x64`)
- Download the zlib development libraries from here: https://www.nuget.org/packages/zlib-msvc14-x64/ (rename the .nupkg extension to .zip)
- Create a `windows` folder in your checkout directory and put an `include` and `lib` directory inside it. Copy the zlib header files and libraries to the respective folders. Also copy the zlib dll to the `release` and/or `debug` folder.
- Optionally, you can also build the style plugins to make more themes available (see below)

## Building
- If you made changes to the ui files or changed build configurations, run qmake again
- Build the project from Qt Creator.

## Building the Styles/Themes (Optional)
- Clone the repository hosted at https://github.com/qt/qtstyleplugins into some temporary directory
- Open the project file in Qt Creator
- Go to the Projects tab and set the build configuration to Release
- Run qmake and build from Qt Creator
- Copy the built plugin DLL files to the `style` directory in your debug/release folder for Bowser's Blueprints

## Building the Installer
- Download and install Inno Setup (https://jrsoftware.org/isinfo.php)
- Before running the installer, remove the `qrc_resources.cpp` file from your release folder
- Open win-installer.iss in the source directory
- Click the green play button to build the installer
