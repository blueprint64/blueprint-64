#define MyAppName "Bowser's Blueprints"
#define MyAppVersion "Alpha 10.0"
#define MyAppPublisher "Matt Pharoah"
#define MyAppURL "https://blueprint64.ca/"
#define MyAppExeName "blueprint64.exe"

#define RegistryId "Bowsers_Blueprints"

[Setup]
AppId={{278E688E-1E1F-49DC-B41B-408254F57FE8}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={autopf}\Bowsers_Blueprints
DisableProgramGroupPage=yes
LicenseFile={#SourcePath}\LICENSE
PrivilegesRequired=admin
OutputDir={#SourcePath}
OutputBaseFilename=blueprint64_setup_win64
Compression=lzma
SolidCompression=yes
WizardStyle=modern
ChangesAssociations=yes

[Registry]
; Associate .bbp files with Bowser's Blueprints
Root: HKCR; Subkey: ".bbp"; ValueName: ""; ValueType: string; ValueData: "{#RegistryId}"; Flags: uninsdeletevalue
Root: HKCR; Subkey: "{#RegistryId}"; ValueName: ""; ValueType: string; ValueData: "Blueprint"; Flags: uninsdeletekey
Root: HKCR; Subkey: "{#RegistryId}\DefaultIcon"; ValueName: ""; ValueType: string; ValueData: "{app}\{#MyAppExeName},0"
Root: HKCR; Subkey: "{#RegistryId}\shell\open\command"; ValueName: ""; ValueType: string; ValueData: """{app}\{#MyAppExeName}"" ""%1"""

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce

[Files]
Source: "{#SourcePath}\release\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
Source: "{#SourcePath}\data\fonts\sm64.fon"; DestDir: "{fonts}"; FontInstall: "Blueprint SM64"; Flags: fontisnttruetype onlyifdoesntexist uninsneveruninstall

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
